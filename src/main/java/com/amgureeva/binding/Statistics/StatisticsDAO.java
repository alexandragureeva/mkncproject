package com.amgureeva.binding.Statistics;

import java.util.List;

/**
 * Created by Александра on 10.05.2016.
 */
public interface StatisticsDAO {
    List<String> getAllHeaders();
    void getValues();

}
