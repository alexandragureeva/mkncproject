package com.amgureeva.binding.Statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Александра on 10.05.2016.
 */
@Service("statisticsService")
public class StatisticsServiceImpl implements StatisticsService {

    StatisticsDAO statisticsDAO;

    @Autowired
    public void setStatisticsDAO(StatisticsDAO statisticsDAO) {this.statisticsDAO = statisticsDAO;}

    @Override
    public List<String> getHeaders() {
        return statisticsDAO.getAllHeaders();
    }
}
