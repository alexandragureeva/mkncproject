package com.amgureeva.binding.Statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Александра on 10.05.2016.
 */
@Repository
public class StatisticsDAOImpl implements StatisticsDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<String> getAllHeaders() {
        String sql = "select sm_name, p.name from diagnosisname, parametersname p WHERE klatskin = sm_name OR " +
                "gcr = sm_name or hcr = sm_name OR alv = sm_name OR mkr = sm_name OR mnkr = sm_name OR" +
                " ech = sm_name OR rzh = sm_name OR zhkb=sm_name GROUP BY sm_name, id";
        List<String> result = namedParameterJdbcTemplate.query(sql, new StatisticsMapper("ПОСТ"));

		sql = "select sm_name, p.name from diagnosisname, parametersname p WHERE klatskin = sm_name OR " +
				"gcr = sm_name or hcr = sm_name OR alv = sm_name OR mkr = sm_name OR mnkr = sm_name OR" +
				" ech = sm_name OR rzh = sm_name GROUP BY sm_name, id";
		List<String> result2 = namedParameterJdbcTemplate.query(sql, new StatisticsMapper("ВЫПИС"));
        sql = "select sm_name, s.name from diagnosisname, surgeryparametersname s WHERE klatskin = sm_name OR " +
                "gcr = sm_name or hcr = sm_name OR alv = sm_name OR mkr = sm_name OR mnkr = sm_name OR" +
                " ech = sm_name OR rzh = sm_name OR zhkb=sm_name GROUP BY sm_name, id";
        List<String> result3 = namedParameterJdbcTemplate.query(sql, new StatisticsMapper("ОПЕР"));
        for (int i = 0; i < result2.size(); i++) {
            if (result2.get(i).contains("zdren")) {
               result2.set(i, result2.get(i).substring(1));
            }
        }
        addHeaders(result);
		for (int i = 0; i < result2.size();) {
			if (result2.get(i).contains("OCENKA_TROF_STATUSA") || result2.get(i).contains("POLOZHIT_KULTURA_PECHENI") ||
					result2.get(i).contains("ALBENDAZOL") || result2.get(i).contains("PRED_OPERACII") ||
					result2.get(i).contains("EPIDEM_ANAMNEZ") || result2.get(i).contains("UZI_KRATNOST_LECHENIYA") ||
					result2.get(i).contains("UZI_KRATNOST_LECHENIYA_DETAILS")
					|| result2.get(i).contains("LECHENIE_SORAFENIBOM") || result2.get(i).contains("TACHE") ||
					result2.get(i).contains("PROTIVOVIRUS_TERAPIA") || result2.get(i).contains("NEOADJUV_TERAPIA") ||
					result2.get(i).contains("NEOADJUV_TERAPIA_KURS") || result2.get(i).contains("NEOADJUV_TERAPIA_PREPARATI") ||
					result2.get(i).contains("NEOADJUV_TERAPIA_EFFECT") || result2.get(i).contains("POJAVLENIE_METASTAZOV_TYPE") ||
					result2.get(i).contains("POJAVLENIE_METASTAZOV_VREMYA") || result2.get(i).contains("SCINTIGRAF_OBJ_OPERAC")  ||
					result2.get(i).contains("SCINTIGRAF_STAND_OBJEM")  || result2.get(i).contains("SCINTIGRAFIA_BUDUSH_OSTATOK")
					|| result2.get(i).contains("RAZMER_OPUCH") ) {
				 result2.remove(result2.get(i));
			} else {
				i++;
			}
		}
        result.addAll(result2);
		result.addAll(result3);
		addDrainHeaders(result);
		result.add("ДАТА_СМЕРТИ_ДЕНЬ");
		result.add("ДАТА_СМЕРТИ_МЕСЯЦ");
		result.add("ДАТА_СМЕРТИ_ГОД");
        return result;
    }

    @Override
    public void getValues() {
        String sql = "select patientId, sex, age, startDate, endDate, bedDays, p.name, value, sm_name from " +
                "patientparametersvalue, diagnosisname d, parametersname p, patient pa" +
                " where diagnosisId = diseaseId and parameterId=p.id and pa.id = patientId;";

    }

    private static final class StatisticsMapper implements RowMapper<String> {

		private String prefix;

		public StatisticsMapper(String prefix) {
			this.prefix = prefix;
		}

        public String mapRow(ResultSet rs, int rowNum) throws SQLException {
            String result = rs.getString(1) + "_" + prefix + ":" + rs.getString(2);
            return result;
        }
    }

    private void addHeaders(List<String> headers) {
        headers.add(0, "ПОЛ");
        headers.add(1, "ВОЗР");
        headers.add(2, "ПОСТУПЛ_ДЕНЬ");
        headers.add(3, "ПОСТУПЛ_МЕСЯЦ");
        headers.add(4, "ПОСТУПЛ_ГОД");
        headers.add(5, "ВЫП_ДЕНЬ");
        headers.add(6, "ВЫП_МЕСЯЦ");
        headers.add(7, "ВЫП_ГОД");
		headers.add(8, "КОЙКО_ДНИ");
        headers.add(9, "ВЕС");
        headers.add(10, "BMI");
        headers.add(11, "ASA");
        headers.add(12, "ПОСТУП_ДИАГНОЗ");
        headers.add(13, "ГОСПИТАЛИЗ");
        headers.add(14, "ЧИСЛО_ГОСПИТАЛИЗ");
        headers.add(15, "ИБС");
        headers.add(16, "СТЕНОКАРД_НАПРЯЖ_КЛАСС");
        headers.add(17, "ГИПЕРТОН_БОЛ");
		headers.add(18, "САХАР_ДИАБЕТ");
		headers.add(19, "ХВН");
		headers.add(20, "БРОНХ_АСТМА");
		headers.add(21, "БРОНХИТ");
		headers.add(22, "ЭМФИЗ_ЛЕГК");
		headers.add(23, "СЕРД_ПОРОКИ");
		headers.add(24, "ОНМК_АНАМН");
		headers.add(25, "БОЛ_ПАРКИНС");
		headers.add(26, "СИНДР_ПАРКИНС");
		headers.add(27, "ОЖИРЕНИЕ");
		headers.add(28, "ЯЗВЕН_БОЛ");
		headers.add(29, "ЯЗВЕН_БОЛ_ОСЛ");
		headers.add(30, "ДОБР_ГИПЕРПЛАЗ_ПРЕДСТАТ_ЖЕЛ");
		headers.add(31, "ДРУГ_БОЛ");
		headers.add(32, "АН_0_КР_ГЕМОГЛАБИН");
		headers.add(33, "АН_0_КР_ЭРИТРОЦ");
		headers.add(34, "АН_0_КР_ЛЕЙКОЦ");
		headers.add(35, "АН_0_МОЧ_РН");
		headers.add(36, "АН_0_МОЧ_БЕЛОК");
		headers.add(37, "АН_0_МОЧ_ЛЕЙК");
		headers.add(38, "АН_0_МОЧ_ЭРИТР");
		headers.add(39, "АН_0_МОЧ_ГЛЮКОЗА");
		headers.add(40, "АН_0_МОЧ_КЕТОНЫ");
		headers.add(41, "АН_0_БИОХИМ_АСТ");
		headers.add(42, "АН_0_БИОХИМ_АЛТ");
		headers.add(43, "АН_0_БИОХИМ_О_БЕЛОК");
		headers.add(44, "АН_0_БИОХИМ_АЛЬБУМ");
		headers.add(45, "АН_0_БИОХИМ_ОБ_БИЛИР");
		headers.add(46, "АН_0_БИОХИМ_ПР_БИЛИР");
		headers.add(47, "АН_0_БИОХИМ_КРЕАТИН");
		headers.add(48, "АН_0_БИОХИМ_САХ_КРОВ");
		headers.add(49, "АН_0_БИОХИМ_МОЧ_КИСЛ");
		headers.add(50, "АН_0_БИОХИМ_МОЧЕВИНА");
		headers.add(51, "АН_0_КОАГУЛ_МНО");
		headers.add(52, "АН_0_КОАГУЛ_АЧТВ");
		headers.add(53, "АН_0_ГРУП_КРОВИ");
		headers.add(54, "АН_0_КОАГУЛ_РЕЗУС_ФАКТОР");
		headers.add(55, "АН_0_ОНКОМАР_СЕА");
		headers.add(56, "АН_0_ОНКОМАР_СА_19_9");
		headers.add(57, "АН_0_ОНКОМАР_АЛЬФ_ФЕТ");
		headers.add(58, "АН_0_ОНКОМАР_СА125");
		headers.add(59, "АН_1_КР_ГЕМОГЛАБИН");
		headers.add(60, "АН_1_КР_ЭРИТРОЦ");
		headers.add(61, "АН_1_КР_ЛЕЙКОЦ");
		headers.add(62, "АН_1_КР_ПАЛ_ЯДЕР");
		headers.add(63, "АН_1_КР_СЕГМЕНТОЯДЕР");
		headers.add(64, "АН_1_КР_ЮНЫЕ");
		headers.add(65, "АН_1_КР_МОНОЦ");
		headers.add(66, "АН_1_КР_ЛИМФОЦ");
		headers.add(67, "АН_1_КР_ТРОМБОЦ");
		headers.add(68, "АН_1_КР_СОЭ");
		headers.add(69, "АН_1_МОЧ_РН");
		headers.add(70, "АН_1_МОЧ_БЕЛОК");
		headers.add(71, "АН_1_МОЧ_ЛЕЙК");
		headers.add(72, "АН_1_МОЧ_ЭРИТР");
		headers.add(73, "АН_1_МОЧ_ГЛЮКОЗА");
		headers.add(74, "АН_1_МОЧ_КЕТОНЫ");
		headers.add(75, "АН_1_БИОХИМ_АСТ");
		headers.add(76, "АН_1_БИОХИМ_АЛТ");
		headers.add(77, "АН_1_БИОХИМ_О_БЕЛОК");
		headers.add(78, "АН_1_БИОХИМ_АЛЬБУМ");
		headers.add(79, "АН_1_БИОХИМ_ОБ_БИЛИР");
		headers.add(80, "АН_1_БИОХИМ_ПР_БИЛИР");
		headers.add(81, "АН_1_БИОХИМ_КРЕАТИН");
		headers.add(82, "АН_1_БИОХИМ_САХ_КРОВ");
		headers.add(83, "АН_1_БИОХИМ_МОЧ_КИСЛ");
		headers.add(84, "АН_1_БИОХИМ_МОЧЕВИНА");
		headers.add(85, "АН_1_КОАГУЛ_МНО");
		headers.add(86, "АН_1_КОАГУЛ_АЧТВ");
		headers.add(87, "АН_5_КР_ГЕМОГЛАБИН");
		headers.add(88, "АН_5_КР_ЭРИТРОЦ");
		headers.add(89, "АН_5_КР_ЛЕЙКОЦ");
		headers.add(90, "АН_5_КР_ПАЛ_ЯДЕР");
		headers.add(91, "АН_5_КР_СЕГМЕНТОЯДЕР");
		headers.add(92, "АН_5_КР_ЮНЫЕ");
		headers.add(93, "АН_5_КР_МОНОЦ");
		headers.add(94, "АН_5_КР_ЛИМФОЦ");
		headers.add(95, "АН_5_КР_ТРОМБОЦ");
		headers.add(96, "АН_5_КР_СОЭ");
		headers.add(97, "АН_5_МОЧ_РН");
		headers.add(98, "АН_5_МОЧ_БЕЛОК");
		headers.add(99, "АН_5_МОЧ_ЛЕЙК");
		headers.add(100, "АН_5_МОЧ_ЭРИТР");
		headers.add(101, "АН_5_МОЧ_ГЛЮКОЗА");
		headers.add(102, "АН_5_МОЧ_КЕТОНЫ");
		headers.add(103, "АН_5_БИОХИМ_АСТ");
		headers.add(104, "АН_5_БИОХИМ_АЛТ");
		headers.add(105, "АН_5_БИОХИМ_О_БЕЛОК");
		headers.add(106, "АН_5_БИОХИМ_АЛЬБУМ");
		headers.add(107, "АН_5_БИОХИМ_ОБ_БИЛИР");
		headers.add(108, "АН_5_БИОХИМ_ПР_БИЛИР");
		headers.add(109, "АН_5_БИОХИМ_КРЕАТИН");
		headers.add(110, "АН_5_БИОХИМ_САХ_КРОВ");
		headers.add(111, "АН_5_БИОХИМ_МОЧ_КИСЛ");
		headers.add(112, "АН_5_БИОХИМ_МОЧЕВИНА");
		headers.add(113, "АН_5_КОАГУЛ_МНО");
		headers.add(114, "АН_5_КОАГУЛ_АЧТВ");
		headers.add(115, "АН_ВЫП_КР_ГЕМОГЛАБИН");
		headers.add(116, "АН_ВЫП_КР_ЭРИТРОЦ");
		headers.add(118, "АН_ВЫП_КР_ЛЕЙКОЦ");
		headers.add(119, "АН_ВЫП_КР_ПАЛ_ЯДЕР");
		headers.add(120, "АН_ВЫП_КР_СЕГМЕНТОЯДЕР");
		headers.add(121, "АН_ВЫП_КР_ЮНЫЕ");
		headers.add(122, "АН_ВЫП_КР_МОНОЦ");
		headers.add(123, "АН_ВЫП_КР_ЛИМФОЦ");
		headers.add(124, "АН_ВЫП_КР_ТРОМБОЦ");
		headers.add(125, "АН_ВЫП_КР_СОЭ");
		headers.add(126, "АН_ВЫП_МОЧ_РН");
		headers.add(127, "АН_ВЫП_МОЧ_БЕЛОК");
		headers.add(128, "АН_ВЫП_МОЧ_ЛЕЙК");
		headers.add(129, "АН_ВЫП_МОЧ_ЭРИТР");
		headers.add(130, "АН_ВЫП_МОЧ_ГЛЮКОЗА");
		headers.add(131, "АН_ВЫП_МОЧ_КЕТОНЫ");
		headers.add(132, "АН_ВЫП_БИОХИМ_АСТ");
		headers.add(133, "АН_ВЫП_БИОХИМ_АЛТ");
		headers.add(134, "АН_ВЫП_БИОХИМ_О_БЕЛОК");
		headers.add(135, "АН_ВЫП_БИОХИМ_АЛЬБУМ");
		headers.add(136, "АН_ВЫП_БИОХИМ_ОБ_БИЛИР");
		headers.add(137, "АН_ВЫП_БИОХИМ_ПР_БИЛИР");
		headers.add(138, "АН_ВЫП_БИОХИМ_КРЕАТИН");
		headers.add(139, "АН_ВЫП_БИОХИМ_САХ_КРОВ");
		headers.add(140, "АН_ВЫП_БИОХИМ_МОЧ_КИСЛ");
		headers.add(141, "АН_ВЫП_БИОХИМ_МОЧЕВИНА");
		headers.add(142, "АН_ВЫП_КОАГУЛ_МНО");
		headers.add(143, "АН_ВЫП_КОАГУЛ_АЧТВ");
		headers.add(144, "АН_ВЫП_ОНКОМАР_СЕА");
		headers.add(145, "АН_ВЫП_ОНКОМАР_СА_19_9");
		headers.add(146, "АН_ВЫП_ОНКОМАР_АЛЬФ_ФЕТ");
		headers.add(147, "АН_ВЫП_ОНКОМАР_СА125");
		headers.add(148, "ОБ_ОС_СОСТ_БОЛ");
		headers.add(149, "ОБ_ОС_ТЕМПЕР");
		headers.add(150, "ОБ_ОС_ЖЕЛТУХА");
		headers.add(151, "ОБ_ОС_ОДЫШКА");
		headers.add(152, "ОБ_ОС_ПУЛЬС");
		headers.add(153, "ОБ_ОС_ТАХИКАРД");
		headers.add(154, "ОБ_ОС_АД");
		headers.add(155, "ОБ_ОС_ТОШН");
		headers.add(156, "ОБ_ОС_РВОТА");
		headers.add(157, "ОБ_ОС_ЖИВОТ");
		headers.add(158, "ОБ_ОС_ПАЛЬПАЦ");
		headers.add(159, "ОБ_ОС_НАПРЯЖ_ЖИВ");
		headers.add(160, "ОБ_ОС_ПЕРИТОН_СИНДР");
		headers.add(161, "ОБ_ОС_ПЕРИСТАЛ");
		headers.add(162, "ОБ_ОС_ОТХОЖ_ГАЗ");
		headers.add(163, "ОБ_ОС_СТУЛ");
		headers.add(164, "ОБ_ОС_ДИЗУР");
		headers.add(165, "ОБ_ОС_ОБ_МОЧИ");
    }
	private void addDrainHeaders(List<String> headers) {
		headers.add("ДРЕН_КОЛВО_ДРЕН");
		headers.add("ДРЕН_НАЛ_ОСЛ");
		headers.add("ДРЕН_ДАТА");
		headers.add("ДРЕН_НАР_ДРЕНАЖИ");
		headers.add("ДРЕН_НАР_ВН_ДРЕН");
		headers.add("ДРЕН_БЛОК");
		headers.add("ДРЕН_ДЛИТ_ЖЕЛТ");
		headers.add("ДРЕН_РЕЦИС_ХОЛАНГ");
		headers.add("ДРЕН_АБС_ХОЛАНГ");
		headers.add("ДРЕН_ДОЛЯ");
		headers.add("ДРЕН_СВОРАЧ_ДРЕН_В_КОЛЬЦ");
		headers.add("ДРЕН_67_СЕГМ");
		headers.add("ДРЕН_58_СЕГМ");
		headers.add("ДРЕН_СУПРАП_ДРЕН");
		headers.add("ДРЕН_ЖЕЛ_ПОСЕВ_ИНФ");
		headers.add("ДРЕН_ЖП_КЛЕБС");
		headers.add("ДРЕН_ЖП_СИНЕГН_ПАЛ");
		headers.add("ДРЕН_КРОВ_ПОСЕВ_ИНФ");
		headers.add("ДРЕН_ОСЛОЖН");
	}
}
