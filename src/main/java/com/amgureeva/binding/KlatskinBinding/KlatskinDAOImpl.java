package com.amgureeva.binding.KlatskinBinding;

import com.amgureeva.Entities.Diagnosis.Klatskin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 28.04.2016.
 */
@Repository
public class KlatskinDAOImpl implements KlatskinDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    @Override
    public Klatskin findById(Integer id, boolean isFirstDiagnosis) {
        if (id != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
			params.put("admissiondiag", (isFirstDiagnosis)?1:0);

            String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=0" +
					" AND admissiondiag=:admissiondiag";

            List<List<Klatskin>> result = null;
            try {
                result = namedParameterJdbcTemplate
                        .query(sql, params, new KlatskinMapper());
            } catch (EmptyResultDataAccessException e) {
                // do nothing, return null
            }
			result.remove(null);
			if (result == null || result.size() == 0)
				return null;
			if (isFirstDiagnosis) {
				if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
			if (!isFirstDiagnosis) {
				if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
        }
        return null;
    }

    @Override
    public List<Klatskin> findAll() {
        String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 0";
        List<List<Klatskin>> result = namedParameterJdbcTemplate.query(sql, new KlatskinMapper());
        result.remove(null);
        return (result == null || result.size() == 0)?null :result.get(0);
    }

    @Override
    public void save(Klatskin klatskin) {
        String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
                + "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),2, String.valueOf(klatskin.getIcg()), klatskin.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),3, String.valueOf(klatskin.getIcgk_f()), klatskin.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),5, String.valueOf(klatskin.getBismuth()), klatskin.isDiagnosisAtStart())); //bismuth
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),6, String.valueOf(klatskin.getVorotInvasia()), klatskin.isDiagnosisAtStart())); //vorot invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),7, String.valueOf(klatskin.getPechInvasia()), klatskin.isDiagnosisAtStart())); //pechenochnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),8, String.valueOf(klatskin.getDifferOpuchol()), klatskin.isDiagnosisAtStart())); //stepen differenzirovki opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),9, String.valueOf((klatskin.isPerinevInvasia()) ? 1 : 0), klatskin.isDiagnosisAtStart())); //perinevralnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),10, String.valueOf((klatskin.isPolozhKultura()) ? 1 : 0), klatskin.isDiagnosisAtStart())); //polozhitelnaya kultura
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),11, String.valueOf(klatskin.getTrofStatus()), klatskin.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),1, String.valueOf(klatskin.getGrwr()), klatskin.isDiagnosisAtStart())); //grwr
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),27, String.valueOf(klatskin.getIndexZachvata()), klatskin.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),28, String.valueOf(klatskin.getObjemOstatka()), klatskin.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),26, String.valueOf(klatskin.getRezekciaType()), klatskin.isDiagnosisAtStart())); //objem
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),29, String.valueOf(klatskin.getRazmer_opucholi()), klatskin.isDiagnosisAtStart()));
    }

    @Override
    public void update(Klatskin klatskin) {
        String sql = "UPDATE patientparametersvalue SET value=:value"
                + " WHERE patientId=:patientId and diseaseId=0 and parameterId=:parameterId " +
				"and admissiondiag=:admissiondiag";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),2, String.valueOf(klatskin.getIcg()), klatskin.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),3, String.valueOf(klatskin.getIcgk_f()), klatskin.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),5, String.valueOf(klatskin.getBismuth()), klatskin.isDiagnosisAtStart())); //bismuth
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),6, String.valueOf(klatskin.getVorotInvasia()), klatskin.isDiagnosisAtStart())); //vorot invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),7, String.valueOf(klatskin.getPechInvasia()), klatskin.isDiagnosisAtStart())); //pechenochnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),8, String.valueOf(klatskin.getDifferOpuchol()), klatskin.isDiagnosisAtStart())); //stepen differenzirovki opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),9, String.valueOf((klatskin.isPerinevInvasia()) ? 1 : 0), klatskin.isDiagnosisAtStart())); //perinevralnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),10, String.valueOf((klatskin.isPolozhKultura()) ? 1 : 0), klatskin.isDiagnosisAtStart())); //polozhitelnaya kultura
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),11, String.valueOf(klatskin.getTrofStatus()), klatskin.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),1, String.valueOf(klatskin.getGrwr()), klatskin.isDiagnosisAtStart())); //grwr
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),27, String.valueOf(klatskin.getIndexZachvata()), klatskin.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),28, String.valueOf(klatskin.getObjemOstatka()), klatskin.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),26, String.valueOf(klatskin.getRezekciaType()), klatskin.isDiagnosisAtStart())); //objem
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (klatskin.getId(),29, String.valueOf(klatskin.getRazmer_opucholi()), klatskin.isDiagnosisAtStart()));
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=0";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }

    private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value,
													  boolean admissiondiag) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("patientId", patientId);
        paramSource.addValue("parameterId", fieldId);
        paramSource.addValue("diseaseId", 0);
        paramSource.addValue("value", value);
		paramSource.addValue("admissiondiag", admissiondiag);
        return paramSource;
    }


    private static final class KlatskinMapper implements RowMapper<List<Klatskin>> {

        public List<Klatskin> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<Klatskin> result = new ArrayList<Klatskin>();
            Klatskin klatskin = new Klatskin();
            int i = 0;
            do {
                klatskin.setId(rs.getString("patientId"));
				klatskin.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
                int parameterId = rs.getInt("parameterId");
                switch(parameterId) {
                    case 1: if (i!=0 && i!=14) return null;i=1;klatskin.setGrwr(rs.getInt("value")); break;
                    case 2: i++; klatskin.setIcg(rs.getInt("value"));break;
                    case 3: i++;klatskin.setIcgk_f(rs.getInt("value"));break;
                    case 5: i++;klatskin.setBismuth(rs.getInt("value"));break;
                    case 6: i++;klatskin.setVorotInvasia(rs.getInt("value"));break;
                    case 7: i++;klatskin.setPechInvasia(rs.getInt("value"));break;
                    case 8: i++;klatskin.setDifferOpuchol(rs.getInt("value"));break;
                    case 9: i++;klatskin.setPerinevInvasia((rs.getInt("value")==0) ? false : true);break;
                    case 10: i++;klatskin.setPolozhKultura((rs.getInt("value")==0) ? false : true);break;
                    case 11: i++;klatskin.setTrofStatus(rs.getInt("value")); break;
                    case 27: i++;klatskin.setIndexZachvata(rs.getString("value")); break;
                    case 28: i++;klatskin.setObjemOstatka(rs.getString("value")); break;
                    case 26: i++; klatskin.setRezekciaType(rs.getInt("value")); break;
                    case 29: i++; klatskin.setRazmer_opucholi(rs.getString("value")); break;
                }
                if (i==14) {
                    result.add(klatskin);
                    klatskin = new Klatskin();
                }
            }while (rs.next());
            return result;
        }
    }
}
