package com.amgureeva.binding.KlatskinBinding;

import com.amgureeva.Entities.Diagnosis.Klatskin;
import com.amgureeva.Entities.Diagnosis.KlatskinList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 04.05.2016.
 */
public class KlatskinListCreator {

    public List<KlatskinList> createKlatskinList (List<Klatskin> klatskins, PatientService patientService) {
       if (klatskins != null && patientService != null) {
           List<KlatskinList> klatskinList = new ArrayList<KlatskinList>();
           KlatskinList klatskinList_ = new KlatskinList();
           for (int i = 0; i < klatskins.size(); i++) {
               klatskinList_.setName(patientService.findPatientById(Integer.valueOf(klatskins.get(i).getId())).getName());
               switch (Integer.valueOf(klatskins.get(i).getBismuth())) {
                   case 0:
                       klatskinList_.setBismuth("--");
                       break;
                   case 1:
                       klatskinList_.setBismuth("I");
                       break;
                   case 2:
                       klatskinList_.setBismuth("II");
                       break;
                   case 3:
                       klatskinList_.setBismuth("IIIa");
                       break;
                   case 4:
                       klatskinList_.setBismuth("IIIb");
                       break;
                   case 5:
                       klatskinList_.setBismuth("IV");
                       break;
               }
               switch (Integer.valueOf(klatskins.get(i).getDifferOpuchol())) {
                   case 0:
                       klatskinList_.setDifferOpuchol("--");
                       break;
                   case 1:
                       klatskinList_.setDifferOpuchol("Низкая");
                       break;
                   case 2:
                       klatskinList_.setDifferOpuchol("Умеренная");
                       break;
                   case 3:
                       klatskinList_.setDifferOpuchol("Высокая");
                       break;
               }
               switch (Integer.valueOf(klatskins.get(i).getGrwr())) {
                   case 0:
                       klatskinList_.setGrwr("--");
                       break;
                   case 1:
                       klatskinList_.setGrwr("<0.8");
                       break;
                   case 2:
                       klatskinList_.setGrwr("0.8-1");
                       break;
                   case 3:
                       klatskinList_.setGrwr(">1");
                       break;
               }
               switch (Integer.valueOf(klatskins.get(i).getIcg())) {
                   case 0:
                       klatskinList_.setIcg("--");
                       break;
                   case 1:
                       klatskinList_.setIcg("<15%");
                       break;
                   case 2:
                       klatskinList_.setIcg("15-25%");
                       break;
                   case 3:
                       klatskinList_.setIcg(">25%");
                       break;
               }
               switch (Integer.valueOf(klatskins.get(i).getIcgk_f())) {
                   case 0:
                       klatskinList_.setIcgk_f("--");
                       break;
                   case 1:
                       klatskinList_.setIcgk_f("<0.4");
                       break;
                   case 2:
                       klatskinList_.setIcgk_f("0.4-0.6");
                       break;
                   case 3:
                       klatskinList_.setIcgk_f(">0.6");
                       break;
               }
               switch (Integer.valueOf(klatskins.get(i).getPechInvasia())) {
                   case 0:
                       klatskinList_.setPechInvasia("--");
                       break;
                   case 1:
                       klatskinList_.setPechInvasia("Ипсилатеральную");
                       break;
                   case 2:
                       klatskinList_.setPechInvasia("Контрлатеральную");
                       break;
                   case 3:
                       klatskinList_.setPechInvasia("В собственную печеночную");
                       break;
               }
               switch (Integer.valueOf(klatskins.get(i).getVorotInvasia())) {
                   case 0:
                       klatskinList_.setVorotInvasia("--");
                       break;
                   case 1:
                       klatskinList_.setVorotInvasia("В ствол");
                       break;
                   case 2:
                       klatskinList_.setVorotInvasia("В долевые ветви");
                       break;
                   case 3:
                       klatskinList_.setVorotInvasia("В сегментарные ветви");
                       break;
               }
               switch (Integer.valueOf(klatskins.get(i).getTrofStatus())) {
                   case 0:
                       klatskinList_.setTrofStatus("--");
                       break;
                   case 1:
                       klatskinList_.setTrofStatus("Хороший");
                       break;
                   case 2:
                       klatskinList_.setTrofStatus("Удовлетворительный");
                       break;
                   case 3:
                       klatskinList_.setTrofStatus("Неудовлетворительный");
                       break;
               }
               if (klatskins.get(i).getIndexZachvata().equals("null"))
                   klatskinList_.setIndexZachvata("--");
               else
                   klatskinList_.setIndexZachvata(klatskins.get(i).getIndexZachvata());
               if (klatskins.get(i).getObjemOstatka().equals("null"))
                   klatskinList_.setObjemOstatka("--");
               else
                   klatskinList_.setObjemOstatka(klatskins.get(i).getObjemOstatka());
               if (klatskins.get(i).isPerinevInvasia() == false)
                   klatskinList_.setPerinevInvasia("-");
               else
                   klatskinList_.setPerinevInvasia("+");
               if (klatskins.get(i).isPolozhKultura() == false)
                   klatskinList_.setPolozhKultura("-");
               else
                   klatskinList_.setPolozhKultura("+");
               klatskinList.add(klatskinList_);
               klatskinList_ = new KlatskinList();
           }

           return klatskinList;
       }
        return null;
    }

    public static String getAllVariants(String s) {
        s = s.replace("_ПОСТ", "");
        s = s.replace("_ВЫПИС", "");
        s = s.replace("_ОПЕР", "");
        if (s.equals("opkl:ICG")) {
            return "1 - \"<15%\"; 2 - \"15-25%\"; 3 - \">25%\"";
        }
        if (s.equals("opkl:ICGK-F")) {
            return "1 - \"<0.4\"; 2 - \"0.4-0.6\"; 3 -\">0.6\"";
        }
        if (s.equals("opkl:INVASIA_VOROTNAYA")) {
            return "1 - \"В ствол\"; 2 - \"В долевые ветви\"; 3 -\"В сегментарные ветви\"";
        }
        if (s.equals("opkl:INVASIYA_PECHENOCHNAYA")) {
            return "1 - \"Ипсилатеральную\"; 2 - \"Контрлатеральную\"; 3 -\"В собственную печеночную\"";
        }
        if (s.equals("opkl:OCENKA_TROF_STATUSA")) {
            return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 -\"Неудовлетворительный\"";
        }
        if (s.equals("opkl:STEPEN_DIFF_OPUCHOLI")) {
            return "1 - \"Низкая\"; 2 - \"Умеренная\"; 3 -\"Высокая\"";
        }
        if (s.equals("opkl:Bismuth")) {
            return "1 - \"I\"; 2 - \"II\"; 3 -\"IIIa\"; " +
                    "4 -\"IIIb\"; 5 -\"IV\"";
        }
        if (s.equals("opkl:GRWR")) {
            return "1 - \"<0.8\"; 2 - \"0.8-1\"; 3 -\">1\"";
        }

        if (s.equals("opkl:REZEKCIA")) {
            return "1 - \"Левосторонняя гемигепатэктомия (ЛГГ)\"; 2 - \"Правосторонняя гемигепатэктомия (ПГГ)\";" +
                    " 3 - \"ПГГ + 1-й сегмент\"; 4 - \"ЛГГ + 1-й сегмент\"";
        }
        if (s.equals("opkl:SOSUD_REZEKCIA")) {
            return "1 - \"Резекция воротной вены\"; 2 - \"Резекция печеночной артерии\"";
        }
        if (s.equals("opkl:VOROT_REZEKCIA")) {
            return "1 - \"Краевая\"; 2 - \"Циркулярная без протезирования\"; 3 - \"Циркулярная с протезированием\"";
        }
        if (s.equals("opkl:KROVOPOTERYA")) {
            return "1 - \"<500\"; 2 - \"500-1000\"; 3 - \">1000\"";
        }
        if (s.equals("opkl:GEATYPE1")) {
            return "1 - \"Единый\"; 2 - \"Раздельный\"";
        }
        if (s.equals("opkl:GEATYPE2")) {
            return "1 - \"Моно-ГЕА\"; 2 - \"Би-ГЕА\"; 3 - \"Три-ГЕА\"; 4 - \"Тетра-ГЕА\";" +
                    " 5 - \"Пента-ГЕА\"; 6 - \"Мульти-ГЕА\"";
        }
        if (s.equals("opkl:GEATYPE3")) {
            return "1 - \"С ТПД\"; 2 - \"Без ТПД\"";
        }
        if (s.equals("opkl:RADIKALNOST")) {
            return "1 - \"R0\"; 2 - \"R1\"; 3 - \"R2\"";
        }
        if (s.equals("opkl:LIMFODISSEKCIA")) {
            return "1 - \"ГДС\"; 2 - \"ГДС+ОПА\"; 3 - \"ГДС+ОПА+ЧС\"";
        }
        if (s.equals("opkl:TNM")) {
            return "1 - \"T1N0M0\"; 2 - \"T2aN0M0\"; 3 - \"T2bN0M0\"; 4 - \"T3N0M0\"; 5 - \"T1N1M0\"; 6 - \"T2N1M0\";" +
                    "7 - \"T3N1M0\"; 8 - \"T4N0M0\"; 9 - \"T4N1M0\"; 10 - \"Любая T, N2M0\"; 11 - \"Любая T, любая N, M1\"";
        }
        if (s.equals("opkl:OSLOZH")) {
            return "1 - \"Внутрибрюшное кровотечение\"; 2 - \"ЖКК\"; 3 - \"Печеночная недостаточность A\";" +
                    " 4 - \"Печеночная недостаточность B\"; 5 - \"Печеночная недостаточность C\";" +
                    " 6 - \"Механическая желтуха\"; 7 - \"Нагноение раны\"; 8 - \"Холангит\";" +
                    " 9 - \"Холангиогенное абсцедирование\"; 10 - \"Асцит\"; 11 - \"Эвентрация\"" +
                    "; 12 - \"Кишечная непроходимость\"; 13 - \"Панкреатический свищ\"; 14 - \"Острый панкреатит\";" +
                    " 15 - \"Сепсис\"; 16 - \"Тяжелый сепсис\"; 17 - \"Септический шок\"; 18 - \"Желчный свищ\"" +
                    "; 19 - \"Несостоятельность анастомоза\"; 20 - \"Кишечный свищ\"; 21 - \"Тромбоз воротной вены\";" +
					" 22 - \"Тромбоз печеночной артерии\";" +
                    " 23 - \"Односторонняя пневмония\"" +
                    "; 24 - \"Двусторонняя пневмония\"; 25 - \"Гидроторакс\"; 26 - \"Почечная недостаточность\"" +
                    "; 27 - \"Сердечно-сосудистая недостаточность\"; 28 - \"Полиорганная недостаточность\"";
        }
        if (s.equals("opkl:CLAVIEN")) {
            return "1 - \"I\"; 2 - \"II\"; 3 - \"IIIa\"; 4 - \"IIIb\"; 5 - \"IVa\"; 6 - \"IVb\"; 7 - \"V\"";
        }

        if (s.equals("opkl:SCINTIGRAF_OBJ_OPERAC")) {
           return "1 - \"Сегментарная\"; 2 - \"Обширная\"";
        }
        return "";
    }
}