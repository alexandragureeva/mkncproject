package com.amgureeva.binding.KlatskinBinding;

import com.amgureeva.Entities.Diagnosis.Klatskin;

import java.util.List;

/**
 * Created by Александра on 28.04.2016.
 */
public interface KlatskinDAO {
    Klatskin findById(Integer id, boolean isFirstDiagnosis);

    List<Klatskin> findAll();

    void save(Klatskin patient);

    void update(Klatskin patient);

    void delete(Integer patientId);
}
