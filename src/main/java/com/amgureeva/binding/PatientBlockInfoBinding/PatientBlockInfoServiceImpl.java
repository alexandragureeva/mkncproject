package com.amgureeva.binding.PatientBlockInfoBinding;

import com.amgureeva.Entities.Editor;
import com.amgureeva.Entities.Patient;
import com.amgureeva.Entities.PatientBlockInfo;
import com.amgureeva.binding.EditorBinding.EditorDAO;
import com.amgureeva.binding.PatientBinding.PatientDao;
import com.amgureeva.binding.UserBinding.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Nikita on 25.08.2016.
 */
@Service("patientBlockService")
public class PatientBlockInfoServiceImpl implements PatientBlockInfoService {


	PatientDao patientDao;
	UserDAO userDAO;
	EditorDAO editorDAO;

	@Autowired
	public void setPatientDao(PatientDao patientDao) {
		this.patientDao = patientDao;
	}

	@Autowired
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Autowired
	public void setEditorDAO(EditorDAO editorDAO) {
		this.editorDAO = editorDAO;
	}

	@Override
	public List<PatientBlockInfo> getAll() {
		List<PatientBlockInfo> patientBlockInfos = new ArrayList<PatientBlockInfo>();
		List<Editor> editors = editorDAO.findAll();
		for (Editor editor : editors) {
			if (!editor.isFull()) {
				PatientBlockInfo patientBlockInfo = new PatientBlockInfo();
				Calendar today = Calendar.getInstance();
				today.add(Calendar.DATE, -1);
				if (editor.getDate().before(today.getTime())) {
					patientBlockInfo.setExpired(true);
				}
				int dateDiff = (int) (Math.ceil((editor.getDate().getTime() - today.getTimeInMillis()) / (1000 * 60 * 60 * 24)));
				Patient p = patientDao.findById(Integer.valueOf(editor.getPatientId()));
				patientBlockInfo.setId(editor.getPatientId());
				patientBlockInfo.setDateDiff(dateDiff);
				patientBlockInfo.setPatientName(p.getName());
				patientBlockInfo.setUserName(userDAO.findById(Integer.valueOf(editor.getid())).
						getUserName());
				patientBlockInfo.setBlockId(editor.getBlockId());
				patientBlockInfo.setToDate(editor.getFormattedDate());
				switch (Integer.valueOf(editor.getBlockId())) {
					case 0:
						patientBlockInfo.setPatientBlock("Поступление");
						break;
					case 1:
						patientBlockInfo.setPatientBlock("Операция");
						break;
					case 3:
						patientBlockInfo.setPatientBlock("Выписка");
						break;
				}

				patientBlockInfos.add(patientBlockInfo);
			}
		}
		return patientBlockInfos;
	}

	@Override
	public List<PatientBlockInfo> getAllNotExpired() {
		Map<String, Integer> fullBlocks = new HashMap<String, Integer>();
		List<PatientBlockInfo> patientBlockInfos = new ArrayList<PatientBlockInfo>();
		List<Editor> editors = editorDAO.findAll();
		for (Editor editor : editors) {
			Calendar today = Calendar.getInstance();
			today.add(Calendar.DATE, -1);
			int dateDiff = (int) (Math.ceil((editor.getDate().getTime() - today.getTimeInMillis()) / (1000 * 60 * 60 * 24)));
			if (dateDiff > 0) {
				Patient p = patientDao.findById(Integer.valueOf(editor.getPatientId()));
				if (!editor.isFull()) {
					PatientBlockInfo patientBlockInfo = new PatientBlockInfo();
					patientBlockInfo.setId(editor.getPatientId());
					patientBlockInfo.setDateDiff(dateDiff);
					patientBlockInfo.setPatientName(p.getName());
					patientBlockInfo.setUserName(userDAO.findById(Integer.valueOf(editor.getid())).
							getUserName());
					switch (Integer.valueOf(editor.getBlockId())) {
						case 0:
							patientBlockInfo.setPatientBlock("Поступление");
							break;
						case 1:
							patientBlockInfo.setPatientBlock("Ведение пациента");
							break;
					}
					patientBlockInfos.add(patientBlockInfo);
				}
			} else {
				editor.setExpired(true);
				if (editor.isFull()) {
					if (fullBlocks.containsKey(editor.getPatientId())) {
						int val = fullBlocks.get(editor.getPatientId());
						fullBlocks.put(editor.getPatientId(), ++val);
					} else {
						fullBlocks.put(editor.getPatientId(), 1);
					}
					/**/
				}
				editorDAO.update(editor);
			}
		}
		List<Integer> numbers = new ArrayList<Integer>(fullBlocks.values());
		List<String> patientIds = new ArrayList<String>(fullBlocks.keySet());
		for (int i = 0; i < numbers.size(); i++) {
			if (numbers.get(i) == 2) {
				editorDAO.delete(Integer.valueOf(patientIds.get(i)), null);
			}
		}
		return patientBlockInfos;
	}
}
