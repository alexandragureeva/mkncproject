package com.amgureeva.binding.PatientBlockInfoBinding;

import com.amgureeva.Entities.PatientBlockInfo;

import java.util.List;

/**
 * Created by Nikita on 25.08.2016.
 */
public interface PatientBlockInfoService {

	List<PatientBlockInfo> getAll();
	List<PatientBlockInfo> getAllNotExpired();
}
