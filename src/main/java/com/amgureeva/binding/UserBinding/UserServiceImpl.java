package com.amgureeva.binding.UserBinding;

import com.amgureeva.Entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Александра on 09.05.2016.
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    UserDAO userDAO;

    @Autowired
    public void setUserDao(UserDAO userDao) {
        this.userDAO = userDao;
    }


    @Override
    public User findById(Integer id) {
        return userDAO.findById(id);
    }

    @Override
    public User findByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public List<User> findAll() {
        return userDAO.findAll();
    }

    @Override
    public void saveOrUpdate(User user) {
        if (user.getUserId() == null) {
            userDAO.save(user);
        } else {
            userDAO.update(user);
        }
    }

    @Override
    public void delete(int id) {
        userDAO.delete(id);
    }
}
