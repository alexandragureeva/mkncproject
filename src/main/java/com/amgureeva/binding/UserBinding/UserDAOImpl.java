package com.amgureeva.binding.UserBinding;

import com.amgureeva.Entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 09.05.2016.
 */
@Repository
public class UserDAOImpl implements UserDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public User findById(Integer id) {
        if (id == null) {
            return null;
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);

        String sql = "SELECT * FROM user INNER JOIN profile ON user.id = p_id " +
                "INNER JOIN  userrole r ON user.id = r.user_role_id WHERE id=:id" +
                "  GROUP BY user_role_id";

        User result = null;
        try {
            result = namedParameterJdbcTemplate
                    .queryForObject(sql, params, new UserMapper());
        } catch (EmptyResultDataAccessException e) {
            // do nothing, return null
        }

        return result;
    }

    @Override
    public User findByEmail(String email) {
        if (email == null) {
            return null;
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", email);

        String sql = "SELECT * FROM user INNER JOIN profile ON user.id = p_id " +
                "INNER JOIN  userrole r ON user.id = r.user_role_id WHERE email=:email" +
                " GROUP BY user_role_id";

        User result = null;
        try {
            result = namedParameterJdbcTemplate
                    .queryForObject(sql, params, new UserMapper());
        } catch (EmptyResultDataAccessException e) {
            // do nothing, return null
        }

        return result;
    }

    @Override
    public List<User> findAll() {
        String sql = "SELECT * FROM user INNER JOIN profile ON user.id = p_id " +
                "INNER JOIN  userrole r ON user.id = r.user_role_id GROUP BY user_role_id";
        List<User> result = namedParameterJdbcTemplate.query(sql, new UserMapper());
        return result;
    }

    @Override
    public void save(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        String sql = "INSERT INTO user(password, email, enabled) "
                + "VALUES ( :password, :email, 1)";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(user), keyHolder);
        user.setUserId(keyHolder.getKey().longValue());
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("user_role_id",user.getUserId());
        sql = "INSERT INTO userrole(user_role_id, role) VALUES (:user_role_id, \"ROLE_USER\")";
        namedParameterJdbcTemplate.update(sql, parameterSource);
        if (user.isAdmin()) {
            parameterSource.addValue("user_role_id",user.getUserId());
            sql = "INSERT INTO userrole(user_role_id, role) VALUES (:user_role_id, \"ROLE_ADMIN\")";
            namedParameterJdbcTemplate.update(sql, parameterSource);
        }
        parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("p_id",user.getUserId());
        parameterSource.addValue("name",user.getUserName());
        sql = "INSERT INTO profile (p_id, name) VALUES (:p_id, :name)";
        namedParameterJdbcTemplate.update(sql, parameterSource);
    }

    @Override
    public void update(User user) {
        String sql = "UPDATE user SET password=:password, email=:email WHERE id=:id";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(user));
        sql = "UPDATE profile SET name=:name, photo=:photo WHERE p_id=:id";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(user));
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM profile WHERE p_id= :p_id";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("p_id", id));
        sql = "DELETE FROM userrole WHERE user_role_id= :user_role_id";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("user_role_id", id));
        sql = "DELETE FROM user WHERE id= :id";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }

    private SqlParameterSource getSqlParameterByModel(User user) {

        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("id", user.getUserId());
        paramSource.addValue("name", user.getUserName());
        paramSource.addValue("password", user.getHash());
        paramSource.addValue("photo", user.getPhoto());
        paramSource.addValue("email", user.getEmail());

        return paramSource;
    }

    private static final class UserMapper implements RowMapper<User> {

        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setUserId(rs.getLong("id"));
            user.setUserName(rs.getString("name"));
            user.setEmail(rs.getString("email"));
            user.setPhoto(rs.getString("photo"));
            user.setHash(rs.getString("password"));
            user.setAdmin((rs.getString("role").equals("ROLE_ADMIN"))?true:false);
            return user;
        }
    }
}
