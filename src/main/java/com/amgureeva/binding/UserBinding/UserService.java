package com.amgureeva.binding.UserBinding;

import com.amgureeva.Entities.User;

import java.util.List;

/**
 * Created by Александра on 09.05.2016.
 */
public interface UserService {
    User findById(Integer id);

    User findByEmail(String email);

    List<User> findAll();

    void saveOrUpdate(User gpi);

    void delete(int id);
}
