package com.amgureeva.binding.EditorBinding;

import com.amgureeva.Entities.Editor;

import java.util.List;

/**
 * Created by Nikita on 19.08.2016.
 */
public interface EditorDAO {
	void save(Editor editor);
	void update (Editor editor);
	void delete (Integer patientId, Integer blockId);
	List<Editor> findAll ();
	List<Editor> findById(Integer patientId);
	Editor findBlockEditorById(Integer patientId, Integer blockId);
}
