package com.amgureeva.binding.EditorBinding;

import com.amgureeva.Entities.Editor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 19.08.2016.
 */
@Repository
public class EditorDAOImpl implements EditorDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public void save(Editor editor) {
		String sql = "INSERT INTO editor(userId, patientId, blockId, date, full) " +
				"VALUES (:userId, :patientId, :blockId, :date, :full)";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(editor));
	}

	@Override
	public void update(Editor editor) {
		String sql = "UPDATE editor SET expired=:expired, full=:full, date=:date" +
				" WHERE patientId=:patientId AND blockId=:blockId";

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("patientId", editor.getPatientId());
		params.put("blockId", editor.getBlockId());
		params.put("expired", editor.isExpired());
		params.put("full", editor.isFull());
		params.put("date", editor.getDate());
		namedParameterJdbcTemplate.update(sql, params);
	}

	@Override
	public void delete(Integer patientId, Integer blockId) {
		String sql;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("patientId", patientId);
		if (blockId == null) {
			sql = "DELETE FROM editor WHERE patientId= :patientId";
		} else {
			sql = "DELETE FROM editor WHERE patientId= :patientId AND blockId=:blockId";
			params.put("blockId", blockId);

		}
		namedParameterJdbcTemplate.update(sql, params);
	}

	@Override
	public List<Editor> findAll() {
		String sql = "SELECT * FROM editor ORDER BY date, blockId";
		List<Editor> result = namedParameterJdbcTemplate.query(sql, new EditorDAOImpl.EditorMapper());
		return result;
	}

	@Override
	public List<Editor> findById(Integer patientId) {
		if (patientId == null) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("patientId", patientId);

		String sql = "SELECT * FROM editor WHERE patientId=:patientId";

		List<Editor> result = null;
		try {
			result = namedParameterJdbcTemplate
					.query(sql, params, new EditorDAOImpl.EditorMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}

		return result;
	}

	@Override
	public Editor findBlockEditorById(Integer patientId, Integer blockId) {
		if (patientId == null || blockId == null) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("patientId", patientId);
		params.put("blockId", blockId);

		String sql = "SELECT * FROM editor WHERE blockId=:blockId AND patientId=:patientId";

		Editor result = null;
		try {
			result = namedParameterJdbcTemplate
					.queryForObject(sql, params, new EditorDAOImpl.EditorMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}

		return result;
	}

	private SqlParameterSource getSqlParameterByModel(Editor editor) {

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("patientId", editor.getPatientId());
		paramSource.addValue("userId", editor.getid());
		paramSource.addValue("blockId", editor.getBlockId());
		paramSource.addValue("date", editor.getDate());
		paramSource.addValue("expired", editor.isExpired());
		paramSource.addValue("full", editor.isFull());
		return paramSource;
	}

	private static final class EditorMapper implements RowMapper<Editor> {

		public Editor mapRow(ResultSet rs, int rowNum) throws SQLException {
			Editor editor = new Editor();
			editor.setid(rs.getString("userId"));
			editor.setPatientId(rs.getString("patientId"));
			editor.setBlockId(rs.getString("blockId"));
			editor.setExpired(rs.getBoolean("expired"));
			editor.setFull(rs.getBoolean("full"));
			editor.setDate(rs.getDate("date"));
			return editor;
		}
	}
}
