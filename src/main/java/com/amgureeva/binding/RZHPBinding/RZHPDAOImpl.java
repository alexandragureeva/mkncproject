package com.amgureeva.binding.RZHPBinding;

import com.amgureeva.Entities.Diagnosis.RZHP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 28.04.2016.
 */
@Repository
public class RZHPDAOImpl implements RZHPDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    @Override
    public RZHP findById(Integer id, boolean isFirstDiagnosis) {
        if (id != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
			params.put("admissiondiag", (isFirstDiagnosis)?1:0);

			String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=5" +
					" AND admissiondiag=:admissiondiag";
            List<List<RZHP>> result = null;
            try {
                result = namedParameterJdbcTemplate
                        .query(sql, params, new RZHPMapper());
            } catch (EmptyResultDataAccessException e) {
                // do nothing, return null
            }
			result.remove(null);
			if (result == null || result.size() == 0)
				return null;
			if (isFirstDiagnosis) {
				if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
			if (!isFirstDiagnosis) {
				if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
        }
        return null;
    }

    @Override
    public List<RZHP> findAll() {
        String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 5";
        List<List<RZHP>> result = namedParameterJdbcTemplate.query(sql, new RZHPMapper());
        return (result == null || result.size() == 0)?null :result.get(0);
    }

    @Override
    public void save(RZHP rzhp) {
        String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
                + "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),2, String.valueOf(rzhp.getIcg()), rzhp.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),3, String.valueOf(rzhp.getIcgk_f()), rzhp.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),37, String.valueOf(rzhp.getStadPervOpuch()), rzhp.isDiagnosisAtStart())); //bismuth
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),6, String.valueOf(rzhp.getVorotInvasia()), rzhp.isDiagnosisAtStart())); //vorot invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),7, String.valueOf(rzhp.getPechInvasia()), rzhp.isDiagnosisAtStart())); //pechenochnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),8, String.valueOf(rzhp.getDifferOpuchol()), rzhp.isDiagnosisAtStart())); //stepen differenzirovki opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),9, String.valueOf((rzhp.isPerinevInvasia()) ? 1 : 0), rzhp.isDiagnosisAtStart())); //perinevralnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),10, String.valueOf((rzhp.isPolozhKultura()) ? 1 : 0), rzhp.isDiagnosisAtStart())); //polozhitelnaya kultura
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),11, String.valueOf(rzhp.getTrofStatus()), rzhp.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),52, String.valueOf(rzhp.getDiagnRaka()), rzhp.isDiagnosisAtStart())); //grwr
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),27, String.valueOf(rzhp.getIndexZachvata()), rzhp.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),28, String.valueOf(rzhp.getObjemOstatka()), rzhp.isDiagnosisAtStart())); //objem funkc doli pecheni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),26, String.valueOf(rzhp.getRezekciaType()), rzhp.isDiagnosisAtStart())); //objem
    }

    @Override
    public void update(RZHP rzhp) {
        String sql = "UPDATE patientparametersvalue SET value=:value"
                + " WHERE patientId=:patientId and diseaseId=5 and parameterId=:parameterId and " +
                "admissiondiag=:admissiondiag";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),2, String.valueOf(rzhp.getIcg()), rzhp.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),3, String.valueOf(rzhp.getIcgk_f()), rzhp.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),37, String.valueOf(rzhp.getStadPervOpuch()), rzhp.isDiagnosisAtStart())); //bismuth
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),6, String.valueOf(rzhp.getVorotInvasia()), rzhp.isDiagnosisAtStart())); //vorot invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),7, String.valueOf(rzhp.getPechInvasia()), rzhp.isDiagnosisAtStart())); //pechenochnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),8, String.valueOf(rzhp.getDifferOpuchol()), rzhp.isDiagnosisAtStart())); //stepen differenzirovki opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),9, String.valueOf((rzhp.isPerinevInvasia()) ? 1 : 0), rzhp.isDiagnosisAtStart())); //perinevralnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),10, String.valueOf((rzhp.isPolozhKultura()) ? 1 : 0), rzhp.isDiagnosisAtStart())); //polozhitelnaya kultura
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),11, String.valueOf(rzhp.getTrofStatus()), rzhp.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),52, String.valueOf(rzhp.getDiagnRaka()), rzhp.isDiagnosisAtStart())); //grwr
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),27, String.valueOf(rzhp.getIndexZachvata()), rzhp.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),28, String.valueOf(rzhp.getObjemOstatka()), rzhp.isDiagnosisAtStart())); //objem funkc doli pecheni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (rzhp.getId(),26, String.valueOf(rzhp.getRezekciaType()), rzhp.isDiagnosisAtStart())); //objem
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=5";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }

    private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value, boolean admissiondiag) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("patientId", patientId);
        paramSource.addValue("parameterId", fieldId);
        paramSource.addValue("diseaseId", 5);
        paramSource.addValue("value", value);
        paramSource.addValue("admissiondiag", admissiondiag);
        return paramSource;
    }


    private static final class RZHPMapper implements RowMapper<List<RZHP>> {

        public List<RZHP> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<RZHP> rzhpList = new ArrayList<RZHP>();
            RZHP rzhp = new RZHP();
            int i = 0;
            do {
                rzhp.setId(rs.getString("patientId"));
                rzhp.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
                int parameterId = rs.getInt("parameterId");
                switch(parameterId) {
                    case 2:  if (i!=0 && i!=13) return null;i=1;rzhp.setIcg(rs.getInt("value"));break;
                    case 3: i++; rzhp.setIcgk_f(rs.getInt("value"));break;
                    case 6: i++; rzhp.setVorotInvasia(rs.getInt("value"));break;
                    case 7: i++; rzhp.setPechInvasia(rs.getInt("value"));break;
                    case 8: i++; rzhp.setDifferOpuchol(rs.getInt("value"));break;
                    case 9: i++; rzhp.setPerinevInvasia((rs.getInt("value")==0) ? false : true);break;
                    case 10: i++; rzhp.setPolozhKultura((rs.getInt("value")==0) ? false : true);break;
                    case 11: i++; rzhp.setTrofStatus(rs.getInt("value")); break;
                    case 37: i++; rzhp.setStadPervOpuch(rs.getInt("value")); break;
                    case 27: i++; rzhp.setIndexZachvata(rs.getString("value")); break;
                    case 28: i++; rzhp.setObjemOstatka(rs.getString("value")); break;
                    case 52: i++; rzhp.setDiagnRaka(rs.getInt("value")); break;
                    case 26: i++; rzhp.setRezekciaType(rs.getInt("value")); break;
                }
                if (i==13) {
                    rzhpList.add(rzhp);
                    rzhp = new RZHP();
                }
            }while (rs.next());
            return rzhpList;
        }
    }
}
