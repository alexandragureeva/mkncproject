package com.amgureeva.binding.RZHPBinding;

import com.amgureeva.Entities.Diagnosis.RZHP;

import java.util.List;

/**
 * Created by Александра on 04.05.2016.
 */
public interface RZHPDAO {
    RZHP findById(Integer id, boolean isFirstDiagnosis);

    List<RZHP> findAll();

    void save(RZHP patient);

    void update(RZHP patient);

    void delete(Integer id);
}
