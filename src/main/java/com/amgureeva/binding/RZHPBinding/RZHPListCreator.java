package com.amgureeva.binding.RZHPBinding;

import com.amgureeva.Entities.Diagnosis.RZHP;
import com.amgureeva.Entities.Diagnosis.RZHPList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 05.05.2016.
 */
public class RZHPListCreator {

    public List<RZHPList> createRZHPList (List<RZHP> rzhps, PatientService patientService) {
        if (rzhps != null && patientService !=null ) {
            List<RZHPList> rzhpList = new ArrayList<RZHPList>();
            RZHPList rzhpList_ = new RZHPList();
            for (int i = 0; i < rzhps.size(); i++) {
                rzhpList_.setName(patientService.findPatientById(Integer.valueOf(rzhps.get(i).getId())).getName());
                switch (Integer.valueOf(rzhps.get(i).getStadPervOpuch())) {
                    case 0:
                        rzhpList_.setStadPervOpuch("--");
                        break;
                    case 1:
                        rzhpList_.setStadPervOpuch("T1N0M0");
                        break;
                    case 2:
                        rzhpList_.setStadPervOpuch("T2N0M0");
                        break;
                    case 3:
                        rzhpList_.setStadPervOpuch("T3N0M0");
                        break;
                    case 4:
                        rzhpList_.setStadPervOpuch("T4аN0M0");
                        break;
                    case 5:
                        rzhpList_.setStadPervOpuch("T4bN0M0");
                        break;
                    case 6:
                        rzhpList_.setStadPervOpuch("T1N1M0");
                        break;
                    case 7:
                        rzhpList_.setStadPervOpuch("T2N1M0");
                        break;
                    case 8:
                        rzhpList_.setStadPervOpuch("T1N2aM0");
                        break;
                    case 9:
                        rzhpList_.setStadPervOpuch("T3N1M0");
                        break;
                    case 10:
                        rzhpList_.setStadPervOpuch("T4aN1M0");
                        break;
                    case 11:
                        rzhpList_.setStadPervOpuch("T2N2aM0");
                        break;
                    case 12:
                        rzhpList_.setStadPervOpuch("T3N2aM0");
                        break;
                    case 13:
                        rzhpList_.setStadPervOpuch("T1N2bM0");
                        break;
                    case 14:
                        rzhpList_.setStadPervOpuch("T2N2bM0");
                        break;
                    case 15:
                        rzhpList_.setStadPervOpuch("T4aN2aM0");
                        break;
                    case 16:
                        rzhpList_.setStadPervOpuch("T3N2bM0");
                        break;
                    case 17:
                        rzhpList_.setStadPervOpuch("T4aN2bM0");
                        break;
                    case 18:
                        rzhpList_.setStadPervOpuch("T4bN1M0");
                        break;
                    case 19:
                        rzhpList_.setStadPervOpuch("T4bN2M0");
                        break;
                    case 20:
                        rzhpList_.setStadPervOpuch("Любая T, любая N, M1a");
                        break;
                    case 21:
                        rzhpList_.setStadPervOpuch("Любая T, любая N, M1b");
                        break;
                }
                switch (Integer.valueOf(rzhps.get(i).getDifferOpuchol())) {
                    case 0:
                        rzhpList_.setDifferOpuchol("--");
                        break;
                    case 1:
                        rzhpList_.setDifferOpuchol("Низкая");
                        break;
                    case 2:
                        rzhpList_.setDifferOpuchol("Умеренная");
                        break;
                    case 3:
                        rzhpList_.setDifferOpuchol("Высокая");
                        break;
                }
                switch (Integer.valueOf(rzhps.get(i).getDiagnRaka())) {
                    case 0:
                        rzhpList_.setDiagnRaka("--");
                        break;
                    case 1:
                        rzhpList_.setDiagnRaka("Манифестация желтухой");
                        break;
                    case 2:
                        rzhpList_.setDiagnRaka("Случ.находка по д-м луч. методов об-я");
                        break;
                    case 3:
                        rzhpList_.setDiagnRaka("Выяв. при гистолог. исс. уд. желч. пузыря");
                        break;
                }
                switch (Integer.valueOf(rzhps.get(i).getIcg())) {
                    case 0:
                        rzhpList_.setIcg("--");
                        break;
                    case 1:
                        rzhpList_.setIcg("<15%");
                        break;
                    case 2:
                        rzhpList_.setIcg("15-25%");
                        break;
                    case 3:
                        rzhpList_.setIcg(">25%");
                        break;
                }
                switch (Integer.valueOf(rzhps.get(i).getIcgk_f())) {
                    case 0:
                        rzhpList_.setIcgk_f("--");
                        break;
                    case 1:
                        rzhpList_.setIcgk_f("<0.4");
                        break;
                    case 2:
                        rzhpList_.setIcgk_f("0.4-0.6");
                        break;
                    case 3:
                        rzhpList_.setIcgk_f(">0.6");
                        break;
                }
                switch (Integer.valueOf(rzhps.get(i).getPechInvasia())) {
                    case 0:
                        rzhpList_.setPechInvasia("--");
                        break;
                    case 1:
                        rzhpList_.setPechInvasia("Ипсилатеральную");
                        break;
                    case 2:
                        rzhpList_.setPechInvasia("Контрлатеральную");
                        break;
                    case 3:
                        rzhpList_.setPechInvasia("В собственную печеночную");
                        break;
                }
                switch (Integer.valueOf(rzhps.get(i).getVorotInvasia())) {
                    case 0:
                        rzhpList_.setVorotInvasia("--");
                        break;
                    case 1:
                        rzhpList_.setVorotInvasia("В ствол");
                        break;
                    case 2:
                        rzhpList_.setVorotInvasia("В долевые ветви");
                        break;
                    case 3:
                        rzhpList_.setVorotInvasia("В сегментарные ветви");
                        break;
                }
                switch (Integer.valueOf(rzhps.get(i).getTrofStatus())) {
                    case 0:
                        rzhpList_.setTrofStatus("--");
                        break;
                    case 1:
                        rzhpList_.setTrofStatus("Хороший");
                        break;
                    case 2:
                        rzhpList_.setTrofStatus("Удовлетворительный");
                        break;
                    case 3:
                        rzhpList_.setTrofStatus("Неудовлетворительный");
                        break;
                }
                if (rzhps.get(i).getIndexZachvata() == null)
                    rzhpList_.setIndexZachvata("--");
                else
                    rzhpList_.setIndexZachvata(rzhps.get(i).getIndexZachvata());
                if (rzhps.get(i).getObjemOstatka() == null)
                    rzhpList_.setObjemOstatka("--");
                else
                    rzhpList_.setObjemOstatka(rzhps.get(i).getObjemOstatka());
                if (rzhps.get(i).isPerinevInvasia() == false)
                    rzhpList_.setPerinevInvasia("-");
                else
                    rzhpList_.setPerinevInvasia("+");
                if (rzhps.get(i).isPolozhKultura() == false)
                    rzhpList_.setPolozhKultura("-");
                else
                    rzhpList_.setPolozhKultura("+");
                rzhpList.add(rzhpList_);
                rzhpList_ = new RZHPList();
            }

            return rzhpList;
        }
        return null;
    }
    public static String getAllVariants(String s) {
        s = s.replace("_ПОСТ", "");
        s = s.replace("_ВЫПИС", "");
		s = s.replace("_ОПЕР", "");
        if (s.equals("rzh:ICG")) {
            return "1 - \"<15%\"; 2 - \"15-25%\"; 3 - \">25%\"";
        }
        if (s.equals("rzh:ICGK-F")) {
            return "1 - \"<0.4\"; 2 - \"0.4-0.6\"; 3 -\">0.6\"";
        }
        if (s.equals("rzh:INVASIA_VOROTNAYA")) {
            return "1 - \"В ствол\"; 2 - \"В долевые ветви\"; 3 -\"В сегментарные ветви\"";
        }
        if (s.equals("rzh:INVASIYA_PECHENOCHNAYA")) {
            return "1 - \"Ипсилатеральную\"; 2 - \"Контрлатеральную\"; 3 -\"В собственную печеночную\"";
        }
        if (s.equals("rzh:OCENKA_TROF_STATUSA")) {
            return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 -\"Неудовлетворительный\"";
        }
        if (s.equals("rzh:STEPEN_DIFF_OPUCHOLI")) {
            return "1 - \"Низкая\"; 2 - \"Умеренная\"; 3 -\"Высокая\"";
        }
        if (s.equals("rzh:Bismuth")) {
            return "1 - \"I\"; 2 - \"II\"; 3 -\"IIIa\"; " +
                    "4 -\"IIIb\"; 5 -\"IV\"";
        }
        if (s.equals("rzh:CHARACTER_DIAGNOSTIKI_RAKA")) {
            return "1 - \"Манифестация желтухой\"; 2 - \"Случ.находка по д-м луч. методов об-я\";" +
                    " 3 -\"Выяв. при гистолог. исс. уд. желч. пузыря\"";
        }
        if (s.equals("rzh:TNM")) {
            return "1 - \"T1N0M0\"; 2 - \"T2N0M0\"; 3 -\"T3N0M0\"; 4 - \"T4аN0M0\"; 5 - \"T4bN0M0\"; 6 - \"T1N1M0\";" +
                    " 7 - \"T2N1M0\"; 8 - \"T1N2aM0\";  9 - \"T3N1M0\";  10 - \"T4aN1M0\";  11 - \"T2N2aM0\"; " +
                    " 12 - \"T3N2aM0\";  13 - \"T1N2bM0\";  14 - \"T2N2bM0\"; " +
                    " 15 - \"T4aN2aM0\";  16 - \"T3N2bM0\";  17 - \"T4aN2bM0\";  18 - \"T4bN1M0\";  19 - \"T4bN2M0\"; " +
                    " 20 - \"Любая T, любая N, M1a\";  21 - \"Любая T, любая N, M1b\";  ";
        }

        if (s.equals("rzh:REZEKCIA")) {
            return "1 - \"Левосторонняя гемигепатэктомия (ЛГГ)\"; 2 - \"Правосторонняя гемигепатэктомия (ПГГ)\";" +
                    " 3 - \"ПГГ + 1-й сегмент\"; 4 - \"ЛГГ + 1-й сегмент\"; 5 - \"Холицистэктомия\"" +
					"; 6 - \"Резекция 4,5 сегментов\"; 7 - \"Лимфодиссекция гепатодуоденальной связки\";" +
					" 8 - \"Лимфодиссекция гепатодуоденальной связки с резекцией желчных протоков\"";
        }
        if (s.equals("rzh:SOSUD_REZEKCIA")) {
            return "1 - \"Резекция воротной вены\"; 2 - \"Резекция печеночной артерии\"";
        }
        if (s.equals("rzh:VOROT_REZEKCIA")) {
            return "1 - \"Краевая\"; 2 - \"Циркулярная без протезирования\"; 3 - \"Циркулярная с протезированием\"";
        }
        if (s.equals("rzh:KROVOPOTERYA")) {
            return "1 - \"<500\"; 2 - \"500-1000\"; 3 - \">1000\"";
        }
        if (s.equals("rzh:GEATYPE1")) {
            return "1 - \"Единый\"; 2 - \"Раздельный\"";
        }
        if (s.equals("rzh:GEATYPE2")) {
            return "1 - \"Моно-ГЕА\"; 2 - \"Би-ГЕА\"; 3 - \"Три-ГЕА\"; 4 - \"Тетра-ГЕА\";" +
                    " 5 - \"Пента-ГЕА\"; 6 - \"Мульти-ГЕА\"";
        }
        if (s.equals("rzh:GEATYPE3")) {
            return "1 - \"С ТПД\"; 2 - \"Без ТПД\"";
        }
        if (s.equals("rzh:RADIKALNOST")) {
            return "1 - \"R0\"; 2 - \"R1\"; 3 - \"R2\"";
        }
        if (s.equals("rzh:LIMFODISSEKCIA")) {
            return "1 - \"ГДС\"; 2 - \"ГДС+ОПА\"; 3 - \"ГДС+ОПА+ЧС\"";
        }
        if (s.equals("rzh:TNM")) {
            return "1 - \"T1N0M0\"; 2 - \"T2aN0M0\"; 3 - \"T2bN0M0\"; 4 - \"T3N0M0\"; 5 - \"T1N1M0\"; 6 - \"T2N1M0\";" +
                    "7 - \"T3N1M0\"; 8 - \"T4N0M0\"; 9 - \"T4N1M0\"; 10 - \"Любая T, N2M0\"; 11 - \"Любая T, любая N, M1\"";
        }
		if (s.equals("rzh:OSLOZH")) {
			return "1 - \"Внутрибрюшное кровотечение\"; 2 - \"ЖКК\"; 3 - \"Печеночная недостаточность A\";" +
					" 4 - \"Печеночная недостаточность B\"; 5 - \"Печеночная недостаточность C\";" +
					" 6 - \"Механическая желтуха\"; 7 - \"Нагноение раны\"; 8 - \"Холангит\";" +
					" 9 - \"Холангиогенное абсцедирование\"; 10 - \"Асцит\"; 11 - \"Эвентрация\"" +
					"; 12 - \"Кишечная непроходимость\"; 13 - \"Панкреатический свищ\"; 14 - \"Острый панкреатит\";" +
					" 15 - \"Сепсис\"; 16 - \"Тяжелый сепсис\"; 17 - \"Септический шок\"; 18 - \"Желчный свищ\"" +
					"; 19 - \"Кишечный свищ\"; 20 - \"Тромбоз воротной вены\"; 21 - \"Тромбоз печеночной артерии\";" +
					" 22 - \"Односторонняя пневмония\"" +
					"; 23 - \"Двусторонняя пневмония\"; 24 - \"Гидроторакс\"; 25 - \"Почечная недостаточность\"" +
					"; 26 - \"Сердечно-сосудистая недостаточность\"; 27 - \"Полиорганная недостаточность\"";
		}
        if (s.equals("rzh:CLAVIEN")) {
            return "1 - \"I\"; 2 - \"II\"; 3 - \"IIIa\"; 4 - \"IIIb\"; 5 - \"IVa\"; 6 - \"IVb\"; 7 - \"V\"";
        }
		if (s.equals("rzh:FORMA_RAKA")) {
			return "1 - \"Диффузно-инфильтративная\"; 2 - \"Рак в форме полипа\"";
		}
		if (s.equals("rzh:LOKALIZACIA_RAKA")) {
			return "1 - \"Рак тела и дна желчного пузыря\"; 2 - \"Рак шейки желчного пузыря\"";
		}
        if (s.equals("rzh:SCINTIGRAF_OBJ_OPERAC")) {
            return "1 - \"Сегментарная\"; 2 - \"Обширная\"";
        }
        return "";
    }
}
