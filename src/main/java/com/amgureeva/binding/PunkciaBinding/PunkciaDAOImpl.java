package com.amgureeva.binding.PunkciaBinding;

import com.amgureeva.Entities.Surgeries.Punkcia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 24.09.2016.
 */
@Repository
public class PunkciaDAOImpl implements PunkciaDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public Punkcia findById(Integer id, Integer drainId) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=10";

			List<List<Punkcia>> result = null;
			try {
				result = namedParameterJdbcTemplate
						.query(sql, params, new PunkciaDAOImpl.PunkciaMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			result.remove(null);
			if (result.size() == 0)
				return null;
			for (Punkcia punkcia : result.get(0)) {
				if (Integer.valueOf(punkcia.getDrenId()) == drainId) {
					return punkcia;
				}
			}
			return null;
		}
		return null;
	}

	@Override
	public List<Punkcia> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 10";
		List<List<Punkcia>> result = namedParameterJdbcTemplate.query(sql,new PunkciaDAOImpl.PunkciaMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(Punkcia punkcia) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId, drainId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId, :drainId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(punkcia.getId(),62, String.valueOf(punkcia.getDate()), punkcia.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(punkcia.getId(),102, String.valueOf(punkcia.getOrgan()), punkcia.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(punkcia.getId(),101, String.valueOf(punkcia.getType()), punkcia.getDrenId()));
	}

	@Override
	public void update(Punkcia punkcia) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=10 and parameterId=:parameterId AND drainId=:drainId";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(punkcia.getId(),62, String.valueOf(punkcia.getDate()), punkcia.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(punkcia.getId(),102, String.valueOf(punkcia.getOrgan()), punkcia.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(punkcia.getId(),101, String.valueOf(punkcia.getType()), punkcia.getDrenId()));
	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=10";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}


	private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value, String drainId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", patientId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 10);
		paramSource.addValue("value", value);
		paramSource.addValue("drainId", drainId);
		return paramSource;
	}

	@Override
	public List<Punkcia> findPatientAll(String id) {
		String sql = "SELECT surgeryId, parameterId, drainId, value FROM surgeryparametersvalue WHERE diseaseId = 10 and surgeryId =:surgeryId";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("surgeryId", id);
		List<List<Punkcia>> result = namedParameterJdbcTemplate.query(sql,params,new PunkciaDAOImpl.PunkciaMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	private static final class PunkciaMapper implements RowMapper<List<Punkcia>> {

		public List<Punkcia> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<Punkcia> list = new ArrayList<Punkcia>();
			Punkcia punkcia = new Punkcia();
			int i = 0;
			do {
				punkcia.setId(rs.getString("surgeryId"));
				punkcia.setDrenId(rs.getString("drainId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 62: if (i!=0 && i!=3) return null;i=1; punkcia.setDate(rs.getString("value")); break;
					case 101: i++; punkcia.setType(Integer.valueOf(rs.getString("value")));break;
					case 102: i++; punkcia.setOrgan(Integer.valueOf(rs.getString("value")));break;
				}
				if (i == 3) {
					list.add(punkcia);
					punkcia = new Punkcia();
				}
			}while (rs.next());
			return list;
		}
	}
}
