package com.amgureeva.binding.PunkciaBinding;

import com.amgureeva.Entities.Surgeries.Punkcia;

import java.util.List;

/**
 * Created by Nikita on 24.09.2016.
 */
public interface PunkciaDAO {



	Punkcia findById(Integer id, Integer drainId);

	List<Punkcia> findAll();

	void save(Punkcia punkcia);

	void update(Punkcia punkcia);

	void delete(Integer id);

	List<Punkcia> findPatientAll(String id);
}
