package com.amgureeva.binding.MNKRBinding;

import com.amgureeva.Entities.Diagnosis.MNKR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 28.04.2016.
 */
@Repository
public class MNKRDAOImpl implements MNKRDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    @Override
    public MNKR findById(Integer id, boolean isFirstDiagnosis) {
        if (id != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
			params.put("admissiondiag", (isFirstDiagnosis)?1:0);

			String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=4" +
					" AND admissiondiag=:admissiondiag";
            List<List<MNKR>> result = null;
            try {
                result = namedParameterJdbcTemplate
                        .query(sql, params, new GCRMapper());
            } catch (EmptyResultDataAccessException e) {
                // do nothing, return null
            }
			result.remove(null);
			if (result == null || result.size() == 0)
				return null;
			if (isFirstDiagnosis) {
				if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
			if (!isFirstDiagnosis) {
				if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
        }
        return null;
    }

    @Override
    public List<MNKR> findAll() {
        String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 4";
        List<List<MNKR>> result = namedParameterJdbcTemplate.query(sql, new GCRMapper());
        return (result == null || result.size() == 0)?null :result.get(0);
    }

    @Override
    public void save(MNKR MNKR) {
        String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
                + "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),35, String.valueOf(MNKR.getNalVnepMet()), MNKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),36, String.valueOf(MNKR.getPervOpuchType()), MNKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),16, String.valueOf(MNKR.getMetNeiroOpuch()), MNKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),13, String.valueOf(MNKR.getMetGiso()), MNKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),2, String.valueOf(MNKR.getIcg()), MNKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),3, String.valueOf(MNKR.getIcgk_f()), MNKR.isDiagnosisAtStart())); //icgk-f
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),18, String.valueOf(MNKR.getRazmerOpucholi()), MNKR.isDiagnosisAtStart())); //razmer opucholi
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),20, String.valueOf(MNKR.getChisloOpuchUzlov()), MNKR.isDiagnosisAtStart())); //chislo opucholevich uzlov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),19, String.valueOf(MNKR.getOpuchUzliType()), MNKR.isDiagnosisAtStart())); //tip opucholev uzlov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),22, String.valueOf(MNKR.getSosudInvasia()), MNKR.isDiagnosisAtStart())); //tip sosudistoi invasii
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),24, String.valueOf(MNKR.getVnepOchagiType()), MNKR.isDiagnosisAtStart())); //tip vnepech oxhagov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),25, String.valueOf((MNKR.getTrombPechType())), MNKR.isDiagnosisAtStart())); //tromboz pechenochnich ven
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),11, String.valueOf(MNKR.getTrofStatus()), MNKR.isDiagnosisAtStart())); //ocenka troficheskogo statusa
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),40, String.valueOf((MNKR.isNalNeoadChimioTer())?1:0), MNKR.isDiagnosisAtStart())); //nalichiee neoad terapii
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),30, String.valueOf(MNKR.getTrombozVorotVeni()), MNKR.isDiagnosisAtStart())); //tromboz vorotnoi veni
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),27, String.valueOf(MNKR.getIndexZachvata()), MNKR.isDiagnosisAtStart())); //index zachvata
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),28, String.valueOf(MNKR.getObjemOstatka()), MNKR.isDiagnosisAtStart())); //objem funkc doli pecheni
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),41, String.valueOf(MNKR.getChimiotKurs()), MNKR.isDiagnosisAtStart())); //cirroz
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),43, String.valueOf(MNKR.getChimiotEffect()), MNKR.isDiagnosisAtStart())); //trombozitopenia
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),44, String.valueOf(MNKR.getPojavMetType()), MNKR.isDiagnosisAtStart())); //etiologia
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),45, String.valueOf(MNKR.getPojavMetTime()), MNKR.isDiagnosisAtStart()));//
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),26, String.valueOf(MNKR.getRezekciaType()), MNKR.isDiagnosisAtStart())); //objem

	}

    @Override
    public void update(MNKR MNKR) {
        String sql = "UPDATE patientparametersvalue SET value=:value"
                + " WHERE patientId=:patientId and diseaseId=4 and parameterId=:parameterId " +
                "and admissiondiag=:admissiondiag";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),35, String.valueOf(MNKR.getNalVnepMet()), MNKR.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),36, String.valueOf(MNKR.getPervOpuchType()), MNKR.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),16, String.valueOf(MNKR.getMetNeiroOpuch()), MNKR.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),13, String.valueOf(MNKR.getMetGiso()), MNKR.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),2, String.valueOf(MNKR.getIcg()), MNKR.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),3, String.valueOf(MNKR.getIcgk_f()), MNKR.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),18, String.valueOf(MNKR.getRazmerOpucholi()), MNKR.isDiagnosisAtStart())); //razmer opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),20, String.valueOf(MNKR.getChisloOpuchUzlov()), MNKR.isDiagnosisAtStart())); //chislo opucholevich uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),19, String.valueOf(MNKR.getOpuchUzliType()), MNKR.isDiagnosisAtStart())); //tip opucholev uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),22, String.valueOf(MNKR.getSosudInvasia()), MNKR.isDiagnosisAtStart())); //tip sosudistoi invasii
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),24, String.valueOf(MNKR.getVnepOchagiType()), MNKR.isDiagnosisAtStart())); //tip vnepech oxhagov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),25, String.valueOf((MNKR.getTrombPechType())), MNKR.isDiagnosisAtStart())); //tromboz pechenochnich ven
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),11, String.valueOf(MNKR.getTrofStatus()), MNKR.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),40, String.valueOf((MNKR.isNalNeoadChimioTer())?1:0), MNKR.isDiagnosisAtStart())); //nalichiee neoad terapii
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),30, String.valueOf(MNKR.getTrombozVorotVeni()), MNKR.isDiagnosisAtStart())); //tromboz vorotnoi veni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),27, String.valueOf(MNKR.getIndexZachvata()), MNKR.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),28, String.valueOf(MNKR.getObjemOstatka()), MNKR.isDiagnosisAtStart())); //objem funkc doli pecheni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),41, String.valueOf(MNKR.getChimiotKurs()), MNKR.isDiagnosisAtStart())); //cirroz
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),43, String.valueOf(MNKR.getChimiotEffect()), MNKR.isDiagnosisAtStart())); //trombozitopenia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),44, String.valueOf(MNKR.getPojavMetType()), MNKR.isDiagnosisAtStart())); //etiologia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (MNKR.getId(),45, String.valueOf(MNKR.getPojavMetTime()), MNKR.isDiagnosisAtStart()));//
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MNKR.getId(),26, String.valueOf(MNKR.getRezekciaType()), MNKR.isDiagnosisAtStart())); //objem
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=4";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }

    private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value, boolean admissiondiag) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("patientId", patientId);
        paramSource.addValue("parameterId", fieldId);
        paramSource.addValue("diseaseId", 4);
        paramSource.addValue("value", value);
        paramSource.addValue("admissiondiag", admissiondiag);
        return paramSource;
    }


    private static final class GCRMapper implements RowMapper<List<MNKR>> {

        public List<MNKR> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<MNKR> mnkrList = new ArrayList<MNKR>();
            MNKR MNKR = new MNKR();
            int i = 0;
            do {
                MNKR.setId(rs.getString("patientId"));
                MNKR.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
                int parameterId = rs.getInt("parameterId");
                switch(parameterId) {
                    case 2:  if (i!=0 && i!=22) return null;i=1;MNKR.setIcg(rs.getInt("value"));break;
                    case 3: i++; MNKR.setIcgk_f(rs.getInt("value"));break;
                    case 18: i++; MNKR.setRazmerOpucholi(rs.getInt("value"));break;
                    case 20: i++; MNKR.setChisloOpuchUzlov(rs.getInt("value"));break;
                    case 19: i++; MNKR.setOpuchUzliType(rs.getInt("value"));break;
                    case 22: i++; MNKR.setSosudInvasia(rs.getInt("value"));break;
                    case 24: i++; MNKR.setVnepOchagiType(rs.getInt("value"));break;
                    case 25: i++; MNKR.setTrombPechType(rs.getInt("value"));break;
                    case 11: i++; MNKR.setTrofStatus(rs.getInt("value")); break;
                    case 30: i++; MNKR.setTrombozVorotVeni(rs.getInt("value")); break;
                    case 27: i++; MNKR.setIndexZachvata(rs.getString("value")); break;
                    case 28: i++; MNKR.setObjemOstatka(rs.getString("value")); break;
                    case 35: i++; MNKR.setNalVnepMet(rs.getInt("value")); break;
                    case 40: i++; MNKR.setNalNeoadChimioTer((rs.getInt("value")==1)?true:false); break;
                    case 41: i++; MNKR.setChimiotKurs(rs.getInt("value")); break;
                    case 43: i++; MNKR.setChimiotEffect(rs.getInt("value")); break;
                    case 44: i++; MNKR.setPojavMetType(rs.getInt("value")); break;
                    case 45: i++; MNKR.setPojavMetTime(rs.getInt("value")); break;
                    case 36: i++; MNKR.setPervOpuchType(rs.getInt("value")); break;
                    case 16: i++; MNKR.setMetNeiroOpuch(rs.getInt("value")); break;
                    case 13: i++; MNKR.setMetGiso(rs.getInt("value")); break;
					case 26: i++; MNKR.setRezekciaType(rs.getInt("value")); break;
                }
                if (i==22) {
                    mnkrList.add(MNKR);
                    MNKR = new MNKR();
                }
            }while (rs.next());
            return mnkrList;
        }
    }
}
