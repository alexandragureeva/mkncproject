package com.amgureeva.binding.MNKRBinding;

import com.amgureeva.Entities.Diagnosis.MNKR;
import com.amgureeva.Entities.Diagnosis.MNKRList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 05.05.2016.
 */

public class MNKRListCreator {

    public List<MNKRList> createMNKRList (List<MNKR> mnkrs, PatientService patientService) {
        if (mnkrs != null && patientService != null) {
            List<MNKRList> mnkrList = new ArrayList<MNKRList>();
            MNKRList mnkrList_ = new MNKRList();
            for (int i = 0; i < mnkrs.size(); i++) {
                mnkrList_.setName(patientService.findPatientById(Integer.valueOf(mnkrs.get(i).getId())).getName());
                switch (Integer.valueOf(mnkrs.get(i).getPervOpuchType())) {
                    case 0:
                        mnkrList_.setPervOpuchType("--");
                        break;
                    case 1:
                        mnkrList_.setPervOpuchType("Метастазы нейроэндокринных опухолей");
                        break;
                    case 2:
                        mnkrList_.setPervOpuchType("Метастазы рака почки");
                        break;
                    case 3:
                        mnkrList_.setPervOpuchType("Метастазы рака поджелудочной железы");
                        break;
                    case 4:
                        mnkrList_.setPervOpuchType("Метастазы рака БДС");
                        break;
                    case 5:
                        mnkrList_.setPervOpuchType("Метастазы рака желудка");
                        break;
                    case 6:
                        mnkrList_.setPervOpuchType("Метастазы ГИСО");
                        break;
                    case 7:
                        mnkrList_.setPervOpuchType("Метастазы забрюшиных сарком");
                        break;
                    case 8:
                        mnkrList_.setPervOpuchType("Метастазы меланомы");
                        break;
                    case 9:
                        mnkrList_.setPervOpuchType("Метастазы БПО");
                        break;
                }

                switch (Integer.valueOf(mnkrs.get(i).getMetGiso())) {
                    case 0:
                        mnkrList_.setMetGiso("--");
                        break;
                    case 1:
                        mnkrList_.setMetGiso("Желудка");
                        break;
                    case 2:
                        mnkrList_.setMetGiso("Тонкой кишки");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getMetNeiroOpuch())) {
                    case 0:
                        mnkrList_.setMetNeiroOpuch("--");
                        break;
                    case 1:
                        mnkrList_.setMetNeiroOpuch("Поджелудочной железы");
                        break;
                    case 2:
                        mnkrList_.setMetNeiroOpuch("Тонкой кишки");
                        break;
                    case 3:
                        mnkrList_.setMetNeiroOpuch("Аппендикса");
                        break;
                    case 4:
                        mnkrList_.setMetNeiroOpuch("Легких");
                        break;
                    case 5:
                        mnkrList_.setMetNeiroOpuch("Других локализаций");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getNalVnepMet())) {
                    case 0:
                        mnkrList_.setNalVnepMet("--");
                        break;
                    case 1:
                        mnkrList_.setNalVnepMet("нет");
                        break;
                    case 2:
                        mnkrList_.setNalVnepMet("В легких");
                        break;
                    case 3:
                        mnkrList_.setNalVnepMet("В лимфатических уздах бр.полости");
                        break;
                    case 4:
                        mnkrList_.setNalVnepMet("По брюшине");
                        break;
                    case 5:
                        mnkrList_.setNalVnepMet("В др.органах бр.полости");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getRazmerOpucholi())) {
                    case 0:
                        mnkrList_.setRazmerOpucholi("--");
                        break;
                    case 1:
                        mnkrList_.setRazmerOpucholi("<10см");
                        break;
                    case 2:
                        mnkrList_.setRazmerOpucholi(">10см");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getOpuchUzliType())) {
                    case 0:
                        mnkrList_.setOpuchUzliType("--");
                        break;
                    case 1:
                        mnkrList_.setOpuchUzliType("Солитарный");
                        break;
                    case 2:
                        mnkrList_.setOpuchUzliType("Множественные");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getChisloOpuchUzlov())) {
                    case 0:
                        mnkrList_.setChisloOpuchUzlov("--");
                        break;
                    case 1:
                        mnkrList_.setChisloOpuchUzlov("0");
                        break;
                    case 2:
                        mnkrList_.setChisloOpuchUzlov("1");
                        break;
                    case 3:
                        mnkrList_.setChisloOpuchUzlov("2");
                        break;
                    case 4:
                        mnkrList_.setChisloOpuchUzlov("3");
                        break;
                    case 5:
                        mnkrList_.setChisloOpuchUzlov("4");
                        break;
                    case 6:
                        mnkrList_.setChisloOpuchUzlov("5");
                        break;
                    case 7:
                        mnkrList_.setChisloOpuchUzlov(">5");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getSosudInvasia())) {
                    case 0:
                        mnkrList_.setSosudInvasia("--");
                        break;
                    case 1:
                        mnkrList_.setSosudInvasia("нет");
                        break;

                    case 2:
                        mnkrList_.setSosudInvasia("Микрососудистая");
                        break;
                    case 3:
                        mnkrList_.setSosudInvasia("Мaкрососудистая");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getPojavMetType())) {
                    case 0:
                        mnkrList_.setPojavMetType("--");
                        break;
                    case 1:
                        mnkrList_.setPojavMetType("Синхронные");
                        break;
                    case 2:
                        mnkrList_.setPojavMetType("Метахронные");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getPojavMetTime())) {
                    case 0:
                        mnkrList_.setPojavMetTime("--");
                        break;
                    case 1:
                        mnkrList_.setPojavMetTime("< 1 года");
                        break;
                    case 2:
                        mnkrList_.setPojavMetTime("> 1 года");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getIcg())) {
                    case 0:
                        mnkrList_.setIcg("--");
                        break;
                    case 1:
                        mnkrList_.setIcg("<15%");
                        break;
                    case 2:
                        mnkrList_.setIcg("15-25%");
                        break;
                    case 3:
                        mnkrList_.setIcg(">25%");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getIcgk_f())) {
                    case 0:
                        mnkrList_.setIcgk_f("--");
                        break;
                    case 1:
                        mnkrList_.setIcgk_f("<0.4");
                        break;
                    case 2:
                        mnkrList_.setIcgk_f("0.4-0.6");
                        break;
                    case 3:
                        mnkrList_.setIcgk_f(">0.6");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getVnepOchagiType())) {
                    case 0:
                        mnkrList_.setVnepOchagiType("--");
                        break;
                    case 1:
                        mnkrList_.setVnepOchagiType("нет");
                        break;
                    case 2:
                        mnkrList_.setVnepOchagiType("Отдаленные метастазы");
                        break;
                    case 3:
                        mnkrList_.setVnepOchagiType("Прорастание соседних органов");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getTrombPechType())) {
                    case 0:
                        mnkrList_.setTrombPechType("--");
                        break;
                    case 1:
                        mnkrList_.setTrombPechType("Нет");
                        break;
                    case 2:
                        mnkrList_.setTrombPechType("Да");
                        break;
                    case 3:
                        mnkrList_.setTrombPechType("С выходом в нижнюю полую вену");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getTrombozVorotVeni())) {
                    case 0:
                        mnkrList_.setTrombozVorotVeni("--");
                        break;
                    case 1:
                        mnkrList_.setTrombozVorotVeni("0");
                        break;
                    case 2:
                        mnkrList_.setTrombozVorotVeni("1");
                        break;
                    case 3:
                        mnkrList_.setTrombozVorotVeni("2");
                        break;
                    case 4:
                        mnkrList_.setTrombozVorotVeni("3");
                        break;
                    case 5:
                        mnkrList_.setTrombozVorotVeni("4");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getTrofStatus())) {
                    case 0:
                        mnkrList_.setTrofStatus("--");
                        break;
                    case 1:
                        mnkrList_.setTrofStatus("Хороший");
                        break;
                    case 2:
                        mnkrList_.setTrofStatus("Удовлетворительный");
                        break;
                    case 3:
                        mnkrList_.setTrofStatus("Неудовлетворительный");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getChimiotKurs())) {
                    case 0:
                        mnkrList_.setChimiotKurs("--");
                        break;
                    case 1:
                        mnkrList_.setChimiotKurs("4 курса");
                        break;
                    case 2:
                        mnkrList_.setChimiotKurs("6-8 курсов");
                        break;
                    case 3:
                        mnkrList_.setChimiotKurs(">8 курсов");
                        break;
                }
                switch (Integer.valueOf(mnkrs.get(i).getChimiotEffect())) {
                    case 0:
                        mnkrList_.setChimiotEffect("--");
                        break;
                    case 1:
                        mnkrList_.setChimiotEffect("Полный ответ");
                        break;
                    case 2:
                        mnkrList_.setChimiotEffect("Частичный ответ");
                        break;
                    case 3:
                        mnkrList_.setChimiotEffect("Стабилизация");
                        break;
                    case 4:
                        mnkrList_.setChimiotEffect("Прогрессия");
                        break;
                }
                if (mnkrs.get(i).getIndexZachvata().equals("null"))
                    mnkrList_.setIndexZachvata("--");
                else
                    mnkrList_.setIndexZachvata(mnkrs.get(i).getIndexZachvata());
                if (mnkrs.get(i).getObjemOstatka().equals("null"))
                    mnkrList_.setObjemOstatka("--");
                else
                    mnkrList_.setObjemOstatka(mnkrs.get(i).getObjemOstatka());
                if (mnkrs.get(i).isNalNeoadChimioTer() == false)
                    mnkrList_.setNalNeoadChimioTer("-");
                else
                    mnkrList_.setNalNeoadChimioTer("+");
                mnkrList.add(mnkrList_);
                mnkrList_ = new MNKRList();
            }

            return mnkrList;
        }
        return null;
    }


    public static String getAllVariants(String s) {
        s = s.replace("_ПОСТ", "");
        s = s.replace("_ВЫПИС", "");
        s = s.replace("_ОПЕР", "");
        if (s.equals("mnkr:ICG")) {
            return "1 - \"<15%\"; 2 - \"15-25%\"; 3 - \">25%\"";
        }
        if (s.equals("mnkr:ICGK-F")) {
            return "1 - \"<0.4\"; 2 - \"0.4-0.6\"; 3 -\">0.6\"";
        }
        if (s.equals("mnkr:INVASIA_VOROTNAYA")) {
            return "1 - \"В ствол\"; 2 - \"В долевые ветви\"; 3 -\"В сегментарные ветви\"";
        }
        if (s.equals("mnkr:INVASIYA_PECHENOCHNAYA")) {
            return "1 - \"Ипсилатеральную\"; 2 - \"Контрлатеральную\"; 3 -\"В собственную печеночную\"";
        }
        if (s.equals("mnkr:OCENKA_TROF_STATUSA")) {
            return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 -\"Неудовлетворительный\"";
        }
        if (s.equals("mnkr:MAX_OPUCHOL")) {
            return "1 - \"<10см\"; 2 - \">10см\"";
        }
        if (s.equals("mnkr:ONKOMARKER")) {
            return "1 - \"CA\"; 2 - \"19-9\"; 3 -\"CEA\"";
        }
        if (s.equals("mnkr:TROMBOZ_PECHEN_VEN")) {
            return "1 - \"нет\"; 2 - \"Да\"; 3 -\"С выходом в нижнюю полую вену\"";
        }
        if (s.equals("mnkr:TROMBOZ_VOROTNOI_VENI")) {
            return "1 - \"0\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"; 5 - \"4\"";
        }
        if (s.equals("mnkr:VNEPECHENOCH_OCHAGI")) {
            return "1 - \"нет\"; 2 - \"Отдаленные метастазы\"; 3 -\"Прорастание соседних органов\"";
        }
        if (s.equals("mnkr:OPUCH_UZLI_CHISLO")) {
            return "1 - \"0\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"; 5 - \"4\"; 6 - \"5\"; 7 - \">5\"";
        }
        if (s.equals("mnkr:OPUCH_UZLI_TYPE")) {
            return "1 - \"Солитарный\"; 2 - \"Множественные\"";
        }
        if (s.equals("mnkr:NEOADJUV_TERAPIA_EFFECT")) {
            return "1 - \"Полный ответ\"; 2 - \"Частичный ответ\"; 3 -\"Стабилизация\"; 4 - \"Прогрессия\"";
        }
        if (s.equals("mnkr:NEOADJUV_TERAPIA_KURS")) {
            return "1 - \"4 курса\"; 2 - \"6-8 курсов\"; 3 -\">8 курсов\"";
        }
        if (s.equals("mnkr:POJAVLENIE_METASTAZOV_TYPE")) {
            return "1 - \"Синхронные\"; 2 - \"Метахронные\"";
        }
        if (s.equals("mnkr:POJAVLENIE_METASTAZOV_VREMYA")) {
            return "1 - \"< 1 года\"; 2 - \"> 1 года\"";
        }
        if (s.equals("mnkr:VNEPECH_METASTAZI")) {
            return "1 - \"нет\"; 2 - \"В легких\"; 3 -\"В лимфатических уздах бр.полости\"; 4 - \"По брюшине\";" +
                    " 5 - \"В др.органах бр.полости\"";
        }
        if (s.equals("mnkr:MET_GISO")) {
            return "1 - \"Желудка\"; 2 - \"Тонкой кишки\"";
        }
        if (s.equals("mnkr:MET_NEIRO")) {
            return "1 - \"Поджелудочной железы\"; 2 - \"Тонкой кишки\"; 3 - \"Аппендикса\"; 4 - \"Легких\"";
        }

        if (s.equals("mnkr:VNEPECH_METASTAZI")) {
            return "1 - \"нет\"; 2 - \"В легких\"; 3 -\"В лимфатических уздах бр.полости\"; 4 - \"По брюшине\";" +
                    " 5 - \"В др.органах бр.полости\"";
        }
        if (s.equals("mnkr:PERVICH_OPUCHOL")) {
            return "1 - \"Метастазы нейроэндокринных опухолей\"; 2 - \"Метастазы рака почки\";" +
                    " 3 -\"Метастазы рака поджелудочной железы\"; 4 - \"Метастазы рака БДС\";" +
                    " 5 - \"Метастазы рака желудка\"; 6 - \"Метастазы ГИСО\"; 7 - \"Метастазы забрюшиных сарком\"; " +
                    "8 - \"Метастазы меланомы\"; 9 - \"Метастазы без выявленного первичного очага\";";
        }
        if (s.equals("mnkr:CHIMIOTERAPIA")) {
            return "1 - \"нет\"; 2 - \"Цитостатиких\"; 3 -\"Цитостатики + таргетная терапия\"";
        }

        if (s.equals("mnkr:KRATNOST_REZEKCII")) {
            return "1 - \"Первичная\"; 2 - \"Вторая\"; 3 -\"Третья\"; 4 -\"Четвертая\"";
        }
        if (s.equals("mnkr:OBJEM_REZEKCII")) {
            return "1 - \"Обширная резекция ЛГГЭ\"; 2 - \"Обширная резекция ПГГЭ\";" +
                    " 3 -\"Cегментарная резекция\"";
        }
        if (s.equals("mnkr:SEGMENT_REZEKCIYA")) {
            return "1 - \"Анатомическая\"; 2 - \"Неанатомическая\"";
        }
        if (s.equals("mnkr:VARIANT_REZEKCII")) {
            return "1 - \"Открытая\"; 2 - \"Лапароскопическая\"";
        }
        if (s.equals("mnkr:NALICHIE_RCHA")) {
            return "1 - \"нет\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"";
        }

        if (s.equals("mnkr:SIMULT_OPERACIA")) {
            return "1 - \"Резекция толстой кишки\"; 2 - \"Резекция прямой кишки\";" +
                    " 3 -\"Резекция диафрагмы\"; 4 -\"Резекция других органов(тонкой кишки и др)\" ";
        }
        if (s.equals("mnkr:OSLOZH")) {
            return "1 - \"Внутрибрюшное кровотечение\"; 2 - \"ЖКК\"; 3 - \"Печеночная недостаточность A\";" +
                    " 4 - \"Печеночная недостаточность B\"; 5 - \"Печеночная недостаточность C\";" +
                    " 6 - \"Механическая желтуха\"; 7 - \"Нагноение раны\"; 8 - \"Холангит\";" +
                    " 9 - \"Холангиогенное абсцедирование\"; 10 - \"Асцит\"; 11 - \"Эвентрация\"" +
                    "; 12 - \"Кишечная непроходимость\"; 13 - \"Панкреатический свищ\"; 14 - \"Острый панкреатит\";" +
                    " 15 - \"Сепсис\"; 16 - \"Тяжелый сепсис\"; 17 - \"Септический шок\"; 18 - \"Желчный свищ\"" +
                    "; 19 - \"Кишечный свищ\"; 20 - \"Тромбоз воротной вены\"; 21 - \"Тромбоз печеночной артерии\";" +
                    " 22 - \"Односторонняя пневмония\"" +
                    "; 23 - \"Двусторонняя пневмония\"; 24 - \"Гидроторакс\"; 25 - \"Почечная недостаточность\"" +
                    "; 26 - \"Сердечно-сосудистая недостаточность\"; 27 - \"Полиорганная недостаточность\"";
        }
        if (s.equals("mnkr:CLAVIEN")) {
            return "1 - \"I\"; 2 - \"II\"; 3 - \"IIIa\"; 4 - \"IIIb\"; 5 - \"IVa\"; 6 - \"IVb\"; 7 - \"V\"";
        }
        if (s.equals("mnkr:SCINTIGRAF_OBJ_OPERAC")) {
            return "1 - \"Сегментарная\"; 2 - \"Обширная\"";
        }
        if (s.equals("mnkr:SOSUD_INVASIA")) {
            return "1 - \"Нет\"; 2 - \"Микрососудистая инвазия\"; 3 -\"Макрососудистая инвазия\"";
        }


        return "";
    }
}
