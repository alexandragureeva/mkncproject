package com.amgureeva.binding.MNKRBinding;

import com.amgureeva.Entities.Diagnosis.MNKR;

import java.util.List;

/**
 * Created by Александра on 04.05.2016.
 */
public interface MNKRDAO {
    MNKR findById(Integer id, boolean isFirstDiagnosis);

    List<MNKR> findAll();

    void save(MNKR patient);

    void update(MNKR patient);

    void delete(Integer id);
}
