package com.amgureeva.binding.OpRZHPBinding;

import com.amgureeva.Entities.Surgeries.OpRZHP;
import com.amgureeva.Entities.Surgeries.OpRZHPList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public class OpRZHPListCreator {

	public List<OpRZHPList> createRzhpList(List<OpRZHP> opRZHPs, PatientService patientService) {
		if (opRZHPs != null && patientService != null) {
			List<OpRZHPList> opRZHPLists = new ArrayList<OpRZHPList>();
			OpRZHPList opRZHPList = new OpRZHPList();
			for (int i = 0; i < opRZHPs.size(); i++) {
				opRZHPList.setName(patientService.findPatientById(Integer.valueOf(opRZHPs.get(i).getId())).getName());
				switch (Integer.valueOf(opRZHPs.get(i).getRezekcia())) {
					case 0:
						opRZHPList.setRezekcia("--");
						break;
					case 1:
						opRZHPList.setRezekcia("ЛГГ");
						break;
					case 2:
						opRZHPList.setRezekcia("ПГГ");
						break;
					case 3:
						opRZHPList.setRezekcia("ПГГ + 1-й сегмент");
						break;
					case 4:
						opRZHPList.setRezekcia("ЛГГ + 1-й сегмент");
						break;
					case 5:
						opRZHPList.setRezekcia("Холицистэктомия");
						break;
					case 6:
						opRZHPList.setRezekcia("Резекция 4,5 сегментов");
						break;
					case 7:
						opRZHPList.setRezekcia("Лимфодиссекция гепатодуоденальной связки");
						break;
					case 8:
						opRZHPList.setRezekcia("Лимфодиссекция гепатодуоденальной связки с резекцией желчных протоков");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getRzhpsosudrezekcia())) {
					case 0:
						opRZHPList.setSosudrezekcia("--");
						break;
					case 1:
						opRZHPList.setSosudrezekcia("Резекция воротной вены");
						break;
					case 2:
						opRZHPList.setSosudrezekcia("Резекция печеночной артерии");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getRzhpvorotrezekcia())) {
					case 0:
						opRZHPList.setVorotrezekcia("--");
						break;
					case 1:
						opRZHPList.setVorotrezekcia("Краевая");
						break;
					case 2:
						opRZHPList.setVorotrezekcia("Циркулярная без протезирования");
						break;
					case 3:
						opRZHPList.setVorotrezekcia("Циркулярная с протезированием");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getKrovopoteria())) {
					case 0:
						opRZHPList.setKrovopoteria("--");
						break;
					case 1:
						opRZHPList.setKrovopoteria("<500");
						break;
					case 2:
						opRZHPList.setKrovopoteria("500-1000");
						break;
					case 3:
						opRZHPList.setKrovopoteria(">1000");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getGeatype1())) {
					case 0:
						opRZHPList.setGeatype1("--");
						break;
					case 1:
						opRZHPList.setGeatype1("Единый");
						break;
					case 2:
						opRZHPList.setGeatype1("Раздельный");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getGeatype2())) {
					case 0:
						opRZHPList.setGeatype2("--");
						break;
					case 1:
						opRZHPList.setGeatype2("Моно-ГЕА");
						break;
					case 2:
						opRZHPList.setGeatype2("Би-ГЕА");
						break;
					case 3:
						opRZHPList.setGeatype2("Три-ГЕА");
						break;
					case 4:
						opRZHPList.setGeatype2("Тетра-ГЕА");
						break;
					case 5:
						opRZHPList.setGeatype2("Пента-ГЕА");
						break;
					case 6:
						opRZHPList.setGeatype2("Мульти-ГЕА");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getGeatype3())) {
					case 0:
						opRZHPList.setGeatype3("--");
						break;
					case 1:
						opRZHPList.setGeatype3("С ТПД");
						break;
					case 2:
						opRZHPList.setGeatype3("Без ТПД");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getRadikal())) {
					case 0:
						opRZHPList.setRadikal("--");
						break;
					case 1:
						opRZHPList.setRadikal("R0");
						break;
					case 2:
						opRZHPList.setRadikal("R1");
						break;
					case 3:
						opRZHPList.setRadikal("R2");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getLimfodissekcia())) {
					case 0:
						opRZHPList.setLimfodissekcia("--");
						break;
					case 1:
						opRZHPList.setLimfodissekcia("ГДС");
						break;
					case 2:
						opRZHPList.setLimfodissekcia("ГДС+ОПА");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getTnm())) {
					case 0:
						opRZHPList.setTnm("--");
						break;
					case 1:
						opRZHPList.setTnm("T1N0M0");
						break;
					case 2:
						opRZHPList.setTnm("T2aN0M0");
						break;
					case 3:
						opRZHPList.setTnm("T2bN0M0");
						break;
					case 4:
						opRZHPList.setTnm("T3N0M0");
						break;
					case 5:
						opRZHPList.setTnm("T1N1M0");
						break;
					case 6:
						opRZHPList.setTnm("T2N1M0");
						break;
					case 7:
						opRZHPList.setTnm("T3N1M0");
						break;
					case 8:
						opRZHPList.setTnm("T4N0M0");
						break;
					case 9:
						opRZHPList.setTnm("T4N1M0");
						break;
					case 10:
						opRZHPList.setTnm("Любая T, N2M0");
						break;
					case 11:
						opRZHPList.setTnm("Любая T, любая N, M1");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getOsl())) {
					case 0:
						opRZHPList.setOsl("--");
						break;
					case 1:
						opRZHPList.setOsl("Внутрибрюшное кровотечение");
						break;
					case 2:
						opRZHPList.setOsl("ЖКК");
						break;
					case 3:
						opRZHPList.setOsl("Печеночная недостаточность A");
						break;
					case 4:
						opRZHPList.setOsl("Печеночная недостаточность B");
						break;
					case 5:
						opRZHPList.setOsl("Печеночная недостаточность C");
						break;
					case 6:
						opRZHPList.setOsl("Паренхиматозная желтуха");
						break;
					case 7:
						opRZHPList.setOsl("Механическая желтуха");
						break;
					case 8:
						opRZHPList.setOsl("Нагноение раны");
						break;
					case 9:
						opRZHPList.setOsl("Холангит");
						break;
					case 10:
						opRZHPList.setOsl("Холангиогенное абсцедирование");
						break;
					case 11:
						opRZHPList.setOsl("Асцит");
						break;
					case 12:
						opRZHPList.setOsl("Эвентрация");
						break;
					case 13:
						opRZHPList.setOsl("Кишечная непроходимость");
						break;
					case 14:
						opRZHPList.setOsl("Панкреатический свищ");
						break;
					case 15:
						opRZHPList.setOsl("Острый панкреатит");
						break;
					case 16:
						opRZHPList.setOsl("Сепсис");
						break;
					case 17:
						opRZHPList.setOsl("Тяжелый сепсис");
						break;
					case 18:
						opRZHPList.setOsl("Септический шок");
						break;
					case 19:
						opRZHPList.setOsl("Желчный свищ");
						break;
					case 20:
						opRZHPList.setOsl("Кишечный свищ");
						break;
					case 21:
						opRZHPList.setOsl("Тромбоз воротной вены");
						break;
					case 22:
						opRZHPList.setOsl("Тромбоз печеночной артерии");
						break;
					case 23:
						opRZHPList.setOsl("Односторонняя пневмония");
						break;
					case 24:
						opRZHPList.setOsl("Двусторонняя пневмония");
						break;
					case 25:
						opRZHPList.setOsl("Гидроторакс");
						break;
					case 26:
						opRZHPList.setOsl("Почечная недостаточность");
						break;
					case 27:
						opRZHPList.setOsl("Сердечно-сосудистая недостаточность");
						break;
					case 28:
						opRZHPList.setOsl("Полиорганная недостаточность");
						break;

				}
				switch (Integer.valueOf(opRZHPs.get(i).getClavien())) {
					case 0:
						opRZHPList.setClavien("--");
						break;
					case 1:
						opRZHPList.setClavien("I");
						break;
					case 2:
						opRZHPList.setClavien("II");
						break;
					case 3:
						opRZHPList.setClavien("IIIa");
						break;
					case 4:
						opRZHPList.setClavien("IIIb");
						break;
					case 5:
						opRZHPList.setClavien("IVa");
						break;
					case 6:
						opRZHPList.setClavien("IVb");
						break;
					case 7:
						opRZHPList.setClavien("V");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getFormRak())) {
					case 0:
						opRZHPList.setFormRak("--");
						break;
					case 1:
						opRZHPList.setFormRak("Диффузно-инфильтративная");
						break;
					case 2:
						opRZHPList.setFormRak("Рак в форме полипа");
						break;
				}
				switch (Integer.valueOf(opRZHPs.get(i).getLokalizaciaRaka())) {
					case 0:
						opRZHPList.setLokalizaciaRaka("--");
						break;
					case 1:
						opRZHPList.setLokalizaciaRaka("Рак тела и дна желчного пузыря");
						break;
					case 2:
						opRZHPList.setLokalizaciaRaka("Рак шейки желчного пузыря");
						break;
				}
				if (opRZHPs.get(i).getDlitop().equals("")||opRZHPs.get(i).getDlitop().equals("null"))
					opRZHPList.setDlitop("--");
				else
					opRZHPList.setDlitop(String.valueOf(opRZHPs.get(i).getDlitop()));
				if (opRZHPs.get(i).getGemotransfusia() == 0)
					opRZHPList.setGemotransfusia("-");
				else
					opRZHPList.setGemotransfusia("+");
				if (opRZHPs.get(i).getRzhpgemotransfusia_ml().equals("")||opRZHPs.get(i).getRzhpgemotransfusia_ml().equals("null"))
					opRZHPList.setGemotransfusia_ml("--");
				else
					opRZHPList.setGemotransfusia_ml(String.valueOf(opRZHPs.get(i).getRzhpgemotransfusia_ml()));
				if (opRZHPs.get(i).getPlazma().equals("")||opRZHPs.get(i).getPlazma().equals("null"))
					opRZHPList.setPlazma("--");
				else
					opRZHPList.setPlazma(String.valueOf(opRZHPs.get(i).getPlazma()));
				if (opRZHPs.get(i).getGdc() == 0)
					opRZHPList.setGdc("-");
				else
					opRZHPList.setGdc("+");
				if (opRZHPs.get(i).getRzhpgdcmin().equals("")||opRZHPs.get(i).getRzhpgdcmin().equals("null"))
					opRZHPList.setGdcmin("--");
				else
					opRZHPList.setGdcmin(String.valueOf(opRZHPs.get(i).getRzhpgdcmin()));
				if (opRZHPs.get(i).getBypass() == 0)
					opRZHPList.setBypass("-");
				else
					opRZHPList.setBypass("+");
				if (opRZHPs.get(i).getTotIz() == 0)
					opRZHPList.setTotIz("-");
				else
					opRZHPList.setTotIz("+");
				if (opRZHPs.get(i).getSelIz() == 0)
					opRZHPList.setSelIz("-");
				else
					opRZHPList.setSelIz("+");
				if (opRZHPs.get(i).getKamni() == 0)
					opRZHPList.setKamni("-");
				else
					opRZHPList.setKamni("+");
				if (opRZHPs.get(i).getZhkb() == 0)
					opRZHPList.setZhkb("-");
				else
					opRZHPList.setZhkb("+");
				opRZHPLists.add(opRZHPList);
				opRZHPList = new OpRZHPList();
			}
			return opRZHPLists;
		}
		return null;
	}
}

