package com.amgureeva.binding.OpRZHPBinding;

import com.amgureeva.Entities.Surgeries.OpRZHP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 22.05.2016.
 */
@Repository
public class OpRZHPDAOImpl implements OpRZHPDAO {


	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public OpRZHP findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=5";

			List<OpRZHP> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpRZHPMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}

	@Override
	public List<OpRZHP> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 5";
		List<List<OpRZHP>> result = namedParameterJdbcTemplate.query(sql,
				new OpRZHPMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(OpRZHP opRZHP) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),1, String.valueOf(opRZHP.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),2, String.valueOf(opRZHP.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),3, String.valueOf(opRZHP.getRezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),4, String.valueOf(opRZHP.getRzhpsosudrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),5, String.valueOf(opRZHP.getRzhpvorotrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),6, String.valueOf(opRZHP.getKrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),7, String.valueOf(opRZHP.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),9, String.valueOf(opRZHP.getGemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),10, String.valueOf(opRZHP.getRzhpgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),11, String.valueOf(opRZHP.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),12, String.valueOf(opRZHP.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),13, String.valueOf(opRZHP.getRzhpgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),14, String.valueOf(opRZHP.getGeatype1())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),15, String.valueOf(opRZHP.getGeatype2())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),16, String.valueOf(opRZHP.getGeatype3())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),17, String.valueOf(opRZHP.getRadikal())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),18, String.valueOf(opRZHP.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),19, String.valueOf(opRZHP.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),21, String.valueOf(opRZHP.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),22, String.valueOf(opRZHP.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),23, String.valueOf(opRZHP.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),25, String.valueOf(opRZHP.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),26, String.valueOf(opRZHP.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),59, String.valueOf(opRZHP.getKamni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),60, String.valueOf(opRZHP.getZhkb())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),61, String.valueOf(opRZHP.getFormRak())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),63, String.valueOf(opRZHP.getLokalizaciaRaka())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),103, String.valueOf(opRZHP.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),104, String.valueOf(opRZHP.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),105, String.valueOf(opRZHP.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),106, String.valueOf(opRZHP.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),107, String.valueOf(opRZHP.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),108, String.valueOf(opRZHP.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),109, String.valueOf(opRZHP.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),110, String.valueOf(opRZHP.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),111, String.valueOf(opRZHP.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),112, String.valueOf(opRZHP.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),113, String.valueOf(opRZHP.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),114, String.valueOf(opRZHP.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),115, String.valueOf(opRZHP.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),116, String.valueOf(opRZHP.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),117, String.valueOf(opRZHP.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),118, String.valueOf(opRZHP.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),119, String.valueOf(opRZHP.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),120, String.valueOf(opRZHP.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),121, String.valueOf(opRZHP.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),122, String.valueOf(opRZHP.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),123, String.valueOf(opRZHP.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),124, String.valueOf(opRZHP.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),125, String.valueOf(opRZHP.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),126, String.valueOf(opRZHP.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),127, String.valueOf(opRZHP.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),128, String.valueOf(opRZHP.getPoliorganNedostat())));
	}

	@Override
	public void update(OpRZHP opRZHP) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=5 and parameterId=:parameterId";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),1, String.valueOf(opRZHP.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),2, String.valueOf(opRZHP.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),3, String.valueOf(opRZHP.getRezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),4, String.valueOf(opRZHP.getRzhpsosudrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),5, String.valueOf(opRZHP.getRzhpvorotrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),6, String.valueOf(opRZHP.getKrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),7, String.valueOf(opRZHP.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),9, String.valueOf(opRZHP.getGemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),10, String.valueOf(opRZHP.getRzhpgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),11, String.valueOf(opRZHP.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),12, String.valueOf(opRZHP.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),13, String.valueOf(opRZHP.getRzhpgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),14, String.valueOf(opRZHP.getGeatype1())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),15, String.valueOf(opRZHP.getGeatype2())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),16, String.valueOf(opRZHP.getGeatype3())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),17, String.valueOf(opRZHP.getRadikal())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),18, String.valueOf(opRZHP.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),19, String.valueOf(opRZHP.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),21, String.valueOf(opRZHP.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),22, String.valueOf(opRZHP.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),23, String.valueOf(opRZHP.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),25, String.valueOf(opRZHP.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),26, String.valueOf(opRZHP.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),59, String.valueOf(opRZHP.getKamni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),60, String.valueOf(opRZHP.getZhkb())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),61, String.valueOf(opRZHP.getFormRak())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),63, String.valueOf(opRZHP.getLokalizaciaRaka())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),103, String.valueOf(opRZHP.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),104, String.valueOf(opRZHP.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),105, String.valueOf(opRZHP.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),106, String.valueOf(opRZHP.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),107, String.valueOf(opRZHP.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),108, String.valueOf(opRZHP.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),109, String.valueOf(opRZHP.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),110, String.valueOf(opRZHP.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),111, String.valueOf(opRZHP.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),112, String.valueOf(opRZHP.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),113, String.valueOf(opRZHP.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),114, String.valueOf(opRZHP.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),115, String.valueOf(opRZHP.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),116, String.valueOf(opRZHP.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),117, String.valueOf(opRZHP.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),118, String.valueOf(opRZHP.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),119, String.valueOf(opRZHP.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),120, String.valueOf(opRZHP.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),121, String.valueOf(opRZHP.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),122, String.valueOf(opRZHP.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),123, String.valueOf(opRZHP.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),124, String.valueOf(opRZHP.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),125, String.valueOf(opRZHP.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),126, String.valueOf(opRZHP.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),127, String.valueOf(opRZHP.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opRZHP.getId(),128, String.valueOf(opRZHP.getPoliorganNedostat())));
	}


	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=5";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}


	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 5);
		paramSource.addValue("value", value);
		return paramSource;
	}


	private static final class OpRZHPMapper implements RowMapper<List<OpRZHP>> {

		public List<OpRZHP> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<OpRZHP> list = new ArrayList<OpRZHP>();
			OpRZHP opRZHP = new OpRZHP();
			int i = 0;
			do {
				opRZHP.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 1: if (i!=0 && i!=53) return null;i=1;  opRZHP.setRezekciaType(rs.getInt("value"));break;
					case 2: i++; opRZHP.setRazdelenieTkanei(rs.getInt("value"));break;
					case 3: i++; opRZHP.setRezekcia(rs.getInt("value")); break;
					case 4: i++; opRZHP.setRzhpsosudrezekcia(rs.getInt("value"));break;
					case 5: i++; opRZHP.setRzhpvorotrezekcia(rs.getInt("value"));break;
					case 6: i++; opRZHP.setKrovopoteria(rs.getInt("value"));break;
					case 7: i++; opRZHP.setDlitop(rs.getString("value"));break;
					case 9: i++; opRZHP.setGemotransfusia(rs.getInt("value"));break;
					case 10: i++; opRZHP.setRzhpgemotransfusia_ml(rs.getString("value"));break;
					case 11: i++; opRZHP.setPlazma(rs.getString("value")); break;
					case 12: i++; opRZHP.setGdc(rs.getInt("value")); break;
					case 13: i++; opRZHP.setRzhpgdcmin(rs.getString("value")); break;
					case 14: i++; opRZHP.setGeatype1(rs.getInt("value")); break;
					case 15: i++; opRZHP.setGeatype2(rs.getInt("value")); break;
					case 16: i++; opRZHP.setGeatype3(rs.getInt("value")); break;
					case 17: i++; opRZHP.setRadikal(rs.getInt("value")); break;
					case 18: i++; opRZHP.setLimfodissekcia(rs.getInt("value")); break;
					case 19: i++; opRZHP.setTnm(rs.getInt("value")); break;
					case 21: i++; opRZHP.setBypass(rs.getInt("value")); break;
					case 22: i++; opRZHP.setTotIz(rs.getInt("value")); break;
					case 23: i++; opRZHP.setSelIz(rs.getInt("value")); break;
					case 25: i++; opRZHP.setOsl(rs.getInt("value")); break;
					case 26: i++; opRZHP.setClavien(rs.getInt("value")); break;
					case 59: i++; opRZHP.setKamni(rs.getInt("value")); break;
					case 60: i++; opRZHP.setZhkb(rs.getInt("value")); break;
					case 61: i++; opRZHP.setFormRak(rs.getInt("value")); break;
					case 63: i++; opRZHP.setLokalizaciaRaka(rs.getInt("value")); break;
					case 103: i++; opRZHP.setVnutrKrovot(rs.getInt("value")); break;
					case 104: i++; opRZHP.setZhkk(rs.getInt("value")); break;
					case 105: i++; opRZHP.setPechNedostatA(rs.getInt("value")); break;
					case 106: i++; opRZHP.setPechNedostatB(rs.getInt("value")); break;
					case 107: i++; opRZHP.setPechNedostatC(rs.getInt("value")); break;
					case 108: i++; opRZHP.setNagnoenie(rs.getInt("value")); break;
					case 109: i++; opRZHP.setCholangit(rs.getInt("value")); break;
					case 110: i++; opRZHP.setCholAbscedir(rs.getInt("value")); break;
					case 111: i++; opRZHP.setAscit(rs.getInt("value")); break;
					case 112: i++; opRZHP.setEventracia(rs.getInt("value")); break;
					case 113: i++; opRZHP.setKishNeprohod(rs.getInt("value")); break;
					case 114: i++; opRZHP.setPankreatSvich(rs.getInt("value")); break;
					case 115: i++; opRZHP.setOstriiSvich(rs.getInt("value")); break;
					case 116: i++; opRZHP.setSepsis(rs.getInt("value")); break;
					case 117: i++; opRZHP.setTyazhSepsis(rs.getInt("value")); break;
					case 118: i++; opRZHP.setSeptShok(rs.getInt("value")); break;
					case 119: i++; opRZHP.setZhelchSvish(rs.getInt("value")); break;
					case 120: i++; opRZHP.setKishSvish(rs.getInt("value")); break;
					case 121: i++; opRZHP.setTrombVorotVeni(rs.getInt("value")); break;
					case 122: i++; opRZHP.setTrombozPechArter(rs.getInt("value")); break;
					case 123: i++; opRZHP.setOdnostorPnevm(rs.getInt("value")); break;
					case 124: i++; opRZHP.setDvustorPnevm(rs.getInt("value")); break;
					case 125: i++; opRZHP.setGidrotorax(rs.getInt("value")); break;
					case 126: i++; opRZHP.setPochNedostat(rs.getInt("value")); break;
					case 127: i++; opRZHP.setSerdSosNedostat(rs.getInt("value")); break;
					case 128: i++; opRZHP.setPoliorganNedostat(rs.getInt("value")); break;
				}
				if (i == 53) {
					list.add(opRZHP);
					opRZHP = new OpRZHP();
				}
			}while (rs.next());
			return list;
		}
	}
}