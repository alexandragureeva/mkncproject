package com.amgureeva.binding.OpRZHPBinding;

import com.amgureeva.Entities.Surgeries.OpRZHP;

import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public interface OpRZHPDAO {
	OpRZHP findById(Integer id);

	List<OpRZHP> findAll();

	void save(OpRZHP opRZHP);

	void update(OpRZHP opRZHP);

	void delete(Integer id);
}
