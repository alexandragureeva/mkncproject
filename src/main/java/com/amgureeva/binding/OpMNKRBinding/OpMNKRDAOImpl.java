package com.amgureeva.binding.OpMNKRBinding;

import com.amgureeva.Entities.Surgeries.OpMNKR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 22.05.2016.
 */
@Repository
public class OpMNKRDAOImpl implements OpMNKRDAO {


	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public OpMNKR findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=4";

			List<OpMNKR> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpMNKRDAOImpl.OpMNKRMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}

	@Override
	public List<OpMNKR> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 4";
		List<List<OpMNKR>> result = namedParameterJdbcTemplate.query(sql,
				new OpMNKRDAOImpl.OpMNKRMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
		
	}

	@Override
	public void save(OpMNKR opMNKR) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 7, String.valueOf(opMNKR.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 12, String.valueOf(opMNKR.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 13, String.valueOf(opMNKR.getMnkrgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 18, String.valueOf(opMNKR.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 21, String.valueOf(opMNKR.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 22, String.valueOf(opMNKR.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 23, String.valueOf(opMNKR.getSelSosIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 25, String.valueOf(opMNKR.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 26, String.valueOf(opMNKR.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 32, String.valueOf(opMNKR.getSimultOp())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 62, String.valueOf(opMNKR.getOplech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 35, String.valueOf(opMNKR.getMnkrobrezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 36, String.valueOf(opMNKR.getMnkrsegrez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 37, String.valueOf(opMNKR.getMnkrchisloUdSeg())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 8, String.valueOf(opMNKR.getSochIntraRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 56, String.valueOf(opMNKR.getVarRezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 57, String.valueOf(opMNKR.getNalRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 58, String.valueOf(opMNKR.getRekOpTKish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 53, String.valueOf(opMNKR.getStadTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 54, String.valueOf(opMNKR.getChimiot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 55, String.valueOf(opMNKR.getKratRez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 1, String.valueOf(opMNKR.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 2, String.valueOf(opMNKR.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),103, String.valueOf(opMNKR.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),104, String.valueOf(opMNKR.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),105, String.valueOf(opMNKR.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),106, String.valueOf(opMNKR.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),107, String.valueOf(opMNKR.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),108, String.valueOf(opMNKR.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),109, String.valueOf(opMNKR.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),110, String.valueOf(opMNKR.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),111, String.valueOf(opMNKR.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),112, String.valueOf(opMNKR.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),113, String.valueOf(opMNKR.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),114, String.valueOf(opMNKR.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),115, String.valueOf(opMNKR.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),116, String.valueOf(opMNKR.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),117, String.valueOf(opMNKR.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),118, String.valueOf(opMNKR.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),119, String.valueOf(opMNKR.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),120, String.valueOf(opMNKR.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),121, String.valueOf(opMNKR.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),122, String.valueOf(opMNKR.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),123, String.valueOf(opMNKR.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),124, String.valueOf(opMNKR.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),125, String.valueOf(opMNKR.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),126, String.valueOf(opMNKR.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),127, String.valueOf(opMNKR.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),128, String.valueOf(opMNKR.getPoliorganNedostat())));
	}

	@Override
	public void update(OpMNKR opMNKR) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=4 and parameterId=:parameterId";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 7, String.valueOf(opMNKR.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 12, String.valueOf(opMNKR.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 13, String.valueOf(opMNKR.getMnkrgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 18, String.valueOf(opMNKR.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 21, String.valueOf(opMNKR.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 22, String.valueOf(opMNKR.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 23, String.valueOf(opMNKR.getSelSosIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 25, String.valueOf(opMNKR.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 26, String.valueOf(opMNKR.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 32, String.valueOf(opMNKR.getSimultOp())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 62, String.valueOf(opMNKR.getOplech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 35, String.valueOf(opMNKR.getMnkrobrezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 36, String.valueOf(opMNKR.getMnkrsegrez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 37, String.valueOf(opMNKR.getMnkrchisloUdSeg())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 8, String.valueOf(opMNKR.getSochIntraRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 56, String.valueOf(opMNKR.getVarRezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 57, String.valueOf(opMNKR.getNalRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 58, String.valueOf(opMNKR.getRekOpTKish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 53, String.valueOf(opMNKR.getStadTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 54, String.valueOf(opMNKR.getChimiot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 55, String.valueOf(opMNKR.getKratRez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 1, String.valueOf(opMNKR.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(), 2, String.valueOf(opMNKR.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),103, String.valueOf(opMNKR.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),104, String.valueOf(opMNKR.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),105, String.valueOf(opMNKR.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),106, String.valueOf(opMNKR.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),107, String.valueOf(opMNKR.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),108, String.valueOf(opMNKR.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),109, String.valueOf(opMNKR.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),110, String.valueOf(opMNKR.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),111, String.valueOf(opMNKR.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),112, String.valueOf(opMNKR.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),113, String.valueOf(opMNKR.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),114, String.valueOf(opMNKR.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),115, String.valueOf(opMNKR.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),116, String.valueOf(opMNKR.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),117, String.valueOf(opMNKR.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),118, String.valueOf(opMNKR.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),119, String.valueOf(opMNKR.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),120, String.valueOf(opMNKR.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),121, String.valueOf(opMNKR.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),122, String.valueOf(opMNKR.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),123, String.valueOf(opMNKR.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),124, String.valueOf(opMNKR.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),125, String.valueOf(opMNKR.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),126, String.valueOf(opMNKR.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),127, String.valueOf(opMNKR.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMNKR.getId(),128, String.valueOf(opMNKR.getPoliorganNedostat())));
	}
	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=4";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 4);
		paramSource.addValue("value", value);
		return paramSource;
	}

private static final class OpMNKRMapper implements RowMapper<List<OpMNKR>> {
	public List<OpMNKR> mapRow(ResultSet rs, int rowNum) throws SQLException {
		List<OpMNKR> list = new ArrayList<OpMNKR>();
		OpMNKR opMNKR = new OpMNKR();
		int i = 0;
		do {
			opMNKR.setId(rs.getString("surgeryId"));
			int parameterId = rs.getInt("parameterId");
			switch(parameterId) {
				case 1: i++;  opMNKR.setRezekciaType(rs.getInt("value")); break;
				case 2: i++; opMNKR.setRazdelenieTkanei(rs.getInt("value")); break;
				case 7: if (i!=0 && i!=49) return null; i = 1; opMNKR.setDlitop(rs.getString("value"));break;
				case 12: i++; opMNKR.setGdc(rs.getInt("value")); break;
				case 13: i++; opMNKR.setMnkrgdcmin(rs.getString("value")); break;
				case 18: i++; opMNKR.setLimfodissekcia(rs.getInt("value")); break;
				case 21: i++; opMNKR.setBypass(rs.getInt("value")); break;
				case 22: i++; opMNKR.setTotIz(rs.getInt("value")); break;
				case 23: i++; opMNKR.setSelSosIz(rs.getInt("value")); break;
				case 25: i++; opMNKR.setOsl(rs.getInt("value")); break;
				case 26: i++; opMNKR.setClavien(rs.getInt("value")); break;
				case 32: i++; opMNKR.setSimultOp(rs.getInt("value")); break;
				case 35: i++; opMNKR.setMnkrobrezekcii(rs.getInt("value")); break;
				case 36: i++; opMNKR.setMnkrsegrez(rs.getInt("value")); break;
				case 37: i++; opMNKR.setMnkrchisloUdSeg(rs.getInt("value")); break;
				case 8: i++; opMNKR.setSochIntraRHA(rs.getInt("value")); break;
				case 53: i++; opMNKR.setStadTnm(rs.getString("value")); break;
				case 54: i++; opMNKR.setChimiot(rs.getInt("value")); break;
				case 55: i++; opMNKR.setKratRez(rs.getInt("value")); break;
				case 56: i++; opMNKR.setVarRezekcii(rs.getInt("value")); break;
				case 57: i++; opMNKR.setNalRHA(rs.getInt("value")); break;
				case 58: i++; opMNKR.setRekOpTKish(rs.getInt("value")); break;
				case 62: i++; opMNKR.setOplech(rs.getInt("value")); break;
				case 103: i++; opMNKR.setVnutrKrovot(rs.getInt("value")); break;
				case 104: i++; opMNKR.setZhkk(rs.getInt("value")); break;
				case 105: i++; opMNKR.setPechNedostatA(rs.getInt("value")); break;
				case 106: i++; opMNKR.setPechNedostatB(rs.getInt("value")); break;
				case 107: i++; opMNKR.setPechNedostatC(rs.getInt("value")); break;
				case 108: i++; opMNKR.setNagnoenie(rs.getInt("value")); break;
				case 109: i++; opMNKR.setCholangit(rs.getInt("value")); break;
				case 110: i++; opMNKR.setCholAbscedir(rs.getInt("value")); break;
				case 111: i++; opMNKR.setAscit(rs.getInt("value")); break;
				case 112: i++; opMNKR.setEventracia(rs.getInt("value")); break;
				case 113: i++; opMNKR.setKishNeprohod(rs.getInt("value")); break;
				case 114: i++; opMNKR.setPankreatSvich(rs.getInt("value")); break;
				case 115: i++; opMNKR.setOstriiSvich(rs.getInt("value")); break;
				case 116: i++; opMNKR.setSepsis(rs.getInt("value")); break;
				case 117: i++; opMNKR.setTyazhSepsis(rs.getInt("value")); break;
				case 118: i++; opMNKR.setSeptShok(rs.getInt("value")); break;
				case 119: i++; opMNKR.setZhelchSvish(rs.getInt("value")); break;
				case 120: i++; opMNKR.setKishSvish(rs.getInt("value")); break;
				case 121: i++; opMNKR.setTrombVorotVeni(rs.getInt("value")); break;
				case 122: i++; opMNKR.setTrombozPechArter(rs.getInt("value")); break;
				case 123: i++; opMNKR.setOdnostorPnevm(rs.getInt("value")); break;
				case 124: i++; opMNKR.setDvustorPnevm(rs.getInt("value")); break;
				case 125: i++; opMNKR.setGidrotorax(rs.getInt("value")); break;
				case 126: i++; opMNKR.setPochNedostat(rs.getInt("value")); break;
				case 127: i++; opMNKR.setSerdSosNedostat(rs.getInt("value")); break;
				case 128: i++; opMNKR.setPoliorganNedostat(rs.getInt("value")); break;

			}
			if (i == 49) {
				list.add(opMNKR);
				opMNKR = new OpMNKR();
			}
		}while (rs.next());
		return list;
	}
}
}

