package com.amgureeva.binding.OpMNKRBinding;

import com.amgureeva.Entities.Surgeries.OpMNKR;
import com.amgureeva.Entities.Surgeries.OpMNKRList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public class OpMNKRListCreator {

	public List<OpMNKRList> createMnkrList(List<OpMNKR> opMNKRs, PatientService patientService) {
		if (opMNKRs != null && patientService != null) {
			List<OpMNKRList> opMNKRLists = new ArrayList<OpMNKRList>();
			OpMNKRList opMNKRList = new OpMNKRList();
			for (int i = 0; i < opMNKRs.size(); i++) {
				opMNKRList.setName(patientService.findPatientById(Integer.valueOf(opMNKRs.get(i).getId())).getName());
				switch (Integer.valueOf(opMNKRs.get(i).getOplech())) {
					case 0:
						opMNKRList.setOplech("--");
						break;
					case 1:
						opMNKRList.setOplech("В легких");
						break;
					case 2:
						opMNKRList.setOplech("В лимфатических узлах брюшной полости");
						break;
					case 3:
						opMNKRList.setOplech("По брюшине");
						break;
					case 4:
						opMNKRList.setOplech("В других органах брюшной полости");
						break;
				}
				switch (Integer.valueOf(opMNKRs.get(i).getMnkrobrezekcii())) {
					case 0:
						opMNKRList.setObrezekcii("--");
						break;
					case 1:
						opMNKRList.setObrezekcii("Обширная резекция ЛГГЭ");
						break;
					case 2:
						opMNKRList.setObrezekcii("Обширная резекция ПГГЭ");
						break;
					case 3:
						opMNKRList.setObrezekcii("Cегментарная резекция");
						break;
				}
				switch (Integer.valueOf(opMNKRs.get(i).getChimiot())) {
					case 0:
						opMNKRList.setChimiot("--");
						break;
					case 1:
						opMNKRList.setChimiot("нет");
						break;
					case 2:
						opMNKRList.setChimiot("Цитостатики");
						break;
					case 3:
						opMNKRList.setChimiot("Цитостатики + таргетная терапия");
						break;
				}
				switch (Integer.valueOf(opMNKRs.get(i).getKratRez())) {
					case 0:
						opMNKRList.setKratRez("--");
						break;
					case 1:
						opMNKRList.setKratRez("Первичная");
						break;
					case 2:
						opMNKRList.setKratRez("Вторая");
						break;
					case 3:
						opMNKRList.setKratRez("Третья");
						break;
					case 4:
						opMNKRList.setKratRez("Четвертая");
						break;
				}
				switch (Integer.valueOf(opMNKRs.get(i).getMnkrsegrez())) {
					case 0:
						opMNKRList.setSegrez("--");
						break;
					case 1:
						opMNKRList.setSegrez("Анатомическая");
						break;
					case 2:
						opMNKRList.setSegrez("Неанатомическая");
						break;
				}
				switch (Integer.valueOf(opMNKRs.get(i).getMnkrchisloUdSeg())) {
					case 0:
						opMNKRList.setChisloUdSeg("--");
						break;
					case 1:
						opMNKRList.setChisloUdSeg("1");
						break;
					case 2:
						opMNKRList.setChisloUdSeg("2");
						break;
					case 3:
						opMNKRList.setChisloUdSeg("3");
						break;
				}
				switch (Integer.valueOf(opMNKRs.get(i).getVarRezekcii())) {
					case 0:
						opMNKRList.setVarRezekcii("--");
						break;
					case 1:
						opMNKRList.setVarRezekcii("Открытая");
						break;
					case 2:
						opMNKRList.setVarRezekcii("Лапароскопическая");
						break;

				}
				switch (Integer.valueOf(opMNKRs.get(i).getNalRHA())) {
					case 0:
						opMNKRList.setNalRHA("--");
						break;
					case 1:
						opMNKRList.setNalRHA("нет");
						break;
					case 2:
						opMNKRList.setNalRHA("1");
						break;
					case 3:
						opMNKRList.setNalRHA("2");
						break;
					case 4:
						opMNKRList.setNalRHA("3");
						break;
				}
				switch (Integer.valueOf(opMNKRs.get(i).getSimultOp())) {
					case 0:
						opMNKRList.setSimultOp("--");
						break;
					case 1:
						opMNKRList.setSimultOp("Резекция толстой кишки");
						break;
					case 2:
						opMNKRList.setSimultOp("Резекция прямой кишки");
						break;
					case 3:
						opMNKRList.setSimultOp("Резекция диафрагмы");
						break;
					case 4:
						opMNKRList.setSimultOp("Резекция других органов(тонкой кишки и др)");
						break;
				}

				switch (Integer.valueOf(opMNKRs.get(i).getClavien())) {
					case 0:
						opMNKRList.setClavien("--");
						break;
					case 1:
						opMNKRList.setClavien("I");
						break;
					case 2:
						opMNKRList.setClavien("II");
						break;
					case 3:
						opMNKRList.setClavien("IIIa");
						break;
					case 4:
						opMNKRList.setClavien("IIIb");
						break;
					case 5:
						opMNKRList.setClavien("IVa");
						break;
					case 6:
						opMNKRList.setClavien("IVb");
						break;
					case 7:
						opMNKRList.setClavien("V");
						break;
				}
				switch (Integer.valueOf(opMNKRs.get(i).getOsl())) {
					case 0:
						opMNKRList.setOsl("--");
						break;
					case 1:
						opMNKRList.setOsl("Внутрибрюшное кровотечение");
						break;
					case 2:
						opMNKRList.setOsl("ЖКК");
						break;
					case 3:
						opMNKRList.setOsl("Печеночная недостаточность A");
						break;
					case 4:
						opMNKRList.setOsl("Печеночная недостаточность B");
						break;
					case 5:
						opMNKRList.setOsl("Печеночная недостаточность C");
						break;
					case 6:
						opMNKRList.setOsl("Паренхиматозная желтуха");
						break;
					case 7:
						opMNKRList.setOsl("Механическая желтуха");
						break;
					case 8:
						opMNKRList.setOsl("Нагноение раны");
						break;
					case 9:
						opMNKRList.setOsl("Холангит");
						break;
					case 10:
						opMNKRList.setOsl("Холангиогенное абсцедирование");
						break;
					case 11:
						opMNKRList.setOsl("Асцит");
						break;
					case 12:
						opMNKRList.setOsl("Эвентрация");
						break;
					case 13:
						opMNKRList.setOsl("Кишечная непроходимость");
						break;
					case 14:
						opMNKRList.setOsl("Панкреатический свищ");
						break;
					case 15:
						opMNKRList.setOsl("Острый панкреатит");
						break;
					case 16:
						opMNKRList.setOsl("Сепсис");
						break;
					case 17:
						opMNKRList.setOsl("Тяжелый сепсис");
						break;
					case 18:
						opMNKRList.setOsl("Септический шок");
						break;
					case 19:
						opMNKRList.setOsl("Желчный свищ");
						break;
					case 20:
						opMNKRList.setOsl("Кишечный свищ");
						break;
					case 21:
						opMNKRList.setOsl("Тромбоз воротной вены");
						break;
					case 22:
						opMNKRList.setOsl("Тромбоз печеночной артерии");
						break;
					case 23:
						opMNKRList.setOsl("Односторонняя пневмония");
						break;
					case 24:
						opMNKRList.setOsl("Двусторонняя пневмония");
						break;
					case 25:
						opMNKRList.setOsl("Гидроторакс");
						break;
					case 26:
						opMNKRList.setOsl("Почечная недостаточность");
						break;
					case 27:
						opMNKRList.setOsl("Сердечно-сосудистая недостаточность");
						break;
					case 28:
						opMNKRList.setOsl("Полиорганная недостаточность");
						break;

				}
				if (opMNKRs.get(i).getDlitop().equals("")||opMNKRs.get(i).getDlitop().equals("null"))
					opMNKRList.setDlitop("--");
				else
					opMNKRList.setDlitop(String.valueOf(opMNKRs.get(i).getDlitop()));
				if (opMNKRs.get(i).getStadTnm()==null||opMNKRs.get(i).getStadTnm().equals("")||opMNKRs.get(i).getStadTnm().equals("null"))
					opMNKRList.setStadTnm("--");
				else
					opMNKRList.setStadTnm(String.valueOf(opMNKRs.get(i).getStadTnm()));
				if (opMNKRs.get(i).getSochIntraRHA() == 0)
					opMNKRList.setSochIntraRHA("-");
				else
					opMNKRList.setSochIntraRHA("+");
				if (opMNKRs.get(i).getLimfodissekcia() == 0)
					opMNKRList.setLimfodissekcia("-");
				else
					opMNKRList.setLimfodissekcia("+");
				if (opMNKRs.get(i).getGdc() == 0)
					opMNKRList.setGdc("-");
				else
					opMNKRList.setGdc("+");
				if (opMNKRs.get(i).getMnkrgdcmin().equals("")||opMNKRs.get(i).getMnkrgdcmin().equals("null"))
					opMNKRList.setGdcmin("--");
				else
					opMNKRList.setGdcmin(String.valueOf(opMNKRs.get(i).getMnkrgdcmin()));
				if (opMNKRs.get(i).getRekOpTKish() == 0)
					opMNKRList.setRekOpTKish("-");
				else
					opMNKRList.setRekOpTKish("+");
				if (opMNKRs.get(i).getBypass() == 0)
					opMNKRList.setBypass("-");
				else
					opMNKRList.setBypass("+");
				if (opMNKRs.get(i).getTotIz() == 0)
					opMNKRList.setTotIz("-");
				else
					opMNKRList.setTotIz("+");
				if (opMNKRs.get(i).getSelSosIz() == 0)
					opMNKRList.setSelSosIz("-");
				else
					opMNKRList.setSelSosIz("+");
				opMNKRLists.add(opMNKRList);
				opMNKRList = new OpMNKRList();
			}
			return opMNKRLists;
		}
		return null;
	}
}
