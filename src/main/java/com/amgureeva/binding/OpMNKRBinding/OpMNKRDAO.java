package com.amgureeva.binding.OpMNKRBinding;

import com.amgureeva.Entities.Surgeries.OpMNKR;

import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public interface OpMNKRDAO {

	OpMNKR findById(Integer id);

	List<OpMNKR> findAll();

	void save(OpMNKR opMNKR);

	void update(OpMNKR opMNKR);

	void delete(Integer id);
}
