package com.amgureeva.binding.EchinokokkozBinding;

import com.amgureeva.Entities.Diagnosis.Echinokokkoz;
import com.amgureeva.Entities.Diagnosis.EchinokokkozList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 05.05.2016.
 */
public class EchinokokkozListCreator {
    public List<EchinokokkozList> createechLists(List<Echinokokkoz> echs, PatientService patientService) {
        if (echs != null && patientService != null) {
            List<EchinokokkozList> echLists = new ArrayList<EchinokokkozList>();
            EchinokokkozList echList = new EchinokokkozList();
            for (int i = 0; i < echs.size(); i++) {
                echList.setName(patientService.findPatientById(Integer.valueOf(echs.get(i).getId())).getName());
                switch (Integer.valueOf(echs.get(i).getVoz())) {
                    case 0:
                        echList.setVoz("--");
                        break;
                    case 1:
                        echList.setVoz("CE0");
                        break;
                    case 2:
                        echList.setVoz("CE1");
                        break;
                    case 3:
                        echList.setVoz("CE2");
                        break;
                    case 4:
                        echList.setVoz("CE3a");
                        break;
                    case 5:
                        echList.setVoz("CE3b");
                        break;
                    case 6:
                        echList.setVoz("CE4");
                        break;
                    case 7:
                        echList.setVoz("CE5");
                        break;
                }
                switch (Integer.valueOf(echs.get(i).getKistyOsl())) {
                    case 0:
                        echList.setKistyOsl("--");
                        break;
                    case 1:
                        echList.setKistyOsl("нет");
                        break;
                    case 2:
                        echList.setKistyOsl("Нагноение");
                        break;
                    case 3:
                        echList.setKistyOsl("Наличие желчного свища в кисте");
                        break;
                    case 4:
                        echList.setKistyOsl("Прорыв в желчные потоки");
                        break;
                    case 5:
                        echList.setKistyOsl("Разрыв внутрь живота");
                        break;
                    case 6:
                        echList.setKistyOsl("Угроза разрыва");
                        break;
                }
                switch (Integer.valueOf(echs.get(i).getEpidemAnamnez())) {
                    case 0:
                        echList.setEpidemAnamnez("--");
                        break;
                    case 1:
                        echList.setEpidemAnamnez("Не относится к группе риска");
                        break;
                    case 2:
                        echList.setEpidemAnamnez("Животновод");
                        break;
                    case 3:
                        echList.setEpidemAnamnez("Проживающий в сельской местности");
                        break;
                    case 4:
                        echList.setEpidemAnamnez("Охотник");
                        break;
                    case 5:
                        echList.setEpidemAnamnez("Продавец мясных продуктов");
                        break;
                    case 6:
                        echList.setEpidemAnamnez("Проживающий в гиперэндемичной области");
                        break;
                }

                switch (Integer.valueOf(echs.get(i).getLechenieType())) {
                    case 0:
                        echList.setLechenieType("--");
                        break;
                    case 1:
                        echList.setLechenieType("Пункционного лечения");
                        break;
                    case 2:
                        echList.setLechenieType("Лапароскопии");
                        break;
                    case 3:
                        echList.setLechenieType("Открытой операции");
                        break;
                }
                switch (Integer.valueOf(echs.get(i).getKratLechenia())) {
                    case 0:
                        echList.setKratLechenia("--");
                        break;
                    case 1:
                        echList.setKratLechenia("Первич.эхинококкоз");
                        break;
                    case 2:
                        echList.setKratLechenia("Рецид.эхинококкоз");
                        break;
                }
                switch (Integer.valueOf(echs.get(i).getVnepPorazhenie())) {
                    case 0:
                        echList.setVnepPorazhenie("--");
                        break;
                    case 1:
                        echList.setVnepPorazhenie("нет");
                        break;
                    case 2:
                        echList.setVnepPorazhenie("Кисты легких");
                        break;
                    case 3:
                        echList.setVnepPorazhenie("Кисты брюшной полочти");
                        break;
                    case 4:
                        echList.setVnepPorazhenie("Кисты других локализаций");
                        break;
                }
                switch (Integer.valueOf(echs.get(i).getTrofStatus())) {
                    case 0:
                        echList.setTrofStatus("--");
                        break;
                    case 1:
                        echList.setTrofStatus("Хороший");
                        break;
                    case 2:
                        echList.setTrofStatus("Удовлетворительный");
                        break;
                    case 3:
                        echList.setTrofStatus("Неудовлетворительный");
                        break;
                }
                if (echs.get(i).getKistyRazmer().equals("null"))
                    echList.setKistyRazmer("--");
                else
                    echList.setKistyRazmer(echs.get(i).getKistyRazmer());
                echList.setAntitelaUroven(String.valueOf(echs.get(i).getAntitelaUroven()));
                echList.setAntitelaRes(String.valueOf(echs.get(i).getAntitelaRes()));
                echList.setKistyNumber(String.valueOf(echs.get(i).getKistyNumber()));
                if (!echs.get(i).isAntitela())
                    echList.setAntitela("-");
                else
                    echList.setAntitela("+");
                if (!echs.get(i).isMnozhKisty())
                    echList.setMnozhKisty("Единичные");
                else
                    echList.setMnozhKisty("Множественные");
                if (echs.get(i).getKistyType())
                    echList.setKistyType("Унилабарные");
                else
                    echList.setKistyType("Билабарные");
                echLists.add(echList);
                echList = new EchinokokkozList();
            }

            return echLists;
        }
        return null;
    }

    public static String getAllVariants(String s) {
        s = s.replace("_ПОСТ", "");
        s = s.replace("_ВЫПИС", "");
        s = s.replace("_ОПЕР", "");
        if (s.equals("ech:OCENKA_TROF_STATUSA")) {
            return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 -\"Проживающий в сельской местности\"";
        }
        if (s.equals("ech:EPIDEM_ANAMNEZ")) {
            return "1 - \"Не относится к группе риска\"; 2 - \"Животновод\"; 3- \"Проживающий в сельской местности\";" +
                    " 4 - \"Охотник\"; 5 -\"Продавец мясных продуктов\"; 6 - \"Проживающий в гиперэндемичной области\"";
        }
        if (s.equals("ech:UZI_KISTI_OSL")) {
            return "1 - \"нет\"; 2 - \"Нагноение\"; 3 -\"Наличие желчного свища в кисте\"" +
                    "4 - \"Прорыв в желчные потоки\"; 5 - \"Разрыв внутрь живота\"; 6 -\"Угроза разрыва\"";
        }
        if (s.equals("ech:UZI_KISTI_TYPE")) {
            return "0 - \"Унилабарные\"; 1 - \"Билабарные\"";
        }
        if (s.equals("ech:UZI_KRATNOST_LECHENIYA_DETAILS")) {
            return "1 - \"После Пункционного лечения\"; 2 - \"После Лапароскопии\"; 3 -\"После Открытой операции\";";
        }
        if (s.equals("ech:UZI_KRATNOST_LECHENIYA")) {
            return "1 - \"Первич.эхинококкоз\"; 2 - \"Рецид.эхинококкоз\";";
        }
        if (s.equals("ech:UZI_NALICHIE_VN_PORAZHENIYA")) {
            return "1 - \"нет\"; 2 - \"Кисты легких\"; 3 -\"Кисты брюшной полочти\"" +
                    "4-\"Кисты других локализаций\"";
        }
        if (s.equals("ech:VOZ")) {
            return "1 - \"CE0\"; 2 - \"CE1\"; 3 -\"CE2\"; 4 - \"CE3a\"; 5 - \"CE1\"; 6 -\"CE4\"; 7 - \"CE5\"";
        }
        if (s.equals("ech:AK_ANTITELA_TYPE")) {
            return "1 - \"Положительный результат\"; 2 - \"Отрицательный результат\"";
        }

        if (s.equals("ech:OSLOZH")) {
            return "1 - \"Внутрибрюшное кровотечение\"; 2 - \"ЖКК\"; 3 - \"Печеночная недостаточность A\";" +
                    " 4 - \"Печеночная недостаточность B\"; 5 - \"Печеночная недостаточность C\";" +
                    " 6 - \"Механическая желтуха\"; 7 - \"Нагноение раны\"; 8 - \"Холангит\";" +
                    " 9 - \"Холангиогенное абсцедирование\"; 10 - \"Асцит\"; 11 - \"Эвентрация\"" +
                    "; 12 - \"Кишечная непроходимость\"; 13 - \"Панкреатический свищ\"; 14 - \"Острый панкреатит\";" +
                    " 15 - \"Сепсис\"; 16 - \"Тяжелый сепсис\"; 17 - \"Септический шок\"; 18 - \"Желчный свищ\"" +
                    "; 19 - \"Кишечный свищ\"; 20 - \"Тромбоз воротной вены\"; 21 - \"Тромбоз печеночной артерии\";" +
                    " 22 - \"Односторонняя пневмония\"" +
                    "; 23 - \"Двусторонняя пневмония\"; 24 - \"Гидроторакс\"; 25 - \"Почечная недостаточность\"" +
                    "; 26 - \"Сердечно-сосудистая недостаточность\"; 27 - \"Полиорганная недостаточность\"";
        }
        if (s.equals("ech:CLAVIEN")) {
            return "1 - \"I\"; 2 - \"II\"; 3 - \"IIIa\"; 4 - \"IIIb\"; 5 - \"IVa\"; 6 - \"IVb\"; 7 - \"V\"";
        }
        if (s.equals("ech:REZEKCIA")) {
            return "1 - \"Идеальная эхтнококкэктомия\"; 2 - \"Открытая эхинококкэктомия\";" +
                    " 3 - \"Сочетание обоих методов\"; 4 - \"Резекция печени\"";
        }
        if (s.equals("ech:OTKR_ECHINOEKTOMIA")) {
            return "1 - \"С тотальной перицисэктомией\"; 2 - \"С субтотальной перицистэктомией\";" +
                    " 3 - \"С частичной перицистэктомией\"";
        }
        if (s.equals("ech:OBR_FIBR_KAPSULI")) {
            return "1 - \"Нет\"; 2 - \"Капитонаж\"; 3 - \"Тампонирование сальником\"";
        }
        if (s.equals("ech:OBR_SVISHA")) {
            return "1 - \"Нет\"; 2 - \"Ушивание устья в фиброзной капсуле\";" +
                    " 3 - \"Клипирование устья в фиб\"; 4 - \"Выделение и перевязка сосудисто-секреторной ножки\"";
        }
        if (s.equals("ech:CHIRURG_LECHEN")) {
            return "1 - \"Пункционно-дренажное лечение (PAER)\"; 2 - \"Лапроскапическая\"; 3 - \"Открытая\"";
        }
        if (s.equals("ech:SIMULT_OPERACIA")) {
            return "1 - \"Нет\"; 2 - \"В сочетании с резекцией легкого\";" +
                    " 3 - \"В сочетании с резекцией др органов брюшной полости\";" +
                    " 4 - \"В сочетании с удалением внеорганных кист\"";
        }
        if (s.equals("ech:INTRAOPER_OSL")) {
            return "1 - \"Нет\"; 2 - \"Анафелоксия с падением АД\"; " +
                    "3 - \"Разрыв кисты с опсеменением брюшной полости\"";
        }
        return "";
    }
}
