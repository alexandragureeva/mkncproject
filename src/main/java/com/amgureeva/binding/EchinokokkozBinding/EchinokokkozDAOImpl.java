package com.amgureeva.binding.EchinokokkozBinding;

import com.amgureeva.Entities.Diagnosis.Echinokokkoz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 04.05.2016.
 */
@Repository
public class EchinokokkozDAOImpl implements EchinokokkozDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Echinokokkoz findById(Integer id, boolean isFirstDiagnosis) {
        if (id != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
			params.put("admissiondiag", (isFirstDiagnosis)?1:0);

			String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=6" +
					" AND admissiondiag=:admissiondiag";

            List<List<Echinokokkoz>> result = null;
            try {
                result = namedParameterJdbcTemplate
                        .query(sql, params, new EchinokokkozMapper());
            } catch (EmptyResultDataAccessException e) {
                // do nothing, return null
            }
            result.remove(null);
            if (result == null || result.size() == 0)
                return null;
            if (isFirstDiagnosis) {
                if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
                if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
            }
            if (!isFirstDiagnosis) {
                if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
                if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
            }
        }
        return null;
    }

    @Override
    public List<Echinokokkoz> findAll() {
        String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 6";
        List<List<Echinokokkoz>> result = namedParameterJdbcTemplate.query(sql, new EchinokokkozMapper());
        return (result == null || result.size() == 0)?null :result.get(0);
    }

    @Override
    public void save(Echinokokkoz echinokokkoz) {
        String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
                + "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),53, String.valueOf(echinokokkoz.getVoz()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),54, String.valueOf((echinokokkoz.isAntitela())?1:0), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),55, String.valueOf(echinokokkoz.getAntitelaRes()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),56, String.valueOf(echinokokkoz.getAntitelaUroven()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),57, String.valueOf(echinokokkoz.getEpidemAnamnez()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),58, String.valueOf((echinokokkoz.isMnozhKisty())?1:0), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),59, String.valueOf(echinokokkoz.getKistyNumber()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),60, String.valueOf((echinokokkoz.getKistyType())?1:0), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),61, String.valueOf(echinokokkoz.getKistyRazmer()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),62, String.valueOf(echinokokkoz.getKistyOsl()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),63, String.valueOf(echinokokkoz.getKratLechenia()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),64, String.valueOf(echinokokkoz.getLechenieType()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),65, String.valueOf(echinokokkoz.getVnepPorazhenie()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),11, String.valueOf(echinokokkoz.getTrofStatus()), echinokokkoz.isDiagnosisAtStart()));
    }

    @Override
    public void update(Echinokokkoz echinokokkoz) {
        String sql = "UPDATE patientparametersvalue SET value=:value"
                + " WHERE patientId=:patientId and diseaseId=6 and parameterId=:parameterId and admissiondiag=:admissiondiag";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),53, String.valueOf(echinokokkoz.getVoz()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),54, String.valueOf((echinokokkoz.isAntitela())?1:0), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),55, String.valueOf(echinokokkoz.getAntitelaRes()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),56, String.valueOf(echinokokkoz.getAntitelaUroven()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),57, String.valueOf(echinokokkoz.getEpidemAnamnez()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),58, String.valueOf((echinokokkoz.isMnozhKisty())?1:0), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),59, String.valueOf(echinokokkoz.getKistyNumber()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),60, String.valueOf((echinokokkoz.getKistyType())?1:0), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),61, String.valueOf(echinokokkoz.getKistyRazmer()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),62, String.valueOf(echinokokkoz.getKistyOsl()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),63, String.valueOf(echinokokkoz.getKratLechenia()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),64, String.valueOf(echinokokkoz.getLechenieType()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),65, String.valueOf(echinokokkoz.getVnepPorazhenie()), echinokokkoz.isDiagnosisAtStart()));
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (echinokokkoz.getId(),11, String.valueOf(echinokokkoz.getTrofStatus()), echinokokkoz.isDiagnosisAtStart()));
        }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=6";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }

    private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value,boolean admissiondiag) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("patientId", patientId);
        paramSource.addValue("parameterId", fieldId);
        paramSource.addValue("diseaseId", 6);
        paramSource.addValue("value", value);
        paramSource.addValue("admissiondiag", admissiondiag);
        return paramSource;
    }


    private static final class EchinokokkozMapper implements RowMapper<List<Echinokokkoz>> {

        public List<Echinokokkoz> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<Echinokokkoz> echList = new ArrayList<Echinokokkoz>();
            Echinokokkoz echinokokkoz = new Echinokokkoz();
            int i = 0;
            do{

                echinokokkoz.setId(rs.getString("patientId"));
                echinokokkoz.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
                int parameterId = rs.getInt("parameterId");
                switch(parameterId) {
                    case 53: i++; echinokokkoz.setVoz(rs.getInt("value"));break;
                    case 54: i++; echinokokkoz.setAntitela((rs.getInt("value")==1)?true:false); break;
                    case 55: i++; echinokokkoz.setAntitelaRes(rs.getInt("value"));break;
                    case 56: i++; echinokokkoz.setAntitelaUroven(rs.getInt("value"));break;
                    case 57: i++; echinokokkoz.setEpidemAnamnez(rs.getInt("value"));break;
                    case 58: i++; echinokokkoz.setMnozhKisty((rs.getInt("value")==1)?true:false); break;
                    case 59: i++; echinokokkoz.setKistyNumber(rs.getInt("value"));break;
                    case 60: i++; echinokokkoz.setKistyType((rs.getInt("value")==1)?true:false);break;
                    case 61: i++; echinokokkoz.setKistyRazmer(rs.getString("value"));break;
                    case 62: i++; echinokokkoz.setKistyOsl(rs.getInt("value")); break;
                    case 63: i++; echinokokkoz.setKratLechenia(rs.getInt("value")); break;
                    case 64: i++; echinokokkoz.setLechenieType(rs.getInt("value")); break;
                    case 65: i++; echinokokkoz.setVnepPorazhenie(rs.getInt("value")); break;
                    case 11: if (i!=0 && i!=14) return null;i=1; echinokokkoz.setTrofStatus(rs.getInt("value")); break;
                }
                if (i == 14) {
                    echList.add(echinokokkoz);
                    echinokokkoz = new Echinokokkoz();
                }
            }while (rs.next());
            return echList;
        }
    }
}
