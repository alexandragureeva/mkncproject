package com.amgureeva.binding.EchinokokkozBinding;

import com.amgureeva.Entities.Diagnosis.Echinokokkoz;

import java.util.List;

/**
 * Created by Александра on 04.05.2016.
 */
public interface EchinokokkozDAO {
    Echinokokkoz findById(Integer id, boolean isFirstDiagnosis);

    List<Echinokokkoz> findAll();

    void save(Echinokokkoz echinokokkoz);

    void update(Echinokokkoz echinokokkoz);

    void delete(Integer id);
}
