package com.amgureeva.binding.OpEchinokokkozBinding;

import com.amgureeva.Entities.Surgeries.OpEchinokokkoz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 22.05.2016.
 */
@Repository
public class OpEchinokokkozDAOImpl implements OpEchinokokkozDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public OpEchinokokkoz findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=6";

			List<OpEchinokokkoz> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpEchinokokkozMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}

	@Override
	public List<OpEchinokokkoz> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 6";
		List<List<OpEchinokokkoz>> result = namedParameterJdbcTemplate.query(sql,
				new OpEchinokokkozMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(OpEchinokokkoz opEchinokokkoz) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),3, String.valueOf(opEchinokokkoz.getRezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),7, String.valueOf(opEchinokokkoz.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),27, String.valueOf(opEchinokokkoz.getOtkrEch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),28, String.valueOf(opEchinokokkoz.getObrabotkaFibr())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),29, String.valueOf(opEchinokokkoz.getObrabotkaSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),30, String.valueOf(opEchinokokkoz.getChirurgLech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),31, String.valueOf(opEchinokokkoz.getEtapLech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),32, String.valueOf(opEchinokokkoz.getSimOper())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),33, String.valueOf(opEchinokokkoz.getIntraOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),12, String.valueOf(opEchinokokkoz.getEchgdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),13, String.valueOf(opEchinokokkoz.getEchgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),21, String.valueOf(opEchinokokkoz.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),22, String.valueOf(opEchinokokkoz.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),23, String.valueOf(opEchinokokkoz.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),25, String.valueOf(opEchinokokkoz.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),26, String.valueOf(opEchinokokkoz.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),1, String.valueOf(opEchinokokkoz.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),2, String.valueOf(opEchinokokkoz.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),103, String.valueOf(opEchinokokkoz.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),104, String.valueOf(opEchinokokkoz.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),105, String.valueOf(opEchinokokkoz.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),106, String.valueOf(opEchinokokkoz.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),107, String.valueOf(opEchinokokkoz.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),108, String.valueOf(opEchinokokkoz.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),109, String.valueOf(opEchinokokkoz.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),110, String.valueOf(opEchinokokkoz.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),111, String.valueOf(opEchinokokkoz.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),112, String.valueOf(opEchinokokkoz.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),113, String.valueOf(opEchinokokkoz.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),114, String.valueOf(opEchinokokkoz.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),115, String.valueOf(opEchinokokkoz.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),116, String.valueOf(opEchinokokkoz.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),117, String.valueOf(opEchinokokkoz.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),118, String.valueOf(opEchinokokkoz.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),119, String.valueOf(opEchinokokkoz.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),120, String.valueOf(opEchinokokkoz.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),121, String.valueOf(opEchinokokkoz.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),122, String.valueOf(opEchinokokkoz.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),123, String.valueOf(opEchinokokkoz.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),124, String.valueOf(opEchinokokkoz.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),125, String.valueOf(opEchinokokkoz.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),126, String.valueOf(opEchinokokkoz.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),127, String.valueOf(opEchinokokkoz.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),128, String.valueOf(opEchinokokkoz.getPoliorganNedostat())));
	}

	@Override
	public void update(OpEchinokokkoz opEchinokokkoz) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=6 and parameterId=:parameterId";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),3, String.valueOf(opEchinokokkoz.getRezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),7, String.valueOf(opEchinokokkoz.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),27, String.valueOf(opEchinokokkoz.getOtkrEch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),28, String.valueOf(opEchinokokkoz.getObrabotkaFibr())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),29, String.valueOf(opEchinokokkoz.getObrabotkaSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),30, String.valueOf(opEchinokokkoz.getChirurgLech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),31, String.valueOf(opEchinokokkoz.getEtapLech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),32, String.valueOf(opEchinokokkoz.getSimOper())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),33, String.valueOf(opEchinokokkoz.getIntraOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),12, String.valueOf(opEchinokokkoz.getEchgdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),13, String.valueOf(opEchinokokkoz.getEchgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),21, String.valueOf(opEchinokokkoz.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),22, String.valueOf(opEchinokokkoz.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),23, String.valueOf(opEchinokokkoz.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),25, String.valueOf(opEchinokokkoz.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),26, String.valueOf(opEchinokokkoz.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),1, String.valueOf(opEchinokokkoz.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),2, String.valueOf(opEchinokokkoz.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),103, String.valueOf(opEchinokokkoz.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),104, String.valueOf(opEchinokokkoz.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),105, String.valueOf(opEchinokokkoz.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),106, String.valueOf(opEchinokokkoz.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),107, String.valueOf(opEchinokokkoz.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),108, String.valueOf(opEchinokokkoz.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),109, String.valueOf(opEchinokokkoz.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),110, String.valueOf(opEchinokokkoz.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),111, String.valueOf(opEchinokokkoz.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),112, String.valueOf(opEchinokokkoz.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),113, String.valueOf(opEchinokokkoz.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),114, String.valueOf(opEchinokokkoz.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),115, String.valueOf(opEchinokokkoz.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),116, String.valueOf(opEchinokokkoz.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),117, String.valueOf(opEchinokokkoz.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),118, String.valueOf(opEchinokokkoz.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),119, String.valueOf(opEchinokokkoz.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),120, String.valueOf(opEchinokokkoz.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),121, String.valueOf(opEchinokokkoz.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),122, String.valueOf(opEchinokokkoz.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),123, String.valueOf(opEchinokokkoz.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),124, String.valueOf(opEchinokokkoz.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),125, String.valueOf(opEchinokokkoz.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),126, String.valueOf(opEchinokokkoz.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),127, String.valueOf(opEchinokokkoz.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opEchinokokkoz.getId(),128, String.valueOf(opEchinokokkoz.getPoliorganNedostat())));
	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=6";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}


	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 6);
		paramSource.addValue("value", value);
		return paramSource;
	}


	private static final class OpEchinokokkozMapper implements RowMapper<List<OpEchinokokkoz>> {

		public List<OpEchinokokkoz> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<OpEchinokokkoz> list = new ArrayList<OpEchinokokkoz>();
			OpEchinokokkoz opEchinokokkoz = new OpEchinokokkoz();
			int i = 0;
			do {
				opEchinokokkoz.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 1: i++; opEchinokokkoz.setRezekciaType(rs.getInt("value")); break;
					case 2: i++; opEchinokokkoz.setRazdelenieTkanei(rs.getInt("value"));break;
					case 3: if (i!=0 && i!=44) return null;i=1; opEchinokokkoz.setRezekcia(rs.getInt("value")); break;
					case 7: i++; opEchinokokkoz.setDlitop(rs.getString("value")); break;
					case 27: i++; opEchinokokkoz.setOtkrEch(rs.getInt("value"));break;
					case 28: i++; opEchinokokkoz.setObrabotkaFibr(rs.getInt("value"));break;
					case 29: i++; opEchinokokkoz.setObrabotkaSvich(rs.getInt("value"));break;
					case 30: i++; opEchinokokkoz.setChirurgLech(rs.getInt("value"));break;
					case 31: i++; opEchinokokkoz.setEtapLech(rs.getString("value"));break;
					case 32: i++; opEchinokokkoz.setSimOper(rs.getInt("value"));break;
					case 12: i++; opEchinokokkoz.setEchgdc(rs.getInt("value")); break;
					case 13: i++; opEchinokokkoz.setEchgdcmin(rs.getString("value")); break;
					case 21: i++; opEchinokokkoz.setBypass(rs.getInt("value")); break;
					case 22: i++; opEchinokokkoz.setTotIz(rs.getInt("value")); break;
					case 23: i++; opEchinokokkoz.setSelIz(rs.getInt("value")); break;
					case 33: i++; opEchinokokkoz.setIntraOsl(rs.getInt("value")); break;
					case 25: i++; opEchinokokkoz.setOsl(rs.getInt("value")); break;
					case 26: i++; opEchinokokkoz.setClavien(rs.getInt("value")); break;
					case 103: i++; opEchinokokkoz.setVnutrKrovot(rs.getInt("value")); break;
					case 104: i++; opEchinokokkoz.setZhkk(rs.getInt("value")); break;
					case 105: i++; opEchinokokkoz.setPechNedostatA(rs.getInt("value")); break;
					case 106: i++; opEchinokokkoz.setPechNedostatB(rs.getInt("value")); break;
					case 107: i++; opEchinokokkoz.setPechNedostatC(rs.getInt("value")); break;
					case 108: i++; opEchinokokkoz.setNagnoenie(rs.getInt("value")); break;
					case 109: i++; opEchinokokkoz.setCholangit(rs.getInt("value")); break;
					case 110: i++; opEchinokokkoz.setCholAbscedir(rs.getInt("value")); break;
					case 111: i++; opEchinokokkoz.setAscit(rs.getInt("value")); break;
					case 112: i++; opEchinokokkoz.setEventracia(rs.getInt("value")); break;
					case 113: i++; opEchinokokkoz.setKishNeprohod(rs.getInt("value")); break;
					case 114: i++; opEchinokokkoz.setPankreatSvich(rs.getInt("value")); break;
					case 115: i++; opEchinokokkoz.setOstriiSvich(rs.getInt("value")); break;
					case 116: i++; opEchinokokkoz.setSepsis(rs.getInt("value")); break;
					case 117: i++; opEchinokokkoz.setTyazhSepsis(rs.getInt("value")); break;
					case 118: i++; opEchinokokkoz.setSeptShok(rs.getInt("value")); break;
					case 119: i++; opEchinokokkoz.setZhelchSvish(rs.getInt("value")); break;
					case 120: i++; opEchinokokkoz.setKishSvish(rs.getInt("value")); break;
					case 121: i++; opEchinokokkoz.setTrombVorotVeni(rs.getInt("value")); break;
					case 122: i++; opEchinokokkoz.setTrombozPechArter(rs.getInt("value")); break;
					case 123: i++; opEchinokokkoz.setOdnostorPnevm(rs.getInt("value")); break;
					case 124: i++; opEchinokokkoz.setDvustorPnevm(rs.getInt("value")); break;
					case 125: i++; opEchinokokkoz.setGidrotorax(rs.getInt("value")); break;
					case 126: i++; opEchinokokkoz.setPochNedostat(rs.getInt("value")); break;
					case 127: i++; opEchinokokkoz.setSerdSosNedostat(rs.getInt("value")); break;
					case 128: i++; opEchinokokkoz.setPoliorganNedostat(rs.getInt("value")); break;
				}
				if (i == 44) {
					list.add(opEchinokokkoz);
					opEchinokokkoz = new OpEchinokokkoz();
				}
			}while (rs.next());
			return list;
		}
	}
}
