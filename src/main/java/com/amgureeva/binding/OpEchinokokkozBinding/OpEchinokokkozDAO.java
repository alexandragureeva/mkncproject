package com.amgureeva.binding.OpEchinokokkozBinding;

import com.amgureeva.Entities.Surgeries.OpEchinokokkoz;

import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public interface OpEchinokokkozDAO {
	OpEchinokokkoz findById(Integer id);

	List<OpEchinokokkoz> findAll();

	void save(OpEchinokokkoz opEchinokokkoz);

	void update(OpEchinokokkoz opEchinokokkoz);

	void delete(Integer id);
}
