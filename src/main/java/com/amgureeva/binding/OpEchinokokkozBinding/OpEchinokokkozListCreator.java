package com.amgureeva.binding.OpEchinokokkozBinding;

import com.amgureeva.Entities.Surgeries.OpEchinokokkoz;
import com.amgureeva.Entities.Surgeries.OpEchinokokkozList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public class OpEchinokokkozListCreator {

	public List<OpEchinokokkozList> createEchList(List<OpEchinokokkoz> echinokokkozs, PatientService patientService) {
		if (echinokokkozs != null && patientService != null) {
			List<OpEchinokokkozList> opEchinokokkozList = new ArrayList<OpEchinokokkozList>();
			OpEchinokokkozList echinokokkoz = new OpEchinokokkozList();
			for (int i = 0; i < echinokokkozs.size(); i++) {
				echinokokkoz.setName(patientService.findPatientById(Integer.valueOf(echinokokkozs.get(i).getId())).getName());
				switch (Integer.valueOf(echinokokkozs.get(i).getRezekcia())) {
					case 0:
						echinokokkoz.setRezekcia("--");
						break;
					case 1:
						echinokokkoz.setRezekcia("Идеальная эхтнококкэктомия");
						break;
					case 2:
						echinokokkoz.setRezekcia("Открытая эхинококкэктомия");
						break;
					case 3:
						echinokokkoz.setRezekcia("Сочетание обоих методов");
						break;
					case 4:
						echinokokkoz.setRezekcia("Резекция печени");
						break;
				}
				switch (Integer.valueOf(echinokokkozs.get(i).getOtkrEch())) {
					case 0:
						echinokokkoz.setOtkrEch("--");
						break;
					case 1:
						echinokokkoz.setOtkrEch("С тотальной перицисэктомией");
						break;
					case 2:
						echinokokkoz.setOtkrEch("С субтотальной перицистэктомией");
						break;
					case 3:
						echinokokkoz.setOtkrEch("С  частичной перицистэктомией");
						break;
				}
				switch (Integer.valueOf(echinokokkozs.get(i).getObrabotkaFibr())) {
					case 0:
						echinokokkoz.setObrabotkaFibr("--");
						break;
					case 1:
						echinokokkoz.setObrabotkaFibr("Нет");
						break;
					case 2:
						echinokokkoz.setObrabotkaFibr("Капитонаж");
						break;
					case 3:
						echinokokkoz.setObrabotkaFibr("Тампонирование сальником");
						break;
				}
				switch (Integer.valueOf(echinokokkozs.get(i).getObrabotkaSvich())) {
					case 0:
						echinokokkoz.setObrabotkaSvich("--");
						break;
					case 1:
						echinokokkoz.setObrabotkaSvich("Нет");
						break;
					case 2:
						echinokokkoz.setObrabotkaSvich("Ушивание устья в фиброзной капсуле");
						break;
					case 3:
						echinokokkoz.setObrabotkaSvich("Клипирование устья в фиб");
						break;
					case 4:
						echinokokkoz.setObrabotkaSvich("Выделение и перевязка сосудисто-секреторной ножки");
						break;
				}
				switch (Integer.valueOf(echinokokkozs.get(i).getChirurgLech())) {
					case 0:
						echinokokkoz.setChirurgLech("--");
						break;
					case 1:
						echinokokkoz.setChirurgLech("Пункционно-дренажное лечение (PAER)");
						break;
					case 2:
						echinokokkoz.setChirurgLech("Лапроскапическая");
						break;
					case 3:
						echinokokkoz.setChirurgLech("Открытая");
						break;
				}
				switch (Integer.valueOf(echinokokkozs.get(i).getSimOper())) {
					case 0:
						echinokokkoz.setSimOper("--");
						break;
					case 1:
						echinokokkoz.setSimOper("Hет");
						break;
					case 2:
						echinokokkoz.setSimOper("В сочетании с резекцией легкого");
						break;
					case 3:
						echinokokkoz.setSimOper("В сочетании с резекцией др органов брюшной полости");
						break;
					case 4:
						echinokokkoz.setSimOper("В сочетании с удалением внеорганных кист");
						break;
				}
				switch (Integer.valueOf(echinokokkozs.get(i).getIntraOsl())) {
					case 0:
						echinokokkoz.setIntraOsl("--");
						break;
					case 1:
						echinokokkoz.setIntraOsl("Hет");
						break;
					case 2:
						echinokokkoz.setIntraOsl("Анафелоксия с падением АД");
						break;
					case 3:
						echinokokkoz.setIntraOsl("Разрыв кисты с опсеменением брюшной полости");
						break;
				}
				switch (Integer.valueOf(echinokokkozs.get(i).getOsl())) {
					case 0:
						echinokokkoz.setOsl("--");
						break;
					case 1:
						echinokokkoz.setOsl("Внутрибрюшное кровотечение");
						break;
					case 2:
						echinokokkoz.setOsl("ЖКК");
						break;
					case 3:
						echinokokkoz.setOsl("Печеночная недостаточность A");
						break;
					case 4:
						echinokokkoz.setOsl("Печеночная недостаточность B");
						break;
					case 5:
						echinokokkoz.setOsl("Печеночная недостаточность C");
						break;
					case 6:
						echinokokkoz.setOsl("Паренхиматозная желтуха");
						break;
					case 7:
						echinokokkoz.setOsl("Механическая желтуха");
						break;
					case 8:
						echinokokkoz.setOsl("Нагноение раны");
						break;
					case 9:
						echinokokkoz.setOsl("Холангит");
						break;
					case 10:
						echinokokkoz.setOsl("Холангиогенное абсцедирование");
						break;
					case 11:
						echinokokkoz.setOsl("Асцит");
						break;
					case 12:
						echinokokkoz.setOsl("Эвентрация");
						break;
					case 13:
						echinokokkoz.setOsl("Кишечная непроходимость");
						break;
					case 14:
						echinokokkoz.setOsl("Панкреатический свищ");
						break;
					case 15:
						echinokokkoz.setOsl("Острый панкреатит");
						break;
					case 16:
						echinokokkoz.setOsl("Сепсис");
						break;
					case 17:
						echinokokkoz.setOsl("Тяжелый сепсис");
						break;
					case 18:
						echinokokkoz.setOsl("Септический шок");
						break;
					case 19:
						echinokokkoz.setOsl("Желчный свищ");
						break;
					case 20:
						echinokokkoz.setOsl("Кишечный свищ");
						break;
					case 21:
						echinokokkoz.setOsl("Тромбоз воротной вены");
						break;
					case 22:
						echinokokkoz.setOsl("Тромбоз печеночной артерии");
						break;
					case 23:
						echinokokkoz.setOsl("Односторонняя пневмония");
						break;
					case 24:
						echinokokkoz.setOsl("Двусторонняя пневмония");
						break;
					case 25:
						echinokokkoz.setOsl("Гидроторакс");
						break;
					case 26:
						echinokokkoz.setOsl("Почечная недостаточность");
						break;
					case 27:
						echinokokkoz.setOsl("Сердечно-сосудистая недостаточность");
						break;
					case 28:
						echinokokkoz.setOsl("Полиорганная недостаточность");
						break;

				}
				switch (Integer.valueOf(echinokokkozs.get(i).getClavien())) {
					case 0:
						echinokokkoz.setClavien("--");
						break;
					case 1:
						echinokokkoz.setClavien("I");
						break;
					case 2:
						echinokokkoz.setClavien("II");
						break;
					case 3:
						echinokokkoz.setClavien("IIIa");
						break;
					case 4:
						echinokokkoz.setClavien("IIIb");
						break;
					case 5:
						echinokokkoz.setClavien("IVa");
						break;
					case 6:
						echinokokkoz.setClavien("IVb");
						break;
					case 7:
						echinokokkoz.setClavien("V");
						break;
				}
				if (echinokokkozs.get(i).getEtapLech().equals("") || echinokokkozs.get(i).getEtapLech().equals("null") )
					echinokokkoz.setEtapLech("-");
				else
					echinokokkoz.setEtapLech("+");
				if (echinokokkozs.get(i).getEchgdc() == 0)
					echinokokkoz.setGdc("-");
				else
					echinokokkoz.setGdc("+");
				if (echinokokkozs.get(i).getEchgdcmin().equals("") || echinokokkozs.get(i).getEchgdcmin().equals("null"))
					echinokokkoz.setGdcmin("--");
				else
					echinokokkoz.setGdcmin(String.valueOf(echinokokkozs.get(i).getEchgdcmin()));
				if (echinokokkozs.get(i).getBypass() == 0)
					echinokokkoz.setBypass("-");
				else
					echinokokkoz.setBypass("+");
				if (echinokokkozs.get(i).getTotIz() == 0)
					echinokokkoz.setTotIz("-");
				else
					echinokokkoz.setTotIz("+");
				if (echinokokkozs.get(i).getSelIz() == 0)
					echinokokkoz.setSelIz("-");
				else
					echinokokkoz.setSelIz("+");
				opEchinokokkozList.add(echinokokkoz);
				echinokokkoz = new OpEchinokokkozList();
			}
			return opEchinokokkozList;
		}
		return null;
	}
}

