package com.amgureeva.binding.OpAlveokokkozBinding;

import com.amgureeva.Entities.Surgeries.OpAlveokokkoz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 21.05.2016.
 */
@Repository
public class OpAlveokokkozDAOImpl implements OpAlveokokkozDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public OpAlveokokkoz findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=7";

			List<OpAlveokokkoz> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpAlveokokkozDAOImpl.OpAlveokokkozMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}

	@Override
	public List<OpAlveokokkoz> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 7";
		List<List<OpAlveokokkoz>> result = namedParameterJdbcTemplate.query(sql,
				new OpAlveokokkozDAOImpl.OpAlveokokkozMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(OpAlveokokkoz opAlveokokkoz) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),3, String.valueOf(opAlveokokkoz.getRezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),4, String.valueOf(opAlveokokkoz.getAlvsosudrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),5, String.valueOf(opAlveokokkoz.getAlvvorotrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),6, String.valueOf(opAlveokokkoz.getAlvkrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),7, String.valueOf(opAlveokokkoz.getAlvdlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),9, String.valueOf(opAlveokokkoz.getAlvgemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),10, String.valueOf(opAlveokokkoz.getAlvgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),11, String.valueOf(opAlveokokkoz.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),12, String.valueOf(opAlveokokkoz.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),13, String.valueOf(opAlveokokkoz.getAlvgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),14, String.valueOf(opAlveokokkoz.getGeatype1())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),15, String.valueOf(opAlveokokkoz.getGeatype2())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),16, String.valueOf(opAlveokokkoz.getGeatype3())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),17, String.valueOf(opAlveokokkoz.getRadikal())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),18, String.valueOf(opAlveokokkoz.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),19, String.valueOf(opAlveokokkoz.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),20, String.valueOf(opAlveokokkoz.getPnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),21, String.valueOf(opAlveokokkoz.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),22, String.valueOf(opAlveokokkoz.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),23, String.valueOf(opAlveokokkoz.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),24, String.valueOf(opAlveokokkoz.getRezDiafragmi())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),25, String.valueOf(opAlveokokkoz.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),26, String.valueOf(opAlveokokkoz.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),1, String.valueOf(opAlveokokkoz.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),2, String.valueOf(opAlveokokkoz.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),103, String.valueOf(opAlveokokkoz.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),104, String.valueOf(opAlveokokkoz.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),105, String.valueOf(opAlveokokkoz.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),106, String.valueOf(opAlveokokkoz.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),107, String.valueOf(opAlveokokkoz.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),108, String.valueOf(opAlveokokkoz.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),109, String.valueOf(opAlveokokkoz.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),110, String.valueOf(opAlveokokkoz.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),111, String.valueOf(opAlveokokkoz.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),112, String.valueOf(opAlveokokkoz.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),113, String.valueOf(opAlveokokkoz.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),114, String.valueOf(opAlveokokkoz.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),115, String.valueOf(opAlveokokkoz.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),116, String.valueOf(opAlveokokkoz.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),117, String.valueOf(opAlveokokkoz.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),118, String.valueOf(opAlveokokkoz.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),119, String.valueOf(opAlveokokkoz.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),120, String.valueOf(opAlveokokkoz.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),121, String.valueOf(opAlveokokkoz.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),122, String.valueOf(opAlveokokkoz.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),123, String.valueOf(opAlveokokkoz.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),124, String.valueOf(opAlveokokkoz.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),125, String.valueOf(opAlveokokkoz.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),126, String.valueOf(opAlveokokkoz.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),127, String.valueOf(opAlveokokkoz.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),128, String.valueOf(opAlveokokkoz.getPoliorganNedostat())));

	}

	@Override
	public void update(OpAlveokokkoz opAlveokokkoz) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=7 and parameterId=:parameterId";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),3, String.valueOf(opAlveokokkoz.getRezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),4, String.valueOf(opAlveokokkoz.getAlvsosudrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),5, String.valueOf(opAlveokokkoz.getAlvvorotrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),6, String.valueOf(opAlveokokkoz.getAlvkrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),7, String.valueOf(opAlveokokkoz.getAlvdlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),9, String.valueOf(opAlveokokkoz.getAlvgemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),10, String.valueOf(opAlveokokkoz.getAlvgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),11, String.valueOf(opAlveokokkoz.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),12, String.valueOf(opAlveokokkoz.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),13, String.valueOf(opAlveokokkoz.getAlvgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),14, String.valueOf(opAlveokokkoz.getGeatype1())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),15, String.valueOf(opAlveokokkoz.getGeatype2())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),16, String.valueOf(opAlveokokkoz.getGeatype3())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),17, String.valueOf(opAlveokokkoz.getRadikal())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),18, String.valueOf(opAlveokokkoz.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),19, String.valueOf(opAlveokokkoz.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),20, String.valueOf(opAlveokokkoz.getPnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),21, String.valueOf(opAlveokokkoz.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),22, String.valueOf(opAlveokokkoz.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),23, String.valueOf(opAlveokokkoz.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),24, String.valueOf(opAlveokokkoz.getRezDiafragmi())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),25, String.valueOf(opAlveokokkoz.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),26, String.valueOf(opAlveokokkoz.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),1, String.valueOf(opAlveokokkoz.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),2, String.valueOf(opAlveokokkoz.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),103, String.valueOf(opAlveokokkoz.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),104, String.valueOf(opAlveokokkoz.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),105, String.valueOf(opAlveokokkoz.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),106, String.valueOf(opAlveokokkoz.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),107, String.valueOf(opAlveokokkoz.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),108, String.valueOf(opAlveokokkoz.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),109, String.valueOf(opAlveokokkoz.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),110, String.valueOf(opAlveokokkoz.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),111, String.valueOf(opAlveokokkoz.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),112, String.valueOf(opAlveokokkoz.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),113, String.valueOf(opAlveokokkoz.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),114, String.valueOf(opAlveokokkoz.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),115, String.valueOf(opAlveokokkoz.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),116, String.valueOf(opAlveokokkoz.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),117, String.valueOf(opAlveokokkoz.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),118, String.valueOf(opAlveokokkoz.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),119, String.valueOf(opAlveokokkoz.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),120, String.valueOf(opAlveokokkoz.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),121, String.valueOf(opAlveokokkoz.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),122, String.valueOf(opAlveokokkoz.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),123, String.valueOf(opAlveokokkoz.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),124, String.valueOf(opAlveokokkoz.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),125, String.valueOf(opAlveokokkoz.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),126, String.valueOf(opAlveokokkoz.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),127, String.valueOf(opAlveokokkoz.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opAlveokokkoz.getId(),128, String.valueOf(opAlveokokkoz.getPoliorganNedostat())));
	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=7";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}


	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 7);
		paramSource.addValue("value", value);
		return paramSource;
	}


	private static final class OpAlveokokkozMapper implements RowMapper<List<OpAlveokokkoz>> {

		public List<OpAlveokokkoz> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<OpAlveokokkoz> alList = new ArrayList<OpAlveokokkoz>();
			OpAlveokokkoz opAlveokokkoz = new OpAlveokokkoz();
			int i = 0;
			do {
				opAlveokokkoz.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 1: i++; opAlveokokkoz.setRezekciaType(rs.getInt("value")); break;
					case 2: i++; opAlveokokkoz.setRazdelenieTkanei(rs.getInt("value"));break;
					case 3: if (i!=0 && i!=51) return null;i=1; opAlveokokkoz.setRezekcia(rs.getInt("value")); break;
					case 4: i++; opAlveokokkoz.setAlvsosudrezekcia(rs.getInt("value"));break;
					case 5: i++; opAlveokokkoz.setAlvvorotrezekcia(rs.getInt("value"));break;
					case 6: i++; opAlveokokkoz.setAlvkrovopoteria(rs.getInt("value"));break;
					case 7: i++; opAlveokokkoz.setAlvdlitop(rs.getString("value"));break;
					case 9: i++; opAlveokokkoz.setAlvgemotransfusia(rs.getInt("value"));break;
					case 10: i++; opAlveokokkoz.setAlvgemotransfusia_ml
							((rs.getString("value").equals("null"))?null:rs.getString("value"));break;
					case 11: i++; opAlveokokkoz.setPlazma(rs.getString("value")); break;
					case 12: i++; opAlveokokkoz.setGdc(rs.getInt("value")); break;
					case 13: i++; opAlveokokkoz.setAlvgdcmin
							((rs.getString("value").equals("null"))?null:rs.getString("value")); break;
					case 14: i++; opAlveokokkoz.setGeatype1(rs.getInt("value")); break;
					case 15: i++; opAlveokokkoz.setGeatype2(rs.getInt("value")); break;
					case 16: i++; opAlveokokkoz.setGeatype3(rs.getInt("value")); break;
					case 17: i++; opAlveokokkoz.setRadikal(rs.getInt("value")); break;
					case 18: i++; opAlveokokkoz.setLimfodissekcia(rs.getInt("value")); break;
					case 19: i++; opAlveokokkoz.setTnm(rs.getInt("value")); break;
					case 20: i++; opAlveokokkoz.setPnm(rs.getInt("value")); break;
					case 21: i++; opAlveokokkoz.setBypass(rs.getInt("value")); break;
					case 22: i++; opAlveokokkoz.setTotIz(rs.getInt("value")); break;
					case 23: i++; opAlveokokkoz.setSelIz(rs.getInt("value")); break;
					case 24: i++; opAlveokokkoz.setRezDiafragmi(rs.getInt("value")); break;
					case 25: i++; opAlveokokkoz.setOsl(rs.getInt("value")); break;
					case 26: i++; opAlveokokkoz.setClavien(rs.getInt("value")); break;
					case 103: i++; opAlveokokkoz.setVnutrKrovot(rs.getInt("value")); break;
					case 104: i++; opAlveokokkoz.setZhkk(rs.getInt("value")); break;
					case 105: i++; opAlveokokkoz.setPechNedostatA(rs.getInt("value")); break;
					case 106: i++; opAlveokokkoz.setPechNedostatB(rs.getInt("value")); break;
					case 107: i++; opAlveokokkoz.setPechNedostatC(rs.getInt("value")); break;
					case 108: i++; opAlveokokkoz.setNagnoenie(rs.getInt("value")); break;
					case 109: i++; opAlveokokkoz.setCholangit(rs.getInt("value")); break;
					case 110: i++; opAlveokokkoz.setCholAbscedir(rs.getInt("value")); break;
					case 111: i++; opAlveokokkoz.setAscit(rs.getInt("value")); break;
					case 112: i++; opAlveokokkoz.setEventracia(rs.getInt("value")); break;
					case 113: i++; opAlveokokkoz.setKishNeprohod(rs.getInt("value")); break;
					case 114: i++; opAlveokokkoz.setPankreatSvich(rs.getInt("value")); break;
					case 115: i++; opAlveokokkoz.setOstriiSvich(rs.getInt("value")); break;
					case 116: i++; opAlveokokkoz.setSepsis(rs.getInt("value")); break;
					case 117: i++; opAlveokokkoz.setTyazhSepsis(rs.getInt("value")); break;
					case 118: i++; opAlveokokkoz.setSeptShok(rs.getInt("value")); break;
					case 119: i++; opAlveokokkoz.setZhelchSvish(rs.getInt("value")); break;
					case 120: i++; opAlveokokkoz.setKishSvish(rs.getInt("value")); break;
					case 121: i++; opAlveokokkoz.setTrombVorotVeni(rs.getInt("value")); break;
					case 122: i++; opAlveokokkoz.setTrombozPechArter(rs.getInt("value")); break;
					case 123: i++; opAlveokokkoz.setOdnostorPnevm(rs.getInt("value")); break;
					case 124: i++; opAlveokokkoz.setDvustorPnevm(rs.getInt("value")); break;
					case 125: i++; opAlveokokkoz.setGidrotorax(rs.getInt("value")); break;
					case 126: i++; opAlveokokkoz.setPochNedostat(rs.getInt("value")); break;
					case 127: i++; opAlveokokkoz.setSerdSosNedostat(rs.getInt("value")); break;
					case 128: i++; opAlveokokkoz.setPoliorganNedostat(rs.getInt("value")); break;
				}
				if (i == 51) {
					alList.add(opAlveokokkoz);
					opAlveokokkoz = new OpAlveokokkoz();
				}
			}while (rs.next());
			return alList;
		}
	}
}