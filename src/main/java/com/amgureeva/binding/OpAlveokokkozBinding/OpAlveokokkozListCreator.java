package com.amgureeva.binding.OpAlveokokkozBinding;

import com.amgureeva.Entities.Surgeries.OpAlveokokkoz;
import com.amgureeva.Entities.Surgeries.OpAlveokokkozList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;



public class OpAlveokokkozListCreator {
	public List<OpAlveokokkozList> createAlveokokkozList (List<OpAlveokokkoz> alveokokkozs, PatientService patientService) {
		if (alveokokkozs != null && patientService != null) {
			List<OpAlveokokkozList> alveokokkozList = new ArrayList<OpAlveokokkozList>();
			OpAlveokokkozList alveokokkozList_ = new OpAlveokokkozList();
			for (int i = 0; i < alveokokkozs.size(); i++) {
				alveokokkozList_.setName(patientService.findPatientById(Integer.valueOf(alveokokkozs.get(i).getId())).getName());
				switch (Integer.valueOf(alveokokkozs.get(i).getRezekcia())) {
					case 0:
						alveokokkozList_.setRezekcia("--");
						break;
					case 1:
						alveokokkozList_.setRezekcia("ЛГГ");
						break;
					case 2:
						alveokokkozList_.setRezekcia("ПГГ");
						break;
					case 3:
						alveokokkozList_.setRezekcia("Расширенная ПГГ");
						break;
					case 4:
						alveokokkozList_.setRezekcia("Расширенная ЛГГ");
						break;
					case 5:
						alveokokkozList_.setRezekcia("Секторэктомия");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getAlvsosudrezekcia())) {
					case 0:
						alveokokkozList_.setSosudrezekcia("--");
						break;
					case 1:
						alveokokkozList_.setSosudrezekcia("Резекция воротной вены");
						break;
					case 2:
						alveokokkozList_.setSosudrezekcia("Резекция печеночной артерии");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getAlvvorotrezekcia())) {
					case 0:
						alveokokkozList_.setVorotrezekcia("--");
						break;
					case 1:
						alveokokkozList_.setVorotrezekcia("Краевая");
						break;
					case 2:
						alveokokkozList_.setVorotrezekcia("Циркулярная без протезирования");
						break;
					case 3:
						alveokokkozList_.setVorotrezekcia("Циркулярная с протезированием");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getAlvkrovopoteria())) {
					case 0:
						alveokokkozList_.setKrovopoteria("--");
						break;
					case 1:
						alveokokkozList_.setKrovopoteria("<500");
						break;
					case 2:
						alveokokkozList_.setKrovopoteria("500-1000");
						break;
					case 3:
						alveokokkozList_.setKrovopoteria(">1000");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getGeatype1())) {
					case 0:
						alveokokkozList_.setGeatype1("--");
						break;
					case 1:
						alveokokkozList_.setGeatype1("Единый");
						break;
					case 2:
						alveokokkozList_.setGeatype1("Раздельный");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getGeatype2())) {
					case 0:
						alveokokkozList_.setGeatype2("--");
						break;
					case 1:
						alveokokkozList_.setGeatype2("Моно-ГЕА");
						break;
					case 2:
						alveokokkozList_.setGeatype2("Би-ГЕА");
						break;
					case 3:
						alveokokkozList_.setGeatype2("Три-ГЕА");
						break;
					case 4:
						alveokokkozList_.setGeatype2("Тетра-ГЕА");
						break;
					case 5:
						alveokokkozList_.setGeatype2("Пента-ГЕА");
						break;
					case 6:
						alveokokkozList_.setGeatype2("Мульти-ГЕА");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getGeatype3())) {
					case 0:
						alveokokkozList_.setGeatype3("--");
						break;
					case 1:
						alveokokkozList_.setGeatype3("С ТПД");
						break;
					case 2:
						alveokokkozList_.setGeatype3("Без ТПД");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getRadikal())) {
					case 0:
						alveokokkozList_.setRadikal("--");
						break;
					case 1:
						alveokokkozList_.setRadikal("R0");
						break;
					case 2:
						alveokokkozList_.setRadikal("R1");
						break;
					case 3:
						alveokokkozList_.setRadikal("R2");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getLimfodissekcia())) {
					case 0:
						alveokokkozList_.setLimfodissekcia("--");
						break;
					case 1:
						alveokokkozList_.setLimfodissekcia("ГДС");
						break;
					case 2:
						alveokokkozList_.setLimfodissekcia("ГДС+ОПА");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getTnm())) {
					case 0:
						alveokokkozList_.setTnm("--");
						break;
					case 1:
						alveokokkozList_.setTnm("T1N0M0");
						break;
					case 2:
						alveokokkozList_.setTnm("T2aN0M0");
						break;
					case 3:
						alveokokkozList_.setTnm("T2bN0M0");
						break;
					case 4:
						alveokokkozList_.setTnm("T3N0M0");
						break;
					case 5:
						alveokokkozList_.setTnm("T1N1M0");
						break;
					case 6:
						alveokokkozList_.setTnm("T2N1M0");
						break;
					case 7:
						alveokokkozList_.setTnm("T3N1M0");
						break;
					case 8:
						alveokokkozList_.setTnm("T4N0M0");
						break;
					case 9:
						alveokokkozList_.setTnm("T4N1M0");
						break;
					case 10:
						alveokokkozList_.setTnm("Любая T, N2M0");
						break;
					case 11:
						alveokokkozList_.setTnm("Любая T, любая N, M1");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getPnm())) {
					case 0:
						alveokokkozList_.setPnm("--");
						break;
					case 1:
						alveokokkozList_.setPnm("Стадия I");
						break;
					case 2:
						alveokokkozList_.setPnm("Стадия II");
						break;
					case 3:
						alveokokkozList_.setPnm("Стадия III");
						break;
					case 4:
						alveokokkozList_.setPnm("Стадия IV");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getOsl())) {
					case 0:
						alveokokkozList_.setOsl("--");
						break;
					case 1:
						alveokokkozList_.setOsl("Внутрибрюшное кровотечение");
						break;
					case 2:
						alveokokkozList_.setOsl("ЖКК");
						break;
					case 3:
						alveokokkozList_.setOsl("Печеночная недостаточность A");
						break;
					case 4:
						alveokokkozList_.setOsl("Печеночная недостаточность B");
						break;
					case 5:
						alveokokkozList_.setOsl("Печеночная недостаточность C");
						break;
					case 6:
						alveokokkozList_.setOsl("Паренхиматозная желтуха");
						break;
					case 7:
						alveokokkozList_.setOsl("Механическая желтуха");
						break;
					case 8:
						alveokokkozList_.setOsl("Нагноение раны");
						break;
					case 9:
						alveokokkozList_.setOsl("Холангит");
						break;
					case 10:
						alveokokkozList_.setOsl("Холангиогенное абсцедирование");
						break;
					case 11:
						alveokokkozList_.setOsl("Асцит");
						break;
					case 12:
						alveokokkozList_.setOsl("Эвентрация");
						break;
					case 13:
						alveokokkozList_.setOsl("Кишечная непроходимость");
						break;
					case 14:
						alveokokkozList_.setOsl("Панкреатический свищ");
						break;
					case 15:
						alveokokkozList_.setOsl("Острый панкреатит");
						break;
					case 16:
						alveokokkozList_.setOsl("Сепсис");
						break;
					case 17:
						alveokokkozList_.setOsl("Тяжелый сепсис");
						break;
					case 18:
						alveokokkozList_.setOsl("Септический шок");
						break;
					case 19:
						alveokokkozList_.setOsl("Желчный свищ");
						break;
					case 20:
						alveokokkozList_.setOsl("Кишечный свищ");
						break;
					case 21:
						alveokokkozList_.setOsl("Тромбоз воротной вены");
						break;
					case 22:
						alveokokkozList_.setOsl("Тромбоз печеночной артерии");
						break;
					case 23:
						alveokokkozList_.setOsl("Односторонняя пневмония");
						break;
					case 24:
						alveokokkozList_.setOsl("Двусторонняя пневмония");
						break;
					case 25:
						alveokokkozList_.setOsl("Гидроторакс");
						break;
					case 26:
						alveokokkozList_.setOsl("Почечная недостаточность");
						break;
					case 27:
						alveokokkozList_.setOsl("Сердечно-сосудистая недостаточность");
						break;
					case 28:
						alveokokkozList_.setOsl("Полиорганная недостаточность");
						break;

				}
				switch (Integer.valueOf(alveokokkozs.get(i).getClavien())) {
					case 0:
						alveokokkozList_.setClavien("--");
						break;
					case 1:
						alveokokkozList_.setClavien("I");
						break;
					case 2:
						alveokokkozList_.setClavien("II");
						break;
					case 3:
						alveokokkozList_.setClavien("IIIa");
						break;
					case 4:
						alveokokkozList_.setClavien("IIIb");
						break;
					case 5:
						alveokokkozList_.setClavien("IVa");
						break;
					case 6:
						alveokokkozList_.setClavien("IVb");
						break;
					case 7:
						alveokokkozList_.setClavien("V");
						break;
				}
				if (alveokokkozs.get(i).getAlvdlitop()==null || alveokokkozs.get(i).getAlvdlitop().isEmpty())
					alveokokkozList_.setDlitop("--");
				else
					alveokokkozList_.setDlitop(String.valueOf(alveokokkozs.get(i).getAlvdlitop()));
				if (alveokokkozs.get(i).getAlvgemotransfusia() == 0)
					alveokokkozList_.setGemotransfusia("-");
				else
					alveokokkozList_.setGemotransfusia("+");
				if (alveokokkozs.get(i).getAlvgemotransfusia_ml()==null ||
						alveokokkozs.get(i).getAlvgemotransfusia_ml().isEmpty())
					alveokokkozList_.setGemotransfusia_ml("--");
				else
					alveokokkozList_.setGemotransfusia_ml(String.valueOf(alveokokkozs.get(i).getAlvgemotransfusia_ml()));
				if (alveokokkozs.get(i).getPlazma() == null || alveokokkozs.get(i).getPlazma().isEmpty())
					alveokokkozList_.setPlazma("--");
				else
					alveokokkozList_.setPlazma(String.valueOf(alveokokkozs.get(i).getPlazma()));
				if (alveokokkozs.get(i).getGdc() == 0)
					alveokokkozList_.setGdc("-");
				else
					alveokokkozList_.setGdc("+");
				if (alveokokkozs.get(i).getAlvgdcmin()==null ||
						alveokokkozs.get(i).getAlvgdcmin().isEmpty())
					alveokokkozList_.setGdcmin("--");
				else
					alveokokkozList_.setGdcmin(String.valueOf(alveokokkozs.get(i).getAlvgdcmin()));
				if (alveokokkozs.get(i).getBypass() == 0)
					alveokokkozList_.setBypass("-");
				else
					alveokokkozList_.setBypass("+");
				if (alveokokkozs.get(i).getTotIz() == 0)
					alveokokkozList_.setTotIz("-");
				else
					alveokokkozList_.setTotIz("+");
				if (alveokokkozs.get(i).getSelIz() == 0)
					alveokokkozList_.setSelIz("-");
				else
					alveokokkozList_.setSelIz("+");
				if (alveokokkozs.get(i).getRezDiafragmi() == 0)
					alveokokkozList_.setRezDiafragmi("-");
				else
					alveokokkozList_.setRezDiafragmi("+");
				alveokokkozList.add(alveokokkozList_);
				alveokokkozList_ = new OpAlveokokkozList();
			}
			return alveokokkozList;
		}
		return null;
	}
}
