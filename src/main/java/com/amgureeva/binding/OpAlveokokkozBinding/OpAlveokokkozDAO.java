package com.amgureeva.binding.OpAlveokokkozBinding;

import com.amgureeva.Entities.Surgeries.OpAlveokokkoz;

import java.util.List;

/**
 * Created by Nikita on 21.05.2016.
 */
public interface OpAlveokokkozDAO {

	OpAlveokokkoz findById(Integer id);

	List<OpAlveokokkoz> findAll();

	void save(OpAlveokokkoz opAlveokokkoz);

	void update(OpAlveokokkoz opAlveokokkoz);

	void delete(Integer id);
}
