package com.amgureeva.binding.OpZHKBBinding;

import com.amgureeva.Entities.Surgeries.OpZhkb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 19.09.2016.
 */
@Repository
public class OpZHKBDAOImpl implements OpZHKBDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public OpZhkb findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=8";

			List<OpZhkb> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpZHKBDAOImpl.OpZhkbMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}

	@Override
	public List<OpZhkb> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 8";
		List<List<OpZhkb>> result = namedParameterJdbcTemplate.query(sql,
				new OpZHKBDAOImpl.OpZhkbMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(OpZhkb opZhkb) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),1, String.valueOf(opZhkb.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),2, String.valueOf(opZhkb.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),26, String.valueOf(opZhkb.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),7, String.valueOf(opZhkb.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),82, String.valueOf(opZhkb.getCholecistekt())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),83, String.valueOf(opZhkb.getDrenirovanie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),84, String.valueOf(opZhkb.getDrenMethod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),85, String.valueOf(opZhkb.getPrimbram())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),86, String.valueOf(opZhkb.getInfiltrat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),87, String.valueOf(opZhkb.getVnutrRaspol())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),88, String.valueOf(opZhkb.getZhelchPuz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),89, String.valueOf(opZhkb.getKamen())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),90, String.valueOf(opZhkb.getSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),91, String.valueOf(opZhkb.getSvishType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),92, String.valueOf(opZhkb.getMirizi())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),93, String.valueOf(opZhkb.getMiriziZavershenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),94, String.valueOf(opZhkb.getPerfor())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),95, String.valueOf(opZhkb.getPovrezhdeniePologo())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),96, String.valueOf(opZhkb.getPovrezhdeniePologoType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),97, String.valueOf(opZhkb.getPovrezhdCholed())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),98, String.valueOf(opZhkb.getPovrezhdCholedType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),99, String.valueOf(opZhkb.getPovrezhdArterii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),100, String.valueOf(opZhkb.getPovrezhdVeni())));

	}

	@Override
	public void update(OpZhkb opZhkb) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=8 and parameterId=:parameterId";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),1, String.valueOf(opZhkb.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),2, String.valueOf(opZhkb.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),26, String.valueOf(opZhkb.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),7, String.valueOf(opZhkb.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),82, String.valueOf(opZhkb.getCholecistekt())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),83, String.valueOf(opZhkb.getDrenirovanie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),84, String.valueOf(opZhkb.getDrenMethod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),85, String.valueOf(opZhkb.getPrimbram())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),86, String.valueOf(opZhkb.getInfiltrat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),87, String.valueOf(opZhkb.getVnutrRaspol())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),88, String.valueOf(opZhkb.getZhelchPuz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),89, String.valueOf(opZhkb.getKamen())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),90, String.valueOf(opZhkb.getSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),91, String.valueOf(opZhkb.getSvishType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),92, String.valueOf(opZhkb.getMirizi())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),93, String.valueOf(opZhkb.getMiriziZavershenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),94, String.valueOf(opZhkb.getPerfor())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),95, String.valueOf(opZhkb.getPovrezhdeniePologo())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),96, String.valueOf(opZhkb.getPovrezhdeniePologoType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),97, String.valueOf(opZhkb.getPovrezhdCholed())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),98, String.valueOf(opZhkb.getPovrezhdCholedType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),99, String.valueOf(opZhkb.getPovrezhdArterii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opZhkb.getId(),100, String.valueOf(opZhkb.getPovrezhdVeni())));
	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=8";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}


	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 8);
		paramSource.addValue("value", value);
		return paramSource;
	}


	private static final class OpZhkbMapper implements RowMapper<List<OpZhkb>> {

		public List<OpZhkb> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<OpZhkb> list = new ArrayList<OpZhkb>();
			OpZhkb opZhkb = new OpZhkb();
			int i = 0;
			do {
				opZhkb.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 1: if (i!=0 && i!=23) return null;i=1;  opZhkb.setRezekciaType(rs.getInt("value"));break;
					case 2: i++; opZhkb.setRazdelenieTkanei(rs.getInt("value"));break;
					case 7: i++; opZhkb.setDlitop(rs.getString("value"));break;
					case 26: i++; opZhkb.setClavien(rs.getInt("value")); break;
					case 82: i++; opZhkb.setCholecistekt(rs.getInt("value")); break;
					case 83: i++; opZhkb.setDrenirovanie(rs.getInt("value")); break;
					case 84: i++; opZhkb.setDrenMethod(rs.getInt("value")); break;
					case 85: i++; opZhkb.setPrimbram(rs.getInt("value")); break;
					case 86: i++; opZhkb.setInfiltrat(rs.getInt("value")); break;
					case 87: i++; opZhkb.setVnutrRaspol(rs.getInt("value")); break;
					case 88: i++; opZhkb.setZhelchPuz(rs.getInt("value")); break;
					case 89: i++; opZhkb.setKamen(rs.getInt("value")); break;
					case 90: i++; opZhkb.setSvish(rs.getInt("value")); break;
					case 91: i++; opZhkb.setSvishType(rs.getInt("value")); break;
					case 92: i++; opZhkb.setMirizi(rs.getInt("value")); break;
					case 93: i++; opZhkb.setMiriziZavershenie(rs.getInt("value")); break;
					case 94: i++; opZhkb.setPerfor(rs.getInt("value")); break;
					case 95: i++; opZhkb.setPovrezhdeniePologo(rs.getInt("value")); break;
					case 96: i++; opZhkb.setPovrezhdeniePologoType(rs.getInt("value")); break;
					case 97: i++; opZhkb.setPovrezhdCholed(rs.getInt("value")); break;
					case 98: i++; opZhkb.setPovrezhdCholedType(rs.getInt("value")); break;
					case 99: i++; opZhkb.setPovrezhdArterii(rs.getInt("value")); break;
					case 100: i++; opZhkb.setPovrezhdVeni(rs.getInt("value")); break;
				}
				if (i == 23) {
					list.add(opZhkb);
					opZhkb = new OpZhkb();
				}
			}while (rs.next());
			return list;
		}
	}
}