package com.amgureeva.binding.OpZHKBBinding;

import com.amgureeva.Entities.Surgeries.OpZhkb;

import java.util.List;

/**
 * Created by Nikita on 19.09.2016.
 */
public interface OpZHKBDAO {

	OpZhkb findById(Integer id);

	List<OpZhkb> findAll();

	void save(OpZhkb opRZHP);

	void update(OpZhkb opRZHP);

	void delete(Integer id);
}
