package com.amgureeva.binding.HCRBinding;

import com.amgureeva.Entities.Diagnosis.HCR;

import java.util.List;

/**
 * Created by Александра on 28.04.2016.
 */
public interface HCRDAO {
    HCR findById(Integer id, boolean isFirstDiagnosis);

    List<HCR> findAll();

    void save(HCR patient);

    void update(HCR patient);

    void delete(Integer id);
}
