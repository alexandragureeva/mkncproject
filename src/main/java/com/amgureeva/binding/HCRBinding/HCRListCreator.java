package com.amgureeva.binding.HCRBinding;

import com.amgureeva.Entities.Diagnosis.HCR;
import com.amgureeva.Entities.Diagnosis.HCRList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 05.05.2016.
 */
public class HCRListCreator {
        public List<HCRList> createHCRList (List<HCR> hcrs, PatientService patientService) {
            if (hcrs != null && patientService != null) {
                List<HCRList> hcrList = new ArrayList<HCRList>();
                HCRList hcrList_ = new HCRList();
                for (int i = 0; i < hcrs.size(); i++) {
                    hcrList_.setName(patientService.findPatientById(Integer.valueOf(hcrs.get(i).getId())).getName());
                    switch (Integer.valueOf(hcrs.get(i).getRazmerOpucholi())) {
                        case 0:
                            hcrList_.setRazmerOpucholi("--");
                            break;
                        case 1:
                            hcrList_.setRazmerOpucholi("<10см");
                            break;
                        case 2:
                            hcrList_.setRazmerOpucholi(">10см");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getOpuchUzliType())) {
                        case 0:
                            hcrList_.setOpuchUzliType("--");
                            break;
                        case 1:
                            hcrList_.setOpuchUzliType("Солитарный");
                            break;
                        case 2:
                            hcrList_.setOpuchUzliType("Множественные");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getChisloOpuchUzlov())) {
                        case 0:
                            hcrList_.setChisloOpuchUzlov("--");
                            break;
                        case 1:
                            hcrList_.setChisloOpuchUzlov("0");
                            break;
                        case 2:
                            hcrList_.setChisloOpuchUzlov("1");
                            break;
                        case 3:
                            hcrList_.setChisloOpuchUzlov("2");
                            break;
                        case 4:
                            hcrList_.setChisloOpuchUzlov("3");
                            break;
                        case 5:
                            hcrList_.setChisloOpuchUzlov("4");
                            break;
                        case 6:
                            hcrList_.setChisloOpuchUzlov("5");
                            break;
                        case 7:
                            hcrList_.setChisloOpuchUzlov(">5");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getGrwr())) {
                        case 0:
                            hcrList_.setGrwr("--");
                            break;
                        case 1:
                            hcrList_.setGrwr("<0.8");
                            break;
                        case 2:
                            hcrList_.setGrwr("0.8-1");
                            break;
                        case 3:
                            hcrList_.setGrwr(">1");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getIcg())) {
                        case 0:
                            hcrList_.setIcg("--");
                            break;
                        case 1:
                            hcrList_.setIcg("<15%");
                            break;
                        case 2:
                            hcrList_.setIcg("15-25%");
                            break;
                        case 3:
                            hcrList_.setIcg(">25%");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getIcgk_f())) {
                        case 0:
                            hcrList_.setIcgk_f("--");
                            break;
                        case 1:
                            hcrList_.setIcgk_f("<0.4");
                            break;
                        case 2:
                            hcrList_.setIcgk_f("0.4-0.6");
                            break;
                        case 3:
                            hcrList_.setIcgk_f(">0.6");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getVnepOchagiType())) {
                        case 0:
                            hcrList_.setVnepOchagiType("--");
                            break;
                        case 1:
                            hcrList_.setVnepOchagiType("нет");
                            break;
                        case 2:
                            hcrList_.setVnepOchagiType("Отдаленные метастазы");
                            break;
                        case 3:
                            hcrList_.setVnepOchagiType("Прорастание соседних органов");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getTrombPechType())) {
                        case 0:
                            hcrList_.setTrombPechType("--");
                            break;
                        case 1:
                            hcrList_.setTrombPechType("Нет");
                            break;
                        case 2:
                            hcrList_.setTrombPechType("Да");
                            break;
                        case 3:
                            hcrList_.setTrombPechType("С выходом в нижнюю полую вену");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getTrombozVorotVeni())) {
                        case 0:
                            hcrList_.setTrombozVorotVeni("--");
                            break;
                        case 1:
                            hcrList_.setTrombozVorotVeni("0");
                            break;
                        case 2:
                            hcrList_.setTrombozVorotVeni("1");
                            break;
                        case 3:
                            hcrList_.setTrombozVorotVeni("2");
                            break;
                        case 4:
                            hcrList_.setTrombozVorotVeni("3");
                            break;
                        case 5:
                            hcrList_.setTrombozVorotVeni("4");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getTrofStatus())) {
                        case 0:
                            hcrList_.setTrofStatus("--");
                            break;
                        case 1:
                            hcrList_.setTrofStatus("Хороший");
                            break;
                        case 2:
                            hcrList_.setTrofStatus("Удовлетворительный");
                            break;
                        case 3:
                            hcrList_.setTrofStatus("Неудовлетворительный");
                            break;
                    }
                    switch (Integer.valueOf(hcrs.get(i).getSosudInvasia())) {
                        case 0:
                            hcrList_.setSosudInvasia("--");
                            break;
                        case 1:
                            hcrList_.setSosudInvasia("нет");
                            break;

                        case 2:
                            hcrList_.setSosudInvasia("Микрососудистая");
                            break;
                        case 3:
                            hcrList_.setSosudInvasia("Мaкрососудистая");
                            break;
                    }
                    if (hcrs.get(i).getIndexZachvata() == null)
                        hcrList_.setIndexZachvata("--");
                    else
                        hcrList_.setIndexZachvata(hcrs.get(i).getIndexZachvata());
                    if (hcrs.get(i).getObjemOstatka() == null)
                        hcrList_.setObjemOstatka("--");
                    else
                        hcrList_.setObjemOstatka(hcrs.get(i).getObjemOstatka());
                    if (hcrs.get(i).isProtivTerapia() == false)
                        hcrList_.setProtivTerapia("-");
                    else
                        hcrList_.setProtivTerapia("+");
                    hcrList.add(hcrList_);
                    hcrList_ = new HCRList();
                }

                return hcrList;
            }
            return null;
        }
    public static String getAllVariants(String s) {
        s = s.replace("_ПОСТ", "");
        s = s.replace("_ВЫПИС", "");
        s = s.replace("_ОПЕР", "");
        if (s.equals("hcr:ICG")) {
            return "1 - \"<15%\"; 2 - \"15-25%\"; 3 - \">25%\"";
        }
        if (s.equals("hcr:ICGK-F")) {
            return "1 - \"<0.4\"; 2 - \"0.4-0.6\"; 3 -\">0.6\"";
        }
        if (s.equals("hcr:INVASIA_VOROTNAYA")) {
            return "1 - \"В ствол\"; 2 - \"В долевые ветви\"; 3 -\"В сегментарные ветви\"";
        }
        if (s.equals("hcr:INVASIYA_PECHENOCHNAYA")) {
            return "1 - \"Ипсилатеральную\"; 2 - \"Контрлатеральную\"; 3 -\"В собственную печеночную\"";
        }
        if (s.equals("hcr:OCENKA_TROF_STATUSA")) {
            return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 -\"Неудовлетворительный\"";
        }
        if (s.equals("hcr:MAX_OPUCHOL")) {
            return "1 - \"<10см\"; 2 - \">10см\"";
        }
        if (s.equals("hcr:ONKOMARKER")) {
            return "1 - \"CA\"; 2 - \"19-9\"; 3 -\"CEA\"";
        }
        if (s.equals("hcr:TROMBOZ_PECHEN_VEN")) {
            return "1 - \"нет\"; 2 - \"Да\"; 3 -\"С выходом в нижнюю полую вену\"";
        }
        if (s.equals("hcr:TROMBOZ_VOROTNOI_VENI")) {
            return "1 - \"0\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"; 5 - \"4\"";
        }
        if (s.equals("hcr:VNEPECHENOCH_OCHAGI")) {
            return "1 - \"нет\"; 2 - \"Отдаленные метастазы\"; 3 -\"Прорастание соседних органов\"";
        }
        if (s.equals("hcr:GRWR")) {
            return "1 - \"<0.8\"; 2 - \"0.8-1\"; 3 -\">1\"";
        }
        if (s.equals("hcr:OPUCH_UZLI_CHISLO")) {
            return "1 - \"0\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"; 5 - \"4\"; 6 - \"5\"; 7 - \">5\"";
        }
        if (s.equals("hcr:OPUCH_UZLI_TYPE")) {
            return "1 - \"Солитарный\"; 2 - \"Множественные\"";
        }


        if (s.equals("hcr:OPERATIV_LECHEN")) {
            return "1 - \"Резекция печени\"; 2 - \"Чрескожая РЧА (чРЧА)\"; 3 -\"Алкоголизация\"";
        }
        if (s.equals("hcr:OBJEM_REZEKCII")) {
            return "1 - \"Обширная резекция ЛГГЭ\"; 2 - \"Обширная резекция ПГГЭ\";" +
                    " 3 -\"Cегментарная резекция\"";
        }
        if (s.equals("hcr:SEGMENT_REZEKCIYA")) {
            return "1 - \"Анатомическая\"; 2 - \"Неанатомическая\"";
        }
        if (s.equals("hcr:CHISLO_OCHAGOV_RCHA")) {
            return "1 - \"1\"; 2 - \"2\"; 3 -\"3\"; 4 - \">3\"";
        }
        if (s.equals("hcr:POLNOTA_RCHA")) {
            return "1 - \"Полная\"; 2 - \"Неполная\"";
        }
        if (s.equals("hcr:KROVOPOTERYA")) {
            return "1 - \"<500\"; 2 - \"500-1000\"; 3 - \">1000\"";
        }
        if (s.equals("hcr:TNM")) {
            return "1 - \"T1N0M0\"; 2 - \"T2aN0M0\"; 3 - \"T2bN0M0\"; 4 - \"T3N0M0\"; 5 - \"T1N1M0\"; 6 - \"T2N1M0\";" +
                    "7 - \"T3N1M0\"; 8 - \"T4N0M0\"; 9 - \"T4N1M0\"; 10 - \"Любая T, N2M0\"; 11 - \"Любая T, любая N, M1\"";
        }

        if (s.equals("hcr:OSLOZH")) {
            return "1 - \"Внутрибрюшное кровотечение\"; 2 - \"ЖКК\"; 3 - \"Печеночная недостаточность A\";" +
                    " 4 - \"Печеночная недостаточность B\"; 5 - \"Печеночная недостаточность C\";" +
                    " 6 - \"Механическая желтуха\"; 7 - \"Нагноение раны\"; 8 - \"Холангит\";" +
                    " 9 - \"Холангиогенное абсцедирование\"; 10 - \"Асцит\"; 11 - \"Эвентрация\"" +
                    "; 12 - \"Кишечная непроходимость\"; 13 - \"Панкреатический свищ\"; 14 - \"Острый панкреатит\";" +
                    " 15 - \"Сепсис\"; 16 - \"Тяжелый сепсис\"; 17 - \"Септический шок\"; 18 - \"Желчный свищ\"" +
                    "; 19 - \"Кишечный свищ\"; 20 - \"Тромбоз воротной вены\"; 21 - \"Тромбоз печеночной артерии\";" +
                    " 22 - \"Односторонняя пневмония\"" +
                    "; 23 - \"Двусторонняя пневмония\"; 24 - \"Гидроторакс\"; 25 - \"Почечная недостаточность\"" +
                    "; 26 - \"Сердечно-сосудистая недостаточность\"; 27 - \"Полиорганная недостаточность\"";
        }
        if (s.equals("hcr:CLAVIEN")) {
            return "1 - \"I\"; 2 - \"II\"; 3 - \"IIIa\"; 4 - \"IIIb\"; 5 - \"IVa\"; 6 - \"IVb\"; 7 - \"V\"";
        }
        if (s.equals("hcr:SCINTIGRAF_OBJ_OPERAC")) {
            return "1 - \"Сегментарная\"; 2 - \"Обширная\"";
        }
        if (s.equals("hcr:SOSUD_INVASIA")) {
            return "1 - \"Нет\"; 2 - \"Микрососудистая инвазия\"; 3 -\"Макрососудистая инвазия\"";
        }
        return "";
    }
}