package com.amgureeva.binding.HCRBinding;

import com.amgureeva.Entities.Diagnosis.HCR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 28.04.2016.
 */
@Repository
public class HCRDAOImpl implements HCRDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    @Override
    public HCR findById(Integer id, boolean isFirstDiagnosis) {
        if (id != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
			params.put("admissiondiag", (isFirstDiagnosis)?1:0);

			String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=2" +
					" AND admissiondiag=:admissiondiag";

            List<List<HCR>> result = null;
            try {
                result = namedParameterJdbcTemplate
                        .query(sql, params, new HCRMapper());
            } catch (EmptyResultDataAccessException e) {
                // do nothing, return null
            }
			result.remove(null);
			if (result == null || result.size() == 0 || result.get(0).size() == 0)
				return null;
			if (isFirstDiagnosis) {
				if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
			if (!isFirstDiagnosis) {
				if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
        }
        return null;
    }

    @Override
    public List<HCR> findAll() {
        String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 2";
        List<List<HCR>> result = namedParameterJdbcTemplate.query(sql, new HCRMapper());
        result.remove(null);
        return (result == null || result.size() == 0)?null :result.get(0);
    }

    @Override
    public void save(HCR hcr) {
        String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
                + "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),2, String.valueOf(hcr.getIcg()), hcr.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),3, String.valueOf(hcr.getIcgk_f()), hcr.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),18, String.valueOf(hcr.getRazmerOpucholi()), hcr.isDiagnosisAtStart())); //razmer opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),20, String.valueOf(hcr.getChisloOpuchUzlov()), hcr.isDiagnosisAtStart())); //chislo opucholevich uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),19, String.valueOf(hcr.getOpuchUzliType()), hcr.isDiagnosisAtStart())); //tip opucholev uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),22, String.valueOf(hcr.getSosudInvasia()), hcr.isDiagnosisAtStart())); //tip sosudistoi invasii
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),24, String.valueOf(hcr.getVnepOchagiType()), hcr.isDiagnosisAtStart())); //tip vnepech oxhagov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),25, String.valueOf((hcr.getTrombPechType())), hcr.isDiagnosisAtStart())); //tromboz pechenochnich ven
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),11, String.valueOf(hcr.getTrofStatus()), hcr.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),29, String.valueOf((hcr.isProtivTerapia())?1:0), hcr.isDiagnosisAtStart())); //is protivovirusnaya terapiya
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),30, String.valueOf(hcr.getTrombozVorotVeni()), hcr.isDiagnosisAtStart())); //tromboz vorotnoi veni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),1, String.valueOf(hcr.getGrwr()), hcr.isDiagnosisAtStart())); //grwr
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),27, String.valueOf(hcr.getIndexZachvata()), hcr.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),28, String.valueOf(hcr.getObjemOstatka()), hcr.isDiagnosisAtStart())); //o
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),26, String.valueOf(hcr.getRezekciaType()), hcr.isDiagnosisAtStart())); //objem

    }

    @Override
    public void update(HCR hcr) {
        String sql = "UPDATE patientparametersvalue SET value=:value"
                + " WHERE patientId=:patientId and diseaseId=2 and parameterId=:parameterId" +
                " and admissiondiag=:admissiondiag";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),2, String.valueOf(hcr.getIcg()), hcr.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),3, String.valueOf(hcr.getIcgk_f()), hcr.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),18, String.valueOf(hcr.getRazmerOpucholi()), hcr.isDiagnosisAtStart())); //razmer opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),20, String.valueOf(hcr.getChisloOpuchUzlov()), hcr.isDiagnosisAtStart())); //chislo opucholevich uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),19, String.valueOf(hcr.getOpuchUzliType()), hcr.isDiagnosisAtStart())); //tip opucholev uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),22, String.valueOf(hcr.getSosudInvasia()), hcr.isDiagnosisAtStart())); //tip sosudistoi invasii
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),24, String.valueOf(hcr.getVnepOchagiType()), hcr.isDiagnosisAtStart())); //tip vnepech oxhagov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),25, String.valueOf((hcr.getTrombPechType())), hcr.isDiagnosisAtStart())); //tromboz pechenochnich ven
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),11, String.valueOf(hcr.getTrofStatus()), hcr.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),29, String.valueOf((hcr.isProtivTerapia())?1:0), hcr.isDiagnosisAtStart())); //is protivovirusnaya terapiya
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),30, String.valueOf(hcr.getTrombozVorotVeni()), hcr.isDiagnosisAtStart())); //tromboz vorotnoi veni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),1, String.valueOf(hcr.getGrwr()), hcr.isDiagnosisAtStart())); //grwr
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),27, String.valueOf(hcr.getIndexZachvata()), hcr.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),28, String.valueOf(hcr.getObjemOstatka()), hcr.isDiagnosisAtStart())); //o
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (hcr.getId(),26, String.valueOf(hcr.getRezekciaType()), hcr.isDiagnosisAtStart())); //objem
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=2";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }

    private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value, boolean admissiondiag) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("patientId", patientId);
        paramSource.addValue("parameterId", fieldId);
        paramSource.addValue("diseaseId", 2);
        paramSource.addValue("value", value);
        paramSource.addValue("admissiondiag", admissiondiag);
        return paramSource;
    }


    private static final class HCRMapper implements RowMapper<List<HCR>> {

        public List<HCR> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<HCR> hcrlist = new ArrayList<HCR>();
            HCR hcr = new HCR();
            int i  = 0;
            do {
                hcr.setId(rs.getString("patientId"));
                hcr.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
                int parameterId = rs.getInt("parameterId");
                switch(parameterId) {
                    case 1: if (i!=0 && i!=15) return null;i=1; hcr.setGrwr(rs.getInt("value")); break;
                    case 2: i++; hcr.setIcg(rs.getInt("value"));break;
                    case 3: i++; hcr.setIcgk_f(rs.getInt("value"));break;
                    case 18: i++; hcr.setRazmerOpucholi(rs.getInt("value"));break;
                    case 20: i++; hcr.setChisloOpuchUzlov(rs.getInt("value"));break;
                    case 19: i++; hcr.setOpuchUzliType(rs.getInt("value"));break;
                    case 22: i++; hcr.setSosudInvasia(rs.getInt("value"));break;
                    case 24: i++; hcr.setVnepOchagiType(rs.getInt("value"));break;
                    case 25: i++; hcr.setTrombPechType(rs.getInt("value"));break;
                    case 11: i++; hcr.setTrofStatus(rs.getInt("value")); break;
                    case 29: i++; hcr.setProtivTerapia(rs.getString("value").equals("1")?true:false); break;
                    case 30: i++; hcr.setTrombozVorotVeni(rs.getInt("value")); break;
                    case 27: i++; hcr.setIndexZachvata(rs.getString("value")); break;
                    case 28: i++; hcr.setObjemOstatka(rs.getString("value")); break;
                    case 26: i++; hcr.setRezekciaType(rs.getInt("value")); break;
                }
                if (i==15) {
                    hcrlist.add(hcr);
                    hcr = new HCR();
                }
            }while (rs.next());
            return hcrlist;
        }
    }
}
