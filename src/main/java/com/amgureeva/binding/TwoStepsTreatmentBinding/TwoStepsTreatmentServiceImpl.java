package com.amgureeva.binding.TwoStepsTreatmentBinding;

import com.amgureeva.Entities.GeneralSurgery;
import com.amgureeva.Entities.TwoStepsTreatment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Nikita on 10.10.2016.
 */
@Service("twoStepsTreatmentService")
public class TwoStepsTreatmentServiceImpl implements TwoStepsTreatmentService {

	private TwoStepsTreatmentDAO twoStepsTreatmentDao;

	@Autowired
	public void setTwoStepsTreatment(TwoStepsTreatmentDAO dao ) {this.twoStepsTreatmentDao = dao;}


	@Override
	public TwoStepsTreatment findById(Integer id) {
		return twoStepsTreatmentDao.findById(id);
	}

	@Override
	public List<TwoStepsTreatment> findAll() {
		return twoStepsTreatmentDao.findAll();
	}

	@Override
	public void saveOrUpdate(TwoStepsTreatment twoStepsTreatment, boolean isNew) {
		if (isNew) {
			twoStepsTreatmentDao.save(twoStepsTreatment);
		} else {
			twoStepsTreatmentDao.update(twoStepsTreatment);
			if(twoStepsTreatment.getProvedenie2Etapnogo() == 0) {
				GeneralSurgery gs = new GeneralSurgery();
				gs.setId(twoStepsTreatment.getId());
				twoStepsTreatmentDao.updateResults(gs);
			}
		}
	}

	@Override
	public void updateResults(GeneralSurgery generalSurgery) {
		twoStepsTreatmentDao.updateResults(generalSurgery);
	}

	@Override
	public void delete(int id) {
		twoStepsTreatmentDao.delete(id);
	}

	@Override
	public GeneralSurgery findResultById(Integer id) {
		return twoStepsTreatmentDao.findResultById(id);
	}
}
