package com.amgureeva.binding.TwoStepsTreatmentBinding;

import com.amgureeva.Entities.GeneralSurgery;
import com.amgureeva.Entities.TwoStepsTreatment;

import java.util.List;

/**
 * Created by Nikita on 10.10.2016.
 */
public interface TwoStepsTreatmentDAO {
	TwoStepsTreatment findById(Integer id);

	GeneralSurgery findResultById(Integer id);

	List<TwoStepsTreatment> findAll();

	void save(TwoStepsTreatment twoStepsTreatment);

	void update(TwoStepsTreatment twoStepsTreatment);

	void updateResults(GeneralSurgery generalSurgery);

	void delete(Integer id);
}
