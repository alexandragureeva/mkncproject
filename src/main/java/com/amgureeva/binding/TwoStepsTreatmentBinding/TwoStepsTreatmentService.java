package com.amgureeva.binding.TwoStepsTreatmentBinding;

import com.amgureeva.Entities.GeneralSurgery;
import com.amgureeva.Entities.TwoStepsTreatment;

import java.util.List;

/**
 * Created by Nikita on 10.10.2016.
 */
public interface TwoStepsTreatmentService {
	TwoStepsTreatment findById(Integer id);

	List<TwoStepsTreatment> findAll();

	void saveOrUpdate(TwoStepsTreatment twoStepsTreatment, boolean isNew);

	void updateResults(GeneralSurgery generalSurgery);

	void delete(int id);

	GeneralSurgery findResultById(Integer id);
}
