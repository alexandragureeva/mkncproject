package com.amgureeva.binding.TwoStepsTreatmentBinding;

import com.amgureeva.Entities.GeneralSurgery;
import com.amgureeva.Entities.TwoStepsTreatment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 10.10.2016.
 */
@Repository
public class TwoStepsTreatmentDAOImpl implements TwoStepsTreatmentDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public TwoStepsTreatment findById(Integer id) {
		if (id == null) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);

		String sql = "SELECT * FROM twostepstreatment WHERE id=:id";

		TwoStepsTreatment result = null;
		try {
			result = namedParameterJdbcTemplate
					.queryForObject(sql, params, new TwoStepsTreatmentDAOImpl.TwoStepsTreatmentMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}

		return result;
	}

	@Override
	public GeneralSurgery findResultById(Integer id) {
		if (id == null) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);

		String sql = "SELECT * FROM twostepstreatment WHERE id=:id";

		GeneralSurgery result = null;
		try {
			result = namedParameterJdbcTemplate
					.queryForObject(sql, params, new TwoStepsTreatmentDAOImpl.GeneralSurgeryMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}

		return result;
	}

	@Override
	public List<TwoStepsTreatment> findAll() {
		String sql = "SELECT * FROM twostepstreatment";
		List<TwoStepsTreatment> result = namedParameterJdbcTemplate.query(sql, new TwoStepsTreatmentDAOImpl.TwoStepsTreatmentMapper());
		return result;
	}

	@Override
	public void save(TwoStepsTreatment twoStepsTreatment) {
		String sql = "INSERT INTO twostepstreatment(id, provedenie2Etapnogo, variant2Etapnogo, okklusia, portoembType, " +
				"microspheres, spirt, lipiodol, otherMaterial, razobsheniePecheni, dostup, rezekcia, vmeshatelstvo, perevyazkaType) " +
				"VALUES (:id, :provedenie2Etapnogo, :variant2Etapnogo, :okklusia, :portoembType, :microspheres, :spirt, :lipiodol," +
				" otherMaterial, :razobsheniePecheni, :dostup, :rezekcia, :vmeshatelstvo, :perevyazkaType)";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(twoStepsTreatment));
	}

	@Override
	public void update(TwoStepsTreatment twoStepsTreatment) {
		String sql = "UPDATE twostepstreatment SET provedenie2Etapnogo=:provedenie2Etapnogo," +
				"variant2Etapnogo=:variant2Etapnogo, okklusia=:okklusia, portoembType=:portoembType," +
				" microspheres=:microspheres, spirt=:spirt, lipiodol=:lipiodol, otherMaterial=:otherMaterial," +
				" razobsheniePecheni=:razobsheniePecheni, dostup=:dostup, rezekcia=:rezekcia," +
				" vmeshatelstvo=:vmeshatelstvo, perevyazkaType=:perevyazkaType" +
				" WHERE id=:id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(twoStepsTreatment));
	}

	@Override
	public void updateResults(GeneralSurgery generalSurgery) {
		String sql = "UPDATE twostepstreatment SET febLichoradka=:febLichoradka, krovotechenie=:krovotechenie, " +
				"absess=:absess, zhidkSkoplenia=:zhidkSkoplenia, tromboz=:tromboz, zelcheistechenie=:zelcheistechenie," +
				" cholangit=:cholangit, nagnoeniePecheni=:nagnoeniePecheni, postoblSyndrom=:postoblSyndrom, " +
				"nepolnayaPortoemb=:nepolnayaPortoemb, nepolnayaPortoembType=:nepolnayaPortoembType, " +
				"nepolnayaGipertrofia=:nepolnayaGipertrofia, nepolnayaGipertrofiaType=:nepolnayaGipertrofiaType, " +
				"postEmbSyndrom=:postEmbSyndrom, gemobilia=:gemobilia," +
				" pechNedostatochnost=:pechNedostatochnost, other=:other,budushOstPech=:budushOstPech, objem=:objem," +
				"dostignuta=:dostignuta" +
				" WHERE id=:id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByResult(generalSurgery));
	}


	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM twostepstreatment WHERE id= :id";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		namedParameterJdbcTemplate.update(sql, params);
	}

	private SqlParameterSource getSqlParameterByModel(TwoStepsTreatment twoStepsTreatment) {

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", twoStepsTreatment.getId());
		paramSource.addValue("provedenie2Etapnogo", twoStepsTreatment.getProvedenie2Etapnogo());
		paramSource.addValue("portoembType", twoStepsTreatment.getPortoembType());
		paramSource.addValue("microspheres", twoStepsTreatment.getMicrospheres());
		paramSource.addValue("spirt", twoStepsTreatment.getSpirt());
		paramSource.addValue("lipiodol", twoStepsTreatment.getLipiodol());
		paramSource.addValue("variant2Etapnogo", twoStepsTreatment.getVariant2Etapnogo());
		paramSource.addValue("okklusia", twoStepsTreatment.getOkklusia());
		paramSource.addValue("otherMaterial", twoStepsTreatment.getOtherMaterial());
		paramSource.addValue("razobsheniePecheni", twoStepsTreatment.getRazobsheniePecheni());
		paramSource.addValue("dostup", twoStepsTreatment.getDostup());
		paramSource.addValue("rezekcia", twoStepsTreatment.getRezekcia());
		paramSource.addValue("vmeshatelstvo", twoStepsTreatment.getVmeshatelstvo());
		paramSource.addValue("perevyazkaType", twoStepsTreatment.getPerevyazkaType());
		
		return paramSource;
	}

	private SqlParameterSource getSqlParameterByResult(GeneralSurgery generalSurgery) {

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", generalSurgery.getId());
		paramSource.addValue("nepolnayaPortoemb", generalSurgery.getNepolnayaPortoembF());
		paramSource.addValue("nepolnayaPortoembType", generalSurgery.getNepolnayaPortoembTypeF());
		paramSource.addValue("nepolnayaGipertrofia", generalSurgery.getNepolnayaGipertrofiaF());
		paramSource.addValue("nepolnayaGipertrofiaType", generalSurgery.getNepolnayaGipertrofiaTypeF());
		paramSource.addValue("postEmbSyndrom", generalSurgery.getPostEmbSyndromF());
		paramSource.addValue("krovotechenie", generalSurgery.getKrovotechenieF());
		paramSource.addValue("gemobilia", generalSurgery.getGemobiliaF());
		paramSource.addValue("pechNedostatochnost", generalSurgery.getPechNedostatochnostF());
		paramSource.addValue("objem", generalSurgery.getObjem());
		paramSource.addValue("dostignuta", generalSurgery.getDostignuta());
		paramSource.addValue("budushOstPech", generalSurgery.getBudushOstPech());
		paramSource.addValue("zelcheistechenie", generalSurgery.getZelcheistechenieF());
		paramSource.addValue("cholangit", generalSurgery.getCholangitF());
		paramSource.addValue("nagnoeniePecheni", generalSurgery.getNagnoeniePecheniF());
		paramSource.addValue("tromboz", generalSurgery.getTrombozF());
		paramSource.addValue("other", generalSurgery.getOtherF());
		paramSource.addValue("absess", generalSurgery.getAbscessF());
		paramSource.addValue("febLichoradka", generalSurgery.getFebLichoradkaF());
		paramSource.addValue("zhidkSkoplenia", generalSurgery.getZhidkSkopleniaF());
		paramSource.addValue("postoblSyndrom", generalSurgery.getPostoblSyndromF());

		return paramSource;
	}
	
	private static final class TwoStepsTreatmentMapper implements RowMapper<TwoStepsTreatment> {

		public TwoStepsTreatment mapRow(ResultSet rs, int rowNum) throws SQLException {
			TwoStepsTreatment twoStepsTreatment = new TwoStepsTreatment();
			twoStepsTreatment.setId(rs.getString("id"));
			twoStepsTreatment.setProvedenie2Etapnogo(rs.getInt("provedenie2Etapnogo"));
			twoStepsTreatment.setPortoembType(rs.getInt("portoembType"));
			twoStepsTreatment.setMicrospheres(rs.getInt("microspheres"));
			twoStepsTreatment.setSpirt(rs.getInt("spirt"));
			twoStepsTreatment.setLipiodol(rs.getInt("lipiodol"));
			twoStepsTreatment.setVariant2Etapnogo(rs.getInt("variant2Etapnogo"));
			twoStepsTreatment.setOkklusia(rs.getInt("okklusia"));
			twoStepsTreatment.setOtherMaterial(rs.getInt("otherMaterial"));
			twoStepsTreatment.setRazobsheniePecheni(rs.getInt("razobsheniePecheni"));
			twoStepsTreatment.setDostup(rs.getInt("dostup"));
			twoStepsTreatment.setRezekcia(rs.getInt("rezekcia"));
			twoStepsTreatment.setVmeshatelstvo(rs.getInt("vmeshatelstvo"));
			twoStepsTreatment.setPerevyazkaType(rs.getInt("perevyazkaType"));

			return twoStepsTreatment;
		}
	}

	private static final class GeneralSurgeryMapper implements RowMapper<GeneralSurgery> {

		public GeneralSurgery mapRow(ResultSet rs, int rowNum) throws SQLException {
			GeneralSurgery gs = new GeneralSurgery();
			gs.setId(rs.getString("id"));
			gs.setNepolnayaPortoembF(rs.getInt("nepolnayaPortoemb"));
			gs.setNepolnayaPortoembTypeF(rs.getInt("nepolnayaPortoembType"));
			gs.setNepolnayaGipertrofiaF(rs.getInt("nepolnayaGipertrofia"));
			gs.setNepolnayaGipertrofiaTypeF(rs.getInt("nepolnayaGipertrofiaType"));
			gs.setPostEmbSyndromF(rs.getInt("postEmbSyndrom"));
			gs.setKrovotechenieF(rs.getInt("krovotechenie"));
			gs.setGemobiliaF(rs.getInt("gemobilia"));
			gs.setPechNedostatochnostF(rs.getInt("pechNedostatochnost"));
			gs.setObjem(rs.getFloat("objem"));
			gs.setDostignuta(rs.getFloat("dostignuta"));
			gs.setBudushOstPech(rs.getFloat("budushOstPech"));
			gs.setZelcheistechenieF(rs.getInt("zelcheistechenie"));
			gs.setCholangitF(rs.getInt("cholangit"));
			gs.setNagnoeniePecheniF(rs.getInt("nagnoeniePecheni"));
			gs.setTrombozF(rs.getInt("tromboz"));
			gs.setOtherF(rs.getInt("other"));
			gs.setAbscessF(rs.getInt("absess"));
			gs.setFebLichoradkaF(rs.getInt("febLichoradka"));
			gs.setZhidkSkopleniaF(rs.getInt("zhidkSkoplenia"));
			gs.setPostoblSyndromF(rs.getInt("postoblSyndrom"));
			return gs;
		}
	}
}
