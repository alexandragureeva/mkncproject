package com.amgureeva.binding.OpMKRBinding;

import com.amgureeva.Entities.Surgeries.OpMKR;

import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public interface OpMKRDAO {

	OpMKR findById(Integer id);

	List<OpMKR> findAll();

	void save(OpMKR opMKR);

	void update(OpMKR opMKR);

	void delete(Integer id);
}