package com.amgureeva.binding.OpMKRBinding;

import com.amgureeva.Entities.Surgeries.OpMKR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 22.05.2016.
 */
@Repository
public class OpMKRDAOImpl implements OpMKRDAO {


	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public OpMKR findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=3";

			List<OpMKR> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpMKRDAOImpl.OpMKRMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}

	@Override
	public List<OpMKR> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 3";
		List<List<OpMKR>> result = namedParameterJdbcTemplate.query(sql,
				new OpMKRDAOImpl.OpMKRMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(OpMKR opMKR) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 7, String.valueOf(opMKR.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 12, String.valueOf(opMKR.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 13, String.valueOf(opMKR.getMkrgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 18, String.valueOf(opMKR.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 21, String.valueOf(opMKR.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 22, String.valueOf(opMKR.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 23, String.valueOf(opMKR.getSelSosIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 25, String.valueOf(opMKR.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 26, String.valueOf(opMKR.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 32, String.valueOf(opMKR.getSimultOp())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 34, String.valueOf(opMKR.getOplech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 35, String.valueOf(opMKR.getMkrobrezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 36, String.valueOf(opMKR.getMkrsegrez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 37, String.valueOf(opMKR.getMkrchisloUdSeg())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 8, String.valueOf(opMKR.getSochIntrRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 56, String.valueOf(opMKR.getVarRezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 57, String.valueOf(opMKR.getNalRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 58, String.valueOf(opMKR.getRekTKish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 1, String.valueOf(opMKR.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 2, String.valueOf(opMKR.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),103, String.valueOf(opMKR.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),104, String.valueOf(opMKR.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),105, String.valueOf(opMKR.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),106, String.valueOf(opMKR.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),107, String.valueOf(opMKR.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),108, String.valueOf(opMKR.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),109, String.valueOf(opMKR.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),110, String.valueOf(opMKR.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),111, String.valueOf(opMKR.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),112, String.valueOf(opMKR.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),113, String.valueOf(opMKR.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),114, String.valueOf(opMKR.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),115, String.valueOf(opMKR.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),116, String.valueOf(opMKR.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),117, String.valueOf(opMKR.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),118, String.valueOf(opMKR.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),119, String.valueOf(opMKR.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),120, String.valueOf(opMKR.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),121, String.valueOf(opMKR.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),122, String.valueOf(opMKR.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),123, String.valueOf(opMKR.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),124, String.valueOf(opMKR.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),125, String.valueOf(opMKR.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),126, String.valueOf(opMKR.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),127, String.valueOf(opMKR.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),128, String.valueOf(opMKR.getPoliorganNedostat())));
	}
	@Override
	public void update(OpMKR opMKR) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=3 and parameterId=:parameterId";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 7, String.valueOf(opMKR.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 12, String.valueOf(opMKR.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 13, String.valueOf(opMKR.getMkrgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 18, String.valueOf(opMKR.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 21, String.valueOf(opMKR.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 22, String.valueOf(opMKR.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 23, String.valueOf(opMKR.getSelSosIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 25, String.valueOf(opMKR.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 26, String.valueOf(opMKR.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 32, String.valueOf(opMKR.getSimultOp())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 34, String.valueOf(opMKR.getOplech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 35, String.valueOf(opMKR.getMkrobrezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 36, String.valueOf(opMKR.getMkrsegrez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 37, String.valueOf(opMKR.getMkrchisloUdSeg())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 8, String.valueOf(opMKR.getSochIntrRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 56, String.valueOf(opMKR.getVarRezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 57, String.valueOf(opMKR.getNalRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 58, String.valueOf(opMKR.getRekTKish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 1, String.valueOf(opMKR.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(), 2, String.valueOf(opMKR.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),103, String.valueOf(opMKR.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),104, String.valueOf(opMKR.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),105, String.valueOf(opMKR.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),106, String.valueOf(opMKR.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),107, String.valueOf(opMKR.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),108, String.valueOf(opMKR.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),109, String.valueOf(opMKR.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),110, String.valueOf(opMKR.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),111, String.valueOf(opMKR.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),112, String.valueOf(opMKR.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),113, String.valueOf(opMKR.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),114, String.valueOf(opMKR.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),115, String.valueOf(opMKR.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),116, String.valueOf(opMKR.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),117, String.valueOf(opMKR.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),118, String.valueOf(opMKR.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),119, String.valueOf(opMKR.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),120, String.valueOf(opMKR.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),121, String.valueOf(opMKR.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),122, String.valueOf(opMKR.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),123, String.valueOf(opMKR.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),124, String.valueOf(opMKR.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),125, String.valueOf(opMKR.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),126, String.valueOf(opMKR.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),127, String.valueOf(opMKR.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opMKR.getId(),128, String.valueOf(opMKR.getPoliorganNedostat())));
	}


	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=3";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 3);
		paramSource.addValue("value", value);
		return paramSource;
	}

	private static final class OpMKRMapper implements RowMapper<List<OpMKR>> {
		public List<OpMKR> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<OpMKR> list = new ArrayList<OpMKR>();
			OpMKR opMKR = new OpMKR();
			int i = 0;
			do {
				opMKR.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 1: i++; opMKR.setRezekciaType(rs.getInt("value"));break;
					case 2: i++; opMKR.setRazdelenieTkanei(rs.getInt("value")); break;
					case 7: if (i!=0 && i!=46) return null; i = 1; opMKR.setDlitop(rs.getString("value"));break;
					case 12: i++; opMKR.setGdc(rs.getInt("value")); break;
					case 13: i++; opMKR.setMkrgdcmin(rs.getString("value")); break;
					case 18: i++; opMKR.setLimfodissekcia(rs.getInt("value")); break;
					case 21: i++; opMKR.setBypass(rs.getInt("value")); break;
					case 22: i++; opMKR.setTotIz(rs.getInt("value")); break;
					case 23: i++; opMKR.setSelSosIz(rs.getInt("value")); break;
					case 25: i++; opMKR.setOsl(rs.getInt("value")); break;
					case 26: i++; opMKR.setClavien(rs.getInt("value")); break;
					case 32: i++; opMKR.setSimultOp(rs.getInt("value")); break;
					case 34: i++; opMKR.setOplech(rs.getInt("value")); break;
					case 35: i++; opMKR.setMkrobrezekcii(rs.getInt("value")); break;
					case 36: i++; opMKR.setMkrsegrez(rs.getInt("value")); break;
					case 37: i++; opMKR.setMkrchisloUdSeg(rs.getInt("value")); break;
					case 8: i++; opMKR.setSochIntrRHA(rs.getInt("value")); break;
					case 56: i++; opMKR.setVarRezekcii(rs.getInt("value")); break;
					case 57: i++; opMKR.setNalRHA(rs.getInt("value")); break;
					case 58: i++; opMKR.setRekTKish(rs.getInt("value")); break;
					case 103: i++; opMKR.setVnutrKrovot(rs.getInt("value")); break;
					case 104: i++; opMKR.setZhkk(rs.getInt("value")); break;
					case 105: i++; opMKR.setPechNedostatA(rs.getInt("value")); break;
					case 106: i++; opMKR.setPechNedostatB(rs.getInt("value")); break;
					case 107: i++; opMKR.setPechNedostatC(rs.getInt("value")); break;
					case 108: i++; opMKR.setNagnoenie(rs.getInt("value")); break;
					case 109: i++; opMKR.setCholangit(rs.getInt("value")); break;
					case 110: i++; opMKR.setCholAbscedir(rs.getInt("value")); break;
					case 111: i++; opMKR.setAscit(rs.getInt("value")); break;
					case 112: i++; opMKR.setEventracia(rs.getInt("value")); break;
					case 113: i++; opMKR.setKishNeprohod(rs.getInt("value")); break;
					case 114: i++; opMKR.setPankreatSvich(rs.getInt("value")); break;
					case 115: i++; opMKR.setOstriiSvich(rs.getInt("value")); break;
					case 116: i++; opMKR.setSepsis(rs.getInt("value")); break;
					case 117: i++; opMKR.setTyazhSepsis(rs.getInt("value")); break;
					case 118: i++; opMKR.setSeptShok(rs.getInt("value")); break;
					case 119: i++; opMKR.setZhelchSvish(rs.getInt("value")); break;
					case 120: i++; opMKR.setKishSvish(rs.getInt("value")); break;
					case 121: i++; opMKR.setTrombVorotVeni(rs.getInt("value")); break;
					case 122: i++; opMKR.setTrombozPechArter(rs.getInt("value")); break;
					case 123: i++; opMKR.setOdnostorPnevm(rs.getInt("value")); break;
					case 124: i++; opMKR.setDvustorPnevm(rs.getInt("value")); break;
					case 125: i++; opMKR.setGidrotorax(rs.getInt("value")); break;
					case 126: i++; opMKR.setPochNedostat(rs.getInt("value")); break;
					case 127: i++; opMKR.setSerdSosNedostat(rs.getInt("value")); break;
					case 128: i++; opMKR.setPoliorganNedostat(rs.getInt("value")); break;

				}
				if (i == 46) {
					list.add(opMKR);
					opMKR = new OpMKR();
				}
			}while (rs.next());
			return list;
		}
	}
}

