package com.amgureeva.binding.OpMKRBinding;

import com.amgureeva.Entities.Surgeries.OpMKR;
import com.amgureeva.Entities.Surgeries.OpMKRList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public class OpMKRListCreator {

	public List<OpMKRList> createMkrList(List<OpMKR> opMKRs, PatientService patientService) {
		if (opMKRs != null && patientService != null) {
			List<OpMKRList> opMKRLists = new ArrayList<OpMKRList>();
			OpMKRList opMKRList = new OpMKRList();
			for (int i = 0; i < opMKRs.size(); i++) {
				opMKRList.setName(patientService.findPatientById(Integer.valueOf(opMKRs.get(i).getId())).getName());
				switch (Integer.valueOf(opMKRs.get(i).getOplech())) {
					case 0:
						opMKRList.setOplech("--");
						break;
					case 1:
						opMKRList.setOplech("Первичная");
						break;
					case 2:
						opMKRList.setOplech("Вторая");
						break;
					case 3:
						opMKRList.setOplech("Третья");
						break;
					case 4:
						opMKRList.setOplech("Четвертая");
						break;
				}
				switch (Integer.valueOf(opMKRs.get(i).getMkrobrezekcii())) {
					case 0:
						opMKRList.setObrezekcii("--");
						break;
					case 1:
						opMKRList.setObrezekcii("Обширная резекция ЛГГЭ");
						break;
					case 2:
						opMKRList.setObrezekcii("Обширная резекция ПГГЭ");
						break;
					case 3:
						opMKRList.setObrezekcii("Cегментарная резекция");
						break;
				}
				switch (Integer.valueOf(opMKRs.get(i).getMkrsegrez())) {
					case 0:
						opMKRList.setSegrez("--");
						break;
					case 1:
						opMKRList.setSegrez("Анатомическая");
						break;
					case 2:
						opMKRList.setSegrez("Неанатомическая");
						break;
				}
				switch (Integer.valueOf(opMKRs.get(i).getMkrchisloUdSeg())) {
					case 0:
						opMKRList.setChisloUdSeg("--");
						break;
					case 1:
						opMKRList.setChisloUdSeg("1");
						break;
					case 2:
						opMKRList.setChisloUdSeg("2");
						break;
					case 3:
						opMKRList.setChisloUdSeg("3");
						break;
				}
				switch (Integer.valueOf(opMKRs.get(i).getVarRezekcii())) {
					case 0:
						opMKRList.setVarRezekcii("--");
						break;
					case 1:
						opMKRList.setVarRezekcii("Открытая");
						break;
					case 2:
						opMKRList.setVarRezekcii("Лапароскопическая");
						break;

				}
				switch (Integer.valueOf(opMKRs.get(i).getNalRHA())) {
					case 0:
						opMKRList.setNalRHA("--");
						break;
					case 1:
						opMKRList.setNalRHA("нет");
						break;
					case 2:
						opMKRList.setNalRHA("1");
						break;
					case 3:
						opMKRList.setNalRHA("2");
						break;
					case 4:
						opMKRList.setNalRHA("3");
						break;
				}
				switch (Integer.valueOf(opMKRs.get(i).getSimultOp())) {
					case 0:
						opMKRList.setSimultOp("--");
						break;
					case 1:
						opMKRList.setSimultOp("Резекция толстой кишки");
						break;
					case 2:
						opMKRList.setSimultOp("Резекция прямой кишки");
						break;
					case 3:
						opMKRList.setSimultOp("Резекция диафрагмы");
						break;
					case 4:
						opMKRList.setSimultOp("Резекция других органов(тонкой кишки и др)");
						break;
				}

				switch (Integer.valueOf(opMKRs.get(i).getClavien())) {
					case 0:
						opMKRList.setClavien("--");
						break;
					case 1:
						opMKRList.setClavien("I");
						break;
					case 2:
						opMKRList.setClavien("II");
						break;
					case 3:
						opMKRList.setClavien("IIIa");
						break;
					case 4:
						opMKRList.setClavien("IIIb");
						break;
					case 5:
						opMKRList.setClavien("IVa");
						break;
					case 6:
						opMKRList.setClavien("IVb");
						break;
					case 7:
						opMKRList.setClavien("V");
						break;
				}
				switch (Integer.valueOf(opMKRs.get(i).getOsl())) {
					case 0:
						opMKRList.setOsl("--");
						break;
					case 1:
						opMKRList.setOsl("Внутрибрюшное кровотечение");
						break;
					case 2:
						opMKRList.setOsl("ЖКК");
						break;
					case 3:
						opMKRList.setOsl("Печеночная недостаточность A");
						break;
					case 4:
						opMKRList.setOsl("Печеночная недостаточность B");
						break;
					case 5:
						opMKRList.setOsl("Печеночная недостаточность C");
						break;
					case 6:
						opMKRList.setOsl("Паренхиматозная желтуха");
						break;
					case 7:
						opMKRList.setOsl("Механическая желтуха");
						break;
					case 8:
						opMKRList.setOsl("Нагноение раны");
						break;
					case 9:
						opMKRList.setOsl("Холангит");
						break;
					case 10:
						opMKRList.setOsl("Холангиогенное абсцедирование");
						break;
					case 11:
						opMKRList.setOsl("Асцит");
						break;
					case 12:
						opMKRList.setOsl("Эвентрация");
						break;
					case 13:
						opMKRList.setOsl("Кишечная непроходимость");
						break;
					case 14:
						opMKRList.setOsl("Панкреатический свищ");
						break;
					case 15:
						opMKRList.setOsl("Острый панкреатит");
						break;
					case 16:
						opMKRList.setOsl("Сепсис");
						break;
					case 17:
						opMKRList.setOsl("Тяжелый сепсис");
						break;
					case 18:
						opMKRList.setOsl("Септический шок");
						break;
					case 19:
						opMKRList.setOsl("Желчный свищ");
						break;
					case 20:
						opMKRList.setOsl("Кишечный свищ");
						break;
					case 21:
						opMKRList.setOsl("Тромбоз воротной вены");
						break;
					case 22:
						opMKRList.setOsl("Тромбоз печеночной артерии");
						break;
					case 23:
						opMKRList.setOsl("Односторонняя пневмония");
						break;
					case 24:
						opMKRList.setOsl("Двусторонняя пневмония");
						break;
					case 25:
						opMKRList.setOsl("Гидроторакс");
						break;
					case 26:
						opMKRList.setOsl("Почечная недостаточность");
						break;
					case 27:
						opMKRList.setOsl("Сердечно-сосудистая недостаточность");
						break;
					case 28:
						opMKRList.setOsl("Полиорганная недостаточность");
						break;

				}
				if (opMKRs.get(i).getDlitop().equals("")||opMKRs.get(i).getDlitop().equals("null"))
					opMKRList.setDlitop("--");
				else
					opMKRList.setDlitop(String.valueOf(opMKRs.get(i).getDlitop()));
				if (opMKRs.get(i).getSochIntrRHA() == 0)
					opMKRList.setSochIntrRHA("-");
				else
					opMKRList.setSochIntrRHA("+");
				if (opMKRs.get(i).getLimfodissekcia() == 0)
					opMKRList.setLimfodissekcia("-");
				else
					opMKRList.setLimfodissekcia("+");
				if (opMKRs.get(i).getGdc() == 0)
					opMKRList.setGdc("-");
				else
					opMKRList.setGdc("+");
				if (opMKRs.get(i).getMkrgdcmin().equals("")||opMKRs.get(i).getMkrgdcmin().equals("null"))
					opMKRList.setGdcmin("--");
				else
					opMKRList.setGdcmin(String.valueOf(opMKRs.get(i).getMkrgdcmin()));
				if (opMKRs.get(i).getRekTKish() == 0)
					opMKRList.setRekTKish("-");
				else
					opMKRList.setRekTKish("+");
				if (opMKRs.get(i).getBypass() == 0)
					opMKRList.setBypass("-");
				else
					opMKRList.setBypass("+");
				if (opMKRs.get(i).getTotIz() == 0)
					opMKRList.setTotIz("-");
				else
					opMKRList.setTotIz("+");
				if (opMKRs.get(i).getSelSosIz() == 0)
					opMKRList.setSelSosIz("-");
				else
					opMKRList.setSelSosIz("+");
				opMKRLists.add(opMKRList);
				opMKRList = new OpMKRList();
			}
			return opMKRLists;
		}
		return null;
	}
}