package com.amgureeva.binding.DrenirovanieBinding;

import com.amgureeva.Entities.Surgeries.Drenirovanie;
import com.amgureeva.Entities.Surgeries.DrenirovanieList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 23.05.2016.
 */
public class DrenirovanieListCreator {
	public List<DrenirovanieList> createAlveokokkozList(List<Drenirovanie> drenirovanies, PatientService patientService) {
		if (drenirovanies != null && patientService != null) {
			List<DrenirovanieList> drenirovanieLists = new ArrayList<DrenirovanieList>();
			DrenirovanieList drenirovanieList = new DrenirovanieList();
			for (int i = 0; i < drenirovanies.size(); i++) {
				drenirovanieList.setName(patientService.findPatientById(Integer.valueOf(drenirovanies.get(i).getId())).getName());

				switch (Integer.valueOf(drenirovanies.get(i).getBlock())) {
					case 0:
						drenirovanieList.setBlock("--");
						break;
					case 1:
						drenirovanieList.setBlock("Общий печеночный проток");
						break;
					case 2:
						drenirovanieList.setBlock("Конфлюэнс");
						break;
					case 3:
						drenirovanieList.setBlock("Холедох");
						break;
					case 4:
						drenirovanieList.setBlock("Разобщение долевых протоков");
						break;
					case 5:
						drenirovanieList.setBlock("Разобщение секторальных протоков");
						break;
				}
				switch (Integer.valueOf(drenirovanies.get(i).getDolya())) {
					case 0:
						drenirovanieList.setDolya("--");
						break;
					case 1:
						drenirovanieList.setDolya("Левая");
						break;
					case 2:
						drenirovanieList.setDolya("Правая");
						break;
					case 3:
						drenirovanieList.setDolya("Обе");
						break;
				}
				if (drenirovanies.get(i).getAbsces_cholangit() == 0)
					drenirovanieList.setAbsces_cholangit("-");
				else
					drenirovanieList.setAbsces_cholangit("+");
				if (drenirovanies.get(i).getKlebsiela() == 0)
					drenirovanieList.setKlebsiela("-");
				else
					drenirovanieList.setKlebsiela("+");
				if (drenirovanies.get(i).getKr_posev() == 0)
					drenirovanieList.setKr_posev("-");
				else
					drenirovanieList.setKr_posev("+");
				if (drenirovanies.get(i).getZh_posev() == 0)
					drenirovanieList.setZh_posev("-");
				else
					drenirovanieList.setZh_posev("+");
				if (drenirovanies.get(i).getPrevious_cholangit() == 0)
					drenirovanieList.setPrevious_cholangit("-");
				else
					drenirovanieList.setPrevious_cholangit("+");
				if (drenirovanies.get(i).getSegment58() == 0)
					drenirovanieList.setSegment58("-");
				else
					drenirovanieList.setSegment58("+");
				if (drenirovanies.get(i).getSegment67() == 0)
					drenirovanieList.setSegment67("-");
				else
					drenirovanieList.setSegment67("+");
				if (drenirovanies.get(i).getSuprapapil() == 0)
					drenirovanieList.setSuprapapil("-");
				else
					drenirovanieList.setSuprapapil("+");
				if (drenirovanies.get(i).getSvoracivanie() == 0)
					drenirovanieList.setSvoracivanie("-");
				else
					drenirovanieList.setSvoracivanie("+");
				if (drenirovanies.get(i).getSinegnoin_palochka() == 0)
					drenirovanieList.setSinegnoin_palochka("-");
				else
					drenirovanieList.setSinegnoin_palochka("+");
				drenirovanieList.setComplications(String.valueOf(drenirovanies.get(i).getOsl()));
				drenirovanieList.setInout_drenages(String.valueOf(drenirovanies.get(i).getInout_drenages()));
				drenirovanieList.setOut_drenages(String.valueOf(drenirovanies.get(i).getOut_drenages()));
				drenirovanieList.setLongevity(String.valueOf(drenirovanies.get(i).getLongevity()));
				drenirovanieLists.add(drenirovanieList);
				drenirovanieList = new DrenirovanieList();
			}

			return drenirovanieLists;
		}
		return null;
	}

	public static String getAllVariants(String s) {
		if (s.equals("ДРЕН_БЛОК")) {
			return "1 - \"Общий печеночный проток\"; 2 - \"Конфлюэнс\"; 3 - \"Холедох\";" +
					" 4 - \"Разобщение долевых протоков\"; 5 - \"Разобщение секторальных протоков\"";
		}
		if (s.equals("ДРЕН_ДОЛЯ")) {
			return "1 - \"Левая\"; 2 - \"Правая\"; 3 -\"Обе\"";
		}
		if (s.equals("ДРЕН_ОСЛОЖН")) {
			return "1 - \"Нет\"; 2 - \"Миграция дренажа\"; 3 -\"Билома\"; 4 -\"Холангит\"; 5 -\"Кровотечение\"" +
					"; 6 -\"Сохранившаяся желтуха\"; 7 -\"Панкреатит\"; 8 -\"Гемобилия\"";
		}
		return "";
	}
}