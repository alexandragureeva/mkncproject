package com.amgureeva.binding.DrenirovanieBinding;

import com.amgureeva.Entities.Surgeries.Drenirovanie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 23.05.2016.
 */
@Repository
public class DrenirovanieDAOImpl implements DrenirovanieDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public Drenirovanie findById(Integer id, Integer drainId) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=9 ORDER BY drainId";

			List<List<Drenirovanie>> result = null;
			try {
				result = namedParameterJdbcTemplate
						.query(sql, params, new DrenirovanieDAOImpl.DrenirovanieMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			result.remove(null);
			if (result.size() == 0)
				return null;
			for (Drenirovanie drenirovanie : result.get(0)) {
				if (Integer.valueOf(drenirovanie.getDrenId()) == drainId) {
					return drenirovanie;
				}
			}
			return null;
		}
		return null;
	}

	@Override
	public List<Drenirovanie> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 9 ORDER BY drainId";
		List<List<Drenirovanie>> result = namedParameterJdbcTemplate.query(sql,new DrenirovanieDAOImpl.DrenirovanieMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}


	@Override
	public List<Drenirovanie> findPatientAll(String id) {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 9 and surgeryId =:surgeryId";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("surgeryId", id);
		List<List<Drenirovanie>> result = namedParameterJdbcTemplate.query(sql,params,new DrenirovanieDAOImpl.DrenirovanieMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(Drenirovanie drenirovanie) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId, drainId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId, :drainId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),62, String.valueOf(drenirovanie.getDate()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),63, String.valueOf(drenirovanie.getOut_drenages()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),64, String.valueOf(drenirovanie.getInout_drenages()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),65, String.valueOf(drenirovanie.getBlock()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),66, String.valueOf(drenirovanie.getDrenId()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),67, String.valueOf(drenirovanie.getLongevity()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),68, String.valueOf(drenirovanie.getPrevious_cholangit()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),69, String.valueOf(drenirovanie.getAbsces_cholangit()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),70, String.valueOf(drenirovanie.getDolya()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),71, String.valueOf(drenirovanie.getSvoracivanie()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),72, String.valueOf(drenirovanie.getSegment67()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),73, String.valueOf(drenirovanie.getSegment58()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),74, String.valueOf(drenirovanie.getSuprapapil()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),75, String.valueOf(drenirovanie.getZh_posev()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),76, String.valueOf(drenirovanie.getKr_posev()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),77, String.valueOf(drenirovanie.getKlebsiela()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),78, String.valueOf(drenirovanie.getSinegnoin_palochka()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),79, String.valueOf(drenirovanie.getOsl()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),139, String.valueOf(drenirovanie.getDrenageMigration()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),140, String.valueOf(drenirovanie.getBiloma()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),141, String.valueOf(drenirovanie.getCholangit()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),142, String.valueOf(drenirovanie.getKrovotechenie()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),143, String.valueOf(drenirovanie.getSochranivZheltucha()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),144, String.valueOf(drenirovanie.getPankreotit()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),145, String.valueOf(drenirovanie.getGemobilia()), drenirovanie.getDrenId()));
	}

	@Override
	public void update(Drenirovanie drenirovanie) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=9 and parameterId=:parameterId AND drainId=:drainId";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),62, String.valueOf(drenirovanie.getDate()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),63, String.valueOf(drenirovanie.getOut_drenages()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),64, String.valueOf(drenirovanie.getInout_drenages()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),65, String.valueOf(drenirovanie.getBlock()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),66, String.valueOf(drenirovanie.getDrenId()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),67, String.valueOf(drenirovanie.getLongevity()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),68, String.valueOf(drenirovanie.getPrevious_cholangit()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),69, String.valueOf(drenirovanie.getAbsces_cholangit()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),70, String.valueOf(drenirovanie.getDolya()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),71, String.valueOf(drenirovanie.getSvoracivanie()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),72, String.valueOf(drenirovanie.getSegment67()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),73, String.valueOf(drenirovanie.getSegment58()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),74, String.valueOf(drenirovanie.getSuprapapil()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),75, String.valueOf(drenirovanie.getZh_posev()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),76, String.valueOf(drenirovanie.getKr_posev()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),77, String.valueOf(drenirovanie.getKlebsiela()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),78, String.valueOf(drenirovanie.getSinegnoin_palochka()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),79, String.valueOf(drenirovanie.getOsl()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),139, String.valueOf(drenirovanie.getDrenageMigration()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),140, String.valueOf(drenirovanie.getBiloma()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),141, String.valueOf(drenirovanie.getCholangit()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),142, String.valueOf(drenirovanie.getKrovotechenie()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),143, String.valueOf(drenirovanie.getSochranivZheltucha()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),144, String.valueOf(drenirovanie.getPankreotit()), drenirovanie.getDrenId()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(drenirovanie.getId(),145, String.valueOf(drenirovanie.getGemobilia()), drenirovanie.getDrenId()));
	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=9";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}


	private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value, String drainId) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", patientId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 9);
		paramSource.addValue("value", value);
		paramSource.addValue("drainId", drainId);
		return paramSource;
	}


	private static final class DrenirovanieMapper implements RowMapper<List<Drenirovanie>> {

		public List<Drenirovanie> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<Drenirovanie> list = new ArrayList<Drenirovanie>();
			Drenirovanie drenirovanie = new Drenirovanie();
			int i = 0;
			do {
				drenirovanie.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 62: if (i!=0 && i!=25) return null;i=1; drenirovanie.setDate(rs.getString("value")); break;
					case 63: i++; drenirovanie.setOut_drenages(Integer.valueOf(rs.getString("value")));break;
					case 64: i++; drenirovanie.setInout_drenages(Integer.valueOf(rs.getString("value")));break;
					case 65: i++; drenirovanie.setBlock(rs.getInt("value"));break;
					case 66: i++; drenirovanie.setDrenId(rs.getString("value"));break;
					case 67: i++; drenirovanie.setLongevity(Integer.valueOf(rs.getString("value")));break;
					case 68: i++; drenirovanie.setPrevious_cholangit(rs.getInt("value")); break;
					case 69: i++; drenirovanie.setAbsces_cholangit(rs.getInt("value")); break;
					case 70: i++; drenirovanie.setDolya(rs.getInt("value")); break;
					case 71: i++; drenirovanie.setSvoracivanie(rs.getInt("value")); break;
					case 72: i++; drenirovanie.setSegment67(rs.getInt("value")); break;
					case 73: i++; drenirovanie.setSegment58(rs.getInt("value")); break;
					case 74: i++; drenirovanie.setSuprapapil(rs.getInt("value")); break;
					case 75: i++; drenirovanie.setZh_posev(rs.getInt("value")); break;
					case 76: i++; drenirovanie.setKr_posev(rs.getInt("value")); break;
					case 77: i++; drenirovanie.setKlebsiela(rs.getInt("value")); break;
					case 78: i++; drenirovanie.setSinegnoin_palochka(rs.getInt("value")); break;
					case 79: i++; drenirovanie.setOsl(rs.getInt("value")); break;
					case 139: i++; drenirovanie.setDrenageMigration(rs.getInt("value")); break;
					case 140: i++; drenirovanie.setBiloma(rs.getInt("value")); break;
					case 141: i++; drenirovanie.setCholangit(rs.getInt("value")); break;
					case 142: i++; drenirovanie.setKrovotechenie(rs.getInt("value")); break;
					case 143: i++; drenirovanie.setSochranivZheltucha(rs.getInt("value")); break;
					case 144: i++; drenirovanie.setPankreotit(rs.getInt("value")); break;
					case 145: i++; drenirovanie.setGemobilia(rs.getInt("value")); break;
				}
				if (i == 25) {
					list.add(drenirovanie);
					drenirovanie = new Drenirovanie();
				}
			}while (rs.next());
			return list;
		}
	}
}
