package com.amgureeva.binding.DrenirovanieBinding;

import com.amgureeva.Entities.Surgeries.Drenirovanie;

import java.util.List;

/**
 * Created by Nikita on 23.05.2016.
 */
public interface DrenirovanieDAO {

	Drenirovanie findById(Integer id, Integer drainId);

	List<Drenirovanie> findAll();

	void save(Drenirovanie drenirovanie);

	void update(Drenirovanie drenirovanie);

	void delete(Integer id);

	List<Drenirovanie> findPatientAll(String id);
}
