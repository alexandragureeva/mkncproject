package com.amgureeva.binding.MKRBinding;

import com.amgureeva.Entities.Diagnosis.MKR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 28.04.2016.
 */
@Repository
public class MKRDAOImpl implements MKRDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public MKR findById(Integer id, boolean isFirstDiagnosis) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);
			params.put("admissiondiag", (isFirstDiagnosis)?1:0);

			String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=3" +
					" AND admissiondiag=:admissiondiag";
			List<List<MKR>> result = null;
			try {
				result = namedParameterJdbcTemplate
						.query(sql, params, new GCRMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			result.remove(null);
			if (result == null || result.size() == 0)
				return null;
			if (isFirstDiagnosis) {
				if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
			if (!isFirstDiagnosis) {
				if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
				if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
			}
		}
		return null;
	}

	@Override
	public List<MKR> findAll() {
		String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 3";
		List<List<MKR>> result = namedParameterJdbcTemplate.query(sql, new GCRMapper());
		return (result == null || result.size() == 0) ? null : result.get(0);
	}

	@Override
	public void save(MKR MKR) {
		String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
				+ "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 35, String.valueOf(MKR.getNalVnepMet()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 36, String.valueOf(MKR.getPervOpuch()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 37, String.valueOf(MKR.getStadPervOpuch()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 38, String.valueOf(MKR.getObOPerPerOpuch()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 2, String.valueOf(MKR.getIcg()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 3, String.valueOf(MKR.getIcgk_f()), MKR.isDiagnosisAtStart())); //icgk-f
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 18, String.valueOf(MKR.getRazmerOpucholi()), MKR.isDiagnosisAtStart())); //razmer opucholi
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 20, String.valueOf(MKR.getChisloOpuchUzlov()), MKR.isDiagnosisAtStart())); //chislo opucholevich uzlov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 19, String.valueOf(MKR.getOpuchUzliType()), MKR.isDiagnosisAtStart())); //tip opucholev uzlov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 22, String.valueOf(MKR.getSosudInvasia()), MKR.isDiagnosisAtStart())); //tip sosudistoi invasii
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 24, String.valueOf(MKR.getVnepOchagiType()), MKR.isDiagnosisAtStart())); //tip vnepech oxhagov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 25, String.valueOf((MKR.getTrombPechType())), MKR.isDiagnosisAtStart())); //tromboz pechenochnich ven
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 11, String.valueOf(MKR.getTrofStatus()), MKR.isDiagnosisAtStart())); //ocenka troficheskogo statusa
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 40, String.valueOf((MKR.isNalNeoadChimioTer()) ? 1 : 0), MKR.isDiagnosisAtStart())); //nalichiee neoad terapii
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 30, String.valueOf(MKR.getTrombozVorotVeni()), MKR.isDiagnosisAtStart())); //tromboz vorotnoi veni
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 27, String.valueOf(MKR.getIndexZachvata()), MKR.isDiagnosisAtStart())); //index zachvata
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 28, String.valueOf(MKR.getObjemOstatka()), MKR.isDiagnosisAtStart())); //objem funkc doli pecheni
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 41, String.valueOf(MKR.getChimiotKurs()), MKR.isDiagnosisAtStart())); //cirroz
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 42, String.valueOf(MKR.getChimiotPrep()), MKR.isDiagnosisAtStart())); //vrv
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 43, String.valueOf(MKR.getChimiotEffect()), MKR.isDiagnosisAtStart())); //trombozitopenia
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 44, String.valueOf(MKR.getPojavMetType()), MKR.isDiagnosisAtStart())); //etiologia
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 45, String.valueOf(MKR.getPojavMetTime()), MKR.isDiagnosisAtStart()));//
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(),26, String.valueOf(MKR.getRezekciaType()), MKR.isDiagnosisAtStart())); //objem
	}

		@Override
	public void update(MKR MKR) {
		String sql = "UPDATE patientparametersvalue SET value=:value"
				+ " WHERE patientId=:patientId and diseaseId=3 and parameterId=:parameterId " +
				"and admissiondiag=:admissiondiag";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 35, String.valueOf(MKR.getNalVnepMet()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 36, String.valueOf(MKR.getPervOpuch()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 37, String.valueOf(MKR.getStadPervOpuch()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 38, String.valueOf(MKR.getObOPerPerOpuch()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 2, String.valueOf(MKR.getIcg()), MKR.isDiagnosisAtStart())); //icg
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 3, String.valueOf(MKR.getIcgk_f()), MKR.isDiagnosisAtStart())); //icgk-f
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 18, String.valueOf(MKR.getRazmerOpucholi()), MKR.isDiagnosisAtStart())); //razmer opucholi
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 20, String.valueOf(MKR.getChisloOpuchUzlov()), MKR.isDiagnosisAtStart())); //chislo opucholevich uzlov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 19, String.valueOf(MKR.getOpuchUzliType()), MKR.isDiagnosisAtStart())); //tip opucholev uzlov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 22, String.valueOf(MKR.getSosudInvasia()), MKR.isDiagnosisAtStart())); //tip sosudistoi invasii
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 24, String.valueOf(MKR.getVnepOchagiType()), MKR.isDiagnosisAtStart())); //tip vnepech oxhagov
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 25, String.valueOf((MKR.getTrombPechType())), MKR.isDiagnosisAtStart())); //tromboz pechenochnich ven
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 11, String.valueOf(MKR.getTrofStatus()), MKR.isDiagnosisAtStart())); //ocenka troficheskogo statusa
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 40, String.valueOf((MKR.isNalNeoadChimioTer()) ? 1 : 0), MKR.isDiagnosisAtStart())); //nalichiee neoad terapii
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 30, String.valueOf(MKR.getTrombozVorotVeni()), MKR.isDiagnosisAtStart())); //tromboz vorotnoi veni
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 27, String.valueOf(MKR.getIndexZachvata()), MKR.isDiagnosisAtStart())); //index zachvata
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 28, String.valueOf(MKR.getObjemOstatka()), MKR.isDiagnosisAtStart())); //objem funkc doli pecheni
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 41, String.valueOf(MKR.getChimiotKurs()), MKR.isDiagnosisAtStart())); //cirroz
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 42, String.valueOf(MKR.getChimiotPrep()), MKR.isDiagnosisAtStart())); //vrv
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 43, String.valueOf(MKR.getChimiotEffect()), MKR.isDiagnosisAtStart())); //trombozitopenia
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 44, String.valueOf(MKR.getPojavMetType()), MKR.isDiagnosisAtStart())); //etiologia
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(MKR.getId(), 45, String.valueOf(MKR.getPojavMetTime()), MKR.isDiagnosisAtStart()));//
			namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
					(MKR.getId(),26, String.valueOf(MKR.getRezekciaType()), MKR.isDiagnosisAtStart())); //objem

	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=3";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value,
													  boolean admissiondiag) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("patientId", patientId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 3);
		paramSource.addValue("value", value);
		paramSource.addValue("admissiondiag", admissiondiag);
		return paramSource;
	}


	private static final class GCRMapper implements RowMapper<List<MKR>> {

		public List<MKR> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<MKR> mkrList = new ArrayList<MKR>();
			MKR MKR = new MKR();
			int i = 0;
			do {
				MKR.setId(rs.getString("patientId"));
				MKR.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
				int parameterId = rs.getInt("parameterId");
				switch (parameterId) {
					case 2:
						if (i != 0 && i != 23) return null;
						i = 1;
						MKR.setIcg(rs.getInt("value"));
						break;
					case 3:
						i++;
						MKR.setIcgk_f(rs.getInt("value"));
						break;
					case 18:
						i++;
						MKR.setRazmerOpucholi(rs.getInt("value"));
						break;
					case 20:
						i++;
						MKR.setChisloOpuchUzlov(rs.getInt("value"));
						break;
					case 19:
						i++;
						MKR.setOpuchUzliType(rs.getInt("value"));
						break;
					case 22:
						i++;
						MKR.setSosudInvasia(rs.getInt("value"));
						break;
					case 24:
						i++;
						MKR.setVnepOchagiType(rs.getInt("value"));
						break;
					case 25:
						i++;
						MKR.setTrombPechType(rs.getInt("value"));
						break;
					case 11:
						i++;
						MKR.setTrofStatus(rs.getInt("value"));
						break;
					case 30:
						i++;
						MKR.setTrombozVorotVeni(rs.getInt("value"));
						break;
					case 27:
						i++;
						MKR.setIndexZachvata(rs.getString("value"));
						break;
					case 28:
						i++;
						MKR.setObjemOstatka(rs.getString("value"));
						break;
					case 35:
						i++;
						MKR.setNalVnepMet(rs.getInt("value"));
						break;
					case 36:
						i++;
						MKR.setPervOpuch(rs.getInt("value"));
						break;
					case 37:
						i++;
						MKR.setStadPervOpuch(rs.getInt("value"));
						break;
					case 38:
						i++;
						MKR.setObOPerPerOpuch(rs.getInt("value"));
						break;
					case 40:
						i++;
						MKR.setNalNeoadChimioTer((rs.getInt("value") == 1) ? true : false);
						break;
					case 41:
						i++;
						MKR.setChimiotKurs(rs.getInt("value"));
						break;
					case 42:
						i++;
						MKR.setChimiotPrep(rs.getInt("value"));
						break;
					case 43:
						i++;
						MKR.setChimiotEffect(rs.getInt("value"));
						break;
					case 44:
						i++;
						MKR.setPojavMetType(rs.getInt("value"));
						break;
					case 45:
						i++;
						MKR.setPojavMetTime(rs.getInt("value"));
						break;
					case 26: i++; MKR.setRezekciaType(rs.getInt("value")); break;
				}
				if (i == 23) {
					mkrList.add(MKR);
					MKR = new MKR();
				}
			} while (rs.next());
			return mkrList;
		}
	}
}
