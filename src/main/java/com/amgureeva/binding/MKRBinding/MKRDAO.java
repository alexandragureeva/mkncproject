package com.amgureeva.binding.MKRBinding;

import com.amgureeva.Entities.Diagnosis.MKR;

import java.util.List;

/**
 * Created by Александра on 04.05.2016.
 */
public interface MKRDAO {
    MKR findById(Integer id, boolean isFirstDiagnosis);

    List<MKR> findAll();

    void save(MKR patient);

    void update(MKR patient);

    void delete(Integer id);
}
