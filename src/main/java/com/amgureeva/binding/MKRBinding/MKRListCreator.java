package com.amgureeva.binding.MKRBinding;

import com.amgureeva.Entities.Diagnosis.MKR;
import com.amgureeva.Entities.Diagnosis.MKRList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 05.05.2016.
 */
public class MKRListCreator {

    public List<MKRList> createMKRList (List<MKR> mkrs, PatientService patientService) {
        if (mkrs != null && patientService != null) {
            List<MKRList> mkrList = new ArrayList<MKRList>();
            MKRList mkrList_ = new MKRList();
            for (int i = 0; i < mkrs.size(); i++) {
                mkrList_.setName(patientService.findPatientById(Integer.valueOf(mkrs.get(i).getId())).getName());
                switch (Integer.valueOf(mkrs.get(i).getStadPervOpuch())) {
                    case 0:
                        mkrList_.setStadPervOpuch("--");
                        break;
                    case 1:
                        mkrList_.setStadPervOpuch("T1N0M0");
                        break;
                    case 2:
                        mkrList_.setStadPervOpuch("T2N0M0");
                        break;
                    case 3:
                        mkrList_.setStadPervOpuch("T3N0M0");
                        break;
                    case 4:
                        mkrList_.setStadPervOpuch("T4аN0M0");
                        break;
                    case 5:
                        mkrList_.setStadPervOpuch("T4bN0M0");
                        break;
                    case 6:
                        mkrList_.setStadPervOpuch("T1N1M0");
                        break;
                    case 7:
                        mkrList_.setStadPervOpuch("T2N1M0");
                        break;
                    case 8:
                        mkrList_.setStadPervOpuch("T1N2aM0");
                        break;
                    case 9:
                        mkrList_.setStadPervOpuch("T3N1M0");
                        break;
                    case 10:
                        mkrList_.setStadPervOpuch("T4aN1M0");
                        break;
                    case 11:
                        mkrList_.setStadPervOpuch("T2N2aM0");
                        break;
                    case 12:
                        mkrList_.setStadPervOpuch("T3N2aM0");
                        break;
                    case 13:
                        mkrList_.setStadPervOpuch("T1N2bM0");
                        break;
                    case 14:
                        mkrList_.setStadPervOpuch("T2N2bM0");
                        break;
                    case 15:
                        mkrList_.setStadPervOpuch("T4aN2aM0");
                        break;
                    case 16:
                        mkrList_.setStadPervOpuch("T3N2bM0");
                        break;
                    case 17:
                        mkrList_.setStadPervOpuch("T4aN2bM0");
                        break;
                    case 18:
                        mkrList_.setStadPervOpuch("T4bN1M0");
                        break;
                    case 19:
                        mkrList_.setStadPervOpuch("T4bN2M0");
                        break;
                    case 20:
                        mkrList_.setStadPervOpuch("Любая T, любая N, M1a");
                        break;
                    case 21:
                        mkrList_.setStadPervOpuch("Любая T, любая N, M1b");
                        break;
                }

                switch (Integer.valueOf(mkrs.get(i).getPervOpuch())) {
                    case 0:
                        mkrList_.setPervOpuch("--");
                        break;
                    case 1:
                        mkrList_.setPervOpuch("Ободочная кишка");
                        break;
                    case 2:
                        mkrList_.setPervOpuch("Прямая кишка");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getObOPerPerOpuch())) {
                    case 0:
                        mkrList_.setObOPerPerOpuch("--");
                        break;
                    case 1:
                        mkrList_.setObOPerPerOpuch("Правосторонняя гемиколэктомия");
                        break;
                    case 2:
                        mkrList_.setObOPerPerOpuch("Резекция поперечной ободочной кишки");
                        break;
                    case 3:
                        mkrList_.setObOPerPerOpuch("Левосторонняя гемиколэктомия");
                        break;
                    case 4:
                        mkrList_.setObOPerPerOpuch("Резекция сигмовидной кишки");
                        break;
                    case 5:
                        mkrList_.setObOPerPerOpuch("Передняя резекция прямой кишки");
                        break;
                    case 6:
                        mkrList_.setObOPerPerOpuch("Низкая передняя резекция прямой кишки");
                        break;
                    case 7:
                        mkrList_.setObOPerPerOpuch("Брюшноанальная резекция прямой кишки");
                        break;
                    case 8:
                        mkrList_.setObOPerPerOpuch("Экстирпация прямой кишки");
                        break;
                    case 9:
                        mkrList_.setObOPerPerOpuch("Другие");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getNalVnepMet())) {
                    case 0:
                        mkrList_.setNalVnepMet("--");
                        break;
                    case 1:
                        mkrList_.setNalVnepMet("нет");
                        break;
                    case 2:
                        mkrList_.setNalVnepMet("В легких");
                        break;
                    case 3:
                        mkrList_.setNalVnepMet("В лимфатических уздах бр.полости");
                        break;
                    case 4:
                        mkrList_.setNalVnepMet("По брюшине");
                        break;
                    case 5:
                        mkrList_.setNalVnepMet("В др.органах бр.полости");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getRazmerOpucholi())) {
                    case 0:
                        mkrList_.setRazmerOpucholi("--");
                        break;
                    case 1:
                        mkrList_.setRazmerOpucholi("<10см");
                        break;
                    case 2:
                        mkrList_.setRazmerOpucholi(">10см");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getOpuchUzliType())) {
                    case 0:
                        mkrList_.setOpuchUzliType("--");
                        break;
                    case 1:
                        mkrList_.setOpuchUzliType("Солитарный");
                        break;
                    case 2:
                        mkrList_.setOpuchUzliType("Множественные");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getChisloOpuchUzlov())) {
                    case 0:
                        mkrList_.setChisloOpuchUzlov("--");
                        break;
                    case 1:
                        mkrList_.setChisloOpuchUzlov("0");
                        break;
                    case 2:
                        mkrList_.setChisloOpuchUzlov("1");
                        break;
                    case 3:
                        mkrList_.setChisloOpuchUzlov("2");
                        break;
                    case 4:
                        mkrList_.setChisloOpuchUzlov("3");
                        break;
                    case 5:
                        mkrList_.setChisloOpuchUzlov("4");
                        break;
                    case 6:
                        mkrList_.setChisloOpuchUzlov("5");
                        break;
                    case 7:
                        mkrList_.setChisloOpuchUzlov(">5");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getSosudInvasia())) {
                    case 0:
                        mkrList_.setSosudInvasia("--");
                        break;
                    case 1:
                        mkrList_.setSosudInvasia("нет");
                        break;

                    case 2:
                        mkrList_.setSosudInvasia("Микрососудистая");
                        break;
                    case 3:
                        mkrList_.setSosudInvasia("Мaкрососудистая");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getPojavMetType())) {
                    case 0:
                        mkrList_.setPojavMetType("--");
                        break;
                    case 1:
                        mkrList_.setPojavMetType("Синхронные");
                        break;
                    case 2:
                        mkrList_.setPojavMetType("Метахронные");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getPojavMetTime())) {
                    case 0:
                        mkrList_.setPojavMetTime("--");
                        break;
                    case 1:
                        mkrList_.setPojavMetTime("< 1 года");
                        break;
                    case 2:
                        mkrList_.setPojavMetTime("> 1 года");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getIcg())) {
                    case 0:
                        mkrList_.setIcg("--");
                        break;
                    case 1:
                        mkrList_.setIcg("<15%");
                        break;
                    case 2:
                        mkrList_.setIcg("15-25%");
                        break;
                    case 3:
                        mkrList_.setIcg(">25%");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getIcgk_f())) {
                    case 0:
                        mkrList_.setIcgk_f("--");
                        break;
                    case 1:
                        mkrList_.setIcgk_f("<0.4");
                        break;
                    case 2:
                        mkrList_.setIcgk_f("0.4-0.6");
                        break;
                    case 3:
                        mkrList_.setIcgk_f(">0.6");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getVnepOchagiType())) {
                    case 0:
                        mkrList_.setVnepOchagiType("--");
                        break;
                    case 1:
                        mkrList_.setVnepOchagiType("нет");
                        break;
                    case 2:
                        mkrList_.setVnepOchagiType("Отдаленные метастазы");
                        break;
                    case 3:
                        mkrList_.setVnepOchagiType("Прорастание соседних органов");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getTrombPechType())) {
                    case 0:
                        mkrList_.setTrombPechType("--");
                        break;
                    case 1:
                        mkrList_.setTrombPechType("Нет");
                        break;
                    case 2:
                        mkrList_.setTrombPechType("Да");
                        break;
                    case 3:
                        mkrList_.setTrombPechType("С выходом в нижнюю полую вену");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getTrombozVorotVeni())) {
                    case 0:
                        mkrList_.setTrombozVorotVeni("--");
                        break;
                    case 1:
                        mkrList_.setTrombozVorotVeni("0");
                        break;
                    case 2:
                        mkrList_.setTrombozVorotVeni("1");
                        break;
                    case 3:
                        mkrList_.setTrombozVorotVeni("2");
                        break;
                    case 4:
                        mkrList_.setTrombozVorotVeni("3");
                        break;
                    case 5:
                        mkrList_.setTrombozVorotVeni("4");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getTrofStatus())) {
                    case 0:
                        mkrList_.setTrofStatus("--");
                        break;
                    case 1:
                        mkrList_.setTrofStatus("Хороший");
                        break;
                    case 2:
                        mkrList_.setTrofStatus("Удовлетворительный");
                        break;
                    case 3:
                        mkrList_.setTrofStatus("Неудовлетворительный");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getChimiotKurs())) {
                    case 0:
                        mkrList_.setChimiotKurs("--");
                        break;
                    case 1:
                        mkrList_.setChimiotKurs("4 курса");
                        break;
                    case 2:
                        mkrList_.setChimiotKurs("6-8 курсов");
                        break;
                    case 3:
                        mkrList_.setChimiotKurs(">8 курсов");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getChimiotPrep())) {
                    case 0:
                        mkrList_.setChimiotPrep("--");
                        break;
                    case 1:
                        mkrList_.setChimiotPrep("Оксалиплатин");
                        break;
                    case 2:
                        mkrList_.setChimiotPrep("Иринотекан");
                        break;
                    case 3:
                        mkrList_.setChimiotPrep("Цитостатики и авастин");
                        break;
                    case 4:
                        mkrList_.setChimiotPrep("Цитостатики и эрбитукс (виктибикс)");
                        break;
                    case 5:
                        mkrList_.setChimiotPrep("Цитостатики и другой таргетный препарат");
                        break;
                    case 6:
                        mkrList_.setChimiotPrep("FOLFOXIRI");
                        break;
                }
                switch (Integer.valueOf(mkrs.get(i).getChimiotEffect())) {
                    case 0:
                        mkrList_.setChimiotEffect("--");
                        break;
                    case 1:
                        mkrList_.setChimiotEffect("Полный ответ");
                        break;
                    case 2:
                        mkrList_.setChimiotEffect("Частичный ответ");
                        break;
                    case 3:
                        mkrList_.setChimiotEffect("Стабилизация");
                        break;
                    case 4:
                        mkrList_.setChimiotEffect("Прогрессия");
                        break;
                }
                if (mkrs.get(i).getIndexZachvata().equals("null"))
                    mkrList_.setIndexZachvata("--");
                else
                    mkrList_.setIndexZachvata(mkrs.get(i).getIndexZachvata());
                if (mkrs.get(i).getObjemOstatka().equals("null"))
                    mkrList_.setObjemOstatka("--");
                else
                    mkrList_.setObjemOstatka(mkrs.get(i).getObjemOstatka());
                if (mkrs.get(i).isNalNeoadChimioTer() == false)
                    mkrList_.setNalNeoadChimioTer("-");
                else
                    mkrList_.setNalNeoadChimioTer("+");
                mkrList.add(mkrList_);
                mkrList_ = new MKRList();
            }

            return mkrList;
        }
        return  null;
    }

    public static String getAllVariants(String s) {
        s = s.replace("_ПОСТ", "");
        s = s.replace("_ВЫПИС", "");
        s = s.replace("_ОПЕР", "");
        if (s.equals("mkr:ICG")) {
            return "1 - \"<15%\"; 2 - \"15-25%\"; 3 - \">25%\"";
        }
        if (s.equals("mkr:ICGK-F")) {
            return "1 - \"<0.4\"; 2 - \"0.4-0.6\"; 3 -\">0.6\"";
        }
        if (s.equals("mkr:INVASIA_VOROTNAYA")) {
            return "1 - \"В ствол\"; 2 - \"В долевые ветви\"; 3 -\"В сегментарные ветви\"";
        }
        if (s.equals("mkr:INVASIYA_PECHENOCHNAYA")) {
            return "1 - \"Ипсилатеральную\"; 2 - \"Контрлатеральную\"; 3 -\"В собственную печеночную\"";
        }
        if (s.equals("mkr:OCENKA_TROF_STATUSA")) {
            return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 -\"Неудовлетворительный\"";
        }
        if (s.equals("mkr:MAX_OPUCHOL")) {
            return "1 - \"<10см\"; 2 - \">10см\"";
        }
        if (s.equals("mkr:ONKOMARKER")) {
            return "1 - \"CA\"; 2 - \"19-9\"; 3 -\"CEA\"";
        }
        if (s.equals("mkr:TROMBOZ_PECHEN_VEN")) {
            return "1 - \"нет\"; 2 - \"Да\"; 3 -\"С выходом в нижнюю полую вену\"";
        }
        if (s.equals("mkr:TROMBOZ_VOROTNOI_VENI")) {
            return "1 - \"0\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"; 5 - \"4\"";
        }
        if (s.equals("mkr:VNEPECHENOCH_OCHAGI")) {
            return "1 - \"нет\"; 2 - \"Отдаленные метастазы\"; 3 -\"Прорастание соседних органов\"";
        }
        if (s.equals("mkr:GRWR")) {
            return "1 - \"<0.8\"; 2 - \"0.8-1\"; 3 -\">1\"";
        }
        if (s.equals("mkr:OPUCH_UZLI_CHISLO")) {
            return "1 - \"0\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"; 5 - \"4\"; 6 - \"5\"; 7 - \">5\"";
        }
        if (s.equals("mkr:OPUCH_UZLI_TYPE")) {
            return "1 - \"Солитарный\"; 2 - \"Множественные\"";
        }
        if (s.equals("mkr:NEOADJUV_TERAPIA_EFFECT")) {
            return "1 - \"Полный ответ\"; 2 - \"Частичный ответ\"; 3 -\"Стабилизация\"; 4 - \"Прогрессия\"";
        }
        if (s.equals("mkr:NEOADJUV_TERAPIA_KURS")) {
            return "1 - \"4 курса\"; 2 - \"6-8 курсов\"; 3 -\">8 курсов\"";
        }
        if (s.equals("mkr:NEOADJUV_TERAPIA_PREPARATI")) {
            return "1 - \"Оксалиплатин\"; 2 - \"Иринотекан\"; 3 -\"Цитостатики и авастин\"; 4 - \"Цитостатики и эрбитукс (виктибикс)\"; " +
                    "5 - \"Цитостатики и другой таргетный препарат\"; 6 - \"FOLFOXIRI\"";
        }
        if (s.equals("mkr:OBIEM_OPERACII_PERV_OPUCHOLI")) {
            return "1 - \"Правосторонняя гемиколэктомия\"; 2 - \"Резекция поперечной ободочной кишки\"; " +
                    "3 -\"Левосторонняя гемиколэктомия\"; 4 - \"Резекция сигмовидной кишки\"; " +
                    "5 - \"Передняя резекция прямой кишки\"; 6 - \"Низкая передняя резекция прямой кишки\"" +
                    "7 - \"Брюшноанальная резекция прямой кишки\"; 8 - \"Экстирпация прямой кишки\"' 9 - \"Другие\"";
        }
        if (s.equals("mkr:PERVICH_OPUCHOL")) {
            return "1 - \"T1N0M0\"; 2 - \"T2N0M0\"; 3 -\"T3N0M0\"; 4 - \"T4аN0M0\"; 5 - \"T4bN0M0\"; 6 - \"T1N1M0\";" +
                    " 7 - \"T2N1M0\"; 8 - \"T1N2aM0\";  9 - \"T3N1M0\";  10 - \"T4aN1M0\";  11 - \"T2N2aM0\"; " +
                    " 12 - \"T3N2aM0\";  13 - \"T1N2bM0\";  14 - \"T2N2bM0\"; " +
                    " 15 - \"T4aN2aM0\";  16 - \"T3N2bM0\";  17 - \"T4aN2bM0\";  18 - \"T4bN1M0\";  19 - \"T4bN2M0\"; " +
                    " 20 - \"Любая T, любая N, M1a\";  21 - \"Любая T, любая N, M1b\";  ";
        }
        if (s.equals("mkr:POJAVLENIE_METASTAZOV_TYPE")) {
            return "1 - \"Синхронные\"; 2 - \"Метахронные\"";
        }
        if (s.equals("mkr:POJAVLENIE_METASTAZOV_VREMYA")) {
            return "1 - \"< 1 года\"; 2 - \"> 1 года\"";
        }
        if (s.equals("mkr:VNEPECH_METASTAZ")) {
            return "1 - \"нет\"; 2 - \"В легких\"; 3 -\"В лимфатических уздах бр.полости\"; 4 - \"По брюшине\";" +
                    " 5 - \"В др.органах бр.полости\"";
        }
        if (s.equals("mkr:KRATNOST_REZEKCII")) {
            return "1 - \"Первичная\"; 2 - \"Вторая\"; 3 -\"Третья\"; 4 -\"Четвертая\"";
        }
        if (s.equals("mkr:OBJEM_REZEKCII")) {
            return "1 - \"Обширная резекция ЛГГЭ\"; 2 - \"Обширная резекция ПГГЭ\";" +
                    " 3 -\"Cегментарная резекция\"";
        }
        if (s.equals("mkr:SEGMENT_REZEKCIYA")) {
            return "1 - \"Анатомическая\"; 2 - \"Неанатомическая\"";
        }
        if (s.equals("mkr:VARIANT_REZEKCII")) {
            return "1 - \"Открытая\"; 2 - \"Лапароскопическая\"";
        }

        if (s.equals("mkr:SIMULT_OPERACIA")) {
            return "1 - \"Резекция толстой кишки\"; 2 - \"Резекция прямой кишки\";" +
                    " 3 -\"Резекция диафрагмы\"; 4 -\"Резекция других органов(тонкой кишки и др)\" ";
        }
        if (s.equals("mkr:OSLOZH")) {
            return "1 - \"Внутрибрюшное кровотечение\"; 2 - \"ЖКК\"; 3 - \"Печеночная недостаточность A\";" +
                    " 4 - \"Печеночная недостаточность B\"; 5 - \"Печеночная недостаточность C\";" +
                    " 6 - \"Механическая желтуха\"; 7 - \"Нагноение раны\"; 8 - \"Холангит\";" +
                    " 9 - \"Холангиогенное абсцедирование\"; 10 - \"Асцит\"; 11 - \"Эвентрация\"" +
                    "; 12 - \"Кишечная непроходимость\"; 13 - \"Панкреатический свищ\"; 14 - \"Острый панкреатит\";" +
                    " 15 - \"Сепсис\"; 16 - \"Тяжелый сепсис\"; 17 - \"Септический шок\"; 18 - \"Желчный свищ\"" +
                    "; 19 - \"Кишечный свищ\"; 20 - \"Тромбоз воротной вены\"; 21 - \"Тромбоз печеночной артерии\";" +
                    " 22 - \"Односторонняя пневмония\"" +
                    "; 23 - \"Двусторонняя пневмония\"; 24 - \"Гидроторакс\"; 25 - \"Почечная недостаточность\"" +
                    "; 26 - \"Сердечно-сосудистая недостаточность\"; 27 - \"Полиорганная недостаточность\"";
        }
        if (s.equals("mkr:CLAVIEN")) {
            return "1 - \"I\"; 2 - \"II\"; 3 - \"IIIa\"; 4 - \"IIIb\"; 5 - \"IVa\"; 6 - \"IVb\"; 7 - \"V\"";
        }

        if (s.equals("mkr:NALICHIE_RCHA")) {
            return "1 - \"нет\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"";
        }
        if (s.equals("mkr:SCINTIGRAF_OBJ_OPERAC")) {
            return "1 - \"Сегментарная\"; 2 - \"Обширная\"";
        }
        if (s.equals("mkr:SOSUD_INVASIA")) {
            return "1 - \"Нет\"; 2 - \"Микрососудистая инвазия\"; 3 -\"Макрососудистая инвазия\"";
        }
        return "";
    }
}
