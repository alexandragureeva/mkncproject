package com.amgureeva.binding.ObjExaminationBinding;

import com.amgureeva.Entities.ObjectiveExamination;

import java.util.List;

/**
 * Created by Nikita on 16.08.2016.
 */
public interface ObjExaminationService {
	ObjectiveExamination findById(Integer id);

	List<ObjectiveExamination> findAll();

	void saveOrUpdate(ObjectiveExamination objectiveExamination, boolean isNew);

	void delete(int id);
}
