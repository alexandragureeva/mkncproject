package com.amgureeva.binding.ObjExaminationBinding;

import com.amgureeva.Entities.ObjectiveExamination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 16.08.2016.
 */
@Repository
public class ObjExaminationDAOImpl implements ObjExaminationDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public ObjectiveExamination findById(Integer id) {
		if (id == null) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);

		String sql = "SELECT * FROM objexamination WHERE id=:id";

		ObjectiveExamination result = null;
		try {
			result = namedParameterJdbcTemplate
					.queryForObject(sql, params, new ObjExaminationMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}

		return result;
	}

	@Override
	public List<ObjectiveExamination> findAll() {
		String sql = "SELECT * FROM objexamination";
		List<ObjectiveExamination> result = namedParameterJdbcTemplate.query(sql, new ObjExaminationDAOImpl.ObjExaminationMapper());
		return result;
	}

	@Override
	public void save(ObjectiveExamination objectiveExamination) {

		String sql = "INSERT INTO objexamination(id, patientState,temperature,nalichieZheltuchi,odishka,puls,tachikardia," +
				"davlenie,nalichieToshnoti,rvota,zhivot,boleznenostZhivota,napryazhenieZhivota,peritorialSymptomi," +
				"peristaltika,otchozhdenieGazov,stul,dizuria,objemMochi) " +
				"VALUES (:id, :patientState,:temperature,:nalichieZheltuchi,:odishka,:puls,:tachikardia," +
				":davlenie,:nalichieToshnoti,:rvota,:zhivot,:boleznenostZhivota,:napryazhenieZhivota," +
				":peritorialSymptomi,:peristaltika,:otchozhdenieGazov,:stul,:dizuria,:objemMochi)";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(objectiveExamination));
	}

	@Override
	public void update(ObjectiveExamination objectiveExamination) {
		String sql = "UPDATE objexamination SET id=:id, patientState=:patientState,temperature=:temperature," +
				"nalichieZheltuchi=:nalichieZheltuchi,odishka=:odishka,puls=:puls,tachikardia=:tachikardia," +
				"davlenie=:davlenie,nalichieToshnoti=:nalichieToshnoti,rvota=:rvota,zhivot=:zhivot," +
				"boleznenostZhivota=:boleznenostZhivota,napryazhenieZhivota=:napryazhenieZhivota," +
				"peritorialSymptomi=:peritorialSymptomi,peristaltika=:peristaltika,otchozhdenieGazov=:otchozhdenieGazov," +
				"stul=:stul,dizuria=:dizuria,objemMochi=:objemMochi" +
				" WHERE id=:id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(objectiveExamination));
	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM objexamination WHERE id= :id";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	private SqlParameterSource getSqlParameterByModel(ObjectiveExamination objEx) {

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", objEx.getId());
		paramSource.addValue("patientState", objEx.getPatientState());
		paramSource.addValue("temperature", objEx.getTemperature());
		paramSource.addValue("nalichieZheltuchi", objEx.getNalichieZheltuchi());
		paramSource.addValue("odishka", objEx.getOdishka());
		paramSource.addValue("puls", objEx.getPuls());
		paramSource.addValue("tachikardia", objEx.getTachikardia());
		paramSource.addValue("davlenie", objEx.getDavlenie());
		paramSource.addValue("nalichieToshnoti", objEx.getNalichieToshnoti());
		paramSource.addValue("rvota", objEx.getRvota());
		paramSource.addValue("zhivot", objEx.getZhivot());
		paramSource.addValue("boleznenostZhivota", objEx.getBoleznenostZhivota());
		paramSource.addValue("napryazhenieZhivota", objEx.getNapryazhenieZhivota());
		paramSource.addValue("peritorialSymptomi", objEx.getPeritorialSymptomi());
		paramSource.addValue("peristaltika", objEx.getPeristaltika());
		paramSource.addValue("otchozhdenieGazov", objEx.getOtchozhdenieGazov());
		paramSource.addValue("stul", objEx.getStul());
		paramSource.addValue("dizuria", objEx.getDizuria());
		paramSource.addValue("objemMochi", objEx.getObjemMochi());


		return paramSource;
	}

	private static final class ObjExaminationMapper implements RowMapper<ObjectiveExamination> {

		public ObjectiveExamination mapRow(ResultSet rs, int rowNum) throws SQLException {
			ObjectiveExamination objEx = new ObjectiveExamination();
			objEx.setId(rs.getString("id"));
			objEx.setPatientState(rs.getInt("patientState"));
			objEx.setTemperature(rs.getInt("temperature"));
			objEx.setNalichieZheltuchi(rs.getInt("nalichieZheltuchi"));
			objEx.setOdishka(rs.getInt("odishka"));
			objEx.setPuls(rs.getInt("puls"));
			objEx.setTachikardia(rs.getInt("tachikardia"));
			objEx.setDavlenie(rs.getString("davlenie"));
			objEx.setNalichieToshnoti(rs.getInt("nalichieToshnoti"));
			objEx.setRvota(rs.getInt("rvota"));
			objEx.setZhivot(rs.getInt("zhivot"));
			objEx.setBoleznenostZhivota(rs.getInt("boleznenostZhivota"));
			objEx.setNapryazhenieZhivota(rs.getInt("napryazhenieZhivota"));
			objEx.setPeritorialSymptomi(rs.getInt("peritorialSymptomi"));
			objEx.setPeristaltika(rs.getInt("peristaltika"));
			objEx.setOtchozhdenieGazov(rs.getInt("otchozhdenieGazov"));
			objEx.setStul(rs.getInt("stul"));
			objEx.setDizuria(rs.getInt("dizuria"));
			objEx.setObjemMochi(rs.getInt("objemMochi"));


			return objEx;
		}
	}

}
