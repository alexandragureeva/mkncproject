package com.amgureeva.binding.ObjExaminationBinding;

import com.amgureeva.Entities.ObjectiveExamination;

import java.util.List;

/**
 * Created by Nikita on 16.08.2016.
 */
public interface ObjExaminationDAO {
	ObjectiveExamination findById(Integer id);

	List<ObjectiveExamination> findAll();

	void save(ObjectiveExamination objectiveExamination);

	void update(ObjectiveExamination objectiveExamination);

	void delete(Integer id);
}
