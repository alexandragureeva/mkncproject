package com.amgureeva.binding.ObjExaminationBinding;

import com.amgureeva.Entities.ObjectiveExamination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Nikita on 16.08.2016.
 */
@Service("objExamService")
public class ObjExaminationServiceImpl implements ObjExaminationService {

	ObjExaminationDAO objExaminationDAO;

	@Autowired
	public void setAnalysisDAO(ObjExaminationDAO objExaminationDAO) {
		this.objExaminationDAO = objExaminationDAO;
	}


	@Override
	public ObjectiveExamination findById(Integer id) {
		return objExaminationDAO.findById(id);
	}

	@Override
	public List<ObjectiveExamination> findAll() {
		return objExaminationDAO.findAll();
	}

	@Override
	public void saveOrUpdate(ObjectiveExamination objectiveExamination, boolean isNew) {
		if (isNew) {
			objExaminationDAO.save(objectiveExamination);
		}
		else {
			objExaminationDAO.update(objectiveExamination);
		}
	}

	@Override
	public void delete(int id) {
		  objExaminationDAO.delete(id);
	}
}
