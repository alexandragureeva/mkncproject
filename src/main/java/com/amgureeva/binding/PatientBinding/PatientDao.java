package com.amgureeva.binding.PatientBinding;

import com.amgureeva.Entities.Patient;

import java.util.List;

/**
 * Created by Александра on 11.04.2016.
 */
public interface PatientDao {

        Patient findById(Integer id);

        Patient findPatientByHisNumber(String hisNumber);

        List<Patient> findAll();

        List<Patient> findAllRecent();

        void save(Patient patient);

        void update(Patient patient);

        void saveEndInfo(Patient patient);

        void updateFillers(Patient patient);

        void delete(Integer id);

        String getMinDate();

        List<String> getDeathDates(int[] ids, String startDate, String endDate);

        List<String> getStartDates(int[] ids, String startDate, String endDate);

        void setTypeOfTreatment(boolean isTwoStep, int id);

}
