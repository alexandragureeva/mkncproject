package com.amgureeva.binding.PatientBinding;

import com.amgureeva.Entities.GeneralPatientInfo;
import com.amgureeva.Entities.Patient;
import com.amgureeva.Entities.Surgeries.Drenirovanie;
import com.amgureeva.binding.AlveokokozBinding.AlveokokkozDAO;
import com.amgureeva.binding.AnalysisBinding.AnalysisService;
import com.amgureeva.binding.DrenirovanieBinding.DrenirovanieDAO;
import com.amgureeva.binding.EchinokokkozBinding.EchinokokkozDAO;
import com.amgureeva.binding.EditorBinding.EditorDAO;
import com.amgureeva.binding.GCRBinding.GCRDAO;
import com.amgureeva.binding.HCRBinding.HCRDAO;
import com.amgureeva.binding.KlatskinBinding.KlatskinDAO;
import com.amgureeva.binding.MKRBinding.MKRDAO;
import com.amgureeva.binding.MNKRBinding.MNKRDAO;
import com.amgureeva.binding.ObjExaminationBinding.ObjExaminationService;
import com.amgureeva.binding.OpAlveokokkozBinding.OpAlveokokkozDAO;
import com.amgureeva.binding.OpEchinokokkozBinding.OpEchinokokkozDAO;
import com.amgureeva.binding.OpGCRBinding.OpGCRDAO;
import com.amgureeva.binding.OpHCRBinding.OpHCRDAO;
import com.amgureeva.binding.OpKlatskinBinding.OpKlatskinDAO;
import com.amgureeva.binding.OpMKRBinding.OpMKRDAO;
import com.amgureeva.binding.OpMNKRBinding.OpMNKRDAO;
import com.amgureeva.binding.OpRZHPBinding.OpRZHPDAO;
import com.amgureeva.binding.OpZHKBBinding.OpZHKBDAO;
import com.amgureeva.binding.PunkciaBinding.PunkciaDAO;
import com.amgureeva.binding.RZHPBinding.RZHPDAO;
import com.amgureeva.binding.ZHKBBinding.ZHKBDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 11.04.2016.
 */
@Service("patientService")
public class PatientServiceImpl implements PatientService {

    PatientDao patientDao;
    KlatskinDAO klatskinDao;
    HCRDAO hcrDao;
    GCRDAO gcrdao;
    MKRDAO mkrdao;
    MNKRDAO mnkrdao;
    EchinokokkozDAO echinokokkozDAO;
    AlveokokkozDAO alveokokkozDAO;
    RZHPDAO rzhpdao;
	ZHKBDAO zhkbdao;
    OpKlatskinDAO opKlatskinDAO;
    OpHCRDAO opHCRDAO;
    OpGCRDAO opGCRDAO;
    OpMKRDAO opMKRDAO;
    OpMNKRDAO opMNKRDAO;
    OpEchinokokkozDAO opEchinokokkozDAO;
    OpAlveokokkozDAO opAlveokokkozDAO;
    OpRZHPDAO opRZHPDAO;
	OpZHKBDAO opZHKBDAO;
    DrenirovanieDAO drenirovanieDAO;
	PunkciaDAO punkciaDAO;
    EditorDAO editorDAO;

	@Autowired
	AnalysisService analysisService;
	@Autowired
	ObjExaminationService objExaminationService;


    @Autowired
    public void setUserDao(PatientDao patientDao) {
        this.patientDao = patientDao;
    }

    @Autowired
    public void setKlatskinDao(KlatskinDAO klatskinDao) {
        this.klatskinDao = klatskinDao;
    }

    @Autowired
    public void setHCRDao(HCRDAO hcrDao) {
        this.hcrDao = hcrDao;
    }

    @Autowired
    public void setGcrdao(GCRDAO gcrDao) {
        this.gcrdao = gcrDao;
    }

    @Autowired
    public void setMkrdao(MKRDAO mkrDao) {
        this.mkrdao = mkrDao;
    }

    @Autowired
    public void setMnkrdao(MNKRDAO mnkrDao) {
        this.mnkrdao = mnkrDao;
    }

    @Autowired
    public void setEchinokokkozDAO(EchinokokkozDAO echinokokkozDAO) {
        this.echinokokkozDAO = echinokokkozDAO;
    }

    @Autowired
    public void setAlveokokkozDAO(AlveokokkozDAO alveokokkozDAO) {
        this.alveokokkozDAO = alveokokkozDAO;
    }

    @Autowired
    public void setRzhpdao(RZHPDAO rzhpdao) {
        this.rzhpdao = rzhpdao;
    }


	@Autowired
	public void setZhkbdao(ZHKBDAO zhkbdao) {
		this.zhkbdao = zhkbdao;
	}
    @Autowired
    public void setOpKlatskinDao(OpKlatskinDAO opklatskinDao) {
        this.opKlatskinDAO = opklatskinDao;
    }

    @Autowired
    public void setOpHCRDao(OpHCRDAO ophcrDao) {
        this.opHCRDAO = ophcrDao;
    }

    @Autowired
    public void setOpGcrdao(OpGCRDAO opgcrDao) {
        this.opGCRDAO = opgcrDao;
    }

    @Autowired
    public void setOpMkrdao(OpMKRDAO opmkrDao) {
        this.opMKRDAO = opmkrDao;
    }

    @Autowired
    public void setOpMnkrdao(OpMNKRDAO opmnkrDao) {
        this.opMNKRDAO = opmnkrDao;
    }

    @Autowired
    public void setOpEchinokokkozDAO(OpEchinokokkozDAO opechinokokkozDAO) {
        this.opEchinokokkozDAO = opechinokokkozDAO;
    }

    @Autowired
    public void setOpAlveokokkozDAO(OpAlveokokkozDAO opalveokokkozDAO) {
        this.opAlveokokkozDAO = opalveokokkozDAO;
    }

    @Autowired
    public void setOpRzhpdao(OpRZHPDAO oprzhpdao) {
        this.opRZHPDAO = oprzhpdao;
    }

	@Autowired
	public void  setOpZHKBDAO (OpZHKBDAO opZHKBDAO) {this.opZHKBDAO = opZHKBDAO;}

    @Autowired
    public void setEditorDAO(EditorDAO editorDAO) {this.editorDAO = editorDAO;}

    @Autowired
    public void setPunkciaDAO(PunkciaDAO punkciaDAO) {
        this.punkciaDAO = punkciaDAO;
    }


	@Autowired
	public void setDrenirovanieDAO(DrenirovanieDAO drenirovanieDAO) {
		this.drenirovanieDAO = drenirovanieDAO;
	}

    @Override
    public GeneralPatientInfo findById(Integer id) {
        GeneralPatientInfo gpi = new GeneralPatientInfo();
        /*gpi.setKlatskin(klatskinDao.findById(id));
        gpi.setHcr(hcrDao.findById(id));
        gpi.setGcr(gcrdao.findById(id));
        gpi.setMkr(mkrdao.findById(id));
        gpi.setMnkr(mnkrdao.findById(id));
        gpi.setRzhp(rzhpdao.findById(id));
        gpi.setEchinokokkoz(echinokokkozDAO.findById(id));
        gpi.setAlveokokkoz(alveokokkozDAO.findById(id));
        gpi.setOpalv(opAlveokokkozDAO.findById(id));
        gpi.setOpech(opEchinokokkozDAO.findById(id));
        gpi.setOprzhp(opRZHPDAO.findById(id));
        gpi.setOpmnkr(opMNKRDAO.findById(id));
        gpi.setOpgcr(opGCRDAO.findById(id));
        gpi.setOphcr(opHCRDAO.findById(id));
        gpi.setOpkl(opKlatskinDAO.findById(id));
        gpi.setOpmkr(opMKRDAO.findById(id));*/
      //  gpi.setDren(drenirovanieDAO.findById(id));
        gpi.setPatient(patientDao.findById(id));
        return gpi;
    }

	@Override
	public Patient findPatientByHisNumber(String hisNumber) {
		return patientDao.findPatientByHisNumber(hisNumber);
	}


	@Override
    public List<GeneralPatientInfo> findAll() {
        List<GeneralPatientInfo> gpis = new ArrayList<GeneralPatientInfo>();
        List<Patient> patients = patientDao.findAll();
        for (Patient patient : patients) {
            GeneralPatientInfo gpi = new GeneralPatientInfo();
            gpi.setPatient(patient);
			gpi.setAnalysis0(analysisService.findById(Integer.valueOf(patient.getId()), 0));
			gpi.setAnalysis1(analysisService.findById(Integer.valueOf(patient.getId()), 1));
			gpi.setAnalysis5(analysisService.findById(Integer.valueOf(patient.getId()), 5));
			gpi.setAnalysis10(analysisService.findById(Integer.valueOf(patient.getId()), 10));
			gpi.setObjectiveExamination(objExaminationService.findById(Integer.valueOf(patient.getId())));
            switch (patient.getDiagnosis()) {
                case 1: gpi.setKlatskin(klatskinDao.findById(Integer.valueOf(patient.getId()), true)); break;        //
                case 2: gpi.setGcr(gcrdao.findById(Integer.valueOf(patient.getId()), true)); break;                   //
                case 3: gpi.setHcr(hcrDao.findById(Integer.valueOf(patient.getId()), true)); break;
                case 4: gpi.setMkr(mkrdao.findById(Integer.valueOf(patient.getId()), true)); break;
                case 5: gpi.setMnkr(mnkrdao.findById(Integer.valueOf(patient.getId()), true)); break;
                case 6: gpi.setRzhp(rzhpdao.findById(Integer.valueOf(patient.getId()), true)); break;
                case 7: gpi.setEchinokokkoz(echinokokkozDAO.findById(Integer.valueOf(patient.getId()), true)); break;
                case 8: gpi.setAlveokokkoz(alveokokkozDAO.findById(Integer.valueOf(patient.getId()), true)); break;
				case 9: gpi.setZhkb(zhkbdao.findById(Integer.valueOf(patient.getId()), true)); break;
            }
            switch (patient.getDiagnosis()) {
                case 1: gpi.setKlatskinEnd(klatskinDao.findById(Integer.valueOf(patient.getId()), false)); break;        //
                case 2: gpi.setGcrEnd(gcrdao.findById(Integer.valueOf(patient.getId()), false)); break;                   //
                case 3: gpi.setHcrEnd(hcrDao.findById(Integer.valueOf(patient.getId()), false)); break;
                case 4: gpi.setMkrEnd(mkrdao.findById(Integer.valueOf(patient.getId()), false)); break;
                case 5: gpi.setMnkrEnd(mnkrdao.findById(Integer.valueOf(patient.getId()), false)); break;
                case 6: gpi.setRzhpEnd(rzhpdao.findById(Integer.valueOf(patient.getId()), false)); break;
                case 7: gpi.setEchinokokkozEnd(echinokokkozDAO.findById(Integer.valueOf(patient.getId()), false)); break;
                case 8: gpi.setAlveokokkozEnd(alveokokkozDAO.findById(Integer.valueOf(patient.getId()), false)); break;
            }

            switch (patient.getDiagnosis()) {
                case 1: gpi.setOpkl(opKlatskinDAO.findById(Integer.valueOf(patient.getId()))); break;        //
                case 2: gpi.setOpgcr(opGCRDAO.findById(Integer.valueOf(patient.getId()))); break;                   //
                case 3: gpi.setOphcr(opHCRDAO.findById(Integer.valueOf(patient.getId()))); break;
                case 4: gpi.setOpmkr(opMKRDAO.findById(Integer.valueOf(patient.getId()))); break;
                case 5: gpi.setOpmnkr(opMNKRDAO.findById(Integer.valueOf(patient.getId()))); break;
                case 6: gpi.setOprzhp(opRZHPDAO.findById(Integer.valueOf(patient.getId()))); break;
                case 7: gpi.setOpech(opEchinokokkozDAO.findById(Integer.valueOf(patient.getId()))); break;
                case 8: gpi.setOpalv(opAlveokokkozDAO.findById(Integer.valueOf(patient.getId()))); break;
				case 9: gpi.setOpZhkb(opZHKBDAO.findById(Integer.valueOf(patient.getId()))); break;
            }
			gpi.setHasOsl(false);
            List<Drenirovanie> drains = drenirovanieDAO.findPatientAll(patient.getId());
			if (drains == null) {
				gpi.setDrainNumber(0);
				gpis.add(gpi);
			} else {
				gpi.setDrainNumber(drains.size());
				for (Drenirovanie drain : drains) {
					if (drain.getOsl() > 1) {
						gpi.setHasOsl(true);
					}
				}
				for (Drenirovanie drain : drains) {
					GeneralPatientInfo nextgpi = new GeneralPatientInfo(gpi);
					nextgpi.setDren(drain);
					gpis.add(nextgpi);
				}
			}

        }
        return gpis;
    }

    @Override
    public List<Patient> findAllPatients() {
        return patientDao.findAll();
    }

	@Override
	public List<Patient> findAllRecentPatients() {
		return patientDao.findAllRecent();
	}

	@Override
    public String saveOrUpdate(GeneralPatientInfo gpi)  {
        Patient p = null;
        if (gpi.getPatient().getId() != null) {
          p = findPatientById(Integer.valueOf(gpi.getPatient().getId()));
        }
        if (gpi.getPatient().getId() == null ||
                 p == null) {
            patientDao.save(gpi.getPatient());
        } else {
            Patient prevPatient = patientDao.findById(Integer.valueOf(gpi.getPatient().getId()));
			gpi.getPatient().setFilledDiagnosisAtStart(prevPatient.getFilledDiagnosisAtStart());
			gpi.getPatient().setFilledDiagnosisAtEnd(prevPatient.getFilledDiagnosisAtEnd());
			gpi.getPatient().setFilledAnalysis0(prevPatient.getFilledAnalysis0());
			gpi.getPatient().setFilledObjExam(prevPatient.getFilledObjExam());
			if (prevPatient.getHasSurgery() != gpi.getPatient().getHasSurgery()) {
				analysisService.delete(Integer.valueOf(gpi.getPatient().getId()), 1);
				analysisService.delete(Integer.valueOf(gpi.getPatient().getId()), 5);
				switch (prevPatient.getDiagnosis()) {
					case 0:
						break;
					case 1:
						opKlatskinDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
						break;
					case 2:
						opGCRDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
						break;
					case 3:
						opHCRDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
						break;
					case 4:
						opMKRDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
						break;
					case 5:
						opMNKRDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
						break;
					case 6:
						opRZHPDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
						break;
					case 7:
						opEchinokokkozDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
						break;
					case 8:
						opAlveokokkozDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
						break;
				}
			}
			if (!prevPatient.getDiagnosis().equals(gpi.getPatient().getDiagnosis())) {
                gpi.getPatient().setFilledDiagnosisAtStart(false);
                gpi.getPatient().setFilledDiagnosisAtEnd(false);
                updateFillers(gpi.getPatient());
                switch (prevPatient.getDiagnosis()) {
                    case 0:
                        break;
                    case 1:
                        klatskinDao.delete(Integer.valueOf(gpi.getPatient().getId()));
                        opKlatskinDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        break;
                    case 2:
                        gcrdao.delete(Integer.valueOf(gpi.getPatient().getId()));
                        opGCRDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        break;
                    case 3:
                        hcrDao.delete(Integer.valueOf(gpi.getPatient().getId()));
                        opHCRDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        break;
                    case 4:
                        mkrdao.delete(Integer.valueOf(gpi.getPatient().getId()));
                        opMKRDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        break;
                    case 5:
                        mnkrdao.delete(Integer.valueOf(gpi.getPatient().getId()));
                        opMNKRDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        break;
                    case 6:
                        rzhpdao.delete(Integer.valueOf(gpi.getPatient().getId()));
                        opRZHPDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        break;
                    case 7:
                        echinokokkozDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        opEchinokokkozDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        break;
                    case 8:
                        alveokokkozDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        opAlveokokkozDAO.delete(Integer.valueOf(gpi.getPatient().getId()));
                        break;
                }
            }
			patientDao.update(gpi.getPatient());
		}
        return gpi.getPatient().getId();
    }

	@Override
	public void updateEndInfo(Patient patient) {
		patientDao.saveEndInfo(patient);
	}

	@Override
    public void updateFillers(Patient patient) {
        patientDao.updateFillers(patient);
    }

	@Override
	public void setTypeOfTreatment(boolean isTwoStep, int id) {
		  patientDao.setTypeOfTreatment(isTwoStep, id);
	}

	@Override
    public void delete(int id) {
        klatskinDao.delete(id);
        hcrDao.delete(id);
        gcrdao.delete(id);
        mkrdao.delete(id);
        mnkrdao.delete(id);
        rzhpdao.delete(id);
        echinokokkozDAO.delete(id);
        alveokokkozDAO.delete(id);
		zhkbdao.delete(id);
        opKlatskinDAO.delete(id);
        opHCRDAO.delete(id);
        opGCRDAO.delete(id);
        opMKRDAO.delete(id);
        opMNKRDAO.delete(id);
        opRZHPDAO.delete(id);
        opAlveokokkozDAO.delete(id);
        opEchinokokkozDAO.delete(id);
		opZHKBDAO.delete(id);
        drenirovanieDAO.delete(id);
		analysisService.delete(id, 0);
		analysisService.delete(id, 1);
		analysisService.delete(id, 5);
		analysisService.delete(id, 10);
		objExaminationService.delete(id);
		editorDAO.delete(id, null);
        patientDao.delete(id);
    }

    @Override
    public String getMinDate() {
        return patientDao.getMinDate();
    }

    @Override
    public List<String> getDeathDates(int[] ids, String startDate, String endDate) {
        return patientDao.getDeathDates(ids, startDate, endDate);
    }

    @Override
    public List<String> getStartDates(int[] ids, String startDate, String endDate) {
        return patientDao.getStartDates(ids, startDate, endDate);
    }


    @Override
    public Patient findPatientById(Integer id) {
        return patientDao.findById(id);
    }


    public KlatskinDAO getKlatskinDao() {
        return klatskinDao;
    }

    public HCRDAO getHcrDao() {
        return hcrDao;
    }

    public GCRDAO getGcrDao() {
        return gcrdao;
    }

    public AlveokokkozDAO getAlveokokkozDAO() {
        return alveokokkozDAO;
    }

    public EchinokokkozDAO getEchinokokkozDAO() {
        return echinokokkozDAO;
    }

    public MNKRDAO getMnkrdao() {
        return mnkrdao;
    }

    public MKRDAO getMkrdao() {
        return mkrdao;
    }

    public RZHPDAO getRzhpdao() {
        return rzhpdao;
    }

	@Override
	public ZHKBDAO getZhhkbdao() {
		return zhkbdao;
	}

    public OpKlatskinDAO getOpKlatskinDao() {
        return opKlatskinDAO;
    }

    public OpHCRDAO getOpHcrDao() {
        return opHCRDAO;
    }

    public OpGCRDAO getOpGcrDao() {
        return opGCRDAO;
    }

    public OpAlveokokkozDAO getOpAlveokokkozDAO() {
        return opAlveokokkozDAO;
    }

    public OpEchinokokkozDAO getOpEchinokokkozDAO() {
        return opEchinokokkozDAO;
    }

    public OpMNKRDAO getOpMnkrdao() {
        return opMNKRDAO;
    }

    public OpMKRDAO getOpMkrdao() {
        return opMKRDAO;
    }

    public OpRZHPDAO getOpRzhpdao() {
        return opRZHPDAO;
    }

	public OpZHKBDAO getOpZHKBdao() {
		return opZHKBDAO;
	}

	public DrenirovanieDAO getDrenirovanie() {
        return drenirovanieDAO;
    }

	public PunkciaDAO getPunkcia() {
		return punkciaDAO;
	}

	public EditorDAO getEditorDAO() {
		return editorDAO;
	}
}
