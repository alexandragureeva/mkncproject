package com.amgureeva.binding.PatientBinding;

import com.amgureeva.Entities.Patient;
import com.amgureeva.Other.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 11.04.2016.
 */

@Repository
public class PatientDaoImpl implements PatientDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public Patient findById(Integer id) {
		if (id == null) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);

		String sql = "SELECT * FROM patient WHERE id=:id";

		Patient result = null;
		try {
			result = namedParameterJdbcTemplate
					.queryForObject(sql, params, new PatientMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}

		return result;
	}

	@Override
	public Patient findPatientByHisNumber(String hisNumber) {
		if (hisNumber == null) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("disHistoryNumber", hisNumber);

		String sql = "SELECT * FROM patient WHERE disHistoryNumber=:disHistoryNumber";

		Patient result = null;
		try {
			result = namedParameterJdbcTemplate
					.queryForObject(sql, params, new PatientMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}

		return result;
	}

	@Override
	public List<Patient> findAll() {
		String sql = "SELECT * FROM patient ORDER BY startDate DESC";
		List<Patient> result = namedParameterJdbcTemplate.query(sql, new PatientMapper());
		return result;
	}

	@Override
	public List<Patient> findAllRecent() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();
		Calendar calendar2 = Calendar.getInstance();
		calendar2.add(Calendar.DATE, -7);
		java.sql.Date startDate = new Date(calendar2.getTimeInMillis());
		java.sql.Date edate = new Date(calendar.getTimeInMillis());
		/*try {
			edate = formatter.parse(formatter.format(calendar.getTime()));
			calendar.add(Calendar.DATE, -7);
			startDate = formatter.parse(formatter.format(calendar.getTime()));
		} catch (ParseException e) {
			return null;
		}*/
		if (startDate != null && edate != null) {
			String sql = "SELECT * FROM patient WHERE startDate BETWEEN '" + startDate.toString() + "' AND '" +
					edate.toString() + "' ORDER BY startDate DESC";
			List<Patient> result = namedParameterJdbcTemplate.query(sql, new PatientMapper());
			return result;
		}
		return null;
	}

	@Override
	public void save(Patient patient) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		String sql = "INSERT INTO patient(name, sex, age, birthdate, diagnosisIdFK, disHistoryNumber," +
				" weight, growth, bmi, asaValue, mkbCode, gospitalization, gospitalizationTime, ibs, hyperDisease, diabet, " +
				"chronVenDisease,bronchAstma,chronBronchit,emfizema,serdechPoroki,onmk,parkinsonDisease, " +
				"parkinsonSyndrom,ozhirenie,yazvenDisease,hyperplazia,otherDiseases,ibsType,hyperDiseaseType," +
				"diabetType,chronVenDiseaseType,bronchAstmaType,chronBronchitType, emfizemaType,serdechPorokiType," +
				"onmkType,parkinsonDiseaseType,parkinsonSyndromType,ozhirenieType, yazvenDiseaseType,hyperplaziaType," +
				"ibsSubType,yazvenDiseaseComplications, startDate, endDate, bedDays, details, deathDate," +
				" closedDate, address, phone1, phone2, surgeryDate, hasSurgery) VALUES ( :name, :sex, :age, :birthdate, :diagnosisIdFK," +
				" :disHistoryNumber, :weight, :growth, :bmi,  :asaValue, :mkbCode, :gospitalization, " +
				":gospitalizationTime, :ibs, :hyperDisease, :diabet," +
				":chronVenDisease,:bronchAstma,:chronBronchit,:emfizema,:serdechPoroki,:onmk,:parkinsonDisease, " +
				":parkinsonSyndrom,:ozhirenie,:yazvenDisease,:hyperplazia,:otherDiseases,:ibsType,:hyperDiseaseType," +
				":diabetType,:chronVenDiseaseType,:bronchAstmaType,:chronBronchitType, :emfizemaType,:serdechPorokiType," +
				":onmkType,:parkinsonDiseaseType,:parkinsonSyndromType,:ozhirenieType, :yazvenDiseaseType,:hyperplaziaType," +
				":ibsSubType,:yazvenDiseaseComplications,:startDate, :endDate, :bedDays, :details, :deathDate," +
				":closedDate, :address, :phone1, :phone2, :surgeryDate, :hasSurgery)";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(patient), keyHolder);
		patient.setId(String.valueOf(keyHolder.getKey().intValue()));
	}

	@Override
	public void update(Patient patient) {
		String sql = "UPDATE patient SET  name=:name, sex=:sex, age=:age, birthdate=:birthdate, diagnosisIdFK=:diagnosisIdFK," +
				" disHistoryNumber=:disHistoryNumber, weight=:weight, growth=:growth, bmi=:bmi,  asaValue=:asaValue, " +
				"mkbCode=:mkbCode, gospitalization=:gospitalization, " +
				"gospitalizationTime=:gospitalizationTime, ibs=:ibs, hyperDisease=:hyperDisease," +
				" diabet=:diabet, surgeryDate=:surgeryDate, " +
				"chronVenDisease=:chronVenDisease,bronchAstma=:bronchAstma,chronBronchit=:chronBronchit," +
				"emfizema=:emfizema,serdechPoroki=:serdechPoroki,onmk=:onmk,parkinsonDisease=:parkinsonDisease, " +
				"parkinsonSyndrom=:parkinsonSyndrom,ozhirenie=:ozhirenie,yazvenDisease=:yazvenDisease,hyperplazia=:hyperplazia," +
				"otherDiseases=:otherDiseases,ibsType=:ibsType,hyperDiseaseType=:hyperDiseaseType," +
				"diabetType=:diabetType,chronVenDiseaseType=:chronVenDiseaseType,bronchAstmaType=:bronchAstmaType,chronBronchitType=:chronBronchitType," +
				" emfizemaType=:emfizemaType,serdechPorokiType=:serdechPorokiType," +
				"onmkType=:onmkType,parkinsonDiseaseType=:parkinsonDiseaseType,parkinsonSyndromType=:parkinsonSyndromType,ozhirenieType=:ozhirenieType," +
				" yazvenDiseaseType=:yazvenDiseaseType,hyperplaziaType=:hyperplaziaType," +
				"ibsSubType=:ibsSubType,yazvenDiseaseComplications=:yazvenDiseaseComplications,startDate=:startDate," +
				" bedDays=:bedDays, details=:details," +
				" address=:address, phone1=:phone1, phone2=:phone2, hasSurgery=:hasSurgery"
				+ " WHERE id=:id";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(patient));
	}

	@Override
	public void saveEndInfo(Patient patient) {
		String sql = "UPDATE patient SET  endDate=:endDate, deathDate=:deathDate, closedDate=:closedDate, bedDays=:bedDays"
				+ " WHERE id=:id";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("endDate", makeRightDate(patient.getEndDate()));
		params.put("closedDate", patient.getClosedDate());
		params.put("deathDate", makeRightDate(patient.getDeathDate()));
		params.put("bedDays", patient.getBedDaysNumber());
		params.put("id", patient.getId());
		namedParameterJdbcTemplate.update(sql, params);
	}

	@Override
	public void updateFillers(Patient patient) {
		String sql = "UPDATE patient SET  filledGeneralInfo=:filledGeneralInfo, " +
				"filledDiagnosisAtStart=:filledDiagnosisAtStart, filledObjExam=:filledObjExam, " +
				"filledAnalysis0=:filledAnalysis0, filledSurgery=:filledSurgery, " +
				"filledAnalysis1=:filledAnalysis1, filledAnalysis5=:filledAnalysis5, " +
				"filledDiagnosisAtEnd=:filledDiagnosisAtEnd, filledAnalysis10=:filledAnalysis10"
				+ " WHERE id=:id";

		namedParameterJdbcTemplate.update(sql, getFillerSqlParameterByModel(patient));
	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM patient WHERE id= :id";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	@Override
	public String getMinDate() {
		String sql = "SELECT MIN(startDate) as minDate FROM patient";
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		List<String> result = namedParameterJdbcTemplate.query(sql, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("minDate");
			}
		});
		if (result.size() > 0)
			return result.get(0);
		else return null;
	}

	@Override
	public List<String> getDeathDates(int[] ids, String startDate, String endDate) {
		StringBuffer sqlid = new StringBuffer();
		sqlid.append("( ");
		for (int id : ids) {
			sqlid.append(id + ",");
		}
		String tosql = sqlid.substring(0, sqlid.length() - 1);
		String sql = "SELECT deathDate FROM patient WHERE id IN" + tosql + ")";
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		List<String> result = namedParameterJdbcTemplate.query(sql, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("deathDate");
			}
		});
		return result;
	}

	@Override
	public List<String> getStartDates(int[] ids, String startDate, String endDate) {
		StringBuffer sqlid = new StringBuffer();
		sqlid.append("( ");
		for (int id : ids) {
			sqlid.append(id + ",");
		}
		String tosql = sqlid.substring(0, sqlid.length() - 1);
		String sql = "SELECT startDate FROM patient WHERE id IN " + tosql + ")";
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		List<String> result = namedParameterJdbcTemplate.query(sql, new RowMapper<String>() {
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getString("startDate");
			}
		});
		return result;
	}

	@Override
	public void setTypeOfTreatment(boolean isTwoStep, int id) {
		String sql = "UPDATE patient SET  twoStepsTreatment=:twoStepsTreatment"
				+ " WHERE id=:id";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("twoStepsTreatment", isTwoStep);
		params.put("id", id);

		namedParameterJdbcTemplate.update(sql, params);
	}


	private SqlParameterSource getFillerSqlParameterByModel(Patient patient) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", patient.getId());
		paramSource.addValue("filledGeneralInfo", patient.getFilledGeneralInfo());
		paramSource.addValue("filledDiagnosisAtStart", patient.getFilledDiagnosisAtStart());
		paramSource.addValue("filledObjExam", patient.getFilledObjExam());
		paramSource.addValue("filledAnalysis0", patient.getFilledAnalysis0());
		paramSource.addValue("filledSurgery", patient.getFilledSurgery());
		paramSource.addValue("filledAnalysis1", patient.getFilledAnalysis1());
		paramSource.addValue("filledAnalysis5", patient.getFilledAnalysis5());
		paramSource.addValue("filledAnalysis10", patient.getFilledAnalysis10());
		paramSource.addValue("filledDiagnosisAtEnd", patient.getFilledDiagnosisAtEnd());

		return paramSource;
	}

	private SqlParameterSource getSqlParameterByModel(Patient patient) {

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", patient.getId());
		paramSource.addValue("name", patient.getName());
		paramSource.addValue("sex", patient.getSex());
		paramSource.addValue("birthdate", makeRightDate(patient.getBirthdate()));
		paramSource.addValue("surgeryDate", makeRightDate(patient.getSurgeryDate()));
		paramSource.addValue("hasSurgery", patient.getHasSurgery());
		paramSource.addValue("age", patient.getAge());
		paramSource.addValue("growth", patient.getGrowth());
		paramSource.addValue("bmi", patient.getBmi());
		paramSource.addValue("startDate", makeRightDate(patient.getStartDate()));
		paramSource.addValue("endDate", makeRightDate(patient.getEndDate()));
		paramSource.addValue("bedDays", patient.getBedDaysNumber());
		paramSource.addValue("weight", patient.getWeight());
		paramSource.addValue("asaValue", patient.getAsaValue());
		paramSource.addValue("mkbCode", patient.getMkbCode());
		paramSource.addValue("gospitalization", patient.getGospitalization());
		paramSource.addValue("gospitalizationTime", patient.getGospitalizationTime());
		paramSource.addValue("ibs", patient.getIbs());
		paramSource.addValue("hyperDisease", patient.getHyperDisease());
		paramSource.addValue("diabet", patient.getDiabet());
		paramSource.addValue("chronVenDisease", patient.getChronVenDisease());
		paramSource.addValue("bronchAstma", patient.getBronchAstma());
		paramSource.addValue("chronBronchit", patient.getChronBronchit());
		paramSource.addValue("emfizema", patient.getEmfizema());
		paramSource.addValue("serdechPoroki", patient.getSerdechPoroki());
		paramSource.addValue("onmk", patient.getOnmk());
		paramSource.addValue("parkinsonDisease", patient.getParkinsonDisease());
		paramSource.addValue("parkinsonSyndrom", patient.getParkinsonSyndrom());
		paramSource.addValue("ozhirenie", patient.getOzhirenie());
		paramSource.addValue("yazvenDisease", patient.getYazvenDisease());
		paramSource.addValue("hyperplazia", patient.getHyperplazia());
		paramSource.addValue("otherDiseases", patient.getOtherDiseases());
		paramSource.addValue("ibsType", patient.getIbsType());
		paramSource.addValue("hyperDiseaseType", patient.getHyperDiseaseType());
		paramSource.addValue("diabetType", patient.getDiabetType());
		paramSource.addValue("chronVenDiseaseType", patient.getChronVenDiseaseType());
		paramSource.addValue("bronchAstmaType", patient.getBronchAstmaType());
		paramSource.addValue("chronBronchitType", patient.getChronBronchitType());
		paramSource.addValue("emfizemaType", patient.getEmfizemaType());
		paramSource.addValue("serdechPorokiType", patient.getSerdechPorokiType());
		paramSource.addValue("onmkType", patient.getOnmkType());
		paramSource.addValue("parkinsonDiseaseType", patient.getParkinsonDiseaseType());
		paramSource.addValue("parkinsonSyndromType", patient.getParkinsonSyndromType());
		paramSource.addValue("ozhirenieType", patient.getOzhirenieType());
		paramSource.addValue("yazvenDiseaseType", patient.getYazvenDiseaseType());
		paramSource.addValue("hyperplaziaType", patient.getHyperplaziaType());
		paramSource.addValue("ibsSubType", patient.getIbsSubType());
		paramSource.addValue("yazvenDiseaseComplications", patient.getYazvenDiseaseComplications());
		paramSource.addValue("address", patient.getAddress());
		paramSource.addValue("phone1", patient.getPhone1());
		paramSource.addValue("phone2", patient.getPhone2());
		paramSource.addValue("details", patient.getDetails());
		paramSource.addValue("disHistoryNumber", patient.getDiseaseNumber());
		paramSource.addValue("deathDate", makeRightDate(patient.getDeathDate()));
		paramSource.addValue("closedDate", patient.getClosedDate());
		paramSource.addValue("filledGeneralInfo", patient.getFilledGeneralInfo());
		paramSource.addValue("filledDiagnosisAtStart", patient.getFilledDiagnosisAtStart());
		paramSource.addValue("filledObjExam", patient.getFilledObjExam());
		paramSource.addValue("filledAnalysis0", patient.getFilledAnalysis0());
		paramSource.addValue("filledSurgery", patient.getFilledSurgery());
		paramSource.addValue("filledAnalysis1", patient.getFilledAnalysis1());
		paramSource.addValue("filledAnalysis5", patient.getFilledAnalysis5());
		paramSource.addValue("filledAnalysis10", patient.getFilledAnalysis10());
		paramSource.addValue("filledDiagnosisAtEnd", patient.getFilledDiagnosisAtEnd());
		com.amgureeva.Other.Properties properties = new Properties();
		paramSource.addValue("diagnosisIdFK", patient.getDiagnosis() - 1);
		return paramSource;
	}

	private static final class PatientMapper implements RowMapper<Patient> {

		public Patient mapRow(ResultSet rs, int rowNum) throws SQLException {
			Patient patient = new Patient();
			patient.setId(rs.getString("id"));
			patient.setName(rs.getString("name"));
			patient.setAge(rs.getString("age"));
			patient.setDiagnosis(rs.getInt("diagnosisIdFK") + 1);
			patient.setDiseaseNumber(rs.getString("disHistoryNumber"));
			patient.setSex(rs.getString("sex"));
			patient.setHasSurgery(rs.getBoolean("hasSurgery"));
			patient.setStartDate(rs.getString("startDate"));
			patient.setSurgeryDate(rs.getString("surgeryDate"));
			patient.setEndDate(rs.getString("endDate"));
			patient.setBedDaysNumber();
			patient.setBirthdate(rs.getString("birthdate"));
			patient.setDetails(rs.getString("details"));
			patient.setDeathDate(rs.getString("deathDate"));
			patient.setClosedDate(rs.getDate("closedDate"));
			patient.setWeight(rs.getDouble("weight"));
			patient.setGrowth(rs.getDouble("growth"));
			patient.setBmi(rs.getDouble("bmi"));
			patient.setAsaValue(rs.getInt("asaValue"));
			patient.setMkbCode(rs.getString("mkbCode"));
			patient.setGospitalization(rs.getInt("gospitalization"));
			patient.setGospitalizationTime(rs.getInt("gospitalizationTime"));
			patient.setIbs(rs.getInt("ibs"));
			patient.setHyperDisease(rs.getInt("hyperDisease"));
			patient.setDiabet(rs.getInt("diabet"));
			patient.setChronVenDisease(rs.getInt("chronVenDisease"));
			patient.setBronchAstma(rs.getInt("bronchAstma"));
			patient.setChronBronchit(rs.getInt("chronBronchit"));
			patient.setEmfizema(rs.getInt("emfizema"));
			patient.setSerdechPoroki(rs.getInt("serdechPoroki"));
			patient.setOnmk(rs.getInt("onmk"));
			patient.setParkinsonDisease(rs.getInt("parkinsonDisease"));
			patient.setParkinsonSyndrom(rs.getInt("parkinsonSyndrom"));
			patient.setOzhirenie(rs.getInt("ozhirenie"));
			patient.setYazvenDisease(rs.getInt("yazvenDisease"));
			patient.setHyperplazia(rs.getInt("hyperplazia"));
			patient.setOtherDiseases(rs.getInt("otherDiseases"));
			patient.setIbsType(rs.getInt("ibsType"));
			patient.setHyperDiseaseType(rs.getInt("hyperDiseaseType"));
			patient.setDiabetType(rs.getInt("diabetType"));
			patient.setChronVenDiseaseType(rs.getInt("chronVenDiseaseType"));
			patient.setBronchAstmaType(rs.getInt("bronchAstmaType"));
			patient.setChronBronchitType(rs.getInt("chronBronchitType"));
			patient.setEmfizemaType(rs.getInt("emfizemaType"));
			patient.setSerdechPorokiType(rs.getInt("serdechPorokiType"));
			patient.setOnmkType(rs.getInt("onmkType"));
			patient.setParkinsonDiseaseType(rs.getInt("parkinsonDiseaseType"));
			patient.setParkinsonSyndromType(rs.getInt("parkinsonSyndromType"));
			patient.setOzhirenieType(rs.getInt("ozhirenieType"));
			patient.setYazvenDiseaseType(rs.getInt("yazvenDiseaseType"));
			patient.setHyperplaziaType(rs.getInt("hyperplaziaType"));
			patient.setIbsSubType(rs.getInt("ibsSubType"));
			patient.setYazvenDiseaseComplications(rs.getInt("yazvenDiseaseComplications"));
			patient.setAddress(rs.getString("address"));
			patient.setPhone1(rs.getLong("phone1"));
			patient.setPhone2(rs.getLong("phone2"));
			patient.setFilledGeneralInfo(rs.getBoolean("filledGeneralInfo"));
			patient.setFilledDiagnosisAtStart(rs.getBoolean("filledDiagnosisAtStart"));
			patient.setFilledObjExam(rs.getBoolean("filledObjExam"));
			patient.setFilledAnalysis0(rs.getBoolean("filledAnalysis0"));
			patient.setFilledSurgery(rs.getBoolean("filledSurgery"));
			patient.setFilledAnalysis1(rs.getBoolean("filledAnalysis1"));
			patient.setFilledAnalysis5(rs.getBoolean("filledAnalysis5"));
			patient.setFilledAnalysis10(rs.getBoolean("filledAnalysis10"));
			patient.setFilledDiagnosisAtEnd(rs.getBoolean("filledDiagnosisAtEnd"));
			patient.setTypeOfTreatment(rs.getBoolean("twoStepsTreatment"));
			return patient;
		}
	}

	private String makeRightDate(String date) {
		if (date != null) {
			StringBuilder builder = new StringBuilder();
			if (date.contains("/")) {
				String[] dateArr = date.split("/");
				builder.append(dateArr[2]);
				builder.append("-");
				builder.append(dateArr[1]);
				builder.append("-");
				builder.append(dateArr[0]);
			} else {
				builder.append(date);
			}
			builder.append(" 00:00:00");
			return builder.toString();
		}
		return null;
	}
}
