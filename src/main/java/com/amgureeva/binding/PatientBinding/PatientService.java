package com.amgureeva.binding.PatientBinding;

import com.amgureeva.Entities.GeneralPatientInfo;
import com.amgureeva.Entities.Patient;
import com.amgureeva.binding.AlveokokozBinding.AlveokokkozDAO;
import com.amgureeva.binding.DrenirovanieBinding.DrenirovanieDAO;
import com.amgureeva.binding.EchinokokkozBinding.EchinokokkozDAO;
import com.amgureeva.binding.EditorBinding.EditorDAO;
import com.amgureeva.binding.GCRBinding.GCRDAO;
import com.amgureeva.binding.HCRBinding.HCRDAO;
import com.amgureeva.binding.KlatskinBinding.KlatskinDAO;
import com.amgureeva.binding.MKRBinding.MKRDAO;
import com.amgureeva.binding.MNKRBinding.MNKRDAO;
import com.amgureeva.binding.OpAlveokokkozBinding.OpAlveokokkozDAO;
import com.amgureeva.binding.OpEchinokokkozBinding.OpEchinokokkozDAO;
import com.amgureeva.binding.OpGCRBinding.OpGCRDAO;
import com.amgureeva.binding.OpHCRBinding.OpHCRDAO;
import com.amgureeva.binding.OpKlatskinBinding.OpKlatskinDAO;
import com.amgureeva.binding.OpMKRBinding.OpMKRDAO;
import com.amgureeva.binding.OpMNKRBinding.OpMNKRDAO;
import com.amgureeva.binding.OpRZHPBinding.OpRZHPDAO;
import com.amgureeva.binding.OpZHKBBinding.OpZHKBDAO;
import com.amgureeva.binding.PunkciaBinding.PunkciaDAO;
import com.amgureeva.binding.RZHPBinding.RZHPDAO;
import com.amgureeva.binding.ZHKBBinding.ZHKBDAO;

import java.util.List;

/**
 * Created by Александра on 11.04.2016.
 */
public interface PatientService {
    GeneralPatientInfo findById(Integer id);

    Patient findPatientById(Integer id);

    Patient findPatientByHisNumber(String hisNumber);


    List<GeneralPatientInfo> findAll();

    List<Patient> findAllPatients();

    List<Patient> findAllRecentPatients();

    String saveOrUpdate(GeneralPatientInfo gpi);

    void updateEndInfo(Patient patient);

    void updateFillers(Patient patient);

    void setTypeOfTreatment(boolean isTwoStep, int id);

    void delete(int id);

    String getMinDate();

    List<String> getDeathDates(int[] ids, String startDate, String endDate);

    List<String> getStartDates(int[] ids, String startDate, String endDate);

    KlatskinDAO getKlatskinDao();

    HCRDAO getHcrDao();

    GCRDAO getGcrDao();

    AlveokokkozDAO getAlveokokkozDAO();

    EchinokokkozDAO getEchinokokkozDAO();

    MNKRDAO getMnkrdao();

    MKRDAO getMkrdao();

    RZHPDAO getRzhpdao();


    ZHKBDAO getZhhkbdao();

    OpKlatskinDAO getOpKlatskinDao();

    OpHCRDAO getOpHcrDao();

    OpGCRDAO getOpGcrDao();

    OpAlveokokkozDAO getOpAlveokokkozDAO();

    OpEchinokokkozDAO getOpEchinokokkozDAO();

    OpMNKRDAO getOpMnkrdao();

    OpMKRDAO getOpMkrdao();

    OpRZHPDAO getOpRzhpdao();

    OpZHKBDAO getOpZHKBdao();

    DrenirovanieDAO getDrenirovanie();

    PunkciaDAO getPunkcia();

    EditorDAO getEditorDAO();
}
