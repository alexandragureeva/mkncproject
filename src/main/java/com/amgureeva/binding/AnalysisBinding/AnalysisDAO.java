package com.amgureeva.binding.AnalysisBinding;

import com.amgureeva.Entities.Analysis;

import java.util.List;

/**
 * Created by Nikita on 14.08.2016.
 */
public interface AnalysisDAO {
	Analysis findById(Integer id, int day);

	List<Analysis> findAll();

	void save(Analysis Analysis);

	void update(Analysis Analysis);

	void delete(Integer id, Integer day);
}
