package com.amgureeva.binding.AnalysisBinding;

import com.amgureeva.Entities.Analysis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 14.08.2016.
 */
@Repository
public class AnalysisDAOImpl implements AnalysisDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}


	@Override
	public Analysis findById(Integer id, int day) {
		if (id == null) {
			return null;
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		params.put("day", day);

		String sql = "SELECT * FROM analysis WHERE id=:id AND day=:day";

		Analysis result = null;
		try {
			result = namedParameterJdbcTemplate
					.queryForObject(sql, params, new AnalysisMapper());
		} catch (EmptyResultDataAccessException e) {
			// do nothing, return null
		}

		return result;
	}

	@Override
	public List<Analysis> findAll() {

		String sql = "SELECT * FROM analysis";
		List<Analysis> result = namedParameterJdbcTemplate.query(sql, new AnalysisDAOImpl.AnalysisMapper());
		return result;
	}

	@Override
	public void save(Analysis analysis) {

		String sql = "INSERT INTO analysis(id, day, gemoglabin, eritrociti, leikociti, palochkoyadernaya, " +
				"segmentarnoyadernaya, yunie, monociti, limfociti,trombociti, soe, ph, " +
				"belok, leikocitiMochi, eritrocitiMochi, glukoza, ketoni," +
				" ast, alt, obshiiBelok, albunin, obshiibilirubin, pryamoibilirubin, " +
				"kreatinin,sacharKrovi, mochevayaKislota, mochevina, mno, achtv, rezFactor, gruppaKrovi, " +
				"onk199, cea, alfoFetoprotein, ca125, onk199Value, ceaValue, alfoFetoproteinValue," +
				" ca125Value) " +
				"VALUES (:id, :day, :gemoglabin, :eritrociti, :leikociti, :palochkoyadernaya," +
				":segmentarnoyadernaya, :yunie, :monociti, :limfociti,:trombociti, :soe, :ph," +
				":belok, :leikocitiMochi, :eritrocitiMochi, :glukoza, :ketoni," +
				":ast, :alt, :obshiiBelok, :albunin, :obshiibilirubin, :pryamoibilirubin," +
				":kreatinin,:sacharKrovi, :mochevayaKislota, :mochevina, :mno, :achtv, :rezFactor, :gruppaKrovi," +
				":onk199, :cea, :alfoFetoprotein, :ca125, :onk199Value, :ceaValue, " +
				":alfoFetoproteinValue, :ca125Value)";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(analysis));
	}

	@Override
	public void update(Analysis analysis) {
		String sql = "UPDATE analysis SET gemoglabin=:gemoglabin, eritrociti=:eritrociti," +
				" leikociti=:leikociti, palochkoyadernaya=:palochkoyadernaya, segmentarnoyadernaya=:segmentarnoyadernaya" +
				", yunie=:yunie, monociti=:monociti, limfociti=:limfociti,trombociti=:trombociti, soe=:soe, " +
				"ph=:ph," +
				"belok=:belok, leikocitiMochi=:leikocitiMochi, eritrocitiMochi=:eritrocitiMochi, " +
				"glukoza=:glukoza, ketoni=:ketoni," +
				"ast=:ast, alt=:alt, obshiiBelok=:obshiiBelok, " +
				"albunin=:albunin, obshiibilirubin=:obshiibilirubin, pryamoibilirubin=:pryamoibilirubin," +
				"kreatinin=:kreatinin,sacharKrovi=:sacharKrovi, mochevayaKislota=:mochevayaKislota, " +
				"mochevina=:mochevina, mno=:mno, achtv=:achtv, rezFactor=:rezFactor, gruppaKrovi=:gruppaKrovi," +
				"onk199=:onk199, cea=:cea, alfoFetoprotein=:alfoFetoprotein, ca125=:ca125," +
				"onk199Value=:onk199Value, ceaValue=:ceaValue," +
				" alfoFetoproteinValue=:alfoFetoproteinValue, ca125Value=:ca125Value" +
				" WHERE id=:id and day=:day";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(analysis));
	}

	@Override
	public void delete(Integer patientId, Integer day) {
		String sql = "DELETE FROM analysis WHERE id= :id and day=:day ";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", patientId);
		params.put("day", day);
		namedParameterJdbcTemplate.update(sql, params);
	}


	private SqlParameterSource getSqlParameterByModel(Analysis analysis) {

		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("id", analysis.getId());
		paramSource.addValue("day", analysis.getDay());
		paramSource.addValue("gemoglabin", analysis.getGemoglabin());
		paramSource.addValue("eritrociti", analysis.getEritrociti());
		paramSource.addValue("leikociti", analysis.getLeikociti());
		paramSource.addValue("palochkoyadernaya", analysis.getPalochkoyadernaya());
		paramSource.addValue("segmentarnoyadernaya", analysis.getSegmentarnoyadernaya());
		paramSource.addValue("yunie", analysis.getYunie());
		paramSource.addValue("monociti", analysis.getMonociti());
		paramSource.addValue("limfociti", analysis.getLimfociti());
		paramSource.addValue("trombociti", analysis.getTrombociti());
		paramSource.addValue("soe", analysis.getSoe());
		paramSource.addValue("ph", analysis.getPh());
		paramSource.addValue("belok", analysis.getBelok());
		paramSource.addValue("leikocitiMochi", analysis.getLeikocitiMochi());
		paramSource.addValue("eritrocitiMochi", analysis.getEritrocitiMochi());
		paramSource.addValue("glukoza", analysis.getGlukoza());
		paramSource.addValue("ketoni", analysis.getKetoni());
		paramSource.addValue("ast", analysis.getAst());
		paramSource.addValue("alt", analysis.getAlt());
		paramSource.addValue("obshiiBelok", analysis.getObshiiBelok());
		paramSource.addValue("albunin", analysis.getAlbunin());
		paramSource.addValue("obshiibilirubin", analysis.getObshiibilirubin());
		paramSource.addValue("pryamoibilirubin", analysis.getPryamoibilirubin());
		paramSource.addValue("kreatinin", analysis.getKreatinin());
		paramSource.addValue("sacharKrovi", analysis.getSacharKrovi());
		paramSource.addValue("mochevayaKislota", analysis.getMochevayaKislota());
		paramSource.addValue("mochevina", analysis.getMochevina());
		paramSource.addValue("mno", analysis.getMno());
		paramSource.addValue("achtv", analysis.getAchtv());
		paramSource.addValue("rezFactor", analysis.getRezFactor());
		paramSource.addValue("gruppaKrovi", analysis.getGruppaKrovi());
		paramSource.addValue("onk199", analysis.getOnk199());
		paramSource.addValue("cea", analysis.getCea());
		paramSource.addValue("alfoFetoprotein", analysis.getAlfoFetoprotein());
		paramSource.addValue("ca125", analysis.getCa125());
		paramSource.addValue("onk199Value", analysis.getOnk199Value());
		paramSource.addValue("ceaValue", analysis.getCeaValue());
		paramSource.addValue("alfoFetoproteinValue", analysis.getAlfoFetValue());
		paramSource.addValue("ca125Value", analysis.getCa125Value());
		
		return paramSource;
	}

	private static final class AnalysisMapper implements RowMapper<Analysis> {

		public Analysis mapRow(ResultSet rs, int rowNum) throws SQLException {
			Analysis analysis = new Analysis();
			analysis.setId(rs.getString("id"));
			analysis.setDay(rs.getInt("day"));
			analysis.setGemoglabin(rs.getFloat("gemoglabin"));
			analysis.setEritrociti(rs.getFloat("eritrociti"));
			analysis.setLeikociti(rs.getFloat("leikociti"));
			analysis.setPalochkoyadernaya(rs.getFloat("palochkoyadernaya"));
			analysis.setSegmentarnoyadernaya(rs.getFloat("segmentarnoyadernaya"));
			analysis.setYunie(rs.getFloat("yunie"));
			analysis.setMonociti(rs.getFloat("monociti"));
			analysis.setLimfociti(rs.getFloat("limfociti"));
			analysis.setTrombociti(rs.getFloat("trombociti"));
			analysis.setSoe(rs.getFloat("soe"));
			analysis.setPh(rs.getFloat("ph"));
			analysis.setBelok(rs.getFloat("belok"));
			analysis.setLeikocitiMochi(rs.getFloat("leikocitiMochi"));
			analysis.setEritrocitiMochi(rs.getFloat("eritrocitiMochi"));
			analysis.setGlukoza(rs.getFloat("glukoza"));
			analysis.setKetoni(rs.getFloat("ketoni"));
			analysis.setAst(rs.getFloat("ast"));
			analysis.setAlt(rs.getFloat("alt"));
			analysis.setObshiiBelok(rs.getFloat("obshiiBelok"));
			analysis.setAlbunin(rs.getFloat("albunin"));
			analysis.setObshiibilirubin(rs.getFloat("obshiibilirubin"));
			analysis.setPryamoibilirubin(rs.getFloat("pryamoibilirubin"));
			analysis.setKreatinin(rs.getFloat("kreatinin"));
			analysis.setSacharKrovi(rs.getFloat("sacharKrovi"));
			analysis.setMochevayaKislota(rs.getFloat("mochevayaKislota"));
			analysis.setMochevina(rs.getFloat("mochevina"));
			analysis.setMno(rs.getFloat("mno"));
			analysis.setAchtv(rs.getFloat("achtv"));
			analysis.setRezFactor(rs.getInt("rezFactor"));
			analysis.setGruppaKrovi(rs.getInt("gruppaKrovi"));
			analysis.setOnk199(rs.getInt("onk199"));
			analysis.setCea(rs.getInt("cea"));
			analysis.setAlfoFetoprotein(rs.getInt("alfoFetoprotein"));
			analysis.setCa125(rs.getInt("ca125"));
			analysis.setOnk199Value(rs.getFloat("onk199Value"));
			analysis.setCeaValue(rs.getFloat("ceaValue"));
			analysis.setAlfoFetValue(rs.getFloat("alfoFetoproteinValue"));
			analysis.setCa125Value(rs.getFloat("ca125Value"));

			return analysis;
		}
	}

}
