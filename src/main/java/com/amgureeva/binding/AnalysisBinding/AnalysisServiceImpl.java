package com.amgureeva.binding.AnalysisBinding;

import com.amgureeva.Entities.Analysis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Nikita on 14.08.2016.
 */
@Service("analysisService")
public class AnalysisServiceImpl implements AnalysisService {

	AnalysisDAO analysisDAO;

	@Autowired
	public void setAnalysisDAO(AnalysisDAO analysisDAO) {
		this.analysisDAO = analysisDAO;
	}


	@Override
	public Analysis findById(Integer id, int day) {
		return analysisDAO.findById(id, day);
	}

	@Override
	public List<Analysis> findAll() {
		return analysisDAO.findAll();
	}

	@Override
	public void saveOrUpdate(Analysis analysis, boolean isNew) {
		if (isNew)
			analysisDAO.save(analysis);
		else
			analysisDAO.update(analysis);
	}

	@Override
	public void delete(int id, Integer day) {
	   analysisDAO.delete(id, day);
	}
}
