package com.amgureeva.binding.AnalysisBinding;

import com.amgureeva.Entities.Analysis;

import java.util.List;

/**
 * Created by Nikita on 14.08.2016.
 */
public interface AnalysisService {

	Analysis findById(Integer id, int day);

	List<Analysis> findAll();

	void saveOrUpdate(Analysis analysis, boolean isNew);

	void delete(int id, Integer day);

}
