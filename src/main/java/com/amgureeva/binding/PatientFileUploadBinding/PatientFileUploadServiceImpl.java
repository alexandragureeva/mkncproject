package com.amgureeva.binding.PatientFileUploadBinding;

import com.amgureeva.Entities.PatientFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Nikita on 09.09.2016.
 */
@Service("patientfileuploadService")
public class PatientFileUploadServiceImpl implements PatientFileUploadService {

	@Autowired
	PatientFileUploadDAO patientFileUploadDAO;
	@Override
	public void save(PatientFileUpload patientFileUpload) {
		   patientFileUploadDAO.save(patientFileUpload);
	}

	@Override
	public List<PatientFileUpload> findAllById(int patientId) {
		return patientFileUploadDAO.findAllByPatientId(patientId);
	}


	@Override
	public PatientFileUpload findById(int patientId, int fileId) {
		return patientFileUploadDAO.findFileById(patientId, fileId);
	}
}
