package com.amgureeva.binding.PatientFileUploadBinding;

import com.amgureeva.Entities.PatientFileUpload;

import java.util.List;

/**
 * Created by Nikita on 09.09.2016.
 */
public interface PatientFileUploadService {
	void save(PatientFileUpload patientFileUpload);
	List<PatientFileUpload> findAllById(int patientId);

	PatientFileUpload findById(int patientId, int fileId);
}
