package com.amgureeva.binding.PatientFileUploadBinding;

import com.amgureeva.Entities.PatientFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 09.09.2016.
 */
@Repository
public class PatientFileUploadDAOImpl implements PatientFileUploadDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}



	@Override
	public void save(PatientFileUpload patientFileUpload)  {
		String sql = "INSERT INTO additionalfiles (patientId, file, fileName) VALUES(:patientId, :file, :fileName) ";
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("patientId",patientFileUpload.getPatientId());
		values.put("file", patientFileUpload.getFileToUpload());
		values.put("fileName", patientFileUpload.getFileName());
		namedParameterJdbcTemplate.update(sql, values);
	}

	@Override
	public List<PatientFileUpload> findAllByPatientId(int patientId) {
		String sql = "SELECT * FROM additionalfiles WHERE patientId=:patientId";
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("patientId", patientId);
		List<PatientFileUpload> result = namedParameterJdbcTemplate.query(sql, values, new PatientFileUploadDAOImpl.FileMapper());
		return result;
	}

	@Override
	public PatientFileUpload findFileById(int patientId, int fileId) {
		String sql = "SELECT * FROM additionalfiles WHERE patientId=:patientId AND fileId=:fileId";
		Map<String, Object> values = new HashMap<String, Object>();
		values.put("patientId", patientId);
		values.put("fileId", fileId);
		PatientFileUpload result = namedParameterJdbcTemplate.queryForObject(
				sql, values, new PatientFileUploadDAOImpl.FileMapper());
		return result;
	}

	private static final class FileMapper implements RowMapper<PatientFileUpload> {

		public PatientFileUpload mapRow(ResultSet rs, int rowNum) throws SQLException {
			PatientFileUpload patientFileUpload = new PatientFileUpload();
			patientFileUpload.setPatientId(rs.getInt("patientId"));
			patientFileUpload.setFileId(rs.getInt("fileId"));
			patientFileUpload.setFileName(rs.getString("fileName"));
			Blob blob = rs.getBlob("file");
			InputStream in = blob.getBinaryStream();
			patientFileUpload.setFileToUpload(in);
			return patientFileUpload;
		}
	}

}
