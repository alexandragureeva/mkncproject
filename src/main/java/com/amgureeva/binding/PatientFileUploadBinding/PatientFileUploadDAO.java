package com.amgureeva.binding.PatientFileUploadBinding;

import com.amgureeva.Entities.PatientFileUpload;

import java.util.List;

/**
 * Created by Nikita on 09.09.2016.
 */
public interface PatientFileUploadDAO {
	void save(PatientFileUpload patientFileUpload);

	List<PatientFileUpload> findAllByPatientId(int patientId);

	PatientFileUpload findFileById (int patientId, int fileId);

}
