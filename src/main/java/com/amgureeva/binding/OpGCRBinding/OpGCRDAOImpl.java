package com.amgureeva.binding.OpGCRBinding;

import com.amgureeva.Entities.Surgeries.OpGCR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 22.05.2016.
 */
@Repository
public class OpGCRDAOImpl implements OpGCRDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public OpGCR findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=1";

			List<OpGCR> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpGCRDAOImpl.OpGCRMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}

	@Override
	public List<OpGCR> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 1";
		List<List<OpGCR>> result = namedParameterJdbcTemplate.query(sql,
				new OpGCRDAOImpl.OpGCRMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(OpGCR opGCR) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),6, String.valueOf(opGCR.getKrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),7, String.valueOf(opGCR.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),9, String.valueOf(opGCR.getGemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),10, String.valueOf(opGCR.getGcrgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),11, String.valueOf(opGCR.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),12, String.valueOf(opGCR.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),13, String.valueOf(opGCR.getGcrgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),19, String.valueOf(opGCR.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),21, String.valueOf(opGCR.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),22, String.valueOf(opGCR.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),23, String.valueOf(opGCR.getSelSosIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),26, String.valueOf(opGCR.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),34, String.valueOf(opGCR.getGcroplech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),35, String.valueOf(opGCR.getGcrobrezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),36, String.valueOf(opGCR.getGcrsegrez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),37, String.valueOf(opGCR.getGcrchisloUdSeg())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),38, String.valueOf(opGCR.getGcrchisloOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),39, String.valueOf(opGCR.getGcrrazOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),40, String.valueOf(opGCR.getGcrblizSosud())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),41, String.valueOf(opGCR.getGcrpolnRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),42, String.valueOf(opGCR.getGcralkChisloOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),43, String.valueOf(opGCR.getGcralkRazOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),44, String.valueOf(opGCR.getPerPolVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),45, String.valueOf(opGCR.getGcrperPolVeniMin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),46, String.valueOf(opGCR.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),47, String.valueOf(opGCR.getGcrselIzMin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),48, String.valueOf(opGCR.getIntrRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),49, String.valueOf(opGCR.getTache())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),1, String.valueOf(opGCR.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),2, String.valueOf(opGCR.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),25, String.valueOf(opGCR.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),103, String.valueOf(opGCR.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),104, String.valueOf(opGCR.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),105, String.valueOf(opGCR.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),106, String.valueOf(opGCR.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),107, String.valueOf(opGCR.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),108, String.valueOf(opGCR.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),109, String.valueOf(opGCR.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),110, String.valueOf(opGCR.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),111, String.valueOf(opGCR.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),112, String.valueOf(opGCR.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),113, String.valueOf(opGCR.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),114, String.valueOf(opGCR.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),115, String.valueOf(opGCR.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),116, String.valueOf(opGCR.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),117, String.valueOf(opGCR.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),118, String.valueOf(opGCR.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),119, String.valueOf(opGCR.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),120, String.valueOf(opGCR.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),121, String.valueOf(opGCR.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),122, String.valueOf(opGCR.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),123, String.valueOf(opGCR.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),124, String.valueOf(opGCR.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),125, String.valueOf(opGCR.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),126, String.valueOf(opGCR.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),127, String.valueOf(opGCR.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),128, String.valueOf(opGCR.getPoliorganNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),130, String.valueOf(opGCR.getVrv())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),131, String.valueOf(opGCR.getPostRezNedostA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),132, String.valueOf(opGCR.getPostRezNedostB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),133, String.valueOf(opGCR.getPostRezNedostC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),134, String.valueOf(opGCR.getPostEmbSyndrom())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),135, String.valueOf(opGCR.getBiloma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),136, String.valueOf(opGCR.getPovrPolOrgan())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),137, String.valueOf(opGCR.getKrovVBrushPolost1())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),138, String.valueOf(opGCR.getKrovVBrushPolost2())));

	}

	@Override
	public void update(OpGCR opGCR) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=1 and parameterId=:parameterId";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),6, String.valueOf(opGCR.getKrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),7, String.valueOf(opGCR.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),9, String.valueOf(opGCR.getGemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),10, String.valueOf(opGCR.getGcrgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),11, String.valueOf(opGCR.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),12, String.valueOf(opGCR.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),13, String.valueOf(opGCR.getGcrgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),19, String.valueOf(opGCR.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),21, String.valueOf(opGCR.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),22, String.valueOf(opGCR.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),23, String.valueOf(opGCR.getSelSosIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),26, String.valueOf(opGCR.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),34, String.valueOf(opGCR.getGcroplech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),35, String.valueOf(opGCR.getGcrobrezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),36, String.valueOf(opGCR.getGcrsegrez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),37, String.valueOf(opGCR.getGcrchisloUdSeg())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),38, String.valueOf(opGCR.getGcrchisloOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),39, String.valueOf(opGCR.getGcrrazOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),40, String.valueOf(opGCR.getGcrblizSosud())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),41, String.valueOf(opGCR.getGcrpolnRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),42, String.valueOf(opGCR.getGcralkChisloOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),43, String.valueOf(opGCR.getGcralkRazOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),44, String.valueOf(opGCR.getPerPolVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),45, String.valueOf(opGCR.getGcrperPolVeniMin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),46, String.valueOf(opGCR.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),47, String.valueOf(opGCR.getGcrselIzMin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),48, String.valueOf(opGCR.getIntrRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),49, String.valueOf(opGCR.getTache())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),1, String.valueOf(opGCR.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),2, String.valueOf(opGCR.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),25, String.valueOf(opGCR.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),103, String.valueOf(opGCR.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),104, String.valueOf(opGCR.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),105, String.valueOf(opGCR.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),106, String.valueOf(opGCR.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),107, String.valueOf(opGCR.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),108, String.valueOf(opGCR.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),109, String.valueOf(opGCR.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),110, String.valueOf(opGCR.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),111, String.valueOf(opGCR.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),112, String.valueOf(opGCR.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),113, String.valueOf(opGCR.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),114, String.valueOf(opGCR.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),115, String.valueOf(opGCR.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),116, String.valueOf(opGCR.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),117, String.valueOf(opGCR.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),118, String.valueOf(opGCR.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),119, String.valueOf(opGCR.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),120, String.valueOf(opGCR.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),121, String.valueOf(opGCR.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),122, String.valueOf(opGCR.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),123, String.valueOf(opGCR.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),124, String.valueOf(opGCR.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),125, String.valueOf(opGCR.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),126, String.valueOf(opGCR.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),127, String.valueOf(opGCR.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),128, String.valueOf(opGCR.getPoliorganNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),130, String.valueOf(opGCR.getVrv())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),131, String.valueOf(opGCR.getPostRezNedostA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),132, String.valueOf(opGCR.getPostRezNedostB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),133, String.valueOf(opGCR.getPostRezNedostC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),134, String.valueOf(opGCR.getPostEmbSyndrom())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),135, String.valueOf(opGCR.getBiloma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),136, String.valueOf(opGCR.getPovrPolOrgan())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),137, String.valueOf(opGCR.getKrovVBrushPolost1())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opGCR.getId(),138, String.valueOf(opGCR.getKrovVBrushPolost2())));
	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=1";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 1);
		paramSource.addValue("value", value);
		return paramSource;
	}


	private static final class OpGCRMapper implements RowMapper<List<OpGCR>> {

		public List<OpGCR> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<OpGCR> alList = new ArrayList<OpGCR>();
			OpGCR opGCR = new OpGCR();

			int i = 0;
			do {
				opGCR.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 1: i++;  opGCR.setRezekciaType(rs.getInt("value"));break;
					case 2: i++; opGCR.setRazdelenieTkanei(rs.getInt("value"));break;
					case 6: if (i!=0 && i!=66) return null;i=1; opGCR.setKrovopoteria(rs.getInt("value"));break;
					case 7: i++; opGCR.setDlitop(rs.getString("value"));break;
					case 9: i++; opGCR.setGemotransfusia(rs.getInt("value"));break;
					case 10: i++; opGCR.setGcrgemotransfusia_ml(rs.getString("value"));break;
					case 11: i++; opGCR.setPlazma(rs.getString("value")); break;
					case 12: i++; opGCR.setGdc(rs.getInt("value")); break;
					case 13: i++; opGCR.setGcrgdcmin(rs.getString("value")); break;
					case 19: i++; opGCR.setTnm(rs.getInt("value")); break;
					case 21: i++; opGCR.setBypass(rs.getInt("value")); break;
					case 22: i++; opGCR.setTotIz(rs.getInt("value")); break;
					case 23: i++; opGCR.setSelSosIz(rs.getInt("value")); break;
					case 25: i++; opGCR.setOsl(rs.getInt("value")); break;
					case 26: i++; opGCR.setClavien(rs.getInt("value")); break;
					case 34: i++; opGCR.setGcroplech(rs.getInt("value")); break;
					case 35: i++; opGCR.setGcrobrezekcii(rs.getInt("value")); break;
					case 36: i++; opGCR.setGcrsegrez(rs.getInt("value")); break;
					case 37: i++; opGCR.setGcrchisloUdSeg(rs.getInt("value")); break;
					case 38: i++; opGCR.setGcrchisloOch(rs.getInt("value")); break;
					case 39: i++; opGCR.setGcrrazOch(rs.getString("value")); break;
					case 40: i++; opGCR.setGcrblizSosud(rs.getInt("value")); break;
					case 41: i++; opGCR.setGcrpolnRHA(rs.getInt("value")); break;
					case 42: i++; opGCR.setGcralkChisloOch(rs.getInt("value")); break;
					case 43: i++; opGCR.setGcralkRazOch(rs.getString("value")); break;
					case 44: i++; opGCR.setPerPolVeni(rs.getInt("value")); break;
					case 45: i++; opGCR.setGcrperPolVeniMin(rs.getString("value")); break;
					case 46: i++; opGCR.setSelIz(rs.getInt("value")); break;
					case 47: i++; opGCR.setGcrselIzMin(rs.getString("value")); break;
					case 48: i++; opGCR.setIntrRHA(rs.getInt("value")); break;
					case 49: i++; opGCR.setTache(rs.getInt("value")); break;
					case 103: i++; opGCR.setVnutrKrovot(rs.getInt("value")); break;
					case 104: i++; opGCR.setZhkk(rs.getInt("value")); break;
					case 105: i++; opGCR.setPechNedostatA(rs.getInt("value")); break;
					case 106: i++; opGCR.setPechNedostatB(rs.getInt("value")); break;
					case 107: i++; opGCR.setPechNedostatC(rs.getInt("value")); break;
					case 108: i++; opGCR.setNagnoenie(rs.getInt("value")); break;
					case 109: i++; opGCR.setCholangit(rs.getInt("value")); break;
					case 110: i++; opGCR.setCholAbscedir(rs.getInt("value")); break;
					case 111: i++; opGCR.setAscit(rs.getInt("value")); break;
					case 112: i++; opGCR.setEventracia(rs.getInt("value")); break;
					case 113: i++; opGCR.setKishNeprohod(rs.getInt("value")); break;
					case 114: i++; opGCR.setPankreatSvich(rs.getInt("value")); break;
					case 115: i++; opGCR.setOstriiSvich(rs.getInt("value")); break;
					case 116: i++; opGCR.setSepsis(rs.getInt("value")); break;
					case 117: i++; opGCR.setTyazhSepsis(rs.getInt("value")); break;
					case 118: i++; opGCR.setSeptShok(rs.getInt("value")); break;
					case 119: i++; opGCR.setZhelchSvish(rs.getInt("value")); break;
					case 120: i++; opGCR.setKishSvish(rs.getInt("value")); break;
					case 121: i++; opGCR.setTrombVorotVeni(rs.getInt("value")); break;
					case 122: i++; opGCR.setTrombozPechArter(rs.getInt("value")); break;
					case 123: i++; opGCR.setOdnostorPnevm(rs.getInt("value")); break;
					case 124: i++; opGCR.setDvustorPnevm(rs.getInt("value")); break;
					case 125: i++; opGCR.setGidrotorax(rs.getInt("value")); break;
					case 126: i++; opGCR.setPochNedostat(rs.getInt("value")); break;
					case 127: i++; opGCR.setSerdSosNedostat(rs.getInt("value")); break;
					case 128: i++; opGCR.setPoliorganNedostat(rs.getInt("value")); break;
					case 130: i++; opGCR.setVrv(rs.getInt("value")); break;
					case 131: i++; opGCR.setPostRezNedostA(rs.getInt("value")); break;
					case 132: i++; opGCR.setPostRezNedostB(rs.getInt("value")); break;
					case 133: i++; opGCR.setPostRezNedostC(rs.getInt("value")); break;
					case 134: i++; opGCR.setPostEmbSyndrom(rs.getInt("value")); break;
					case 135: i++; opGCR.setBiloma(rs.getInt("value")); break;
					case 136: i++; opGCR.setPovrPolOrgan(rs.getInt("value")); break;
					case 137: i++; opGCR.setKrovVBrushPolost1(rs.getInt("value")); break;
					case 138: i++; opGCR.setKrovVBrushPolost2(rs.getInt("value")); break;
				}
				if (i == 66) {
					alList.add(opGCR);
					opGCR = new OpGCR();
				}
			}while (rs.next());
			return alList;
		}
	}
}
