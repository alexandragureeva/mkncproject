package com.amgureeva.binding.OpGCRBinding;

import com.amgureeva.Entities.Surgeries.OpGCR;
import com.amgureeva.Entities.Surgeries.OpGCRList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public class OpGCRListCreator {

	public List<OpGCRList> createGcrList(List<OpGCR> opGCRs, PatientService patientService) {
		if (opGCRs != null && patientService != null) {
			List<OpGCRList> opGCRLists = new ArrayList<OpGCRList>();
			OpGCRList gcrList = new OpGCRList();
			for (int i = 0; i < opGCRs.size(); i++) {
				gcrList.setName(patientService.findPatientById(Integer.valueOf(opGCRs.get(i).getId())).getName());
				switch (Integer.valueOf(opGCRs.get(i).getGcroplech())) {
					case 0:
						gcrList.setOplech("--");
						break;
					case 1:
						gcrList.setOplech("Резекция печени");
						break;
					case 2:
						gcrList.setOplech("Чрескожая РЧА (чРЧА)");
						break;
					case 3:
						gcrList.setOplech("Алкоголизация");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getGcrobrezekcii())) {
					case 0:
						gcrList.setObrezekcii("--");
						break;
					case 1:
						gcrList.setObrezekcii("Обширная резекция ЛГГЭ");
						break;
					case 2:
						gcrList.setObrezekcii("Обширная резекция ПГГЭ");
						break;
					case 3:
						gcrList.setObrezekcii("Cегментарная резекция");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getGcrsegrez())) {
					case 0:
						gcrList.setSegrez("--");
						break;
					case 1:
						gcrList.setSegrez("Анатомическая");
						break;
					case 2:
						gcrList.setSegrez("Неанатомическая");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getKrovopoteria())) {
					case 0:
						gcrList.setKrovopoteria("--");
						break;
					case 1:
						gcrList.setKrovopoteria("<500");
						break;
					case 2:
						gcrList.setKrovopoteria("500-1000");
						break;
					case 3:
						gcrList.setKrovopoteria(">1000");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getGcrchisloUdSeg())) {
					case 0:
						gcrList.setChisloUdSeg("--");
						break;
					case 1:
						gcrList.setChisloUdSeg("1");
						break;
					case 2:
						gcrList.setChisloUdSeg("2");
						break;
					case 3:
						gcrList.setChisloUdSeg("3");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getGcrchisloOch())) {
					case 0:
						gcrList.setChisloOch("--");
						break;
					case 1:
						gcrList.setChisloOch("1");
						break;
					case 2:
						gcrList.setChisloOch("2");
						break;
					case 3:
						gcrList.setChisloOch("3");
						break;
					case 4:
						gcrList.setChisloOch(">3");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getGcrpolnRHA())) {
					case 0:
						gcrList.setPolnRHA("--");
						break;
					case 1:
						gcrList.setPolnRHA("Полная");
						break;
					case 2:
						gcrList.setPolnRHA("Неполная");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getGcralkChisloOch())) {
					case 0:
						gcrList.setAlkChisloOch("--");
						break;
					case 1:
						gcrList.setAlkChisloOch("1");
						break;
					case 2:
						gcrList.setAlkChisloOch("2");
						break;
					case 3:
						gcrList.setAlkChisloOch("3");
						break;
					case 4:
						gcrList.setAlkChisloOch(">3");
						break;
				}

			/*	switch (Integer.valueOf(opGCRs.get(i).getOslRHA())) {
					case 0:
						gcrList.setOslRHA("--");
						break;
					case 1:
						gcrList.setOslRHA("Постабляционный синдром");
						break;
					case 2:
						gcrList.setOslRHA("Билома");
						break;
					case 3:
						gcrList.setOslRHA("Повреждения полых органов");
						break;
					case 4:
						gcrList.setOslRHA("Печеночная недостаточность");
						break;
					case 5:
						gcrList.setOslRHA("Кровотечение в бр.полость(остановлено консервативно)");
						break;
					case 6:
						gcrList.setOslRHA("Кровотечение в бр.полость(потребовавшее лапроскопии(лапоротомии))");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getOslAlk())) {
					case 0:
						gcrList.setOslAlk("--");
						break;
					case 1:
						gcrList.setOslAlk("Печеночная недостаточность A");
						break;
					case 2:
						gcrList.setOslAlk("Печеночная недостаточность B");
						break;
					case 3:
						gcrList.setOslAlk("Печеночная недостаточность C");
						break;
					case 4:
						gcrList.setOslAlk("Кровотечение в бр.полость(остановлено консервативно)");
						break;
					case 5:
						gcrList.setOslAlk("Кровотечение в бр.полость(потребовавшее лапроскопии(лапоротомии))");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getTnm())) {
					case 0:
						gcrList.setTnm("--");
						break;
					case 1:
						gcrList.setTnm("T1N0M0");
						break;
					case 2:
						gcrList.setTnm("T2aN0M0");
						break;
					case 3:
						gcrList.setTnm("T2bN0M0");
						break;
					case 4:
						gcrList.setTnm("T3N0M0");
						break;
					case 5:
						gcrList.setTnm("T1N1M0");
						break;
					case 6:
						gcrList.setTnm("T2N1M0");
						break;
					case 7:
						gcrList.setTnm("T3N1M0");
						break;
					case 8:
						gcrList.setTnm("T4N0M0");
						break;
					case 9:
						gcrList.setTnm("T4N1M0");
						break;
					case 10:
						gcrList.setTnm("Любая T, N2M0");
						break;
					case 11:
						gcrList.setTnm("Любая T, любая N, M1");
						break;
				}
				switch (Integer.valueOf(opGCRs.get(i).getOslRez())) {
					case 0:
						gcrList.setOslRez("--");
						break;
					case 1:
						gcrList.setOslRez("Внутрибрюшное кровотечение");
						break;
					case 2:
						gcrList.setOslRez("ЖКК");
						break;
					case 3:
						gcrList.setOslRez("Из ВРВ пищевода и желудка");
						break;
					case 4:
						gcrList.setOslRez("Печеночная недостаточность A");
						break;
					case 5:
						gcrList.setOslRez("Печеночная недостаточность B");
						break;
					case 6:
						gcrList.setOslRez("Печеночная недостаточность C");
						break;
					case 7:
						gcrList.setOslRez("Паренхиматозная желтуха");
						break;
					case 8:
						gcrList.setOslRez("Механическая желтуха");
						break;
					case 9:
						gcrList.setOslRez("Нагноение раны");
						break;
					case 10:
						gcrList.setOslRez("Холангит");
						break;
					case 11:
						gcrList.setOslRez("Холангиогенное абсцедирование");
						break;
					case 12:
						gcrList.setOslRez("Асцит");
						break;
					case 13:
						gcrList.setOslRez("Эвентрация");
						break;
					case 14:
						gcrList.setOslRez("Кишечная непроходимость");
						break;
					case 15:
						gcrList.setOslRez("Панкреатический свищ");
						break;
					case 16:
						gcrList.setOslRez("Острый панкреатит");
						break;
					case 17:
						gcrList.setOslRez("Сепсис");
						break;
					case 18:
						gcrList.setOslRez("Тяжелый сепсис");
						break;
					case 19:
						gcrList.setOslRez("Септический шок");
						break;
					case 20:
						gcrList.setOslRez("Желчный свищ");
						break;
					case 21:
						gcrList.setOslRez("Кишечный свищ");
						break;
					case 22:
						gcrList.setOslRez("Тромбоз воротной вены");
						break;
					case 23:
						gcrList.setOslRez("Тромбоз печеночной артерии");
						break;
					case 24:
						gcrList.setOslRez("Односторонняя пневмония");
						break;
					case 25:
						gcrList.setOslRez("Двусторонняя пневмония");
						break;
					case 26:
						gcrList.setOslRez("Гидроторакс");
						break;
					case 27:
						gcrList.setOslRez("Почечная недостаточность");
						break;
					case 28:
						gcrList.setOslRez("Сердечно-сосудистая недостаточность");
						break;
					case 29:
						gcrList.setOslRez("Полиорганная недостаточность");
						break;
					case 30:
						gcrList.setOslRez("Пострезекционная недостаточность A");
						break;
					case 31:
						gcrList.setOslRez("Пострезекционная недостаточность B");
						break;
					case 32:
						gcrList.setOslRez("Пострезекционная недостаточность C");
						break;

				}*/
				switch (Integer.valueOf(opGCRs.get(i).getClavien())) {
					case 0:
						gcrList.setClavien("--");
						break;
					case 1:
						gcrList.setClavien("I");
						break;
					case 2:
						gcrList.setClavien("II");
						break;
					case 3:
						gcrList.setClavien("IIIa");
						break;
					case 4:
						gcrList.setClavien("IIIb");
						break;
					case 5:
						gcrList.setClavien("IVa");
						break;
					case 6:
						gcrList.setClavien("IVb");
						break;
					case 7:
						gcrList.setClavien("V");
						break;
				}
				if (opGCRs.get(i).getDlitop().equals("")||opGCRs.get(i).getDlitop().equals("null"))
					gcrList.setDlitop("--");
				else
					gcrList.setDlitop(String.valueOf(opGCRs.get(i).getDlitop()));
				if (opGCRs.get(i).getGcrrazOch().equals("")||opGCRs.get(i).getGcrrazOch().equals("null"))
					gcrList.setRazOch("--");
				else
					gcrList.setRazOch(String.valueOf(opGCRs.get(i).getGcrrazOch()));
				if (opGCRs.get(i).getGcrblizSosud() == 0)
					gcrList.setBlizSosud("-");
				else
					gcrList.setBlizSosud("+");
				if (opGCRs.get(i).getGcralkRazOch().equals("") || opGCRs.get(i).getGcralkRazOch().equals("null"))
					gcrList.setAlkRazOch("--");
				else
					gcrList.setAlkRazOch(String.valueOf(opGCRs.get(i).getGcralkRazOch()));
				if (opGCRs.get(i).getGemotransfusia() == 0)
					gcrList.setGemotransfusia("-");
				else
					gcrList.setGemotransfusia("+");
				if (opGCRs.get(i).getGcrgemotransfusia_ml().equals("") || opGCRs.get(i).getGcrgemotransfusia_ml().equals("null"))
					gcrList.setGemotransfusia_ml("--");
				else
					gcrList.setGemotransfusia_ml(String.valueOf(opGCRs.get(i).getGcrgemotransfusia_ml()));
				if (opGCRs.get(i).getPlazma().equals("")||opGCRs.get(i).getPlazma().equals("null"))
					gcrList.setPlazma("--");
				else
					gcrList.setPlazma(String.valueOf(opGCRs.get(i).getPlazma()));
				if (opGCRs.get(i).getGdc() == 0)
					gcrList.setGdc("-");
				else
					gcrList.setGdc("+");
				if (opGCRs.get(i).getGcrgdcmin().equals("") || opGCRs.get(i).getGcrgdcmin().equals("null"))
					gcrList.setGdcmin("--");
				else
					gcrList.setGdcmin(String.valueOf(opGCRs.get(i).getGcrgdcmin()));
				if (opGCRs.get(i).getPerPolVeni() == 0)
					gcrList.setPerPolVeni("-");
				else
					gcrList.setPerPolVeni("+");
				if (opGCRs.get(i).getGcrperPolVeniMin().equals("") || opGCRs.get(i).getGcrperPolVeniMin().equals("null"))
					gcrList.setPerPolVeniMin("--");
				else
					gcrList.setPerPolVeniMin(String.valueOf(opGCRs.get(i).getGcrperPolVeniMin()));
				if (opGCRs.get(i).getBypass() == 0)
					gcrList.setBypass("-");
				else
					gcrList.setBypass("+");
				if (opGCRs.get(i).getTotIz() == 0)
					gcrList.setTotIz("-");
				else
					gcrList.setTotIz("+");
				if (opGCRs.get(i).getSelSosIz() == 0)
					gcrList.setSelSosIz("-");
				else
					gcrList.setSelSosIz("+");
				if (opGCRs.get(i).getSelIz() == 0)
					gcrList.setSelIz("-");
				else
					gcrList.setSelIz("+");
				if (opGCRs.get(i).getGcrselIzMin().equals("") || opGCRs.get(i).getGcrselIzMin().equals("null"))
					gcrList.setSelIzMin("-");
				else
					gcrList.setSelIzMin("+");
				if (opGCRs.get(i).getIntrRHA() == 0)
					gcrList.setIntrRHA("-");
				else
					gcrList.setIntrRHA("+");
				if (opGCRs.get(i).getTache() == 0)
					gcrList.setTache("-");
				else
					gcrList.setTache("+");
				opGCRLists.add(gcrList);
				gcrList = new OpGCRList();
			}
			return opGCRLists;
		}
		return null;
	}
}