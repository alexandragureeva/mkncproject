package com.amgureeva.binding.OpGCRBinding;

import com.amgureeva.Entities.Surgeries.OpGCR;

import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public interface OpGCRDAO {
	OpGCR findById(Integer id);

	List<OpGCR> findAll();

	void save(OpGCR opGCR);

	void update(OpGCR opGCR);

	void delete(Integer id);
}
