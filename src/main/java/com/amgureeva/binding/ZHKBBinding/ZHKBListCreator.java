package com.amgureeva.binding.ZHKBBinding;

/**
 * Created by Александра on 05.05.2016.
 */
public class ZHKBListCreator {
	
	public static String getAllVariants(String header) {
		header = header.replace("_ПОСТ", "");
		header = header.replace("_ВЫПИС", "");
		header = header.replace("_ОПЕР", "");
		if (header.equals("zhkb:OCENKA_TROF_STATUSA")) {
			return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 - \"Неудовлетворительный\"";
		}
		if (header.equals("zhkb:OSTRII_KALK_CHOLEC")) {
			return "1 - \"Катаральный\"; 2 - \"Деструктивный\"";
		}
		if (header.equals("zhkb:KALK_CHOLEC_TYPE")) {
			return "1 - \"Хронический\"; 2 - \"Острый\"";
		}
		if (header.equals("zhkb:MIRIZI_ZHELTUCHA")) {
			return "1 - \"С желтухой\"; 2 - \"Без желтухи\"";
		}
		if (header.equals("zhkb:MIRIZI_TYPE")) {
			return "1 - \"1 тип\"; 2 - \"2 тип\"; 3 - \"3 тип\"";
		}
		if (header.equals("zhkb:CHOLANGIOLITIAZ_ZHELTUCHA")) {
			return "1 - \"С желтухой\"; 2 - \"Без желтухи\"";
		}
		if (header.equals("zhkb:CHOLANGIOLITIAZ_TYPE")) {
			return "1 - \"Холедохолитиаз\"; 2 - \"Внутрипеченочный холангиолитиаз\"; " +
					"3 - \"Сочетание внутрипеченочного и внепеченочного холангиолитиаза\"";
		}
		if (header.equals("zhkb:CHOLECISTEKTOMIA")) {
			return "1 - \"Открытая\"; 2 - \"Лапароскопическая\"";
		}
		if (header.equals("zhkb:CHOLEC_DREN_METHOD")) {
			return "1 - \"По Холстеду\"; 2 - \"По Керу\"";
		}
		if (header.equals("zhkb:SVISH_ZHELCH_PUZIR_TYPE")) {
			return "1 - \"С ДПК\"; 2 - \"С толстой кишкой\"";
		}
		if (header.equals("zhkb:CHOLEC_PRI_MIRIZI_ZAVERSHEN")) {
			return "1 - \"Завершение с ГЕА\"; 2 - \"Завершение без ГЕА\"";
		}
		if (header.equals("zhkb:OSLOZH_POVR_POL_ORG_TYPE")) {
			return "1 - \"ДПК\"; 2 - \"Ободочной кишки\"";
		}
		if (header.equals("zhkb:OSLOZH_POVR_CHOLED_TYPE")) {
			return "1 - \"Неполное пересечение\"; 2 - \"Полное пересечение\"; 3 - \"Клипирование\"";
		}
		return "";	
	}
}
