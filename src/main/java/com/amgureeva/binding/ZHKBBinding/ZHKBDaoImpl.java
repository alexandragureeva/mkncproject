package com.amgureeva.binding.ZHKBBinding;

import com.amgureeva.Entities.Diagnosis.ZHKB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 04.05.2016.
 */
@Repository
public class ZHKBDaoImpl implements ZHKBDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    
    @Override
    public ZHKB findById(Integer id, boolean isAtStart) {

        if (id != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
            params.put("admissiondiag", (isAtStart)?1:0);

            String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=8" +
                    " AND admissiondiag=:admissiondiag";
            List<List<ZHKB>> result = null;
            try {
                result = namedParameterJdbcTemplate
                        .query(sql, params, new ZHKBDaoImpl.ZHKBMapper());
            } catch (EmptyResultDataAccessException e) {
                // do nothing, return null
            }
            result.remove(null);
            if (result == null || result.size() == 0)
                return null;
            if (isAtStart) {
                if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
                if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
            }
            if (!isAtStart) {
                if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
                if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
            }
        }
        return null;
    }

    @Override
    public List<ZHKB> findAll() {
        String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 8";
        List<List<ZHKB>> result = namedParameterJdbcTemplate.query(sql, new ZHKBDaoImpl.ZHKBMapper());
        return (result == null || result.size() == 0)?null :result.get(0);
    }

    @Override
    public void save(ZHKB zhkb) {
        String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
                + "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (zhkb.getId(),71, String.valueOf(zhkb.getCholecistit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),72, String.valueOf(zhkb.getOstriiCholecistit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),73, String.valueOf(zhkb.getOslozhnenii()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),74, String.valueOf(zhkb.getMirizi()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),75, String.valueOf(zhkb.getZhelt_mirizi()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),76, String.valueOf(zhkb.getType()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),77, String.valueOf(zhkb.getCholangiol()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),78, String.valueOf(zhkb.getZhelt_cholang()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),79, String.valueOf(zhkb.getType_cholang()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),80, String.valueOf(zhkb.getVodyanka()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),81, String.valueOf(zhkb.getSvich()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),82, String.valueOf(zhkb.getSmorch()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),83, String.valueOf(zhkb.getPeritonit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),84, String.valueOf(zhkb.getEmpiema()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),85, String.valueOf(zhkb.getCholangit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),86, String.valueOf(zhkb.getCholpankreotit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),12, String.valueOf(zhkb.getTrof_status()), zhkb.isDiagnosisAtStart()));
	}

    @Override
    public void update(ZHKB zhkb) {
        String sql = "UPDATE patientparametersvalue SET value=:value"
                + " WHERE patientId=:patientId and diseaseId=8 and parameterId=:parameterId and " +
                "admissiondiag=:admissiondiag";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),71, String.valueOf(zhkb.getCholecistit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),72, String.valueOf(zhkb.getOstriiCholecistit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),73, String.valueOf(zhkb.getOslozhnenii()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),74, String.valueOf(zhkb.getMirizi()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),75, String.valueOf(zhkb.getZhelt_mirizi()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),76, String.valueOf(zhkb.getType()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),77, String.valueOf(zhkb.getCholangiol()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),78, String.valueOf(zhkb.getZhelt_cholang()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),79, String.valueOf(zhkb.getType_cholang()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),80, String.valueOf(zhkb.getVodyanka()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),81, String.valueOf(zhkb.getSvich()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),82, String.valueOf(zhkb.getSmorch()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),83, String.valueOf(zhkb.getPeritonit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),84, String.valueOf(zhkb.getEmpiema()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),85, String.valueOf(zhkb.getCholangit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),86, String.valueOf(zhkb.getCholpankreotit()), zhkb.isDiagnosisAtStart()));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(zhkb.getId(),12, String.valueOf(zhkb.getTrof_status()), zhkb.isDiagnosisAtStart()));
    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=8";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }
    private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value, boolean admissiondiag) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("patientId", patientId);
        paramSource.addValue("parameterId", fieldId);
        paramSource.addValue("diseaseId", 8);
        paramSource.addValue("value", value);
        paramSource.addValue("admissiondiag", admissiondiag);
        return paramSource;
    }


    private static final class ZHKBMapper implements RowMapper<List<ZHKB>> {

        public List<ZHKB> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<ZHKB> zhkbList = new ArrayList<ZHKB>();
            ZHKB zhkb = new ZHKB();
            int i = 0;
            do {
                zhkb.setId(rs.getString("patientId"));
                zhkb.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
                int parameterId = rs.getInt("parameterId");
                switch(parameterId) {
                    case 71: i++;zhkb.setCholecistit(rs.getInt("value"));break;
                    case 72: i++; zhkb.setOstriiCholecistit(rs.getInt("value"));break;
					case 73: i++; zhkb.setOslozhnenii(rs.getInt("value"));break;
					case 74: i++; zhkb.setMirizi(rs.getInt("value"));break;
					case 75: i++; zhkb.setZhelt_mirizi(rs.getInt("value"));break;
					case 76: i++; zhkb.setType(rs.getInt("value"));break;
                    case 77: i++; zhkb.setCholangiol(rs.getInt("value"));break;
                    case 78: i++; zhkb.setZhelt_cholang(rs.getInt("value")); break;
                    case 79: i++; zhkb.setType_cholang(rs.getInt("value")); break;
                    case 80: i++; zhkb.setVodyanka(rs.getInt("value")); break;
                    case 81: i++; zhkb.setSvich(rs.getInt("value")); break;
                    case 82: i++; zhkb.setSmorch(rs.getInt("value")); break;
                    case 83: i++; zhkb.setPeritonit(rs.getInt("value")); break;
					case 84: i++; zhkb.setEmpiema(rs.getInt("value")); break;
					case 85: i++; zhkb.setCholangit(rs.getInt("value")); break;
					case 86: i++; zhkb.setCholpankreotit(rs.getInt("value")); break;
					case 12: if (i!=0 && i!=17) return null;i=1; zhkb.setTrof_status(rs.getInt("value")); break;
                }
                if (i==17) {
                    zhkbList.add(zhkb);
                    zhkb = new ZHKB();
                }
            }while (rs.next());
            return zhkbList;
        }
    }
}