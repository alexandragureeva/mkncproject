package com.amgureeva.binding.ZHKBBinding;

import com.amgureeva.Entities.Diagnosis.ZHKB;

import java.util.List;

/**
 * Created by Александра on 04.05.2016.
 */
public interface ZHKBDAO {
    ZHKB findById(Integer id, boolean isAtStart);

    List<ZHKB> findAll();

    void save(ZHKB patient);

    void update(ZHKB patient);

    void delete(Integer id);
}
