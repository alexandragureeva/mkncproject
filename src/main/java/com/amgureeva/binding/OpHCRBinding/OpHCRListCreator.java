package com.amgureeva.binding.OpHCRBinding;

import com.amgureeva.Entities.Surgeries.OpHCR;
import com.amgureeva.Entities.Surgeries.OpHCRList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public class OpHCRListCreator {
	public List<OpHCRList> createHcrList(List<OpHCR> opHCRs, PatientService patientService) {
		if (opHCRs != null && patientService != null) {
			List<OpHCRList> opHCRLists = new ArrayList<OpHCRList>();
			OpHCRList opHCRList = new OpHCRList();
			for (int i = 0; i < opHCRs.size(); i++) {
				opHCRList.setName(patientService.findPatientById(Integer.valueOf(opHCRs.get(i).getId())).getName());
				switch (Integer.valueOf(opHCRs.get(i).getHcroplech())) {
					case 0:
						opHCRList.setOplech("--");
						break;
					case 1:
						opHCRList.setOplech("Резекция печени");
						break;
					case 2:
						opHCRList.setOplech("Чрескожая РЧА (чРЧА)");
						break;
					case 3:
						opHCRList.setOplech("Алкоголизация");
						break;
				}
				switch (Integer.valueOf(opHCRs.get(i).getHcrobrezekcii())) {
					case 0:
						opHCRList.setObrezekcii("--");
						break;
					case 1:
						opHCRList.setObrezekcii("Обширная резекция ЛГГЭ");
						break;
					case 2:
						opHCRList.setObrezekcii("Обширная резекция ПГГЭ");
						break;
					case 3:
						opHCRList.setObrezekcii("Cегментарная резекция");
						break;
				}
				switch (Integer.valueOf(opHCRs.get(i).getHcrsegrez())) {
					case 0:
						opHCRList.setSegrez("--");
						break;
					case 1:
						opHCRList.setSegrez("Анатомическая");
						break;
					case 2:
						opHCRList.setSegrez("Неанатомическая");
						break;
				}
				switch (Integer.valueOf(opHCRs.get(i).getKrovopoteria())) {
					case 0:
						opHCRList.setKrovopoteria("--");
						break;
					case 1:
						opHCRList.setKrovopoteria("<500");
						break;
					case 2:
						opHCRList.setKrovopoteria("500-1000");
						break;
					case 3:
						opHCRList.setKrovopoteria(">1000");
						break;
				}
				switch (Integer.valueOf(opHCRs.get(i).getHcrchisloUdSeg())) {
					case 0:
						opHCRList.setChisloUdSeg("--");
						break;
					case 1:
						opHCRList.setChisloUdSeg("1");
						break;
					case 2:
						opHCRList.setChisloUdSeg("2");
						break;
					case 3:
						opHCRList.setChisloUdSeg("3");
						break;
				}
				switch (Integer.valueOf(opHCRs.get(i).getHcrchisloOch())) {
					case 0:
						opHCRList.setChisloOch("--");
						break;
					case 1:
						opHCRList.setChisloOch("1");
						break;
					case 2:
						opHCRList.setChisloOch("2");
						break;
					case 3:
						opHCRList.setChisloOch("3");
						break;
					case 4:
						opHCRList.setChisloOch(">3");
						break;
				}
				switch (Integer.valueOf(opHCRs.get(i).getHcrpolnRHA())) {
					case 0:
						opHCRList.setPolnRHA("--");
						break;
					case 1:
						opHCRList.setPolnRHA("Полная");
						break;
					case 2:
						opHCRList.setPolnRHA("Неполная");
						break;
				}
				switch (Integer.valueOf(opHCRs.get(i).getTnm())) {
					case 0:
						opHCRList.setTnm("--");
						break;
					case 1:
						opHCRList.setTnm("T1N0M0");
						break;
					case 2:
						opHCRList.setTnm("T2aN0M0");
						break;
					case 3:
						opHCRList.setTnm("T2bN0M0");
						break;
					case 4:
						opHCRList.setTnm("T3N0M0");
						break;
					case 5:
						opHCRList.setTnm("T1N1M0");
						break;
					case 6:
						opHCRList.setTnm("T2N1M0");
						break;
					case 7:
						opHCRList.setTnm("T3N1M0");
						break;
					case 8:
						opHCRList.setTnm("T4N0M0");
						break;
					case 9:
						opHCRList.setTnm("T4N1M0");
						break;
					case 10:
						opHCRList.setTnm("Любая T, N2M0");
						break;
					case 11:
						opHCRList.setTnm("Любая T, любая N, M1");
						break;
				}
				
				switch (Integer.valueOf(opHCRs.get(i).getClavien())) {
					case 0:
						opHCRList.setClavien("--");
						break;
					case 1:
						opHCRList.setClavien("I");
						break;
					case 2:
						opHCRList.setClavien("II");
						break;
					case 3:
						opHCRList.setClavien("IIIa");
						break;
					case 4:
						opHCRList.setClavien("IIIb");
						break;
					case 5:
						opHCRList.setClavien("IVa");
						break;
					case 6:
						opHCRList.setClavien("IVb");
						break;
					case 7:
						opHCRList.setClavien("V");
						break;
				}
				switch (Integer.valueOf(opHCRs.get(i).getOsl())) {
					case 0:
						opHCRList.setOsl("--");
						break;
					case 1:
						opHCRList.setOsl("Внутрибрюшное кровотечение");
						break;
					case 2:
						opHCRList.setOsl("ЖКК");
						break;
					case 3:
						opHCRList.setOsl("Печеночная недостаточность A");
						break;
					case 4:
						opHCRList.setOsl("Печеночная недостаточность B");
						break;
					case 5:
						opHCRList.setOsl("Печеночная недостаточность C");
						break;
					case 6:
						opHCRList.setOsl("Паренхиматозная желтуха");
						break;
					case 7:
						opHCRList.setOsl("Механическая желтуха");
						break;
					case 8:
						opHCRList.setOsl("Нагноение раны");
						break;
					case 9:
						opHCRList.setOsl("Холангит");
						break;
					case 10:
						opHCRList.setOsl("Холангиогенное абсцедирование");
						break;
					case 11:
						opHCRList.setOsl("Асцит");
						break;
					case 12:
						opHCRList.setOsl("Эвентрация");
						break;
					case 13:
						opHCRList.setOsl("Кишечная непроходимость");
						break;
					case 14:
						opHCRList.setOsl("Панкреатический свищ");
						break;
					case 15:
						opHCRList.setOsl("Острый панкреатит");
						break;
					case 16:
						opHCRList.setOsl("Сепсис");
						break;
					case 17:
						opHCRList.setOsl("Тяжелый сепсис");
						break;
					case 18:
						opHCRList.setOsl("Септический шок");
						break;
					case 19:
						opHCRList.setOsl("Желчный свищ");
						break;
					case 20:
						opHCRList.setOsl("Кишечный свищ");
						break;
					case 21:
						opHCRList.setOsl("Тромбоз воротной вены");
						break;
					case 22:
						opHCRList.setOsl("Тромбоз печеночной артерии");
						break;
					case 23:
						opHCRList.setOsl("Односторонняя пневмония");
						break;
					case 24:
						opHCRList.setOsl("Двусторонняя пневмония");
						break;
					case 25:
						opHCRList.setOsl("Гидроторакс");
						break;
					case 26:
						opHCRList.setOsl("Почечная недостаточность");
						break;
					case 27:
						opHCRList.setOsl("Сердечно-сосудистая недостаточность");
						break;
					case 28:
						opHCRList.setOsl("Полиорганная недостаточность");
						break;

				}
				if (opHCRs.get(i).getDlitop().equals("")||opHCRs.get(i).getDlitop().equals("null"))
					opHCRList.setDlitop("--");
				else
					opHCRList.setDlitop(String.valueOf(opHCRs.get(i).getDlitop()));
				if (opHCRs.get(i).getHcrrazOch().equals("")||opHCRs.get(i).getHcrrazOch().equals("null"))
					opHCRList.setRazOch("--");
				else
					opHCRList.setRazOch(String.valueOf(opHCRs.get(i).getHcrrazOch()));
				if (opHCRs.get(i).getHcrblizSosud() == 0)
					opHCRList.setBlizSosud("-");
				else
					opHCRList.setBlizSosud("+");
				if (opHCRs.get(i).getGemotransfusia() == 0)
					opHCRList.setGemotransfusia("-");
				else
					opHCRList.setGemotransfusia("+");
				if (opHCRs.get(i).getHcrgemotransfusia_ml().equals("")||opHCRs.get(i).getHcrgemotransfusia_ml().equals("null"))
					opHCRList.setGemotransfusia_ml("--");
				else
					opHCRList.setGemotransfusia_ml(String.valueOf(opHCRs.get(i).getHcrgemotransfusia_ml()));
				if (opHCRs.get(i).getPlazma().equals("")||opHCRs.get(i).getPlazma().equals("null"))
					opHCRList.setPlazma("--");
				else
					opHCRList.setPlazma(String.valueOf(opHCRs.get(i).getPlazma()));
				if (opHCRs.get(i).getGdc() == 0)
					opHCRList.setGdc("-");
				else
					opHCRList.setGdc("+");
				if (opHCRs.get(i).getHcrgdcmin().equals("")||opHCRs.get(i).getHcrgdcmin().equals("null"))
					opHCRList.setGdcmin("--");
				else
					opHCRList.setGdcmin(String.valueOf(opHCRs.get(i).getHcrgdcmin()));
				if (opHCRs.get(i).getPerPolVeni() == 0)
					opHCRList.setPerPolVeni("-");
				else
					opHCRList.setPerPolVeni("+");
				if (opHCRs.get(i).getHcrperPolVeniMin().equals("")||opHCRs.get(i).getHcrperPolVeniMin().equals("null"))
					opHCRList.setPerPolVeniMin("--");
				else
					opHCRList.setPerPolVeniMin(String.valueOf(opHCRs.get(i).getHcrperPolVeniMin()));
				if (opHCRs.get(i).getBypass() == 0)
					opHCRList.setBypass("-");
				else
					opHCRList.setBypass("+");
				if (opHCRs.get(i).getTotIz() == 0)
					opHCRList.setTotIz("-");
				else
					opHCRList.setTotIz("+");
				if (opHCRs.get(i).getSelSosIz() == 0)
					opHCRList.setSelSosIz("-");
				else
					opHCRList.setSelSosIz("+");
				if (opHCRs.get(i).getSelIz() == 0)
					opHCRList.setSelIz("-");
				else
					opHCRList.setSelIz("+");
				if (opHCRs.get(i).getHcrselIzMin().equals("")||opHCRs.get(i).getHcrselIzMin().equals("null"))
					opHCRList.setSelIzMin("-");
				else
					opHCRList.setSelIzMin("+");
				if (opHCRs.get(i).getIntrRHA() == 0)
					opHCRList.setIntrRHA("-");
				else
					opHCRList.setIntrRHA("+");
				opHCRLists.add(opHCRList);
				opHCRList = new OpHCRList();
			}
			return opHCRLists;
		}
		return null;
	}
}
