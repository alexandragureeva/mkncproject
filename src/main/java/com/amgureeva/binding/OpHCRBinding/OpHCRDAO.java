package com.amgureeva.binding.OpHCRBinding;

import com.amgureeva.Entities.Surgeries.OpHCR;

import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public interface OpHCRDAO {

	OpHCR findById(Integer id);

	List<OpHCR> findAll();

	void save(OpHCR opHCR);

	void update(OpHCR opHCR);

	void delete(Integer id);
}
