package com.amgureeva.binding.OpHCRBinding;

import com.amgureeva.Entities.Surgeries.OpHCR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 22.05.2016.
 */
@Repository
public class OpHCRDAOImpl implements OpHCRDAO {
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public OpHCR findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=2";

			List<OpHCR> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpHCRMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}

	@Override
	public List<OpHCR> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 2";
		List<List<OpHCR>> result = namedParameterJdbcTemplate.query(sql,
				new OpHCRMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(OpHCR opHCR) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),6, String.valueOf(opHCR.getKrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),7, String.valueOf(opHCR.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),9, String.valueOf(opHCR.getGemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),10, String.valueOf(opHCR.getHcrgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),11, String.valueOf(opHCR.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),12, String.valueOf(opHCR.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),13, String.valueOf(opHCR.getHcrgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),19, String.valueOf(opHCR.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),21, String.valueOf(opHCR.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),22, String.valueOf(opHCR.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),23, String.valueOf(opHCR.getSelSosIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),26, String.valueOf(opHCR.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),34, String.valueOf(opHCR.getHcroplech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),35, String.valueOf(opHCR.getHcrobrezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),36, String.valueOf(opHCR.getHcrsegrez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),37, String.valueOf(opHCR.getHcrchisloUdSeg())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),38, String.valueOf(opHCR.getHcrchisloOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),39, String.valueOf(opHCR.getHcrrazOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),40, String.valueOf(opHCR.getHcrblizSosud())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),41, String.valueOf(opHCR.getHcrpolnRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),44, String.valueOf(opHCR.getPerPolVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),45, String.valueOf(opHCR.getHcrperPolVeniMin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),46, String.valueOf(opHCR.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),47, String.valueOf(opHCR.getHcrselIzMin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),48, String.valueOf(opHCR.getIntrRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),25, String.valueOf(opHCR.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),1, String.valueOf(opHCR.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),2, String.valueOf(opHCR.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),103, String.valueOf(opHCR.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),104, String.valueOf(opHCR.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),105, String.valueOf(opHCR.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),106, String.valueOf(opHCR.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),107, String.valueOf(opHCR.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),108, String.valueOf(opHCR.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),109, String.valueOf(opHCR.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),110, String.valueOf(opHCR.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),111, String.valueOf(opHCR.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),112, String.valueOf(opHCR.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),113, String.valueOf(opHCR.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),114, String.valueOf(opHCR.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),115, String.valueOf(opHCR.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),116, String.valueOf(opHCR.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),117, String.valueOf(opHCR.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),118, String.valueOf(opHCR.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),119, String.valueOf(opHCR.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),120, String.valueOf(opHCR.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),121, String.valueOf(opHCR.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),122, String.valueOf(opHCR.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),123, String.valueOf(opHCR.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),124, String.valueOf(opHCR.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),125, String.valueOf(opHCR.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),126, String.valueOf(opHCR.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),127, String.valueOf(opHCR.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),128, String.valueOf(opHCR.getPoliorganNedostat())));
	}

	@Override
	public void update(OpHCR opHCR) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=2 and parameterId=:parameterId";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),6, String.valueOf(opHCR.getKrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),7, String.valueOf(opHCR.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),9, String.valueOf(opHCR.getGemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),10, String.valueOf(opHCR.getHcrgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),11, String.valueOf(opHCR.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),12, String.valueOf(opHCR.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),13, String.valueOf(opHCR.getHcrgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),19, String.valueOf(opHCR.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),21, String.valueOf(opHCR.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),22, String.valueOf(opHCR.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),23, String.valueOf(opHCR.getSelSosIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),26, String.valueOf(opHCR.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),34, String.valueOf(opHCR.getHcroplech())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),35, String.valueOf(opHCR.getHcrobrezekcii())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),36, String.valueOf(opHCR.getHcrsegrez())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),37, String.valueOf(opHCR.getHcrchisloUdSeg())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),38, String.valueOf(opHCR.getHcrchisloOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),39, String.valueOf(opHCR.getHcrrazOch())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),40, String.valueOf(opHCR.getHcrblizSosud())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),41, String.valueOf(opHCR.getHcrpolnRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),44, String.valueOf(opHCR.getPerPolVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),45, String.valueOf(opHCR.getHcrperPolVeniMin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),46, String.valueOf(opHCR.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),47, String.valueOf(opHCR.getHcrselIzMin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),48, String.valueOf(opHCR.getIntrRHA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),25, String.valueOf(opHCR.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),1, String.valueOf(opHCR.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),2, String.valueOf(opHCR.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),103, String.valueOf(opHCR.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),104, String.valueOf(opHCR.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),105, String.valueOf(opHCR.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),106, String.valueOf(opHCR.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),107, String.valueOf(opHCR.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),108, String.valueOf(opHCR.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),109, String.valueOf(opHCR.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),110, String.valueOf(opHCR.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),111, String.valueOf(opHCR.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),112, String.valueOf(opHCR.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),113, String.valueOf(opHCR.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),114, String.valueOf(opHCR.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),115, String.valueOf(opHCR.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),116, String.valueOf(opHCR.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),117, String.valueOf(opHCR.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),118, String.valueOf(opHCR.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),119, String.valueOf(opHCR.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),120, String.valueOf(opHCR.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),121, String.valueOf(opHCR.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),122, String.valueOf(opHCR.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),123, String.valueOf(opHCR.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),124, String.valueOf(opHCR.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),125, String.valueOf(opHCR.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),126, String.valueOf(opHCR.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),127, String.valueOf(opHCR.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opHCR.getId(),128, String.valueOf(opHCR.getPoliorganNedostat())));

	}

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=2";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 2);
		paramSource.addValue("value", value);
		return paramSource;
	}


	private static final class OpHCRMapper implements RowMapper<List<OpHCR>> {

		public List<OpHCR> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<OpHCR> alList = new ArrayList<OpHCR>();
			OpHCR opHCR = new OpHCR();

			int i = 0;
			do {
				opHCR.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 1: i++;  opHCR.setRezekciaType(rs.getInt("value"));break;
					case 2: i++; opHCR.setRazdelenieTkanei(rs.getInt("value"));break;
					case 6:  if (i!=0 && i!=54) return null;i=1;  opHCR.setKrovopoteria(rs.getInt("value"));break;
					case 7: i++; opHCR.setDlitop(rs.getString("value"));break;
					case 9: i++; opHCR.setGemotransfusia(rs.getInt("value"));break;
					case 10: i++; opHCR.setHcrgemotransfusia_ml(rs.getString("value"));break;
					case 11: i++; opHCR.setPlazma(rs.getString("value")); break;
					case 12: i++; opHCR.setGdc(rs.getInt("value")); break;
					case 13: i++; opHCR.setHcrgdcmin(rs.getString("value")); break;
					case 19: i++; opHCR.setTnm(rs.getInt("value")); break;
					case 21: i++; opHCR.setBypass(rs.getInt("value")); break;
					case 22: i++; opHCR.setTotIz(rs.getInt("value")); break;
					case 23: i++; opHCR.setSelSosIz(rs.getInt("value")); break;
					case 26: i++; opHCR.setClavien(rs.getInt("value")); break;
					case 34: i++; opHCR.setHcroplech(rs.getInt("value")); break;
					case 35: i++; opHCR.setHcrobrezekcii(rs.getInt("value")); break;
					case 36: i++; opHCR.setHcrsegrez(rs.getInt("value")); break;
					case 37: i++; opHCR.setHcrchisloUdSeg(rs.getInt("value")); break;
					case 38: i++; opHCR.setHcrchisloOch(rs.getInt("value")); break;
					case 39: i++; opHCR.setHcrrazOch(rs.getString("value")); break;
					case 40: i++; opHCR.setHcrblizSosud(rs.getInt("value")); break;
					case 41: i++; opHCR.setHcrpolnRHA(rs.getInt("value")); break;
					case 44: i++; opHCR.setPerPolVeni(rs.getInt("value")); break;
					case 45: i++; opHCR.setHcrperPolVeniMin(rs.getString("value")); break;
					case 46: i++; opHCR.setSelIz(rs.getInt("value")); break;
					case 47: i++; opHCR.setHcrselIzMin(rs.getString("value")); break;
					case 48: i++; opHCR.setIntrRHA(rs.getInt("value")); break;
					case 25: i++; opHCR.setOsl(rs.getInt("value")); break;
					case 103: i++; opHCR.setVnutrKrovot(rs.getInt("value")); break;
					case 104: i++; opHCR.setZhkk(rs.getInt("value")); break;
					case 105: i++; opHCR.setPechNedostatA(rs.getInt("value")); break;
					case 106: i++; opHCR.setPechNedostatB(rs.getInt("value")); break;
					case 107: i++; opHCR.setPechNedostatC(rs.getInt("value")); break;
					case 108: i++; opHCR.setNagnoenie(rs.getInt("value")); break;
					case 109: i++; opHCR.setCholangit(rs.getInt("value")); break;
					case 110: i++; opHCR.setCholAbscedir(rs.getInt("value")); break;
					case 111: i++; opHCR.setAscit(rs.getInt("value")); break;
					case 112: i++; opHCR.setEventracia(rs.getInt("value")); break;
					case 113: i++; opHCR.setKishNeprohod(rs.getInt("value")); break;
					case 114: i++; opHCR.setPankreatSvich(rs.getInt("value")); break;
					case 115: i++; opHCR.setOstriiSvich(rs.getInt("value")); break;
					case 116: i++; opHCR.setSepsis(rs.getInt("value")); break;
					case 117: i++; opHCR.setTyazhSepsis(rs.getInt("value")); break;
					case 118: i++; opHCR.setSeptShok(rs.getInt("value")); break;
					case 119: i++; opHCR.setZhelchSvish(rs.getInt("value")); break;
					case 120: i++; opHCR.setKishSvish(rs.getInt("value")); break;
					case 121: i++; opHCR.setTrombVorotVeni(rs.getInt("value")); break;
					case 122: i++; opHCR.setTrombozPechArter(rs.getInt("value")); break;
					case 123: i++; opHCR.setOdnostorPnevm(rs.getInt("value")); break;
					case 124: i++; opHCR.setDvustorPnevm(rs.getInt("value")); break;
					case 125: i++; opHCR.setGidrotorax(rs.getInt("value")); break;
					case 126: i++; opHCR.setPochNedostat(rs.getInt("value")); break;
					case 127: i++; opHCR.setSerdSosNedostat(rs.getInt("value")); break;
					case 128: i++; opHCR.setPoliorganNedostat(rs.getInt("value")); break;
				}
				if (i == 54) {
					alList.add(opHCR);
					opHCR = new OpHCR();
				}
			}while (rs.next());
			return alList;
		}
	}
}

