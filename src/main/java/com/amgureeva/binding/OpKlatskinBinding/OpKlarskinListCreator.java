package com.amgureeva.binding.OpKlatskinBinding;

import com.amgureeva.Entities.Surgeries.OpKlatskin;
import com.amgureeva.Entities.Surgeries.OpKlatskinList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public class OpKlarskinListCreator {

	public List<OpKlatskinList> createKlatskinLists(List<OpKlatskin> opKlatskins, PatientService patientService) {
		if (opKlatskins != null && patientService != null) {
			List<OpKlatskinList> opKlatskinLists = new ArrayList<OpKlatskinList>();
			OpKlatskinList opKlatskinList = new OpKlatskinList();
			for (int i = 0; i < opKlatskins.size(); i++) {
				opKlatskinList.setName(patientService.findPatientById(Integer.valueOf(opKlatskins.get(i).getId())).getName());
				switch (Integer.valueOf(opKlatskins.get(i).getRezekcia())) {
					case 0:
						opKlatskinList.setRezekcia("--");
						break;
					case 1:
						opKlatskinList.setRezekcia("ЛГГ");
						break;
					case 2:
						opKlatskinList.setRezekcia("ПГГ");
						break;
					case 3:
						opKlatskinList.setRezekcia("Расширенная ПГГ");
						break;
					case 4:
						opKlatskinList.setRezekcia("Расширенная ЛГГ");
						break;
					case 5:
						opKlatskinList.setRezekcia("Секторэктомия");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getKlatsosudrezekcia())) {
					case 0:
						opKlatskinList.setSosudrezekcia("--");
						break;
					case 1:
						opKlatskinList.setSosudrezekcia("Резекция воротной вены");
						break;
					case 2:
						opKlatskinList.setSosudrezekcia("Резекция печеночной артерии");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getKlatvorotrezekcia())) {
					case 0:
						opKlatskinList.setVorotrezekcia("--");
						break;
					case 1:
						opKlatskinList.setVorotrezekcia("Краевая");
						break;
					case 2:
						opKlatskinList.setVorotrezekcia("Циркулярная без протезирования");
						break;
					case 3:
						opKlatskinList.setVorotrezekcia("Циркулярная с протезированием");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getKrovopoteria())) {
					case 0:
						opKlatskinList.setKrovopoteria("--");
						break;
					case 1:
						opKlatskinList.setKrovopoteria("<500");
						break;
					case 2:
						opKlatskinList.setKrovopoteria("500-1000");
						break;
					case 3:
						opKlatskinList.setKrovopoteria(">1000");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getGeatype1())) {
					case 0:
						opKlatskinList.setGeatype1("--");
						break;
					case 1:
						opKlatskinList.setGeatype1("Единый");
						break;
					case 2:
						opKlatskinList.setGeatype1("Раздельный");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getGeatype2())) {
					case 0:
						opKlatskinList.setGeatype2("--");
						break;
					case 1:
						opKlatskinList.setGeatype2("Моно-ГЕА");
						break;
					case 2:
						opKlatskinList.setGeatype2("Би-ГЕА");
						break;
					case 3:
						opKlatskinList.setGeatype2("Три-ГЕА");
						break;
					case 4:
						opKlatskinList.setGeatype2("Тетра-ГЕА");
						break;
					case 5:
						opKlatskinList.setGeatype2("Пента-ГЕА");
						break;
					case 6:
						opKlatskinList.setGeatype2("Мульти-ГЕА");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getGeatype3())) {
					case 0:
						opKlatskinList.setGeatype3("--");
						break;
					case 1:
						opKlatskinList.setGeatype3("С ТПД");
						break;
					case 2:
						opKlatskinList.setGeatype3("Без ТПД");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getRadikal())) {
					case 0:
						opKlatskinList.setRadikal("--");
						break;
					case 1:
						opKlatskinList.setRadikal("R0");
						break;
					case 2:
						opKlatskinList.setRadikal("R1");
						break;
					case 3:
						opKlatskinList.setRadikal("R2");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getLimfodissekcia())) {
					case 0:
						opKlatskinList.setLimfodissekcia("--");
						break;
					case 1:
						opKlatskinList.setLimfodissekcia("ГДС");
						break;
					case 2:
						opKlatskinList.setLimfodissekcia("ГДС+ОПА");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getTnm())) {
					case 0:
						opKlatskinList.setTnm("--");
						break;
					case 1:
						opKlatskinList.setTnm("T1N0M0");
						break;
					case 2:
						opKlatskinList.setTnm("T2aN0M0");
						break;
					case 3:
						opKlatskinList.setTnm("T2bN0M0");
						break;
					case 4:
						opKlatskinList.setTnm("T3N0M0");
						break;
					case 5:
						opKlatskinList.setTnm("T1N1M0");
						break;
					case 6:
						opKlatskinList.setTnm("T2N1M0");
						break;
					case 7:
						opKlatskinList.setTnm("T3N1M0");
						break;
					case 8:
						opKlatskinList.setTnm("T4N0M0");
						break;
					case 9:
						opKlatskinList.setTnm("T4N1M0");
						break;
					case 10:
						opKlatskinList.setTnm("Любая T, N2M0");
						break;
					case 11:
						opKlatskinList.setTnm("Любая T, любая N, M1");
						break;
				}
				switch (Integer.valueOf(opKlatskins.get(i).getOsl())) {
					case 0:
						opKlatskinList.setOsl("--");
						break;
					case 1:
						opKlatskinList.setOsl("Внутрибрюшное кровотечение");
						break;
					case 2:
						opKlatskinList.setOsl("ЖКК");
						break;
					case 3:
						opKlatskinList.setOsl("Печеночная недостаточность A");
						break;
					case 4:
						opKlatskinList.setOsl("Печеночная недостаточность B");
						break;
					case 5:
						opKlatskinList.setOsl("Печеночная недостаточность C");
						break;
					case 6:
						opKlatskinList.setOsl("Паренхиматозная желтуха");
						break;
					case 7:
						opKlatskinList.setOsl("Механическая желтуха");
						break;
					case 8:
						opKlatskinList.setOsl("Нагноение раны");
						break;
					case 9:
						opKlatskinList.setOsl("Холангит");
						break;
					case 10:
						opKlatskinList.setOsl("Холангиогенное абсцедирование");
						break;
					case 11:
						opKlatskinList.setOsl("Асцит");
						break;
					case 12:
						opKlatskinList.setOsl("Эвентрация");
						break;
					case 13:
						opKlatskinList.setOsl("Кишечная непроходимость");
						break;
					case 14:
						opKlatskinList.setOsl("Панкреатический свищ");
						break;
					case 15:
						opKlatskinList.setOsl("Острый панкреатит");
						break;
					case 16:
						opKlatskinList.setOsl("Сепсис");
						break;
					case 17:
						opKlatskinList.setOsl("Тяжелый сепсис");
						break;
					case 18:
						opKlatskinList.setOsl("Септический шок");
						break;
					case 19:
						opKlatskinList.setOsl("Желчный свищ");
						break;
					case 20:
						opKlatskinList.setOsl("Кишечный свищ");
						break;
					case 21:
						opKlatskinList.setOsl("Тромбоз воротной вены");
						break;
					case 22:
						opKlatskinList.setOsl("Тромбоз печеночной артерии");
						break;
					case 23:
						opKlatskinList.setOsl("Односторонняя пневмония");
						break;
					case 24:
						opKlatskinList.setOsl("Двусторонняя пневмония");
						break;
					case 25:
						opKlatskinList.setOsl("Гидроторакс");
						break;
					case 26:
						opKlatskinList.setOsl("Почечная недостаточность");
						break;
					case 27:
						opKlatskinList.setOsl("Сердечно-сосудистая недостаточность");
						break;
					case 28:
						opKlatskinList.setOsl("Полиорганная недостаточность");
						break;

				}
				switch (Integer.valueOf(opKlatskins.get(i).getClavien())) {
					case 0:
						opKlatskinList.setClavien("--");
						break;
					case 1:
						opKlatskinList.setClavien("I");
						break;
					case 2:
						opKlatskinList.setClavien("II");
						break;
					case 3:
						opKlatskinList.setClavien("IIIa");
						break;
					case 4:
						opKlatskinList.setClavien("IIIb");
						break;
					case 5:
						opKlatskinList.setClavien("IVa");
						break;
					case 6:
						opKlatskinList.setClavien("IVb");
						break;
					case 7:
						opKlatskinList.setClavien("V");
						break;
				}
				if (opKlatskins.get(i).getDlitop().equals("")||opKlatskins.get(i).getDlitop().equals("null"))
					opKlatskinList.setDlitop("--");
				else
					opKlatskinList.setDlitop(String.valueOf(opKlatskins.get(i).getDlitop()));
				if (opKlatskins.get(i).getGemotransfusia() == 0)
					opKlatskinList.setGemotransfusia("-");
				else
					opKlatskinList.setGemotransfusia("+");
				if (opKlatskins.get(i).getKlatgemotransfusia_ml().equals("")||opKlatskins.get(i).getKlatgemotransfusia_ml().equals("null"))
					opKlatskinList.setGemotransfusia_ml("--");
				else
					opKlatskinList.setGemotransfusia_ml(String.valueOf(opKlatskins.get(i).getKlatgemotransfusia_ml()));
				if (opKlatskins.get(i).getPlazma().equals("")||opKlatskins.get(i).getPlazma().equals("null"))
					opKlatskinList.setPlazma("--");
				else
					opKlatskinList.setPlazma(String.valueOf(opKlatskins.get(i).getPlazma()));
				if (opKlatskins.get(i).getGdc() == 0)
					opKlatskinList.setGdc("-");
				else
					opKlatskinList.setGdc("+");
				if (opKlatskins.get(i).getKlatgdcmin().equals("")||opKlatskins.get(i).getKlatgdcmin().equals("null"))
					opKlatskinList.setGdcmin("--");
				else
					opKlatskinList.setGdcmin(String.valueOf(opKlatskins.get(i).getKlatgdcmin()));
				if (opKlatskins.get(i).getBypass() == 0)
					opKlatskinList.setBypass("-");
				else
					opKlatskinList.setBypass("+");
				if (opKlatskins.get(i).getTotIz() == 0)
					opKlatskinList.setTotIz("-");
				else
					opKlatskinList.setTotIz("+");
				if (opKlatskins.get(i).getSelIz() == 0)
					opKlatskinList.setSelIz("-");
				else
					opKlatskinList.setSelIz("+");
				opKlatskinLists.add(opKlatskinList);
				opKlatskinList = new OpKlatskinList();
			}
			return opKlatskinLists;
		}
		return null;
	}
}

