package com.amgureeva.binding.OpKlatskinBinding;

import com.amgureeva.Entities.Surgeries.OpKlatskin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 22.05.2016.
 */
@Repository
public class OpKlatskinDAOImpl implements OpKlatskinDAO {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	public void setNamedParameterJdbcTemplate(
			NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}

	@Override
	public OpKlatskin findById(Integer id) {
		if (id != null) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id", id);

			String sql = "SELECT * FROM surgeryparametersvalue WHERE surgeryId=:id and diseaseId=0";

			List<OpKlatskin> result = null;
			try {
				result = namedParameterJdbcTemplate
						.queryForObject(sql, params, new OpKlatskinDAOImpl.OpKlatskinMapper());
			} catch (EmptyResultDataAccessException e) {
				// do nothing, return null
			}
			return (result == null || result.size() == 0) ? null : result.get(0);
		}
		return null;
	}
	@Override
	public List<OpKlatskin> findAll() {
		String sql = "SELECT surgeryId, parameterId, value FROM surgeryparametersvalue WHERE diseaseId = 0";
		List<List<OpKlatskin>> result = namedParameterJdbcTemplate.query(sql,
				new OpKlatskinDAOImpl.OpKlatskinMapper());
		result.remove(null);
		return (result == null || result.size() == 0)?null :result.get(0);
	}

	@Override
	public void save(OpKlatskin opKlatskin) {
		String sql = "INSERT INTO surgeryparametersvalue(surgeryId, parameterId, value, diseaseId) "
				+ "VALUES ( :surgeryId, :parameterId, :value, :diseaseId)";
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),3, String.valueOf(opKlatskin.getRezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),4, String.valueOf(opKlatskin.getKlatsosudrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),5, String.valueOf(opKlatskin.getKlatvorotrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),6, String.valueOf(opKlatskin.getKrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),7, String.valueOf(opKlatskin.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),9, String.valueOf(opKlatskin.getGemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),10, String.valueOf(opKlatskin.getKlatgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),11, String.valueOf(opKlatskin.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),12, String.valueOf(opKlatskin.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),13, String.valueOf(opKlatskin.getKlatgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),14, String.valueOf(opKlatskin.getGeatype1())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),15, String.valueOf(opKlatskin.getGeatype2())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),16, String.valueOf(opKlatskin.getGeatype3())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),17, String.valueOf(opKlatskin.getRadikal())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),18, String.valueOf(opKlatskin.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),19, String.valueOf(opKlatskin.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),21, String.valueOf(opKlatskin.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),22, String.valueOf(opKlatskin.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),23, String.valueOf(opKlatskin.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),25, String.valueOf(opKlatskin.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),26, String.valueOf(opKlatskin.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),1, String.valueOf(opKlatskin.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),2, String.valueOf(opKlatskin.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),103, String.valueOf(opKlatskin.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),104, String.valueOf(opKlatskin.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),105, String.valueOf(opKlatskin.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),106, String.valueOf(opKlatskin.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),107, String.valueOf(opKlatskin.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),108, String.valueOf(opKlatskin.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),109, String.valueOf(opKlatskin.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),110, String.valueOf(opKlatskin.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),111, String.valueOf(opKlatskin.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),112, String.valueOf(opKlatskin.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),113, String.valueOf(opKlatskin.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),114, String.valueOf(opKlatskin.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),115, String.valueOf(opKlatskin.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),116, String.valueOf(opKlatskin.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),117, String.valueOf(opKlatskin.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),118, String.valueOf(opKlatskin.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),119, String.valueOf(opKlatskin.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),120, String.valueOf(opKlatskin.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),121, String.valueOf(opKlatskin.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),122, String.valueOf(opKlatskin.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),123, String.valueOf(opKlatskin.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),124, String.valueOf(opKlatskin.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),125, String.valueOf(opKlatskin.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),126, String.valueOf(opKlatskin.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),127, String.valueOf(opKlatskin.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),128, String.valueOf(opKlatskin.getPoliorganNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),129, String.valueOf(opKlatskin.getNesostAnastomoza())));
	}


	@Override
	public void update(OpKlatskin opKlatskin) {
		String sql = "UPDATE surgeryparametersvalue SET value=:value"
				+ " WHERE surgeryId=:surgeryId and diseaseId=0 and parameterId=:parameterId";

		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),3, String.valueOf(opKlatskin.getRezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),4, String.valueOf(opKlatskin.getKlatsosudrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),5, String.valueOf(opKlatskin.getKlatvorotrezekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),6, String.valueOf(opKlatskin.getKrovopoteria())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),7, String.valueOf(opKlatskin.getDlitop())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),9, String.valueOf(opKlatskin.getGemotransfusia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),10, String.valueOf(opKlatskin.getKlatgemotransfusia_ml())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),11, String.valueOf(opKlatskin.getPlazma())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),12, String.valueOf(opKlatskin.getGdc())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),13, String.valueOf(opKlatskin.getKlatgdcmin())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),14, String.valueOf(opKlatskin.getGeatype1())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),15, String.valueOf(opKlatskin.getGeatype2())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),16, String.valueOf(opKlatskin.getGeatype3())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),17, String.valueOf(opKlatskin.getRadikal())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),18, String.valueOf(opKlatskin.getLimfodissekcia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),19, String.valueOf(opKlatskin.getTnm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),21, String.valueOf(opKlatskin.getBypass())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),22, String.valueOf(opKlatskin.getTotIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),23, String.valueOf(opKlatskin.getSelIz())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),25, String.valueOf(opKlatskin.getOsl())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),26, String.valueOf(opKlatskin.getClavien())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),1, String.valueOf(opKlatskin.getRezekciaType())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),2, String.valueOf(opKlatskin.getRazdelenieTkanei())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),103, String.valueOf(opKlatskin.getVnutrKrovot())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),104, String.valueOf(opKlatskin.getZhkk())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),105, String.valueOf(opKlatskin.getPechNedostatA())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),106, String.valueOf(opKlatskin.getPechNedostatB())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),107, String.valueOf(opKlatskin.getPechNedostatC())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),108, String.valueOf(opKlatskin.getNagnoenie())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),109, String.valueOf(opKlatskin.getCholangit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),110, String.valueOf(opKlatskin.getCholAbscedir())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),111, String.valueOf(opKlatskin.getAscit())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),112, String.valueOf(opKlatskin.getEventracia())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),113, String.valueOf(opKlatskin.getKishNeprohod())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),114, String.valueOf(opKlatskin.getPankreatSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),115, String.valueOf(opKlatskin.getOstriiSvich())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),116, String.valueOf(opKlatskin.getSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),117, String.valueOf(opKlatskin.getTyazhSepsis())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),118, String.valueOf(opKlatskin.getSeptShok())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),119, String.valueOf(opKlatskin.getZhelchSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),120, String.valueOf(opKlatskin.getKishSvish())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),121, String.valueOf(opKlatskin.getTrombVorotVeni())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),122, String.valueOf(opKlatskin.getTrombozPechArter())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),123, String.valueOf(opKlatskin.getOdnostorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),124, String.valueOf(opKlatskin.getDvustorPnevm())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),125, String.valueOf(opKlatskin.getGidrotorax())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),126, String.valueOf(opKlatskin.getPochNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),127, String.valueOf(opKlatskin.getSerdSosNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),128, String.valueOf(opKlatskin.getPoliorganNedostat())));
		namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
				(opKlatskin.getId(),129, String.valueOf(opKlatskin.getNesostAnastomoza())));
	}
	

	@Override
	public void delete(Integer id) {
		String sql = "DELETE FROM surgeryparametersvalue WHERE surgeryId= :id and diseaseId=0";
		namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
	}

	private SqlParameterSource getSqlParameterByModel(String surgeryId, int fieldId, String value) {
		MapSqlParameterSource paramSource = new MapSqlParameterSource();
		paramSource.addValue("surgeryId", surgeryId);
		paramSource.addValue("parameterId", fieldId);
		paramSource.addValue("diseaseId", 0);
		paramSource.addValue("value", value);
		return paramSource;
	}

	private static final class OpKlatskinMapper implements RowMapper<List<OpKlatskin>> {
		public List<OpKlatskin> mapRow(ResultSet rs, int rowNum) throws SQLException {
			List<OpKlatskin> list = new ArrayList<OpKlatskin>();
			OpKlatskin opKlatskin = new OpKlatskin();
			int i = 0;
			do {
				opKlatskin.setId(rs.getString("surgeryId"));
				int parameterId = rs.getInt("parameterId");
				switch(parameterId) {
					case 1: i++; opKlatskin.setRezekciaType(rs.getInt("value")); break;
					case 2: i++; opKlatskin.setRazdelenieTkanei(rs.getInt("value"));break;
					case 3: if (i!=0 && i!=50) return null;i=1; opKlatskin.setRezekcia(rs.getInt("value")); break;
					case 4: i++; opKlatskin.setKlatsosudrezekcia(rs.getInt("value"));break;
					case 5: i++; opKlatskin.setKlatvorotrezekcia(rs.getInt("value"));break;
					case 6: i++; opKlatskin.setKrovopoteria(rs.getInt("value"));break;
					case 7: i++; opKlatskin.setDlitop(rs.getString("value"));break;
					case 9: i++; opKlatskin.setGemotransfusia(rs.getInt("value"));break;
					case 10: i++; opKlatskin.setKlatgemotransfusia_ml(rs.getString("value"));break;
					case 11: i++; opKlatskin.setPlazma(rs.getString("value")); break;
					case 12: i++; opKlatskin.setGdc(rs.getInt("value")); break;
					case 13: i++; opKlatskin.setKlatgdcmin(rs.getString("value")); break;
					case 14: i++; opKlatskin.setGeatype1(rs.getInt("value")); break;
					case 15: i++; opKlatskin.setGeatype2(rs.getInt("value")); break;
					case 16: i++; opKlatskin.setGeatype3(rs.getInt("value")); break;
					case 17: i++; opKlatskin.setRadikal(rs.getInt("value")); break;
					case 18: i++; opKlatskin.setLimfodissekcia(rs.getInt("value")); break;
					case 19: i++; opKlatskin.setTnm(rs.getInt("value")); break;
					case 21: i++; opKlatskin.setBypass(rs.getInt("value")); break;
					case 22: i++; opKlatskin.setTotIz(rs.getInt("value")); break;
					case 23: i++; opKlatskin.setSelIz(rs.getInt("value")); break;
					case 25: i++; opKlatskin.setOsl(rs.getInt("value")); break;
					case 26: i++; opKlatskin.setClavien(rs.getInt("value")); break;
					case 103: i++; opKlatskin.setVnutrKrovot(rs.getInt("value")); break;
					case 104: i++; opKlatskin.setZhkk(rs.getInt("value")); break;
					case 105: i++; opKlatskin.setPechNedostatA(rs.getInt("value")); break;
					case 106: i++; opKlatskin.setPechNedostatB(rs.getInt("value")); break;
					case 107: i++; opKlatskin.setPechNedostatC(rs.getInt("value")); break;
					case 108: i++; opKlatskin.setNagnoenie(rs.getInt("value")); break;
					case 109: i++; opKlatskin.setCholangit(rs.getInt("value")); break;
					case 110: i++; opKlatskin.setCholAbscedir(rs.getInt("value")); break;
					case 111: i++; opKlatskin.setAscit(rs.getInt("value")); break;
					case 112: i++; opKlatskin.setEventracia(rs.getInt("value")); break;
					case 113: i++; opKlatskin.setKishNeprohod(rs.getInt("value")); break;
					case 114: i++; opKlatskin.setPankreatSvich(rs.getInt("value")); break;
					case 115: i++; opKlatskin.setOstriiSvich(rs.getInt("value")); break;
					case 116: i++; opKlatskin.setSepsis(rs.getInt("value")); break;
					case 117: i++; opKlatskin.setTyazhSepsis(rs.getInt("value")); break;
					case 118: i++; opKlatskin.setSeptShok(rs.getInt("value")); break;
					case 119: i++; opKlatskin.setZhelchSvish(rs.getInt("value")); break;
					case 120: i++; opKlatskin.setKishSvish(rs.getInt("value")); break;
					case 121: i++; opKlatskin.setTrombVorotVeni(rs.getInt("value")); break;
					case 122: i++; opKlatskin.setTrombozPechArter(rs.getInt("value")); break;
					case 123: i++; opKlatskin.setOdnostorPnevm(rs.getInt("value")); break;
					case 124: i++; opKlatskin.setDvustorPnevm(rs.getInt("value")); break;
					case 125: i++; opKlatskin.setGidrotorax(rs.getInt("value")); break;
					case 126: i++; opKlatskin.setPochNedostat(rs.getInt("value")); break;
					case 127: i++; opKlatskin.setSerdSosNedostat(rs.getInt("value")); break;
					case 128: i++; opKlatskin.setPoliorganNedostat(rs.getInt("value")); break;
					case 129: i++; opKlatskin.setNesostAnastomoza(rs.getInt("value")); break;
				}
				if (i == 50) {
					list.add(opKlatskin);
					opKlatskin = new OpKlatskin();
				}
			}while (rs.next());
			return list;
		}
	}
}

