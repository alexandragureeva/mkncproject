package com.amgureeva.binding.OpKlatskinBinding;

import com.amgureeva.Entities.Surgeries.OpKlatskin;

import java.util.List;

/**
 * Created by Nikita on 22.05.2016.
 */
public interface OpKlatskinDAO {
	OpKlatskin findById(Integer id);

	List<OpKlatskin> findAll();

	void save(OpKlatskin opKlatskin);

	void update(OpKlatskin opKlatskin);

	void delete(Integer id);
}
