package com.amgureeva.binding.AlveokokozBinding;

import com.amgureeva.Entities.Diagnosis.Alveokokkoz;

import java.util.List;

/**
 * Created by Александра on 04.05.2016.
 */
public interface AlveokokkozDAO {
    Alveokokkoz findById(Integer id, boolean isFirstDiagnosis);

    List<Alveokokkoz> findAll();

    void save(Alveokokkoz patient);

    void update(Alveokokkoz patient);

    void delete(Integer id);
}
