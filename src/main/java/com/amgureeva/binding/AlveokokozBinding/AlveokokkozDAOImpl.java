package com.amgureeva.binding.AlveokokozBinding;

import com.amgureeva.Entities.Diagnosis.Alveokokkoz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 04.05.2016.
 */
@Repository
public class AlveokokkozDAOImpl implements AlveokokkozDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    
    @Override
    public Alveokokkoz findById(Integer id, boolean isFirstDiagnosis) {
        if (id != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
            params.put("admissiondiag", (isFirstDiagnosis)?1:0);

            String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=7" +
                    " AND admissiondiag=:admissiondiag";

            List<List<Alveokokkoz>> result = null;
            try {
                result = namedParameterJdbcTemplate
                        .query(sql, params, new AlveokokkozMapper());
            } catch (EmptyResultDataAccessException e) {
                // do nothing, return null
            }
            result.remove(null);
            if (result == null || result.size() == 0)
                return null;
            if (isFirstDiagnosis) {
                if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
                if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
            }
            if (!isFirstDiagnosis) {
                if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
                if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
            }
        }
        return null;
    }

    @Override
    public List<Alveokokkoz> findAll() {
        String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 7";
        List<List<Alveokokkoz>> result = namedParameterJdbcTemplate.query(sql, new AlveokokkozMapper());
        result.remove(null);
        return (result == null || result.size() == 0)?null :result.get(0);
    }

    @Override
    public void save(Alveokokkoz alveokokkoz) {
        String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
                + "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),2, String.valueOf(alveokokkoz.getIcg()), alveokokkoz.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),3, String.valueOf(alveokokkoz.getIcgk_f()), alveokokkoz.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),69, String.valueOf(alveokokkoz.getPredOperacii()), alveokokkoz.isDiagnosisAtStart())); //bismuth
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),6, String.valueOf(alveokokkoz.getVorotInvasia()), alveokokkoz.isDiagnosisAtStart())); //vorot invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),7, String.valueOf(alveokokkoz.getPechInvasia()), alveokokkoz.isDiagnosisAtStart())); //pechenochnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),10, alveokokkoz.getPolozhKultura(), alveokokkoz.isDiagnosisAtStart())); //polozhitelnaya kultura
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),11, String.valueOf(alveokokkoz.getTrofStatus()), alveokokkoz.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),1, String.valueOf(alveokokkoz.getGrwr()), alveokokkoz.isDiagnosisAtStart())); //grwr
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),27, String.valueOf(alveokokkoz.getIndexZachvata()), alveokokkoz.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),28, String.valueOf(alveokokkoz.getObjemOstatka()), alveokokkoz.isDiagnosisAtStart())); //objem
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),67, alveokokkoz.getAlbendazol(), alveokokkoz.isDiagnosisAtStart())); //objem
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),26, String.valueOf(alveokokkoz.getRezekciaType()), alveokokkoz.isDiagnosisAtStart())); //objem
    }

    @Override
    public void update(Alveokokkoz alveokokkoz) {
        String sql = "UPDATE patientparametersvalue SET value=:value"
                + " WHERE patientId=:patientId and diseaseId=7 and parameterId=:parameterId and admissiondiag=:admissiondiag";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),2, String.valueOf(alveokokkoz.getIcg()), alveokokkoz.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),3, String.valueOf(alveokokkoz.getIcgk_f()), alveokokkoz.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),69, String.valueOf(alveokokkoz.getPredOperacii()), alveokokkoz.isDiagnosisAtStart())); //bismuth
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),6, String.valueOf(alveokokkoz.getVorotInvasia()), alveokokkoz.isDiagnosisAtStart())); //vorot invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),7, String.valueOf(alveokokkoz.getPechInvasia()), alveokokkoz.isDiagnosisAtStart())); //pechenochnaya invasia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),10, alveokokkoz.getPolozhKultura(), alveokokkoz.isDiagnosisAtStart())); //polozhitelnaya kultura
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),11, String.valueOf(alveokokkoz.getTrofStatus()), alveokokkoz.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),1, String.valueOf(alveokokkoz.getGrwr()), alveokokkoz.isDiagnosisAtStart())); //grwr
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),27, String.valueOf(alveokokkoz.getIndexZachvata()), alveokokkoz.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),28, String.valueOf(alveokokkoz.getObjemOstatka()), alveokokkoz.isDiagnosisAtStart())); //objem
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),67, alveokokkoz.getAlbendazol(), alveokokkoz.isDiagnosisAtStart())); //objem
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (alveokokkoz.getId(),26, String.valueOf(alveokokkoz.getRezekciaType()), alveokokkoz.isDiagnosisAtStart())); //objem

    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=7";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }
    private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value,
                                                      boolean admissiondiag) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("patientId", patientId);
        paramSource.addValue("parameterId", fieldId);
        paramSource.addValue("diseaseId", 7);
        paramSource.addValue("value", value);
        paramSource.addValue("admissiondiag", admissiondiag);
        return paramSource;
    }


    private static final class AlveokokkozMapper implements RowMapper<List<Alveokokkoz>> {

        public List<Alveokokkoz> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<Alveokokkoz> alList = new ArrayList<Alveokokkoz>();
            Alveokokkoz alveokokkoz = new Alveokokkoz();
            int i = 0;
            do {
                alveokokkoz.setId(rs.getString("patientId"));
                alveokokkoz.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
                int parameterId = rs.getInt("parameterId");
                switch(parameterId) {
                    case 1: if (i!=0 && i!=12) return null;i=1; alveokokkoz.setGrwr(rs.getInt("value")); break;
                    case 2: i++; alveokokkoz.setIcg(rs.getInt("value"));break;
                    case 3: i++; alveokokkoz.setIcgk_f(rs.getInt("value"));break;
                    case 6: i++; alveokokkoz.setVorotInvasia(rs.getInt("value"));break;
                    case 7: i++; alveokokkoz.setPechInvasia(rs.getInt("value"));break;
                    case 10: i++; alveokokkoz.setPolozhKultura(rs.getString("value"));break;
                    case 11: i++; alveokokkoz.setTrofStatus(rs.getInt("value")); break;
                    case 27: i++; alveokokkoz.setIndexZachvata(rs.getString("value")); break;
                    case 28: i++; alveokokkoz.setObjemOstatka(rs.getString("value")); break;
                    case 26: i++; alveokokkoz.setRezekciaType(rs.getInt("value")); break;
                    case 69: i++; alveokokkoz.setPredOperacii(rs.getInt("value")); break;
                    case 67: i++; alveokokkoz.setAlbendazol(rs.getString("value"));break;
                }
                if (i == 12) {
                    alList.add(alveokokkoz);
                    alveokokkoz = new Alveokokkoz();
                }
            }while (rs.next());
            return alList;
        }
    }
}

