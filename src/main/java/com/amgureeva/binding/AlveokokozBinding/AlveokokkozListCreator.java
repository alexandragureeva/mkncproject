package com.amgureeva.binding.AlveokokozBinding;

import com.amgureeva.Entities.Diagnosis.Alveokokkoz;
import com.amgureeva.Entities.Diagnosis.AlveokokkozList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 05.05.2016.
 */
public class AlveokokkozListCreator {

	public List<AlveokokkozList> createAlveokokkozList(List<Alveokokkoz> alveokokkozs, PatientService patientService) {
		if (alveokokkozs != null && patientService != null) {
			List<AlveokokkozList> alveokokkozList = new ArrayList<AlveokokkozList>();
			AlveokokkozList alveokokkozList_ = new AlveokokkozList();
			for (int i = 0; i < alveokokkozs.size(); i++) {
				alveokokkozList_.setName(patientService.findPatientById(Integer.valueOf(alveokokkozs.get(i).getId())).getName());

				switch (Integer.valueOf(alveokokkozs.get(i).getPredOperacii())) {
					case 0:
						alveokokkozList_.setPredOperacii("--");
						break;
					case 1:
						alveokokkozList_.setPredOperacii("нет");
						break;
					case 2:
						alveokokkozList_.setPredOperacii("Резекция печени");
						break;
					case 3:
						alveokokkozList_.setPredOperacii("Диагностическая лапаротомия");
						break;
					case 4:
						alveokokkozList_.setPredOperacii("Другие операции");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getGrwr())) {
					case 0:
						alveokokkozList_.setGrwr("--");
						break;
					case 1:
						alveokokkozList_.setGrwr("<0.8");
						break;
					case 2:
						alveokokkozList_.setGrwr("0.8-1");
						break;
					case 3:
						alveokokkozList_.setGrwr(">1");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getIcg())) {
					case 0:
						alveokokkozList_.setIcg("--");
						break;
					case 1:
						alveokokkozList_.setIcg("<15%");
						break;
					case 2:
						alveokokkozList_.setIcg("15-25%");
						break;
					case 3:
						alveokokkozList_.setIcg(">25%");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getIcgk_f())) {
					case 0:
						alveokokkozList_.setIcgk_f("--");
						break;
					case 1:
						alveokokkozList_.setIcgk_f("<0.4");
						break;
					case 2:
						alveokokkozList_.setIcgk_f("0.4-0.6");
						break;
					case 3:
						alveokokkozList_.setIcgk_f(">0.6");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getPechInvasia())) {
					case 0:
						alveokokkozList_.setPechInvasia("--");
						break;
					case 1:
						alveokokkozList_.setPechInvasia("Ипсилатеральную");
						break;
					case 2:
						alveokokkozList_.setPechInvasia("Контрлатеральную");
						break;
					case 3:
						alveokokkozList_.setPechInvasia("В собственную печеночную");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getVorotInvasia())) {
					case 0:
						alveokokkozList_.setVorotInvasia("--");
						break;
					case 1:
						alveokokkozList_.setVorotInvasia("В ствол");
						break;
					case 2:
						alveokokkozList_.setVorotInvasia("В долевые ветви");
						break;
					case 3:
						alveokokkozList_.setVorotInvasia("В сегментарные ветви");
						break;
				}
				switch (Integer.valueOf(alveokokkozs.get(i).getTrofStatus())) {
					case 0:
						alveokokkozList_.setTrofStatus("--");
						break;
					case 1:
						alveokokkozList_.setTrofStatus("Хороший");
						break;
					case 2:
						alveokokkozList_.setTrofStatus("Удовлетворительный");
						break;
					case 3:
						alveokokkozList_.setTrofStatus("Неудовлетворительный");
						break;
				}
				if (alveokokkozs.get(i).getIndexZachvata().equals("null"))
					alveokokkozList_.setIndexZachvata("--");
				else
					alveokokkozList_.setIndexZachvata(alveokokkozs.get(i).getIndexZachvata());
				if (alveokokkozs.get(i).getObjemOstatka().equals("null"))
					alveokokkozList_.setObjemOstatka("--");
				else
					alveokokkozList_.setObjemOstatka(alveokokkozs.get(i).getObjemOstatka());
				if (alveokokkozs.get(i).getPolozhKultura()==null || alveokokkozs.get(i).getPolozhKultura().equals(""))
					alveokokkozList_.setPolozhKultura("--");
				else if (alveokokkozs.get(i).getPolozhKultura().equals("0"))
					alveokokkozList_.setPolozhKultura("-");
				else
					alveokokkozList_.setPolozhKultura("+");
				if (alveokokkozs.get(i).getAlbendazol()==null || alveokokkozs.get(i).getAlbendazol().equals(""))
					alveokokkozList_.setAlbendazol("--");
				else if (alveokokkozs.get(i).getAlbendazol().equals("0"))
					alveokokkozList_.setAlbendazol("-");
				else
					alveokokkozList_.setAlbendazol("+");
				alveokokkozList.add(alveokokkozList_);
				alveokokkozList_ = new AlveokokkozList();
			}

			return alveokokkozList;
		}
		return null;
	}

	public static String getAllVariants(String s) {
		s = s.replace("_ПОСТ", "");
		s = s.replace("_ВЫПИС", "");
		s = s.replace("_ОПЕР", "");
		if (s.equals("alv:ICG")) {
			return "1 - \"<15%\"; 2 - \"15-25%\"; 3 - \">25%\"";
		}
		if (s.equals("alv:ICGK-F")) {
			return "1 - \"<0.4\"; 2 - \"0.4-0.6\"; 3 -\">0.6\"";
		}
		if (s.equals("alv:INVASIA_VOROTNAYA")) {
			return "1 - \"В ствол\"; 2 - \"В долевые ветви\"; 3 -\"В сегментарные ветви\"";
		}
		if (s.equals("alv:INVASIYA_PECHENOCHNAYA")) {
			return "1 - \"Ипсилатеральную\"; 2 - \"Контрлатеральную\"; 3 -\"В собственную печеночную\"";
		}
		if (s.equals("alv:OCENKA_TROF_STATUSA")) {
			return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 -\"Неудовлетворительный\"";
		}
		if (s.equals("alv:PRED_OPERACII")) {
			return "1 - \"нет\"; 2 - \"Резекция печени\"; 3 -\"Диагностическая лапаротомия\"; " +
					"4 -\"Другие операции\"";
		}
		if (s.equals("alv:GRWR")) {
			return "1 - \"<0.8\"; 2 - \"0.8-1\"; 3 -\">1\"";
		}
		if (s.equals("alv:SOSUD_REZEKCIA")) {
			return "1 - \"Резекция воротной вены\"; 2 - \"Резекция печеночной артерии\"";
		}
		if (s.equals("alv:REZEKCIA")) {
			return "1 - \"Левосторонняя гемигепатэктомия (ЛГГ)\"; 2 - \"Правосторонняя гемигепатэктомия (ПГГ)\";" +
					" 3 - \"Расширенная ПГГ\"; 4 - \"Расширенная ЛГГ\"; 5 - \"Секторэктомия\"";
		}
		if (s.equals("alv:VOROT_REZEKCIA")) {
			return "1 - \"Краевая\"; 2 - \"Циркулярная без протезирования\"; 3 - \"Циркулярная с протезированием\"";
		}
		if (s.equals("alv:KROVOPOTERYA")) {
			return "1 - \"<500\"; 2 - \"500-1000\"; 3 - \">1000\"";
		}
		if (s.equals("alv:GEATYPE1")) {
			return "1 - \"Единый\"; 2 - \"Раздельный\"";
		}
		if (s.equals("alv:GEATYPE2")) {
			return "1 - \"Моно-ГЕА\"; 2 - \"Би-ГЕА\"; 3 - \"Три-ГЕА\"; 4 - \"Тетра-ГЕА\";" +
					" 5 - \"Пента-ГЕА\"; 6 - \"Мульти-ГЕА\"";
		}
		if (s.equals("alv:GEATYPE3")) {
			return "1 - \"С ТПД\"; 2 - \"Без ТПД\"";
		}
		if (s.equals("alv:RADIKALNOST")) {
			return "1 - \"R0\"; 2 - \"R1\"; 3 - \"R2\"";
		}
		if (s.equals("alv:LIMFODISSEKCIA")) {
			return "1 - \"ГДС\"; 2 - \"ГДС+ОПА\"; 3 - \"ГДС+ОПА+ЧС\"";
		}
		if (s.equals("alv:TNM")) {
			return "1 - \"T1N0M0\"; 2 - \"T2aN0M0\"; 3 - \"T2bN0M0\"; 4 - \"T3N0M0\"; 5 - \"T1N1M0\"; 6 - \"T2N1M0\";" +
					"7 - \"T3N1M0\"; 8 - \"T4N0M0\"; 9 - \"T4N1M0\"; 10 - \"Любая T, N2M0\"; 11 - \"Любая T, любая N, M1\"";
		}
		if (s.equals("alv:PNM")) {
			return "1 - \"Стадия I\"; 2 - \"Стадия II\"; 3 - \"Стадия III\"; 4 - \"Стадия IV\"";
		}
		if (s.equals("alv:OSLOZH")) {
			return "1 - \"Внутрибрюшное кровотечение\"; 2 - \"ЖКК\"; 3 - \"Печеночная недостаточность A\";" +
					" 4 - \"Печеночная недостаточность B\"; 5 - \"Печеночная недостаточность C\";" +
					" 6 - \"Механическая желтуха\"; 7 - \"Нагноение раны\"; 8 - \"Холангит\";" +
					" 9 - \"Холангиогенное абсцедирование\"; 10 - \"Асцит\"; 11 - \"Эвентрация\"" +
					"; 12 - \"Кишечная непроходимость\"; 13 - \"Панкреатический свищ\"; 14 - \"Острый панкреатит\";" +
					" 15 - \"Сепсис\"; 16 - \"Тяжелый сепсис\"; 17 - \"Септический шок\"; 18 - \"Желчный свищ\"" +
					"; 19 - \"Кишечный свищ\"; 20 - \"Тромбоз воротной вены\"; 21 - \"Тромбоз печеночной артерии\";" +
					" 22 - \"Односторонняя пневмония\"" +
					"; 23 - \"Двусторонняя пневмония\"; 24 - \"Гидроторакс\"; 25 - \"Почечная недостаточность\"" +
					"; 26 - \"Сердечно-сосудистая недостаточность\"; 27 - \"Полиорганная недостаточность\"";
		}
		if (s.equals("alv:CLAVIEN")) {
			return "1 - \"I\"; 2 - \"II\"; 3 - \"IIIa\"; 4 - \"IIIb\"; 5 - \"IVa\"; 6 - \"IVb\"; 7 - \"V\"";
		}

		if (s.equals("alv:SCINTIGRAF_OBJ_OPERAC")) {
			return "1 - \"Сегментарная\"; 2 - \"Обширная\"";
		}
		return "";
	}
}
