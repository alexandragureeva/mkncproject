package com.amgureeva.binding.GCRBinding;

import com.amgureeva.Entities.Diagnosis.GCR;

import java.util.List;

/**
 * Created by Александра on 28.04.2016.
 */
public interface GCRDAO {
    GCR findById(Integer id, boolean isFirstDiagnosis);

    List<GCR> findAll();

    void save(GCR patient);

    void update(GCR patient);

    void delete(Integer id);
}
