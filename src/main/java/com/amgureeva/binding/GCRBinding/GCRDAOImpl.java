package com.amgureeva.binding.GCRBinding;

import com.amgureeva.Entities.Diagnosis.GCR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Александра on 28.04.2016.
 */
@Repository
public class GCRDAOImpl implements GCRDAO {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(
            NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    @Override
    public GCR findById(Integer id, boolean isFirstDiagnosis) {
        if (id != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
			params.put("admissiondiag", (isFirstDiagnosis)?1:0);

			String sql = "SELECT * FROM patientparametersvalue WHERE patientId=:id and diseaseId=1" +
					" AND admissiondiag=:admissiondiag";

            List<List<GCR>> result = null;
            try {
                result = namedParameterJdbcTemplate
                        .query(sql, params, new GCRMapper());
            } catch (EmptyResultDataAccessException e) {
                // do nothing, return null
            }
            result.remove(null);
            if (result == null || result.size() == 0)
                return null;
            if (isFirstDiagnosis) {
                if (result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
                if (result.size() == 2 && result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
            }
            if (!isFirstDiagnosis) {
                if (!result.get(0).get(0).isDiagnosisAtStart()) return result.get(0).get(0);
                if (result.size() == 2 && !result.get(1).get(0).isDiagnosisAtStart())  return result.get(1).get(0);
            }
        }
        return null;
    }

    @Override
    public List<GCR> findAll() {
        String sql = "SELECT patientId, parameterId, value, admissiondiag FROM patientparametersvalue WHERE diseaseId = 1";
        List<List<GCR>> result = namedParameterJdbcTemplate.query(sql, new GCRMapper());
        return (result == null || result.size() == 0)?null :result.get(0);
    }

    @Override
    public void save(GCR gcr) {
        String sql = "INSERT INTO patientparametersvalue(patientId, parameterId, value, diseaseId, admissiondiag) "
                + "VALUES ( :patientId, :parameterId, :value, :diseaseId, :admissiondiag)";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),2, String.valueOf(gcr.getIcg()), gcr.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),3, String.valueOf(gcr.getIcgk_f()), gcr.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),18, String.valueOf(gcr.getRazmerOpucholi()), gcr.isDiagnosisAtStart())); //razmer opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),20, String.valueOf(gcr.getChisloOpuchUzlov()), gcr.isDiagnosisAtStart())); //chislo opucholevich uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),19, String.valueOf(gcr.getOpuchUzliType()), gcr.isDiagnosisAtStart())); //tip opucholev uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),22, String.valueOf(gcr.getSosudInvasia()), gcr.isDiagnosisAtStart())); //tip sosudistoi invasii
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),24, String.valueOf(gcr.getVnepOchagiType()), gcr.isDiagnosisAtStart())); //tip vnepech oxhagov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),25, String.valueOf((gcr.getTrombPechType())), gcr.isDiagnosisAtStart())); //tromboz pechenochnich ven
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),11, String.valueOf(gcr.getTrofStatus()), gcr.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),29, String.valueOf((gcr.isProtivTerapia())?1:0), gcr.isDiagnosisAtStart())); //is protivovirusnaya terapiya
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),30, String.valueOf(gcr.getTrombozVorotVeni()), gcr.isDiagnosisAtStart())); //tromboz vorotnoi veni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),31, String.valueOf((gcr.isLechenieSorafinib())?1:0), gcr.isDiagnosisAtStart())); //lecenie sorafenibom
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),27, String.valueOf(gcr.getIndexZachvata()), gcr.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),28, String.valueOf(gcr.getObjemOstatka()), gcr.isDiagnosisAtStart())); //objem funkc doli pecheni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),47, String.valueOf(gcr.getCirrozClass()), gcr.isDiagnosisAtStart())); //cirroz
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),48, String.valueOf(gcr.getVrvStepen()), gcr.isDiagnosisAtStart())); //vrv
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),50, String.valueOf(gcr.getTrombozStepen()), gcr.isDiagnosisAtStart())); //trombozitopenia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),12, String.valueOf(gcr.getEtiologiaCirroza()), gcr.isDiagnosisAtStart())); //etiologia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),14, String.valueOf(gcr.getGepatitStepen()), gcr.isDiagnosisAtStart()));//stepen gepatita
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),17, String.valueOf(gcr.getFibrozClass()), gcr.isDiagnosisAtStart())); //fibroz
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),32, String.valueOf((gcr.isTache())?1:0), gcr.isDiagnosisAtStart())); //tache
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),33, String.valueOf(gcr.getBclcType()), gcr.isDiagnosisAtStart())); //bclc
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),26, String.valueOf(gcr.getRezekciaType()), gcr.isDiagnosisAtStart())); //objem

    }

    @Override
    public void update(GCR gcr) {
        String sql = "UPDATE patientparametersvalue SET value=:value"
                + " WHERE patientId=:patientId and diseaseId=1 and parameterId=:parameterId" +
                " and admissiondiag=:admissiondiag";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),2, String.valueOf(gcr.getIcg()), gcr.isDiagnosisAtStart())); //icg
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),3, String.valueOf(gcr.getIcgk_f()), gcr.isDiagnosisAtStart())); //icgk-f
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),18, String.valueOf(gcr.getRazmerOpucholi()), gcr.isDiagnosisAtStart())); //razmer opucholi
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),20, String.valueOf(gcr.getChisloOpuchUzlov()), gcr.isDiagnosisAtStart())); //chislo opucholevich uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),19, String.valueOf(gcr.getOpuchUzliType()), gcr.isDiagnosisAtStart())); //tip opucholev uzlov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),22, String.valueOf(gcr.getSosudInvasia()), gcr.isDiagnosisAtStart())); //tip sosudistoi invasii
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),24, String.valueOf(gcr.getVnepOchagiType()), gcr.isDiagnosisAtStart())); //tip vnepech oxhagov
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),25, String.valueOf((gcr.getTrombPechType())), gcr.isDiagnosisAtStart())); //tromboz pechenochnich ven
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),11, String.valueOf(gcr.getTrofStatus()), gcr.isDiagnosisAtStart())); //ocenka troficheskogo statusa
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),29, String.valueOf((gcr.isProtivTerapia())?1:0), gcr.isDiagnosisAtStart())); //is protivovirusnaya terapiya
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),30, String.valueOf(gcr.getTrombozVorotVeni()), gcr.isDiagnosisAtStart())); //tromboz vorotnoi veni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),31, String.valueOf((gcr.isLechenieSorafinib())?1:0), gcr.isDiagnosisAtStart())); //lecenie sorafenibom
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),27, String.valueOf(gcr.getIndexZachvata()), gcr.isDiagnosisAtStart())); //index zachvata
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),28, String.valueOf(gcr.getObjemOstatka()), gcr.isDiagnosisAtStart())); //objem funkc doli pecheni
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),47, String.valueOf(gcr.getCirrozClass()), gcr.isDiagnosisAtStart())); //cirroz
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),48, String.valueOf(gcr.getVrvStepen()), gcr.isDiagnosisAtStart())); //vrv
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),50, String.valueOf(gcr.getTrombozStepen()), gcr.isDiagnosisAtStart())); //trombozitopenia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),12, String.valueOf(gcr.getEtiologiaCirroza()), gcr.isDiagnosisAtStart())); //etiologia
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),14, String.valueOf(gcr.getGepatitStepen()), gcr.isDiagnosisAtStart()));//stepen gepatita
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),17, String.valueOf(gcr.getFibrozClass()), gcr.isDiagnosisAtStart())); //fibroz
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),32, String.valueOf((gcr.isTache())?1:0), gcr.isDiagnosisAtStart())); //tache
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),33, String.valueOf(gcr.getBclcType()), gcr.isDiagnosisAtStart())); //bclc
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel
                (gcr.getId(),26, String.valueOf(gcr.getRezekciaType()), gcr.isDiagnosisAtStart())); //objem

    }

    @Override
    public void delete(Integer id) {
        String sql = "DELETE FROM patientparametersvalue WHERE patientId= :id and diseaseId=1";
        namedParameterJdbcTemplate.update(sql, new MapSqlParameterSource("id", id));
    }

    private SqlParameterSource getSqlParameterByModel(String patientId, int fieldId, String value,
                                                      boolean admissiondiag) {
        MapSqlParameterSource paramSource = new MapSqlParameterSource();
        paramSource.addValue("patientId", patientId);
        paramSource.addValue("parameterId", fieldId);
        paramSource.addValue("diseaseId", 1);
        paramSource.addValue("value", value);
        paramSource.addValue("admissiondiag", admissiondiag);
        return paramSource;
    }


    private static final class GCRMapper implements RowMapper<List<GCR>> {

        public List<GCR> mapRow(ResultSet rs, int rowNum) throws SQLException {
            List<GCR> gcrList = new ArrayList<GCR>();
            GCR gcr = new GCR();
            int i = 0;
            do {
                gcr.setId(rs.getString("patientId"));
                gcr.setDiagnosisAtStart(rs.getBoolean("admissiondiag"));
                int parameterId = rs.getInt("parameterId");
                switch(parameterId) {
                    case 2: if (i!=0 && i!=23) return null;i=1; gcr.setIcg(rs.getInt("value"));break;
                    case 3: i++; gcr.setIcgk_f(rs.getInt("value"));break;
                    case 11: i++; gcr.setTrofStatus(rs.getInt("value")); break;
                    case 12: i++; gcr.setEtiologiaCirroza(rs.getInt("value")); break;
                    case 14: i++; gcr.setGepatitStepen(rs.getInt("value")); break;
                    case 17: i++; gcr.setFibrozClass(rs.getInt("value")); break;
                    case 18: i++; gcr.setRazmerOpucholi(rs.getInt("value"));break;
                    case 19: i++; gcr.setOpuchUzliType(rs.getInt("value"));break;
                    case 20: i++; gcr.setChisloOpuchUzlov(rs.getInt("value"));break;
                    case 22: i++; gcr.setSosudInvasia(rs.getInt("value"));break;
                    case 24: i++; gcr.setVnepOchagiType(rs.getInt("value"));break;
                    case 25: i++; gcr.setTrombPechType(rs.getInt("value"));break;
                    case 27: i++; gcr.setIndexZachvata(rs.getString("value")); break;
                    case 28: i++; gcr.setObjemOstatka(rs.getString("value")); break;
                    case 29: i++; gcr.setProtivTerapia(rs.getString("value").equals("1")?true:false); break;
                    case 30: i++; gcr.setTrombozVorotVeni(rs.getInt("value")); break;
                    case 31: i++; gcr.setLechenieSorafinib((rs.getInt("value")==1)?true:false); break;
                    case 32: i++; gcr.setTache((rs.getInt("value")==1)?true:false); break;
                    case 33: i++; gcr.setBclcType(rs.getInt("value")); break;
                    case 47: i++; gcr.setCirrozClass(rs.getInt("value")); break;
                    case 48: i++; gcr.setVrvStepen(rs.getInt("value")); break;
                    case 50: i++; gcr.setTrombozStepen(rs.getInt("value")); break;
                    case 26: i++; gcr.setRezekciaType(rs.getInt("value")); break;
                }
                if ( i == 23) {
                    gcrList.add(gcr);
                    gcr = new GCR();
                }
            }while (rs.next());
            return gcrList;
        }
    }
}
