package com.amgureeva.binding.GCRBinding;

import com.amgureeva.Entities.Diagnosis.GCR;
import com.amgureeva.Entities.Diagnosis.GCRList;
import com.amgureeva.binding.PatientBinding.PatientService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Александра on 05.05.2016.
 */
public class GCRListCreator {
    public List<GCRList> createGCRList (List<GCR> gcrs, PatientService patientService) {
        if (gcrs != null && patientService != null) {
            List<GCRList> gcrList = new ArrayList<GCRList>();
            GCRList gcrList_ = new GCRList();
            for (int i = 0; i < gcrs.size(); i++) {
                gcrList_.setName(patientService.findPatientById(Integer.valueOf(gcrs.get(i).getId())).getName());
                switch (Integer.valueOf(gcrs.get(i).getCirrozClass())) {
                    case 0:
                        gcrList_.setCirrozClass("--");
                        break;
                    case 1:
                        gcrList_.setCirrozClass("нет");
                        break;
                    case 2:
                        gcrList_.setCirrozClass("А");
                        break;
                    case 3:
                        gcrList_.setCirrozClass("В");
                        break;
                    case 4:
                        gcrList_.setCirrozClass("С");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getVrvStepen())) {
                    case 0:
                        gcrList_.setVrvStepen("--");
                        break;
                    case 1:
                        gcrList_.setVrvStepen("0");
                        break;
                    case 2:
                        gcrList_.setVrvStepen("1");
                        break;
                    case 3:
                        gcrList_.setVrvStepen("2");
                        break;
                    case 4:
                        gcrList_.setVrvStepen("3");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getTrombozStepen())) {
                    case 0:
                        gcrList_.setTrombozStepen("--");
                        break;
                    case 1:
                        gcrList_.setTrombozStepen("нет");
                        break;
                    case 2:
                        gcrList_.setTrombozStepen("Легкая");
                        break;
                    case 3:
                        gcrList_.setTrombozStepen("Средняя");
                        break;
                    case 4:
                        gcrList_.setTrombozStepen("Тяжелая");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getGepatitStepen())) {
                    case 0:
                        gcrList_.setGepatitStepen("--");
                        break;
                    case 1:
                        gcrList_.setGepatitStepen("нет");
                        break;
                    case 2:
                        gcrList_.setGepatitStepen("Гепатит В");
                        break;
                    case 3:
                        gcrList_.setGepatitStepen("Гепатит С");
                        break;
                    case 4:
                        gcrList_.setGepatitStepen("Гeпатит В+С");
                        break;
                    case 5:
                        gcrList_.setGepatitStepen("Гепатит B+D");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getEtiologiaCirroza())) {
                    case 0:
                        gcrList_.setEtiologiaCirroza("--");
                        break;
                    case 1:
                        gcrList_.setEtiologiaCirroza("Гепатит В");
                        break;
                    case 2:
                        gcrList_.setEtiologiaCirroza("Гепатит С");
                        break;
                    case 3:
                        gcrList_.setEtiologiaCirroza("Алкогольный");
                        break;
                    case 4:
                        gcrList_.setEtiologiaCirroza("Другой этиологии");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getFibrozClass())) {
                    case 0:
                        gcrList_.setFibrozClass("--");
                        break;
                    case 1:
                        gcrList_.setFibrozClass("нет");
                        break;
                    case 2:
                        gcrList_.setFibrozClass("f0");
                        break;
                    case 3:
                        gcrList_.setFibrozClass("f1");
                        break;
                    case 4:
                        gcrList_.setFibrozClass("f2");
                        break;
                    case 5:
                        gcrList_.setFibrozClass("f3");
                        break;
                    case 6:
                        gcrList_.setFibrozClass("f4");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getRazmerOpucholi())) {
                    case 0:
                        gcrList_.setRazmerOpucholi("--");
                        break;
                    case 1:
                        gcrList_.setRazmerOpucholi("<10см");
                        break;
                    case 2:
                        gcrList_.setRazmerOpucholi(">10см");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getOpuchUzliType())) {
                    case 0:
                        gcrList_.setOpuchUzliType("--");
                        break;
                    case 1:
                        gcrList_.setOpuchUzliType("Солитарный");
                        break;
                    case 2:
                        gcrList_.setOpuchUzliType("Множественные");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getChisloOpuchUzlov())) {
                    case 0:
                        gcrList_.setChisloOpuchUzlov("--");
                        break;
                    case 1:
                        gcrList_.setChisloOpuchUzlov("0");
                        break;
                    case 2:
                        gcrList_.setChisloOpuchUzlov("1");
                        break;
                    case 3:
                        gcrList_.setChisloOpuchUzlov("2");
                        break;
                    case 4:
                        gcrList_.setChisloOpuchUzlov("3");
                        break;
                    case 5:
                        gcrList_.setChisloOpuchUzlov("4");
                        break;
                    case 6:
                        gcrList_.setChisloOpuchUzlov("5");
                        break;
                    case 7:
                        gcrList_.setChisloOpuchUzlov(">5");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getSosudInvasia())) {
                    case 0:
                        gcrList_.setSosudInvasia("--");
                        break;
                    case 1:
                        gcrList_.setSosudInvasia("нет");
                        break;

                    case 2:
                        gcrList_.setSosudInvasia("Микрососудистая");
                        break;
                    case 3:
                        gcrList_.setSosudInvasia("Мaкрососудистая");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getIcg())) {
                    case 0:
                        gcrList_.setIcg("--");
                        break;
                    case 1:
                        gcrList_.setIcg("<15%");
                        break;
                    case 2:
                        gcrList_.setIcg("15-25%");
                        break;
                    case 3:
                        gcrList_.setIcg(">25%");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getIcgk_f())) {
                    case 0:
                        gcrList_.setIcgk_f("--");
                        break;
                    case 1:
                        gcrList_.setIcgk_f("<0.4");
                        break;
                    case 2:
                        gcrList_.setIcgk_f("0.4-0.6");
                        break;
                    case 3:
                        gcrList_.setIcgk_f(">0.6");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getVnepOchagiType())) {
                    case 0:
                        gcrList_.setVnepOchagiType("--");
                        break;
                    case 1:
                        gcrList_.setVnepOchagiType("нет");
                        break;
                    case 2:
                        gcrList_.setVnepOchagiType("Отдаленные метастазы");
                        break;
                    case 3:
                        gcrList_.setVnepOchagiType("Прорастание соседних органов");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getTrombPechType())) {
                    case 0:
                        gcrList_.setTrombPechType("--");
                        break;
                    case 1:
                        gcrList_.setTrombPechType("Нет");
                        break;
                    case 2:
                        gcrList_.setTrombPechType("Да");
                        break;
                    case 3:
                        gcrList_.setTrombPechType("С выходом в нижнюю полую вену");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getTrombozVorotVeni())) {
                    case 0:
                        gcrList_.setTrombozVorotVeni("--");
                        break;
                    case 1:
                        gcrList_.setTrombozVorotVeni("0");
                        break;
                    case 2:
                        gcrList_.setTrombozVorotVeni("1");
                        break;
                    case 3:
                        gcrList_.setTrombozVorotVeni("2");
                        break;
                    case 4:
                        gcrList_.setTrombozVorotVeni("3");
                        break;
                    case 5:
                        gcrList_.setTrombozVorotVeni("4");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getTrofStatus())) {
                    case 0:
                        gcrList_.setTrofStatus("--");
                        break;
                    case 1:
                        gcrList_.setTrofStatus("Хороший");
                        break;
                    case 2:
                        gcrList_.setTrofStatus("Удовлетворительный");
                        break;
                    case 3:
                        gcrList_.setTrofStatus("Неудовлетворительный");
                        break;
                }
                switch (Integer.valueOf(gcrs.get(i).getBclcType())) {
                    case 0:
                        gcrList_.setBclcType("--");
                        break;
                    case 1:
                        gcrList_.setBclcType("A");
                        break;
                    case 2:
                        gcrList_.setBclcType("B");
                        break;
                    case 3:
                        gcrList_.setBclcType("C");
                        break;
                }
                if (gcrs.get(i).getIndexZachvata().equals("null"))
                    gcrList_.setIndexZachvata("--");
                else
                    gcrList_.setIndexZachvata(gcrs.get(i).getIndexZachvata());
                if (gcrs.get(i).getObjemOstatka().equals("null"))
                    gcrList_.setObjemOstatka("--");
                else
                    gcrList_.setObjemOstatka(gcrs.get(i).getObjemOstatka());
                if (gcrs.get(i).isProtivTerapia() == false)
                    gcrList_.setProtivTerapia("-");
                else
                    gcrList_.setProtivTerapia("+");
                if (gcrs.get(i).isLechenieSorafinib() == false)
                    gcrList_.setLechenieSorafinib("-");
                else
                    gcrList_.setLechenieSorafinib("+");
                if (gcrs.get(i).isTache() == false)
                    gcrList_.setTache("-");
                else
                    gcrList_.setTache("+");
                gcrList.add(gcrList_);
                gcrList_ = new GCRList();
            }

            return gcrList;
        }
        return null;
    }

    public static String getAllVariants(String s) {
        s = s.replace("_ПОСТ", "");
        s = s.replace("_ВЫПИС", "");
        s = s.replace("_ОПЕР", "");
        if (s.equals("gcr:ICG")) {
            return "1 - \"<15%\"; 2 - \"15-25%\"; 3 - \">25%\"";
        }
        if (s.equals("gcr:ICGK-F")) {
            return "1 - \"<0.4\"; 2 - \"0.4-0.6\"; 3 -\">0.6\"";
        }
        if (s.equals("gcr:INVASIA_VOROTNAYA")) {
            return "1 - \"В ствол\"; 2 - \"В долевые ветви\"; 3 -\"В сегментарные ветви\"";
        }
        if (s.equals("gcr:SOSUD_INVASIA")) {
            return "1 - \"Нет\"; 2 - \"Микрососудистая инвазия\"; 3 -\"Макрососудистая инвазия\"";
        }
        if (s.equals("gcr:INVASIYA_PECHENOCHNAYA")) {
            return "1 - \"Ипсилатеральную\"; 2 - \"Контрлатеральную\"; 3 -\"В собственную печеночную\"";
        }
        if (s.equals("gcr:OCENKA_TROF_STATUSA")) {
            return "1 - \"Хороший\"; 2 - \"Удовлетворительный\"; 3 -\"Неудовлетворительный\"";
        }
        if (s.equals("gcr:BCLC")) {
            return "1 - \"A\"; 2 - \"B\"; 3 -\"C\"";
        }
        if (s.equals("gcr:BCLC")) {
            return "1 - \"A\"; 2 - \"B\"; 3 -\"C\"";
        }
        if (s.equals("gcr:CIRROZ_KLASS")) {
            return "1 - \"нет\"; 2 - \"A\"; 3 -\"B\"; 4 - \"C\"";
        }
        if (s.equals("gcr:ETIOLOGIA_CIRROZA")) {
            return "1 - \"Гепатит В\"; 2 - \"Гепатит С\"; 3 -\"Алкогольный\"; 4 - \"Другой этиологии\"";
        }
        if (s.equals("gcr:FIBROZ_CL")) {
            return "1 - \"нет\"; 2 - \"f0\"; 3 -\"f1\"; 4 - \"f2\"; 5 - \"f3\"; 6 - \"f4\"";
        }
        if (s.equals("gcr:GEPATIT_TYPE")) {
            return "1 - \"Гепатит В\"; 2 - \"Гепатит С\"; 3 -\"Гeпатит В+С\"; 4 - \"Гепатит B+D\"";
        }
        if (s.equals("gcr:MAX_OPUCHOL")) {
            return "1 - \"<10см\"; 2 - \">10см\"";
        }
        if (s.equals("gcr:ONKOMARKER")) {
            return "1 - \"CA\"; 2 - \"19-9\"; 3 -\"CEA\"";
        }
        if (s.equals("gcr:TROMBOCITOPENIA")) {
            return "1 - \"нет\"; 2 - \"Легкая\"; 3 -\"Средняя\"; 4 -\"Тяжелая\"";
        }
        if (s.equals("gcr:TROMBOZ_PECHEN_VEN")) {
            return "1 - \"нет\"; 2 - \"Да\"; 3 -\"С выходом в нижнюю полую вену\"";
        }
        if (s.equals("gcr:TROMBOZ_VOROTNOI_VENI")) {
            return "1 - \"0\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"; 5 - \"4\"";
        }
        if (s.equals("gcr:VNEPECHENOCH_OCHAGI")) {
            return "1 - \"нет\"; 2 - \"Отдаленные метастазы\"; 3 -\"Прорастание соседних органов\"";
        }
        if (s.equals("gcr:OPUCH_UZLI_TYPE")) {
            return "1 - \"Солитарный\"; 2 - \"Множестевенные\"";
        }
        if (s.equals("gcr:VRV")) {
            return "1 - \"0\"; 2 - \"1\"; 3 -\"2\"; 4 - \"3\"";
        }
        if (s.equals("gcr:OPERATIV_LECHEN")) {
            return "1 - \"Резекция печени\"; 2 - \"Чрескожая РЧА (чРЧА)\"; 3 -\"Алкоголизация\"";
        }
        if (s.equals("gcr:OBJEM_REZEKCII")) {
            return "1 - \"Обширная резекция ЛГГЭ\"; 2 - \"Обширная резекция ПГГЭ\";" +
                    " 3 -\"Cегментарная резекция\"";
        }
        if (s.equals("gcr:SEGMENT_REZEKCIYA")) {
            return "1 - \"Анатомическая\"; 2 - \"Неанатомическая\"";
        }
        if (s.equals("gcr:CHISLO_OCHAGOV_RCHA")) {
            return "1 - \"1\"; 2 - \"2\"; 3 -\"3\"; 4 - \">3\"";
        }
        if (s.equals("gcr:POLNOTA_RCHA")) {
            return "1 - \"Полная\"; 2 - \"Неполная\"";
        }
        if (s.equals("gcr:CHISLO_OCHAGOV_ALK")) {
            return "1 - \"1\"; 2 - \"2\"; 3 -\"3\"; 4 - \">3\"";
        }
        if (s.equals("gcr:KROVOPOTERYA")) {
            return "1 - \"<500\"; 2 - \"500-1000\"; 3 - \">1000\"";
        }
        if (s.equals("gcr:TNM")) {
            return "1 - \"T1N0M0\"; 2 - \"T2aN0M0\"; 3 - \"T2bN0M0\"; 4 - \"T3N0M0\"; 5 - \"T1N1M0\"; 6 - \"T2N1M0\";" +
                    "7 - \"T3N1M0\"; 8 - \"T4N0M0\"; 9 - \"T4N1M0\"; 10 - \"Любая T, N2M0\"; 11 - \"Любая T, любая N, M1\"";
        }
        if (s.equals("gcr:OSL_REZEKCIA")) {
            return "1 - \"Внутрибрюшное кровотечение\"; 2 - \"ЖКК\";3 - \"Из ВРВ пищевода и желудка\";" +
                    " 4 - \"Печеночная недостаточность A\";" +
                    " 5 - \"Печеночная недостаточность B\"; 6 - \"Печеночная недостаточность C\";" +
                    " 7 - \"Механическая желтуха\"; 8 - \"Нагноение раны\"; 9 - \"Холангит\";" +
                    " 10 - \"Холангиогенное абсцедирование\"; 11 - \"Асцит\"; 12 - \"Эвентрация\"" +
                    "; 13 - \"Кишечная непроходимость\"; 14 - \"Панкреатический свищ\"; 15 - \"Острый панкреатит\";" +
                    " 16 - \"Сепсис\"; 17 - \"Тяжелый сепсис\"; 18 - \"Септический шок\"; 19 - \"Желчный свищ\"" +
                    "; 20 - \"Кишечный свищ\"; 21 - \"Тромбоз воротной вены\"; 22 - \"Тромбоз печеночной артерии\";" +
                    " 23 - \"Односторонняя пневмония\"" +
                    "; 24 - \"Двусторонняя пневмония\"; 25 - \"Гидроторакс\"; 26 - \"Почечная недостаточность\"" +
                    "; 27 - \"Сердечно-сосудистая недостаточность\"; 28 - \"Полиорганная недостаточность\"" +
                    "; 29 - \"Пострезекционная недостаточность A\"; 30 - \"Пострезекционная недостаточность B\";" +
                    " 31 - \"Пострезекционная недостаточность C\"";
        }
        if (s.equals("gcr:OSL_RCHA")) {
            return "1 - \"Постабляционный синдром\"; 2 - \"Билома\"; " +
                    "3 - \"Повреждения полых органов\"; 4 - \"Печеночная недостаточность\";" +
                    " 5 - \"Кровотечение в бр.полость(остановлено консервативно)\";" +
                    "6 - \"Кровотечение в бр.полость(потребовавшее лапроскопии(лапоротомии)\"";
        }
        if (s.equals("gcr:OSL_ALKOGOLIZACIA")) {
            return "1 - \"Печеночная недостаточность A\"; 2 - \"Печеночная недостаточность B\"; " +
                    "3 - \"Печеночная недостаточность C\"; 4 - \"Кровотечение в бр.полость(остановлено консервативно)\";" +
                    "5 - \"Кровотечение в бр.полость(потребовавшее лапроскопии(лапоротомии)\"";
        }
        if (s.equals("gcr:CLAVIEN")) {
            return "1 - \"I\"; 2 - \"II\"; 3 - \"IIIa\"; 4 - \"IIIb\"; 5 - \"IVa\"; 6 - \"IVb\"; 7 - \"V\"";
        }
        if (s.equals("gcr:SCINTIGRAF_OBJ_OPERAC")) {
            return "1 - \"Сегментарная\"; 2 - \"Обширная\"";
        }
        return "";
    }
}
