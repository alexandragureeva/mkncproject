package com.amgureeva.Services;

import com.amgureeva.Entities.DBProperties;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

//import jdk.nashorn.internal.runtime.Version;

/**
 * Servlet implementation class UploadServlet
 */
@Controller
public class PhotoUploadService {

    private static final String DATA_DIRECTORY = "resources/images";
    private static final int MAX_MEMORY_SIZE = 1024 * 1024 * 100;
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 100;
    private static final int MAX_PHOTO_SIZE = 1024 * 1024 * 10;
    private static final int IMG_WIDTH = 70;
    private static final int IMG_HEIGHT = 70;


    public String execute(HttpServletRequest request, ServletContext context, MultipartFile file, long user_id)  {

        // Check that we have a file upload request
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if (!isMultipart) {
            return "Вы не выбрали файл.";
        } else {
            // Create a factory for disk-based file items
            DiskFileItemFactory factory = new DiskFileItemFactory();

            // Sets the size threshold beyond which files are written directly to
            // disk.
            factory.setSizeThreshold(MAX_MEMORY_SIZE);

            // Sets the directory used to temporarily store files that are larger
            // than the configured size threshold. We use temporary directory for
            // java
            factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

            // constructs the folder where uploaded file will be stored
            String uploadFolder = context.getRealPath("")
                    + File.separator + DATA_DIRECTORY;

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            // Set overall request size constraint
            upload.setSizeMax(MAX_REQUEST_SIZE);
            try {
                String fileName=file.getOriginalFilename();
                if (fileName.contains(".")) {
                    fileName = user_id + fileName.substring(fileName.lastIndexOf("."), fileName.length());
                } else {
                    fileName = user_id + "";
                }
                String filePath = uploadFolder + File.separator + fileName;
				String smallFilePath = uploadFolder + File.separator + "min_" + fileName;

                String ct = file.getContentType();
                if (ct.equals("image/jpeg") ||
                        ct.equals("image/jpg") ||
                        ct.equals("image/png") ||
                        ct.equals("image/gif") ||
                        ct.equals("image/bmp")) {
                    if (file.getSize() < MAX_PHOTO_SIZE) {
						writeFile(filePath, file);
						writeFile(smallFilePath, file);
                        prepareImageForAvatar(filePath, false);
						prepareImageForAvatar(smallFilePath, true);

                        if (saveNewAvatarInDB(user_id, fileName)) {
                            return "Фото было изменено!";
                        } else {
                            return "Ошибка на стороне сервера. Попытайтесь снова.";
                        }
                    } else {
                        return "Загружаемый файл слишком большой.";
                    }
                } else {
                    return  "Неправильный тип файла.";
                }
            } catch (Exception ex) {
                return  "Ошибка на стороне сервера. Попытайтесь снова.";
            }
        }
    }

	private String writeFile (String path, MultipartFile file) {
		File uploadedFile = new File(path);
		// saves the file to upload directory
		byte[] bytes = new byte[0];
		try {
			bytes = file.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(
					new FileOutputStream(uploadedFile));
			stream.write(bytes);
			stream.close();
		} catch (IOException e) {
			return  "Ошибка на стороне сервера. Попытайтесь снова.";
		}
		return "";
	}
    //crop & resize
    private File prepareImageForAvatar(String path, boolean isSmall) throws IOException {
        File res = new File(path);
        BufferedImage in = ImageIO.read(res);
        in = Crop(in);
			in = Resize(in, isSmall);
        ImageIO.write(in, "jpg", res);
        return res;
    }

    private BufferedImage Crop(BufferedImage in) {
        BufferedImage dest = in.getSubimage(0, 0, Math.min(in.getHeight(), in.getWidth()), Math.min(in.getHeight(), in.getWidth()));
        return dest;
    }

    private BufferedImage Resize(BufferedImage in, boolean isSmall) {
        BufferedImage scaledBI;
		Graphics2D g;
		if (!isSmall) {
			scaledBI=new BufferedImage(400, 400, BufferedImage.TYPE_INT_RGB);
			g = scaledBI.createGraphics();
			g.drawImage(in, 0, 0, 400, 400, null);
		} else {
			scaledBI=new BufferedImage(70, 70, BufferedImage.TYPE_INT_RGB);
			g = scaledBI.createGraphics();
			g.drawImage(in, 0, 0, 70, 70, null);
		}
        g.dispose();
        return scaledBI;
    }

    private boolean saveNewAvatarInDB(long user_id, String fileName) {
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            con = (Connection) DriverManager.getConnection(DBProperties.conUrl, DBProperties.conUser, DBProperties.conPassword);
            st = (Statement) con.createStatement();
            st.executeUpdate("UPDATE profile SET photo='"+ fileName +"' WHERE p_id='"+ user_id +"'");
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (st != null) { st.close(); }
                if (con != null) { con.close(); }
            } catch (SQLException ex) {
            }
        }
        return false;
    }
}