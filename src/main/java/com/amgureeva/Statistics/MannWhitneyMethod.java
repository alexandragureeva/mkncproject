package com.amgureeva.Statistics;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.math3.stat.inference.MannWhitneyUTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MannWhitneyMethod {
	public  String setValues(String input, String groupingInput, int[] comparedIndexes,
							 String comparing) {
		String[][] dArr = prepareArr(new Gson().fromJson(input, String[][].class), comparedIndexes);
		String[][] gdArr = new Gson().fromJson(groupingInput, String[][].class);
		String[][] compareArr = new Gson().fromJson(comparing, String[][].class);
		String[][] results = new String[dArr.length][compareArr.length];

		for (int i = 0; i < comparedIndexes.length; i++)
		for (int j = 0; j < gdArr.length; j++ ) {
			if (compareArr[j].length != 2) {
				results[i][j] = "Incomparable";
			}  else {
				List<Double> list1 = new ArrayList<Double>();
				List<Double> list2 = new ArrayList<Double>();
				for (int k = 0; k < dArr[i].length; k ++ ) {
					if (!dArr[i][k].isEmpty() && StringUtils.isNumeric(dArr[i][k])) {
						double value = Double.valueOf(dArr[i][k]);
						String[] group1 = new Gson().fromJson(compareArr[j][0], String[].class);
						String[] group2 = new Gson().fromJson(compareArr[j][1], String[].class);
						Boolean add = addToGroup(group1[0], gdArr[j][k]);
						if (add!=null) {
							if (add) list1.add(value);
						} /*   else {
							results[i][j] = "Wrong grouping";
						} */
						add = addToGroup(group2[0], gdArr[j][k]);
						if (add!=null) {
							if (add) list2.add(value);
						}   /*else {
							results[i][j] = "Wrong grouping";
						}     */
					}
				}
				if (!list1.isEmpty() && !list2.isEmpty() && (results[i][j] ==null || results[i][j].isEmpty())) {
					MannWhitneyUTest mannWhitneyUTest = new MannWhitneyUTest();
					double[] list1Arr = new double[list1.size()], list2Arr = new double[list2.size()];
					for (int l = 0; l < list1.size(); l++) {
						list1Arr[l] = list1.get(l);
					}
					for (int l = 0; l < list2.size(); l++) {
						list2Arr[l] = list2.get(l);
					}
					double val = mannWhitneyUTest.mannWhitneyU(list1Arr, list2Arr);
					double pval = mannWhitneyUTest.mannWhitneyUTest(list1Arr, list2Arr);
					 results[i][j] = String.valueOf(val) + ";p=" + String.valueOf(pval);
				} else {
					if (results[i][j] ==null || results[i][j].isEmpty())
						results[i][j] = "No data to compare";
				}
			}
		}
		String ddd = new Gson().toJson(results);
		return ddd.substring(1, ddd.length()-1);
	}

	private String[][] prepareArr(String[][] arr, int[] comparedIndex) {
		Integer[] indexes = new Integer[]{0,1,2,3,4,5,11,13,14,15,19,20,23,24,25,26,44,45,46,48,49,65,66,67,
				79,80,86,104,105,109,121,122,124,125,131,132,134,135, 139, 140, 141, 142, 143, 150, 154, 155, 156, 157, 158,
				159, 161, 162, 163, 170, 171, 174, 175, 176, 177, 178, 179, 180, 185, 186, 187, 188, 189, 19, 192, 193, 194,
				202, 203, 205, 206, 207,208,209, 210, 211, 212, 213, 214, 215, 216, 224, 227, 228, 229, 230, 231, 232, 233,
				234, 241, 247, 253, 254, 255, 256, 257, 258, 263, 265, 266, 267, 274, 275, 276, 277, 278, 279, 284, 286,
				287, 288, 291, 292, 295, 296, 297, 299, 300, 301, 302, 304, 305, 306, 307, 308, 309, 310, 311, 312};
		List<Integer> indexesArr = Arrays.asList(indexes);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (!indexesArr.contains(comparedIndex[i])) {
					if (arr[i][j].equals("0")) {
						arr[i][j] = "";
					}
				}
			}
		}

		return arr;
	}
	
	private Boolean addToGroup(String group, String val) {
		if (group.contains("<") || group.contains(">")) {
			String temp;
			if (group.contains("=")) {
				temp = group.substring(2);
			} else {
				temp = group.substring(1);
			}
			if (!StringUtils.isNumeric(temp) || !StringUtils.isNumeric(val)) {
				return null;
			} else {
				double val1 = Double.valueOf(temp);
				if (group.contains("<")) {
					if (group.contains("=") && val1 >= Double.valueOf(val) ||
							!group.contains("=") && val1 > Double.valueOf(val)) {
						return true;
					} else {
						return false;

					}
				} else {
					if (group.contains("=") && val1 <= Double.valueOf(val) ||
							!group.contains("=") && val1 < Double.valueOf(val)) {
						return true;
					} else {
						return false;

					}
				}
			}
		} else {
			if (group.contains("[") && group.contains("]")) {
				String temp = group.substring(1, group.length() - 1);
				String[] arr = temp.split("(,)|(;)");
				if (Arrays.asList(arr).contains(val)) {
					return true;
				}
				return false;
			} else {
				if (Arrays.asList(group).contains(val)) {
					return true;
				} else {
					return false;
				}
			}
		}
	}
}
