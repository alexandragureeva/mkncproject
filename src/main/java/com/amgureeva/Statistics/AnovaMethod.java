package com.amgureeva.Statistics;

import com.google.gson.Gson;
import org.apache.commons.math3.stat.inference.OneWayAnova;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Nikita on 19.05.2016.
 */
public class AnovaMethod {
	public  String setValues(String input, String groupingInput, int[] comparedIndexes) {
		String[][] dArr = new Gson().fromJson(input, String[][].class);
		String[][] gdArr = new Gson().fromJson(groupingInput, String[][].class);
		String[][] results = new String[dArr.length][gdArr.length];

		for (int i = 0; i < comparedIndexes.length; i++) {
			for (int j = 0; j < gdArr.length; j++) {
				Map<String, Integer> categoricalValuesMap = new LinkedHashMap<String, Integer>();
				int l = 0;
				for (int k = 0; k < gdArr[j].length; k++) {
					if (categoricalValuesMap.get(gdArr[j][k]) == null) {
						categoricalValuesMap.put(gdArr[j][k], l);
						l++;
					}
				}
				boolean notComputable = false;
				List<List<Double>> values = new ArrayList<List<Double>>();
				for (int k = 0; k < categoricalValuesMap.size(); k++) {
					values.add(new ArrayList<Double>());
				}
				for (int k = 0; k < dArr[i].length; k++) {
					if (!isNumber(dArr[i][k])) {
						results[i][j] = "Variable #"+ (i+1) + " is not a number";
						notComputable = true;
						break;
					}
					int group = categoricalValuesMap.get(gdArr[j][k]);
					values.get(group).add(Double.valueOf(dArr[i][k]));

				}
				if (!notComputable) {
					for (int k = 0; k < categoricalValuesMap.size(); k++) {
						if (values.get(k).isEmpty()) {
							values.remove(k);
						}
					}
					if (values.size() == 0 || values.size() == 1) {
						results[i][j] = "Not enough groups: " + values.size();
						break;
					}
					for (int h = 0; h < values.size(); h++) {
						if (values.get(h).size() < 2) {
							results[i][j] = "At least one group has less than 2 items";
							notComputable = true;
							break;
						}
					}
					if (!notComputable) {
						if (results[i][j] == null || results[i][j].isEmpty()) {
							List<double[]> vals = new ArrayList<double[]>();
							for (int k = 0; k < categoricalValuesMap.size(); k++) {
								vals.add(new double[values.get(k).size()]);
								for (int m = 0; m < values.get(k).size(); m++) {
									vals.get(k)[m] = values.get(k).get(m);
								}
							}
							OneWayAnova anova = new OneWayAnova();
							results[i][j] = String.valueOf(anova.anovaFValue(vals));
						}
					}
				}
			}
		}
		String ddd = new Gson().toJson(results);
		return ddd.substring(1, ddd.length()-1);
	}

	private boolean isNumber(String string) {
		if (string == null || string.trim().isEmpty()) {
			return false;
		}
		int i = 0;
		if (string.charAt(0) == '-') {
			if (string.length() > 1) {
				i++;
			} else {
				return false;
			}
		}
		for (; i < string.length(); i++) {
			if (!Character.isDigit(string.charAt(i))) {
				return false;
			}
		}
		return true;
	}
}
