package com.amgureeva.Statistics;

import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class KaplanMeierMethod {
	public String setValues(List<String> deathDates, List<String> startDates, String[] intervals) {
		for(int i=0;i<startDates.size();i++){
			for(int j=i+1;j<startDates.size();j++){
				if(compare(startDates.get(i), startDates.get(j)) == 1){
					Collections.swap(deathDates, i, j);
					Collections.swap(startDates, i, j);

				}
			}
		}
		List<Integer> observed = new ArrayList<Integer>();
		List<Boolean> censored = new ArrayList<Boolean>();
		for (int i = 0;  i < startDates.size(); i++) {
			for (int j = 0; j < intervals.length; j++){
				if (compare(startDates.get(i), intervals[j]) == 1) {
					if (j+1 < intervals.length && compare(startDates.get(i), intervals[j+1]) == -1) {
						observed.add(j+1);
						censored.add(deathDates.get(i)==null);
					}
				}
			}
		}
		List<Interval> resultingIntervals = compute(observed, censored);
		if (resultingIntervals != null) {
			StringBuilder builder = new StringBuilder();
			for (Interval i : resultingIntervals) {
				System.out.println(i.getStart() + "\t" + i.getEnd() + "\t" + i.getNumberDied() + "\t" + i.getCensored().size() + "\t" + i.getCumulativeSurvival());
				builder.append("[");
				builder.append(i.getStart()+","+ i.getNumberDied()+","+ i.getNumberCensured()+","+ i.getCumulativeSurvival() +", "+ "true, " + i.getEnd() + ", \"" + intervals[i.getEnd()].replace('/', '.') + "\"") ;
				builder.append("],");
				for (int j = 0; j < observed.size(); j++) {
					if (observed.get(j) >= i.getStart() && observed.get(j)<i.getEnd()) {
						builder.append("[");
						builder.append(observed.get(j) +","+ i.getNumberDied()+","+ i.getNumberCensured()+","+ i.getCumulativeSurvival() +", "+ "false," + i.getEnd() + ", \"" + intervals[i.getEnd()].replace('/', '.') + "\"");
					    builder.append("],");
					}
				}
			}
			return builder.substring(0, builder.length()-1);
		} else {
			return "Not enough info";
		}
	}


	private int compare(String date1, String date2) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date d1 = sdf.parse(date1);
			Date d2 = sdf.parse(date2);
			return d1.compareTo(d2);
		} catch (ParseException e) {
			return 0;
		}
	}

	static List<Interval> compute(List<Integer> time, List<Boolean> censured) {

		if (time.size() != censured.size()) {
			return null;
		}
		if (time.size() < 2) {
			return null;
		}

		// step 1 -- find the intervals
		ArrayList<Interval> intervals = new ArrayList();
		int startTime = 0;
		int endTime = 0;
		for (int i = 0; i < time.size(); i++) {
			endTime = time.get(i);
			if (censured.get(i) == false && endTime > startTime) {
				intervals.add(new Interval(startTime, endTime));
				startTime = endTime;
			}
		}
		if (endTime > startTime) {
			intervals.add(new Interval(startTime, endTime));
		}

		// init variables.  Initially everyone is at risk, and the cumulative survival is 1
		float atRisk = time.size();
		float cumulativeSurvival = 1;
		Iterator<Interval> intervalIter = intervals.iterator();
		Interval currentInterval = intervalIter.next();
		currentInterval.setCumulativeSurvival(cumulativeSurvival);

		for (int i = 0; i < time.size(); i++) {

			int t = time.get(i);

			// If we have moved past the current interval compute the cumulative survival and adjust the # at risk
			// for the start of the next interval.
			if (t > currentInterval.getEnd()) {
				atRisk -= currentInterval.getNumberCensured();
				float survivors = atRisk - currentInterval.getNumberDied();
				float tmp = survivors / atRisk;
				cumulativeSurvival *= tmp;

				// Skip to the next interval
				atRisk -= currentInterval.getNumberDied();
				while (intervalIter.hasNext() && t > currentInterval.getEnd()) {
					currentInterval = intervalIter.next();
					currentInterval.setCumulativeSurvival(cumulativeSurvival);
				}
			}

			if (censured.get(i)) {
				currentInterval.addCensure(time.get(i));

			} else {
				currentInterval.incDied();
			}
		}
		currentInterval.setCumulativeSurvival(cumulativeSurvival);

		return intervals;

	}


	public static class Interval {
		private int start;
		private int end;
		private int numberDied;
		private ArrayList<Integer> censored = new ArrayList<Integer>();
		private float cumulativeSurvival;


		public Interval(int start, int end) {
			this.setStart(start);
			this.setEnd(end);
		}

		void incDied() {
			numberDied++;
		}

		void addCensure(int time) {
			censored.add(time);
		}

		public int getStart() {
			return start;
		}

		public void setStart(int start) {
			this.start = start;
		}

		public int getEnd() {
			return end;
		}

		public void setEnd(int end) {
			this.end = end;
		}

		public int getNumberDied() {
			return numberDied;
		}


		public List<Integer> getCensored() {
			return censored;
		}

		public float getCumulativeSurvival() {
			return cumulativeSurvival;
		}

		public void setCumulativeSurvival(float cumulativeSurvival) {
			this.cumulativeSurvival = cumulativeSurvival;
		}

		public int getNumberCensured() {
			return censored.size();
		}
	}

}
