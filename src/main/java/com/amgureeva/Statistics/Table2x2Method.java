package com.amgureeva.Statistics;

import com.google.gson.Gson;
import org.apache.commons.math3.special.Gamma;
import org.apache.commons.math3.stat.inference.ChiSquareTest;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Table2x2Method {
	public  String setValues(String values){
		int[] dArr = new Gson().fromJson(values, int[].class);
		int a = dArr[0];
		int b = dArr[2];
		int c = dArr[1];
		int d = dArr[3];
		String[][] results = new String[11][3];
		results[0][0] = String.valueOf(a);
		results[0][1] = String.valueOf(b);
		results[0][2] = String.valueOf(a+b);
		results[1][0] = String.format("%.4f", (double)a/(a+b+c+d)) + "%";
		results[1][1] = String.format("%.4f", (double)b/(a+b+c+d)) + "%";
		results[1][2] = String.format("%.4f", (double)(a+b)/(a+b+c+d)) + "%";
		results[2][0] = String.valueOf(c);
		results[2][1] = String.valueOf(d);
		results[2][2] = String.valueOf(c+d);
		results[3][0] = String.format("%.4f", (double)c/(a+b+c+d)) + "%";
		results[3][1] = String.format("%.4f", (double)d/(a+b+c+d)) + "%";
		results[3][2] = String.format("%.4f", (double)(c+d)/(a+b+c+d)) + "%";
		results[4][0] = String.valueOf(a+c);
		results[4][1] = String.valueOf(b+d);
		results[4][2] = String.valueOf(a+b+c+d);
		results[5][0] = String.format("%.4f", (double)(a+c)/(a+b+c+d)) + "%";
		results[5][1] = String.format("%.4f", (double)(b+d)/(a+b+c+d)) + "%";
		results[5][2] = "";
		ChiSquareTest test = new ChiSquareTest();
		results[6][0] = String.format("%.4f", test.chiSquare(new long[][]{{a, c},{b, d}}));
		results[6][1] = "p=" + String.format("%.4f", test.chiSquareTestDataSetsComparison(new long[]{a, c}, new long[]{b, d}));
		results[6][2] = "";
		double yates =  getYatesChiSquare(a, b, c, d);
		results[7][0] = String.format("%.4f", yates);
		results[7][1] = "p=" + String.format("%.4f", chisqr(1, yates));
		results[7][2] = "";
		double[] fisherValues = getFisher(a, b, c, d);
		results[8][1] = "p=" + String.format("%.4f", fisherValues[0]);
		results[9][1] = "p=" + String.format("%.4f", fisherValues[1]);
		results[8][0] = "";
		results[8][2] = "";
		results[9][0] = "";
		results[9][2] = "";
		double mcNemar = getMcNemar(b, c);
		results[10][0] = String.format("%.4f", mcNemar);
		results[10][1] = "p=" + String.format("%.4f", chisqr(1, mcNemar));
		results[10][2] = "";




		String ddd = new Gson().toJson(results);
		return ddd.substring(1, ddd.length()-1);
	}


	private double getYatesChiSquare(int a, int b, int c, int d) {
		double summ = a+b+c+d;
		double r_a = (a+b)*(a+c)/summ;
		double r_b = (a+b)*(b+d)/summ;
		double r_c = (a+c)*(c+d)/summ;
		double r_d = (b+d)*(c+d)/summ;
		return (Math.abs(r_a-a)-0.5)*(Math.abs(r_a-a)-0.5)/r_a + (Math.abs(r_b -b)-0.5)*(Math.abs(r_b-b)-0.5)/r_b +
				(Math.abs(r_c-c)-0.5)*(Math.abs(r_c-c)-0.5)/r_c + (Math.abs(r_d-d)-0.5)*(Math.abs(r_d-d)-0.5)/r_d;
	}

	private double chisqr(int Dof, double Cv) {
		//	Gamma gamma;
		if (Cv < 0 || Dof < 1) {
			return 0.0;
		}
		double K = ((double) Dof) * 0.5;
		double X = Cv * 0.5;
		if (Dof == 2) {
			return Math.exp(-1.0 * X);
		}

		Double PValue = igf(K, X);
		if ((PValue.isNaN()) || (PValue.isInfinite()) || PValue <= 1e-8) {
			return 1e-14;
		}

		PValue /= Gamma.gamma(K);

		return (1.0 - PValue);
	}

	private double igf(double S, double Z) {
		if (Z < 0.0) {
			return 0.0;
		}
		double Sc = (1.0 / S);
		Sc *= Math.pow(Z, S);
		Sc *= Math.exp(-Z);

		double Sum = 1.0;
		double Nom = 1.0;
		double Denom = 1.0;

		for (int I = 0; I < 200; I++) {
			Nom *= Z;
			S++;
			Denom *= S;
			Sum += (Nom / Denom);
		}

		return Sum * Sc;
	}

	private double[] getFisher(int a, int b, int c, int d) {
		double[] res = new double[2];
		double imp = getbinominal(a,b,c,d);
		long minVal = Math.min(a,b);
		minVal = Math.min(minVal, c);
		minVal = Math.min(minVal,d);
		long tempa, tempb, tempc, tempd;
		if (minVal == a) {
			tempa = a;
			tempb = b;
			tempc = c;
			tempd = d;
		} else if  (minVal == b) {
			tempa = b;
			tempb = a;
			tempc = d;
			tempd = c;
		} else if (minVal == c) {
			tempa = c;
			tempb = d;
			tempc = a;
			tempd = b;
		}  else {
			tempa = d;
			tempb = c;
			tempc = b;
			tempd = a;
		}
		double oneSideP = imp;
		int i = 0;
		while (tempa>0){
			i += 1;
			tempa--;
			tempb++;
			tempc++;
			tempd--;
			double val = getbinominal(tempa, tempb, tempc, tempd);
			if (val != -1) {
				oneSideP += val;
			}
		}
		tempa +=i;
		tempb -= i;
		tempd += i;
		tempc -= i;
		long max = tempb+tempa;
		double twoSidep = oneSideP;
		while (tempa<max){
			tempa++;
			tempb--;
			tempc--;
			tempd++;
			double val = getbinominal(tempa, tempb, tempc, tempd);
			if (val != -1 && val<=imp) {
				twoSidep+=val;
			}
		}
		res[0] = oneSideP;
		res[1] = twoSidep;
		return res;
	}

	private double getbinominal(long a, long b, long c, long d) {
		if (a<0 || b < 0 || c<0 || d<0)
			return -1;
		BigDecimal res =  (factorial(a+b).divide(factorial(a), 4, RoundingMode.HALF_UP));
		res = res.multiply(factorial(c+d).divide(factorial(d), 4, RoundingMode.HALF_UP));
		res = res.multiply((factorial(a+c).divide(factorial(c), 4, RoundingMode.HALF_UP)));
		res = res.multiply(factorial(d+b).divide(factorial(b), 4, RoundingMode.HALF_UP));
		res = res.divide(factorial(a+b+c+d), 4, RoundingMode.HALF_UP);
		return res.doubleValue();
	}
	private BigDecimal factorial(long n) {
		BigDecimal fact = new BigDecimal("1");
		for (int c = 1 ; c <= n ; c++ ) {
			fact = fact.multiply(BigDecimal.valueOf(c));
		}
		return fact;
	}

	private double getMcNemar(int b, int c) {
		return Math.pow(b-c, 2)/(b+c);
	}

}
