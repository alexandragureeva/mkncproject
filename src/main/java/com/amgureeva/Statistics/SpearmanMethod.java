package com.amgureeva.Statistics;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SpearmanMethod {
	public  String setSpearman(String input,int[] indexes){
		String[][] dArr = new Gson().fromJson(input, String[][].class);
		//values = prepareData(values, id);
		String[][] results = new String[dArr.length][dArr.length];
		int i = 0;
		for(String[] values : dArr) {
			int j = 0;
			for (String[] values2 : dArr) {
				if (!values.equals(values2)) {
					if (values.length > 0 && values2.length > 0) {
						List<Double> vals = prepareData(new LinkedList<String>(Arrays.asList(values)), indexes[i]);
						List<Double> vals2 = prepareData(new LinkedList<String>(Arrays.asList(values2)), indexes[j]);
						if (!vals.isEmpty() && !vals2.isEmpty()) {
							if (vals.size() != vals2.size()) {
								results[i][j] = "Dimension mismatch (" + vals.size() + " != " + vals2.size() + ")";
							} else {
								if (vals.size() < 2) {
									results[i][j] = "Insufficient dimension <2";
								} else {
									SpearmansCorrelation spearmansCorrelation = new SpearmansCorrelation();
									double[] v1 = new double[vals.size()];
									double[] v2 = new double[vals2.size()];
									for (int k = 0;  k < vals.size(); k++) {
										v1[k] = vals.get(k);
										v2[k] = vals2.get(k);
									}
									results[i][j] = String.format("%.4f", spearmansCorrelation.correlation(v1, v2));
								}
							}
						} else {
							results[i][j] = "No values";
						}
					}

				} else {
					results[i][j] = "";
				}
				 j++;
			}
			i++;
		}
		String ddd = new Gson().toJson(results);
		return ddd.substring(1, ddd.length()-1);
	}

	private List<Double> prepareData(List<String> values, int id) {
		Integer[] indexes = new Integer[]{0,1,2,3,4,5,11,13,14,15,19,20,23,24,25,26,44,45,46,48,49,65,66,67,
				79,80,86,104,105,109,121,122,124,125,131,132,134,135, 139, 140, 141, 142, 143, 150, 154, 155, 156, 157, 158,
				159, 161, 162, 163, 170, 171, 174, 175, 176, 177, 178, 179, 180, 185, 186, 187, 188, 189, 19, 192, 193, 194,
				202, 203, 205, 206, 207,208,209, 210, 211, 212, 213, 214, 215, 216, 224, 227, 228, 229, 230, 231, 232, 233,
				234, 241, 247, 253, 254, 255, 256, 257, 258, 263, 265, 266, 267, 274, 275, 276, 277, 278, 279, 284, 286,
				287, 288, 291, 292, 295, 296, 297, 299, 300, 301, 302, 304, 305, 306, 307, 308, 309, 310, 311, 312};
		List<Integer> indexesArr = Arrays.asList(indexes);
		if (!indexesArr.contains(id)) {
			values.remove("0");
		}
		List<Double> result = new ArrayList<Double>();
		for (int i = 0; i < values.size(); i++) {
				if (!values.get(i).isEmpty() && StringUtils.isNumeric(values.get(i))) {
					result.add(Double.valueOf(values.get(i)));
				}
		}
		return result;
	}
}
