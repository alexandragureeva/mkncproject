package com.amgureeva.Statistics;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * Created by Nikita on 13.05.2016.
 */
public class DescStatMethods {
	//discriptional statistics
	public  String setDStatistics(String input,int[] indexes,  int[] functions){
		String[][] dArr = new Gson().fromJson(input, String[][].class);
		//values = prepareData(values, id);
		int i = 0;
		String[][] results = new String[dArr.length][functions.length];
		for(String[] values : dArr) {
			List<String> vals = prepareData(new ArrayList<String>(Arrays.asList(values)), indexes[i]);
		//	if (vals.size() > 0) {
				int j = 0;
				for (int function : functions) {
					switch (function) {
						case 0:
							results[i][j] = String.valueOf(getNum(vals));
							break;
						case 1:
							Double avg = getAverage(vals);
							results[i][j] = String.valueOf((avg.equals(Double.MIN_VALUE)) ? "No values" : avg);
							break;
						case 2:
							Double se = getStandardError(vals);
							results[i][j] = String.valueOf((se.equals(-1.0)) ? "No values" : se);
							break;
						case 3:
							Double sd = getStandardDeviation(vals);
							results[i][j] = String.valueOf((sd.equals(-1.0)) ? "No values" : sd);
							break;
						case 4:
							Double ev = getExpectedValue(vals);
							results[i][j] = String.valueOf((ev.equals(Double.MIN_VALUE)) ? "No values" : ev);
							break;
						case 5:
							results[i][j] = String.valueOf((getMax(vals)));
							break;
						case 6:
							results[i][j] = String.valueOf((getMin(vals)));
							break;
						case 7:
							results[i][j] = String.valueOf((getMedian(vals)));
							break;
						case 8:
							results[i][j] = getMode(vals);
							break;
					}
					j++;
				}
				i++;

		//	}
		}
		String ddd = new Gson().toJson(results);
		return ddd.substring(1, ddd.length()-1);
	}

	private int getNum(List<String> values) {
		return values.size();
	}

	private Double getAverage(List<String> values) {
		if (values.size() > 0) {
			double sum = 0;
			for (String val : values) {
				if (!val.isEmpty() && StringUtils.isNumeric(val)) {
					sum += Double.valueOf(val);
				}
			}
			return sum/values.size();
		}   else  {
			return Double.MIN_VALUE;
		}
	}

	private Double getStandardDeviation(List<String> values) {
		if (values.size() > 1) {
			double avg = getAverage(values);
			double deviation = 0.0;
			for (String val : values) {
				if (!val.isEmpty() && StringUtils.isNumeric(val)) {
					deviation += Math.pow((Double.valueOf(val) - avg), 2);
				}
			}
			return Math.sqrt(deviation/(values.size()-1));
		} else {
			return -1.0;
		}
	}

	private Double getStandardError(List<String> values) {
		if (values.size() > 0) {
			Double deviation =  getStandardDeviation(values);
			if (!deviation.equals(-1.0) && !deviation.isNaN()) {
				return deviation / Math.sqrt(values.size());
			}   else {
				return -1.0;
			}
		} else {
			return -1.0;
		}
	}

	private Double getExpectedValue(List<String> values) {
		if (values.size() > 0) {
			double possibility = 1.0/values.size();
			double expectedValue = 0.0;
			for (String val : values) {
				if (!val.isEmpty() && StringUtils.isNumeric(val)) {
					expectedValue += Double.valueOf(val) * possibility;
				}
			}
			return expectedValue;
		} else {
			return Double.MIN_VALUE;
		}
	}

	private String getMin(List<String> values) {
		if (values.size() > 0) {
			int minIndex = 0;
			for (int i = 0; i < values.size(); i++) {
				if (!values.get(i).isEmpty() && !values.get(i).isEmpty() && StringUtils.isNumeric(values.get(i))) {
					if (Double.valueOf(values.get(minIndex)) <= Double.valueOf(values.get(i))) {
						minIndex = i;
					}
				}
			}
			return values.get(minIndex);
		} else {
			return "No values";
		}
	}

	private String getMax(List<String> values) {
		if (values.size() > 0) {
			int maxIndex = 0;
			for (int i = 0; i < values.size(); i++) {
				if (StringUtils.isNumeric(values.get(i))) {
					if (Double.valueOf(values.get(maxIndex)) >= Double.valueOf(values.get(i))) {
						maxIndex = i;
					}
				}
			}
			return values.get(maxIndex);
		} else {
			return "No values";
		}
	}

	private String getMedian(List<String> values) {
		if (values.size() > 0) {
			List<Double> dValues = new ArrayList<Double>(values.size());
			for (String val : values) {
				if (!val.isEmpty() && StringUtils.isNumeric(val)) {
					dValues.add(Double.valueOf(val));
				}
			}
			Collections.sort(dValues);
			if (dValues.size() > 0) {
				if (dValues.size() % 2 == 1) {
					return String.valueOf(dValues.get((int) ((dValues.size()) / 2)));
				} else {
					double sum = dValues.get((dValues.size()) / 2) +
							dValues.get((dValues.size() - 1) / 2);
					return String.valueOf(sum / 2);
				}
			} else {
				return "No values";
			}
		} else {
			return "No values";
		}
	}

	private String getMode(List<String> values) {
		if (values.size() > 0) {
		HashMap<Double,Double> freqs = new HashMap<Double,Double>();
		for (String d: values) {
			if (!d.isEmpty() && StringUtils.isNumeric(d)) {
				Double freq = freqs.get(Double.valueOf(d));
				freqs.put(Double.valueOf(d), (freq == null ? 1 : freq + 1));
			}
		}
		List<Double> modes = new ArrayList<Double>();
		double maxFreq = 0;
		for (Map.Entry<Double,Double> entry : freqs.entrySet()) {
			double freq = entry.getValue();
			if (freq > maxFreq) {
				maxFreq = freq;
				modes = new ArrayList<Double>();
				modes.add(entry.getKey());
			}   else if (freq == maxFreq) {
				modes.add(entry.getKey());
			}
		}
		return modes.toString();
		} else {
			return "No values";
		}
	}

	private List<String> prepareData(List<String> values, int id) {
		values.removeAll(Arrays.asList(null,""));
		Integer[] indexes = new Integer[]{0,1,2,3,4,5,11,13,14,15,19,20,23,24,25,26,44,45,46,48,49,65,66,67,
		79,80,86,104,105,109,121,122,124,125,131,132,134,135, 139, 140, 141, 142, 143, 150, 154, 155, 156, 157, 158,
		159, 161, 162, 163, 170, 171, 174, 175, 176, 177, 178, 179, 180, 185, 186, 187, 188, 189, 19, 192, 193, 194,
		202, 203, 205, 206, 207,208,209, 210, 211, 212, 213, 214, 215, 216, 224, 227, 228, 229, 230, 231, 232, 233,
		234, 241, 247, 253, 254, 255, 256, 257, 258, 263, 265, 266, 267, 274, 275, 276, 277, 278, 279, 284, 286,
		287, 288, 291, 292, 295, 296, 297, 299, 300, 301, 302, 304, 305, 306, 307, 308, 309, 310, 311, 312};
		List<Integer> indexesArr = Arrays.asList(indexes);
		 if (!indexesArr.contains(id)) {
			 values.remove("0");
		 }
		return values;
	}
}
