package com.amgureeva.web.controller;

import com.amgureeva.Entities.*;
import com.amgureeva.Entities.Surgeries.Drenirovanie;
import com.amgureeva.Entities.Surgeries.Punkcia;
import com.amgureeva.Other.Properties;
import com.amgureeva.binding.AnalysisBinding.AnalysisService;
import com.amgureeva.binding.ObjExaminationBinding.ObjExaminationService;
import com.amgureeva.binding.PatientBinding.PatientService;
import com.amgureeva.binding.PatientFileUploadBinding.PatientFileUploadService;
import com.amgureeva.binding.TwoStepsTreatmentBinding.TwoStepsTreatmentService;
import com.amgureeva.binding.UserBinding.UserService;
import com.amgureeva.web.controller.Validators.AnalysisValidator;
import com.amgureeva.web.controller.Validators.ObjExaminationValidator;
import com.amgureeva.web.controller.Validators.PatientFormValidator;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Александра on 11.04.2016.
 */
@Controller
public class PatientController {

	@Autowired
	private PatientService patientService;
	@Autowired
	private AnalysisService analysisService;


	@Autowired
	private UserService userService;

	@Autowired
	private TwoStepsTreatmentService twoStepsTreatmentService;

	@Autowired
	private ObjExaminationService objExaminationService;

	@Autowired
	private PatientFileUploadService patientFileUploadService;

	@Autowired
	private Properties properties;

	@Autowired
	PatientFormValidator patientFormValidator;

	@Autowired
	ObjExaminationValidator objExaminationValidator;

	@Autowired
	AnalysisValidator analysisValidator;

	@Autowired
	public void setAnalysisValidator(AnalysisValidator analysisValidator) {
		this.analysisValidator = analysisValidator;
	}

	@InitBinder("patient")
	protected void initPatientBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.setValidator(patientFormValidator);
	}

	@InitBinder("objExamination")
	protected void initObjExaminationBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.setValidator(objExaminationValidator);
	}


	@InitBinder("analysis")
	protected void initAnalysisBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.setValidator(analysisValidator);
	}

	@RequestMapping(value = "/saveGenInfo", method = RequestMethod.POST)
	public String saveGenInfo(@ModelAttribute("patient") @Validated Patient patient, BindingResult genInfoResult,
							  final RedirectAttributes redirectAttributes, Model model) {
		if (genInfoResult.hasErrors()) {
			populateDefaultModel(model);
			redirectAttributes.addFlashAttribute("patient", patient);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.patient", genInfoResult);
			if (patient.getId() == null) {
				return "redirect:/generalInfo";
			} else {
				return "redirect:/generalInfo/" + patient.getId();
			}
		} else {
			if (patient != null) {
				GeneralPatientInfo gpi = new GeneralPatientInfo();
				gpi.setPatient(patient);
				redirectAttributes.addFlashAttribute("gpi", gpi);
				redirectAttributes.addFlashAttribute("css", "success");
				redirectAttributes.addFlashAttribute("msg", "Данные о пациенте были успешно загружены!");
				try {
					String id = patientService.saveOrUpdate(gpi);

					if (patient.areAllFieldsFilled()) {
						patient.setFilledGeneralInfo(true);
					} else {
						patient.setFilledGeneralInfo(false);
					}
					if (patient.getDiagnosisName().equals("Другой диагноз")) {
						patient.setFilledDiagnosisAtStart(true);
						patient.setFilledSurgery(true);
					}
					patientService.updateFillers(patient);
					if (patient.getClosedDate() == null || patient.getClosedDate().after(Calendar.getInstance().getTime()) ||
							patient.getClosedDate().equals(Calendar.getInstance().getTime())) {
						String name = SecurityContextHolder.getContext().getAuthentication().getName();
						User user = userService.findByEmail(name);
						Editor editor = patientService.getEditorDAO().findBlockEditorById(Integer.valueOf(id), 0);
						if (editor == null) {
							editor = new Editor(String.valueOf(user.getUserId()), id, "0");
							Calendar calendar = Calendar.getInstance();
							calendar.add(Calendar.DATE, 3);
							editor.setDate(new java.sql.Date(calendar.getTimeInMillis()));
							patientService.getEditorDAO().save(editor);
						}
					}
					return "redirect:/patients/update/" + id;

				} catch (Throwable t) {
					redirectAttributes.addFlashAttribute("msg", t + "\nОшибка при добавлении пациента. Попробуйте снова.");
					redirectAttributes.addFlashAttribute("css", "danger");
					redirectAttributes.addFlashAttribute("patient", patient);
					populateDefaultModel(model);
				}
			}
		}
		return "redirect:/generalInfo";
	}

	@RequestMapping(value = "/saveEndGenInfo", method = RequestMethod.POST)
	public String saveEndGenInfo(@ModelAttribute("generalInfoEnd") GeneralInfoEnd generalInfoEnd,
								 final RedirectAttributes redirectAttributes) throws ParseException {
		Patient p = patientService.findPatientById(generalInfoEnd.getId());
		if (p != null) {
			if (generalInfoEnd.getEndDate() != null && !generalInfoEnd.getEndDate().isEmpty()) {
				p.setEndDate(generalInfoEnd.getEndDate());
				p.setBedDaysNumber();
				if (p.getClosedDate() == null || p.getClosedDate().after(Calendar.getInstance().getTime()) ||
						p.getClosedDate().equals(Calendar.getInstance().getTime())) {
					Calendar calendar = Calendar.getInstance();
					DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					java.util.Date optDate = formatter.parse(generalInfoEnd.getEndDate());
					calendar.setTime(optDate);
					if (calendar.getTime().after(Calendar.getInstance().getTime()) ||
							calendar.getTime().equals(Calendar.getInstance().getTime())) {
						calendar.add(Calendar.DATE, 3);
					} else {
						calendar = Calendar.getInstance();
						calendar.add(Calendar.DATE, 1);
					}
					p.setClosedDate(calendar.getTime());
					Editor editor = patientService.getEditorDAO().findBlockEditorById(generalInfoEnd.getId(), 1);
					if (editor == null) {
						String name = SecurityContextHolder.getContext().getAuthentication().getName();
						User user = userService.findByEmail(name);
						editor = new Editor(String.valueOf(user.getUserId()), p.getId(), "1");
						editor.setDate(new java.sql.Date(calendar.getTimeInMillis()));
						patientService.getEditorDAO().save(editor);
					} else {
						editor.setDate(new java.sql.Date(calendar.getTimeInMillis()));
						patientService.getEditorDAO().update(editor);
					}
				}
			}
			if (generalInfoEnd.getDeathDate() != null && !generalInfoEnd.getDeathDate().isEmpty()) {
				p.setDeathDate(generalInfoEnd.getDeathDate());
			}
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные о пациенте при выписке были успешно загружены!");
			try {
				patientService.updateEndInfo(p);
				if (p.areAllFieldsFilled()) {
					p.setFilledGeneralInfo(true);
				} else {
					p.setFilledGeneralInfo(false);
				}
				patientService.updateFillers(p);
				return "redirect:/patients/update/" + generalInfoEnd.getId();

			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\nОшибка при добавлении информации о выписке пациента." +
						" Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("generalInfoEnd", generalInfoEnd);
			}
		}

		return "redirect:/generalEndInfo/" + generalInfoEnd.getId();
	}

	// show add user form
	@RequestMapping(value = "/generalInfo", method = RequestMethod.GET)
	public String showAddUserForm(Model model) {
		model.addAttribute("expired", false);
		if (model.asMap().get("patient") != null) {
			model.addAttribute("patient", model.asMap().get("patient"));
			model.addAttribute("org.springframework.validation.BindingResult.patient",
					model.asMap().get("org.springframework.validation.BindingResult.patient"));
		} else {
			model.addAttribute("patient", new Patient());
		}
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userService.findByEmail(name);
		if (user.isAdmin()) {
			model.addAttribute("isAdmin", true);
		} else {
			model.addAttribute("isAdmin", false);
		}
		model.addAttribute("msg", model.asMap().get("msg"));
		model.addAttribute("css", model.asMap().get("css"));

		populateDefaultModel(model);
		return "patientSubpages/general_info";
	}


	@RequestMapping(value = "/generalEndInfo/{id}", method = RequestMethod.GET)
	public String showGeneralEndInfoForm(@PathVariable("id") int id, Model model) {
		model.addAttribute("expired", false);
		Patient patient = patientService.findPatientById(id);
		GeneralInfoEnd gie = new GeneralInfoEnd();
		gie.setId(id);
		if (patient == null) {
			model.addAttribute("generalInfoEnd", gie);
		} else {
			if (patient.getEndDate() != null && !patient.getEndDate().isEmpty()) {
				gie.setEndDate(patient.getEndDate());
			}
			if (patient.getDeathDate() != null && !patient.getDeathDate().isEmpty()) {
				gie.setDeathDate(patient.getDeathDate());
			}
		}
		model.addAttribute("generalInfoEnd", gie);
		Calendar c = Calendar.getInstance();
		if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
			model.addAttribute("expired", true);
		}
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userService.findByEmail(name);
		if (user.isAdmin()) {
			model.addAttribute("isAdmin", true);
		} else {
			model.addAttribute("isAdmin", false);
		}
		populateDefaultModel(model);
		return "patientSubpages/general_info_end";
	}

	@RequestMapping(value = "/generalInfo/{id}", method = RequestMethod.GET)
	public String showGeneralInfoForm(@PathVariable("id") int id, Model model) {
		model.addAttribute("expired", false);
		Patient patient = patientService.findPatientById(id);
		if (patient == null) {
			patient = new Patient();
			patient.setId(String.valueOf(id));
		}
		model.addAttribute("patient", patient);
		Calendar c = Calendar.getInstance();
		if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
			model.addAttribute("expired", true);
		} else {
			List<Editor> editors =
					patientService.getEditorDAO().findById(id);
			for (Editor e : editors) {
				if (e.getBlockId().equals("0") && e.isExpired()) {
					model.addAttribute("expired", true);
				}
			}
		}
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userService.findByEmail(name);
		if (user.isAdmin()) {
			model.addAttribute("isAdmin", true);
		} else {
			model.addAttribute("isAdmin", false);
		}
		populateDefaultModel(model);
		return "patientSubpages/general_info";
	}

	@RequestMapping(value = "/analysis/firstday/{id}", method = RequestMethod.GET)
	public String showAnalysis0Form(@PathVariable("id") int id, Model model) {
		constructAnalysisModel(id, 0, model);
		return "patientSubpages/analysis";
	}


	@RequestMapping(value = "/analysis/day1/{id}", method = RequestMethod.GET)
	public String showAnalysis1Form(@PathVariable("id") int id, Model model) {
		constructAnalysisModel(id, 1, model);
		return "patientSubpages/analysis";
	}

	@RequestMapping(value = "/analysis/day5/{id}", method = RequestMethod.GET)
	public String showAnalysis5Form(@PathVariable("id") int id, Model model) {
		constructAnalysisModel(id, 5, model);
		return "patientSubpages/analysis";
	}

	@RequestMapping(value = "/analysis/lastday/{id}", method = RequestMethod.GET)
	public String showAnalysis10Form(@PathVariable("id") int id, Model model) {
		constructAnalysisModel(id, 10, model);
		return "patientSubpages/analysis";
	}

	private void constructAnalysisModel(int id, int day, Model model) {
		if (model.asMap().get("analysis") != null) {
			model.addAttribute("analysis", model.asMap().get("analysis"));
			model.addAttribute("org.springframework.validation.BindingResult.analysis",
					model.asMap().get("org.springframework.validation.BindingResult.analysis"));
		} else {
			Analysis analysis = analysisService.findById(id, day);
			if (analysis == null) {
				analysis = new Analysis();
				analysis.setId(String.valueOf(id));
				analysis.setIsNew(true);
				analysis.setDay(day);
			}
			model.addAttribute("expired", false);
			Patient patient = patientService.findPatientById(Integer.valueOf(analysis.getId()));
			Calendar c = Calendar.getInstance();
			if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
				model.addAttribute("expired", true);
			} else {
				List<Editor> editors =
						patientService.getEditorDAO().findById(id);
				for (Editor e : editors) {
					switch (day) {
						case 0:
							if (e.getBlockId().equals("0") && e.isExpired()) {
								model.addAttribute("expired", true);
							}
							break;
						default:
							if (e.getBlockId().equals("1") && e.isExpired()) {
								model.addAttribute("expired", true);
							}
							break;
					}
				}
			}
			String name = SecurityContextHolder.getContext().getAuthentication().getName();
			User user = userService.findByEmail(name);
			if (user.isAdmin()) {
				model.addAttribute("isAdmin", true);
			} else {
				model.addAttribute("isAdmin", false);
			}
			model.addAttribute("analysis", analysis);

		}
	}

	@RequestMapping(value = "/saveAnalysis", method = RequestMethod.POST)
	public String submitAnalysisForm(@ModelAttribute("analysis") @Validated Analysis analysis,
									 BindingResult analysisResult,
									 final RedirectAttributes redirectAttributes) {
		String day = null;
		switch (analysis.getDay()) {
			case 0:
				day = "firstday";
				break;
			case 1:
				day = "day1";
				break;
			case 5:
				day = "day5";
				break;
			case 10:
				day = "lastday";
				break;
		}
		if (analysisResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("analysis", analysis);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.analysis", analysisResult);
			return "redirect:/analysis/" + day + "/" + analysis.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные анализов были успешно загружены!");
			try {
				Patient patient = patientService.findPatientById(Integer.valueOf(analysis.getId()));
				if (analysis.getIsNew()) {
					switch (analysis.getDay()) {
						case 0:
							patient.setFilledAnalysis0(true);
							break;
						case 1:
							patient.setFilledAnalysis1(true);
							break;
						case 5:
							patient.setFilledAnalysis5(true);
							break;
						case 10:
							patient.setFilledAnalysis10(true);
							break;
					}
					patientService.updateFillers(patient);
				}
				analysisService.saveOrUpdate(analysis, analysis.getIsNew());
				return "redirect:/patients/update/" + analysis.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\nОшибка при добавлении " +
						"информации о пациенте. Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("analysis", analysis);
			}
		}
		return "redirect:/analysis/" + day + "/" + analysis.getId();
	}

	@RequestMapping(value = "/objexamination/{id}", method = RequestMethod.GET)
	public String showObjExaminationForm(@PathVariable("id") int id, Model model) {
		if (model.asMap().get("objExamination") != null) {
			model.addAttribute("objExamination", model.asMap().get("objExamination"));
			model.addAttribute("org.springframework.validation.BindingResult.objExamination",
					model.asMap().get("org.springframework.validation.BindingResult.objExamination"));
		} else {
			ObjectiveExamination objectiveExamination = objExaminationService.findById(id);
			if (objectiveExamination == null) {
				objectiveExamination = new ObjectiveExamination();
				objectiveExamination.setId(String.valueOf(id));
				objectiveExamination.setIsNew(true);
			}
			model.addAttribute("expired", false);
			Patient patient = patientService.findPatientById(Integer.valueOf(objectiveExamination.getId()));
			Calendar c = Calendar.getInstance();
			if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
				model.addAttribute("expired", true);
			} else {
				List<Editor> editors =
						patientService.getEditorDAO().findById(id);
				for (Editor e : editors) {
					if (e.getBlockId().equals("0") && e.isExpired()) {
						model.addAttribute("expired", true);
					}
				}
			}
			String name = SecurityContextHolder.getContext().getAuthentication().getName();
			User user = userService.findByEmail(name);
			if (user.isAdmin()) {
				model.addAttribute("isAdmin", true);
			} else {
				model.addAttribute("isAdmin", false);
			}
			model.addAttribute("objExamination", objectiveExamination);
		}
		return "patientSubpages/objective_examination";
	}

	@RequestMapping(value = "/saveObjectiveExamination", method = RequestMethod.POST)
	public String submitObjExaminationForm(@ModelAttribute("objExamination")
										   @Validated ObjectiveExamination objectiveExamination,
										   BindingResult objExResult,
										   final RedirectAttributes redirectAttributes, Model model) {
		if (objExResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("objExamination", objectiveExamination);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.objExamination", objExResult);
			return "redirect:/objexamination/" + objectiveExamination.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные об объективном осмотре были успешно загружены!");
			try {
				objExaminationService.saveOrUpdate(objectiveExamination, objectiveExamination.getIsNew());
				Patient p = patientService.findPatientById(Integer.valueOf(objectiveExamination.getId()));
				if (objectiveExamination.areAllFieldsFilled()) {
					p.setFilledObjExam(true);
				} else {
					p.setFilledObjExam(false);
				}
				patientService.updateFillers(p);
				return "redirect:/patients/update/" + objectiveExamination.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\nОшибка при добавлении информации об" +
						"объективном осмотре врача. Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("objExamination", objectiveExamination);
			}
		}
		return "redirect:/objexamination/" + objectiveExamination.getId();
	}

	@RequestMapping(value = "/twoStepsTreatment/{id}", method = RequestMethod.GET)
	public String showTwoStepsTreatment(@PathVariable("id") int id, Model model) {
		if (model.asMap().get("twostepstreat") != null) {
			model.addAttribute("twostepstreat", model.asMap().get("twostepstreat"));
			model.addAttribute("org.springframework.validation.BindingResult.twostepstreat",
					model.asMap().get("org.springframework.validation.BindingResult.twostepstreat"));
		} else {
			TwoStepsTreatment twoStepsTreatment = twoStepsTreatmentService.findById(id);
			if (twoStepsTreatment == null) {
				twoStepsTreatment = new TwoStepsTreatment();
				twoStepsTreatment.setId(String.valueOf(id));
				twoStepsTreatment.setIsNew(true);
			}
			model.addAttribute("expired", false);
			Patient patient = patientService.findPatientById(id);
			if (patient == null) {
				patient = new Patient();
				patient.setId(String.valueOf(id));
			}
			model.addAttribute("twostepstreat", twoStepsTreatment);
			Calendar c = Calendar.getInstance();
			if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
				model.addAttribute("expired", true);
			} else {
				List<Editor> editors =
						patientService.getEditorDAO().findById(id);
				for (Editor e : editors) {
					if (e.getBlockId().equals("0") && e.isExpired()) {
						model.addAttribute("expired", true);
					}
				}
			}
			String name = SecurityContextHolder.getContext().getAuthentication().getName();
			User user = userService.findByEmail(name);
			if (user.isAdmin()) {
				model.addAttribute("isAdmin", true);
			} else {
				model.addAttribute("isAdmin", false);
			}
		}
		return "patientSubpages/twostepstreatment";
	}

	@RequestMapping(value = "/saveTwoStepsTreatment", method = RequestMethod.POST)
	public String submitTwoStepsTreatment(@ModelAttribute("twostepstreat")
										   @Validated TwoStepsTreatment twoStepsTreatment,
										   BindingResult objExResult,
										   final RedirectAttributes redirectAttributes) {
		if (objExResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("twostepstreat", twoStepsTreatment);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.twostepstreat", objExResult);
			return "redirect:/twoStepsTreatment/" + twoStepsTreatment.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные о двухэтапном лечении были успешно загружены!");
			try {
				twoStepsTreatmentService.saveOrUpdate(twoStepsTreatment, twoStepsTreatment.getIsNew());
				patientService.setTypeOfTreatment((twoStepsTreatment.getProvedenie2Etapnogo()==1)?true:false,
						Integer.valueOf(twoStepsTreatment.getId()));
				return "redirect:/patients/update/" + twoStepsTreatment.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\nОшибка при добавлении информации о двухэтапном лечении. " +
						"Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("twostepstreat", twoStepsTreatment);
			}
		}
		return "redirect:/twoStepsTreatment/" + twoStepsTreatment.getId();
	}

	// show update form
	@RequestMapping(value = "/patients/update/{id}", method = RequestMethod.GET)
	public String showUpdateUserForm(@PathVariable("id") int id, Model model) {
		GeneralPatientInfo gpi = patientService.findById(id);
		model.addAttribute("gpi", gpi);
		List<Drenirovanie> drains = patientService.getDrenirovanie().findPatientAll(String.valueOf(id));
		model.addAttribute("drains", drains);
		List<Punkcia> punkcias = patientService.getPunkcia().findPatientAll(String.valueOf(id));
		model.addAttribute("punkcias", punkcias);
		populateDefaultModel(model);
		List<Editor> editors = patientService.getEditorDAO().findById(id);
		for (Editor editor : editors) {
			switch (Integer.valueOf(editor.getBlockId())) {
				case 0:
					if (gpi.getPatient().getFilledAnalysis0() && gpi.getPatient().getFilledDiagnosisAtStart()
							&& gpi.getPatient().getFilledGeneralInfo() && gpi.getPatient().getFilledObjExam()) {
						editor.setFull(true);
					} else {
						editor.setFull(false);
					}
					patientService.getEditorDAO().update(editor);
					model.addAttribute("editor0", editor);
					break;
				case 1:
					if (gpi.getPatient().getHasSurgery() && gpi.getPatient().getFilledAnalysis1() &&
							gpi.getPatient().getFilledAnalysis5() && gpi.getPatient().getFilledAnalysis10()
							&& gpi.getPatient().getFilledSurgery() || !gpi.getPatient().getHasSurgery() &&
							gpi.getPatient().getFilledAnalysis10()) {
						editor.setFull(true);
					} else {
						editor.setFull(false);
					}
					patientService.getEditorDAO().update(editor);
					model.addAttribute("editor1", editor);
					break;
			}
		}
		List<PatientFileUpload> patientFileUploads = patientFileUploadService.findAllById(id);
		model.addAttribute("files", patientFileUploads);
		List<String> extensions = new ArrayList<String>();
		for (PatientFileUpload fileUpload : patientFileUploads) {
			Pattern pattern = Pattern.compile("\\.(\\w*\\W*)");
			Matcher matcher = pattern.matcher(fileUpload.getFileName());
			if (matcher.find()) {
				extensions.add(matcher.group(matcher.groupCount()));
			} else {
				extensions.add("");
			}
		}
		model.addAttribute("extensions", extensions);
		populateDefaultModel(model);
		return "patient";

	}

	// delete user
	@RequestMapping(value = "/patients/{id}/delete", method = RequestMethod.GET)
	public String deleteUser(@PathVariable("id") int id,
							 final RedirectAttributes redirectAttributes) {
		patientService.delete(id);
		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "Информация о пациенте была удалена!");

		return "redirect:/admin";

	}


	// delete user
	@RequestMapping(value = "/getFile", method = RequestMethod.GET)
	public @ResponseBody byte[] getFile(@RequestParam("id") int id, @RequestParam("fileId") int fileId,
										HttpServletResponse response) throws IOException {
		PatientFileUpload file = patientFileUploadService.findById(id, fileId);
		File resFile = new File(file.getFileName());
		FileUtils.copyInputStreamToFile(file.getFileToUpload(), resFile);
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(file.getFileName(), "UTF-8") + "\"");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength((int) resFile.length());
		try {
			byte[] res = FileUtils.readFileToByteArray(resFile);
			resFile.delete();
			return res;
		} catch (IOException e) {
			return new byte[0];
		}
	}

// show user
	/*
	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String showUser(@PathVariable("id") int id, Model model) {
        Patient patient = patientService.findById(id);
        if (patient == null) {
            model.addAttribute("css", "danger");
            model.addAttribute("msg", "User not found");
        }
        model.addAttribute("patient", patient);

        return "users/show";

    }*/

	private void populateDefaultModel(Model model) {

		List<String> diagnosis = new ArrayList<String>();
		if (properties.getDiagnosisNames().keySet().isEmpty()) {
			properties.installDiagnosis();
		}
		diagnosis.addAll(properties.getDiagnosisNames().values());
		model.addAttribute("diagnosisList", diagnosis);
		//  request.getSession().setAttribute("diagnosisList", diagnosis);
	}

}