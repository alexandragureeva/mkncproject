package com.amgureeva.web.controller;

import com.amgureeva.Entities.Surgeries.Drenirovanie;
import com.amgureeva.binding.PatientBinding.PatientService;
import com.amgureeva.binding.TwoStepsTreatmentBinding.TwoStepsTreatmentService;
import com.amgureeva.binding.UserBinding.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import static com.amgureeva.web.controller.DrenirovanieController.determineSurgeryPage;

/**
 * Created by Nikita on 16.10.2016.
 */
@Controller
public class ComplicationController {

	@Autowired
	private PatientService patientService;

	@Autowired
	private UserService userService;

	@Autowired
	private TwoStepsTreatmentService twoStepsTreatmentService;


	@RequestMapping(value = "/mainComplications/{id}", method = RequestMethod.GET)
	public String showUserMainSurgeryComplicationsForm(@PathVariable("id") int id, Model model) {
		return SurgeryController.determineSurgeryPage(id, model, patientService, userService, twoStepsTreatmentService,
				true);

	}

	@RequestMapping(value = "/surgeryComplications/{id}", method = RequestMethod.GET)
	public String showUserSurgeryComplicationsForm(@PathVariable("id") int id, Model model) {
		model.addAttribute("id", id);
		List<Drenirovanie> drains = patientService.getDrenirovanie().findPatientAll(String.valueOf(id));
		for (Drenirovanie drain : drains) {
			drain.setShowComplications(true);
		}
		model.addAttribute("drains", drains);
		return "surgeries/list_of_surgeries";

	}

	@RequestMapping(value = "/drainComplications/{id}/{drainId}", method = RequestMethod.GET)
	public String showDrainComplicationsForm(@PathVariable("id") int id,@PathVariable("drainId") int drainId,
									Model model) {
		return determineSurgeryPage(id, drainId, model, patientService, userService, true);
	}
}
