package com.amgureeva.web.controller;

import com.amgureeva.Entities.Diagnosis.*;
import com.amgureeva.Entities.Editor;
import com.amgureeva.Entities.GeneralPatientInfo;
import com.amgureeva.Entities.Patient;
import com.amgureeva.Entities.PatientClasses.GenInfoList;
import com.amgureeva.Entities.PatientClasses.ObjectiveExaminationList;
import com.amgureeva.Entities.Surgeries.*;
import com.amgureeva.Entities.User;
import com.amgureeva.Services.PhotoUploadService;
import com.amgureeva.Statistics.*;
import com.amgureeva.binding.AlveokokozBinding.AlveokokkozListCreator;
import com.amgureeva.binding.DrenirovanieBinding.DrenirovanieListCreator;
import com.amgureeva.binding.EchinokokkozBinding.EchinokokkozListCreator;
import com.amgureeva.binding.GCRBinding.GCRListCreator;
import com.amgureeva.binding.HCRBinding.HCRListCreator;
import com.amgureeva.binding.KlatskinBinding.KlatskinListCreator;
import com.amgureeva.binding.MKRBinding.MKRListCreator;
import com.amgureeva.binding.MNKRBinding.MNKRListCreator;
import com.amgureeva.binding.OpAlveokokkozBinding.OpAlveokokkozListCreator;
import com.amgureeva.binding.OpEchinokokkozBinding.OpEchinokokkozListCreator;
import com.amgureeva.binding.OpGCRBinding.OpGCRListCreator;
import com.amgureeva.binding.OpHCRBinding.OpHCRListCreator;
import com.amgureeva.binding.OpKlatskinBinding.OpKlarskinListCreator;
import com.amgureeva.binding.OpMKRBinding.OpMKRListCreator;
import com.amgureeva.binding.OpMNKRBinding.OpMNKRListCreator;
import com.amgureeva.binding.OpRZHPBinding.OpRZHPListCreator;
import com.amgureeva.binding.PatientBinding.PatientService;
import com.amgureeva.binding.PatientBlockInfoBinding.PatientBlockInfoService;
import com.amgureeva.binding.RZHPBinding.RZHPListCreator;
import com.amgureeva.binding.Statistics.StatisticsService;
import com.amgureeva.binding.UserBinding.UserService;
import com.amgureeva.binding.ZHKBBinding.ZHKBListCreator;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

@Controller
public class MainController {

	@Autowired
	ServletContext context;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private PatientService patientService;

	@Autowired
	private PatientBlockInfoService patientBlockInfoService;

	@Autowired
	private UserService userService;

	@Autowired
	private StatisticsService statisticsService;

	@RequestMapping(value = {"/", "/welcome**"}, method = RequestMethod.GET)
	public String defaultPage(Model model, Principal principal) {
		User user = userService.findByEmail(principal.getName());
		model.addAttribute("u", user);
		List<Patient> patients = patientService.findAllPatients();
		model.addAttribute("patients", patients);
		KlatskinListCreator klatskinListCreator = new KlatskinListCreator();
		GCRListCreator gcrListCreator = new GCRListCreator();
		HCRListCreator hcrListCreator = new HCRListCreator();
		MNKRListCreator mnkrListCreator = new MNKRListCreator();
		MKRListCreator mkrListCreator = new MKRListCreator();
		AlveokokkozListCreator alveokokkozListCreator = new AlveokokkozListCreator();
		EchinokokkozListCreator echinokokkozListCreator = new EchinokokkozListCreator();
		RZHPListCreator rzhpListCreator = new RZHPListCreator();
		OpKlarskinListCreator opklatskinListCreator = new OpKlarskinListCreator();
		OpGCRListCreator opgcrListCreator = new OpGCRListCreator();
		OpHCRListCreator ophcrListCreator = new OpHCRListCreator();
		OpMNKRListCreator opmnkrListCreator = new OpMNKRListCreator();
		OpMKRListCreator opmkrListCreator = new OpMKRListCreator();
		OpAlveokokkozListCreator opalveokokkozListCreator = new OpAlveokokkozListCreator();
		OpEchinokokkozListCreator opechinokokkozListCreator = new OpEchinokokkozListCreator();
		OpRZHPListCreator oprzhpListCreator = new OpRZHPListCreator();
		List<KlatskinList> klatskins = klatskinListCreator.createKlatskinList(patientService.getKlatskinDao().findAll(), patientService);
		List<GCRList> gcrc = gcrListCreator.createGCRList(patientService.getGcrDao().findAll(), patientService);
		List<HCRList> hcrc = hcrListCreator.createHCRList(patientService.getHcrDao().findAll(), patientService);
		List<MKRList> mkrs = mkrListCreator.createMKRList(patientService.getMkrdao().findAll(), patientService);
		List<MNKRList> mnkrs = mnkrListCreator.createMNKRList(patientService.getMnkrdao().findAll(), patientService);
		List<EchinokokkozList> echinokokkozs = echinokokkozListCreator.createechLists(patientService.getEchinokokkozDAO().findAll(), patientService);
		List<AlveokokkozList> alveokokkozs = alveokokkozListCreator.createAlveokokkozList(patientService.getAlveokokkozDAO().findAll(), patientService);
		List<RZHPList> rzhps = rzhpListCreator.createRZHPList(patientService.getRzhpdao().findAll(), patientService);
		List<OpKlatskinList> klatskinLists = opklatskinListCreator.createKlatskinLists
				(patientService.getOpKlatskinDao().findAll(), patientService);
		List<OpGCRList> gcrLists = opgcrListCreator.createGcrList(patientService.getOpGcrDao().findAll(),
				patientService);
		List<OpHCRList> hcrLists = ophcrListCreator.createHcrList(patientService.getOpHcrDao().findAll(), patientService);
		List<OpMKRList> mkrLists = opmkrListCreator.createMkrList(patientService.getOpMkrdao().findAll(), patientService);
		List<OpMNKRList> mnkrLists = opmnkrListCreator.createMnkrList(patientService.getOpMnkrdao().findAll(), patientService);
		List<OpEchinokokkozList> echList = opechinokokkozListCreator.createEchList
				(patientService.getOpEchinokokkozDAO().findAll(), patientService);
		List<OpAlveokokkozList> alveokokkozLists = opalveokokkozListCreator.createAlveokokkozList
				(patientService.getOpAlveokokkozDAO().findAll(), patientService);
		List<OpRZHPList> opRZHPLists = oprzhpListCreator.createRzhpList(patientService.getOpRzhpdao().findAll(), patientService);
		model.addAttribute("klatskin", klatskins);
		model.addAttribute("gcr", gcrc);
		model.addAttribute("hcr", hcrc);
		model.addAttribute("echinokokkoz", echinokokkozs);
		model.addAttribute("alveokokkoz", alveokokkozs);
		model.addAttribute("mkr", mkrs);
		model.addAttribute("mnkr", mnkrs);
		model.addAttribute("rzhps", rzhps);
		model.addAttribute("opKlatskin", klatskinLists);
		model.addAttribute("opgcr", gcrLists);
		model.addAttribute("ophcr", hcrLists);
		model.addAttribute("opech", echList);
		model.addAttribute("opalv", alveokokkozLists);
		model.addAttribute("opmkr", mkrLists);
		model.addAttribute("opmnkr", mnkrLists);
		model.addAttribute("oprzhp", opRZHPLists);
		model.addAttribute("dren", new DrenirovanieListCreator().createAlveokokkozList(patientService.getDrenirovanie().findAll(), patientService));
		model.addAttribute("patientsInfo", patientBlockInfoService.getAllNotExpired());
		return "index";

	}

	@RequestMapping(value = "/admin**", method = RequestMethod.GET)
	public String adminPage(Model model, Principal principal) {
		List<User> users = userService.findAll();
		List<Patient> patients = patientService.findAllPatients();
		List<Patient> newPatients = patientService.findAllRecentPatients();
		User user = userService.findByEmail(principal.getName());
		users.remove(user);
		model.addAttribute(user);
		model.addAttribute("users", users);
		model.addAttribute("patients", patients);
		model.addAttribute("newPatients", newPatients);
		Calendar calendar = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		model.addAttribute("today", dateFormat.format(calendar.getTime()));
		calendar.add(Calendar.DATE, -7);
		model.addAttribute("from", dateFormat.format(calendar.getTime()));
		model.addAttribute("unfilledBlocks", patientBlockInfoService.getAll());
		model.addAttribute("msg", model.asMap().get("msg"));
		model.addAttribute("css", model.asMap().get("css"));
		return "new_admin";

	}

	@RequestMapping(value = "/accessBlock/{id}/{blockId}", method = RequestMethod.GET)
	public @ResponseBody String accessBlock(@PathVariable("id") int id, @PathVariable("blockId") int blockId) {
		Editor editor = patientService.getEditorDAO().findBlockEditorById(id, blockId);
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 3);
		editor.setDate(new java.sql.Date(calendar.getTimeInMillis()));
		editor.setExpired(false);
		Patient patient = patientService.findPatientById(id);
		if (patient != null) {
			patient.setClosedDate(calendar.getTime());
			patientService.updateEndInfo(patient);
		}
		patientService.getEditorDAO().update(editor);
		return editor.getFormattedDate();
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
							  @RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("msg", "Неправильная пара email/пароль!");
			model.addObject("css", "danger");
		}

		if (logout != null) {
			model.addObject("msg", "Вы вышли из системы.");
			model.addObject("css", "success");
		}
		model.setViewName("login");

		return model;

	}

	//for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		//check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("name", userDetail.getUsername());
		}

		model.setViewName("403");
		return model;

	}

	@RequestMapping(value = "/edit_account/{id}", method = RequestMethod.GET)
	public String editAccount(@PathVariable("id") int id, Model model) {
		User user = userService.findById(id);
		model.addAttribute("u", user);
		model.addAttribute("msg", model.asMap().get("msg"));
		model.addAttribute("css", model.asMap().get("css"));
		return "edit_account";

	}

	//@RequestParam(value="photo", required = false) MultipartFile uploadedFile
	@RequestMapping(value = "/edit_photo/{id}", method = RequestMethod.POST)
	public RedirectView editPhoto(@PathVariable("id") int id, @RequestParam("Inputfile") MultipartFile file,
								  RedirectAttributes redirectAttrs) {
		PhotoUploadService service = new PhotoUploadService();
		String message = service.execute(request, context, file, id);
		redirectAttrs.addFlashAttribute("msg", message);
		if (message.equals("Фото было изменено!")) {
			redirectAttrs.addFlashAttribute("css", "success");
		} else {
			redirectAttrs.addFlashAttribute("css", "danger");
		}
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		redirectView.setUrl("/edit_account/" + id);
		return redirectView;
	}

	@RequestMapping(value = "/delete_photo/{id}", method = RequestMethod.GET)
	public RedirectView deletePhoto(@PathVariable("id") int id, final RedirectAttributes redirectAttributes) {
		User user = null;
		try {
			user = userService.findById(id);
		} catch (Throwable t) {
			redirectAttributes.addFlashAttribute("msg", "Ошибка при поиске данных о пользователе. Попробуйбе снова.");
			redirectAttributes.addFlashAttribute("css", "danger");
		}
		if (user != null) {
			user.setPhoto("default_icon.png");
			try {
				userService.saveOrUpdate(user);
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", "Ошибка при удалении фотографии. Попробуйбе снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
			}
			redirectAttributes.addFlashAttribute("msg", "Фото было удалено.");
			redirectAttributes.addFlashAttribute("css", "success");
		}
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		redirectView.setUrl("/edit_account/" + id);
		return redirectView;
	}

	@RequestMapping(value = "/edit_profile/{id}", method = RequestMethod.POST)
	public RedirectView editProfile(@PathVariable("id") int id, @RequestParam("fio") String fio,
									@RequestParam("email") String email, final RedirectAttributes redirectAttributes) {
		if (!email.contains("@")) {
			redirectAttributes.addFlashAttribute("msg", "Неправильный адрес электронной почты.");
			redirectAttributes.addFlashAttribute("css", "danger");
		} else {
			User user = null;
			try {
				user = userService.findById(id);
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", "Ошибка при поиске данных о пользователе. Попробуйбе снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
			}
			if (user != null) {
				user.setEmail(email);
				user.setUserName(fio);
				try {
					userService.saveOrUpdate(user);
				} catch (Throwable t) {
					redirectAttributes.addFlashAttribute("msg", "Ошибка при изменении данных. Попробуйбе снова.");
					redirectAttributes.addFlashAttribute("css", "danger");
				}
				redirectAttributes.addFlashAttribute("msg", "Данные были изменены.");
				redirectAttributes.addFlashAttribute("css", "success");
			}
		}
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		redirectView.setUrl("/edit_account/" + id);
		return redirectView;
	}

	@RequestMapping(value = "/new_profile", method = RequestMethod.POST)
	public RedirectView addProfile(@RequestParam("fio") String fio, @RequestParam("email") String email,
								   @RequestParam("password") String password,
								   @RequestParam(value = "isAdmin", required = false) Boolean isAdmin,
								   final RedirectAttributes redirectAttributes) {
		if (!email.contains("@")) {
			redirectAttributes.addFlashAttribute("msg", "Неправильный адрес электронной почты.");
			redirectAttributes.addFlashAttribute("css", "danger");
		} else {
			User prevUser = null;
			try {
				prevUser = userService.findByEmail(email);
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", "Ошибка при создании нового пользователя. Попробуйбе снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
			}
			if (prevUser != null) {
				redirectAttributes.addFlashAttribute("msg", "Пользователь с таким email уже создан!");
				redirectAttributes.addFlashAttribute("css", "danger");
			} else {
				User user = new User(null, fio, email);
				user.setPassword(password);
				if (isAdmin == null) {
					isAdmin = false;
				}
				user.setAdmin(isAdmin);
				try {
					userService.saveOrUpdate(user);
				} catch (Throwable t) {
					redirectAttributes.addFlashAttribute("msg", "Ошибка при создании нового пользователя. Попробуйбе снова.");
					redirectAttributes.addFlashAttribute("css", "danger");
				}
				redirectAttributes.addFlashAttribute("msg", "Новый пользователь с данными: ФИО(" + fio + "), " +
						"email(" + email + ") был создан в системе.");
				redirectAttributes.addFlashAttribute("css", "success");
			}
		}
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		redirectView.setUrl("admin");
		return redirectView;
	}

	@RequestMapping(value = "/delete_profile/{id}", method = RequestMethod.GET)
	public String deleteProfile(@PathVariable("id") int id, final RedirectAttributes redirectAttributes) {

		try {
			userService.delete(id);
		} catch (Throwable t) {
			redirectAttributes.addFlashAttribute("msg", "Ошибка при удалении пользователя. Попробуйбе снова.");
			redirectAttributes.addFlashAttribute("css", "danger");
		}
		redirectAttributes.addFlashAttribute("msg", "Пользователь был удален из системы.");
		redirectAttributes.addFlashAttribute("css", "success");
		return "redirect:/admin";
	}


	@RequestMapping(value = "/change_password/{id}", method = RequestMethod.POST)
	public RedirectView changePassword(@PathVariable("id") int id, @RequestParam("old") String old_pass, @RequestParam("new") String new_pass,
									   @RequestParam("new_conf") String new_pass_conf, final RedirectAttributes redirectAttributes) {
		User user = userService.findById(id);
		if (BCrypt.checkpw(old_pass, user.getHash())) {
			if (new_pass.equals(new_pass_conf)) {
				if (new_pass.length() > 3) {
					user.setPassword(new_pass);
					try {
						userService.saveOrUpdate(user);
					} catch (Throwable t) {
						redirectAttributes.addFlashAttribute("msg", "Ошибка при изменении пароля пользователя." +
								" Попробуйбе снова.");
						redirectAttributes.addFlashAttribute("css", "danger");
					}
					redirectAttributes.addFlashAttribute("msg", "Пароль пользователя был изменен.");
					redirectAttributes.addFlashAttribute("css", "success");

				} else {
					redirectAttributes.addFlashAttribute("msg", "Пароль слишком короткий!");
					redirectAttributes.addFlashAttribute("css", "danger");
				}
			} else {
				redirectAttributes.addFlashAttribute("msg", "Новый пароль не совпадает с подтверждением!");
				redirectAttributes.addFlashAttribute("css", "danger");
			}
		} else {
			redirectAttributes.addFlashAttribute("msg", "Неверный старый пароль!");
			redirectAttributes.addFlashAttribute("css", "danger");
		}
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		redirectView.setUrl("/edit_account/" + id);
		return redirectView;

	}


	@RequestMapping(value = "/help", method = RequestMethod.GET)
	public String showHelp(Model model, Principal principal) {
		User user = userService.findByEmail(principal.getName());
		model.addAttribute("u", user);
	   return "help";
	}

	@RequestMapping(value = "/statistics", method = RequestMethod.GET)
	public ModelAndView setStatistics() {
		List<String> headers = statisticsService.getHeaders();
		List<GeneralPatientInfo> gpis = patientService.findAll();
		ModelAndView model = new ModelAndView();
		model.setViewName("statistics");
		model.addObject("headers", headers);
		model.addObject("gpis", gpis);
		List<String> headerTooltips = new ArrayList<String>(headers.size());
		for (String header : headers) {
			if (header.contains("REZEKCIA_TYPE")) {
				headerTooltips.add("\"1 - \"Лапароскопическая\"; 2 - \"Роботическая\";" +
						" 3 -\"Открытая\"; 4 -\"Гибридная\"");
			}   else if (header.contains("RAZDELEN_TKANEI")) {
				headerTooltips.add("\"1 - \"Зажимом\"; 2 - \"Биполярным коагулятором\";" +
						" 3 -\"Ультразвуковым деструктором\"; 4 -\"Водоструйным деструктором\"");
			}   else
			if (header.contains("alv")) headerTooltips.add(AlveokokkozListCreator.getAllVariants(header));
			else if (header.contains("ech")) headerTooltips.add(EchinokokkozListCreator.getAllVariants(header));
			else if (header.contains("mkr")) headerTooltips.add(MKRListCreator.getAllVariants(header));
			else if (header.contains("mnkr")) headerTooltips.add(MNKRListCreator.getAllVariants(header));
			else if (header.contains("opkl")) headerTooltips.add(KlatskinListCreator.getAllVariants(header));
			else if (header.contains("gcr")) headerTooltips.add(GCRListCreator.getAllVariants(header));
			else if (header.contains("hcr")) headerTooltips.add(HCRListCreator.getAllVariants(header));
			else if (header.contains("rzh")) headerTooltips.add(RZHPListCreator.getAllVariants(header));
			else if (header.contains("zhkb")) headerTooltips.add(ZHKBListCreator.getAllVariants(header));
			else if (header.contains("ДРЕН_")) headerTooltips.add(DrenirovanieListCreator.getAllVariants(header));
			else if (header.contains("ОБ_ОС")) headerTooltips.add(ObjectiveExaminationList.getAllVariants(header));
			else headerTooltips.add(GenInfoList.getAllVariants(header));
		}
		model.addObject("headerTooltips", headerTooltips);
		return model;
	}

	//	@ResponseBody
	@RequestMapping(value = "/descriptiveStatistics", method = RequestMethod.POST)
	public
	@ResponseBody
	String performDescriptiveStatistics(@RequestParam("functions[]") int[] functions,
										@RequestParam("indexes[]") int[] indexes,
										@RequestParam("values") String values) {
		DescStatMethods statMethods = new DescStatMethods();
		return statMethods.setDStatistics(values, indexes, functions);
	}

	@RequestMapping(value = "/spearmanStatistics", method = RequestMethod.POST)
	public
	@ResponseBody
	String performSpearmanStatistics(@RequestParam("indexes[]") int[] indexes,
									 @RequestParam("values") String values) {
		SpearmanMethod statMethods = new SpearmanMethod();
		return statMethods.setSpearman(values, indexes);
	}


	@RequestMapping(value = "/22Statistics", method = RequestMethod.POST)
	public
	@ResponseBody
	String perform2x2Statistics(@RequestParam("values") String values) {
		Table2x2Method table22method = new Table2x2Method();
		return table22method.setValues(values);
	}

	@RequestMapping(value = "/mannWhithneyStatistics", method = RequestMethod.POST)
	public
	@ResponseBody
	String performMannWhitneyStatistics(@RequestParam("values") String values,
										@RequestParam("groupValues") String groupValues,
										@RequestParam("groups") String groups,
										@RequestParam("indexes[]") int[] indexes) {
		MannWhitneyMethod mwmethod = new MannWhitneyMethod();
		return mwmethod.setValues(values, groupValues, indexes, groups);
	}

	@RequestMapping(value = "/getDatesPeriod", method = RequestMethod.GET)
	public
	@ResponseBody
	String getDatesPeriod() {
		String minDate = patientService.getMinDate();
		return Patient.makeRightDate(minDate);
	}

	@RequestMapping(value = "/getDeathDates", method = RequestMethod.POST)
	public
	@ResponseBody
	String getDeathDates(@RequestParam("ids[]") int[] ids,
						 @RequestParam("startDate") String startDate,
						 @RequestParam("endDate") String endDate,
						 @RequestParam("intervals[]") String[] intervals) {
		//  String[] intervals = new String[0];
		List<String> deathDates = (patientService.getDeathDates(ids, startDate, endDate));
		List<String> startDates = (patientService.getStartDates(ids, startDate, endDate));
		for (int i = 0; i < startDates.size(); i++) {
			startDates.set(i, Patient.makeRightDate(startDates.get(i)));
			deathDates.set(i, Patient.makeRightDate(deathDates.get(i)));
		}
		for (int i = 0; i < intervals.length; i++) {
			intervals[i] = Patient.makeRightDate(intervals[i]);
		}
		KaplanMeierMethod kmmethod = new KaplanMeierMethod();
		return kmmethod.setValues(deathDates, startDates, intervals);
	}

	@RequestMapping(value = "/anovaStatistics", method = RequestMethod.POST)
	public
	@ResponseBody
	String performAnovaStatistics(@RequestParam("values") String values,
								  @RequestParam("groupValues") String groupValues,
								  @RequestParam("indexes[]") int[] indexes) {
		AnovaMethod anovaMethod = new AnovaMethod();
		return anovaMethod.setValues(values, groupValues, indexes);
	}

/*
	@RequestMapping(value = "/save_patient", method = RequestMethod.POST)
    public ModelAndView savePatient() {
        String s = request.getParameter("age");
        ModelAndView model = new ModelAndView();
        generalPatientInfo.saveGeneralInfo();
        model.setViewName("new_patient");
        return model;

    }*/

	@RequestMapping(value = "/getExcelFile/{name}", method = RequestMethod.GET)
	public
	@ResponseBody
	byte[] returnExcelFile(@PathVariable("name") String name, HttpServletResponse response) {
		File file = new File(name + ".xlsx");
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setContentLength((int) file.length());
		try {
			byte[] res = FileUtils.readFileToByteArray(file);
			boolean deleted = file.delete();

			return res;
		} catch (IOException e) {
			return new byte[0];
		}
	}

	@RequestMapping(value = "/exportPng", method = RequestMethod.POST)
	public
	@ResponseBody
	String exportPng(@RequestParam("values") String values) {
		String[][][] dArr = new Gson().fromJson(values, String[][][].class);
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Patient Statistics");
		int row = 1;
		int maxColumn = 1;
		FileOutputStream fileOut = null;

		CellStyle style = workbook.createCellStyle();
		style.setFillBackgroundColor(IndexedColors.SKY_BLUE.getIndex());
		Font font = workbook.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setBold(true);
		style.setFont(font);
		String filename = "excelFile" + new Random().nextInt(Integer.MAX_VALUE) + ".xlsx";
		try {
			fileOut = new FileOutputStream(filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if (fileOut != null) {
			for (String[][] dArr2 : dArr) {
				if (dArr2.length == 1 && dArr2[0][0] != null && dArr2[0][0].contains("file")) {

						String imageUrl = "http://export.highcharts.com/" + dArr2[0][0];
						String name = null;
						try {
							name = saveImage(imageUrl);
						} catch (IOException e) {
							e.printStackTrace();
						}
						if (name != null) {
							try {
								InputStream inputStream = new FileInputStream(name);
								byte[] imageBytes = IOUtils.toByteArray(inputStream);
								int pictureureIdx = workbook.addPicture(imageBytes, Workbook.PICTURE_TYPE_PNG);
								inputStream.close();
								File file = new File(name);
								boolean deleted = file.delete();
								CreationHelper helper = workbook.getCreationHelper();
								Drawing drawing = sheet.createDrawingPatriarch();
								ClientAnchor anchor = helper.createClientAnchor();
								anchor.setCol1(1);
								anchor.setRow1(row); // same row is okay
								anchor.setRow2(row + 20);
								anchor.setCol2(6);
								drawing.createPicture(anchor, pictureureIdx);
								row += 11;
								//		workbook.write(fileOut);
							} catch (Exception e) {
								e.printStackTrace();
							}

					}
				} else {
					for (int k = 0; k < dArr2.length; k++) {
						Row r = sheet.createRow(row++);
						int cellnum = 1;
						for (String val : dArr2[k]) {
							Cell cell = r.createCell(cellnum++);
							if (!val.trim().isEmpty() && StringUtils.isNumeric(val.trim())) {
								cell.setCellValue(new Double(val.trim()));
							}
							cell.setCellValue(val.trim());
							if (k == 0) {
								cell.setCellStyle(style);
							}
						}
						if (cellnum > maxColumn) {
							maxColumn = cellnum;
						}
					}
					row++;
					//workbook.write();
				}
			}
			try {
				for (int i = 1; i <= maxColumn; i++) {
					sheet.autoSizeColumn(i);
				}
				workbook.write(fileOut);
				fileOut.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return filename;
	}

	public String saveImage(String imageUrl) throws IOException {
		URL url = new URL(imageUrl);
		URLConnection urlConnection = url.openConnection();
		urlConnection.setRequestProperty("User-Agent", "NING/1.0");
		InputStream inputStream = urlConnection.getInputStream();
		String name = "image" + new Random().nextInt(Integer.MAX_VALUE) + ".png";
		ImageIO.write(ImageIO.read(inputStream), "png", new File(name));
		return name;
	}


}