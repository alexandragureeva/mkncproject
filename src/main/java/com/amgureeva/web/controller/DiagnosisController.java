package com.amgureeva.web.controller;

import com.amgureeva.Entities.Diagnosis.*;
import com.amgureeva.Entities.Editor;
import com.amgureeva.Entities.Patient;
import com.amgureeva.Entities.User;
import com.amgureeva.binding.PatientBinding.PatientService;
import com.amgureeva.binding.UserBinding.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;
import java.util.List;

@Controller
public class DiagnosisController {

	@Autowired
	private PatientService patientService;


	@Autowired
	private UserService userService;

	@Autowired
	public void setUserService(PatientService patientService) {
		this.patientService = patientService;
	}

/*
	@Autowired
	AlviokokkozValidator alviokokkozValidator;*/

	/*//Set a form validator
	@InitBinder("alviokokkoz")
	protected void initPatientBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.setValidator(alviokokkozValidator);
	}*/

	@RequestMapping(value = "/diagnosis/start/{id}", method = RequestMethod.GET)
	public String showUserDiagnosisAtStartForm(@PathVariable("id") int id, Model model) {
		return constructDiagnosisModel(id, model, true);
	}

	@RequestMapping(value = "/diagnosis/end/{id}", method = RequestMethod.GET)
	public String showUserDiagnosisAtEndForm(@PathVariable("id") int id, Model model) {
		return constructDiagnosisModel(id, model, false);
	}

	private String constructDiagnosisModel(int id, Model model, boolean isAtStart) {
		Patient patient = patientService.findPatientById(id);
		model.addAttribute("expired", false);
		List<Editor> editors =
				patientService.getEditorDAO().findById(id);
		for (Editor e : editors) {
			if (isAtStart && e.getBlockId().equals("0") && e.isExpired()) {
				model.addAttribute("expired", true);
			} else if (!isAtStart && e.getBlockId().equals("1") && e.isExpired()) {
				model.addAttribute("expired", true);
			}
		}
		Calendar c = Calendar.getInstance();
		if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
			model.addAttribute("expired", true);
		}
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userService.findByEmail(name);
		if (user.isAdmin()) {
			model.addAttribute("isAdmin", true);
		} else {
			model.addAttribute("isAdmin", false);
		}
		switch (patient.getDiagnosis()) {
			case 1:
				Klatskin klatskin = patientService.getKlatskinDao().findById(id, isAtStart);
				if (model.asMap().get("klatskin") != null) {
					model.addAttribute("klatskin", model.asMap().get("klatskin"));
					model.addAttribute("org.springframework.validation.BindingResult.klatskin",
							model.asMap().get("org.springframework.validation.BindingResult.klatskin"));
				} else {
					if (klatskin == null) {
						klatskin = new Klatskin();
						klatskin.setDiagnosisAtStart(isAtStart);
						klatskin.setIsNew(true);
						klatskin.setId(String.valueOf(id));
					}
					model.addAttribute("klatskin", klatskin);
				}
				return "diagnosis/klatskin";
			case 2:
				GCR gcr = patientService.getGcrDao().findById(id, isAtStart);
				if (model.asMap().get("gcr") != null) {
					model.addAttribute("gcr", model.asMap().get("gcr"));
					model.addAttribute("org.springframework.validation.BindingResult.gcr",
							model.asMap().get("org.springframework.validation.BindingResult.gcr"));
				} else {
					if (gcr == null) {
						gcr = new GCR();
						gcr.setDiagnosisAtStart(isAtStart);
						gcr.setIsNew(true);
						gcr.setId(String.valueOf(id));
					}
					model.addAttribute("gcr", gcr);
				}
				return "diagnosis/gcr";
			case 3:
				HCR hcr = patientService.getHcrDao().findById(id, isAtStart);
				if (model.asMap().get("hcr") != null) {
					model.addAttribute("hcr", model.asMap().get("hcr"));
					model.addAttribute("org.springframework.validation.BindingResult.hcr",
							model.asMap().get("org.springframework.validation.BindingResult.hcr"));
				} else {
					if (hcr == null) {
						hcr = new HCR();
						hcr.setDiagnosisAtStart(isAtStart);
						hcr.setIsNew(true);
						hcr.setId(String.valueOf(id));
					}
					model.addAttribute("hcr", hcr);
				}
				return "diagnosis/hcr";
			case 4:
				MKR mkr = patientService.getMkrdao().findById(id, isAtStart);
				if (model.asMap().get("mkr") != null) {
					model.addAttribute("mkr", model.asMap().get("mkr"));
					model.addAttribute("org.springframework.validation.BindingResult.mkr",
							model.asMap().get("org.springframework.validation.BindingResult.mkr"));
				} else {
					if (mkr == null) {
						mkr = new MKR();
						mkr.setDiagnosisAtStart(isAtStart);
						mkr.setIsNew(true);
						mkr.setId(String.valueOf(id));
					}
					model.addAttribute("mkr", mkr);
				}
				return "diagnosis/mkr";
			case 5:
				MNKR mnkr = patientService.getMnkrdao().findById(id, isAtStart);
				if (model.asMap().get("mnkr") != null) {
					model.addAttribute("mnkr", model.asMap().get("mnkr"));
					model.addAttribute("org.springframework.validation.BindingResult.mnkr",
							model.asMap().get("org.springframework.validation.BindingResult.mnkr"));
				} else {
					if (mnkr == null) {
						mnkr = new MNKR();
						mnkr.setDiagnosisAtStart(isAtStart);
						mnkr.setIsNew(true);
						mnkr.setId(String.valueOf(id));
					}
					model.addAttribute("mnkr", mnkr);
				}
				return "diagnosis/mnkr";
			case 6:
				RZHP rzhp = patientService.getRzhpdao().findById(id, isAtStart);
				if (model.asMap().get("rzhp") != null) {
					model.addAttribute("rzhp", model.asMap().get("rzhp"));
					model.addAttribute("org.springframework.validation.BindingResult.rzhp",
							model.asMap().get("org.springframework.validation.BindingResult.rzhp"));
				} else {
					if (rzhp == null) {
						rzhp = new RZHP();
						rzhp.setDiagnosisAtStart(isAtStart);
						rzhp.setIsNew(true);
						rzhp.setId(String.valueOf(id));
					}
					model.addAttribute("rzhp", rzhp);
				}
				return "diagnosis/rzhp";
			case 7:
				Echinokokkoz echinokokkoz = patientService.getEchinokokkozDAO().findById(id, isAtStart);
				if (model.asMap().get("echinokokkoz") != null) {
					model.addAttribute("echinokokkoz", model.asMap().get("echinokokkoz"));
					model.addAttribute("org.springframework.validation.BindingResult.echinokokkoz",
							model.asMap().get("org.springframework.validation.BindingResult.echinokokkoz"));
				} else {
					if (echinokokkoz == null) {
						echinokokkoz = new Echinokokkoz();
						echinokokkoz.setDiagnosisAtStart(isAtStart);
						echinokokkoz.setIsNew(true);
						echinokokkoz.setId(String.valueOf(id));
					}
					model.addAttribute("echinokokkoz", echinokokkoz);
				}
				return "diagnosis/echinokokkoz";
			case 8:
				Alveokokkoz alveokokkoz = patientService.getAlveokokkozDAO().findById(id, isAtStart);
				if (model.asMap().get("alviokokkoz") != null) {
					model.addAttribute("alviokokkoz", model.asMap().get("alviokokkoz"));
					model.addAttribute("org.springframework.validation.BindingResult.alviokokkoz",
							model.asMap().get("org.springframework.validation.BindingResult.alviokokkoz"));
				} else {
					if (alveokokkoz == null) {
						alveokokkoz = new Alveokokkoz();
						alveokokkoz.setDiagnosisAtStart(isAtStart);
						alveokokkoz.setIsNew(true);
						alveokokkoz.setId(String.valueOf(id));
					}
					model.addAttribute("alviokokkoz", alveokokkoz);
				}
				return "diagnosis/alveokokkoz";
			case 9:
				ZHKB zhkb = patientService.getZhhkbdao().findById(id, isAtStart);
				if (model.asMap().get("zhkb") != null) {
					model.addAttribute("zhkb", model.asMap().get("zhkb"));
					model.addAttribute("org.springframework.validation.BindingResult.zhkb",
							model.asMap().get("org.springframework.validation.BindingResult.zhkb"));
				} else {
					if (zhkb == null) {
						zhkb = new ZHKB();
						zhkb.setDiagnosisAtStart(isAtStart);
						zhkb.setIsNew(true);
						zhkb.setId(String.valueOf(id));
					}
					model.addAttribute("zhkb", zhkb);
				}
				return "diagnosis/zhkb";
		}
		return "redirect:/patients/update/" + id;
	}


	@RequestMapping(value = "/saveAlviokokkoz", method = RequestMethod.POST)
	public String submitAlviokokozForm(@ModelAttribute("alviokokkoz")
									   @Validated Alveokokkoz alviokokkoz,
									   BindingResult alvResult,
									   final RedirectAttributes redirectAttributes) {
		if (alvResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("alviokokkoz", alviokokkoz);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.alviokokkoz", alvResult);
			if (alviokokkoz.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + alviokokkoz.getId();
			} else {
				return "redirect:/diagnosis/end/" + alviokokkoz.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"Альвиококкоз\" были успешно загружены!");
			try {
				if (alviokokkoz.getIsNew()) {
					patientService.getAlveokokkozDAO().save(alviokokkoz);
				} else {
					patientService.getAlveokokkozDAO().update(alviokokkoz);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(alviokokkoz.getId()));
				if (alviokokkoz.areAllFieldsFilled()) {
					if (alviokokkoz.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				} else {
					if (alviokokkoz.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + alviokokkoz.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"Алвиококкоз\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("alviokokkoz", alviokokkoz);
			}
		}
		if (alviokokkoz.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + alviokokkoz.getId();
		} else {
			return "redirect:/diagnosis/end/" + alviokokkoz.getId();
		}
	}

	@RequestMapping(value = "/saveEchinokokkoz", method = RequestMethod.POST)
	public String submitEchinokokkozForm(@ModelAttribute("echinokokkoz")
										 @Validated Echinokokkoz echinokokkoz,
										 BindingResult echResult,
										 final RedirectAttributes redirectAttributes) {
		if (echResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("echinokokkoz", echinokokkoz);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.echinokokkoz", echResult);
			if (echinokokkoz.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + echinokokkoz.getId();
			} else {
				return "redirect:/diagnosis/end/" + echinokokkoz.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"Эхинококкоз\" были успешно загружены!");
			try {
				if (echinokokkoz.getIsNew()) {
					patientService.getEchinokokkozDAO().save(echinokokkoz);
				} else {
					patientService.getEchinokokkozDAO().update(echinokokkoz);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(echinokokkoz.getId()));
				if (echinokokkoz.areAllFieldsFilled()) {
					if (echinokokkoz.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				}  else {
					if (echinokokkoz.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + echinokokkoz.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"Эхинококкоз\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("echinokokkoz", echinokokkoz);
			}
		}
		if (echinokokkoz.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + echinokokkoz.getId();
		} else {
			return "redirect:/diagnosis/end/" + echinokokkoz.getId();
		}
	}

	@RequestMapping(value = "/saveGcr", method = RequestMethod.POST)
	public String submitGcrForm(@ModelAttribute("gcr") @Validated GCR gcr, BindingResult gcrResult,
								final RedirectAttributes redirectAttributes) {
		if (gcrResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("gcr", gcr);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gcr", gcrResult);
			if (gcr.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + gcr.getId();
			} else {
				return "redirect:/diagnosis/end/" + gcr.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"ГЦР\" были успешно загружены!");
			try {
				if (gcr.getIsNew()) {
					patientService.getGcrDao().save(gcr);
				} else {
					patientService.getGcrDao().update(gcr);
				}

				Patient p = patientService.findPatientById(Integer.valueOf(gcr.getId()));
				if (gcr.areAllFieldsFilled()) {
					if (gcr.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				}  else {
					if (gcr.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + gcr.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"ГЦР\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("gcr", gcr);
			}
		}
		if (gcr.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + gcr.getId();
		} else {
			return "redirect:/diagnosis/end/" + gcr.getId();
		}
	}

	@RequestMapping(value = "/saveHcr", method = RequestMethod.POST)
	public String submitHcrForm(@ModelAttribute("hcr") @Validated HCR hcr, BindingResult hcrResult,
								final RedirectAttributes redirectAttributes) {
		if (hcrResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("hcr", hcr);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.hcr", hcrResult);
			if (hcr.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + hcr.getId();
			} else {
				return "redirect:/diagnosis/end/" + hcr.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"ХЦР\" были успешно загружены!");
			try {
				if (hcr.getIsNew()) {
					patientService.getHcrDao().save(hcr);
				} else {
					patientService.getHcrDao().update(hcr);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(hcr.getId()));
				if (hcr.areAllFieldsFilled()) {
					if (hcr.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				}  else {
					if (hcr.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + hcr.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"ХЦР\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("hcr", hcr);
			}
		}
		if (hcr.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + hcr.getId();
		} else {
			return "redirect:/diagnosis/end/" + hcr.getId();
		}
	}

	@RequestMapping(value = "/saveKlatskin", method = RequestMethod.POST)
	public String submitKlatskinForm(@ModelAttribute("klatskin") @Validated Klatskin klatskin,
									 BindingResult klatskinResult,
									 final RedirectAttributes redirectAttributes) {
		if (klatskinResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("klatskin", klatskin);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.klatskin", klatskinResult);
			if (klatskin.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + klatskin.getId();
			} else {
				return "redirect:/diagnosis/end/" + klatskin.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"Опухоль Клацкина\" были успешно загружены!");
			try {
				if (klatskin.getIsNew()) {
					patientService.getKlatskinDao().save(klatskin);
				} else {
					patientService.getKlatskinDao().update(klatskin);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(klatskin.getId()));
				if (klatskin.areAllFieldsFilled()) {
					if (klatskin.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				}   else {
					if (klatskin.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + klatskin.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"Опухоль Клацкина\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("klatskin", klatskin);
			}
		}
		if (klatskin.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + klatskin.getId();
		} else {
			return "redirect:/diagnosis/end/" + klatskin.getId();
		}
	}


	@RequestMapping(value = "/saveMkr", method = RequestMethod.POST)
	public String submitMKRForm(@ModelAttribute("mkr") @Validated MKR mkr,
								BindingResult mkrResult,
								final RedirectAttributes redirectAttributes) {
		if (mkrResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("mkr", mkr);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.mkr", mkrResult);
			if (mkr.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + mkr.getId();
			} else {
				return "redirect:/diagnosis/end/" + mkr.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"МКР\" были успешно загружены!");
			try {
				if (mkr.getIsNew()) {
					patientService.getMkrdao().save(mkr);
				} else {
					patientService.getMkrdao().update(mkr);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(mkr.getId()));
				if (mkr.areAllFieldsFilled()) {
					if (mkr.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				}   else {
					if (mkr.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + mkr.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"МКР\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("mkr", mkr);
			}
		}
		if (mkr.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + mkr.getId();
		} else {
			return "redirect:/diagnosis/end/" + mkr.getId();
		}
	}

	@RequestMapping(value = "/saveMnkr", method = RequestMethod.POST)
	public String submitMNKRForm(@ModelAttribute("mnkr") @Validated MNKR mnkr,
								 BindingResult mnkrResult,
								 final RedirectAttributes redirectAttributes) {
		if (mnkrResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("mnkr", mnkr);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.mnkr", mnkrResult);
			if (mnkr.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + mnkr.getId();
			} else {
				return "redirect:/diagnosis/end/" + mnkr.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"МНКР\" были успешно загружены!");
			try {
				if (mnkr.getIsNew()) {
					patientService.getMnkrdao().save(mnkr);
				} else {
					patientService.getMnkrdao().update(mnkr);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(mnkr.getId()));
				if (mnkr.areAllFieldsFilled()) {
					if (mnkr.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				}  else {
					if (mnkr.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + mnkr.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"МНКР\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("mnkr", mnkr);
			}
		}
		if (mnkr.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + mnkr.getId();
		} else {
			return "redirect:/diagnosis/end/" + mnkr.getId();
		}
	}

	@RequestMapping(value = "/saveRzhp", method = RequestMethod.POST)
	public String submitRZHPForm(@ModelAttribute("rzhp") @Validated RZHP rzhp,
								 BindingResult rzhpResult,
								 final RedirectAttributes redirectAttributes) {
		if (rzhpResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("rzhp", rzhp);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.rzhp", rzhpResult);
			if (rzhp.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + rzhp.getId();
			} else {
				return "redirect:/diagnosis/end/" + rzhp.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"Рак желудка\" были успешно загружены!");
			try {
				if (rzhp.getIsNew()) {
					patientService.getRzhpdao().save(rzhp);
				} else {
					patientService.getRzhpdao().update(rzhp);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(rzhp.getId()));
				if (rzhp.areAllFieldsFilled()) {
					if (rzhp.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				}         else {
					if (rzhp.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + rzhp.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"Рак желудка\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("rzhp", rzhp);
			}
		}
		if (rzhp.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + rzhp.getId();
		} else {
			return "redirect:/diagnosis/end/" + rzhp.getId();
		}
	}

	@RequestMapping(value = "/saveZhkb", method = RequestMethod.POST)
	public String submitRZHKBForm(@ModelAttribute("zhkb") @Validated ZHKB zhkb,
								 BindingResult zhkbResult,
								 final RedirectAttributes redirectAttributes) {
		if (zhkbResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("zhkb", zhkb);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.zhkb", zhkbResult);
			if (zhkb.isDiagnosisAtStart()) {
				return "redirect:/diagnosis/start/" + zhkb.getId();
			} else {
				return "redirect:/diagnosis/end/" + zhkb.getId();
			}
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по диагнозу \"Желчнокаменная болезнь\" были успешно загружены!");
			try {
				if (zhkb.getIsNew()) {
					patientService.getZhhkbdao().save(zhkb);
				} else {
					patientService.getZhhkbdao().update(zhkb);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(zhkb.getId()));
				if (zhkb.areAllFieldsFilled()) {
					if (zhkb.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(true);
					else
						p.setFilledDiagnosisAtEnd(true);
				}         else {
					if (zhkb.isDiagnosisAtStart())
						p.setFilledDiagnosisAtStart(false);
					else
						p.setFilledDiagnosisAtEnd(false);
				}
				patientService.updateFillers(p);
				if (p.getFilledDiagnosisAtEnd() && p.getFilledAnalysis10() && p.getEndDate() != null &&
						!p.getEndDate().isEmpty() ||
						p.getFilledDiagnosisAtStart() && p.getFilledAnalysis0()
								&& p.getFilledGeneralInfo() && p.getFilledObjExam()) {
				}
				return "redirect:/patients/update/" + zhkb.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по диагнозу" +
						" \"Желчнокаменная болезнь\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("zhkb", zhkb);
			}
		}
		if (zhkb.isDiagnosisAtStart()) {
			return "redirect:/diagnosis/start/" + zhkb.getId();
		} else {
			return "redirect:/diagnosis/end/" + zhkb.getId();
		}
	}
}
