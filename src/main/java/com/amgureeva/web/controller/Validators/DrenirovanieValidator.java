package com.amgureeva.web.controller.Validators;

import com.amgureeva.Entities.Surgeries.Drenirovanie;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Nikita on 19.08.2016.
 */
public class DrenirovanieValidator implements Validator {
	@Override
	public boolean supports(Class<?> aClass) {
		return Drenirovanie.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Drenirovanie drenirovanie = (Drenirovanie) o;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "NotEmpty.drenirovanie.date");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "inout_drenages", "NotEmpty.drenirovanie.inout_drenages");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "out_drenages", "NotEmpty.drenirovanie.out_drenages");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "longevity", "NotEmpty.drenirovanie.longevity");
		if (drenirovanie.getBlock() == 0) {
			errors.rejectValue("block", "NotEmpty.drenirovanie.block");
		}
		if (drenirovanie.getDolya() == 0) {
			errors.rejectValue("dolya", "NotEmpty.drenirovanie.dolya");
		}
	}
}