package com.amgureeva.web.controller.Validators;

import com.amgureeva.Entities.Analysis;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Nikita on 18.08.2016.
 */
public class AnalysisValidator implements Validator {
	@Override
	public boolean supports(Class<?> aClass) {
		return Analysis.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Analysis analysis = (Analysis) o;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "gemoglabin", "NotEmpty.analysis.gemoglabin");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eritrociti", "NotEmpty.analysis.eritrociti");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "leikociti", "NotEmpty.analysis.leikociti");
		if (analysis.getDay() != 0) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "palochkoyadernaya",
					"NotEmpty.analysis.palochkoyadernaya");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "yunie", "NotEmpty.analysis.yunie");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "monociti", "NotEmpty.analysis.monociti");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "limfociti", "NotEmpty.analysis.limfociti");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "trombociti", "NotEmpty.analysis.trombociti");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "soe", "NotEmpty.analysis.soe");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "segmentarnoyadernaya",
					"NotEmpty.analysis.segmentarnoyadernaya");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ph", "NotEmpty.analysis.ph");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "belok", "NotEmpty.analysis.belok");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "leikocitiMochi", "NotEmpty.analysis.leikocitiMochi");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "eritrocitiMochi", "NotEmpty.analysis.eritrocitiMochi");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "glukoza", "NotEmpty.analysis.glukoza");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ketoni", "NotEmpty.analysis.ketoni");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "albunin", "NotEmpty.analysis.albunin");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "obshiibilirubin", "NotEmpty.analysis.obshiibilirubin");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ast", "NotEmpty.analysis.ast");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "alt", "NotEmpty.analysis.alt");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "obshiiBelok", "NotEmpty.analysis.obshiiBelok");

		if (analysis.getOnk199() != null && analysis.getOnk199()  ==1 )
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "onk199Value", "NotEmpty.analysis.onk199Value");
		if (analysis.getCea() != null && analysis.getCea()  ==1 )
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ceaValue", "NotEmpty.analysis.ceaValue");
		if (analysis.getAlfoFetoprotein() != null && analysis.getAlfoFetoprotein()  ==1 )
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "alfoFetValue", "NotEmpty.analysis.alfoFetoproteinValue");
		if (analysis.getCa125() != null && analysis.getCa125()  ==1 )
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ca125Value", "NotEmpty.analysis.ca125Value");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"pryamoibilirubin", "NotEmpty.analysis.pryamoibilirubin");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "kreatinin", "NotEmpty.analysis.kreatinin");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"sacharKrovi", "NotEmpty.analysis.sacharKrovi");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"mochevayaKislota", "NotEmpty.analysis.mochevayaKislota");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"mochevina", "NotEmpty.analysis.mochevina");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mno", "NotEmpty.analysis.mno");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "achtv", "NotEmpty.analysis.achtv");
		if (analysis.getDay() == 0) {
			if (analysis.getGruppaKrovi() == null || analysis.getGruppaKrovi() == 0) {
				errors.rejectValue("gruppaKrovi", "NotEmpty.analysis.gruppaKrovi");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rezFactor",
						"NotEmpty.analysis.rezFactor");
			}
			if (analysis.getRezFactor() == 0) {
				errors.rejectValue( "rezFactor", "NotEmpty.analysis.rezFactor");
			}
		}
	}
}