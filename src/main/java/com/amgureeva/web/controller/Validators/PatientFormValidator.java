package com.amgureeva.web.controller.Validators;

import com.amgureeva.Entities.Patient;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Александра on 11.04.2016.
 */
public class PatientFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Patient.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Patient patient = (Patient) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.userForm.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthdate", "NotEmpty.userForm.birthdate");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "startDate", "NotEmpty.userForm.startDate");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "diseaseNumber","NotEmpty.userForm.diseaseNumber");
        if (patient.getDiagnosis() == 0) {
            errors.rejectValue("diagnosis", "NotEmpty.userForm.diagnosis");
        }
        if (patient.getHasSurgery()) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surgeryDate", "NotEmpty.userForm.surgeryDate");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sex", "NotEmpty.userForm.sex");
    }
}
