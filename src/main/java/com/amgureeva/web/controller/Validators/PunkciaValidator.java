package com.amgureeva.web.controller.Validators;

import com.amgureeva.Entities.Surgeries.Punkcia;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Nikita on 28.09.2016.
 */
public class PunkciaValidator  implements Validator {
	@Override
	public boolean supports(Class<?> aClass) {
		return Punkcia.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Punkcia punkcia = (Punkcia) o;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "NotEmpty.punkcia.date");
		if (punkcia.getType() == 0) {
			errors.rejectValue("type", "NotEmpty.punkcia.type");
		}
		if (punkcia.getOrgan() == 0) {
			errors.rejectValue("organ", "NotEmpty.punkcia.organ");
		}
	}
}