package com.amgureeva.web.controller.Validators;

import com.amgureeva.Entities.Diagnosis.Klatskin;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Александра on 22.04.2016.
 */
public class KlatskinFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Klatskin.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "indexZachvata", "NotEmpty.userForm.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "objemOstatka", "NotEmpty.userForm.name");
    }
}