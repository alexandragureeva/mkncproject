package com.amgureeva.web.controller.Validators;

import com.amgureeva.Entities.ObjectiveExamination;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Nikita on 17.08.2016.
 */
public class ObjExaminationValidator implements Validator {
	@Override
	public boolean supports(Class<?> aClass) {
		return ObjectiveExamination.class.equals(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ObjectiveExamination objExamination = (ObjectiveExamination) o;
		if (objExamination.getPatientState() == 0) {
			errors.rejectValue("patientState", "WrongNumber.objectiveExamination.patientState");
		}
		if (objExamination.getTemperature() == 0) {
			errors.rejectValue("temperature", "WrongNumber.objectiveExamination.temperature");
		}
		if (objExamination.getOdishka() == 0) {
			errors.rejectValue("odishka", "WrongNumber.objectiveExamination.odishka");
		}
		if (objExamination.getPuls() == 0) {
			errors.rejectValue("puls", "WrongNumber.objectiveExamination.puls");
		}
		if (objExamination.getTachikardia() == 0) {
			errors.rejectValue("tachikardia", "WrongNumber.objectiveExamination.tachikardia");
		}
		if (objExamination.getRvota() == 0) {
			errors.rejectValue("rvota", "WrongNumber.objectiveExamination.rvota");
		}
		if (objExamination.getZhivot() == 0) {
			errors.rejectValue("zhivot", "WrongNumber.objectiveExamination.zhivot");
		}
		if (objExamination.getPeristaltika() == 0) {
			errors.rejectValue("peristaltika", "WrongNumber.objectiveExamination.peristaltika");
		}
		if (objExamination.getStul() == 0) {
			errors.rejectValue("stul", "WrongNumber.objectiveExamination.stul");
		}
		if (objExamination.getObjemMochi() == 0) {
			errors.rejectValue("objemMochi", "WrongNumber.objectiveExamination.objemMochi");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "davlenie", "NotEmpty.objectiveExamination.davlenie");
	}
}
