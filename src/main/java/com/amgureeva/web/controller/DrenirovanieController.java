package com.amgureeva.web.controller;

import com.amgureeva.Entities.Patient;
import com.amgureeva.Entities.Surgeries.Drenirovanie;
import com.amgureeva.Entities.Surgeries.Punkcia;
import com.amgureeva.Entities.User;
import com.amgureeva.binding.PatientBinding.PatientService;
import com.amgureeva.binding.UserBinding.UserService;
import com.amgureeva.web.controller.Validators.DrenirovanieValidator;
import com.amgureeva.web.controller.Validators.PunkciaValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Nikita on 19.08.2016.
 */
@Controller
public class DrenirovanieController {

	@Autowired
	private PatientService patientService;

	@Autowired
	private UserService userService;

	@Autowired
	public void setUserService(PatientService patientService) {
		this.patientService = patientService;
	}


	@Autowired
	DrenirovanieValidator drenirovanieValidator;
	@Autowired
	PunkciaValidator punkciaValidator;

	@InitBinder("drenirovanie")
	protected void initDrenirovanieBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.setValidator(drenirovanieValidator);
	}


	@InitBinder("punkcia")
	protected void initPunkciaBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		binder.setValidator(punkciaValidator);
	}

	@RequestMapping(value = "/drain/{id}", method = RequestMethod.GET)
	public String showUserDrainForm(@PathVariable("id") int id, Model model,
									final RedirectAttributes redirectAttributes) {
		if (model.asMap().get("drenirovanie") != null) {
			model.addAttribute("drenirovanie", model.asMap().get("drenirovanie"));
			model.addAttribute("org.springframework.validation.BindingResult.drenirovanie",
					model.asMap().get("org.springframework.validation.BindingResult.drenirovanie"));
		} else {
			Drenirovanie drenirovanie = new Drenirovanie();
			drenirovanie.setId(String.valueOf(id));
			drenirovanie.setIsNew(true);
			model.addAttribute("drenirovanie", drenirovanie);
		}
		Patient patient = patientService.findPatientById(id);
		Calendar c = Calendar.getInstance();
		if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
			redirectAttributes.addFlashAttribute("msg", "Невозможно добавить информацию по дренированию; " +
					"время редактирования пацинета истекло.");
			redirectAttributes.addFlashAttribute("css", "danger");
			return "redirect:/patients/update/" + id;
		}
		return "surgeries/drenirovanie";
	}

	@RequestMapping(value = "/drain/{id}/{drainId}", method = RequestMethod.GET)
	public String showUserDrainForm(@PathVariable("id") int id,@PathVariable("drainId") int drainId,
									Model model) {
		return determineSurgeryPage(id, drainId, model, patientService, userService, false);
	}


	@RequestMapping(value = "/punkcia/{id}", method = RequestMethod.GET)
	public String showUserPunkcianForm(@PathVariable("id") int id, Model model,
									final RedirectAttributes redirectAttributes) {
		if (model.asMap().get("punkcia") != null) {
			model.addAttribute("punkcia", model.asMap().get("punkcia"));
			model.addAttribute("org.springframework.validation.BindingResult.punkcia",
					model.asMap().get("org.springframework.validation.BindingResult.punkcia"));
		} else {
			Punkcia punkcia = new Punkcia();
			punkcia.setId(String.valueOf(id));
			punkcia.setIsNew(true);
			model.addAttribute("punkcia", punkcia);
		}
		Patient patient = patientService.findPatientById(id);
		Calendar c = Calendar.getInstance();
		if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
			redirectAttributes.addFlashAttribute("msg", "Невозможно добавить информацию по дренированию; " +
					"время редактирования пацинета истекло.");
			redirectAttributes.addFlashAttribute("css", "danger");
			return "redirect:/patients/update/" + id;
		}
		return "surgeries/punkcia";
	}


	@RequestMapping(value = "/punkcia/{id}/{drainId}", method = RequestMethod.GET)
	public String showUserPunkciaForm(@PathVariable("id") int id,@PathVariable("drainId") int drainId,
									Model model) {
		if (model.asMap().get("punkcia") != null) {
			model.addAttribute("punkcia", model.asMap().get("punkcia"));
			model.addAttribute("org.springframework.validation.BindingResult.punkcia",
					model.asMap().get("org.springframework.validation.BindingResult.punkcia"));
		} else {
			Punkcia punkcia = patientService.getPunkcia().findById(id, drainId);
			if (punkcia == null) {
				return "redirect:/punkcia/" + id;
			}
			model.addAttribute("punkcia", punkcia);
		}
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userService.findByEmail(name);
		if (user.isAdmin()) {
			model.addAttribute("isAdmin", true);
		} else {
			model.addAttribute("isAdmin", false);
		}
		model.addAttribute("expired", false);
		Patient patient = patientService.findPatientById(id);
		Calendar c = Calendar.getInstance();
		if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
			model.addAttribute("expired", true);
		}

		return "surgeries/punkcia";
	}
	
	@RequestMapping(value = "/saveDrenirovanie", method = RequestMethod.POST)
	public String submitDrenirovanieForm(@ModelAttribute("drenirovanie")
									   @Validated Drenirovanie drenirovanie,
									   BindingResult drenResult,
									   final RedirectAttributes redirectAttributes) {
		if (drenResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("drenirovanie", drenirovanie);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.drenirovanie", drenResult);
			if (drenirovanie.getIsNew())
				return "redirect:/drain/"+ drenirovanie.getId();
			else
				return "redirect:/drain/"+ drenirovanie.getId() + "/" + drenirovanie.getDrenId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по дренированию были успешно загружены!");
			try {
				if (drenirovanie.getIsNew()) {
					List<Drenirovanie> drains= patientService.getDrenirovanie().findPatientAll(drenirovanie.getId());
					drenirovanie.setDrenId((drains==null)?"0":String.valueOf(drains.size()));
					patientService.getDrenirovanie().save(drenirovanie);
				} else {
					patientService.getDrenirovanie().update(drenirovanie);
				}
				return "redirect:/patients/update/" + drenirovanie.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по дренированию" +
						" . Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("drenirovanie", drenirovanie);
			}
		}
		return "redirect:/drain/" + drenirovanie.getId();
	}

	@RequestMapping(value = "/savePunkcia", method = RequestMethod.POST)
	public String submitPunkciaeForm(@ModelAttribute("punkcia")
										 @Validated Punkcia punkcia,
										 BindingResult punResult,
										 final RedirectAttributes redirectAttributes) {
		if (punResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("punkcia", punkcia);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.punkcia", punResult);
			if (punkcia.getIsNew())
				return "redirect:/punkcia/"+ punkcia.getId();
			else
				return "redirect:/punkcia/"+ punkcia.getId() + "/" + punkcia.getDrenId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по дренированию были успешно загружены!");
			try {
				if (punkcia.getIsNew()) {
					List<Punkcia> punkcias= patientService.getPunkcia().findPatientAll(punkcia.getId());
					punkcia.setDrenId((punkcias==null)?"0":String.valueOf(punkcias.size()));
					patientService.getPunkcia().save(punkcia);
				} else {
					patientService.getPunkcia().update(punkcia);
				}
				return "redirect:/patients/update/" + punkcia.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по дренированию" +
						" . Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("punkcia", punkcia);
			}
		}
		return "redirect:/punkcia/" + punkcia.getId();
	}

	public static String determineSurgeryPage(int id, int drainId, Model model, PatientService patientService,
											  UserService userService, boolean showComplications) {
		if (model.asMap().get("drenirovanie") != null) {
			model.addAttribute("drenirovanie", model.asMap().get("drenirovanie"));
			model.addAttribute("org.springframework.validation.BindingResult.drenirovanie",
					model.asMap().get("org.springframework.validation.BindingResult.drenirovanie"));
		} else {
			Drenirovanie drenirovanie = patientService.getDrenirovanie().findById(id, drainId);
			if (drenirovanie == null) {
				return "redirect:/drain/" + id;
			}
			drenirovanie.setShowComplications(showComplications);
			model.addAttribute("drenirovanie", drenirovanie);
		}
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userService.findByEmail(name);
		if (user.isAdmin()) {
			model.addAttribute("isAdmin", true);
		} else {
			model.addAttribute("isAdmin", false);
		}
		model.addAttribute("expired", false);
		Patient patient = patientService.findPatientById(id);
		Calendar c = Calendar.getInstance();
		if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
			model.addAttribute("expired", true);
		}

		return "surgeries/drenirovanie";
	}

}
