package com.amgureeva.web.controller;

import com.amgureeva.Entities.GeneralSurgery;
import com.amgureeva.Entities.Patient;
import com.amgureeva.Entities.Surgeries.*;
import com.amgureeva.Entities.User;
import com.amgureeva.binding.PatientBinding.PatientService;
import com.amgureeva.binding.TwoStepsTreatmentBinding.TwoStepsTreatmentService;
import com.amgureeva.binding.UserBinding.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Calendar;

/**
 * Created by Nikita on 19.08.2016.
 */
@Controller
public class SurgeryController {
	@Autowired
	private PatientService patientService;


	@Autowired
	private UserService userService;

	@Autowired
	private TwoStepsTreatmentService twoStepsTreatmentService;

	@RequestMapping(value = "/surgery/{id}", method = RequestMethod.GET)
	public String showUserSurgeryForm(@PathVariable("id") int id, Model model) {
		return determineSurgeryPage(id, model, patientService, userService, twoStepsTreatmentService, false);
	}

	@RequestMapping(value = "/saveOpAlviokokkoz", method = RequestMethod.POST)
	public String submitAlviokokozForm(@ModelAttribute("alviokokkoz")
									   @Validated OpAlveokokkoz alviokokkoz,
									   BindingResult alvResult,
									   final RedirectAttributes redirectAttributes) {
		if (alvResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("alviokokkoz", alviokokkoz);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.alviokokkoz", alvResult);
			return "redirect:/surgery/" + alviokokkoz.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"Альвиококкоз\" были успешно загружены!");
			try {
				if (alviokokkoz.getIsNew()) {
					patientService.getOpAlveokokkozDAO().save(alviokokkoz);
				} else {
					patientService.getOpAlveokokkozDAO().update(alviokokkoz);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(alviokokkoz.getId()));
				if (alviokokkoz.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				} else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				if (p.getTypeOfTreatment()) {
					GeneralSurgery gs = (GeneralSurgery)alviokokkoz;
					twoStepsTreatmentService.updateResults(gs);
				}
				return "redirect:/patients/update/" + alviokokkoz.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"Алвиококкоз\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("alviokokkoz", alviokokkoz);
			}
		}
		return "redirect:/surgery/" + alviokokkoz.getId();
	}

	@RequestMapping(value = "/saveOpEchinokokkoz", method = RequestMethod.POST)
	public String submitOpEchinokokkozForm(@ModelAttribute("echinokokkoz")
										   @Validated OpEchinokokkoz echinokokkoz,
										   BindingResult echResult,
										   final RedirectAttributes redirectAttributes) {
		if (echResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("echinokokkoz", echinokokkoz);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.echinokokkoz", echResult);
			return "redirect:/surgery/" + echinokokkoz.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"Эхинококкоз\" были успешно загружены!");
			try {
				if (echinokokkoz.getIsNew()) {
					patientService.getOpEchinokokkozDAO().save(echinokokkoz);
				} else {
					patientService.getOpEchinokokkozDAO().update(echinokokkoz);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(echinokokkoz.getId()));
				if (echinokokkoz.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				}  else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				return "redirect:/patients/update/" + echinokokkoz.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"Эхинококкоз\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("echinokokkoz", echinokokkoz);
			}
		}
		return "redirect:/surgery/" + echinokokkoz.getId();
	}

	@RequestMapping(value = "/saveOpGcr", method = RequestMethod.POST)
	public String submitOpGcrForm(@ModelAttribute("gcr") @Validated OpGCR gcr, BindingResult gcrResult,
								  final RedirectAttributes redirectAttributes) {
		if (gcrResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("gcr", gcr);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gcr", gcrResult);
			return "redirect:/surgery/" + gcr.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"ГЦР\" были успешно загружены!");
			try {
				if (gcr.getIsNew()) {
					patientService.getOpGcrDao().save(gcr);
				} else {
					patientService.getOpGcrDao().update(gcr);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(gcr.getId()));
				if (gcr.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				} else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				if (p.getTypeOfTreatment()) {
					GeneralSurgery gs = (GeneralSurgery)gcr;
					twoStepsTreatmentService.updateResults(gs);
				}
				return "redirect:/patients/update/" + gcr.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"ГЦР\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("gcr", gcr);
			}
		}
		return "redirect:/surgery/" + gcr.getId();
	}

	@RequestMapping(value = "/saveOpHcr", method = RequestMethod.POST)
	public String submitOpHcrForm(@ModelAttribute("hcr") @Validated OpHCR hcr, BindingResult hcrResult,
								  final RedirectAttributes redirectAttributes) {
		if (hcrResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("hcr", hcr);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.hcr", hcrResult);
			return "redirect:/surgery/" + hcr.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"ХЦР\" были успешно загружены!");
			try {
				if (hcr.getIsNew()) {
					patientService.getOpHcrDao().save(hcr);
				} else {
					patientService.getOpHcrDao().update(hcr);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(hcr.getId()));
				if (hcr.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				}  else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				if (p.getTypeOfTreatment()) {
					GeneralSurgery gs = (GeneralSurgery)hcr;
					twoStepsTreatmentService.updateResults(gs);
				}
				return "redirect:/patients/update/" + hcr.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"ХЦР\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("hcr", hcr);
			}
		}
		return "redirect:/surgery/" + hcr.getId();
	}

	@RequestMapping(value = "/saveOpKlatskin", method = RequestMethod.POST)
	public String submitOpKlatskinForm(@ModelAttribute("klatskin") @Validated OpKlatskin klatskin,
									   BindingResult klatskinResult,
									   final RedirectAttributes redirectAttributes) {
		if (klatskinResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("klatskin", klatskin);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.klatskin", klatskinResult);
			return "redirect:/surgery/" + klatskin.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"Опухоль Клацкина\" были успешно загружены!");
			try {
				if (klatskin.getIsNew()) {
					patientService.getOpKlatskinDao().save(klatskin);
				} else {
					patientService.getOpKlatskinDao().update(klatskin);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(klatskin.getId()));
				if (klatskin.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				}  else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				if (p.getTypeOfTreatment()) {
					GeneralSurgery gs = (GeneralSurgery)klatskin;
					twoStepsTreatmentService.updateResults(gs);
				}
				return "redirect:/patients/update/" + klatskin.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"Опухоль Клацкина\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("klatskin", klatskin);
			}
		}
		return "redirect:/surgery/" + klatskin.getId();
	}


	@RequestMapping(value = "/saveOpMkr", method = RequestMethod.POST)
	public String submitMKRForm(@ModelAttribute("mkr") @Validated OpMKR mkr,
								BindingResult mkrResult,
								final RedirectAttributes redirectAttributes) {
		if (mkrResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("mkr", mkr);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.mkr", mkrResult);
			return "redirect:/surgery/" + mkr.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"МКР\" были успешно загружены!");
			try {
				if (mkr.getIsNew()) {
					patientService.getOpMkrdao().save(mkr);
				} else {
					patientService.getOpMkrdao().update(mkr);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(mkr.getId()));
				if (mkr.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				}  else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				if (p.getTypeOfTreatment()) {
					GeneralSurgery gs = (GeneralSurgery)mkr;
					twoStepsTreatmentService.updateResults(gs);
				}
				return "redirect:/patients/update/" + mkr.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"МКР\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("mkr", mkr);
			}
		}
		return "redirect:/surgery/" + mkr.getId();
	}

	@RequestMapping(value = "/saveOpMnkr", method = RequestMethod.POST)
	public String submitMNKRForm(@ModelAttribute("mnkr") @Validated OpMNKR mnkr,
								 BindingResult mnkrResult,
								 final RedirectAttributes redirectAttributes) {
		if (mnkrResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("mnkr", mnkr);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.mnkr", mnkrResult);
			return "redirect:/surgery/" + mnkr.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"МНКР\" были успешно загружены!");
			try {
				if (mnkr.getIsNew()) {
					patientService.getOpMnkrdao().save(mnkr);
				} else {
					patientService.getOpMnkrdao().update(mnkr);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(mnkr.getId()));
				if (mnkr.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				}  else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				if (p.getTypeOfTreatment()) {
					GeneralSurgery gs = (GeneralSurgery)mnkr;
					twoStepsTreatmentService.updateResults(gs);
				}
				return "redirect:/patients/update/" + mnkr.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"МНКР\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("mnkr", mnkr);
			}
		}
		return "redirect:/surgery/" + mnkr.getId();
	}

	@RequestMapping(value = "/saveOpRzhp", method = RequestMethod.POST)
	public String submitRZHPForm(@ModelAttribute("rzhp") @Validated OpRZHP rzhp,
								 BindingResult rzhpResult,
								 final RedirectAttributes redirectAttributes) {
		if (rzhpResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("rzhp", rzhp);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.rzhp", rzhpResult);
			return "redirect:/surgery/" + rzhp.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"Рак желудка\" были успешно загружены!");
			try {
				if (rzhp.getIsNew()) {
					patientService.getOpRzhpdao().save(rzhp);
				} else {
					patientService.getOpRzhpdao().update(rzhp);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(rzhp.getId()));
				if (rzhp.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				}  else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				if (p.getTypeOfTreatment()) {
					GeneralSurgery gs = (GeneralSurgery)rzhp;
					twoStepsTreatmentService.updateResults(gs);
				}
				return "redirect:/patients/update/" + rzhp.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"Рак желудка\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("rzhp", rzhp);
			}
		}
		return "redirect:/surgery/" + rzhp.getId();
	}

	@RequestMapping(value = "/saveOpZhkb", method = RequestMethod.POST)
	public String submitZHKBForm(@ModelAttribute("zhkb") @Validated OpZhkb zhkb,
								 BindingResult zhkbResult,
								 final RedirectAttributes redirectAttributes) {
		if (zhkbResult.hasErrors()) {
			redirectAttributes.addFlashAttribute("zhkb", zhkb);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.zhkb", zhkbResult);
			return "redirect:/surgery/" + zhkb.getId();
		} else {
			redirectAttributes.addFlashAttribute("css", "success");
			redirectAttributes.addFlashAttribute("msg", "Данные по операции при диагнозе \"Желчнокаменная болезнь\" " +
					"были успешно загружены!");
			try {
				if (zhkb.getIsNew()) {
					patientService.getOpZHKBdao().save(zhkb);
				} else {
					patientService.getOpZHKBdao().update(zhkb);
				}
				Patient p = patientService.findPatientById(Integer.valueOf(zhkb.getId()));
				if (zhkb.areAllFieldsFilled()) {
					p.setFilledSurgery(true);
				}  else {
					p.setFilledSurgery(false);
				}
				patientService.updateFillers(p);
				return "redirect:/patients/update/" + zhkb.getId();
			} catch (Throwable t) {
				redirectAttributes.addFlashAttribute("msg", t + "\n Ошибка при добавлении информации по операции при диагнозе" +
						" \"Желчнокаменная болезнь\". Попробуйте снова.");
				redirectAttributes.addFlashAttribute("css", "danger");
				redirectAttributes.addFlashAttribute("zhkb", zhkb);
			}
		}
		return "redirect:/surgery/" + zhkb.getId();
	}


	public static String determineSurgeryPage(int id, Model model, PatientService patientService, UserService userService,
											  TwoStepsTreatmentService twoStepsTreatmentService, boolean isComplication) {
		Patient patient = patientService.findPatientById(id);
		model.addAttribute("showOslozhneniya", isComplication);
		model.addAttribute("twoStepsTreatment", patient.getTypeOfTreatment());
		model.addAttribute("expired", false);
		Calendar c = Calendar.getInstance();
		if (patient.getClosedDate() != null && patient.getClosedDate().before(c.getTime())) {
			model.addAttribute("expired", true);
		}
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userService.findByEmail(name);
		if (user.isAdmin()) {
			model.addAttribute("isAdmin", true);
		} else {
			model.addAttribute("isAdmin", false);
		}
		switch (patient.getDiagnosis()) {
			case 1:
				OpKlatskin klatskin = patientService.getOpKlatskinDao().findById(id);
				if (model.asMap().get("klatskin") != null) {
					model.addAttribute("klatskin", model.asMap().get("klatskin"));
					model.addAttribute("org.springframework.validation.BindingResult.klatskin",
							model.asMap().get("org.springframework.validation.BindingResult.klatskin"));
				} else {
					if (klatskin == null) {
						klatskin = new OpKlatskin();
						klatskin.setIsNew(true);
						klatskin.setId(String.valueOf(id));
					}
					if (patient.getTypeOfTreatment()) {
						GeneralSurgery gs = twoStepsTreatmentService.findResultById(id);
						if (gs != null) {
							klatskin.setFirstStepResults(gs);
						}
					}
					klatskin.setShowComplications(isComplication);
					model.addAttribute("klatskin", klatskin);
				}
				return "surgeries/opklatskin";
			case 2:
				OpGCR gcr = patientService.getOpGcrDao().findById(id);
				if (model.asMap().get("gcr") != null) {
					model.addAttribute("gcr", model.asMap().get("gcr"));
					model.addAttribute("org.springframework.validation.BindingResult.gcr",
							model.asMap().get("org.springframework.validation.BindingResult.gcr"));
				} else {
					if (gcr == null) {
						gcr = new OpGCR();
						gcr.setIsNew(true);
						gcr.setId(String.valueOf(id));
					}

					if (patient.getTypeOfTreatment()) {
						GeneralSurgery gs = twoStepsTreatmentService.findResultById(id);
						if (gs != null) {
							gcr.setFirstStepResults(gs);
						}
					}
					gcr.setShowComplications(isComplication);
					model.addAttribute("gcr", gcr);
				}
				return "surgeries/opgcr";
			case 3:
				OpHCR hcr = patientService.getOpHcrDao().findById(id);
				if (model.asMap().get("hcr") != null) {
					model.addAttribute("hcr", model.asMap().get("hcr"));
					model.addAttribute("org.springframework.validation.BindingResult.hcr",
							model.asMap().get("org.springframework.validation.BindingResult.hcr"));
				} else {
					if (hcr == null) {
						hcr = new OpHCR();
						hcr.setIsNew(true);
						hcr.setId(String.valueOf(id));
					}

					if (patient.getTypeOfTreatment()) {
						GeneralSurgery gs = twoStepsTreatmentService.findResultById(id);
						if (gs != null) {
							hcr.setFirstStepResults(gs);
						}
					}
					hcr.setShowComplications(isComplication);
					model.addAttribute("hcr", hcr);
				}
				return "surgeries/ophcr";
			case 4:
				OpMKR mkr = patientService.getOpMkrdao().findById(id);
				if (model.asMap().get("mkr") != null) {
					model.addAttribute("mkr", model.asMap().get("mkr"));
					model.addAttribute("org.springframework.validation.BindingResult.mkr",
							model.asMap().get("org.springframework.validation.BindingResult.mkr"));
				} else {
					if (mkr == null) {
						mkr = new OpMKR();
						mkr.setIsNew(true);
						mkr.setId(String.valueOf(id));
					}

					if (patient.getTypeOfTreatment()) {
						GeneralSurgery gs = twoStepsTreatmentService.findResultById(id);
						if (gs != null) {
							mkr.setFirstStepResults(gs);
						}
					}
					mkr.setShowComplications(isComplication);
					model.addAttribute("mkr", mkr);
				}
				return "surgeries/opmkr";
			case 5:
				OpMNKR mnkr = patientService.getOpMnkrdao().findById(id);
				if (model.asMap().get("mnkr") != null) {
					model.addAttribute("mnkr", model.asMap().get("mnkr"));
					model.addAttribute("org.springframework.validation.BindingResult.mnkr",
							model.asMap().get("org.springframework.validation.BindingResult.mnkr"));
				} else {
					if (mnkr == null) {
						mnkr = new OpMNKR();
						mnkr.setIsNew(true);
						mnkr.setId(String.valueOf(id));
					}

					if (patient.getTypeOfTreatment()) {
						GeneralSurgery gs = twoStepsTreatmentService.findResultById(id);
						if (gs != null) {
							mnkr.setFirstStepResults(gs);
						}
					}
					mnkr.setShowComplications(isComplication);
					model.addAttribute("mnkr", mnkr);
				}
				return "surgeries/opmnkr";
			case 6:
				OpRZHP rzhp = patientService.getOpRzhpdao().findById(id);
				if (model.asMap().get("rzhp") != null) {
					model.addAttribute("rzhp", model.asMap().get("rzhp"));
					model.addAttribute("org.springframework.validation.BindingResult.rzhp",
							model.asMap().get("org.springframework.validation.BindingResult.rzhp"));
				} else {
					if (rzhp == null) {
						rzhp = new OpRZHP();
						rzhp.setIsNew(true);
						rzhp.setId(String.valueOf(id));
					}

					if (patient.getTypeOfTreatment()) {
						GeneralSurgery gs = twoStepsTreatmentService.findResultById(id);
						if (gs != null) {
							rzhp.setFirstStepResults(gs);
						}
					}
					rzhp.setShowComplications(isComplication);
					model.addAttribute("rzhp", rzhp);
				}
				return "surgeries/oprzhp";
			case 7:
				OpEchinokokkoz echinokokkoz = patientService.getOpEchinokokkozDAO().findById(id);
				if (model.asMap().get("echinokokkoz") != null) {
					model.addAttribute("echinokokkoz", model.asMap().get("echinokokkoz"));
					model.addAttribute("org.springframework.validation.BindingResult.echinokokkoz",
							model.asMap().get("org.springframework.validation.BindingResult.echinokokkoz"));
				} else {
					if (echinokokkoz == null) {
						echinokokkoz = new OpEchinokokkoz();
						echinokokkoz.setIsNew(true);
						echinokokkoz.setId(String.valueOf(id));
					}
					echinokokkoz.setShowComplications(isComplication);
					model.addAttribute("echinokokkoz", echinokokkoz);
				}
				return "surgeries/opech";
			case 8:
				OpAlveokokkoz alveokokkoz = patientService.getOpAlveokokkozDAO().findById(id);
				if (model.asMap().get("alviokokkoz") != null) {
					model.addAttribute("alviokokkoz", model.asMap().get("alviokokkoz"));
					model.addAttribute("org.springframework.validation.BindingResult.alviokokkoz",
							model.asMap().get("org.springframework.validation.BindingResult.alviokokkoz"));
				} else {
					if (alveokokkoz == null) {
						alveokokkoz = new OpAlveokokkoz();
						alveokokkoz.setIsNew(true);
						alveokokkoz.setId(String.valueOf(id));
					}

					if (patient.getTypeOfTreatment()) {
						GeneralSurgery gs = twoStepsTreatmentService.findResultById(id);
						if (gs != null) {
							alveokokkoz.setFirstStepResults(gs);
						}
					}
					alveokokkoz.setShowComplications(isComplication);
					model.addAttribute("alviokokkoz", alveokokkoz);
				}
				return "surgeries/opalv";
			case 9:
				OpZhkb zhkb = patientService.getOpZHKBdao().findById(id);
				if (model.asMap().get("zhkb") != null) {
					model.addAttribute("zhkb", model.asMap().get("zhkb"));
					model.addAttribute("org.springframework.validation.BindingResult.zhkb",
							model.asMap().get("org.springframework.validation.BindingResult.zhkb"));
				} else {
					if (zhkb == null) {
						zhkb = new OpZhkb();
						zhkb.setIsNew(true);
						zhkb.setId(String.valueOf(id));
					}
					zhkb.setShowComplications(isComplication);
					model.addAttribute("zhkb", zhkb);
				}
				return "surgeries/opzhkb";
		}
		return "redirect:/patients/update/" + id;
	}
}

