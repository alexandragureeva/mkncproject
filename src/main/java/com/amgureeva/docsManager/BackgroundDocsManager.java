package com.amgureeva.docsManager;

import com.amgureeva.binding.PatientBinding.PatientService;
import com.amgureeva.binding.PatientFileUploadBinding.PatientFileUploadService;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nikita on 09.09.2016.
 */
@WebListener
public class BackgroundDocsManager implements ServletContextListener {

	private ScheduledExecutorService scheduler;

	private PatientService patientService;
	private PatientFileUploadService patientFileUploadService;

	@Override
	public void contextInitialized(ServletContextEvent event) {
		WebApplicationContext servletContext =  WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());

		patientService = (PatientService) servletContext.getBean("patientService");
		patientFileUploadService = (PatientFileUploadService) servletContext.getBean("patientfileuploadService");
		scheduler = Executors.newSingleThreadScheduledExecutor();
		scheduler.scheduleAtFixedRate(new DocsGetter(patientService, patientFileUploadService), 0, 1, TimeUnit.MINUTES);

	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {

		scheduler.shutdownNow();

	}

}