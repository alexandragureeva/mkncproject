package com.amgureeva.docsManager;

import com.amgureeva.Entities.Patient;
import com.amgureeva.Entities.PatientFileUpload;
import com.amgureeva.binding.PatientBinding.PatientService;
import com.amgureeva.binding.PatientFileUploadBinding.PatientFileUploadService;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.TransientDataAccessResourceException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.mail.search.FlagTerm;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nikita on 09.09.2016.
 */
@org.springframework.stereotype.Service
public class DocsGetter implements Runnable {

	PatientService patientService;
	private PatientFileUploadService patientFileUploadService;

	public DocsGetter() {
	}

	public DocsGetter(PatientService patientService, PatientFileUploadService patientFileUploadService) {
		this.patientService = patientService;
		this.patientFileUploadService = patientFileUploadService;
	}

	//Credentials
	private String host = "imap.gmail.com";
	private String username = "mkncdoccollector@gmail.com";
	private String password = "ghVB67TY";

	@Override
	public void run() {
		check();
	}


	public void check() {
		try {
			Properties properties = new Properties();
			properties.setProperty("mail.imap.ssl.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);
			Store store = emailSession.getStore("imap");
			store.connect(host, username, password);

			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_WRITE);

			Message[] messages = emailFolder.search(new FlagTerm(new Flags(
					Flags.Flag.SEEN), false));
			for (int i = 0, n = messages.length; i < n; i++) {
				saveFile(messages[i], emailSession);
				messages[i].setFlag(Flags.Flag.SEEN, true);
			}
			emailFolder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveFile(Message message, Session session) throws MessagingException, IOException {
		if (message.getSubject().matches("[\\w*\\W*]*\\d+\\/\\d+[\\w*\\W*]*")) {
			if (message.getContent() instanceof Multipart) {
				Multipart multipart = (Multipart) message.getContent();
				boolean hasAttachment = false;
				for (int j = 0; j < multipart.getCount(); j++) {
					BodyPart bodyPart = multipart.getBodyPart(j);
					if (!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition()) &&
							!StringUtils.isNotBlank(bodyPart.getFileName())) {
						continue; // dealing with attachments only
					}
					hasAttachment = true;
					String disNumber;
					Pattern pattern = Pattern.compile("\\d+\\/\\d+");
					Matcher matcher = pattern.matcher(message.getSubject());
					if (matcher.find()) {
						disNumber = matcher.group(0);
						Patient patient = patientService.findPatientByHisNumber(disNumber);
						if (patient != null) {
							String destFilePath = null;
							try {
								String extension = null;
								pattern = Pattern.compile("\\.(\\w*\\W*)");
								String value = bodyPart.getFileName();
								Matcher matcher2 = pattern.matcher(MimeUtility.decodeText(value));
								if (matcher2.find()) {
									extension = matcher2.group(matcher2.groupCount());
								}
								destFilePath = patient.getName() + "_" +
										patient.getDiseaseNumber() +
										"_" + message.getSubject().substring(0, matcher.start() - 1) + "." + extension;
								destFilePath = destFilePath.replace("/", "_");
								destFilePath = destFilePath.replace(" ", "_");
							} catch (StringIndexOutOfBoundsException ex) {
								reply("Тема сообщения составлена неправильно. В теме отсутствует тип исследования.\n" +
										"Формирование письма:\n" +
										"Тема сообщения должна содержать: {название_исследования} {номер истории болезни}\n" +
										"Передаваемый файл должен содержать расширение на конце (к примеру .pdf).\n" +
										"Номер истории болезни должен выглядеть следующим образом: {цифры}/{цифры}", message);
							}
							if (destFilePath != null) {
								PatientFileUpload patientFileUpload = new PatientFileUpload();
								patientFileUpload.setPatientId(Integer.valueOf(patient.getId()));
								patientFileUpload.setFileToUpload(bodyPart.getInputStream());
								patientFileUpload.setFileName(destFilePath);
								try {
									patientFileUploadService.save(patientFileUpload);
								} catch(TransientDataAccessResourceException ex) {
									reply("Передаваемый файл слишком большой. Сохранение таких файлов невозможно.", message);
								}
							}
						} else {
							reply("Пациент с таким номером истории болезни отсутствует.\n" +
									"Формирование письма:\n" +
									"Тема сообщения должна содержать: {название_исследования} {номер истории болезни}\n" +
									"Передаваемый файл должен содержать расширение на конце (к примеру .pdf).\n" +
									"Номер истории болезни должен выглядеть следующим образом: {цифры}/{цифры}", message);
							}
					} else {
						reply("Тема сообщения составлена неправильно. В теме отсутствует номер истории болезни.\n" +
								"Формирование письма:\n" +
								"Тема сообщения должна содержать: {название_исследования} {номер истории болезни}\n" +
								"Передаваемый файл должен содержать расширение на конце (к примеру .pdf).\n" +
								"Номер истории болезни должен выглядеть следующим образом: {цифры}/{цифры}", message);
					}
				}
				if (!hasAttachment) {
					reply("Сообщение, отправленное Вами не содержит вложений.", message);
				}
			} else {
				reply("Сообщение, отправленное Вами не содержит вложений.", message);
			}
		} else {
			reply("Тема сообщения составлена неправильно.\n" +
					"Формирование письма:\n" +
					"Тема сообщения должна содержать: {название_исследования} {номер истории болезни}\n" +
					"Передаваемый файл должен содержать расширение на конце (к примеру .pdf)." +
					"Номер истории болезни должен выглядеть следующим образом: {цифры}/{цифры}", message);
		}
	}

	private void reply(String body, Message message) throws MessagingException {
		String host = "smtp.gmail.com";
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.ssl.enable", "true");
		Session session = Session.getInstance(props);
		MimeMessage msg = new MimeMessage(session);
		//MimeMessage msg = (MimeMessage) message.reply(false);
		msg.setFrom(new InternetAddress(username));
		msg.setSubject("RE: " + message.getSubject());
		msg.setText(body);
		Transport t = session.getTransport("smtp");
		try {
			t.connect(host, username, password);
			Address[] addresses = message.getFrom();
			t.sendMessage(msg, addresses);
		} finally {
			t.close();
		}
	}

}


/*
body of email sent back:
	Тема сообщения должна содержать: {название_исследования} {номер истории болезни}
	Передаваемый файл должен содержать расширение на конце.
 */