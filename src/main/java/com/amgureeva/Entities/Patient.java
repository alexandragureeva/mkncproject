package com.amgureeva.Entities;

import com.amgureeva.Other.Properties;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Александра on 09.04.2016.
 */
public class Patient {

    private String id;
    private String name;
    private String sex;
    private String birthdate;
	private String age;
	private Double weight;
	private Double growth;
	private Double bmi;
	private Integer asaValue;
	
    private String diseaseNumber;
    private Integer diagnosis;
	private String mkbCode;
	private Integer gospitalization;
	private Integer gospitalizationTime;

	private boolean hasSurgery;

	private boolean typeOfTreatment;
	
	//simultaneous diseases
	private Integer ibs;
	private Integer hyperDisease;
	private Integer diabet;
	private Integer chronVenDisease;
	private Integer bronchAstma;
	private Integer chronBronchit;
	private Integer emfizema;
	private Integer serdechPoroki;
	private Integer onmk;
	private Integer parkinsonDisease;
	private Integer parkinsonSyndrom;
	private Integer ozhirenie;
	private Integer yazvenDisease;
	private Integer hyperplazia;
	private Integer otherDiseases;


	private Integer ibsType;
	private Integer hyperDiseaseType;
	private Integer diabetType;
	private Integer chronVenDiseaseType;
	private Integer bronchAstmaType;
	private Integer chronBronchitType;
	private Integer emfizemaType;
	private Integer serdechPorokiType;
	private Integer onmkType;
	private Integer parkinsonDiseaseType;
	private Integer parkinsonSyndromType;
	private Integer ozhirenieType;
	private Integer yazvenDiseaseType;
	private Integer hyperplaziaType;

	private Integer ibsSubType;
	private Integer yazvenDiseaseComplications;

	private String address;
	private Long phone1, phone2;
	
    private String details;

    private String startDate, endDate;
    private String bedDaysNumber;

	private boolean filledGeneralInfo;
	private boolean filledDiagnosisAtStart;
	private boolean filledObjExam;
	private boolean filledAnalysis0;
	private boolean filledSurgery;
	private boolean filledAnalysis1;
	private boolean filledAnalysis5;
	private boolean filledAnalysis10;
	private boolean filledDiagnosisAtEnd;

	private String surgeryDate;

	private String deathDate;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private java.util.Date closedDate;

    public Patient(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthdate() {
        return (birthdate == null) ? null : birthdate.substring(0, 10);
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = makeRightDate(birthdate);
    }

    public String getDiseaseNumber() {
        return diseaseNumber;
    }

    public void setDiseaseNumber(String diseaseNumber) {
        this.diseaseNumber = diseaseNumber;
    }

    public Integer getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(Integer diagnosis) {
        this.diagnosis = diagnosis;
    }

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getGrowth() {
		return growth;
	}

	public void setGrowth(Double growth) {
		this.growth = growth;
	}

	public Double getBmi() {
		return bmi;
	}

	public void setBmi(Double bmi) {
		this.bmi = bmi;
	}

	public Integer getAsaValue() {
		return asaValue;
	}

	public void setAsaValue(Integer asaValue) {
		this.asaValue = asaValue;
	}

	public String getMkbCode() {
		return mkbCode;
	}

	public void setMkbCode(String mkbCode) {
		this.mkbCode = mkbCode;
	}

	public Integer getGospitalization() {
		return gospitalization;
	}

	public void setGospitalization(Integer gospitalization) {
		this.gospitalization = gospitalization;
	}

	public Integer getGospitalizationTime() {
		return gospitalizationTime;
	}

	public void setGospitalizationTime(Integer gospitalizationTime) {
		this.gospitalizationTime = gospitalizationTime;
	}

	public Integer getIbs() {
		return ibs;
	}

	public void setIbs(Integer ibs) {
		this.ibs = ibs;
	}

	public Integer getHyperDisease() {
		return hyperDisease;
	}

	public void setHyperDisease(Integer hyperDisease) {
		this.hyperDisease = hyperDisease;
	}

	public Integer getDiabet() {
		return diabet;
	}

	public void setDiabet(Integer diabet) {
		this.diabet = diabet;
	}

	public Integer getChronVenDisease() {
		return chronVenDisease;
	}

	public void setChronVenDisease(Integer chronVenDisease) {
		this.chronVenDisease = chronVenDisease;
	}

	public Integer getBronchAstma() {
		return bronchAstma;
	}

	public void setBronchAstma(Integer bronchAstma) {
		this.bronchAstma = bronchAstma;
	}

	public Integer getChronBronchit() {
		return chronBronchit;
	}

	public void setChronBronchit(Integer chronBronchit) {
		this.chronBronchit = chronBronchit;
	}

	public Integer getEmfizema() {
		return emfizema;
	}

	public void setEmfizema(Integer emfizema) {
		this.emfizema = emfizema;
	}

	public Integer getSerdechPoroki() {
		return serdechPoroki;
	}

	public void setSerdechPoroki(Integer serdechPoroki) {
		this.serdechPoroki = serdechPoroki;
	}

	public Integer getOnmk() {
		return onmk;
	}

	public void setOnmk(Integer onmk) {
		this.onmk = onmk;
	}

	public Integer getParkinsonDisease() {
		return parkinsonDisease;
	}

	public void setParkinsonDisease(Integer parkinsonDisease) {
		this.parkinsonDisease = parkinsonDisease;
	}

	public Integer getParkinsonSyndrom() {
		return parkinsonSyndrom;
	}

	public void setParkinsonSyndrom(Integer parkinsonSyndrom) {
		this.parkinsonSyndrom = parkinsonSyndrom;
	}

	public Integer getOzhirenie() {
		return ozhirenie;
	}

	public void setOzhirenie(Integer ozhirenie) {
		this.ozhirenie = ozhirenie;
	}

	public Integer getYazvenDisease() {
		return yazvenDisease;
	}

	public void setYazvenDisease(Integer yazvenDisease) {
		this.yazvenDisease = yazvenDisease;
	}

	public Integer getHyperplazia() {
		return hyperplazia;
	}

	public void setHyperplazia(Integer hyperplazia) {
		this.hyperplazia = hyperplazia;
	}

	public Integer getOtherDiseases() {
		return otherDiseases;
	}

	public void setOtherDiseases(Integer otherDiseases) {
		this.otherDiseases = otherDiseases;
	}

	public Integer getIbsType() {
		return ibsType;
	}

	public void setIbsType(Integer ibsType) {
		this.ibsType = ibsType;
	}

	public Integer getHyperDiseaseType() {
		return hyperDiseaseType;
	}

	public void setHyperDiseaseType(Integer hyperDiseaseType) {
		this.hyperDiseaseType = hyperDiseaseType;
	}

	public Integer getDiabetType() {
		return diabetType;
	}

	public void setDiabetType(Integer diabetType) {
		this.diabetType = diabetType;
	}

	public Integer getChronVenDiseaseType() {
		return chronVenDiseaseType;
	}

	public void setChronVenDiseaseType(Integer chronVenDiseaseType) {
		this.chronVenDiseaseType = chronVenDiseaseType;
	}

	public Integer getBronchAstmaType() {
		return bronchAstmaType;
	}

	public void setBronchAstmaType(Integer bronchAstmaType) {
		this.bronchAstmaType = bronchAstmaType;
	}

	public Integer getChronBronchitType() {
		return chronBronchitType;
	}

	public void setChronBronchitType(Integer chronBronchitType) {
		this.chronBronchitType = chronBronchitType;
	}

	public Integer getEmfizemaType() {
		return emfizemaType;
	}

	public void setEmfizemaType(Integer emfizemaType) {
		this.emfizemaType = emfizemaType;
	}

	public Integer getSerdechPorokiType() {
		return serdechPorokiType;
	}

	public void setSerdechPorokiType(Integer serdechPorokiType) {
		this.serdechPorokiType = serdechPorokiType;
	}

	public Integer getOnmkType() {
		return onmkType;
	}

	public void setOnmkType(Integer onmkType) {
		this.onmkType = onmkType;
	}

	public Integer getParkinsonDiseaseType() {
		return parkinsonDiseaseType;
	}

	public void setParkinsonDiseaseType(Integer parkinsonDiseaseType) {
		this.parkinsonDiseaseType = parkinsonDiseaseType;
	}

	public Integer getParkinsonSyndromType() {
		return parkinsonSyndromType;
	}

	public void setParkinsonSyndromType(Integer parkinsonSyndromType) {
		this.parkinsonSyndromType = parkinsonSyndromType;
	}

	public Integer getOzhirenieType() {
		return ozhirenieType;
	}

	public void setOzhirenieType(Integer ozhirenieType) {
		this.ozhirenieType = ozhirenieType;
	}

	public Integer getYazvenDiseaseType() {
		return yazvenDiseaseType;
	}

	public void setYazvenDiseaseType(Integer yazvenDiseaseType) {
		this.yazvenDiseaseType = yazvenDiseaseType;
	}

	public Integer getHyperplaziaType() {
		return hyperplaziaType;
	}

	public void setHyperplaziaType(Integer hyperplaziaType) {
		this.hyperplaziaType = hyperplaziaType;
	}

	public Integer getIbsSubType() {
		return ibsSubType;
	}

	public void setIbsSubType(Integer ibsSubType) {
		this.ibsSubType = ibsSubType;
	}

	public Integer getYazvenDiseaseComplications() {
		return yazvenDiseaseComplications;
	}

	public void setYazvenDiseaseComplications(Integer yazvenDiseaseComplications) {
		this.yazvenDiseaseComplications = yazvenDiseaseComplications;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getPhone1() {
		return phone1;
	}

	public void setPhone1(Long phone1) {
		this.phone1 = phone1;
	}

	public Long getPhone2() {
		return phone2;
	}

	public void setPhone2(Long phone2) {
		this.phone2 = phone2;
	}

	public String getStartDate() {
        return (startDate == null) ? null : startDate.substring(0, 10);
    }

    public void setStartDate(String startDate) {
        this.startDate = makeRightDate(startDate);
    }

    public String getEndDate() {
        return (endDate == null) ? null : endDate.substring(0, 10);
    }

    public void setEndDate(String endDate) {
        this.endDate = makeRightDate(endDate);
    }

    public String getBedDaysNumber() {
        return bedDaysNumber;
    }


    public void setBedDaysNumber() {
		if (startDate != null && endDate != null) {
			String[] date = startDate.split("/");
			//Calendar calendar1 = Calendar.getInstance(); // );
			java.util.Date dat = new Date(Integer.valueOf(date[2]),Integer.valueOf(date[1])-1, Integer.valueOf(date[0]));
			date = endDate.split("/");
			java.util.Date dat2 = new Date(Integer.valueOf(date[2]),Integer.valueOf(date[1])-1, Integer.valueOf(date[0]));
			//Calendar calendar2 = new GregorianCalendar(Integer.valueOf(date[2]),Integer.valueOf(date[1]), Integer.valueOf(date[0]));
			this.bedDaysNumber = String.valueOf((dat2.getTime()-dat.getTime())/ (1000 * 60 * 60 * 24));
		}
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDeathDate() {
        return (deathDate == null) ? null : deathDate.substring(0, 10);
    }

    public void setDeathDate(String deathDate) {
        this.deathDate = makeRightDate(deathDate);
    }

	public java.util.Date getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(java.util.Date closedDate) {
		this.closedDate = closedDate;
	}

	public String getSurgeryDate() {
		return (surgeryDate == null) ? null : surgeryDate.substring(0, 10);
	}

	public void setSurgeryDate(String surgeryDate) {
		this.surgeryDate = makeRightDate(surgeryDate);
	}


	public boolean isNew() {
        return (this.id==null);
    }

	public boolean getFilledGeneralInfo() {
		return filledGeneralInfo;
	}

	public void setFilledGeneralInfo(boolean filledGeneralInfo) {
		this.filledGeneralInfo = filledGeneralInfo;
	}

	public boolean getFilledDiagnosisAtStart() {
		return filledDiagnosisAtStart;
	}

	public void setFilledDiagnosisAtStart(boolean filledDiagnosisAtStart) {
		this.filledDiagnosisAtStart = filledDiagnosisAtStart;
	}

	public boolean getFilledObjExam() {
		return filledObjExam;
	}

	public void setFilledObjExam(boolean filledObjExam) {
		this.filledObjExam = filledObjExam;
	}

	public boolean getFilledAnalysis0() {
		return filledAnalysis0;
	}

	public void setFilledAnalysis0(boolean filledAnalysis0) {
		this.filledAnalysis0 = filledAnalysis0;
	}

	public boolean getFilledSurgery() {
		return filledSurgery;
	}

	public void setFilledSurgery(boolean filledSurgery) {
		this.filledSurgery = filledSurgery;
	}

	public boolean getFilledAnalysis1() {
		return filledAnalysis1;
	}

	public void setFilledAnalysis1(boolean filledAnalysis1) {
		this.filledAnalysis1 = filledAnalysis1;
	}

	public boolean getFilledAnalysis5() {
		return filledAnalysis5;
	}

	public void setFilledAnalysis5(boolean filledAnalysis5) {
		this.filledAnalysis5 = filledAnalysis5;
	}

	public boolean getFilledAnalysis10() {
		return filledAnalysis10;
	}

	public void setFilledAnalysis10(boolean filledAnalysis10) {
		this.filledAnalysis10 = filledAnalysis10;
	}

	public boolean getFilledDiagnosisAtEnd() {
		return filledDiagnosisAtEnd;
	}

	public void setFilledDiagnosisAtEnd(boolean filledDiagnosisAtEnd) {
		this.filledDiagnosisAtEnd = filledDiagnosisAtEnd;
	}

	public boolean getHasSurgery() {
		return hasSurgery;
	}

	public void setHasSurgery(boolean hasSurgery) {
		this.hasSurgery = hasSurgery;
	}

	public static String makeRightDate(String date) {
		if (date== null || date.length() < 10)
			return date;
		if (date.contains("-")) {
			date = date.substring(0, 10);
			String[] dateArr = date.split("-");
			StringBuilder builder = new StringBuilder();
			builder.append(dateArr[2]);
			builder.append("/");
			builder.append(dateArr[1]);
			builder.append("/");
			builder.append(dateArr[0]);
			return builder.toString();
		}
		return date.substring(0, 10);
	}

	public String getDiagnosisName() {
		Properties properties = new Properties();
		return properties.getDiagnosisNumber(diagnosis);
	}

	public boolean getTypeOfTreatment() {
		return typeOfTreatment;
	}

	public void setTypeOfTreatment(boolean typeOfTreatment) {
		this.typeOfTreatment = typeOfTreatment;
	}

	public boolean areAllFieldsFilled() {
		if (weight!= null) {
			if (growth != null) {
				if (asaValue != null && asaValue != 0) {
					if (mkbCode != null && !mkbCode.isEmpty()) {
						if (gospitalization != null && gospitalization != 0) {
							if (gospitalization != 2 ||
									gospitalization == 2 && gospitalizationTime != null && gospitalizationTime != 0) {
								if (address != null && !address.isEmpty()) {
									if (phone1 != null && phone1 != 0) {
										if (ibs == null || ibs == 0 || ibsType != null  && ibsType != 0) {
											if (ibsType == null || ibsType == 0 || ibsType == 2 ||  ibsType == 1 && ibsSubType != 0) {
												if (hyperDisease == null || hyperDisease == 0 || hyperDiseaseType != null && hyperDiseaseType != 0) {
													if (diabet == null || diabet == 0 || diabetType != null && diabetType != 0) {
														if (chronVenDisease == null || chronVenDisease== 0 || chronVenDiseaseType != null && chronVenDiseaseType != 0) {
															if (bronchAstma == null || bronchAstma==0 || bronchAstmaType != null && bronchAstmaType != 0) {
																if (chronBronchit == null || chronBronchit == 0 || chronBronchitType != null && chronBronchitType != 0) {
																	if (emfizema == null || emfizema == 0 || emfizemaType != null && emfizemaType != 0) {
																		if (serdechPoroki == null || serdechPoroki == 0 || serdechPorokiType != null && serdechPorokiType != 0) {
																			if (onmk == null || onmk == 0 || onmkType != null && onmkType != 0) {
																				if (parkinsonDisease == null || parkinsonDisease == 0 || parkinsonDiseaseType != null && parkinsonDiseaseType != 0) {
																					if (parkinsonSyndrom == null || parkinsonSyndrom == 0 ||parkinsonSyndromType != null && parkinsonSyndromType != 0) {
																						if (ozhirenie == null || ozhirenie == 0 || ozhirenieType != null && ozhirenieType != 0) {
																							if (yazvenDisease == null || yazvenDisease == 0 || yazvenDiseaseType != null && yazvenDiseaseType != 0
																									&& yazvenDiseaseComplications != null && yazvenDiseaseComplications != 0) {
																								if (hyperplazia == null || hyperplazia == 0 || hyperplaziaType != null && hyperplaziaType != 0) {
																									  return true;
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}

												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}
