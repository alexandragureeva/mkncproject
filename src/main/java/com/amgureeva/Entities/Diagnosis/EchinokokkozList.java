package com.amgureeva.Entities.Diagnosis;

/**
 * Created by Александра on 05.05.2016.
 */
public class EchinokokkozList {
    private String name;
    private String voz;
    private String antitela;
    private String antitelaRes;
    private String antitelaUroven;
    private String epidemAnamnez;
    private String mnozhKisty;
    private String kistyNumber;
    private String kistyType;
    private String kistyRazmer;
    private String kistyOsl;
    private String kratLechenia;
    private String lechenieType;
    private String vnepPorazhenie;
    private String trofStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoz() {
        return voz;
    }

    public void setVoz(String voz) {
        this.voz = voz;
    }

    public String getAntitela() {
        return antitela;
    }

    public void setAntitela(String antitela) {
        this.antitela = antitela;
    }

    public String getAntitelaRes() {
        return antitelaRes;
    }

    public void setAntitelaRes(String antitelaRes) {
        this.antitelaRes = antitelaRes;
    }

    public String getAntitelaUroven() {
        return antitelaUroven;
    }

    public void setAntitelaUroven(String antitelaUroven) {
        this.antitelaUroven = antitelaUroven;
    }

    public String getEpidemAnamnez() {
        return epidemAnamnez;
    }

    public void setEpidemAnamnez(String epidemAnamnez) {
        this.epidemAnamnez = epidemAnamnez;
    }

    public String getMnozhKisty() {
        return mnozhKisty;
    }

    public void setMnozhKisty(String mnozhKisty) {
        this.mnozhKisty = mnozhKisty;
    }

    public String getKistyNumber() {
        return kistyNumber;
    }

    public void setKistyNumber(String kistyNumber) {
        this.kistyNumber = kistyNumber;
    }

    public String getKistyType() {
        return kistyType;
    }

    public void setKistyType(String kistyType) {
        this.kistyType = kistyType;
    }

    public String getKistyRazmer() {
        return kistyRazmer;
    }

    public void setKistyRazmer(String kistyRazmer) {
        this.kistyRazmer = kistyRazmer;
    }

    public String getKistyOsl() {
        return kistyOsl;
    }

    public void setKistyOsl(String kistyOsl) {
        this.kistyOsl = kistyOsl;
    }

    public String getKratLechenia() {
        return kratLechenia;
    }

    public void setKratLechenia(String kratLechenia) {
        this.kratLechenia = kratLechenia;
    }

    public String getLechenieType() {
        return lechenieType;
    }

    public void setLechenieType(String lechenieType) {
        this.lechenieType = lechenieType;
    }

    public String getVnepPorazhenie() {
        return vnepPorazhenie;
    }

    public void setVnepPorazhenie(String vnepPorazhenie) {
        this.vnepPorazhenie = vnepPorazhenie;
    }

    public String getTrofStatus() {
        return trofStatus;
    }

    public void setTrofStatus(String trofStatus) {
        this.trofStatus = trofStatus;
    }
}
