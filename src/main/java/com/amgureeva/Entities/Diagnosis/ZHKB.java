package com.amgureeva.Entities.Diagnosis;

import com.amgureeva.Entities.GeneralDiagnosis;

/**
 * Created by Александра on 23.04.2016.
 */
public class ZHKB extends GeneralDiagnosis {
	public ZHKB() {
	}

	private int cholecistit;
	private int ostriiCholecistit;
	private Integer oslozhnenii;
	private Integer mirizi;
	private Integer zhelt_mirizi;
	private Integer type;
	private Integer cholangiol;
	private Integer zhelt_cholang;
	private Integer type_cholang;
	private Integer vodyanka;
	private Integer svich;
	private Integer smorch;
	private Integer peritonit;
	private Integer empiema;
	private Integer cholangit;
	private Integer cholpankreotit;
	private int trof_status;

	public Integer getOslozhnenii() {
		return (oslozhnenii == null) ? 0 : oslozhnenii;
	}

	public void setOslozhnenii(Integer oslozhnenii) {
		this.oslozhnenii = oslozhnenii;
	}

	public int getCholecistit() {
		return cholecistit;
	}

	public void setCholecistit(int cholecistit) {
		this.cholecistit = cholecistit;
	}

	public int getOstriiCholecistit() {
		return ostriiCholecistit;
	}

	public void setOstriiCholecistit(int ostriiCholecistit) {
		this.ostriiCholecistit = ostriiCholecistit;
	}

	public Integer getMirizi() {
		return (mirizi == null) ? 0 : mirizi;
	}

	public void setMirizi(Integer mirizi) {
		this.mirizi = mirizi;
	}

	public Integer getZhelt_mirizi() {
		return (zhelt_mirizi == null) ? 0 : zhelt_mirizi;
	}

	public void setZhelt_mirizi(Integer zhelt_mirizi) {
		this.zhelt_mirizi = zhelt_mirizi;
	}

	public Integer getType() {
		return (type == null) ? 0 : type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getCholangiol() {
		return (cholangiol == null) ? 0 : cholangiol;
	}

	public void setCholangiol(Integer cholangiol) {
		this.cholangiol = cholangiol;
	}

	public Integer getZhelt_cholang() {
		return (zhelt_cholang == null) ? 0 : zhelt_cholang;
	}

	public void setZhelt_cholang(Integer zhelt_cholang) {
		this.zhelt_cholang = zhelt_cholang;
	}

	public Integer getType_cholang() {
		return (type_cholang == null) ? 0 : type_cholang;
	}

	public void setType_cholang(Integer type_cholang) {
		this.type_cholang = type_cholang;
	}

	public Integer getVodyanka() {
		return (vodyanka == null) ? 0 : vodyanka;
	}

	public void setVodyanka(Integer vodyanka) {
		this.vodyanka = vodyanka;
	}

	public Integer getSvich() {
		return (svich == null) ? 0 : svich;
	}

	public void setSvich(Integer svich) {
		this.svich = svich;
	}

	public Integer getSmorch() {
		return (smorch == null) ? 0 : smorch;
	}

	public void setSmorch(Integer smorch) {
		this.smorch = smorch;
	}

	public Integer getPeritonit() {
		return (peritonit == null) ? 0 : peritonit;
	}

	public void setPeritonit(Integer peritonit) {
		this.peritonit = peritonit;
	}

	public Integer getEmpiema() {
		return (empiema == null) ? 0 : empiema;
	}

	public void setEmpiema(Integer empiema) {
		this.empiema = empiema;
	}

	public Integer getCholangit() {
		return (cholangit == null) ? 0 : cholangit;
	}

	public void setCholangit(Integer cholangit) {
		this.cholangit = cholangit;
	}

	public Integer getCholpankreotit() {
		return (cholpankreotit == null) ? 0 : cholpankreotit;
	}

	public void setCholpankreotit(Integer cholpankreotit) {
		this.cholpankreotit = cholpankreotit;
	}

	public int getTrof_status() {
		return trof_status;
	}

	public void setTrof_status(int trof_status) {
		this.trof_status = trof_status;
	}


	public boolean areAllFieldsFilled() {
		if (cholecistit != 0) {
			if (cholecistit == 1 || ostriiCholecistit != 0) {
				if (!isDiagnosisAtStart() || trof_status != 0) {
					if (mirizi == null || mirizi == 0 || (zhelt_mirizi != 0 && type != 0)) {
						if (cholangiol == null || cholangiol == 0 || (zhelt_cholang != 0 && type_cholang != 0)) {
							return true;
						}
					}
				}
			}
			return false;
		}
		return false;
	}
}
