package com.amgureeva.Entities.Diagnosis;

/**
 * Created by Александра on 05.05.2016.
 */
public class AlveokokkozList {
    private String name;
    private String icg; //0,1,2
    private String icgk_f; // 0,1,2
    private String grwr; // 0,1,2
    private String indexZachvata;
    private String objemOstatka;
    private String vorotInvasia; // 0,1,2
    private String pechInvasia; // 0,1,2
    private String polozhKultura;
    private String predOperacii;
    private String albendazol;
    private String trofStatus; // 0,1,2


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcg() {
        return icg;
    }

    public void setIcg(String icg) {
        this.icg = icg;
    }

    public String getIcgk_f() {
        return icgk_f;
    }

    public void setIcgk_f(String icgk_f) {
        this.icgk_f = icgk_f;
    }

    public String getGrwr() {
        return grwr;
    }

    public void setGrwr(String grwr) {
        this.grwr = grwr;
    }

    public String getIndexZachvata() {
        return indexZachvata;
    }

    public void setIndexZachvata(String indexZachvata) {
        this.indexZachvata = indexZachvata;
    }

    public String getObjemOstatka() {
        return objemOstatka;
    }

    public void setObjemOstatka(String objemOstatka) {
        this.objemOstatka = objemOstatka;
    }

    public String getVorotInvasia() {
        return vorotInvasia;
    }

    public void setVorotInvasia(String vorotInvasia) {
        this.vorotInvasia = vorotInvasia;
    }

    public String getPechInvasia() {
        return pechInvasia;
    }

    public void setPechInvasia(String pechInvasia) {
        this.pechInvasia = pechInvasia;
    }

    public String getPolozhKultura() {
        return polozhKultura;
    }

    public void setPolozhKultura(String polozhKultura) {
        this.polozhKultura = polozhKultura;
    }

    public String getPredOperacii() {
        return predOperacii;
    }

    public void setPredOperacii(String predOperacii) {
        this.predOperacii = predOperacii;
    }

    public String getAlbendazol() {
        return albendazol;
    }

    public void setAlbendazol(String albendazol) {
        this.albendazol = albendazol;
    }

    public String getTrofStatus() {
        return trofStatus;
    }

    public void setTrofStatus(String trofStatus) {
        this.trofStatus = trofStatus;
    }
}
