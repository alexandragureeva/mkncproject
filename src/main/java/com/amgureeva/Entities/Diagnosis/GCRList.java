package com.amgureeva.Entities.Diagnosis;

/**
 * Created by Александра on 05.05.2016.
 */
public class GCRList {
    private String name;
    private String cirrozClass;//0,1,2,3 id=46
    private String vrvStepen;//0,1,2,3 id=47
    private String trombozStepen;//0,1,2,3 id=4
    private String etiologiaCirroza;//0,1,2,3 id=11
    private String gepatitStepen;//0,1,2,3,4 id=47
    private String fibrozClass;//0,1,2,3,4,5 id=47
    private String razmerOpucholi; //0,1
    private String chisloOpuchUzlov;//0,1,2,3,4,5,6
    private String opuchUzliType; //0,1
    private String sosudInvasia;//0,1,2
    private String vnepOchagiType;//0,1,2
    private String trombPechType; //0,1,2
    private String onkomarker;
    private String icg; //0,1,2
    private String icgk_f; //0,1,2
    private String indexZachvata;
    private String objemOstatka;
    private String protivTerapia;
    private String trombozVorotVeni;//0,1,2,3,4
    private String lechenieSorafinib;
    private String tache;
    private String bclcType;//0,1,2
    private String trofStatus; // 0,1,2


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCirrozClass() {
        return cirrozClass;
    }

    public void setCirrozClass(String cirrozClass) {
        this.cirrozClass = cirrozClass;
    }

    public String getVrvStepen() {
        return vrvStepen;
    }

    public void setVrvStepen(String vrvStepen) {
        this.vrvStepen = vrvStepen;
    }

    public String getTrombozStepen() {
        return trombozStepen;
    }

    public void setTrombozStepen(String trombozStepen) {
        this.trombozStepen = trombozStepen;
    }

    public String getEtiologiaCirroza() {
        return etiologiaCirroza;
    }

    public void setEtiologiaCirroza(String etiologiaCirroza) {
        this.etiologiaCirroza = etiologiaCirroza;
    }

    public String getGepatitStepen() {
        return gepatitStepen;
    }

    public void setGepatitStepen(String gepatitStepen) {
        this.gepatitStepen = gepatitStepen;
    }

    public String getFibrozClass() {
        return fibrozClass;
    }

    public void setFibrozClass(String fibrozClass) {
        this.fibrozClass = fibrozClass;
    }

    public String getRazmerOpucholi() {
        return razmerOpucholi;
    }

    public void setRazmerOpucholi(String razmerOpucholi) {
        this.razmerOpucholi = razmerOpucholi;
    }

    public String getChisloOpuchUzlov() {
        return chisloOpuchUzlov;
    }

    public void setChisloOpuchUzlov(String chisloOpuchUzlov) {
        this.chisloOpuchUzlov = chisloOpuchUzlov;
    }

    public String getOpuchUzliType() {
        return opuchUzliType;
    }

    public void setOpuchUzliType(String opuchUzliType) {
        this.opuchUzliType = opuchUzliType;
    }

    public String getSosudInvasia() {
        return sosudInvasia;
    }

    public void setSosudInvasia(String sosudInvasia) {
        this.sosudInvasia = sosudInvasia;
    }

    public String getVnepOchagiType() {
        return vnepOchagiType;
    }

    public void setVnepOchagiType(String vnepOchagiType) {
        this.vnepOchagiType = vnepOchagiType;
    }

    public String getTrombPechType() {
        return trombPechType;
    }

    public void setTrombPechType(String trombPechType) {
        this.trombPechType = trombPechType;
    }

    public String getOnkomarker() {
        return onkomarker;
    }

    public void setOnkomarker(String onkomarker) {
        this.onkomarker = onkomarker;
    }

    public String getIcg() {
        return icg;
    }

    public void setIcg(String icg) {
        this.icg = icg;
    }

    public String getIcgk_f() {
        return icgk_f;
    }

    public void setIcgk_f(String icgk_f) {
        this.icgk_f = icgk_f;
    }

    public String getIndexZachvata() {
        return indexZachvata;
    }

    public void setIndexZachvata(String indexZachvata) {
        this.indexZachvata = indexZachvata;
    }

    public String getObjemOstatka() {
        return objemOstatka;
    }

    public void setObjemOstatka(String objemOstatka) {
        this.objemOstatka = objemOstatka;
    }

    public String getProtivTerapia() {
        return protivTerapia;
    }

    public void setProtivTerapia(String protivTerapia) {
        this.protivTerapia = protivTerapia;
    }

    public String getTrombozVorotVeni() {
        return trombozVorotVeni;
    }

    public void setTrombozVorotVeni(String trombozVorotVeni) {
        this.trombozVorotVeni = trombozVorotVeni;
    }

    public String getLechenieSorafinib() {
        return lechenieSorafinib;
    }

    public void setLechenieSorafinib(String lechenieSorafinib) {
        this.lechenieSorafinib = lechenieSorafinib;
    }

    public String getTache() {
        return tache;
    }

    public void setTache(String tache) {
        this.tache = tache;
    }

    public String getBclcType() {
        return bclcType;
    }

    public void setBclcType(String bclcType) {
        this.bclcType = bclcType;
    }

    public String getTrofStatus() {
        return trofStatus;
    }

    public void setTrofStatus(String trofStatus) {
        this.trofStatus = trofStatus;
    }
}
