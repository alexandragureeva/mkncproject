package com.amgureeva.Entities.Diagnosis;

import com.amgureeva.Entities.GeneralDiagnosis;

/**
 * Created by Александра on 23.04.2016.
 */
public class RZHP extends GeneralDiagnosis {
	public RZHP() {

	}

	private int icg; //0,1,2
	private int icgk_f; //0,1,2
	private String indexZachvata;
	private String objemOstatka;
	private int rezekciaType;
	private int stadPervOpuch;
	private int vorotInvasia; // 0,1,2
	private int pechInvasia; // 0,1,2
	private int differOpuchol; // 0,1,2
	private boolean perinevInvasia;
	private boolean polozhKultura;
	private int diagnRaka;
	private int trofStatus; // 0,1,2

	public int getIcg() {
		return icg;
	}

	public void setIcg(int icg) {
		this.icg = icg;
	}

	public int getIcgk_f() {
		return icgk_f;
	}

	public void setIcgk_f(int icgk_f) {
		this.icgk_f = icgk_f;
	}

	public String getIndexZachvata() {
		return (indexZachvata == null || indexZachvata.equals("null")) ? "" : indexZachvata;
	}

	public void setIndexZachvata(String indexZachvata) {
		this.indexZachvata = indexZachvata;
	}

	public String getObjemOstatka() {
		return (objemOstatka == null || objemOstatka.equals("null")) ? "" : objemOstatka;
	}

	public void setObjemOstatka(String objemOstatka) {
		this.objemOstatka = objemOstatka;
	}

	public int getStadPervOpuch() {
		return stadPervOpuch;
	}

	public void setStadPervOpuch(int stadPervOpuch) {
		this.stadPervOpuch = stadPervOpuch;
	}

	public int getVorotInvasia() {
		return vorotInvasia;
	}

	public void setVorotInvasia(int vorotInvasia) {
		this.vorotInvasia = vorotInvasia;
	}

	public int getPechInvasia() {
		return pechInvasia;
	}

	public void setPechInvasia(int pechInvasia) {
		this.pechInvasia = pechInvasia;
	}

	public int getDifferOpuchol() {
		return differOpuchol;
	}

	public void setDifferOpuchol(int differOpuchol) {
		this.differOpuchol = differOpuchol;
	}

	public boolean isPerinevInvasia() {
		return perinevInvasia;
	}

	public void setPerinevInvasia(boolean perinevInvasia) {
		this.perinevInvasia = perinevInvasia;
	}

	public boolean isPolozhKultura() {
		return polozhKultura;
	}

	public void setPolozhKultura(boolean polozhKultura) {
		this.polozhKultura = polozhKultura;
	}

	public int getDiagnRaka() {
		return diagnRaka;
	}

	public void setDiagnRaka(int diagnRaka) {
		this.diagnRaka = diagnRaka;
	}

	public int getTrofStatus() {
		return trofStatus;
	}

	public void setTrofStatus(int trofStatus) {
		this.trofStatus = trofStatus;
	}

	public int getRezekciaType() {
		return rezekciaType;
	}

	public void setRezekciaType(int rezekciaType) {
		this.rezekciaType = rezekciaType;
	}

	public boolean areAllFieldsFilled() {
		if (icg != 0) {
			if (icgk_f != 0) {
				if (stadPervOpuch != 0) {
					if (vorotInvasia != 0) {
						if (pechInvasia != 0) {
							if (differOpuchol != 0) {
								if (!isDiagnosisAtStart() || diagnRaka != 0) {
									if (!isDiagnosisAtStart() || trofStatus != 0) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}