package com.amgureeva.Entities.Diagnosis;

import com.amgureeva.Entities.GeneralDiagnosis;

/**
 * Created by Александра on 23.04.2016.
 */
public class MNKR extends GeneralDiagnosis {

	public MNKR() {
	}

	private int pervOpuchType;
	private int metNeiroOpuch;
	private int metGiso;
	private int nalVnepMet;
	private int razmerOpucholi; //0,1
	private int chisloOpuchUzlov;//0,1,2,3,4,5,6
	private int opuchUzliType; //0,1
	private int sosudInvasia;//0,1,2
	private int vnepOchagiType;//0,1,2
	private int trombPechType; //0,1,2
	private int icg; //0,1,2
	private int icgk_f; //0,1,2
	private int trombozVorotVeni;//0,1,2,3,4
	private String indexZachvata;
	private String objemOstatka;
	private int rezekciaType;
	private boolean nalNeoadChimioTer;
	private int chimiotKurs;
	private int chimiotEffect;
	private int pojavMetType;
	private int pojavMetTime;
	private int trofStatus; // 0,1,2

	public int getPervOpuchType() {
		return pervOpuchType;
	}

	public void setPervOpuchType(int pervOpuchType) {
		this.pervOpuchType = pervOpuchType;
	}

	public int getMetNeiroOpuch() {
		return metNeiroOpuch;
	}

	public void setMetNeiroOpuch(int metNeiroOpuch) {
		this.metNeiroOpuch = metNeiroOpuch;
	}

	public int getMetGiso() {
		return metGiso;
	}

	public void setMetGiso(int metGiso) {
		this.metGiso = metGiso;
	}

	public int getNalVnepMet() {
		return nalVnepMet;
	}

	public void setNalVnepMet(int nalVnepMet) {
		this.nalVnepMet = nalVnepMet;
	}

	public int getRazmerOpucholi() {
		return razmerOpucholi;
	}

	public void setRazmerOpucholi(int razmerOpucholi) {
		this.razmerOpucholi = razmerOpucholi;
	}

	public int getChisloOpuchUzlov() {
		return chisloOpuchUzlov;
	}

	public void setChisloOpuchUzlov(int chisloOpuchUzlov) {
		this.chisloOpuchUzlov = chisloOpuchUzlov;
	}

	public int getOpuchUzliType() {
		return opuchUzliType;
	}

	public void setOpuchUzliType(int opuchUzliType) {
		this.opuchUzliType = opuchUzliType;
	}

	public int getSosudInvasia() {
		return sosudInvasia;
	}

	public void setSosudInvasia(int sosudInvasia) {
		this.sosudInvasia = sosudInvasia;
	}

	public int getVnepOchagiType() {
		return vnepOchagiType;
	}

	public void setVnepOchagiType(int vnepOchagiType) {
		this.vnepOchagiType = vnepOchagiType;
	}

	public int getTrombPechType() {
		return trombPechType;
	}

	public void setTrombPechType(int trombPechType) {
		this.trombPechType = trombPechType;
	}

	public int getIcg() {
		return icg;
	}

	public void setIcg(int icg) {
		this.icg = icg;
	}

	public int getIcgk_f() {
		return icgk_f;
	}

	public void setIcgk_f(int icgk_f) {
		this.icgk_f = icgk_f;
	}

	public int getTrombozVorotVeni() {
		return trombozVorotVeni;
	}

	public void setTrombozVorotVeni(int trombozVorotVeni) {
		this.trombozVorotVeni = trombozVorotVeni;
	}

	public String getIndexZachvata() {
		return (indexZachvata == null || indexZachvata.equals("null")) ? "" : indexZachvata;
	}

	public void setIndexZachvata(String indexZachvata) {
		this.indexZachvata = indexZachvata;
	}

	public String getObjemOstatka() {
		return (objemOstatka == null || objemOstatka.equals("null")) ? "" : objemOstatka;
	}

	public void setObjemOstatka(String objemOstatka) {
		this.objemOstatka = objemOstatka;
	}

	public boolean isNalNeoadChimioTer() {
		return nalNeoadChimioTer;
	}

	public void setNalNeoadChimioTer(boolean nalNeoadChimioTer) {
		this.nalNeoadChimioTer = nalNeoadChimioTer;
	}

	public int getChimiotKurs() {
		return chimiotKurs;
	}

	public void setChimiotKurs(int chimiotKurs) {
		this.chimiotKurs = chimiotKurs;
	}

	public int getChimiotEffect() {
		return chimiotEffect;
	}

	public void setChimiotEffect(int chimiotEffect) {
		this.chimiotEffect = chimiotEffect;
	}

	public int getPojavMetType() {
		return pojavMetType;
	}

	public void setPojavMetType(int pojavMetType) {
		this.pojavMetType = pojavMetType;
	}

	public int getPojavMetTime() {
		return pojavMetTime;
	}

	public void setPojavMetTime(int pojavMetTime) {
		this.pojavMetTime = pojavMetTime;
	}

	public int getTrofStatus() {
		return trofStatus;
	}

	public void setTrofStatus(int trofStatus) {
		this.trofStatus = trofStatus;
	}

	public int getRezekciaType() {
		return rezekciaType;
	}

	public void setRezekciaType(int rezekciaType) {
		this.rezekciaType = rezekciaType;
	}

	public boolean areAllFieldsFilled() {
		if (nalVnepMet != 0) {
			if (razmerOpucholi != 0) {
				if (opuchUzliType != 0) {
					if (opuchUzliType != 2 || chisloOpuchUzlov != 0) {
						if (sosudInvasia != 0) {
							if (vnepOchagiType != 0) {
								if (trombPechType != 0) {
									if (icg != 0) {
										if (icgk_f != 0) {
											if (trombozVorotVeni != 0) {
												if (!isDiagnosisAtStart() || trofStatus != 0) {
													if (pervOpuchType != 0) {
														if (pervOpuchType != 6 || metGiso != 0) {
															if (pervOpuchType != 1 || metNeiroOpuch != 0) {
																if (!isDiagnosisAtStart() || !nalNeoadChimioTer || chimiotKurs != 0) {
																	if (!isDiagnosisAtStart() || !nalNeoadChimioTer || chimiotEffect != 0) {
																		if (!isDiagnosisAtStart() || pojavMetType != 0) {
																			if (!isDiagnosisAtStart() || pojavMetType != 2 || pojavMetTime != 0) {
																				return true;
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}
