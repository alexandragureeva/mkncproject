package com.amgureeva.Entities.Diagnosis;

import com.amgureeva.Entities.GeneralDiagnosis;

/**
 * Created by Александра on 23.04.2016.
 */
public class Alveokokkoz extends GeneralDiagnosis {

	public Alveokokkoz() {
		polozhKultura = "";
	}

	private int icg; //0,1,2
	private int icgk_f; // 0,1,2
	private int grwr; // 0,1,2
	private String indexZachvata;
	private String objemOstatka;
	private int rezekciaType;
	private int vorotInvasia; // 0,1,2
	private int pechInvasia; // 0,1,2
	private String polozhKultura;
	private int predOperacii;
	private String albendazol;
	private int trofStatus; // 0,1,2

	public int getIcg() {
		return icg;
	}

	public void setIcg(int icg) {
		this.icg = icg;
	}

	public int getIcgk_f() {
		return icgk_f;
	}

	public void setIcgk_f(int icgk_f) {
		this.icgk_f = icgk_f;
	}

	public int getGrwr() {
		return grwr;
	}

	public void setGrwr(int grwr) {
		this.grwr = grwr;
	}

	public String getIndexZachvata() {
		return (indexZachvata == null || indexZachvata.equals("null")) ? "" : indexZachvata;
	}

	public void setIndexZachvata(String indexZachvata) {
		this.indexZachvata = indexZachvata;
	}

	public String getObjemOstatka() {
		return (objemOstatka == null || objemOstatka.equals("null")) ? "" : objemOstatka;
	}

	public void setObjemOstatka(String objemOstatka) {
		this.objemOstatka = objemOstatka;
	}

	public int getVorotInvasia() {
		return vorotInvasia;
	}

	public void setVorotInvasia(int vorotInvasia) {
		this.vorotInvasia = vorotInvasia;
	}

	public int getPechInvasia() {
		return pechInvasia;
	}

	public void setPechInvasia(int pechInvasia) {
		this.pechInvasia = pechInvasia;
	}

	public String getPolozhKultura() {
		return polozhKultura;
	}

	public void setPolozhKultura(String polozhKultura) {
		this.polozhKultura = polozhKultura;
	}

	public int getPredOperacii() {
		return predOperacii;
	}

	public void setPredOperacii(int predOperacii) {
		this.predOperacii = predOperacii;
	}

	public String getAlbendazol() {
		return albendazol;
	}

	public void setAlbendazol(String albendazol) {
		this.albendazol = albendazol;
	}

	public int getTrofStatus() {
		return trofStatus;
	}

	public void setTrofStatus(int trofStatus) {
		this.trofStatus = trofStatus;
	}

	public int getRezekciaType() {
		return rezekciaType;
	}

	public void setRezekciaType(int rezekciaType) {
		this.rezekciaType = rezekciaType;
	}

	public boolean areAllFieldsFilled() {
		if (icg != 0) {
			if (icgk_f != 0) {
				if (grwr != 0) {
					if (vorotInvasia != 0) {
						if (pechInvasia != 0) {
							if (!isDiagnosisAtStart() || predOperacii != 0) {
								if (!isDiagnosisAtStart() || trofStatus != 0) {
									return true;
								}
							}
						}

					}
				}
			}
		}
		return false;
	}
}
