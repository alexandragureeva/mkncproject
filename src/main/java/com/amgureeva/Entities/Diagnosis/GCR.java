package com.amgureeva.Entities.Diagnosis;

import com.amgureeva.Entities.GeneralDiagnosis;

/**
 * Created by Александра on 22.04.2016.
 */
public class GCR extends GeneralDiagnosis {

	public GCR() {
	}

	//24 attributes
	private int cirrozClass;//0,1,2,3 id=46
	private int vrvStepen;//0,1,2,3 id=47
	private int trombozStepen;//0,1,2,3 id=4
	private int etiologiaCirroza;//0,1,2,3 id=11
	private int gepatitStepen;//0,1,2,3,4 id=47
	private int fibrozClass;//0,1,2,3,4,5 id=47
	private int razmerOpucholi; //0,1
	private int chisloOpuchUzlov;//0,1,2,3,4,5,6
	private int opuchUzliType; //0,1
	private int sosudInvasia;//0,1,2
	private int vnepOchagiType;//0,1,2
	private int trombPechType;
	private int icg; //0,1,2
	private int icgk_f; //0,1,2
	private String indexZachvata;
	private String objemOstatka;
	private int rezekciaType;
	private boolean protivTerapia;
	private int trombozVorotVeni;//0,1,2,3,4
	private boolean lechenieSorafinib;
	private boolean tache;
	private int bclcType;//0,1,2
	private int trofStatus; // 0,1,2

	public int getCirrozClass() {
		return cirrozClass;
	}

	public void setCirrozClass(int cirrozClass) {
		this.cirrozClass = cirrozClass;
	}

	public int getVrvStepen() {
		return vrvStepen;
	}

	public void setVrvStepen(int vrvStepen) {
		this.vrvStepen = vrvStepen;
	}

	public int getTrombozStepen() {
		return trombozStepen;
	}

	public void setTrombozStepen(int trombozStepen) {
		this.trombozStepen = trombozStepen;
	}

	public int getEtiologiaCirroza() {
		return etiologiaCirroza;
	}

	public void setEtiologiaCirroza(int etiologiaCirroza) {
		this.etiologiaCirroza = etiologiaCirroza;
	}

	public int getGepatitStepen() {
		return gepatitStepen;
	}

	public void setGepatitStepen(int gepatitStepen) {
		this.gepatitStepen = gepatitStepen;
	}

	public int getFibrozClass() {
		return fibrozClass;
	}

	public void setFibrozClass(int fibrozClass) {
		this.fibrozClass = fibrozClass;
	}

	public int getRazmerOpucholi() {
		return razmerOpucholi;
	}

	public void setRazmerOpucholi(int razmerOpucholi) {
		this.razmerOpucholi = razmerOpucholi;
	}

	public int getChisloOpuchUzlov() {
		return chisloOpuchUzlov;
	}

	public void setChisloOpuchUzlov(int chisloOpuchUzlov) {
		this.chisloOpuchUzlov = chisloOpuchUzlov;
	}

	public int getOpuchUzliType() {
		return opuchUzliType;
	}

	public void setOpuchUzliType(int opuchUzliType) {
		this.opuchUzliType = opuchUzliType;
	}

	public int getSosudInvasia() {
		return sosudInvasia;
	}

	public void setSosudInvasia(int sosudInvasia) {
		this.sosudInvasia = sosudInvasia;
	}

	public int getVnepOchagiType() {
		return vnepOchagiType;
	}

	public void setVnepOchagiType(int vnepOchagiType) {
		this.vnepOchagiType = vnepOchagiType;
	}

	public int getTrombPechType() {
		return trombPechType;
	}

	public void setTrombPechType(int trombPechType) {
		this.trombPechType = trombPechType;
	}

	public int getIcg() {
		return icg;
	}

	public void setIcg(int icg) {
		this.icg = icg;
	}

	public int getIcgk_f() {
		return icgk_f;
	}

	public void setIcgk_f(int icgk_f) {
		this.icgk_f = icgk_f;
	}

	public String getIndexZachvata() {
		return (indexZachvata == null || indexZachvata.equals("null")) ? "" : indexZachvata;
	}

	public void setIndexZachvata(String indexZachvata) {
		this.indexZachvata = indexZachvata;
	}

	public String getObjemOstatka() {
		return (objemOstatka == null || objemOstatka.equals("null")) ? "" : objemOstatka;
	}

	public void setObjemOstatka(String objemOstatka) {
		this.objemOstatka = objemOstatka;
	}

	public boolean isProtivTerapia() {
		return protivTerapia;
	}

	public void setProtivTerapia(boolean protivTerapia) {
		this.protivTerapia = protivTerapia;
	}

	public int getTrombozVorotVeni() {
		return trombozVorotVeni;
	}

	public void setTrombozVorotVeni(int trombozVorotVeni) {
		this.trombozVorotVeni = trombozVorotVeni;
	}

	public boolean isLechenieSorafinib() {
		return lechenieSorafinib;
	}

	public void setLechenieSorafinib(boolean lechenieSorafinib) {
		this.lechenieSorafinib = lechenieSorafinib;
	}

	public boolean isTache() {
		return tache;
	}

	public void setTache(boolean tache) {
		this.tache = tache;
	}

	public int getBclcType() {
		return bclcType;
	}

	public void setBclcType(int bclcType) {
		this.bclcType = bclcType;
	}

	public int getTrofStatus() {
		return trofStatus;
	}

	public void setTrofStatus(int trofStatus) {
		this.trofStatus = trofStatus;
	}

	public int getRezekciaType() {
		return rezekciaType;
	}

	public void setRezekciaType(int rezekciaType) {
		this.rezekciaType = rezekciaType;
	}

	public boolean areAllFieldsFilled() {
		if (cirrozClass != 0) {
			if (vrvStepen != 0) {
				if (trombozStepen != 0) {
					if (etiologiaCirroza != 0) {
						if (gepatitStepen != 0) {
							if (fibrozClass != 0) {
								if (razmerOpucholi != 0) {
									if (opuchUzliType != 2 || chisloOpuchUzlov != 0) {
										if (opuchUzliType != 0) {
											if (sosudInvasia != 0) {
												if (vnepOchagiType != 0) {
													if (trombPechType != 0) {
														if (icg != 0) {
															if (icgk_f != 0) {
																if (trombozVorotVeni != 0) {
																	if (bclcType != 0) {
																		if (!isDiagnosisAtStart() || trofStatus != 0) {
																			return true;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}
