package com.amgureeva.Entities.Diagnosis;

import com.amgureeva.Entities.GeneralDiagnosis;

/**
 * Created by Александра on 23.04.2016.
 */
public class Echinokokkoz extends GeneralDiagnosis {

	public Echinokokkoz() {
	}

	private int voz;
	private boolean antitela;
	private int antitelaRes;
	private int antitelaUroven;
	private int epidemAnamnez;
	private boolean mnozhKisty;
	private int kistyNumber;
	private boolean kistyType;
	private String kistyRazmer;
	private int kistyOsl;
	private int kratLechenia;
	private int lechenieType;
	private int vnepPorazhenie;
	private int trofStatus;

	public int getVoz() {
		return voz;
	}

	public void setVoz(int voz) {
		this.voz = voz;
	}

	public boolean isAntitela() {
		return antitela;
	}

	public void setAntitela(boolean antitela) {
		this.antitela = antitela;
	}

	public int getAntitelaRes() {
		return antitelaRes;
	}

	public void setAntitelaRes(int antitelaRes) {
		this.antitelaRes = antitelaRes;
	}

	public int getAntitelaUroven() {
		return antitelaUroven;
	}

	public void setAntitelaUroven(int antitelaUroven) {
		this.antitelaUroven = antitelaUroven;
	}

	public int getEpidemAnamnez() {
		return epidemAnamnez;
	}

	public void setEpidemAnamnez(int epidemAnamnez) {
		this.epidemAnamnez = epidemAnamnez;
	}

	public boolean isMnozhKisty() {
		return mnozhKisty;
	}

	public void setMnozhKisty(boolean mnozhKisty) {
		this.mnozhKisty = mnozhKisty;
	}

	public int getKistyNumber() {
		return kistyNumber;
	}

	public void setKistyNumber(int kistyNumber) {
		this.kistyNumber = kistyNumber;
	}

	public boolean getKistyType() {
		return kistyType;
	}

	public void setKistyType(boolean kistyType) {
		this.kistyType = kistyType;
	}

	public String getKistyRazmer() {
		return (kistyRazmer == null || kistyRazmer.equals("null")) ? "" : kistyRazmer;
	}

	public void setKistyRazmer(String kistyRazmer) {
		this.kistyRazmer = kistyRazmer;
	}

	public int getKistyOsl() {
		return kistyOsl;
	}

	public void setKistyOsl(int kistyOsl) {
		this.kistyOsl = kistyOsl;
	}

	public int getKratLechenia() {
		return kratLechenia;
	}

	public void setKratLechenia(int kratLechenia) {
		this.kratLechenia = kratLechenia;
	}

	public int getLechenieType() {
		return lechenieType;
	}

	public void setLechenieType(int lechenieType) {
		this.lechenieType = lechenieType;
	}

	public int getVnepPorazhenie() {
		return vnepPorazhenie;
	}

	public void setVnepPorazhenie(int vnepPorazhenie) {
		this.vnepPorazhenie = vnepPorazhenie;
	}

	public int getTrofStatus() {
		return trofStatus;
	}

	public void setTrofStatus(int trofStatus) {
		this.trofStatus = trofStatus;
	}

	public boolean areAllFieldsFilled() {
		if (voz != 0) {
			if (!antitela || antitelaRes != 0) {
				if (!isDiagnosisAtStart() || epidemAnamnez != 0) {
					if (vnepPorazhenie != 0) {
						if (kistyRazmer != null && !kistyRazmer.isEmpty()) {
							if (!isDiagnosisAtStart() || trofStatus != 0) {
								if (!isDiagnosisAtStart() || kratLechenia != 0) {
									if (kratLechenia != 2 || lechenieType != 0) {
										if (vnepPorazhenie != 0) {
											if (kistyOsl != 0) {

												return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}
