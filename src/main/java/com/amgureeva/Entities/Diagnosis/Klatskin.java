package com.amgureeva.Entities.Diagnosis;

import com.amgureeva.Entities.GeneralDiagnosis;

/**
 * Created by Александра on 22.04.2016.
 */
public class Klatskin extends GeneralDiagnosis {
	public Klatskin() {
	}

	private int icg; //0,1,2
	private int icgk_f; // 0,1,2
	private int grwr; // 0,1,2
	private String indexZachvata;
	private String objemOstatka;
	private int rezekciaType;
	private int bismuth; // 0,1,2,3,4
	private int vorotInvasia; // 0,1,2
	private int pechInvasia; // 0,1,2
	private int differOpuchol; // 0,1,2
	private String razmer_opucholi;
	private boolean perinevInvasia;
	private boolean polozhKultura;
	private int trofStatus; // 0,1,2

	public int getIcg() {
		return icg;
	}

	public void setIcg(int icg) {
		this.icg = icg;
	}

	public int getIcgk_f() {
		return icgk_f;
	}

	public void setIcgk_f(int icgk_f) {
		this.icgk_f = icgk_f;
	}

	public int getGrwr() {
		return grwr;
	}

	public void setGrwr(int grwr) {
		this.grwr = grwr;
	}

	public String getIndexZachvata() {
		return (indexZachvata == null || indexZachvata.equals("null")) ? "" : indexZachvata;
	}

	public void setIndexZachvata(String indexZachvata) {
		this.indexZachvata = indexZachvata;
	}

	public String getObjemOstatka() {
		return (objemOstatka == null || objemOstatka.equals("null")) ? "" : objemOstatka;
	}

	public void setObjemOstatka(String objemOstatka) {
		this.objemOstatka = objemOstatka;
	}

	public int getBismuth() {
		return bismuth;
	}

	public void setBismuth(int bismuth) {
		this.bismuth = bismuth;
	}

	public int getVorotInvasia() {
		return vorotInvasia;
	}

	public void setVorotInvasia(int vorotInvasia) {
		this.vorotInvasia = vorotInvasia;
	}

	public int getPechInvasia() {
		return pechInvasia;
	}

	public void setPechInvasia(int pechInvasia) {
		this.pechInvasia = pechInvasia;
	}

	public int getDifferOpuchol() {
		return differOpuchol;
	}

	public void setDifferOpuchol(int differOpuchol) {
		this.differOpuchol = differOpuchol;
	}

	public boolean isPerinevInvasia() {
		return perinevInvasia;
	}

	public void setPerinevInvasia(boolean perinevInvasia) {
		this.perinevInvasia = perinevInvasia;
	}

	public boolean isPolozhKultura() {
		return polozhKultura;
	}

	public void setPolozhKultura(boolean polozhKultura) {
		this.polozhKultura = polozhKultura;
	}

	public int getTrofStatus() {
		return trofStatus;
	}

	public void setTrofStatus(int trofStatus) {
		this.trofStatus = trofStatus;
	}

	public int getRezekciaType() {
		return rezekciaType;
	}

	public void setRezekciaType(int rezekciaType) {
		this.rezekciaType = rezekciaType;
	}

	public String getRazmer_opucholi() {
		return razmer_opucholi;
	}

	public void setRazmer_opucholi(String razmer_opucholi) {
		this.razmer_opucholi = razmer_opucholi;
	}

	public boolean areAllFieldsFilled() {
		if (icg != 0) {
			if (icgk_f != 0) {
				if (grwr != 0) {
					if (!isDiagnosisAtStart() || (razmer_opucholi != null && !razmer_opucholi.isEmpty())) {
						if (vorotInvasia != 0) {
							if (pechInvasia != 0) {
								if (differOpuchol != 0) {
									if (bismuth != 0) {
										if (!isDiagnosisAtStart() || trofStatus != 0) {
											return true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}
