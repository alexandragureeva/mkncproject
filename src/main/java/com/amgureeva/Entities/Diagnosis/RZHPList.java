package com.amgureeva.Entities.Diagnosis;

/**
 * Created by Александра on 05.05.2016.
 */
public class RZHPList {
    private String name;
    private String icg; //0,1,2
    private String icgk_f; //0,1,2
    private String indexZachvata;
    private String objemOstatka;
    private String stadPervOpuch;
    private String vorotInvasia; // 0,1,2
    private String pechInvasia; // 0,1,2
    private String differOpuchol; // 0,1,2
    private String perinevInvasia;
    private String polozhKultura;
    private String diagnRaka;
    private String trofStatus; // 0,1,2

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcg() {
        return icg;
    }

    public void setIcg(String icg) {
        this.icg = icg;
    }

    public String getIcgk_f() {
        return icgk_f;
    }

    public void setIcgk_f(String icgk_f) {
        this.icgk_f = icgk_f;
    }

    public String getIndexZachvata() {
        return indexZachvata;
    }

    public void setIndexZachvata(String indexZachvata) {
        this.indexZachvata = indexZachvata;
    }

    public String getObjemOstatka() {
        return objemOstatka;
    }

    public void setObjemOstatka(String objemOstatka) {
        this.objemOstatka = objemOstatka;
    }

    public String getStadPervOpuch() {
        return stadPervOpuch;
    }

    public void setStadPervOpuch(String stadPervOpuch) {
        this.stadPervOpuch = stadPervOpuch;
    }

    public String getVorotInvasia() {
        return vorotInvasia;
    }

    public void setVorotInvasia(String vorotInvasia) {
        this.vorotInvasia = vorotInvasia;
    }

    public String getPechInvasia() {
        return pechInvasia;
    }

    public void setPechInvasia(String pechInvasia) {
        this.pechInvasia = pechInvasia;
    }

    public String getDifferOpuchol() {
        return differOpuchol;
    }

    public void setDifferOpuchol(String differOpuchol) {
        this.differOpuchol = differOpuchol;
    }

    public String getPerinevInvasia() {
        return perinevInvasia;
    }

    public void setPerinevInvasia(String perinevInvasia) {
        this.perinevInvasia = perinevInvasia;
    }

    public String getPolozhKultura() {
        return polozhKultura;
    }

    public void setPolozhKultura(String polozhKultura) {
        this.polozhKultura = polozhKultura;
    }

    public String getDiagnRaka() {
        return diagnRaka;
    }

    public void setDiagnRaka(String diagnRaka) {
        this.diagnRaka = diagnRaka;
    }

    public String getTrofStatus() {
        return trofStatus;
    }

    public void setTrofStatus(String trofStatus) {
        this.trofStatus = trofStatus;
    }
}
