package com.amgureeva.Entities.Diagnosis;

/**
 * Created by Александра on 05.05.2016.
 */
public class MNKRList {
    private String name;
    private String pervOpuchType;
    private String metNeiroOpuch;
    private String metGiso;
    private String nalVnepMet;
    private String razmerOpucholi; //0,1
    private String chisloOpuchUzlov;//0,1,2,3,4,5,6
    private String opuchUzliType; //0,1
    private String sosudInvasia;//0,1,2
    private String vnepOchagiType;//0,1,2
    private String trombPechType; //0,1,2
    private String onkomarker;
    private String icg; //0,1,2
    private String icgk_f; //0,1,2
    private String trombozVorotVeni;//0,1,2,3,4
    private String indexZachvata;
    private String objemOstatka;
    private String nalNeoadChimioTer;
    private String chimiotKurs;
    private String chimiotEffect;
    private String pojavMetType;
    private String pojavMetTime;
    private String trofStatus; // 0,1,2


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPervOpuchType() {
        return pervOpuchType;
    }

    public void setPervOpuchType(String pervOpuchType) {
        this.pervOpuchType = pervOpuchType;
    }

    public String getMetNeiroOpuch() {
        return metNeiroOpuch;
    }

    public void setMetNeiroOpuch(String metNeiroOpuch) {
        this.metNeiroOpuch = metNeiroOpuch;
    }

    public String getMetGiso() {
        return metGiso;
    }

    public void setMetGiso(String metGiso) {
        this.metGiso = metGiso;
    }

    public String getNalVnepMet() {
        return nalVnepMet;
    }

    public void setNalVnepMet(String nalVnepMet) {
        this.nalVnepMet = nalVnepMet;
    }

    public String getRazmerOpucholi() {
        return razmerOpucholi;
    }

    public void setRazmerOpucholi(String razmerOpucholi) {
        this.razmerOpucholi = razmerOpucholi;
    }

    public String getChisloOpuchUzlov() {
        return chisloOpuchUzlov;
    }

    public void setChisloOpuchUzlov(String chisloOpuchUzlov) {
        this.chisloOpuchUzlov = chisloOpuchUzlov;
    }

    public String getOpuchUzliType() {
        return opuchUzliType;
    }

    public void setOpuchUzliType(String opuchUzliType) {
        this.opuchUzliType = opuchUzliType;
    }

    public String getSosudInvasia() {
        return sosudInvasia;
    }

    public void setSosudInvasia(String sosudInvasia) {
        this.sosudInvasia = sosudInvasia;
    }

    public String getVnepOchagiType() {
        return vnepOchagiType;
    }

    public void setVnepOchagiType(String vnepOchagiType) {
        this.vnepOchagiType = vnepOchagiType;
    }

    public String getTrombPechType() {
        return trombPechType;
    }

    public void setTrombPechType(String trombPechType) {
        this.trombPechType = trombPechType;
    }

    public String getOnkomarker() {
        return onkomarker;
    }

    public void setOnkomarker(String onkomarker) {
        this.onkomarker = onkomarker;
    }

    public String getIcg() {
        return icg;
    }

    public void setIcg(String icg) {
        this.icg = icg;
    }

    public String getIcgk_f() {
        return icgk_f;
    }

    public void setIcgk_f(String icgk_f) {
        this.icgk_f = icgk_f;
    }

    public String getTrombozVorotVeni() {
        return trombozVorotVeni;
    }

    public void setTrombozVorotVeni(String trombozVorotVeni) {
        this.trombozVorotVeni = trombozVorotVeni;
    }

    public String getIndexZachvata() {
        return indexZachvata;
    }

    public void setIndexZachvata(String indexZachvata) {
        this.indexZachvata = indexZachvata;
    }

    public String getObjemOstatka() {
        return objemOstatka;
    }

    public void setObjemOstatka(String objemOstatka) {
        this.objemOstatka = objemOstatka;
    }

    public String getNalNeoadChimioTer() {
        return nalNeoadChimioTer;
    }

    public void setNalNeoadChimioTer(String nalNeoadChimioTer) {
        this.nalNeoadChimioTer = nalNeoadChimioTer;
    }

    public String getChimiotKurs() {
        return chimiotKurs;
    }

    public void setChimiotKurs(String chimiotKurs) {
        this.chimiotKurs = chimiotKurs;
    }

    public String getChimiotEffect() {
        return chimiotEffect;
    }

    public void setChimiotEffect(String chimiotEffect) {
        this.chimiotEffect = chimiotEffect;
    }

    public String getPojavMetType() {
        return pojavMetType;
    }

    public void setPojavMetType(String pojavMetType) {
        this.pojavMetType = pojavMetType;
    }

    public String getPojavMetTime() {
        return pojavMetTime;
    }

    public void setPojavMetTime(String pojavMetTime) {
        this.pojavMetTime = pojavMetTime;
    }

    public String getTrofStatus() {
        return trofStatus;
    }

    public void setTrofStatus(String trofStatus) {
        this.trofStatus = trofStatus;
    }
}
