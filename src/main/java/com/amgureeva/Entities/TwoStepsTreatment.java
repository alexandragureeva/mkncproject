package com.amgureeva.Entities;

/**
 * Created by Nikita on 07.10.2016.
 */
public class TwoStepsTreatment {

	private String id;
	private boolean isNew;

	private Integer provedenie2Etapnogo;
	private int variant2Etapnogo;

	private int okklusia;

	private int portoembType;
	private Integer microspheres;
	private Integer spirt;
	private Integer lipiodol;
	private Integer otherMaterial;

	private int razobsheniePecheni;
	private int dostup;
	private int rezekcia;
	private int vmeshatelstvo;


	private int perevyazkaType;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean aNew) {
		isNew = aNew;
	}

	public Integer getProvedenie2Etapnogo() {
		return (provedenie2Etapnogo == null) ? 0 : provedenie2Etapnogo;
	}

	public void setProvedenie2Etapnogo(Integer provedenie2Etapnogo) {
		this.provedenie2Etapnogo = provedenie2Etapnogo;
	}

	public int getPortoembType() {
		return portoembType;
	}

	public void setPortoembType(int portoembType) {
		this.portoembType = portoembType;
	}

	public Integer getMicrospheres() {
		return (microspheres == null) ? 0 : microspheres;
	}

	public void setMicrospheres(Integer microspheres) {
		this.microspheres = microspheres;
	}

	public Integer getSpirt() {
		return (spirt == null) ? 0 : spirt;
	}

	public void setSpirt(Integer spirt) {
		this.spirt = spirt;
	}

	public Integer getLipiodol() {
		return (lipiodol == null) ? 0 : lipiodol;
	}

	public void setLipiodol(Integer lipiodol) {
		this.lipiodol = lipiodol;
	}

/*	public Integer getNepolnayaPortoemb() {
		return (nepolnayaPortoemb == null) ? 0 : nepolnayaPortoemb;
	}

	public void setNepolnayaPortoemb(Integer nepolnayaPortoemb) {
		this.nepolnayaPortoemb = nepolnayaPortoemb;
	}

	public int getNepolnayaPortoembType() {
		return nepolnayaPortoembType;
	}

	public void setNepolnayaPortoembType(int nepolnayaPortoembType) {
		this.nepolnayaPortoembType = nepolnayaPortoembType;
	}

	public Integer getNepolnayaGipertrofiaPortembol() {
		return (nepolnayaGipertrofiaPortembol == null) ? 0 : nepolnayaGipertrofiaPortembol;
	}

	public void setNepolnayaGipertrofiaPortembol(Integer nepolnayaGipertrofiaPortembol) {
		this.nepolnayaGipertrofiaPortembol = nepolnayaGipertrofiaPortembol;
	}

	public int getNepolnayaGipertrofiaPortembolType() {
		return nepolnayaGipertrofiaPortembolType;
	}

	public void setNepolnayaGipertrofiaPortembolType(int nepolnayaGipertrofiaPortembolType) {
		this.nepolnayaGipertrofiaPortembolType = nepolnayaGipertrofiaPortembolType;
	}

	public Integer getPostEmbSyndrom() {
		return (postEmbSyndrom == null) ? 0 : postEmbSyndrom;
	}

	public void setPostEmbSyndrom(Integer postEmbSyndrom) {
		this.postEmbSyndrom = postEmbSyndrom;
	}

	public Integer getKrovotechenie() {
		return (krovotechenie == null) ? 0 : krovotechenie;
	}

	public void setKrovotechenie(Integer krovotechenie) {
		this.krovotechenie = krovotechenie;
	}

	public Integer getPechNedostatochnost() {
		return (pechNedostatochnost == null) ? 0 : pechNedostatochnost;
	}

	public void setPechNedostatochnost(Integer pechNedostatochnost) {
		this.pechNedostatochnost = pechNedostatochnost;
	}

	public Float getObjem() {
		return objem;
	}

	public void setObjem(Float objem) {
		this.objem = objem;
	}

	public Float getDostignuta() {
		return dostignuta;
	}

	public void setDostignuta(Float dostignuta) {
		this.dostignuta = dostignuta;
	}

	public Float getBudushOstPech() {
		return budushOstPech;
	}

	public void setBudushOstPech(Float budushOstPech) {
		this.budushOstPech = budushOstPech;
	}*/

	public int getRazobsheniePecheni() {
		return razobsheniePecheni;
	}

	public void setRazobsheniePecheni(int razobsheniePecheni) {
		this.razobsheniePecheni = razobsheniePecheni;
	}

	public int getDostup() {
		return dostup;
	}

	public void setDostup(int dostup) {
		this.dostup = dostup;
	}

	public int getRezekcia() {
		return rezekcia;
	}

	public void setRezekcia(int rezekcia) {
		this.rezekcia = rezekcia;
	}

	public int getVmeshatelstvo() {
		return vmeshatelstvo;
	}

	public void setVmeshatelstvo(int vmeshatelstvo) {
		this.vmeshatelstvo = vmeshatelstvo;
	}

	/*public Integer getNepolnayaGipertrofiaALPPS() {
		return (nepolnayaGipertrofiaALPPS == null) ? 0 : nepolnayaGipertrofiaALPPS;
	}

	public void setNepolnayaGipertrofiaALPPS(Integer nepolnayaGipertrofiaALPPS) {
		this.nepolnayaGipertrofiaALPPS = nepolnayaGipertrofiaALPPS;
	}

	public int getNepolnayaGipertrofiaALPPSType() {
		return nepolnayaGipertrofiaALPPSType;
	}

	public void setNepolnayaGipertrofiaALPPSType(int nepolnayaGipertrofiaALPPSType) {
		this.nepolnayaGipertrofiaALPPSType = nepolnayaGipertrofiaALPPSType;
	}

	public Integer getPechNedostatochnostALPPS() {
		return (pechNedostatochnostALPPS == null) ? 0 : pechNedostatochnostALPPS;
	}

	public void setPechNedostatochnostALPPS(Integer pechNedostatochnostALPPS) {
		this.pechNedostatochnostALPPS = pechNedostatochnostALPPS;
	}

	public Integer getZelcheistechenie() {
		return (zelcheistechenie == null) ? 0 : zelcheistechenie;
	}

	public void setZelcheistechenie(Integer zelcheistechenie) {
		this.zelcheistechenie = zelcheistechenie;
	}

	public Integer getCholangit() {
		return (cholangit == null) ? 0 : cholangit;
	}

	public void setCholangit(Integer cholangit) {
		this.cholangit = cholangit;
	}

	public Integer getNagnoenie() {
		return (nagnoenie == null) ? 0 : nagnoenie;
	}

	public void setNagnoenie(Integer nagnoenie) {
		this.nagnoenie = nagnoenie;
	}*/

	public int getPerevyazkaType() {
		return perevyazkaType;
	}

	public void setPerevyazkaType(int perevyazkaType) {
		this.perevyazkaType = perevyazkaType;
	}

	/*public Integer getGemobilia() {
		return gemobilia;
	}

	public void setGemobilia(Integer gemobilia) {
		this.gemobilia = gemobilia;
	}
*/
	public int getVariant2Etapnogo() {
		return variant2Etapnogo;
	}

	public void setVariant2Etapnogo(int variant2Etapnogo) {
		this.variant2Etapnogo = variant2Etapnogo;
	}

	public int getOkklusia() {
		return okklusia;
	}

	public void setOkklusia(int okklusia) {
		this.okklusia = okklusia;
	}

	public Integer getOtherMaterial() {
		return otherMaterial;
	}

	public void setOtherMaterial(Integer otherMaterial) {
		this.otherMaterial = otherMaterial;
	}

/*	public Integer getZhelcheistechenieP() {
		return zhelcheistechenieP;
	}

	public void setZhelcheistechenieP(Integer zhelcheistechenieP) {
		this.zhelcheistechenieP = zhelcheistechenieP;
	}

	public Integer getOtherP() {
		return otherP;
	}

	public void setOtherP(Integer otherP) {
		this.otherP = otherP;
	}

	public Integer getPostoblSyndrom() {
		return postoblSyndrom;
	}

	public void setPostoblSyndrom(Integer postoblSyndrom) {
		this.postoblSyndrom = postoblSyndrom;
	}

	public Integer getOtherA() {
		return otherA;
	}

	public void setOtherA(Integer otherA) {
		this.otherA = otherA;
	}

	public Integer getFebLichoradka() {
		return febLichoradka;
	}

	public void setFebLichoradka(Integer febLichoradka) {
		this.febLichoradka = febLichoradka;
	}

	public Integer getKrovotechenieV() {
		return krovotechenieV;
	}

	public void setKrovotechenieV(Integer krovotechenieV) {
		this.krovotechenieV = krovotechenieV;
	}

	public Integer getAbscess() {
		return abscess;
	}

	public void setAbscess(Integer abscess) {
		this.abscess = abscess;
	}

	public Integer getZhidkSkoplenia() {
		return zhidkSkoplenia;
	}

	public void setZhidkSkoplenia(Integer zhidkSkoplenia) {
		this.zhidkSkoplenia = zhidkSkoplenia;
	}

	public Integer getZhelcheistechV() {
		return zhelcheistechV;
	}

	public void setZhelcheistechV(Integer zhelcheistechV) {
		this.zhelcheistechV = zhelcheistechV;
	}

	public Integer getTromboz() {
		return tromboz;
	}

	public void setTromboz(Integer tromboz) {
		this.tromboz = tromboz;
	}*/
}
