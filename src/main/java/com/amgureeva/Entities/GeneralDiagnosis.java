package com.amgureeva.Entities;

/**
 * Created by Nikita on 18.08.2016.
 */
public class GeneralDiagnosis {
	private String id;
	private boolean isNew;
	private boolean diagnosisAtStart;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean aNew) {
		isNew = aNew;
	}

	public boolean isDiagnosisAtStart() {
		return diagnosisAtStart;
	}

	public void setDiagnosisAtStart(boolean diagnosisAtStart) {
		this.diagnosisAtStart = diagnosisAtStart;
	}
}
