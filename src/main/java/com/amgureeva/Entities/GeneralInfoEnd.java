package com.amgureeva.Entities;

/**
 * Created by Nikita on 29.08.2016.
 */
public class GeneralInfoEnd {
	private int id;
	private String endDate;
	private String deathDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(String deathDate) {
		this.deathDate = deathDate;
	}
}
