package com.amgureeva.Entities;

/**
 * Created by Nikita on 14.08.2016.
 */
public class Analysis {

	private String id;
	private int day;
	private Float gemoglabin;
	private Float eritrociti;
	private Float leikociti;
	private Float palochkoyadernaya;
	private Float segmentarnoyadernaya;
	private Float yunie;
	private Float monociti;
	private Float limfociti;
	private Float trombociti;
	private Float soe;
	private Float ph;
	private Float belok;
	private Float leikocitiMochi;
	private Float eritrocitiMochi;
	private Float glukoza;
	private Float ketoni;
	private Float ast;
	private Float alt;
	private Float obshiiBelok;
	private Float albunin;
	private Float obshiibilirubin;
	private Float pryamoibilirubin;
	private Float kreatinin;
	private Float sacharKrovi;
	private Float mochevayaKislota;
	private Float mochevina;
	private Float mno;
	private Float achtv;
	private Integer gruppaKrovi;
	private int rezFactor;
	private Integer onk199;
	private Integer cea;
	private Integer alfoFetoprotein;
	private Integer ca125;

	private Float onk199Value;
	private Float ceaValue;
	private Float alfoFetValue;
	private Float ca125Value;
	

	private boolean isNew;

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean aNew) {
		isNew = aNew;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public Float getGemoglabin() {
		return gemoglabin;
	}

	public void setGemoglabin(Float gemoglabin) {
		this.gemoglabin = gemoglabin;
	}

	public Float getEritrociti() {
		return eritrociti;
	}

	public void setEritrociti(Float eritrociti) {
		this.eritrociti = eritrociti;
	}

	public Float getLeikociti() {
		return leikociti;
	}

	public void setLeikociti(Float leikociti) {
		this.leikociti = leikociti;
	}

	public Float getPalochkoyadernaya() {
		return palochkoyadernaya;
	}

	public void setPalochkoyadernaya(Float palochkoyadernaya) {
		this.palochkoyadernaya = palochkoyadernaya;
	}

	public Float getSegmentarnoyadernaya() {
		return segmentarnoyadernaya;
	}

	public void setSegmentarnoyadernaya(Float segmentarnoyadernaya) {
		this.segmentarnoyadernaya = segmentarnoyadernaya;
	}

	public Float getYunie() {
		return yunie;
	}

	public void setYunie(Float yunie) {
		this.yunie = yunie;
	}

	public Float getMonociti() {
		return monociti;
	}

	public void setMonociti(Float monociti) {
		this.monociti = monociti;
	}

	public Float getLimfociti() {
		return limfociti;
	}

	public void setLimfociti(Float limfociti) {
		this.limfociti = limfociti;
	}

	public Float getTrombociti() {
		return trombociti;
	}

	public void setTrombociti(Float trombociti) {
		this.trombociti = trombociti;
	}

	public Float getSoe() {
		return soe;
	}

	public void setSoe(Float soe) {
		this.soe = soe;
	}

	public Float getPh() {
		return ph;
	}

	public void setPh(Float ph) {
		this.ph = ph;
	}

	public Float getBelok() {
		return belok;
	}

	public void setBelok(Float belok) {
		this.belok = belok;
	}

	public Float getLeikocitiMochi() {
		return leikocitiMochi;
	}

	public void setLeikocitiMochi(Float leikocitiMochi) {
		this.leikocitiMochi = leikocitiMochi;
	}

	public Float getEritrocitiMochi() {
		return eritrocitiMochi;
	}

	public void setEritrocitiMochi(Float eritrocitiMochi) {
		this.eritrocitiMochi = eritrocitiMochi;
	}

	public Float getGlukoza() {
		return glukoza;
	}

	public void setGlukoza(Float glukoza) {
		this.glukoza = glukoza;
	}

	public Float getKetoni() {
		return ketoni;
	}

	public void setKetoni(Float ketoni) {
		this.ketoni = ketoni;
	}

	public Float getAst() {
		return ast;
	}

	public void setAst(Float ast) {
		this.ast = ast;
	}

	public Float getAlt() {
		return alt;
	}

	public void setAlt(Float alt) {
		this.alt = alt;
	}

	public Float getObshiiBelok() {
		return obshiiBelok;
	}

	public void setObshiiBelok(Float obshiiBelok) {
		this.obshiiBelok = obshiiBelok;
	}

	public Float getAlbunin() {
		return albunin;
	}

	public void setAlbunin(Float albunin) {
		this.albunin = albunin;
	}

	public Float getObshiibilirubin() {
		return obshiibilirubin;
	}

	public void setObshiibilirubin(Float obshiibilirubin) {
		this.obshiibilirubin = obshiibilirubin;
	}

	public Float getPryamoibilirubin() {
		return pryamoibilirubin;
	}

	public void setPryamoibilirubin(Float pryamoibilirubin) {
		this.pryamoibilirubin = pryamoibilirubin;
	}

	public Float getKreatinin() {
		return kreatinin;
	}

	public void setKreatinin(Float kreatinin) {
		this.kreatinin = kreatinin;
	}

	public Float getSacharKrovi() {
		return sacharKrovi;
	}

	public void setSacharKrovi(Float sacharKrovi) {
		this.sacharKrovi = sacharKrovi;
	}

	public Float getMochevayaKislota() {
		return mochevayaKislota;
	}

	public void setMochevayaKislota(Float mochevayaKislota) {
		this.mochevayaKislota = mochevayaKislota;
	}

	public Float getMochevina() {
		return mochevina;
	}

	public void setMochevina(Float mochevina) {
		this.mochevina = mochevina;
	}

	public Float getMno() {
		return mno;
	}

	public void setMno(Float mno) {
		this.mno = mno;
	}

	public Float getAchtv() {
		return achtv;
	}

	public void setAchtv(Float achtv) {
		this.achtv = achtv;
	}

	public Integer getGruppaKrovi() {
		return gruppaKrovi;
	}

	public void setGruppaKrovi(Integer gruppaKrovi) {
		this.gruppaKrovi = gruppaKrovi;
	}

	public Integer getOnk199() {
		return onk199;
	}

	public void setOnk199(Integer onk199) {
		this.onk199 = onk199;
	}

	public Integer getCea() {
		return cea;
	}

	public void setCea(Integer cea) {
		this.cea = cea;
	}

	public Integer getAlfoFetoprotein() {
		return alfoFetoprotein;
	}

	public void setAlfoFetoprotein(Integer alfoFetoprotein) {
		this.alfoFetoprotein = alfoFetoprotein;
	}

	public Integer getCa125() {
		return ca125;
	}

	public void setCa125(Integer ca125) {
		this.ca125 = ca125;
	}

	public Float getOnk199Value() {
		return onk199Value;
	}

	public void setOnk199Value(Float onk199Value) {
		this.onk199Value = onk199Value;
	}

	public Float getCeaValue() {
		return ceaValue;
	}

	public void setCeaValue(Float ceaValue) {
		this.ceaValue = ceaValue;
	}

	public Float getAlfoFetValue() {
		return alfoFetValue;
	}

	public void setAlfoFetValue(Float alfoFetValue) {
		this.alfoFetValue = alfoFetValue;
	}

	public Float getCa125Value() {
		return ca125Value;
	}

	public void setCa125Value(Float ca125Value) {
		this.ca125Value = ca125Value;
	}

	public int getRezFactor() {
		return rezFactor;
	}

	public void setRezFactor(int rezFactor) {
		this.rezFactor = rezFactor;
	}
}
