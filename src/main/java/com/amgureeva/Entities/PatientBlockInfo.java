package com.amgureeva.Entities;

/**
 * Created by Nikita on 25.08.2016.
 */
public class PatientBlockInfo {

	private String id;

	private String blockId;
	private String userName;
	private String patientName;
	private String patientBlock;
	private long dateDiff;
	private String day;
	private String toDate;
	private boolean expired;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientBlock() {
		return patientBlock;
	}

	public void setPatientBlock(String patientBlock) {
		this.patientBlock = patientBlock;
	}

	public long getDateDiff() {
		return dateDiff;
	}

	public void setDateDiff(int dateDiff) {
		this.dateDiff = dateDiff;
		switch (dateDiff) {
			case 0:
				day = "дней";
				break;
			case 1:
				day = "день";
				break;
			case 2:
				day = "дня";
				break;
			case 3:
				day = "дня";
				break;
			default:
				if (dateDiff % 100 > 20) {
					if (dateDiff % 10 == 2 || dateDiff % 10 == 3 || dateDiff % 10 == 4) {
						day = "дня";
					}
					if (dateDiff % 10 == 1) {
						day = "день";
					}
				} else {
					day = "дней";
				}
				break;
		}

	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public String getBlockId() {
		return blockId;
	}

	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}
}
