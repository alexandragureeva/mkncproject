package com.amgureeva.Entities.PatientClasses;

/**
 * Created by Nikita on 01.09.2016.
 */
public class ObjectiveExaminationList {
	public static String getAllVariants(String s) {
		if (s.equals("ОБ_ОС_СОСТ_БОЛ")) {
			return "1 - \"Удовлетворительное\"; 2 - \"Средняя степень тяжести\"; 3 - \"Тяжелое\"";
		}
		if (s.equals("ОБ_ОС_ТЕМПЕР")) {
			return "1 - \"Нормальная\"; 2 - \"37-38\"; 3 - \">38\"";
		}
		if (s.equals("ОБ_ОС_ОДЫШКА")) {
			return "1 - \"Нет\"; 2 - \"Есть, ЧДД >20\"; 3 - \"Есть, ЧДД>30\"";
		}
		if (s.equals("ОБ_ОС_ПУЛЬС")) {
			return "1 - \"Ритмичный\"; 2 - \"Аритмичный\"";
		}
		if (s.equals("ОБ_ОС_ТАХИКАРД")) {
			return "1 - \"Нет\"; 2 - \"ЧСС < 100\"; 3 - \"ЧСС > 100\"";
		}
		if (s.equals("ОБ_ОС_РВОТА")) {
			return "1 - \"Нет\"; 2 - \"< 1 литра\"; 3 - \"> 1 литра\"";
		}
		if (s.equals("ОБ_ОС_ЖИВОТ")) {
			return "1 - \"Увеличен\"; " +
					"2 - \"Не увеличен\"";
		}
		if (s.equals("ОБ_ОС_ПЕРИСТАЛ")) {
			return "1 - \"Нормальная\"; 2 - \"Усиленная\"; 3 - \"Ослабленная\"";
		}
		if (s.equals("ОБ_ОС_СТУЛ")) {
			return "1 - \"Регулярный\"; 2 - \"Задержка\"; 3 - \"Диарея\"";
		}
		if (s.equals("ОБ_ОС_ОБ_МОЧИ")) {
			return "1 - \"<500 мл\"; 2 - \"500-1000 мл\"; 3 - \">1000 мл\"";
		}
		return "";
	}
}
