package com.amgureeva.Entities;

import com.amgureeva.Entities.Diagnosis.*;
import com.amgureeva.Entities.Surgeries.*;

/**
 * Created by Александра on 23.04.2016.
 */
public class GeneralPatientInfo {

	public  GeneralPatientInfo(GeneralPatientInfo copy) {
		this.patient = copy.getPatient();
		this.objectiveExamination = copy.getObjectiveExamination();
		this.analysis0 = copy.getAnalysis0();
		this.analysis1 = copy.getAnalysis1();
		this.analysis5 = copy.getAnalysis5();
		this.analysis10 = copy.getAnalysis10();
		this.klatskin = copy.getKlatskin();
		this.klatskinEnd = copy.getKlatskinEnd();
		this.gcr = copy.getGcr();
		this.gcrEnd = copy.getGcrEnd();
		this.hcr = copy.getHcr();
		this.hcrEnd = copy.getHcrEnd();
		this.mkr = copy.getMkr();
		this.mkrEnd = copy.getMkrEnd();
		this.mnkr = copy.getMnkr();
		this.mnkrEnd = copy.getMnkrEnd();
		this.alveokokkoz = copy.getAlveokokkoz();
		this.alveokokkozEnd = copy.getAlveokokkozEnd();
		this.echinokokkoz = copy.getEchinokokkoz();
		this.echinokokkozEnd = copy.getEchinokokkozEnd();
		this.rzhp = copy.getRzhp();
		this.zhkb = copy.getZhkb();
		this.rzhpEnd = copy.getRzhpEnd();
		this.opalv = copy.getOpalv();
		this.opech = copy.getOpech();
		this.opgcr = copy.getOpgcr();
		this.ophcr = copy.getOphcr();
		this.opZhkb = copy.getOpZhkb();
		this.opmkr = copy.getOpmkr();
		this.opmnkr = copy.getOpmnkr();
		this.opkl = copy.getOpkl();
		this.oprzhp = copy.getOprzhp();
		this.drainNumber = copy.getDrainNumber();
		this.dren = copy.getDren();
		this.hasOsl = copy.getHasOsl();
	}
    private Patient patient;
	private ObjectiveExamination objectiveExamination;
	private Analysis analysis0, analysis1, analysis5, analysis10;
    private Klatskin klatskin, klatskinEnd;
    private GCR gcr, gcrEnd;
    private HCR hcr, hcrEnd;
    private MKR mkr, mkrEnd;
    private RZHP rzhp, rzhpEnd;
    private MNKR mnkr, mnkrEnd;
    private Echinokokkoz echinokokkoz, echinokokkozEnd;
    private Alveokokkoz alveokokkoz, alveokokkozEnd;
	private ZHKB zhkb;
    private OpKlatskin opkl;
    private OpRZHP oprzhp;
    private OpAlveokokkoz opalv;
    private OpEchinokokkoz opech;
    private OpMKR opmkr;
    private OpMNKR opmnkr;
    private OpHCR ophcr;
    private OpGCR opgcr;
	private OpZhkb opZhkb;
    private Drenirovanie dren;
	private Integer drainNumber;
	private boolean hasOsl;

    public GeneralPatientInfo() {
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Klatskin getKlatskin() {
        return klatskin;
    }

    public void setKlatskin(Klatskin klatskin) {
        this.klatskin = klatskin;
    }

    public GCR getGcr() {
        return gcr;
    }

    public void setGcr(GCR gcr) {
        this.gcr = gcr;
    }

    public HCR getHcr() {
        return hcr;
    }

    public void setHcr(HCR hcr) {
        this.hcr = hcr;
    }

    public MKR getMkr() {
        return mkr;
    }

    public void setMkr(MKR mkr) {
        this.mkr = mkr;
    }

    public RZHP getRzhp() {
        return rzhp;
    }

    public void setRzhp(RZHP rzhp) {
        this.rzhp = rzhp;
    }

    public MNKR getMnkr() {
        return mnkr;
    }

    public void setMnkr(MNKR mnkr) {
        this.mnkr = mnkr;
    }

    public Echinokokkoz getEchinokokkoz() {
        return echinokokkoz;
    }

    public void setEchinokokkoz(Echinokokkoz echinokokkoz) {
        this.echinokokkoz = echinokokkoz;
    }

    public Alveokokkoz getAlveokokkoz() {
        return alveokokkoz;
    }

    public void setAlveokokkoz(Alveokokkoz alveokokkoz) {
        this.alveokokkoz = alveokokkoz;
    }

    public OpKlatskin getOpkl() {
        return opkl;
    }

    public void setOpkl(OpKlatskin opkl) {
        this.opkl = opkl;
    }

    public OpRZHP getOprzhp() {
        return oprzhp;
    }

    public void setOprzhp(OpRZHP oprzhp) {
        this.oprzhp = oprzhp;
    }

    public OpAlveokokkoz getOpalv() {
        return opalv;
    }

    public void setOpalv(OpAlveokokkoz opalv) {
        this.opalv = opalv;
    }

    public OpEchinokokkoz getOpech() {
        return opech;
    }

    public void setOpech(OpEchinokokkoz opech) {
        this.opech = opech;
    }

    public OpMKR getOpmkr() {
        return opmkr;
    }

    public void setOpmkr(OpMKR opmkr) {
        this.opmkr = opmkr;
    }

    public OpMNKR getOpmnkr() {
        return opmnkr;
    }

    public void setOpmnkr(OpMNKR opmnkr) {
        this.opmnkr = opmnkr;
    }

    public OpHCR getOphcr() {
        return ophcr;
    }

    public void setOphcr(OpHCR ophcr) {
        this.ophcr = ophcr;
    }

    public OpGCR getOpgcr() {
        return opgcr;
    }

    public void setOpgcr(OpGCR opgcr) {
        this.opgcr = opgcr;
    }

    public Drenirovanie getDren() {
        return dren;
    }

    public void setDren(Drenirovanie dren) {
        this.dren = dren;
    }

    public Klatskin getKlatskinEnd() {
        return klatskinEnd;
    }

    public void setKlatskinEnd(Klatskin klatskinEnd) {
        this.klatskinEnd = klatskinEnd;
    }

    public GCR getGcrEnd() {
        return gcrEnd;
    }

    public void setGcrEnd(GCR gcrEnd) {
        this.gcrEnd = gcrEnd;
    }

    public HCR getHcrEnd() {
        return hcrEnd;
    }

    public void setHcrEnd(HCR hcrEnd) {
        this.hcrEnd = hcrEnd;
    }

    public MKR getMkrEnd() {
        return mkrEnd;
    }

    public void setMkrEnd(MKR mkrEnd) {
        this.mkrEnd = mkrEnd;
    }

    public RZHP getRzhpEnd() {
        return rzhpEnd;
    }

    public void setRzhpEnd(RZHP rzhpEnd) {
        this.rzhpEnd = rzhpEnd;
    }

    public MNKR getMnkrEnd() {
        return mnkrEnd;
    }

    public void setMnkrEnd(MNKR mnkrEnd) {
        this.mnkrEnd = mnkrEnd;
    }

    public Echinokokkoz getEchinokokkozEnd() {
        return echinokokkozEnd;
    }

    public void setEchinokokkozEnd(Echinokokkoz echinokokkozEnd) {
        this.echinokokkozEnd = echinokokkozEnd;
    }

    public Alveokokkoz getAlveokokkozEnd() {
        return alveokokkozEnd;
    }

    public void setAlveokokkozEnd(Alveokokkoz alveokokkozEnd) {
        this.alveokokkozEnd = alveokokkozEnd;
    }

	public ObjectiveExamination getObjectiveExamination() {
		return objectiveExamination;
	}

	public void setObjectiveExamination(ObjectiveExamination objectiveExamination) {
		this.objectiveExamination = objectiveExamination;
	}

	public Analysis getAnalysis0() {
		return analysis0;
	}

	public void setAnalysis0(Analysis analysis0) {
		this.analysis0 = analysis0;
	}

	public Analysis getAnalysis1() {
		return analysis1;
	}

	public void setAnalysis1(Analysis analysis1) {
		this.analysis1 = analysis1;
	}

	public Analysis getAnalysis5() {
		return analysis5;
	}

	public void setAnalysis5(Analysis analysis5) {
		this.analysis5 = analysis5;
	}

	public Analysis getAnalysis10() {
		return analysis10;
	}

	public void setAnalysis10(Analysis analysis10) {
		this.analysis10 = analysis10;
	}

	public Integer getDrainNumber() {
		return drainNumber;
	}

	public void setDrainNumber(Integer drainNumber) {
		this.drainNumber = drainNumber;
	}

	public boolean getHasOsl() {
		return hasOsl;
	}

	public void setHasOsl(boolean hasOsl) {
		this.hasOsl = hasOsl;
	}

	public ZHKB getZhkb() {
		return zhkb;
	}

	public void setZhkb(ZHKB zhkb) {
		this.zhkb = zhkb;
	}

	public OpZhkb getOpZhkb() {
		return opZhkb;
	}

	public void setOpZhkb(OpZhkb opZhkb) {
		this.opZhkb = opZhkb;
	}
}
