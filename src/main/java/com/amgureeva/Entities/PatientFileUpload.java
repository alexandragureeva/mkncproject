package com.amgureeva.Entities;

import java.io.InputStream;

/**
 * Created by Nikita on 09.09.2016.
 */
public class PatientFileUpload {

	private int patientId;
	private int fileId;
	private InputStream fileToUpload;
	private String fileName;

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public InputStream getFileToUpload() {
		return fileToUpload;
	}

	public void setFileToUpload(InputStream fileToUpload) {
		this.fileToUpload = fileToUpload;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
