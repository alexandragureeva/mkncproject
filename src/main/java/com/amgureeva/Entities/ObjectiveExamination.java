package com.amgureeva.Entities;

/**
 * Created by Nikita on 16.08.2016.
 */
public class ObjectiveExamination {

	private String id;
	private Integer patientState;
	private Integer temperature;
	private Integer nalichieZheltuchi;
	private Integer odishka;
	private Integer puls;
	private Integer tachikardia;
	private String davlenie;
	private Integer nalichieToshnoti;
	private Integer rvota;
	private Integer zhivot;
	private Integer boleznenostZhivota;
	private Integer napryazhenieZhivota;
	private Integer peritorialSymptomi;
	private Integer peristaltika;
	private Integer otchozhdenieGazov;
	private Integer stul;
	private Integer dizuria;
	private Integer objemMochi;

	private boolean isNew;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getPatientState() {
		return patientState;
	}

	public void setPatientState(Integer patientState) {
		this.patientState = patientState;
	}

	public Integer getTemperature() {
		return temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public Integer getNalichieZheltuchi() {
		return nalichieZheltuchi;
	}

	public void setNalichieZheltuchi(Integer nalichieZheltuchi) {
		this.nalichieZheltuchi = nalichieZheltuchi;
	}

	public Integer getOdishka() {
		return odishka;
	}

	public void setOdishka(Integer odishka) {
		this.odishka = odishka;
	}

	public Integer getPuls() {
		return puls;
	}

	public void setPuls(Integer puls) {
		this.puls = puls;
	}

	public Integer getTachikardia() {
		return tachikardia;
	}

	public void setTachikardia(Integer tachikardia) {
		this.tachikardia = tachikardia;
	}

	public String getDavlenie() {
		return davlenie;
	}

	public void setDavlenie(String davlenie) {
		this.davlenie = davlenie;
	}

	public Integer getNalichieToshnoti() {
		return nalichieToshnoti;
	}

	public void setNalichieToshnoti(Integer nalichieToshnoti) {
		this.nalichieToshnoti = nalichieToshnoti;
	}

	public Integer getRvota() {
		return rvota;
	}

	public void setRvota(Integer rvota) {
		this.rvota = rvota;
	}

	public Integer getZhivot() {
		return zhivot;
	}

	public void setZhivot(Integer zhivot) {
		this.zhivot = zhivot;
	}

	public Integer getBoleznenostZhivota() {
		return boleznenostZhivota;
	}

	public void setBoleznenostZhivota(Integer boleznenostZhivota) {
		this.boleznenostZhivota = boleznenostZhivota;
	}

	public Integer getNapryazhenieZhivota() {
		return napryazhenieZhivota;
	}

	public void setNapryazhenieZhivota(Integer napryazhenieZhivota) {
		this.napryazhenieZhivota = napryazhenieZhivota;
	}

	public Integer getPeritorialSymptomi() {
		return peritorialSymptomi;
	}

	public void setPeritorialSymptomi(Integer peritorialSymptomi) {
		this.peritorialSymptomi = peritorialSymptomi;
	}

	public Integer getPeristaltika() {
		return peristaltika;
	}

	public void setPeristaltika(Integer peristaltika) {
		this.peristaltika = peristaltika;
	}

	public Integer getOtchozhdenieGazov() {
		return otchozhdenieGazov;
	}

	public void setOtchozhdenieGazov(Integer otchozhdenieGazov) {
		this.otchozhdenieGazov = otchozhdenieGazov;
	}

	public Integer getStul() {
		return stul;
	}

	public void setStul(Integer stul) {
		this.stul = stul;
	}

	public Integer getDizuria() {
		return dizuria;
	}

	public void setDizuria(Integer dizuria) {
		this.dizuria = dizuria;
	}

	public Integer getObjemMochi() {
		return objemMochi;
	}

	public void setObjemMochi(Integer objemMochi) {
		this.objemMochi = objemMochi;
	}

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean aNew) {
		isNew = aNew;
	}

	public boolean areAllFieldsFilled() {
		if (patientState!= null && patientState != 0) {
			if (temperature != null && temperature != 0) {
				if (odishka != null && odishka != 0) {
					if (puls != null && puls != 0) {
						if (tachikardia != null && tachikardia != 0) {
							if (davlenie != null && !davlenie.isEmpty()) {
								if (rvota != null && rvota != 0) {
									if (zhivot != null && zhivot != 0) {
										if (peristaltika != null && peristaltika != 0) {
											if (stul != null && stul != 0) {
												if (objemMochi != null && objemMochi != 0) {
													return true;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return false;
	}
}
