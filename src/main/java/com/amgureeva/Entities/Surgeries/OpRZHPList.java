package com.amgureeva.Entities.Surgeries;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpRZHPList {

	public OpRZHPList() {
	}


	private String id;
	private String name;
	private String rezekcia;
	private String sosudrezekcia;
	private String vorotrezekcia;
	private String krovopoteria;
	private String dlitop;
	private String gemotransfusia;
	private String gemotransfusia_ml;
	private String plazma;
	private String gdc;
	private String gdcmin;
	private String geatype1;
	private String geatype2;
	private String geatype3;
	private String radikal;
	private String limfodissekcia;
	private String bypass;
	private String kamni;
	private String zhkb;
	private String totIz;
	private String selIz;
	private String formRak;
	private String lokalizaciaRaka;
	private String tnm;
	private String osl;
	private String clavien;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRezekcia() {
		return rezekcia;
	}

	public void setRezekcia(String rezekcia) {
		this.rezekcia = rezekcia;
	}

	public String getSosudrezekcia() {
		return sosudrezekcia;
	}

	public void setSosudrezekcia(String sosudrezekcia) {
		this.sosudrezekcia = sosudrezekcia;
	}

	public String getVorotrezekcia() {
		return vorotrezekcia;
	}

	public void setVorotrezekcia(String vorotrezekcia) {
		this.vorotrezekcia = vorotrezekcia;
	}

	public String getKrovopoteria() {
		return krovopoteria;
	}

	public void setKrovopoteria(String krovopoteria) {
		this.krovopoteria = krovopoteria;
	}

	public String getDlitop() {
		return dlitop;
	}

	public void setDlitop(String dlitop) {
		this.dlitop = dlitop;
	}

	public String getGemotransfusia() {
		return gemotransfusia;
	}

	public void setGemotransfusia(String gemotransfusia) {
		this.gemotransfusia = gemotransfusia;
	}

	public String getGemotransfusia_ml() {
		return gemotransfusia_ml;
	}

	public void setGemotransfusia_ml(String gemotransfusia_ml) {
		this.gemotransfusia_ml = gemotransfusia_ml;
	}

	public String getPlazma() {
		return plazma;
	}

	public void setPlazma(String plazma) {
		this.plazma = plazma;
	}

	public String getGdc() {
		return gdc;
	}

	public void setGdc(String gdc) {
		this.gdc = gdc;
	}

	public String getGdcmin() {
		return gdcmin;
	}

	public void setGdcmin(String gdcmin) {
		this.gdcmin = gdcmin;
	}

	public String getGeatype1() {
		return geatype1;
	}

	public void setGeatype1(String geatype1) {
		this.geatype1 = geatype1;
	}

	public String getGeatype2() {
		return geatype2;
	}

	public void setGeatype2(String geatype2) {
		this.geatype2 = geatype2;
	}

	public String getGeatype3() {
		return geatype3;
	}

	public void setGeatype3(String geatype3) {
		this.geatype3 = geatype3;
	}

	public String getRadikal() {
		return radikal;
	}

	public void setRadikal(String radikal) {
		this.radikal = radikal;
	}

	public String getLimfodissekcia() {
		return limfodissekcia;
	}

	public void setLimfodissekcia(String limfodissekcia) {
		this.limfodissekcia = limfodissekcia;
	}

	public String getBypass() {
		return bypass;
	}

	public void setBypass(String bypass) {
		this.bypass = bypass;
	}

	public String getKamni() {
		return kamni;
	}

	public void setKamni(String kamni) {
		this.kamni = kamni;
	}

	public String getZhkb() {
		return zhkb;
	}

	public void setZhkb(String zhkb) {
		this.zhkb = zhkb;
	}

	public String getTotIz() {
		return totIz;
	}

	public void setTotIz(String totIz) {
		this.totIz = totIz;
	}

	public String getSelIz() {
		return selIz;
	}

	public void setSelIz(String selIz) {
		this.selIz = selIz;
	}

	public String getFormRak() {
		return formRak;
	}

	public void setFormRak(String formRak) {
		this.formRak = formRak;
	}

	public String getLokalizaciaRaka() {
		return lokalizaciaRaka;
	}

	public void setLokalizaciaRaka(String lokalizaciaRaka) {
		this.lokalizaciaRaka = lokalizaciaRaka;
	}

	public String getTnm() {
		return tnm;
	}

	public void setTnm(String tnm) {
		this.tnm = tnm;
	}

	public String getOsl() {
		return osl;
	}

	public void setOsl(String osl) {
		this.osl = osl;
	}

	public String getClavien() {
		return clavien;
	}

	public void setClavien(String clavien) {
		this.clavien = clavien;
	}
}

