package com.amgureeva.Entities.Surgeries;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpGCRList {
	public OpGCRList() {
	}
	private String id;
	private String name;
	private String dlitop;
	private String oplech;
	private String obrezekcii;
	private String segrez;
	private String chisloUdSeg;
	private String chisloOch;
	private String razOch;
	private String blizSosud;
	private String polnRHA;
	private String alkChisloOch;
	private String alkRazOch;
	private String krovopoteria;
	private String gemotransfusia;
	private String gemotransfusia_ml;
	private String plazma;
	private String gdc;
	private String gdcmin;
	private String perPolVeni;
	private String perPolVeniMin;
	private String selIz;
	private String selIzMin;
	private String intrRHA;
	private String tnm;
	private String bypass;
	private String totIz;
	private String selSosIz;
	private String tache;
	private String oslRHA;
	private String oslRez;
	private String oslAlk;
	private String clavien;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDlitop() {
		return dlitop;
	}

	public void setDlitop(String dlitop) {
		this.dlitop = dlitop;
	}

	public String getOplech() {
		return oplech;
	}

	public void setOplech(String oplech) {
		this.oplech = oplech;
	}

	public String getObrezekcii() {
		return obrezekcii;
	}

	public void setObrezekcii(String obrezekcii) {
		this.obrezekcii = obrezekcii;
	}

	public String getSegrez() {
		return segrez;
	}

	public void setSegrez(String segrez) {
		this.segrez = segrez;
	}

	public String getChisloUdSeg() {
		return chisloUdSeg;
	}

	public void setChisloUdSeg(String chisloUdSeg) {
		this.chisloUdSeg = chisloUdSeg;
	}

	public String getChisloOch() {
		return chisloOch;
	}

	public void setChisloOch(String chisloOch) {
		this.chisloOch = chisloOch;
	}

	public String getRazOch() {
		return razOch;
	}

	public void setRazOch(String razOch) {
		this.razOch = razOch;
	}

	public String getBlizSosud() {
		return blizSosud;
	}

	public void setBlizSosud(String blizSosud) {
		this.blizSosud = blizSosud;
	}

	public String getPolnRHA() {
		return polnRHA;
	}

	public void setPolnRHA(String polnRHA) {
		this.polnRHA = polnRHA;
	}

	public String getAlkChisloOch() {
		return alkChisloOch;
	}

	public void setAlkChisloOch(String alkChisloOch) {
		this.alkChisloOch = alkChisloOch;
	}

	public String getAlkRazOch() {
		return alkRazOch;
	}

	public void setAlkRazOch(String alkRazOch) {
		this.alkRazOch = alkRazOch;
	}

	public String getKrovopoteria() {
		return krovopoteria;
	}

	public void setKrovopoteria(String krovopoteria) {
		this.krovopoteria = krovopoteria;
	}

	public String getGemotransfusia() {
		return gemotransfusia;
	}

	public void setGemotransfusia(String gemotransfusia) {
		this.gemotransfusia = gemotransfusia;
	}

	public String getGemotransfusia_ml() {
		return gemotransfusia_ml;
	}

	public void setGemotransfusia_ml(String gemotransfusia_ml) {
		this.gemotransfusia_ml = gemotransfusia_ml;
	}

	public String getPlazma() {
		return plazma;
	}

	public void setPlazma(String plazma) {
		this.plazma = plazma;
	}

	public String getGdc() {
		return gdc;
	}

	public void setGdc(String gdc) {
		this.gdc = gdc;
	}

	public String getGdcmin() {
		return gdcmin;
	}

	public void setGdcmin(String gdcmin) {
		this.gdcmin = gdcmin;
	}

	public String getPerPolVeni() {
		return perPolVeni;
	}

	public void setPerPolVeni(String perPolVeni) {
		this.perPolVeni = perPolVeni;
	}

	public String getPerPolVeniMin() {
		return perPolVeniMin;
	}

	public void setPerPolVeniMin(String perPolVeniMin) {
		this.perPolVeniMin = perPolVeniMin;
	}

	public String getSelIz() {
		return selIz;
	}

	public void setSelIz(String selIz) {
		this.selIz = selIz;
	}

	public String getSelIzMin() {
		return selIzMin;
	}

	public void setSelIzMin(String selIzMin) {
		this.selIzMin = selIzMin;
	}

	public String getIntrRHA() {
		return intrRHA;
	}

	public void setIntrRHA(String StringrRHA) {
		this.intrRHA = StringrRHA;
	}

	public String getTnm() {
		return tnm;
	}

	public void setTnm(String tnm) {
		this.tnm = tnm;
	}

	public String getBypass() {
		return bypass;
	}

	public void setBypass(String bypass) {
		this.bypass = bypass;
	}

	public String getTotIz() {
		return totIz;
	}

	public void setTotIz(String totIz) {
		this.totIz = totIz;
	}

	public String getSelSosIz() {
		return selSosIz;
	}

	public void setSelSosIz(String selSosIz) {
		this.selSosIz = selSosIz;
	}

	public String getTache() {
		return tache;
	}

	public void setTache(String tache) {
		this.tache = tache;
	}

	public String getOslRHA() {
		return oslRHA;
	}

	public void setOslRHA(String oslRHA) {
		this.oslRHA = oslRHA;
	}

	public String getOslRez() {
		return oslRez;
	}

	public void setOslRez(String oslRez) {
		this.oslRez = oslRez;
	}

	public String getOslAlk() {
		return oslAlk;
	}

	public void setOslAlk(String oslAlk) {
		this.oslAlk = oslAlk;
	}

	public String getClavien() {
		return clavien;
	}

	public void setClavien(String clavien) {
		this.clavien = clavien;
	}
}

