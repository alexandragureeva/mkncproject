package com.amgureeva.Entities.Surgeries;

/**
 * Created by Nikita on 23.05.2016.
 */
public class DrenirovanieList {

	public DrenirovanieList() {
	}

	private String name;
	private String bilirubin;
	private String out_drenages;
	private String inout_drenages;
	private String block;
	private String mno;
	private String longevity;
	private String previous_cholangit;
	private String absces_cholangit;
	private String dolya;
	private String svoracivanie;
	private String segment67;
	private String segment58;
	private String suprapapil;
	private String zh_posev;
	private String kr_posev;
	private String klebsiela;
	private String sinegnoin_palochka;
	private String complications;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBilirubin() {
		return bilirubin;
	}

	public void setBilirubin(String bilirubin) {
		this.bilirubin = bilirubin;
	}

	public String getOut_drenages() {
		return out_drenages;
	}

	public void setOut_drenages(String out_drenages) {
		this.out_drenages = out_drenages;
	}

	public String getInout_drenages() {
		return inout_drenages;
	}

	public void setInout_drenages(String inout_drenages) {
		this.inout_drenages = inout_drenages;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getMno() {
		return mno;
	}

	public void setMno(String mno) {
		this.mno = mno;
	}

	public String getLongevity() {
		return longevity;
	}

	public void setLongevity(String longevity) {
		this.longevity = longevity;
	}

	public String getPrevious_cholangit() {
		return previous_cholangit;
	}

	public void setPrevious_cholangit(String previous_cholangit) {
		this.previous_cholangit = previous_cholangit;
	}

	public String getAbsces_cholangit() {
		return absces_cholangit;
	}

	public void setAbsces_cholangit(String absces_cholangit) {
		this.absces_cholangit = absces_cholangit;
	}

	public String getDolya() {
		return dolya;
	}

	public void setDolya(String dolya) {
		this.dolya = dolya;
	}

	public String getSvoracivanie() {
		return svoracivanie;
	}

	public void setSvoracivanie(String svoracivanie) {
		this.svoracivanie = svoracivanie;
	}

	public String getSegment67() {
		return segment67;
	}

	public void setSegment67(String segment67) {
		this.segment67 = segment67;
	}

	public String getSegment58() {
		return segment58;
	}

	public void setSegment58(String segment58) {
		this.segment58 = segment58;
	}

	public String getSuprapapil() {
		return suprapapil;
	}

	public void setSuprapapil(String suprapapil) {
		this.suprapapil = suprapapil;
	}

	public String getZh_posev() {
		return zh_posev;
	}

	public void setZh_posev(String zh_posev) {
		this.zh_posev = zh_posev;
	}

	public String getKr_posev() {
		return kr_posev;
	}

	public void setKr_posev(String kr_posev) {
		this.kr_posev = kr_posev;
	}

	public String getKlebsiela() {
		return klebsiela;
	}

	public void setKlebsiela(String klebsiela) {
		this.klebsiela = klebsiela;
	}

	public String getSinegnoin_palochka() {
		return sinegnoin_palochka;
	}

	public void setSinegnoin_palochka(String sinegnoin_palochka) {
		this.sinegnoin_palochka = sinegnoin_palochka;
	}

	public String getComplications() {
		return complications;
	}

	public void setComplications(String complications) {
		this.complications = complications;
	}
}
