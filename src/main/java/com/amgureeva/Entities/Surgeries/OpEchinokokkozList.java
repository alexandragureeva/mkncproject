package com.amgureeva.Entities.Surgeries;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpEchinokokkozList {

	public OpEchinokokkozList() {
	}

	private String id;
	private String name;
	private String rezekcia;
	private  String otkrEch;
	private String obrabotkaFibr;
	private String obrabotkaSvich;
	private String chirurgLech;
	private String etapLech;
	private String simOper;
	private String gdc;
	private String gdcmin;
	private String bypass;
	private String totIz;
	private String selIz;
	private String intraOsl;
	private String osl;
	private String clavien;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRezekcia() {
		return rezekcia;
	}

	public void setRezekcia(String rezekcia) {
		this.rezekcia = rezekcia;
	}

	public String getOtkrEch() {
		return otkrEch;
	}

	public void setOtkrEch(String otkrEch) {
		this.otkrEch = otkrEch;
	}

	public String getObrabotkaFibr() {
		return obrabotkaFibr;
	}

	public void setObrabotkaFibr(String obrabotkaFibr) {
		this.obrabotkaFibr = obrabotkaFibr;
	}

	public String getObrabotkaSvich() {
		return obrabotkaSvich;
	}

	public void setObrabotkaSvich(String obrabotkaSvich) {
		this.obrabotkaSvich = obrabotkaSvich;
	}

	public String getChirurgLech() {
		return chirurgLech;
	}

	public void setChirurgLech(String chirurgLech) {
		this.chirurgLech = chirurgLech;
	}

	public String getEtapLech() {
		return etapLech;
	}

	public void setEtapLech(String etapLech) {
		this.etapLech = etapLech;
	}

	public String getSimOper() {
		return simOper;
	}

	public void setSimOper(String simOper) {
		this.simOper = simOper;
	}

	public String getGdc() {
		return gdc;
	}

	public void setGdc(String gdc) {
		this.gdc = gdc;
	}

	public String getGdcmin() {
		return gdcmin;
	}

	public void setGdcmin(String gdcmin) {
		this.gdcmin = gdcmin;
	}

	public String getBypass() {
		return bypass;
	}

	public void setBypass(String bypass) {
		this.bypass = bypass;
	}

	public String getTotIz() {
		return totIz;
	}

	public void setTotIz(String totIz) {
		this.totIz = totIz;
	}

	public String getSelIz() {
		return selIz;
	}

	public void setSelIz(String selIz) {
		this.selIz = selIz;
	}

	public String getIntraOsl() {
		return intraOsl;
	}

	public void setIntraOsl(String StringraOsl) {
		this.intraOsl = StringraOsl;
	}

	public String getOsl() {
		return osl;
	}

	public void setOsl(String osl) {
		this.osl = osl;
	}

	public String getClavien() {
		return clavien;
	}

	public void setClavien(String clavien) {
		this.clavien = clavien;
	}
}

