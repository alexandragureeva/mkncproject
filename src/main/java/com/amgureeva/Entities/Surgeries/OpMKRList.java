package com.amgureeva.Entities.Surgeries;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpMKRList {

	public OpMKRList() {
	}

	private String id;
	private String name;
	private String dlitop;
	private String oplech;
	private String obrezekcii;
	private String segrez;
	private String chisloUdSeg;
	private String varRezekcii;
	private String limfodissekcia;
	private String sochIntrRHA;
	private String nalRHA;
	private String simultOp;
	private String rekTKish;
	private String gdc;
	private String gdcmin;
	private String bypass;
	private String totIz;
	private String selSosIz;
	private String osl;
	private String clavien;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDlitop() {
		return dlitop;
	}

	public void setDlitop(String dlitop) {
		this.dlitop = dlitop;
	}

	public String getOplech() {
		return oplech;
	}

	public void setOplech(String oplech) {
		this.oplech = oplech;
	}

	public String getObrezekcii() {
		return obrezekcii;
	}

	public void setObrezekcii(String obrezekcii) {
		this.obrezekcii = obrezekcii;
	}

	public String getSegrez() {
		return segrez;
	}

	public void setSegrez(String segrez) {
		this.segrez = segrez;
	}

	public String getChisloUdSeg() {
		return chisloUdSeg;
	}

	public void setChisloUdSeg(String chisloUdSeg) {
		this.chisloUdSeg = chisloUdSeg;
	}

	public String getVarRezekcii() {
		return varRezekcii;
	}

	public void setVarRezekcii(String varRezekcii) {
		this.varRezekcii = varRezekcii;
	}

	public String getLimfodissekcia() {
		return limfodissekcia;
	}

	public void setLimfodissekcia(String limfodissekcia) {
		this.limfodissekcia = limfodissekcia;
	}

	public String getSochIntrRHA() {
		return sochIntrRHA;
	}

	public void setSochIntrRHA(String sochIntrRHA) {
		this.sochIntrRHA = sochIntrRHA;
	}

	public String getNalRHA() {
		return nalRHA;
	}

	public void setNalRHA(String nalRHA) {
		this.nalRHA = nalRHA;
	}

	public String getSimultOp() {
		return simultOp;
	}

	public void setSimultOp(String simultOp) {
		this.simultOp = simultOp;
	}

	public String getRekTKish() {
		return rekTKish;
	}

	public void setRekTKish(String StringrRHA) {
		this.rekTKish = StringrRHA;
	}

	public String getGdc() {
		return gdc;
	}

	public void setGdc(String gdc) {
		this.gdc = gdc;
	}

	public String getGdcmin() {
		return gdcmin;
	}

	public void setGdcmin(String gdcmin) {
		this.gdcmin = gdcmin;
	}

	public String getBypass() {
		return bypass;
	}

	public void setBypass(String bypass) {
		this.bypass = bypass;
	}

	public String getTotIz() {
		return totIz;
	}

	public void setTotIz(String totIz) {
		this.totIz = totIz;
	}

	public String getSelSosIz() {
		return selSosIz;
	}

	public void setSelSosIz(String selSosIz) {
		this.selSosIz = selSosIz;
	}

	public String getOsl() {
		return osl;
	}

	public void setOsl(String osl) {
		this.osl = osl;
	}

	public String getClavien() {
		return clavien;
	}

	public void setClavien(String clavien) {
		this.clavien = clavien;
	}
}

