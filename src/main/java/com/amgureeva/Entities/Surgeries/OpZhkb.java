package com.amgureeva.Entities.Surgeries;

import com.amgureeva.Entities.GeneralSurgery;

/**
 * Created by Nikita on 19.09.2016.
 */
public class OpZhkb extends GeneralSurgery {
	private String dlitop;
	private int cholecistekt;
	private Integer drenirovanie;
	private int drenMethod;
	private Integer primbram;
	private Integer infiltrat;
	private Integer vnutrRaspol;
	private Integer zhelchPuz;
	private Integer kamen;
	private Integer svish;
	private int svishType;
	private Integer mirizi;
	private int miriziZavershenie;
	private Integer perfor;
	private Integer povrezhdeniePologo;
	private int povrezhdeniePologoType;
	private Integer povrezhdCholed;
	private int povrezhdCholedType;
	private Integer povrezhdArterii;
	private Integer povrezhdVeni;


	public String getDlitop() {
		return dlitop;
	}

	public void setDlitop(String dlitop) {
		this.dlitop = dlitop;
	}

	public int getCholecistekt() {
		return cholecistekt;
	}

	public void setCholecistekt(int cholecistekt) {
		this.cholecistekt = cholecistekt;
	}

	public Integer getDrenirovanie() {
		return (drenirovanie == null) ? 0 : drenirovanie;
	}

	public void setDrenirovanie(Integer drenirovanie) {
		this.drenirovanie = drenirovanie;
	}

	public int getDrenMethod() {
		return drenMethod;
	}

	public void setDrenMethod(int drenMethod) {
		this.drenMethod = drenMethod;
	}

	public Integer getPrimbram() {
		return (primbram == null) ? 0 : primbram;
	}

	public void setPrimbram(Integer primbram) {
		this.primbram = primbram;
	}

	public Integer getInfiltrat() {
		return (infiltrat == null) ? 0 : infiltrat;
	}

	public void setInfiltrat(Integer infiltrat) {
		this.infiltrat = infiltrat;
	}

	public Integer getVnutrRaspol() {
		return (vnutrRaspol == null) ? 0 : vnutrRaspol;
	}

	public void setVnutrRaspol(Integer vnutrRaspol) {
		this.vnutrRaspol = vnutrRaspol;
	}

	public Integer getZhelchPuz() {
		return (zhelchPuz == null) ? 0 : zhelchPuz;
	}

	public void setZhelchPuz(Integer zhelchPuz) {
		this.zhelchPuz = zhelchPuz;
	}

	public Integer getKamen() {
		return (kamen == null) ? 0 : kamen;
	}

	public void setKamen(Integer kamen) {
		this.kamen = kamen;
	}

	public Integer getSvish() {
		return (svish == null) ? 0 : svish;
	}

	public void setSvish(Integer svish) {
		this.svish = svish;
	}

	public int getSvishType() {
		return svishType;
	}

	public void setSvishType(int svishType) {
		this.svishType = svishType;
	}

	public Integer getMirizi() {
		return (mirizi == null) ? 0 : mirizi;
	}

	public void setMirizi(Integer mirizi) {
		this.mirizi = mirizi;
	}

	public int getMiriziZavershenie() {
		return miriziZavershenie;
	}

	public void setMiriziZavershenie(int miriziZavershenie) {
		this.miriziZavershenie = miriziZavershenie;
	}

	public Integer getPerfor() {
		return (perfor == null) ? 0 : perfor;
	}

	public void setPerfor(Integer perfor) {
		this.perfor = perfor;
	}

	public Integer getPovrezhdeniePologo() {
		return (povrezhdeniePologo == null) ? 0 : povrezhdeniePologo;
	}

	public void setPovrezhdeniePologo(Integer povrezhdeniePologo) {
		this.povrezhdeniePologo = povrezhdeniePologo;
	}

	public int getPovrezhdeniePologoType() {
		return povrezhdeniePologoType;
	}

	public void setPovrezhdeniePologoType(int povrezhdeniePologoType) {
		this.povrezhdeniePologoType = povrezhdeniePologoType;
	}

	public Integer getPovrezhdCholed() {
		return (povrezhdCholed == null) ? 0 : povrezhdCholed;
	}

	public void setPovrezhdCholed(Integer povrezhdCholed) {
		this.povrezhdCholed = povrezhdCholed;
	}

	public int getPovrezhdCholedType() {
		return povrezhdCholedType;
	}

	public void setPovrezhdCholedType(int povrezhdCholedType) {
		this.povrezhdCholedType = povrezhdCholedType;
	}

	public Integer getPovrezhdArterii() {
		return (povrezhdArterii == null) ? 0 : povrezhdArterii;
	}

	public void setPovrezhdArterii(Integer povrezhdArterii) {
		this.povrezhdArterii = povrezhdArterii;
	}

	public Integer getPovrezhdVeni() {
		return (povrezhdVeni == null) ? 0 : povrezhdVeni;
	}

	public void setPovrezhdVeni(Integer povrezhdVeni) {
		this.povrezhdVeni = povrezhdVeni;
	}

	public boolean areAllFieldsFilled() {
		if (cholecistekt != 0) {
			if (drenirovanie == null || drenirovanie == 0 || drenMethod != 0) {
				if (svish == null || svish == 0 || svishType != 0) {
					if (mirizi == null || mirizi == 0 || miriziZavershenie != 0) {
						if (dlitop != null && !dlitop.isEmpty()) {
							if (povrezhdeniePologo == null || povrezhdeniePologo == 0 || povrezhdeniePologoType != 0) {
								if (povrezhdCholed == null || povrezhdCholed == 0 || povrezhdCholedType != 0) {
									if (dlitop != null && !dlitop.isEmpty()) {
										if (getRezekciaType() != 0) {
											if (getRazdelenieTkanei() != 0) {
												return true;
											}
										}
									}
								}
							}
						}
					}

				}
			}
		}
		return false;
	}
}
