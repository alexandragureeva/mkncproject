package com.amgureeva.Entities.Surgeries;

import com.amgureeva.Entities.GeneralSurgery;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpHCR extends GeneralSurgery {

	public OpHCR() {
	}

	private String dlitop;
	private int hcroplech;
	private int hcrobrezekcii;
	private int hcrsegrez;
	private int hcrchisloUdSeg;
	private int hcrchisloOch;
	private String hcrrazOch;
	private int hcrblizSosud;
	private int hcrpolnRHA;
	private int krovopoteria;
	private int gemotransfusia;
	private String hcrgemotransfusia_ml;
	private String plazma;
	private int gdc;
	private String hcrgdcmin;
	private int perPolVeni;
	private String hcrperPolVeniMin;
	private int selIz;
	private String hcrselIzMin;
	private int intrRHA;
	private int tnm;
	private int bypass;
	private int totIz;
	private int selSosIz;
	private int osl;
	private Integer vnutrKrovot = 0;
	private Integer zhkk = 0;
	private Integer pechNedostatA = 0;
	private Integer pechNedostatB = 0;
	private Integer pechNedostatC = 0;
	private Integer nagnoenie = 0;
	private Integer cholangit = 0;
	private Integer cholAbscedir = 0;
	private Integer ascit = 0;
	private Integer eventracia = 0;
	private Integer kishNeprohod = 0;
	private Integer pankreatSvich = 0;
	private Integer ostriiSvich = 0;
	private Integer sepsis = 0;
	private Integer tyazhSepsis = 0;
	private Integer septShok = 0;
	private Integer zhelchSvish = 0;
	private Integer kishSvish = 0;
	private Integer trombVorotVeni = 0;
	private Integer trombozPechArter = 0;
	private Integer odnostorPnevm = 0;
	private Integer dvustorPnevm = 0;
	private Integer gidrotorax = 0;
	private Integer pochNedostat = 0;
	private Integer serdSosNedostat = 0;
	private Integer poliorganNedostat = 0;

	public String getDlitop() {
		return (dlitop == null || dlitop.equals("null")) ? "" : dlitop;
	}

	public void setDlitop(String dlitop) {
		this.dlitop = dlitop;
	}

	public int getHcroplech() {
		return hcroplech;
	}

	public void setHcroplech(int hcroplech) {
		this.hcroplech = hcroplech;
	}

	public int getHcrobrezekcii() {
		return hcrobrezekcii;
	}

	public void setHcrobrezekcii(int hcrobrezekcii) {
		this.hcrobrezekcii = hcrobrezekcii;
	}

	public int getHcrsegrez() {
		return hcrsegrez;
	}

	public void setHcrsegrez(int hcrsegrez) {
		this.hcrsegrez = hcrsegrez;
	}

	public int getHcrchisloUdSeg() {
		return hcrchisloUdSeg;
	}

	public void setHcrchisloUdSeg(int hcrchisloUdSeg) {
		this.hcrchisloUdSeg = hcrchisloUdSeg;
	}

	public int getHcrchisloOch() {
		return hcrchisloOch;
	}

	public void setHcrchisloOch(int hcrchisloOch) {
		this.hcrchisloOch = hcrchisloOch;
	}

	public String getHcrrazOch() {
		return (hcrrazOch == null || hcrrazOch.equals("null")) ? "" : hcrrazOch;
	}

	public void setHcrrazOch(String hcrrazOch) {
		this.hcrrazOch = hcrrazOch;
	}

	public int getHcrblizSosud() {
		return hcrblizSosud;
	}

	public void setHcrblizSosud(int hcrblizSosud) {
		this.hcrblizSosud = hcrblizSosud;
	}

	public int getHcrpolnRHA() {
		return hcrpolnRHA;
	}

	public void setHcrpolnRHA(int hcrpolnRHA) {
		this.hcrpolnRHA = hcrpolnRHA;
	}

	public int getKrovopoteria() {
		return krovopoteria;
	}

	public void setKrovopoteria(int krovopoteria) {
		this.krovopoteria = krovopoteria;
	}

	public int getGemotransfusia() {
		return gemotransfusia;
	}

	public void setGemotransfusia(int gemotransfusia) {
		this.gemotransfusia = gemotransfusia;
	}

	public String getHcrgemotransfusia_ml() {
		return (hcrgemotransfusia_ml == null || hcrgemotransfusia_ml.equals("null")) ? "" : hcrgemotransfusia_ml;
	}

	public void setHcrgemotransfusia_ml(String hcrgemotransfusia_ml) {
		this.hcrgemotransfusia_ml = hcrgemotransfusia_ml;
	}

	public String getPlazma() {
		return (plazma == null || plazma.equals("null")) ? "" : plazma;
	}

	public void setPlazma(String plazma) {
		this.plazma = plazma;
	}

	public int getGdc() {
		return gdc;
	}

	public void setGdc(int gdc) {
		this.gdc = gdc;
	}

	public String getHcrgdcmin() {
		return (hcrgdcmin == null || hcrgdcmin.equals("null")) ? "" : hcrgdcmin;
	}

	public void setHcrgdcmin(String hcrgdcmin) {
		this.hcrgdcmin = hcrgdcmin;
	}

	public int getPerPolVeni() {
		return perPolVeni;
	}

	public void setPerPolVeni(int perPolVeni) {
		this.perPolVeni = perPolVeni;
	}

	public String getHcrperPolVeniMin() {
		return (hcrperPolVeniMin == null || hcrperPolVeniMin.equals("null")) ? "" : hcrperPolVeniMin;
	}

	public void setHcrperPolVeniMin(String hcrperPolVeniMin) {
		this.hcrperPolVeniMin = hcrperPolVeniMin;
	}

	public int getSelIz() {
		return selIz;
	}

	public void setSelIz(int selIz) {
		this.selIz = selIz;
	}

	public String getHcrselIzMin() {
		return (hcrselIzMin == null || hcrselIzMin.equals("null")) ? "" : hcrselIzMin;
	}

	public void setHcrselIzMin(String hcrselIzMin) {
		this.hcrselIzMin = hcrselIzMin;
	}

	public int getIntrRHA() {
		return intrRHA;
	}

	public void setIntrRHA(int intrRHA) {
		this.intrRHA = intrRHA;
	}

	public int getTnm() {
		return tnm;
	}

	public void setTnm(int tnm) {
		this.tnm = tnm;
	}

	public int getBypass() {
		return bypass;
	}

	public void setBypass(int bypass) {
		this.bypass = bypass;
	}

	public int getTotIz() {
		return totIz;
	}

	public void setTotIz(int totIz) {
		this.totIz = totIz;
	}

	public int getSelSosIz() {
		return selSosIz;
	}

	public void setSelSosIz(int selSosIz) {
		this.selSosIz = selSosIz;
	}

	public int getOsl() {
		return osl;
	}

	public void setOsl(int osl) {
		this.osl = osl;
	}

	public Integer getVnutrKrovot() {
		return (vnutrKrovot == null) ? 0 : vnutrKrovot;
	}

	public void setVnutrKrovot(Integer vnutrKrovot) {
		this.vnutrKrovot = vnutrKrovot;
	}

	public Integer getZhkk() {
		return (zhkk == null) ? 0 : zhkk;
	}

	public void setZhkk(Integer zhkk) {
		this.zhkk = zhkk;
	}

	public Integer getPechNedostatA() {
		return (pechNedostatA == null) ? 0 : pechNedostatA;
	}

	public void setPechNedostatA(Integer pechNedostatA) {
		this.pechNedostatA = pechNedostatA;
	}

	public Integer getPechNedostatB() {
		return (pechNedostatB == null) ? 0 : pechNedostatB;
	}

	public void setPechNedostatB(Integer pechNedostatB) {
		this.pechNedostatB = pechNedostatB;
	}

	public Integer getPechNedostatC() {
		return (pechNedostatC == null) ? 0 : pechNedostatC;
	}

	public void setPechNedostatC(Integer pechNedostatC) {
		this.pechNedostatC = pechNedostatC;
	}

	public Integer getNagnoenie() {
		return (nagnoenie == null) ? 0 : nagnoenie;
	}

	public void setNagnoenie(Integer nagnoenie) {
		this.nagnoenie = nagnoenie;
	}

	public Integer getCholangit() {
		return (cholangit == null) ? 0 : cholangit;
	}

	public void setCholangit(Integer cholangit) {
		this.cholangit = cholangit;
	}

	public Integer getCholAbscedir() {
		return (cholAbscedir == null) ? 0 : cholAbscedir;
	}

	public void setCholAbscedir(Integer cholAbscedir) {
		this.cholAbscedir = cholAbscedir;
	}

	public Integer getAscit() {
		return (ascit == null) ? 0 : ascit;
	}

	public void setAscit(Integer ascit) {
		this.ascit = ascit;
	}

	public Integer getEventracia() {
		return (eventracia == null) ? 0 : eventracia;
	}

	public void setEventracia(Integer eventracia) {
		this.eventracia = eventracia;
	}

	public Integer getKishNeprohod() {
		return (kishNeprohod == null) ? 0 : kishNeprohod;
	}

	public void setKishNeprohod(Integer kishNeprohod) {
		this.kishNeprohod = kishNeprohod;
	}

	public Integer getPankreatSvich() {
		return (pankreatSvich == null) ? 0 : pankreatSvich;
	}

	public void setPankreatSvich(Integer pankreatSvich) {
		this.pankreatSvich = pankreatSvich;
	}

	public Integer getOstriiSvich() {
		return (ostriiSvich == null) ? 0 : ostriiSvich;
	}

	public void setOstriiSvich(Integer ostriiSvich) {
		this.ostriiSvich = ostriiSvich;
	}

	public Integer getSepsis() {
		return (sepsis == null) ? 0 : sepsis;
	}

	public void setSepsis(Integer sepsis) {
		this.sepsis = sepsis;
	}

	public Integer getTyazhSepsis() {
		return (tyazhSepsis == null) ? 0 : tyazhSepsis;
	}

	public void setTyazhSepsis(Integer tyazhSepsis) {
		this.tyazhSepsis = tyazhSepsis;
	}

	public Integer getSeptShok() {
		return (septShok == null) ? 0 : septShok;
	}

	public void setSeptShok(Integer septShok) {
		this.septShok = septShok;
	}

	public Integer getZhelchSvish() {
		return (zhelchSvish == null) ? 0 : zhelchSvish;
	}

	public void setZhelchSvish(Integer zhelchSvish) {
		this.zhelchSvish = zhelchSvish;
	}

	public Integer getKishSvish() {
		return (kishSvish == null) ? 0 : kishSvish;
	}

	public void setKishSvish(Integer kishSvish) {
		this.kishSvish = kishSvish;
	}

	public Integer getTrombVorotVeni() {
		return (trombVorotVeni == null) ? 0 : trombVorotVeni;
	}

	public void setTrombVorotVeni(Integer trombVorotVeni) {
		this.trombVorotVeni = trombVorotVeni;
	}

	public Integer getTrombozPechArter() {
		return (trombozPechArter == null) ? 0 : trombozPechArter;
	}

	public void setTrombozPechArter(Integer trombozPechArter) {
		this.trombozPechArter = trombozPechArter;
	}

	public Integer getOdnostorPnevm() {
		return (odnostorPnevm == null) ? 0 : odnostorPnevm;
	}

	public void setOdnostorPnevm(Integer odnostorPnevm) {
		this.odnostorPnevm = odnostorPnevm;
	}

	public Integer getDvustorPnevm() {
		return (dvustorPnevm == null) ? 0 : dvustorPnevm;
	}

	public void setDvustorPnevm(Integer dvustorPnevm) {
		this.dvustorPnevm = dvustorPnevm;
	}

	public Integer getGidrotorax() {
		return (gidrotorax == null) ? 0 : gidrotorax;
	}

	public void setGidrotorax(Integer gidrotorax) {
		this.gidrotorax = gidrotorax;
	}

	public Integer getPochNedostat() {
		return (pochNedostat == null) ? 0 : pochNedostat;
	}

	public void setPochNedostat(Integer pochNedostat) {
		this.pochNedostat = pochNedostat;
	}

	public Integer getSerdSosNedostat() {
		return (serdSosNedostat == null) ? 0 : serdSosNedostat;
	}

	public void setSerdSosNedostat(Integer serdSosNedostat) {
		this.serdSosNedostat = serdSosNedostat;
	}

	public Integer getPoliorganNedostat() {
		return (poliorganNedostat == null) ? 0 : poliorganNedostat;
	}

	public void setPoliorganNedostat(Integer poliorganNedostat) {
		this.poliorganNedostat = poliorganNedostat;
	}


	public boolean areAllFieldsFilled() {
		if (dlitop != null && !dlitop.isEmpty()) {
			if (hcroplech != 0) {
				if (hcroplech != 1 || hcrobrezekcii != 0) {
					if (hcrobrezekcii != 3 || hcrsegrez != 0 && hcrchisloUdSeg != 0) {
						if (hcroplech != 2 || hcrchisloOch != 0 && hcrrazOch != null && !hcrrazOch.isEmpty()
								&& hcrpolnRHA != 0) {
							if (gdc == 0 || hcrgdcmin != null && !hcrgdcmin.isEmpty()) {
								if (gemotransfusia == 0 || hcrgemotransfusia_ml != null && !hcrgemotransfusia_ml.isEmpty()) {
									if (krovopoteria != 0) {
										if (plazma != null && !plazma.isEmpty()) {
											if (perPolVeni == 0 || hcrperPolVeniMin != null && !hcrperPolVeniMin.isEmpty()) {
												if (selIz == 0 || hcrselIzMin != null && !hcrselIzMin.isEmpty()) {
													if (tnm != 0) {
																if (getRezekciaType() != 0) {
																	if (getRazdelenieTkanei() != 0) {
																		return true;
																	}
																}
															}
														}
													}
											}
										}
								}
							}
						}
					}
				}
			}

		}
		return false;
	}

}
