package com.amgureeva.Entities.Surgeries;

import com.amgureeva.Entities.GeneralSurgery;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpMNKR extends GeneralSurgery {
	public OpMNKR() {
	}

	private String dlitop;
	private int oplech;
	private String stadTnm;
	private int gdc;
	private String mnkrgdcmin;
	private int bypass;
	private int totIz;
	private int selSosIz;
	private int chimiot;
	private int kratRez;
	private int mnkrobrezekcii;
	private int mnkrsegrez;
	private int mnkrchisloUdSeg;
	private int varRezekcii;
	private int limfodissekcia;
	private int sochIntraRHA;
	private int nalRHA;
	private int simultOp;
	private int rekOpTKish;
	private int osl;
	private Integer vnutrKrovot = 0;
	private Integer zhkk = 0;
	private Integer pechNedostatA = 0;
	private Integer pechNedostatB = 0;
	private Integer pechNedostatC = 0;
	private Integer nagnoenie = 0;
	private Integer cholangit = 0;
	private Integer cholAbscedir = 0;
	private Integer ascit = 0;
	private Integer eventracia = 0;
	private Integer kishNeprohod = 0;
	private Integer pankreatSvich = 0;
	private Integer ostriiSvich = 0;
	private Integer sepsis = 0;
	private Integer tyazhSepsis = 0;
	private Integer septShok = 0;
	private Integer zhelchSvish = 0;
	private Integer kishSvish = 0;
	private Integer trombVorotVeni = 0;
	private Integer trombozPechArter = 0;
	private Integer odnostorPnevm = 0;
	private Integer dvustorPnevm = 0;
	private Integer gidrotorax = 0;
	private Integer pochNedostat = 0;
	private Integer serdSosNedostat = 0;
	private Integer poliorganNedostat = 0;

	public String getDlitop() {
		return (dlitop == null || dlitop.equals("null")) ? "" : dlitop;
	}

	public void setDlitop(String dlitop) {
		this.dlitop = dlitop;
	}

	public int getOplech() {
		return oplech;
	}

	public void setOplech(int oplech) {
		this.oplech = oplech;
	}

	public String getStadTnm() {
		return (stadTnm == null || stadTnm.equals("null")) ? "" : stadTnm;
	}

	public void setStadTnm(String stadTnm) {
		this.stadTnm = stadTnm;
	}

	public int getGdc() {
		return gdc;
	}

	public void setGdc(int gdc) {
		this.gdc = gdc;
	}

	public String getMnkrgdcmin() {
		return (mnkrgdcmin == null || mnkrgdcmin.equals("null")) ? "" : mnkrgdcmin;
	}

	public void setMnkrgdcmin(String mnkrgdcmin) {
		this.mnkrgdcmin = mnkrgdcmin;
	}

	public int getBypass() {
		return bypass;
	}

	public void setBypass(int bypass) {
		this.bypass = bypass;
	}

	public int getTotIz() {
		return totIz;
	}

	public void setTotIz(int totIz) {
		this.totIz = totIz;
	}

	public int getSelSosIz() {
		return selSosIz;
	}

	public void setSelSosIz(int selSosIz) {
		this.selSosIz = selSosIz;
	}

	public int getChimiot() {
		return chimiot;
	}

	public void setChimiot(int chimiot) {
		this.chimiot = chimiot;
	}

	public int getKratRez() {
		return kratRez;
	}

	public void setKratRez(int kratRez) {
		this.kratRez = kratRez;
	}

	public int getMnkrobrezekcii() {
		return mnkrobrezekcii;
	}

	public void setMnkrobrezekcii(int mnkrobrezekcii) {
		this.mnkrobrezekcii = mnkrobrezekcii;
	}

	public int getMnkrsegrez() {
		return mnkrsegrez;
	}

	public void setMnkrsegrez(int mnkrsegrez) {
		this.mnkrsegrez = mnkrsegrez;
	}

	public int getMnkrchisloUdSeg() {
		return mnkrchisloUdSeg;
	}

	public void setMnkrchisloUdSeg(int mnkrchisloUdSeg) {
		this.mnkrchisloUdSeg = mnkrchisloUdSeg;
	}

	public int getVarRezekcii() {
		return varRezekcii;
	}

	public void setVarRezekcii(int varRezekcii) {
		this.varRezekcii = varRezekcii;
	}

	public int getLimfodissekcia() {
		return limfodissekcia;
	}

	public void setLimfodissekcia(int limfodissekcia) {
		this.limfodissekcia = limfodissekcia;
	}

	public int getSochIntraRHA() {
		return sochIntraRHA;
	}

	public void setSochIntraRHA(int sochIntraRHA) {
		this.sochIntraRHA = sochIntraRHA;
	}

	public int getNalRHA() {
		return nalRHA;
	}

	public void setNalRHA(int nalRHA) {
		this.nalRHA = nalRHA;
	}

	public int getSimultOp() {
		return simultOp;
	}

	public void setSimultOp(int simultOp) {
		this.simultOp = simultOp;
	}

	public int getRekOpTKish() {
		return rekOpTKish;
	}

	public void setRekOpTKish(int rekOpTKish) {
		this.rekOpTKish = rekOpTKish;
	}

	public int getOsl() {
		return osl;
	}

	public void setOsl(int osl) {
		this.osl = osl;
	}

	public Integer getVnutrKrovot() {
		return (vnutrKrovot == null) ? 0 : vnutrKrovot;
	}

	public void setVnutrKrovot(Integer vnutrKrovot) {
		this.vnutrKrovot = vnutrKrovot;
	}

	public Integer getZhkk() {
		return (zhkk == null) ? 0 : zhkk;
	}

	public void setZhkk(Integer zhkk) {
		this.zhkk = zhkk;
	}

	public Integer getPechNedostatA() {
		return (pechNedostatA == null) ? 0 : pechNedostatA;
	}

	public void setPechNedostatA(Integer pechNedostatA) {
		this.pechNedostatA = pechNedostatA;
	}

	public Integer getPechNedostatB() {
		return (pechNedostatB == null) ? 0 : pechNedostatB;
	}

	public void setPechNedostatB(Integer pechNedostatB) {
		this.pechNedostatB = pechNedostatB;
	}

	public Integer getPechNedostatC() {
		return (pechNedostatC == null) ? 0 : pechNedostatC;
	}

	public void setPechNedostatC(Integer pechNedostatC) {
		this.pechNedostatC = pechNedostatC;
	}

	public Integer getNagnoenie() {
		return (nagnoenie == null) ? 0 : nagnoenie;
	}

	public void setNagnoenie(Integer nagnoenie) {
		this.nagnoenie = nagnoenie;
	}

	public Integer getCholangit() {
		return (cholangit == null) ? 0 : cholangit;
	}

	public void setCholangit(Integer cholangit) {
		this.cholangit = cholangit;
	}

	public Integer getCholAbscedir() {
		return (cholAbscedir == null) ? 0 : cholAbscedir;
	}

	public void setCholAbscedir(Integer cholAbscedir) {
		this.cholAbscedir = cholAbscedir;
	}

	public Integer getAscit() {
		return (ascit == null) ? 0 : ascit;
	}

	public void setAscit(Integer ascit) {
		this.ascit = ascit;
	}

	public Integer getEventracia() {
		return (eventracia == null) ? 0 : eventracia;
	}

	public void setEventracia(Integer eventracia) {
		this.eventracia = eventracia;
	}

	public Integer getKishNeprohod() {
		return (kishNeprohod == null) ? 0 : kishNeprohod;
	}

	public void setKishNeprohod(Integer kishNeprohod) {
		this.kishNeprohod = kishNeprohod;
	}

	public Integer getPankreatSvich() {
		return (pankreatSvich == null) ? 0 : pankreatSvich;
	}

	public void setPankreatSvich(Integer pankreatSvich) {
		this.pankreatSvich = pankreatSvich;
	}

	public Integer getOstriiSvich() {
		return (ostriiSvich == null) ? 0 : ostriiSvich;
	}

	public void setOstriiSvich(Integer ostriiSvich) {
		this.ostriiSvich = ostriiSvich;
	}

	public Integer getSepsis() {
		return (sepsis == null) ? 0 : sepsis;
	}

	public void setSepsis(Integer sepsis) {
		this.sepsis = sepsis;
	}

	public Integer getTyazhSepsis() {
		return (tyazhSepsis == null) ? 0 : tyazhSepsis;
	}

	public void setTyazhSepsis(Integer tyazhSepsis) {
		this.tyazhSepsis = tyazhSepsis;
	}

	public Integer getSeptShok() {
		return (septShok == null) ? 0 : septShok;
	}

	public void setSeptShok(Integer septShok) {
		this.septShok = septShok;
	}

	public Integer getZhelchSvish() {
		return (zhelchSvish == null) ? 0 : zhelchSvish;
	}

	public void setZhelchSvish(Integer zhelchSvish) {
		this.zhelchSvish = zhelchSvish;
	}

	public Integer getKishSvish() {
		return (kishSvish == null) ? 0 : kishSvish;
	}

	public void setKishSvish(Integer kishSvish) {
		this.kishSvish = kishSvish;
	}

	public Integer getTrombVorotVeni() {
		return (trombVorotVeni == null) ? 0 : trombVorotVeni;
	}

	public void setTrombVorotVeni(Integer trombVorotVeni) {
		this.trombVorotVeni = trombVorotVeni;
	}

	public Integer getTrombozPechArter() {
		return (trombozPechArter == null) ? 0 : trombozPechArter;
	}

	public void setTrombozPechArter(Integer trombozPechArter) {
		this.trombozPechArter = trombozPechArter;
	}

	public Integer getOdnostorPnevm() {
		return (odnostorPnevm == null) ? 0 : odnostorPnevm;
	}

	public void setOdnostorPnevm(Integer odnostorPnevm) {
		this.odnostorPnevm = odnostorPnevm;
	}

	public Integer getDvustorPnevm() {
		return (dvustorPnevm == null) ? 0 : dvustorPnevm;
	}

	public void setDvustorPnevm(Integer dvustorPnevm) {
		this.dvustorPnevm = dvustorPnevm;
	}

	public Integer getGidrotorax() {
		return (gidrotorax == null) ? 0 : gidrotorax;
	}

	public void setGidrotorax(Integer gidrotorax) {
		this.gidrotorax = gidrotorax;
	}

	public Integer getPochNedostat() {
		return (pochNedostat == null) ? 0 : pochNedostat;
	}

	public void setPochNedostat(Integer pochNedostat) {
		this.pochNedostat = pochNedostat;
	}

	public Integer getSerdSosNedostat() {
		return (serdSosNedostat == null) ? 0 : serdSosNedostat;
	}

	public void setSerdSosNedostat(Integer serdSosNedostat) {
		this.serdSosNedostat = serdSosNedostat;
	}

	public Integer getPoliorganNedostat() {
		return (poliorganNedostat == null) ? 0 : poliorganNedostat;
	}

	public void setPoliorganNedostat(Integer poliorganNedostat) {
		this.poliorganNedostat = poliorganNedostat;
	}

	public boolean areAllFieldsFilled() {
		if (oplech != 0) {
			if (mnkrobrezekcii != 0) {
				if (mnkrobrezekcii != 3 || mnkrsegrez != 0 && mnkrchisloUdSeg != 0) {
					if (varRezekcii != 0) {
						if (dlitop != null && !dlitop.isEmpty()) {
							if (gdc == 0 || mnkrgdcmin != null && !mnkrgdcmin.isEmpty()) {
								if (nalRHA != 0) {
									if (stadTnm != null && !stadTnm.isEmpty()) {
										if (simultOp != 0) {
											if (chimiot != 0) {
												if (kratRez != 0) {
													if (getRezekciaType() != 0) {
														if (getRazdelenieTkanei() != 0) {
															return true;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}
		return false;
	}


}
