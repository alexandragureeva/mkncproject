package com.amgureeva.Entities.Surgeries;

public class Drenirovanie {

	public Drenirovanie() {
	}

	private String id;
	private String drenId;
	private String date;
	private boolean isNew;
	private int out_drenages;
	private int inout_drenages;
	private int block;
	private int longevity;
	private int previous_cholangit;
	private int absces_cholangit;
	private int dolya;
	private int svoracivanie;
	private int segment67;
	private int segment58;
	private int suprapapil;
	private int zh_posev;
	private int kr_posev;
	private int klebsiela;
	private int sinegnoin_palochka;
	private Integer osl;
	private Integer biloma;
	private Integer cholangit;
	private Integer krovotechenie;
	private Integer sochranivZheltucha;
	private Integer pankreotit;
	private Integer gemobilia;
	private Integer drenageMigration;

	private boolean showComplications;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public int getOut_drenages() {
		return out_drenages;
	}

	public void setOut_drenages(int out_drenages) {
		this.out_drenages = out_drenages;
	}

	public int getInout_drenages() {
		return inout_drenages;
	}

	public void setInout_drenages(int inout_drenages) {
		this.inout_drenages = inout_drenages;
	}

	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	public int getLongevity() {
		return longevity;
	}

	public void setLongevity(int longevity) {
		this.longevity = longevity;
	}

	public int getPrevious_cholangit() {
		return previous_cholangit;
	}

	public void setPrevious_cholangit(int previous_cholangit) {
		this.previous_cholangit = previous_cholangit;
	}

	public int getAbsces_cholangit() {
		return absces_cholangit;
	}

	public void setAbsces_cholangit(int absces_cholangit) {
		this.absces_cholangit = absces_cholangit;
	}

	public int getDolya() {
		return dolya;
	}

	public void setDolya(int dolya) {
		this.dolya = dolya;
	}


	public int getSvoracivanie() {
		return svoracivanie;
	}

	public void setSvoracivanie(int svoracivanie) {
		this.svoracivanie = svoracivanie;
	}

	public int getSegment67() {
		return segment67;
	}

	public void setSegment67(int segment67) {
		this.segment67 = segment67;
	}

	public int getSegment58() {
		return segment58;
	}

	public void setSegment58(int segment58) {
		this.segment58 = segment58;
	}

	public int getSuprapapil() {
		return suprapapil;
	}

	public void setSuprapapil(int suprapapil) {
		this.suprapapil = suprapapil;
	}

	public int getZh_posev() {
		return zh_posev;
	}

	public void setZh_posev(int zh_posev) {
		this.zh_posev = zh_posev;
	}

	public int getKr_posev() {
		return kr_posev;
	}

	public void setKr_posev(int kr_posev) {
		this.kr_posev = kr_posev;
	}

	public int getKlebsiela() {
		return klebsiela;
	}

	public void setKlebsiela(int klebsiela) {
		this.klebsiela = klebsiela;
	}

	public int getSinegnoin_palochka() {
		return sinegnoin_palochka;
	}

	public void setSinegnoin_palochka(int sinegnoin_palochka) {
		this.sinegnoin_palochka = sinegnoin_palochka;
	}

	public Integer getOsl() {
		return (osl == null) ? 0 : osl;
	}

	public void setOsl(Integer osl) {
		this.osl = osl;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean aNew) {
		isNew = aNew;
	}

	public String getDrenId() {
		return drenId;
	}

	public void setDrenId(String drenId) {
		this.drenId = drenId;
	}

	public Integer getBiloma() {
		return (biloma == null) ? 0 : biloma;
	}

	public void setBiloma(Integer biloma) {
		this.biloma = biloma;
	}

	public Integer getCholangit() {
		return (cholangit == null) ? 0 : cholangit;
	}

	public void setCholangit(Integer cholangit) {
		this.cholangit = cholangit;
	}

	public Integer getKrovotechenie() {
		return (krovotechenie == null) ? 0 : krovotechenie;
	}

	public void setKrovotechenie(Integer krovotechenie) {
		this.krovotechenie = krovotechenie;
	}

	public Integer getSochranivZheltucha() {
		return (sochranivZheltucha == null) ? 0 : sochranivZheltucha;
	}

	public void setSochranivZheltucha(Integer sochranivZheltucha) {
		this.sochranivZheltucha = sochranivZheltucha;
	}

	public Integer getPankreotit() {
		return (pankreotit == null) ? 0 : pankreotit;
	}

	public void setPankreotit(Integer pankreotit) {
		this.pankreotit = pankreotit;
	}

	public Integer getGemobilia() {
		return (gemobilia == null) ? 0 : gemobilia;
	}

	public void setGemobilia(Integer gemobilia) {
		this.gemobilia = gemobilia;
	}

	public Integer getDrenageMigration() {
		return (drenageMigration == null) ? 0 : drenageMigration;
	}

	public void setDrenageMigration(Integer drenageMigration) {
		this.drenageMigration = drenageMigration;
	}

	public boolean areAllFieldsFilled() {
		if (block != 0) {
			if (dolya != 0) {
				return true;
			}
		}
		return false;
	}

	public boolean getShowComplications() {
		return showComplications;
	}

	public void setShowComplications(boolean showComplications) {
		this.showComplications = showComplications;
	}
}
