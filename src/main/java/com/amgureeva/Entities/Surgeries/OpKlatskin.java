package com.amgureeva.Entities.Surgeries;

import com.amgureeva.Entities.GeneralSurgery;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpKlatskin extends GeneralSurgery {

	public OpKlatskin() {
	}

	private int rezekcia;
	private int klatsosudrezekcia;
	private int klatvorotrezekcia;
	private int krovopoteria;
	private String dlitop;
	private int gemotransfusia;
	private String klatgemotransfusia_ml;
	private String plazma;
	private int gdc;
	private String klatgdcmin;
	private int geatype1;
	private int geatype2;
	private int geatype3;
	private int radikal;
	private int limfodissekcia;
	private int tnm;
	private int bypass;
	private int totIz;
	private int selIz;
	private int osl;
	private Integer vnutrKrovot = 0;
	private Integer zhkk = 0;
	private Integer pechNedostatA = 0;
	private Integer pechNedostatB = 0;
	private Integer pechNedostatC = 0;
	private Integer nagnoenie = 0;
	private Integer cholangit = 0;
	private Integer cholAbscedir = 0;
	private Integer ascit = 0;
	private Integer eventracia = 0;
	private Integer kishNeprohod = 0;
	private Integer pankreatSvich = 0;
	private Integer ostriiSvich = 0;
	private Integer sepsis = 0;
	private Integer tyazhSepsis = 0;
	private Integer septShok = 0;
	private Integer zhelchSvish = 0;
	private Integer kishSvish = 0;
	private Integer trombVorotVeni = 0;
	private Integer trombozPechArter = 0;
	private Integer odnostorPnevm = 0;
	private Integer dvustorPnevm = 0;
	private Integer gidrotorax = 0;
	private Integer pochNedostat = 0;
	private Integer serdSosNedostat = 0;
	private Integer poliorganNedostat = 0;
	private Integer nesostAnastomoza = 0;

	public int getRezekcia() {
		return rezekcia;
	}

	public void setRezekcia(int rezekcia) {
		this.rezekcia = rezekcia;
	}

	public int getKlatsosudrezekcia() {
		return klatsosudrezekcia;
	}

	public void setKlatsosudrezekcia(int klatsosudrezekcia) {
		this.klatsosudrezekcia = klatsosudrezekcia;
	}

	public int getKlatvorotrezekcia() {
		return klatvorotrezekcia;
	}

	public void setKlatvorotrezekcia(int klatvorotrezekcia) {
		this.klatvorotrezekcia = klatvorotrezekcia;
	}

	public int getKrovopoteria() {
		return krovopoteria;
	}

	public void setKrovopoteria(int krovopoteria) {
		this.krovopoteria = krovopoteria;
	}

	public String getDlitop() {
		return (dlitop == null || dlitop.equals("null")) ? "" : dlitop;
	}

	public void setDlitop(String dlitop) {
		this.dlitop = dlitop;
	}

	public int getGemotransfusia() {
		return gemotransfusia;
	}

	public void setGemotransfusia(int gemotransfusia) {
		this.gemotransfusia = gemotransfusia;
	}

	public String getKlatgemotransfusia_ml() {
		return (klatgemotransfusia_ml == null || klatgemotransfusia_ml.equals("null")) ? "" : klatgemotransfusia_ml;
	}

	public void setKlatgemotransfusia_ml(String klatgemotransfusia_ml) {
		this.klatgemotransfusia_ml = klatgemotransfusia_ml;
	}

	public String getPlazma() {
		return (plazma == null || plazma.equals("null")) ? "" : plazma;
	}

	public void setPlazma(String plazma) {
		this.plazma = plazma;
	}

	public int getGdc() {
		return gdc;
	}

	public void setGdc(int gdc) {
		this.gdc = gdc;
	}

	public String getKlatgdcmin() {
		return (klatgdcmin == null || klatgdcmin.equals("null")) ? "" : klatgdcmin;
	}

	public void setKlatgdcmin(String klatgdcmin) {
		this.klatgdcmin = klatgdcmin;
	}

	public int getGeatype1() {
		return geatype1;
	}

	public void setGeatype1(int geatype1) {
		this.geatype1 = geatype1;
	}

	public int getGeatype2() {
		return geatype2;
	}

	public void setGeatype2(int geatype2) {
		this.geatype2 = geatype2;
	}

	public int getGeatype3() {
		return geatype3;
	}

	public void setGeatype3(int geatype3) {
		this.geatype3 = geatype3;
	}

	public int getRadikal() {
		return radikal;
	}

	public void setRadikal(int radikal) {
		this.radikal = radikal;
	}

	public int getLimfodissekcia() {
		return limfodissekcia;
	}

	public void setLimfodissekcia(int limfodissekcia) {
		this.limfodissekcia = limfodissekcia;
	}

	public int getTnm() {
		return tnm;
	}

	public void setTnm(int tnm) {
		this.tnm = tnm;
	}

	public int getBypass() {
		return bypass;
	}

	public void setBypass(int bypass) {
		this.bypass = bypass;
	}

	public int getTotIz() {
		return totIz;
	}

	public void setTotIz(int totIz) {
		this.totIz = totIz;
	}

	public int getSelIz() {
		return selIz;
	}

	public void setSelIz(int selIz) {
		this.selIz = selIz;
	}

	public int getOsl() {
		return osl;
	}

	public void setOsl(int osl) {
		this.osl = osl;
	}

	public Integer getVnutrKrovot() {
		return (vnutrKrovot == null) ? 0 : vnutrKrovot;
	}

	public void setVnutrKrovot(Integer vnutrKrovot) {
		this.vnutrKrovot = vnutrKrovot;
	}

	public Integer getZhkk() {
		return (zhkk == null) ? 0 : zhkk;
	}

	public void setZhkk(Integer zhkk) {
		this.zhkk = zhkk;
	}

	public Integer getPechNedostatA() {
		return (pechNedostatA == null) ? 0 : pechNedostatA;
	}

	public void setPechNedostatA(Integer pechNedostatA) {
		this.pechNedostatA = pechNedostatA;
	}

	public Integer getPechNedostatB() {
		return (pechNedostatB == null) ? 0 : pechNedostatB;
	}

	public void setPechNedostatB(Integer pechNedostatB) {
		this.pechNedostatB = pechNedostatB;
	}

	public Integer getPechNedostatC() {
		return (pechNedostatC == null) ? 0 : pechNedostatC;
	}

	public void setPechNedostatC(Integer pechNedostatC) {
		this.pechNedostatC = pechNedostatC;
	}

	public Integer getNagnoenie() {
		return (nagnoenie == null) ? 0 : nagnoenie;
	}

	public void setNagnoenie(Integer nagnoenie) {
		this.nagnoenie = nagnoenie;
	}

	public Integer getCholangit() {
		return (cholangit == null) ? 0 : cholangit;
	}

	public void setCholangit(Integer cholangit) {
		this.cholangit = cholangit;
	}

	public Integer getCholAbscedir() {
		return (cholAbscedir == null) ? 0 : cholAbscedir;
	}

	public void setCholAbscedir(Integer cholAbscedir) {
		this.cholAbscedir = cholAbscedir;
	}

	public Integer getAscit() {
		return (ascit == null) ? 0 : ascit;
	}

	public void setAscit(Integer ascit) {
		this.ascit = ascit;
	}

	public Integer getEventracia() {
		return (eventracia == null) ? 0 : eventracia;
	}

	public void setEventracia(Integer eventracia) {
		this.eventracia = eventracia;
	}

	public Integer getKishNeprohod() {
		return (kishNeprohod == null) ? 0 : kishNeprohod;
	}

	public void setKishNeprohod(Integer kishNeprohod) {
		this.kishNeprohod = kishNeprohod;
	}

	public Integer getPankreatSvich() {
		return (pankreatSvich == null) ? 0 : pankreatSvich;
	}

	public void setPankreatSvich(Integer pankreatSvich) {
		this.pankreatSvich = pankreatSvich;
	}

	public Integer getOstriiSvich() {
		return (ostriiSvich == null) ? 0 : ostriiSvich;
	}

	public void setOstriiSvich(Integer ostriiSvich) {
		this.ostriiSvich = ostriiSvich;
	}

	public Integer getSepsis() {
		return (sepsis == null) ? 0 : sepsis;
	}

	public void setSepsis(Integer sepsis) {
		this.sepsis = sepsis;
	}

	public Integer getTyazhSepsis() {
		return (tyazhSepsis == null) ? 0 : tyazhSepsis;
	}

	public void setTyazhSepsis(Integer tyazhSepsis) {
		this.tyazhSepsis = tyazhSepsis;
	}

	public Integer getSeptShok() {
		return (septShok == null) ? 0 : septShok;
	}

	public void setSeptShok(Integer septShok) {
		this.septShok = septShok;
	}

	public Integer getZhelchSvish() {
		return (zhelchSvish == null) ? 0 : zhelchSvish;
	}

	public void setZhelchSvish(Integer zhelchSvish) {
		this.zhelchSvish = zhelchSvish;
	}

	public Integer getKishSvish() {
		return (kishSvish == null) ? 0 : kishSvish;
	}

	public void setKishSvish(Integer kishSvish) {
		this.kishSvish = kishSvish;
	}

	public Integer getTrombVorotVeni() {
		return (trombVorotVeni == null) ? 0 : trombVorotVeni;
	}

	public void setTrombVorotVeni(Integer trombVorotVeni) {
		this.trombVorotVeni = trombVorotVeni;
	}

	public Integer getTrombozPechArter() {
		return (trombozPechArter == null) ? 0 : trombozPechArter;
	}

	public void setTrombozPechArter(Integer trombozPechArter) {
		this.trombozPechArter = trombozPechArter;
	}

	public Integer getOdnostorPnevm() {
		return (odnostorPnevm == null) ? 0 : odnostorPnevm;
	}

	public void setOdnostorPnevm(Integer odnostorPnevm) {
		this.odnostorPnevm = odnostorPnevm;
	}

	public Integer getDvustorPnevm() {
		return (dvustorPnevm == null) ? 0 : dvustorPnevm;
	}

	public void setDvustorPnevm(Integer dvustorPnevm) {
		this.dvustorPnevm = dvustorPnevm;
	}

	public Integer getGidrotorax() {
		return (gidrotorax == null) ? 0 : gidrotorax;
	}

	public void setGidrotorax(Integer gidrotorax) {
		this.gidrotorax = gidrotorax;
	}

	public Integer getPochNedostat() {
		return (pochNedostat == null) ? 0 : pochNedostat;
	}

	public void setPochNedostat(Integer pochNedostat) {
		this.pochNedostat = pochNedostat;
	}

	public Integer getSerdSosNedostat() {
		return (serdSosNedostat == null) ? 0 : serdSosNedostat;
	}

	public void setSerdSosNedostat(Integer serdSosNedostat) {
		this.serdSosNedostat = serdSosNedostat;
	}

	public Integer getPoliorganNedostat() {
		return (poliorganNedostat == null) ? 0 : poliorganNedostat;
	}

	public void setPoliorganNedostat(Integer poliorganNedostat) {
		this.poliorganNedostat = poliorganNedostat;
	}


	public Integer getNesostAnastomoza() {
		return (nesostAnastomoza == null) ? 0 : nesostAnastomoza;
	}

	public void setNesostAnastomoza(Integer nesostAnastomoza) {
		this.nesostAnastomoza = nesostAnastomoza;
	}

	public boolean areAllFieldsFilled() {
		if (rezekcia != 0) {
			if (klatsosudrezekcia != 0) {
				if (klatsosudrezekcia != 1 || klatvorotrezekcia != 0) {
					if (krovopoteria != 0) {
						if (dlitop != null && !dlitop.isEmpty()) {
							if (gemotransfusia == 0 || klatgemotransfusia_ml != null && !klatgemotransfusia_ml.isEmpty()) {
								if (gdc == 0 || klatgdcmin != null && !klatgdcmin.isEmpty()) {
									if (plazma != null && !plazma.isEmpty()) {
										if (geatype1 != 0) {
											if (geatype2 != 0) {
												if (geatype3 != 0) {
													if (radikal != 0) {
														if (limfodissekcia != 0) {
															if (tnm != 0) {
																if (getRezekciaType() != 0) {
																	if (getRazdelenieTkanei() != 0) {
																		return true;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}
		return false;
	}
}