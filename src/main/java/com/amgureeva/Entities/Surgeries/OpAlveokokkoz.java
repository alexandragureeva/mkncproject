package com.amgureeva.Entities.Surgeries;

import com.amgureeva.Entities.GeneralSurgery;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpAlveokokkoz extends GeneralSurgery {
	public OpAlveokokkoz() {
	}

	private int rezekcia;
	private int alvsosudrezekcia;
	private int alvvorotrezekcia;
	private int alvkrovopoteria;
	private String alvdlitop;
	private int alvgemotransfusia;
	private String alvgemotransfusia_ml;
	private String plazma;
	private int gdc;
	private String alvgdcmin;
	private int geatype1;
	private int geatype2;
	private int geatype3;
	private int radikal;
	private int limfodissekcia;
	private int tnm;
	private int pnm;
	private int bypass;
	private int totIz;
	private int selIz;
	private int rezDiafragmi;
	private int osl;

	private Integer vnutrKrovot = 0;
	private Integer zhkk = 0;
	private Integer pechNedostatA = 0;
	private Integer pechNedostatB = 0;
	private Integer pechNedostatC = 0;
	private Integer nagnoenie = 0;
	private Integer cholangit = 0;
	private Integer cholAbscedir = 0;
	private Integer ascit = 0;
	private Integer eventracia = 0;
	private Integer kishNeprohod = 0;
	private Integer pankreatSvich = 0;
	private Integer ostriiSvich = 0;
	private Integer sepsis = 0;
	private Integer tyazhSepsis = 0;
	private Integer septShok = 0;
	private Integer zhelchSvish = 0;
	private Integer kishSvish = 0;
	private Integer trombVorotVeni = 0;
	private Integer trombozPechArter = 0;
	private Integer odnostorPnevm = 0;
	private Integer dvustorPnevm = 0;
	private Integer gidrotorax = 0;
	private Integer pochNedostat = 0;
	private Integer serdSosNedostat = 0;
	private Integer poliorganNedostat = 0;


	public int getRezekcia() {
		return rezekcia;
	}

	public void setRezekcia(int rezekcia) {
		this.rezekcia = rezekcia;
	}

	public int getAlvsosudrezekcia() {
		return alvsosudrezekcia;
	}

	public void setAlvsosudrezekcia(int alvsosudrezekcia) {
		this.alvsosudrezekcia = alvsosudrezekcia;
	}

	public int getAlvvorotrezekcia() {
		return alvvorotrezekcia;
	}

	public void setAlvvorotrezekcia(int alvvorotrezekcia) {
		this.alvvorotrezekcia = alvvorotrezekcia;
	}

	public int getAlvkrovopoteria() {
		return alvkrovopoteria;
	}

	public void setAlvkrovopoteria(int alvkrovopoteria) {
		this.alvkrovopoteria = alvkrovopoteria;
	}

	public String getAlvdlitop() {
		return alvdlitop;
	}

	public void setAlvdlitop(String alvdlitop) {
		this.alvdlitop = alvdlitop;
	}

	public int getAlvgemotransfusia() {
		return alvgemotransfusia;
	}

	public void setAlvgemotransfusia(int alvgemotransfusia) {
		this.alvgemotransfusia = alvgemotransfusia;
	}

	public String getAlvgemotransfusia_ml() {
		return alvgemotransfusia_ml;
	}

	public void setAlvgemotransfusia_ml(String alvgemotransfusia_ml) {
		this.alvgemotransfusia_ml = alvgemotransfusia_ml;
	}

	public String getPlazma() {
		return plazma;
	}

	public void setPlazma(String plazma) {
		this.plazma = plazma;
	}

	public int getGdc() {
		return gdc;
	}

	public void setGdc(int gdc) {
		this.gdc = gdc;
	}

	public String getAlvgdcmin() {
		return alvgdcmin;
	}

	public void setAlvgdcmin(String alvgdcmin) {
		this.alvgdcmin = alvgdcmin;
	}

	public int getGeatype1() {
		return geatype1;
	}

	public void setGeatype1(int geatype1) {
		this.geatype1 = geatype1;
	}

	public int getGeatype2() {
		return geatype2;
	}

	public void setGeatype2(int geatype2) {
		this.geatype2 = geatype2;
	}

	public int getGeatype3() {
		return geatype3;
	}

	public void setGeatype3(int geatype3) {
		this.geatype3 = geatype3;
	}

	public int getRadikal() {
		return radikal;
	}

	public void setRadikal(int radikal) {
		this.radikal = radikal;
	}

	public int getLimfodissekcia() {
		return limfodissekcia;
	}

	public void setLimfodissekcia(int limfodissekcia) {
		this.limfodissekcia = limfodissekcia;
	}

	public int getTnm() {
		return tnm;
	}

	public void setTnm(int tnm) {
		this.tnm = tnm;
	}

	public int getPnm() {
		return pnm;
	}

	public void setPnm(int pnm) {
		this.pnm = pnm;
	}

	public int getBypass() {
		return bypass;
	}

	public void setBypass(int bypass) {
		this.bypass = bypass;
	}

	public int getTotIz() {
		return totIz;
	}

	public void setTotIz(int totIz) {
		this.totIz = totIz;
	}

	public int getSelIz() {
		return selIz;
	}

	public void setSelIz(int gcrselIzMin) {
		this.selIz = gcrselIzMin;
	}

	public int getRezDiafragmi() {
		return rezDiafragmi;
	}

	public void setRezDiafragmi(int rezDiafragmi) {
		this.rezDiafragmi = rezDiafragmi;
	}

	public int getOsl() {
		return osl;
	}

	public void setOsl(int osl) {
		this.osl = osl;
	}

	public Integer getVnutrKrovot() {
		return (vnutrKrovot == null) ? 0 : vnutrKrovot;
	}

	public void setVnutrKrovot(Integer vnutrKrovot) {
		this.vnutrKrovot = vnutrKrovot;
	}

	public Integer getZhkk() {
		return (zhkk == null) ? 0 : zhkk;
	}

	public void setZhkk(Integer zhkk) {
		this.zhkk = zhkk;
	}

	public Integer getPechNedostatA() {
		return (pechNedostatA == null) ? 0 : pechNedostatA;
	}

	public void setPechNedostatA(Integer pechNedostatA) {
		this.pechNedostatA = pechNedostatA;
	}

	public Integer getPechNedostatB() {
		return (pechNedostatB == null) ? 0 : pechNedostatB;
	}

	public void setPechNedostatB(Integer pechNedostatB) {
		this.pechNedostatB = pechNedostatB;
	}

	public Integer getPechNedostatC() {
		return (pechNedostatC == null) ? 0 : pechNedostatC;
	}

	public void setPechNedostatC(Integer pechNedostatC) {
		this.pechNedostatC = pechNedostatC;
	}

	public Integer getNagnoenie() {
		return (nagnoenie == null) ? 0 : nagnoenie;
	}

	public void setNagnoenie(Integer nagnoenie) {
		this.nagnoenie = nagnoenie;
	}

	public Integer getCholangit() {
		return (cholangit == null) ? 0 : cholangit;
	}

	public void setCholangit(Integer cholangit) {
		this.cholangit = cholangit;
	}

	public Integer getCholAbscedir() {
		return (cholAbscedir == null) ? 0 : cholAbscedir;
	}

	public void setCholAbscedir(Integer cholAbscedir) {
		this.cholAbscedir = cholAbscedir;
	}

	public Integer getAscit() {
		return (ascit == null) ? 0 : ascit;
	}

	public void setAscit(Integer ascit) {
		this.ascit = ascit;
	}

	public Integer getEventracia() {
		return (eventracia == null) ? 0 : eventracia;
	}

	public void setEventracia(Integer eventracia) {
		this.eventracia = eventracia;
	}

	public Integer getKishNeprohod() {
		return (kishNeprohod == null) ? 0 : kishNeprohod;
	}

	public void setKishNeprohod(Integer kishNeprohod) {
		this.kishNeprohod = kishNeprohod;
	}

	public Integer getPankreatSvich() {
		return (pankreatSvich == null) ? 0 : pankreatSvich;
	}

	public void setPankreatSvich(Integer pankreatSvich) {
		this.pankreatSvich = pankreatSvich;
	}

	public Integer getOstriiSvich() {
		return (ostriiSvich == null) ? 0 : ostriiSvich;
	}

	public void setOstriiSvich(Integer ostriiSvich) {
		this.ostriiSvich = ostriiSvich;
	}

	public Integer getSepsis() {
		return (sepsis == null) ? 0 : sepsis;
	}

	public void setSepsis(Integer sepsis) {
		this.sepsis = sepsis;
	}

	public Integer getTyazhSepsis() {
		return (tyazhSepsis == null) ? 0 : tyazhSepsis;
	}

	public void setTyazhSepsis(Integer tyazhSepsis) {
		this.tyazhSepsis = tyazhSepsis;
	}

	public Integer getSeptShok() {
		return (septShok == null) ? 0 : septShok;
	}

	public void setSeptShok(Integer septShok) {
		this.septShok = septShok;
	}

	public Integer getZhelchSvish() {
		return (zhelchSvish == null) ? 0 : zhelchSvish;
	}

	public void setZhelchSvish(Integer zhelchSvish) {
		this.zhelchSvish = zhelchSvish;
	}

	public Integer getKishSvish() {
		return (kishSvish == null) ? 0 : kishSvish;
	}

	public void setKishSvish(Integer kishSvish) {
		this.kishSvish = kishSvish;
	}

	public Integer getTrombVorotVeni() {
		return (trombVorotVeni == null) ? 0 : trombVorotVeni;
	}

	public void setTrombVorotVeni(Integer trombVorotVeni) {
		this.trombVorotVeni = trombVorotVeni;
	}

	public Integer getTrombozPechArter() {
		return (trombozPechArter == null) ? 0 : trombozPechArter;
	}

	public void setTrombozPechArter(Integer trombozPechArter) {
		this.trombozPechArter = trombozPechArter;
	}

	public Integer getOdnostorPnevm() {
		return (odnostorPnevm == null) ? 0 : odnostorPnevm;
	}

	public void setOdnostorPnevm(Integer odnostorPnevm) {
		this.odnostorPnevm = odnostorPnevm;
	}

	public Integer getDvustorPnevm() {
		return (dvustorPnevm == null) ? 0 : dvustorPnevm;
	}

	public void setDvustorPnevm(Integer dvustorPnevm) {
		this.dvustorPnevm = dvustorPnevm;
	}

	public Integer getGidrotorax() {
		return (gidrotorax == null) ? 0 : gidrotorax;
	}

	public void setGidrotorax(Integer gidrotorax) {
		this.gidrotorax = gidrotorax;
	}

	public Integer getPochNedostat() {
		return (pochNedostat == null) ? 0 : pochNedostat;
	}

	public void setPochNedostat(Integer pochNedostat) {
		this.pochNedostat = pochNedostat;
	}

	public Integer getSerdSosNedostat() {
		return (serdSosNedostat == null) ? 0 : serdSosNedostat;
	}

	public void setSerdSosNedostat(Integer serdSosNedostat) {
		this.serdSosNedostat = serdSosNedostat;
	}

	public Integer getPoliorganNedostat() {
		return (poliorganNedostat == null) ? 0 : poliorganNedostat;
	}

	public void setPoliorganNedostat(Integer poliorganNedostat) {
		this.poliorganNedostat = poliorganNedostat;
	}

	public boolean areAllFieldsFilled() {
		if (rezekcia != 0) {
			if (alvsosudrezekcia != 0) {
				if (alvsosudrezekcia != 1 || alvvorotrezekcia != 0) {
					if (alvkrovopoteria != 0) {
						if (alvdlitop != null && !alvdlitop.isEmpty()) {
							if (alvgemotransfusia == 0 || alvgemotransfusia_ml != null && !alvgemotransfusia_ml.isEmpty()) {
								if (gdc == 0 || alvgdcmin != null && !alvgdcmin.isEmpty()) {
									if (plazma != null && !plazma.isEmpty()) {
										if (geatype1 != 0) {
											if (geatype2 != 0) {
												if (geatype3 != 0) {
													if (radikal != 0) {
														if (limfodissekcia != 0) {
															if (tnm != 0) {
																if (pnm != 0) {
																	if (getRezekciaType() != 0) {
																		if (getRazdelenieTkanei() != 0) {
																			return true;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}
		return false;
	}
}
