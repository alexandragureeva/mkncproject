package com.amgureeva.Entities.Surgeries;

/**
 * Created by Nikita on 24.09.2016.
 */
public class Punkcia {

	private String id;
	private String drenId;
	private String date;
	private boolean isNew;
	private int organ;
	private int type;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDrenId() {
		return drenId;
	}

	public void setDrenId(String drenId) {
		this.drenId = drenId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean aNew) {
		isNew = aNew;
	}

	public int getOrgan() {
		return organ;
	}

	public void setOrgan(int organ) {
		this.organ = organ;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
