package com.amgureeva.Entities.Surgeries;

import com.amgureeva.Entities.GeneralSurgery;

/**
 * Created by Nikita on 21.05.2016.
 */
public class OpGCR extends GeneralSurgery {

	public OpGCR() {
	}

	private String dlitop;
	private int gcroplech;
	private int gcrobrezekcii;
	private int gcrsegrez;
	private int gcrchisloUdSeg;
	private int gcrchisloOch;
	private String gcrrazOch;
	private int gcrblizSosud;
	private int gcrpolnRHA;
	private int gcralkChisloOch;
	private String gcralkRazOch;
	private int krovopoteria;
	private int gemotransfusia;
	private String gcrgemotransfusia_ml;
	private String plazma;
	private int gdc;
	private String gcrgdcmin;
	private int perPolVeni;
	private String gcrperPolVeniMin;
	private int selIz;
	private String gcrselIzMin;
	private int intrRHA;
	private int tnm;
	private int bypass;
	private int totIz;
	private int selSosIz;
	private int tache;

	private int osl;

	private Integer vnutrKrovot = 0;
	private Integer zhkk = 0;
	private Integer pechNedostatA = 0;
	private Integer pechNedostatB = 0;
	private Integer pechNedostatC = 0;
	private Integer nagnoenie = 0;
	private Integer cholangit = 0;
	private Integer cholAbscedir = 0;
	private Integer ascit = 0;
	private Integer eventracia = 0;
	private Integer kishNeprohod = 0;
	private Integer pankreatSvich = 0;
	private Integer ostriiSvich = 0;
	private Integer sepsis = 0;
	private Integer tyazhSepsis = 0;
	private Integer septShok = 0;
	private Integer zhelchSvish = 0;
	private Integer kishSvish = 0;
	private Integer trombVorotVeni = 0;
	private Integer trombozPechArter = 0;
	private Integer odnostorPnevm = 0;
	private Integer dvustorPnevm = 0;
	private Integer gidrotorax = 0;
	private Integer pochNedostat = 0;
	private Integer serdSosNedostat = 0;
	private Integer poliorganNedostat = 0;
	private Integer vrv = 0;
	private Integer postRezNedostA = 0;
	private Integer postRezNedostB = 0;
	private Integer postRezNedostC = 0;
	private Integer postEmbSyndrom = 0;
	private Integer biloma = 0;
	private Integer povrPolOrgan = 0;
	private Integer krovVBrushPolost1 = 0;
	private Integer krovVBrushPolost2 = 0;

	public String getDlitop() {
		return (dlitop == null || dlitop.equals("null")) ? ",,,,," : dlitop;
	}

	public void setDlitop(String dlitop) {
		this.dlitop = dlitop;
	}

	public int getGcroplech() {
		return gcroplech;
	}

	public void setGcroplech(int gcroplech) {
		this.gcroplech = gcroplech;
	}

	public int getGcrobrezekcii() {
		return gcrobrezekcii;
	}

	public void setGcrobrezekcii(int gcrobrezekcii) {
		this.gcrobrezekcii = gcrobrezekcii;
	}

	public int getGcrsegrez() {
		return gcrsegrez;
	}

	public void setGcrsegrez(int gcrsegrez) {
		this.gcrsegrez = gcrsegrez;
	}

	public int getGcrchisloUdSeg() {
		return gcrchisloUdSeg;
	}

	public void setGcrchisloUdSeg(int gcrchisloUdSeg) {
		this.gcrchisloUdSeg = gcrchisloUdSeg;
	}

	public int getGcrchisloOch() {
		return gcrchisloOch;
	}

	public void setGcrchisloOch(int gcrchisloOch) {
		this.gcrchisloOch = gcrchisloOch;
	}

	public String getGcrrazOch() {
		return (gcrrazOch == null || gcrrazOch.equals("null")) ? "" : gcrrazOch;
	}

	public void setGcrrazOch(String gcrrazOch) {
		this.gcrrazOch = gcrrazOch;
	}

	public int getGcrblizSosud() {
		return gcrblizSosud;
	}

	public void setGcrblizSosud(int gcrblizSosud) {
		this.gcrblizSosud = gcrblizSosud;
	}

	public int getGcrpolnRHA() {
		return gcrpolnRHA;
	}

	public void setGcrpolnRHA(int gcrpolnRHA) {
		this.gcrpolnRHA = gcrpolnRHA;
	}

	public int getGcralkChisloOch() {
		return gcralkChisloOch;
	}

	public void setGcralkChisloOch(int gcralkChisloOch) {
		this.gcralkChisloOch = gcralkChisloOch;
	}

	public String getGcralkRazOch() {
		return (gcralkRazOch == null || gcralkRazOch.equals("null")) ? "" : gcralkRazOch;
	}

	public void setGcralkRazOch(String gcralkRazOch) {
		this.gcralkRazOch = gcralkRazOch;
	}

	public int getKrovopoteria() {
		return krovopoteria;
	}

	public void setKrovopoteria(int krovopoteria) {
		this.krovopoteria = krovopoteria;
	}

	public int getGemotransfusia() {
		return gemotransfusia;
	}

	public void setGemotransfusia(int gemotransfusia) {
		this.gemotransfusia = gemotransfusia;
	}

	public String getGcrgemotransfusia_ml() {
		return (gcrgemotransfusia_ml == null || gcrgemotransfusia_ml.equals("null")) ? "" : gcrgemotransfusia_ml;
	}

	public void setGcrgemotransfusia_ml(String gcrgemotransfusia_ml) {
		this.gcrgemotransfusia_ml = gcrgemotransfusia_ml;
	}

	public String getPlazma() {
		return (plazma == null || plazma.equals("null")) ? "" : plazma;
	}

	public void setPlazma(String plazma) {
		this.plazma = plazma;
	}

	public int getGdc() {
		return gdc;
	}

	public void setGdc(int gdc) {
		this.gdc = gdc;
	}

	public String getGcrgdcmin() {
		return (gcrgdcmin == null || gcrgdcmin.equals("null")) ? "" : gcrgdcmin;
	}

	public void setGcrgdcmin(String gcrgdcmin) {
		this.gcrgdcmin = gcrgdcmin;
	}

	public int getPerPolVeni() {
		return perPolVeni;
	}

	public void setPerPolVeni(int perPolVeni) {
		this.perPolVeni = perPolVeni;
	}

	public String getGcrperPolVeniMin() {
		return (gcrperPolVeniMin == null || gcrperPolVeniMin.equals("null")) ? "" : gcrperPolVeniMin;
	}

	public void setGcrperPolVeniMin(String gcrperPolVeniMin) {
		this.gcrperPolVeniMin = gcrperPolVeniMin;
	}

	public int getSelIz() {
		return selIz;
	}

	public void setSelIz(int selIz) {
		this.selIz = selIz;
	}

	public String getGcrselIzMin() {
		return (gcrselIzMin == null || gcrselIzMin.equals("null")) ? "" : gcrselIzMin;
	}

	public void setGcrselIzMin(String gcrselIzMin) {
		this.gcrselIzMin = gcrselIzMin;
	}

	public int getIntrRHA() {
		return intrRHA;
	}

	public void setIntrRHA(int intrRHA) {
		this.intrRHA = intrRHA;
	}

	public int getTnm() {
		return tnm;
	}

	public void setTnm(int tnm) {
		this.tnm = tnm;
	}

	public int getBypass() {
		return bypass;
	}

	public void setBypass(int bypass) {
		this.bypass = bypass;
	}

	public int getTotIz() {
		return totIz;
	}

	public void setTotIz(int totIz) {
		this.totIz = totIz;
	}

	public int getSelSosIz() {
		return selSosIz;
	}

	public void setSelSosIz(int selSosIz) {
		this.selSosIz = selSosIz;
	}

	public int getTache() {
		return tache;
	}

	public void setTache(int tache) {
		this.tache = tache;
	}


	public int getOsl() {
		return osl;
	}

	public void setOsl(int osl) {
		this.osl = osl;
	}

	public Integer getVnutrKrovot() {
		return (vnutrKrovot == null) ? 0 : vnutrKrovot;
	}

	public void setVnutrKrovot(Integer vnutrKrovot) {
		this.vnutrKrovot = vnutrKrovot;
	}

	public Integer getZhkk() {
		return (zhkk == null) ? 0 : zhkk;
	}

	public void setZhkk(Integer zhkk) {
		this.zhkk = zhkk;
	}

	public Integer getPechNedostatA() {
		return (pechNedostatA == null) ? 0 : pechNedostatA;
	}

	public void setPechNedostatA(Integer pechNedostatA) {
		this.pechNedostatA = pechNedostatA;
	}

	public Integer getPechNedostatB() {
		return (pechNedostatB == null) ? 0 : pechNedostatB;
	}

	public void setPechNedostatB(Integer pechNedostatB) {
		this.pechNedostatB = pechNedostatB;
	}

	public Integer getPechNedostatC() {
		return (pechNedostatC == null) ? 0 : pechNedostatC;
	}

	public void setPechNedostatC(Integer pechNedostatC) {
		this.pechNedostatC = pechNedostatC;
	}

	public Integer getNagnoenie() {
		return (nagnoenie == null) ? 0 : nagnoenie;
	}

	public void setNagnoenie(Integer nagnoenie) {
		this.nagnoenie = nagnoenie;
	}

	public Integer getCholangit() {
		return (cholangit == null) ? 0 : cholangit;
	}

	public void setCholangit(Integer cholangit) {
		this.cholangit = cholangit;
	}

	public Integer getCholAbscedir() {
		return (cholAbscedir == null) ? 0 : cholAbscedir;
	}

	public void setCholAbscedir(Integer cholAbscedir) {
		this.cholAbscedir = cholAbscedir;
	}

	public Integer getAscit() {
		return (ascit == null) ? 0 : ascit;
	}

	public void setAscit(Integer ascit) {
		this.ascit = ascit;
	}

	public Integer getEventracia() {
		return (eventracia == null) ? 0 : eventracia;
	}

	public void setEventracia(Integer eventracia) {
		this.eventracia = eventracia;
	}

	public Integer getKishNeprohod() {
		return (kishNeprohod == null) ? 0 : kishNeprohod;
	}

	public void setKishNeprohod(Integer kishNeprohod) {
		this.kishNeprohod = kishNeprohod;
	}

	public Integer getPankreatSvich() {
		return (pankreatSvich == null) ? 0 : pankreatSvich;
	}

	public void setPankreatSvich(Integer pankreatSvich) {
		this.pankreatSvich = pankreatSvich;
	}

	public Integer getOstriiSvich() {
		return (ostriiSvich == null) ? 0 : ostriiSvich;
	}

	public void setOstriiSvich(Integer ostriiSvich) {
		this.ostriiSvich = ostriiSvich;
	}

	public Integer getSepsis() {
		return (sepsis == null) ? 0 : sepsis;
	}

	public void setSepsis(Integer sepsis) {
		this.sepsis = sepsis;
	}

	public Integer getTyazhSepsis() {
		return (tyazhSepsis == null) ? 0 : tyazhSepsis;
	}

	public void setTyazhSepsis(Integer tyazhSepsis) {
		this.tyazhSepsis = tyazhSepsis;
	}

	public Integer getSeptShok() {
		return (septShok == null) ? 0 : septShok;
	}

	public void setSeptShok(Integer septShok) {
		this.septShok = septShok;
	}

	public Integer getZhelchSvish() {
		return (zhelchSvish == null) ? 0 : zhelchSvish;
	}

	public void setZhelchSvish(Integer zhelchSvish) {
		this.zhelchSvish = zhelchSvish;
	}

	public Integer getKishSvish() {
		return (kishSvish == null) ? 0 : kishSvish;
	}

	public void setKishSvish(Integer kishSvish) {
		this.kishSvish = kishSvish;
	}

	public Integer getTrombVorotVeni() {
		return (trombVorotVeni == null) ? 0 : trombVorotVeni;
	}

	public void setTrombVorotVeni(Integer trombVorotVeni) {
		this.trombVorotVeni = trombVorotVeni;
	}

	public Integer getTrombozPechArter() {
		return (trombozPechArter == null) ? 0 : trombozPechArter;
	}

	public void setTrombozPechArter(Integer trombozPechArter) {
		this.trombozPechArter = trombozPechArter;
	}

	public Integer getOdnostorPnevm() {
		return (odnostorPnevm == null) ? 0 : odnostorPnevm;
	}

	public void setOdnostorPnevm(Integer odnostorPnevm) {
		this.odnostorPnevm = odnostorPnevm;
	}

	public Integer getDvustorPnevm() {
		return (dvustorPnevm == null) ? 0 : dvustorPnevm;
	}

	public void setDvustorPnevm(Integer dvustorPnevm) {
		this.dvustorPnevm = dvustorPnevm;
	}

	public Integer getGidrotorax() {
		return (gidrotorax == null) ? 0 : gidrotorax;
	}

	public void setGidrotorax(Integer gidrotorax) {
		this.gidrotorax = gidrotorax;
	}

	public Integer getPochNedostat() {
		return (pochNedostat == null) ? 0 : pochNedostat;
	}

	public void setPochNedostat(Integer pochNedostat) {
		this.pochNedostat = pochNedostat;
	}

	public Integer getSerdSosNedostat() {
		return (serdSosNedostat == null) ? 0 : serdSosNedostat;
	}

	public void setSerdSosNedostat(Integer serdSosNedostat) {
		this.serdSosNedostat = serdSosNedostat;
	}

	public Integer getPoliorganNedostat() {
		return (poliorganNedostat == null) ? 0 : poliorganNedostat;
	}

	public void setPoliorganNedostat(Integer poliorganNedostat) {
		this.poliorganNedostat = poliorganNedostat;
	}

	public Integer getVrv() {
		return (vrv == null) ? 0 :vrv;
	}

	public void setVrv(Integer vrv) {
		this.vrv = vrv;
	}

	public Integer getPostRezNedostA() {
		return (postRezNedostA == null) ? 0 :postRezNedostA;
	}

	public void setPostRezNedostA(Integer postRezNedostA) {
		this.postRezNedostA = postRezNedostA;
	}

	public Integer getPostRezNedostB() {
		return (postRezNedostB == null) ? 0 : postRezNedostB;
	}

	public void setPostRezNedostB(Integer postRezNedostB) {
		this.postRezNedostB = postRezNedostB;
	}

	public Integer getPostRezNedostC() {
		return (postRezNedostC == null) ? 0 : postRezNedostC;
	}

	public void setPostRezNedostC(Integer postRezNedostC) {
		this.postRezNedostC = postRezNedostC;
	}

	public Integer getPostEmbSyndrom() {
		return (postEmbSyndrom == null) ? 0 : postEmbSyndrom;
	}

	public void setPostEmbSyndrom(Integer postEmbSyndrom) {
		this.postEmbSyndrom = postEmbSyndrom;
	}

	public Integer getBiloma() {
		return (biloma == null) ? 0 : biloma;
	}

	public void setBiloma(Integer biloma) {
		this.biloma = biloma;
	}

	public Integer getPovrPolOrgan() {
		return (povrPolOrgan == null) ? 0 : povrPolOrgan;
	}

	public void setPovrPolOrgan(Integer povrPolOrgan) {
		this.povrPolOrgan = povrPolOrgan;
	}

	public Integer getKrovVBrushPolost1() {
		return (krovVBrushPolost1 == null) ? 0 : krovVBrushPolost1;
	}

	public void setKrovVBrushPolost1(Integer krovVBrushPolost1) {
		this.krovVBrushPolost1 = krovVBrushPolost1;
	}

	public Integer getKrovVBrushPolost2() {
		return (krovVBrushPolost2 == null) ? 0 : krovVBrushPolost2;
	}

	public void setKrovVBrushPolost2(Integer krovVBrushPolost2) {
		this.krovVBrushPolost2 = krovVBrushPolost2;
	}

	public boolean areAllFieldsFilled() {
		if (dlitop != null && !dlitop.isEmpty()) {
			if (gcroplech != 0) {
				if (gcroplech != 1 || gcrobrezekcii != 0) {
					if (gcrobrezekcii != 3 || gcrsegrez != 0 && gcrchisloUdSeg != 0) {
						if (gcroplech != 2 || gcrchisloOch != 0 && gcrrazOch != null && !gcrrazOch.isEmpty()
								&& gcrpolnRHA != 0) {
							if (gcroplech != 3 || gcralkChisloOch != 0 && gcralkRazOch != null && !gcralkRazOch.isEmpty()) {
								if (gdc == 0 || gcrgdcmin != null && !gcrgdcmin.isEmpty()) {
									if (gemotransfusia == 0 || gcrgemotransfusia_ml != null && !gcrgemotransfusia_ml.isEmpty()) {
										if (krovopoteria != 0) {
											if (plazma != null && !plazma.isEmpty()) {
												if (perPolVeni == 0 || gcrperPolVeniMin != null && !gcrperPolVeniMin.isEmpty()) {
													if (selIz == 0 || gcrselIzMin != null && !gcrselIzMin.isEmpty()) {
														if (tnm != 0) {
																if (getRezekciaType() != 0) {
																	if (getRazdelenieTkanei() != 0) {
																		return true;
																	}
																}
															}
														}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}

		}
		return false;
	}

}
