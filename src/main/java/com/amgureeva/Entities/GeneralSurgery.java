package com.amgureeva.Entities;

/**
 * Created by Nikita on 19.08.2016.
 */
public class GeneralSurgery {

	private String id; //id of patient
	private String date;
	private boolean isNew;
	private int clavien;
	private int rezekciaType;
	private int razdelenieTkanei;

	//twoStepsTreatment

	private Float objem;
	private Float dostignuta;
	private Float budushOstPech;

	private Integer febLichoradkaF;
	private Integer krovotechenieF;
	private Integer abscessF;
	private Integer zhidkSkopleniaF;
	private Integer trombozF;
	private Integer zelcheistechenieF;
	private Integer cholangitF;
	private Integer nagnoeniePecheniF;
	private Integer postoblSyndromF;
	private Integer nepolnayaPortoembF;
	private int nepolnayaPortoembTypeF;
	private Integer nepolnayaGipertrofiaF;
	private int nepolnayaGipertrofiaTypeF;
	private Integer postEmbSyndromF;
	private Integer gemobiliaF;
	private Integer pechNedostatochnostF;
	private Integer otherF;

	private boolean showComplications;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(boolean aNew) {
		isNew = aNew;
	}

	public int getClavien() {
		return clavien;
	}

	public void setClavien(int clavien) {
		this.clavien = clavien;
	}

	public int getRezekciaType() {
		return rezekciaType;
	}

	public void setRezekciaType(int rezekciaType) {
		this.rezekciaType = rezekciaType;
	}

	public int getRazdelenieTkanei() {
		return razdelenieTkanei;
	}

	public void setRazdelenieTkanei(int razdelenieTkanei) {
		this.razdelenieTkanei = razdelenieTkanei;
	}


	//setters and getters for twoStepsTreatment


	public Integer getFebLichoradkaF() {
		return febLichoradkaF;
	}

	public void setFebLichoradkaF(Integer febLichoradkaF) {
		this.febLichoradkaF = febLichoradkaF;
	}

	public Integer getKrovotechenieF() {
		return krovotechenieF;
	}

	public void setKrovotechenieF(Integer krovotechenieF) {
		this.krovotechenieF = krovotechenieF;
	}

	public Integer getAbscessF() {
		return abscessF;
	}

	public void setAbscessF(Integer abscessF) {
		this.abscessF = abscessF;
	}

	public Integer getZhidkSkopleniaF() {
		return zhidkSkopleniaF;
	}

	public void setZhidkSkopleniaF(Integer zhidkSkopleniaF) {
		this.zhidkSkopleniaF = zhidkSkopleniaF;
	}

	public Integer getTrombozF() {
		return trombozF;
	}

	public void setTrombozF(Integer trombozF) {
		this.trombozF = trombozF;
	}

	public Integer getZelcheistechenieF() {
		return zelcheistechenieF;
	}

	public void setZelcheistechenieF(Integer zelcheistechenieF) {
		this.zelcheistechenieF = zelcheistechenieF;
	}

	public Integer getCholangitF() {
		return cholangitF;
	}

	public void setCholangitF(Integer cholangitF) {
		this.cholangitF = cholangitF;
	}

	public Integer getNagnoeniePecheniF() {
		return nagnoeniePecheniF;
	}

	public void setNagnoeniePecheniF(Integer nagnoeniePecheniF) {
		this.nagnoeniePecheniF = nagnoeniePecheniF;
	}

	public Integer getPostoblSyndromF() {
		return postoblSyndromF;
	}

	public void setPostoblSyndromF(Integer postoblSyndromF) {
		this.postoblSyndromF = postoblSyndromF;
	}

	public Integer getNepolnayaPortoembF() {
		return nepolnayaPortoembF;
	}

	public void setNepolnayaPortoembF(Integer nepolnayaPortoembF) {
		this.nepolnayaPortoembF = nepolnayaPortoembF;
	}

	public int getNepolnayaPortoembTypeF() {
		return nepolnayaPortoembTypeF;
	}

	public void setNepolnayaPortoembTypeF(int nepolnayaPortoembTypeF) {
		this.nepolnayaPortoembTypeF = nepolnayaPortoembTypeF;
	}

	public Integer getNepolnayaGipertrofiaF() {
		return nepolnayaGipertrofiaF;
	}

	public void setNepolnayaGipertrofiaF(Integer nepolnayaGipertrofiaF) {
		this.nepolnayaGipertrofiaF = nepolnayaGipertrofiaF;
	}

	public int getNepolnayaGipertrofiaTypeF() {
		return nepolnayaGipertrofiaTypeF;
	}

	public void setNepolnayaGipertrofiaTypeF(int nepolnayaGipertrofiaTypeF) {
		this.nepolnayaGipertrofiaTypeF = nepolnayaGipertrofiaTypeF;
	}

	public Integer getPostEmbSyndromF() {
		return postEmbSyndromF;
	}

	public void setPostEmbSyndromF(Integer postEmbSyndromF) {
		this.postEmbSyndromF = postEmbSyndromF;
	}

	public Integer getGemobiliaF() {
		return gemobiliaF;
	}

	public void setGemobiliaF(Integer gemobiliaF) {
		this.gemobiliaF = gemobiliaF;
	}

	public Integer getPechNedostatochnostF() {
		return pechNedostatochnostF;
	}

	public void setPechNedostatochnostF(Integer pechNedostatochnostF) {
		this.pechNedostatochnostF = pechNedostatochnostF;
	}

	public Integer getOtherF() {
		return otherF;
	}

	public void setOtherF(Integer otherF) {
		this.otherF = otherF;
	}

	public Float getObjem() {
		return objem;
	}

	public void setObjem(Float objem) {
		this.objem = objem;
	}

	public Float getDostignuta() {
		return dostignuta;
	}

	public void setDostignuta(Float dostignuta) {
		this.dostignuta = dostignuta;
	}

	public Float getBudushOstPech() {
		return budushOstPech;
	}

	public void setBudushOstPech(Float budushOstPech) {
		this.budushOstPech = budushOstPech;
	}



	public void setFirstStepResults(GeneralSurgery gs) {
		this.abscessF = gs.getAbscessF();
		this.budushOstPech = gs.getBudushOstPech();
		this.cholangitF = gs.getCholangitF();
		this.dostignuta = gs.getDostignuta();
		this.febLichoradkaF = gs.getFebLichoradkaF();
		this.gemobiliaF = gs.getGemobiliaF();
		this.krovotechenieF = gs.getKrovotechenieF();
		this.nagnoeniePecheniF = gs.getNagnoeniePecheniF();
		this.nepolnayaGipertrofiaF = gs.getNepolnayaGipertrofiaF();
		this.nepolnayaGipertrofiaTypeF = gs.getNepolnayaGipertrofiaTypeF();
		this.nepolnayaPortoembF = gs.getNepolnayaPortoembF();
		this.nepolnayaPortoembTypeF = gs.getNepolnayaPortoembTypeF();
		this.objem = gs.getObjem();
		this.otherF = gs.getOtherF();
		this.pechNedostatochnostF = gs.getPechNedostatochnostF();
		this.postEmbSyndromF = gs.getPostEmbSyndromF();
		this.postoblSyndromF= gs.getPostoblSyndromF();
		this.trombozF = gs.getTrombozF();
		this.zelcheistechenieF = gs.getZelcheistechenieF();
		this.zhidkSkopleniaF = gs.getZhidkSkopleniaF();
	}

	public boolean getShowComplications() {
		return showComplications;
	}

	public void setShowComplications(boolean showComplications) {
		this.showComplications = showComplications;
	}
}
