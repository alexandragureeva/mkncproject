package com.amgureeva.Entities;


import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Editor {

	public Editor() {
	}

	public Editor(String id, String patientId, String blockId) {
		this.id = id;
		this.patientId = patientId;
		this.blockId = blockId;
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 3);
		this.date = new Date(calendar.getTimeInMillis());
	}

	private String id;
	private String patientId;
	private String blockId;
	private Date date;
	private boolean expired;
	private boolean full;

	public String getid() {
		return id;
	}

	public void setid(String id) {
		this.id = id;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getBlockId() {
		return blockId;
	}

	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getFormattedDate() {
		Calendar calendar = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date optDate = null;
		try {
			optDate = formatter.parse(date.toString());
		} catch (ParseException e) {
			return Calendar.getInstance().toString();
		}
		calendar.setTime(optDate);
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(calendar.getTime());
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public boolean isFull() {
		return full;
	}

	public void setFull(boolean full) {
		this.full = full;
	}
}
