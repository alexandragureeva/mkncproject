package com.amgureeva.Other;

import com.amgureeva.Entities.DBProperties;

import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Александра on 09.04.2016.
 */
public class Properties {
    public Properties(){}

    private Map<Integer, String> diagnosisNames;
    {
        installDiagnosis();
    }

    public String getDiagnosisNumber(int diagnosis) {
        if (diagnosisNames == null) {
            installDiagnosis();
        }
        if (diagnosis == diagnosisNames.size() - 1) {
            return "Другой диагноз";
        }
        return diagnosisNames.get(diagnosis);
    }

    public Map<Integer, String> getDiagnosisNames() {
        if (diagnosisNames == null) {
            installDiagnosis();
        }
        return diagnosisNames;
    }

    public void installDiagnosis() {
        diagnosisNames = new LinkedHashMap<Integer, String>();
        diagnosisNames.put(0, "---------");
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            con = DriverManager.getConnection(DBProperties.conUrl, DBProperties.conUser, DBProperties.conPassword);
            st = con.createStatement();

            rs = st.executeQuery("select * from diagnosisname");
            while (rs.next()) {
                diagnosisNames.put(rs.getInt(1)+1, rs.getString(2));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (st != null) { st.close(); }
                if (con != null) { con.close(); }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
