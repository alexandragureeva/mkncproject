$(function () {
    $(document).ready(function () {
        var  hasSurgery =   document.getElementById("hasSurgery").value;
        var selectedVal = $("#diagnosis").find("option:selected").val();
        setDiagnosis(selectedVal);
        var hasDrain = document.getElementById("hasDrain");
        if (hasDrain.value == "true") {
            $("#addDrain").innerText = "Удалить информацию по дренированию";
            document.getElementById("drainli").removeAttribute("style");
            document.getElementById("drenForm").setAttribute("style", "display:block;");
        }
        document.getElementById("hasSurgery").value = hasSurgery;
        if (hasSurgery == "true") {
            document.getElementById("surgeryli").removeAttribute("style");
            $("#addSurgery").innerText = "Удалить информацию по операции";
            switch (selectedVal) {
                case "0":
                    document.getElementById("hasSurgery").value = false; //no one diagnosis is selected
                    break;
                case "1": //Opuchol Klatskina
                    document.getElementById("opklatskinForm").setAttribute("style", "display:block;");
                    break;
                case "2": //GCR
                    document.getElementById("opgcrForm").setAttribute("style", "display:block;");
                    break;
                case "3": //HCR
                    document.getElementById("ophcrForm").setAttribute("style", "display:block;");
                    break;
                case "4": //MKR
                    document.getElementById("opmkrForm").setAttribute("style", "display:block;");
                    break;
                case "5": //MNKR
                    document.getElementById("opmnkrForm").setAttribute("style", "display:block;");
                    break;
                case "6"://RZHP
                    document.getElementById("oprzhpForm").setAttribute("style", "display:block;");
                    break;
                case "7"://Echinokokkoz
                    document.getElementById("opechForm").setAttribute("style", "display:block;");
                    break;
                case "8"://Alveokokkoz
                    document.getElementById("opalvForm").setAttribute("style", "display:block;");
                    break;
            }
        }

    });
    $(document).on("change", "#diagnosis", function (e) {
        var selectedVal = $(this).find("option:selected").val();
        setDiagnosis(selectedVal);
    });
    $(document).on("click", ".add-tab li a", function (e) {
        e.preventDefault();
        var selText = $(this).text();
        var id = $("#tab-info").children().length;
        $("#tab-info").append('<li><a href="#tab_' + id + '">' + selText + '</a></li>');
        $('#tab-info-content').append('<div class="tab-pane fade in" id="tab_' + id + '">Contact Form: New Contact ' + id + '</div>');
        $('#tab-info a:last').tab('show');
    });

    $(document).on("click", "#addSurgery", function (e) {
        e.preventDefault();
        document.getElementById("opklatskinForm").setAttribute("style", "display:none;");
        document.getElementById("opgcrForm").setAttribute("style", "display:none;");
        document.getElementById("ophcrForm").setAttribute("style", "display:none;");
        document.getElementById("opmkrForm").setAttribute("style", "display:none;");
        document.getElementById("oprzhpForm").setAttribute("style", "display:none;");
        document.getElementById("opmnkrForm").setAttribute("style", "display:none;");
        document.getElementById("opechForm").setAttribute("style", "display:none;");
        document.getElementById("opalvForm").setAttribute("style", "display:none;");
        var hasSurgery = document.getElementById("hasDrain");
        if (hasSurgery.value == "true") {
            hasSurgery.value = false;
            this.innerText = "Добавить информацию по операции";
            document.getElementById("surgeryli").setAttribute("style", "display:none;");
            $("#general_pane").addClass("active in");  // this deactivates the home tab
            $("#diagnosis_pane").removeClass("active in");  // this deactivates the home tab
            $("#surgery_pane").removeClass("active in");  //
            $("#generalli").addClass("active");
            $("#diagnosisli").removeClass("active");
            $("#surgeryli").removeClass("active");
            $("#drain_pane").removeClass("active in");
            $("#drainli").removeClass("active");
        } else {
            hasSurgery.value = true;
            this.innerText = "Удалить информацию по операции";
            document.getElementById("surgeryli").removeAttribute("style");
            var selectedVal = $("#diagnosis").find("option:selected").val();
            switch (selectedVal) {
                case "0":
                    document.getElementById("hasSurgery").value = false; //no one diagnosis is selected
                    $("#addSurgery").innerText = "Добавить информацию по операции";
                    break;
                case "1": //Opuchol Klatskina
                    document.getElementById("opklatskinForm").setAttribute("style", "display:block;");
                    break;
                case "2": //GCR
                    document.getElementById("opgcrForm").setAttribute("style", "display:block;");
                    break;
                case "3": //HCR
                    document.getElementById("ophcrForm").setAttribute("style", "display:block;");
                    break;
                case "4": //MKR
                    document.getElementById("opmkrForm").setAttribute("style", "display:block;");
                    break;
                case "5": //MNKR
                    document.getElementById("opmnkrForm").setAttribute("style", "display:block;");
                    break;
                case "6"://RZHP
                    document.getElementById("oprzhpForm").setAttribute("style", "display:block;");
                    break;
                case "7"://Echinokokkoz
                    document.getElementById("opechForm").setAttribute("style", "display:block;");
                    break;
                case "8"://Alveokokkoz
                    document.getElementById("opalvForm").setAttribute("style", "display:block;");
                    break;
            }
            if (selectedVal != 0) {
                $("#general_pane").removeClass("active in");  // this deactivates the home tab
                $("#diagnosis_pane").removeClass("active in");  // this deactivates the home tab
                $("#surgery_pane").addClass("active in");  //
                $("#generalli").removeClass("active");
                $("#diagnosisli").removeClass("active");
                $("#surgeryli").addClass("active");
                $("#drain_pane").removeClass("active in");
                $("#drainli").removeClass("active");
            }
        }
    });

   
    $(document).on("click", "#addDrain", function (e) {
        e.preventDefault();
        var hasDrain = document.getElementById("hasDrain");
        if (hasDrain.value == "true") {
            hasDrain.value = false;
            this.innerText = "Добавить информацию по дренированию";
            document.getElementById("drainli").setAttribute("style", "display:none;");
            document.getElementById("drenForm").setAttribute("style", "display:none;");
            $("#general_pane").addClass("active in");  // this deactivates the home tab
            $("#diagnosis_pane").removeClass("active in");  // this deactivates the home tab
            $("#surgery_pane").removeClass("active in");  //
            $("#generalli").addClass("active");
            $("#diagnosisli").removeClass("active");
            $("#surgeryli").removeClass("active");
            $("#drain_pane").removeClass("active in");
            $("#drainli").removeClass("active");
        } else {
            hasDrain.value = true;
            this.innerText = "Удалить информацию по дренированию";
            document.getElementById("drenForm").setAttribute("style", "display:block;");
            document.getElementById("drainli").removeAttribute("style");

            $("#general_pane").removeClass("active in");  // this deactivates the home tab
            $("#diagnosis_pane").removeClass("active in");  // this deactivates the home tab
            $("#surgery_pane").removeClass("active in");  //
            $("#generalli").removeClass("active");
            $("#diagnosisli").removeClass("active");
            $("#surgeryli").removeClass("active");
            $("#drain_pane").addClass("active in");
            $("#drainli").addClass("active");
        }


    });
    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[1] - 1, mdy[0]);
    }

    function calculateDays(d1, d2) {
        var dayInMs = 24 * 60 * 60 * 1000;
        return Math.round(Math.abs((d1.getTime() - d2.getTime()) / (dayInMs)));
    }


    function setDiagnosis(selectedVal) {
        document.getElementById("hasSurgery").value = false;
        document.getElementById("opklatskinForm").setAttribute("style", "display:none;");
        document.getElementById("opgcrForm").setAttribute("style", "display:none;");
        document.getElementById("ophcrForm").setAttribute("style", "display:none;");
        document.getElementById("opmkrForm").setAttribute("style", "display:none;");
        document.getElementById("oprzhpForm").setAttribute("style", "display:none;");
        document.getElementById("opmnkrForm").setAttribute("style", "display:none;");
        document.getElementById("opechForm").setAttribute("style", "display:none;");
        document.getElementById("opalvForm").setAttribute("style", "display:none;");
        document.getElementById("klatskinForm").setAttribute("style", "display:none;");
        document.getElementById("gcrForm").setAttribute("style", "display:none;");
        document.getElementById("hcrForm").setAttribute("style", "display:none;");
        document.getElementById("mkrForm").setAttribute("style", "display:none;");
        document.getElementById("rzhpForm").setAttribute("style", "display:none;");
        document.getElementById("mnkrForm").setAttribute("style", "display:none;");
        document.getElementById("echinokokkozForm").setAttribute("style", "display:none;");
        document.getElementById("alveokokkozForm").setAttribute("style", "display:none;");
        switch (selectedVal) {
            case "0": //no one diagnosis is selected
                break;
            case "1": //Opuchol Klatskina
                document.getElementById("klatskinForm").setAttribute("style", "display:block;");
                document.getElementById("opklatskinForm").setAttribute("style", "display:block;");
                break;
            case "2": //GCR
                document.getElementById("gcrForm").setAttribute("style", "display:block;");
                document.getElementById("opgcrForm").setAttribute("style", "display:block;");
                break;
            case "3": //HCR
                document.getElementById("hcrForm").setAttribute("style", "display:block;");
                document.getElementById("ophcrForm").setAttribute("style", "display:block;");
                break;
            case "4": //MKR
                document.getElementById("mkrForm").setAttribute("style", "display:block;");
                document.getElementById("opmkrForm").setAttribute("style", "display:block;");
                break;
            case "5": //MNKR
                document.getElementById("mnkrForm").setAttribute("style", "display:block;");
                document.getElementById("opmnkrForm").setAttribute("style", "display:block;");
                break;
            case "6"://RZHP
                document.getElementById("rzhpForm").setAttribute("style", "display:block;");
                document.getElementById("oprzhpForm").setAttribute("style", "display:block;");
                break;
            case "7"://Echinokokkoz
                document.getElementById("echinokokkozForm").setAttribute("style", "display:block;");
                document.getElementById("opechForm").setAttribute("style", "display:block;");
                break;
            case "8"://Alveokokkoz
                document.getElementById("alveokokkozForm").setAttribute("style", "display:block;");
                document.getElementById("opalvForm").setAttribute("style", "display:block;");
                break;
        }
    }


}(jQuery));
