(function ($) {
    var selectedValue = "";
    var chart;
    $(document).ready(function () {
        $("#valuesTable tbody tr").each(function () {
            var tds = this.getElementsByTagName('td');
            for (var j = 0; j < tds.length-1; j++) {
                tds[j].setAttribute("class", j);
            }
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100 && $(this).scrollTop() < (document.body.scrollHeight - 100)) {
                $('.scrollToTop').fadeIn();
                $('.scrollToBottom').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
                $('.scrollToBottom').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.scrollToBottom').click(function () {
            $('html, body').animate({scrollTop: document.body.scrollHeight}, 800);
            return false;
        });
        $('.scrollToTop').click(function () {
            $('html, body').animate({scrollTop: 0}, 800);
            return false;
        });
        var table4_Props = {
            exact_match: true,
            rows_counter: true,
            rows_counter_text: "Пациентов: ",
            sort: true,
            sort_config: {
                sort_types: ['Number', 'Number', 'Number', 'Number', 'Number', 'Number', 'Number', 'Number', 'Number']
            },
            remember_grid_values: true,
            loader: true,
            loader_text: "Фильтрую...",
            btn_reset: true
        };
        var tf4 = setFilterGrid("valuesTable", table4_Props);

        //    var tf = new TableFilter('demo');
        //      tf.init();
        $(".headers th").each(function (index, element) {
            var item = '<a href="#" class="btn btn-warning btn-sm active" style="padding:1px; role="button" name="' +
                $(element).attr('class') + '">' + $(element).text() + '</a>';
            $('#well').append(item);
        });
    });
    $(document).on('click', '.btn-add', function (e) {
        e.preventDefault();
        var controlForm = $('.controls form:first'),
            currentEntry = $(this).parents('.entry:first'),
            selText = currentEntry.find('.value').val();
        newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        newEntry.find('.dropdown-toggle').html('Выберите атрибут' + '<span class="caret"></span>');
        controlForm.find('.entry2:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');

        //Изменяем таблицу
        if (selectedValue != "") {
            var $input = $(this);
            var column = $('th:contains("' + selectedValue + '")').index(),
                $table2 = $('.table:first'),
                $rows = $table2.find('tbody tr');
            var $filteredRows = $rows.filter(function () {
                var value = $(this).find('td').eq(column).text().toLowerCase();
                return value.indexOf(selText) === -1;
            });
            /* Clean previous no-result if exist */
            $table2.find('#table-body .no-result').remove();
            /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
            $rows.show();
            $filteredRows.hide();
            /* Prepend no-result row if all rows are filtered */
            if ($filteredRows.length === $rows.length) {
                $table2.find('#table-body').prepend($('<tr class="no-result text-center"><td colspan="' + $table2.find('.filters th').length + '">No result found</td></tr>'));
            }
        }
    }).on('click', '.btn-remove', function (e) {
        $(this).parents('.entry:first').remove();

        e.preventDefault();
        return false;
    }).on('click', '.dropdown-menu li a', function (e) {
        var selText = $(this).text();
        selectedValue = selText;
        $(this).parents('.attr').find('.dropdown-toggle').html(selText + '<span class="caret"></span>');
        e.preventDefault();
    }).on('click', '#well a', function (e) {
        e.preventDefault();
        if ($(this).hasClass('btn-warning')) {
            $(this).addClass('btn-success').removeClass('btn-warning');
            var column = "table ." + $(this).attr("name");
            $(column).hide();
        } else {
            $(this).addClass('btn-warning').removeClass('btn-success');
            var column = "table ." + $(this).attr("name");
            $(column).toggle();
        }
    }).on('click', '#show_all', function (e) {
        e.preventDefault();
        var show = false;
        $("#well a").each(function (index, element) {
            // element == this
            if (index == 0) {
                if ($(this).hasClass('btn-warning')) {
                    show = true;
                }
            }
            //if (index > 0) {
            if (!show) {
                $(this).addClass('btn-warning').removeClass('btn-success');
                var column = "table ." + $(this).attr("name");
                $(column).show();
            } else {
                $(this).addClass('btn-success').removeClass('btn-warning');
                var column = "table ." + $(this).attr("name");
                $(column).hide();
            }
            //}
        });
    }).on('click', '#show_klat', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 273 && index < 286) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_alv', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 169 && index < 181) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_ech', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 180 && index < 195) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_gcr', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 194 && index < 217) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_hcr', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 216 && index < 231) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_mkr', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 230 && index < 253) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_mnkr', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 252 && index < 274) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_rzh', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 285 && index < 298) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_klat_p', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 374 && index < 385) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_alv_p', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 297 && index < 305) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_ech_p', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 304 && index < 315) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_gcr_p', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 314 && index < 333) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_hcr_p', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 332 && index < 345) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_mkr_p', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 344 && index < 360) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_mnkr_p', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 359 && index < 375) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#show_rzh_p', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 384 && index < 395) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#op_klat', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 528 && index < 550) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#op_alv', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 394 && index < 418) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#op_ech', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 417 && index < 433) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#op_gcr', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 432 && index < 464) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#op_hcr', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 463 && index < 490) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#op_mkr', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 489 && index < 508) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#op_mnkr', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 507 && index < 529) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#op_rzh', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 549 && index < 575) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#gen_info', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index >= 0 && index < 32 || index > 574 && index < 577 || index > 593) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#ob_os', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 151 && index < 170) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#an0', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 31 && index < 60) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#an1', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 59 && index < 89) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#an5', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 88 && index < 118) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#an10', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 117 && index < 152) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '#dren', function (e) {
        e.preventDefault();
        $("#well a").each(function (index, element) {
            // element == this
            if (index > 576 && index < 594) {
                if ($(this).hasClass('btn-success')) {
                    $(this).addClass('btn-warning').removeClass('btn-success');
                } else {
                    $(this).addClass('btn-success').removeClass('btn-warning');
                }
                var column = "table ." + $(this).attr("name");
                $(column).toggle();
            }
        });
    }).on('click', '.list-group-item', function (e) {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
        e.preventDefault();
    }).on('click', '.select_all_var', function (e) {
        $('.var').addClass('active');
        e.preventDefault();
    }).on('click', '.select_all_com', function (e) {
        $('.com').addClass('active');
        e.preventDefault();
    }).on('click', '.unselect_all_var', function (e) {
        $('.var').removeClass('active');
        e.preventDefault();
    }).on('click', '.unselect_all_com', function (e) {
        $('.com').removeClass('active');
        e.preventDefault();
    }).on('click', '#descriptive', function (e) {
        $(".headers th").each(function (index, element) {
            if ($(this).css('display') != 'none') {
                var item = '<li class="list-group-item var ' + $(element).attr('class') + '"><small>' + $(element).text() + '</small></li>';
                $('#list').append(item);
            }
        });
        $("#descriptive_modal").modal('toggle');
        e.preventDefault();
    }).on('click', '#2x2', function (e) {
        $("#2x2_modal").modal('toggle');
        e.preventDefault();
    }).on('click', '#spirman', function (e) {
        $(".headers th").each(function (index, element) {
            if ($(this).css('display') != 'none') {
                var item = '<li class="list-group-item var ' + $(element).attr('class') + '"><small>' +
                    $(element).text() + '</small></li>';
                $('#list3').append(item);
            }
        });
        $("#spirman_modal").modal('toggle');
        e.preventDefault();
    }).on('click', '#manna_uitni', function (e) {
        $(".headers th").each(function (index, element) {
            if ($(this).css('display') != 'none') {
                var item = '<li class="list-group-item var ' + $(element).attr('class') + '"><small>' + $(element).text() + '</small></li>';
                $('#list4').append(item);
                var item = '<li class="list-group-item com ' + $(element).attr('class') + '"><small>'
                    + $(element).text() + '</small><input class="form-control" placeholder="Группа 1">' +
                    '<input class="form-control" placeholder="Группа 2"></li>';
                $('#list5').append(item);
            }
        });
        $("#manna_uitni_modal").modal('toggle');
        e.preventDefault();
    })/*.on('click', '#correlation', function(e)
     {
     $( ".headers th" ).each(function( index, element ) {
     if($(this).css('display') != 'none') {
     var item = '<li class="list-group-item var ' + $(element).attr('class') + '"><small>' + $(element).text() + '</small></li>';
     $('#list6').append(item);
     var item = '<li class="list-group-item com ' + $(element).attr('class') + '"><small>' + $(element).text() + '</small></li>';
     $('#list7').append(item);
     }
     });
     $("#correlation_modal").modal('toggle');
     e.preventDefault();
     })*/.on('click', '#survival', function (e) {
        $.ajax({
            type: 'get',//тип запроса: get,post либо head
            url: 'getDatesPeriod',
            success: function (data) {//возвращаемый результат от сервера
                $("#startPeriod").val(data);
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                today = dd + '/' + mm + '/' + yyyy;
                $("#endPeriod").val(today);
                e.preventDefault();
                $("#survival_modal").modal('toggle');
            }
        });
    }).on('click', '#dispertion', function (e) {
        $(".headers th").each(function (index, element) {
            if ($(this).css('display') != 'none') {
                var item = '<li class="list-group-item var ' + $(element).attr('class') + '"><small>' + $(element).text() + '</small></li>';
                $('#list10').append(item);
                var item = '<li class="list-group-item com ' + $(element).attr('class') + '"><small>' + $(element).text() + '</small></li>';
                $('#list11').append(item);
            }
        });
        $("#dispertion_modal").modal('toggle');
        e.preventDefault();
    }).on('click', '.spinner .btn:first-of-type', function (e) {
        e.preventDefault();
        var $spinnerr = $(this).parents(".spinner:first");
        var $inputt = $spinnerr.find("input[type='text']");
        $inputt.val(parseInt($inputt.val(), 10) + 1);
    }).on('click', '.spinner .btn:last-of-type', function (e) {
        e.preventDefault();
        var $spinnerr = $(this).parents(".spinner:first");
        var $inputt = $spinnerr.find("input[type='text']");
        if (parseInt($inputt.val(), 10) > 0) {
            $inputt.val(parseInt($inputt.val(), 10) - 1);
        }
    }).on('click', '#calc_desc', function (e) {
        var activeColumns = [];
        $("#list .active").each(function () {
            var id = $(this).attr('class').split(' ')[2];
            activeColumns.push(parseInt(id));
        });
        var values = [];
        for (var i = 0; i < activeColumns.length; i++) {
            values.push([]);
            var columnId = parseInt(activeColumns[i]);
            $('#valuesTable tbody tr').each(function () {
                if ($(this).css('display') != 'none') {
                    values[i].push($(this).find('td').eq(columnId).text());
                }
            });
        }
        var activeFunctions = [];
        $("#list2 .active").each(function () {
            var id = $(this).attr('class').split(' ')[2];
            activeFunctions.push(parseInt(id));
        });
        e.preventDefault();
        $('.var').removeClass('active');
        $('.com').removeClass('active');
        var jsonParams = {};
        jsonParams[csrfParameter] = csrfToken;
        jsonParams['values'] = JSON.stringify(values);
        jsonParams['indexes'] = activeColumns;
        jsonParams['functions'] = activeFunctions;
        $.ajax({
            type: 'post',//тип запроса: get,post либо head
            url: 'descriptiveStatistics',//url адрес файла обработчика
            data: jsonParams,
            success: function (data) {//возвращаемый результат от сервера
                var arrs = JSON.parse('[' + data + ']');
                var html = '<div class="header"><h5>Описательная статистика</h5></div>';
                html += '<div class="table-responsive">' +
                    '<table class="table table-striped table-bordered table-hover table-condensed">' +
                    '<thead><tr>';
                html += "<th>Переменная</th>";
                for (var func in activeFunctions) {
                    if (!isNaN(func)) {
                        var $el = $("#list2 ." + activeFunctions[func])[0];
                        html += "<th>" + $el.innerText + "</th>";
                    }
                }
                html += '</tr></thead><tbody>';
                for (var i in arrs) {
                    if (!isNaN(i)) {
                        html += '<tr>';
                        var $el = $("#list ." + activeColumns[i])[0];
                        html += "<td>" + $el.innerText + "</td>";
                        for (var j in arrs[i]) {
                            if (!isNaN(j)) {
                                html += "<td>" + arrs[i][j] + "</td>";
                            }
                        }
                        html += '</tr>';
                    }
                }
                html += '</tbody></table></div>';
                $(".result").append(html);
            }
        });
    }).on('click', '#calc_spear', function (e) {
        var activeColumns = [];
        $("#list3 .active").each(function () {
            var id = $(this).attr('class').split(' ')[2];
            activeColumns.push(parseInt(id));
        });
        var values = [];
        for (var i = 0; i < activeColumns.length; i++) {
            values.push([]);
            var columnId = parseInt(activeColumns[i]);
            $('#valuesTable tbody tr').each(function () {
                if ($(this).css('display') != 'none') {
                    values[i].push($(this).find('td').eq(columnId).text());
                }
            });
        }
        e.preventDefault();
        $('.var').removeClass('active');
        $('.com').removeClass('active');
        var jsonParams = {};
        jsonParams[csrfParameter] = csrfToken;
        jsonParams['values'] = JSON.stringify(values);
        jsonParams['indexes'] = activeColumns;
        $.ajax({
            type: 'post',//тип запроса: get,post либо head
            url: 'spearmanStatistics',//url адрес файла обработчика
            data: jsonParams,
            success: function (data) {//возвращаемый результат от сервера
                var arrs = JSON.parse('[' + data + ']');
                var html = '<div class="header"><h5>Коэффициент корреляции Спирмена</h5></div>';
                html += '<div class="table-responsive">' +
                    '<table class="table table-striped table-bordered table-hover table-condensed">' +
                    '<thead><tr>';
                html += "<th>Переменные</th>";
                for (var func in activeColumns) {
                    if (!isNaN(func)) {
                        var $el = $("#list3 ." + activeColumns[func])[0];
                        html += "<td>" + $el.innerText + "</td>";
                    }
                }
                html += '</tr></thead><tbody>';
                for (var i in arrs) {
                    if (!isNaN(i)) {
                        html += '<tr>';
                        var $el = $("#list3 ." + activeColumns[i])[0];
                        html += "<td>" + $el.innerText + "</td>";
                        for (var j in arrs[i]) {
                            if (!isNaN(j)) {
                                html += "<td>" + arrs[i][j] + "</td>";
                            }
                        }
                        html += '</tr>';
                    }
                }
                html += '</tbody></table></div>';
                $(".result").append(html);
            }
        });
    }).on('click', '#calc_22', function (e) {
        var values = [];
        values.push(Math.abs(parseInt(document.getElementById("f1").value)));
        values.push(Math.abs(parseInt(document.getElementById("f2").value)));
        values.push(Math.abs(parseInt(document.getElementById("f3").value)));
        values.push(Math.abs(parseInt(document.getElementById("f4").value)));
        var jsonParams = {};
        jsonParams[csrfParameter] = csrfToken;
        jsonParams['values'] = JSON.stringify(values);
        $.ajax({
            type: 'post',//тип запроса: get,post либо head
            url: '22Statistics',//url адрес файла обработчика
            data: jsonParams,
            success: function (data) {//возвращаемый результат от сервера
                var arrs = JSON.parse('[' + data + ']');
                var html = '<div class="header"><h5>Таблица частот 2х2</h5></div>';
                html += '<div class="table-responsive">' +
                    '<table class="table table-striped table-bordered table-hover table-condensed">' +
                    '<thead><tr>';
                html += "<th></th><th>Значения1</th><th>Значения2</th><th>Сумма</th>";
                html += '</tr></thead><tbody>';
                for (var i in arrs) {
                    if (!isNaN(i)) {
                        html += '<tr>';
                        switch (i) {
                            case "0":
                                html += "<td>Частоты 1</td>";
                                break;
                            case "1":
                                html += "<td>Процент 1</td>";
                                break;
                            case "2":
                                html += "<td>Частоты 2</td>";
                                break;
                            case "3":
                                html += "<td>Процент 2</td>";
                                break;
                            case "4":
                                html += "<td>Сумма</td>";
                                break;
                            case "5":
                                html += "<td>Процент суммы</td>";
                                break;
                            case "6":
                                html += "<td>Критерий Пирсона(хи)</td>";
                                break;
                            case "7":
                                html += "<td>Поправка Йейтса</td>";
                                break;
                            case "8":
                                html += "<td>Критерий Фишера(1)</td>";
                                break;
                            case "9":
                                html += "<td>Критерий Фишера(2)</td>";
                                break;
                            case "10":
                                html += "<td>Критерий Мак-Немара</td>";
                                break;
                        }
                        for (var j in arrs[i]) {
                            if (!isNaN(j)) {
                                html += "<td>" + arrs[i][j] + "</td>";
                            }
                        }
                        html += '</tr>';
                    }
                }
                html += '</tbody></table></div>';
                $(".result").append(html);
            }
        });
    }).on('click', '#calc_mw', function (e) {
        var activeColumns = [];
        $("#list4 .active").each(function () {
            var id = $(this).attr('class').split(' ')[2];
            activeColumns.push(parseInt(id));
        });
        var values = [];
        for (var i = 0; i < activeColumns.length; i++) {
            values.push([]);
            var columnId = parseInt(activeColumns[i]);
            $('#valuesTable tbody tr').each(function () {
                if ($(this).css('display') != 'none') {
                    values[i].push($(this).find('td').eq(columnId).text());
                }
            });
        }
        var activeGrouping = [];
        $("#list5 .active").each(function () {
            var id = $(this).attr('class').split(' ')[2];
            activeGrouping.push(parseInt(id));
        });
        var groupingValues = [];
        for (var i = 0; i < activeGrouping.length; i++) {
            groupingValues.push([]);
            var columnId = parseInt(activeGrouping[i]);
            $('#valuesTable tbody tr').each(function () {
                if ($(this).css('display') != 'none') {
                    groupingValues[i].push($(this).find('td').eq(columnId).text());
                }
            });
        }
        var groups = [];
        var i = 0;
        $("#list5 .active").each(function () {
            groups.push([]);
            var inputs = $(this).children();
            groups[i].push("[\"" + inputs[1].value + "\"]");
            groups[i].push("[\"" + inputs[2].value + "\"]");
            i++;
        });
        e.preventDefault();
        $('.var').removeClass('active');
        $('.com').removeClass('active');
        var jsonParams = {};
        jsonParams[csrfParameter] = csrfToken;
        jsonParams['values'] = JSON.stringify(values);
        jsonParams['indexes'] = activeColumns;
        jsonParams['groupValues'] = JSON.stringify(groupingValues);
        jsonParams['groups'] = JSON.stringify(groups);
        $.ajax({
            type: 'post',//тип запроса: get,post либо head
            url: 'mannWhithneyStatistics',//url адрес файла обработчика
            data: jsonParams,
            success: function (data) {//возвращаемый результат от сервера
                var arrs = JSON.parse('[' + data + ']');
                var html = '<div class="header"><h5>U-критерий Манна-Уитни</h5></div>';
                html += '<div class="table-responsive">' +
                    '<table class="table table-striped table-bordered table-hover table-condensed">' +
                    '<thead><tr>';
                html += "<th>Переменные</th>";
                for (var func in activeGrouping) {
                    if (!isNaN(func)) {
                        var $el = $("#list5 ." + activeGrouping[func])[0];
                        html += "<th>" + $el.innerText +
                            "\n[" + groups[func][0] + "],[" + groups[func][1] + "</th>";
                    }
                }
                html += '</tr></thead><tbody>';
                for (var i in arrs) {
                    if (!isNaN(i)) {
                        html += '<tr>';
                        var $el = $("#list4 ." + activeColumns[i])[0];
                        html += "<td>" + $el.innerText + "</td>";
                        for (var j in arrs[i]) {
                            if (!isNaN(j)) {
                                html += "<td>" + arrs[i][j] + "</td>";
                            }
                        }
                        html += '</tr>';
                    }
                }
                html += '</tbody></table></div>';
                $(".result").append(html);
            }
        });
    }).on('click', '#calc_surv', function (e) {
        var startDate = $("#startPeriod").val();
        var endDate = $("#endPeriod").val();
        var datePoints = [];
        var tempDate = new parseMDY(startDate);
        var endXDate = new parseMDY(endDate);
        if (document.getElementById('rb1').checked) {
            while (tempDate.diffDays(endXDate) > 0) {
                datePoints.push(tempDate.toISOString());
                tempDate = tempDate.addWeeks(1);
            }
        }
        if (document.getElementById('rb2').checked) {
            while (tempDate.diffDays(endXDate) > 0) {
                datePoints.push(tempDate.toISOString());
                tempDate = tempDate.addMonths(1);
            }
        }
        if (document.getElementById('rb3').checked) {
            while (tempDate.diffDays(endXDate) > 0) {
                datePoints.push(tempDate.toISOString());
                tempDate = tempDate.addMonths(3);
            }
        }
        if (document.getElementById('rb4').checked) {
            while (tempDate.diffDays(endXDate) > 0) {
                datePoints.push(tempDate.toISOString());
                tempDate = tempDate.addYears(1);
            }
        }
        if (document.getElementById('rb5').checked) {
            var days = $("#numDays").val();
            while (tempDate.diffDays(endXDate) > 0) {
                datePoints.push(tempDate.toISOString());
                tempDate = tempDate.addDays(days);
            }
        }
        datePoints.push(endXDate.toISOString());
        var ids = [];
        $('#valuesTable tbody tr').each(function () {
            if ($(this).css('display') != 'none') {
                var loc_array = $(this).find('td');
                ids.push(parseInt(loc_array[loc_array.length - 1].innerText));
            }
        });
        var jsonParams = {};
        jsonParams[csrfParameter] = csrfToken;
        jsonParams['intervals'] = datePoints;
        jsonParams['startDate'] = startDate;
        jsonParams['endDate'] = endDate;
        jsonParams['ids'] = ids;
        $.ajax({
            type: 'post',
            url: 'getDeathDates',
            data: jsonParams,
            success: function (data) {
                var arrs = JSON.parse('[' + data + ']');
                var html = '<div class="header"><h5>Анализ выживаемости</h5></div>';
                html += '<div class="table-responsive">' +
                    '<table class="table table-striped table-bordered table-hover table-condensed">' +
                    '<thead><tr>';
                html += "<th>Интервалы</th><th>Начало</th><th>Конец</th><th>Число выживших</th>" +
                    "<th>Число умерших</th><th>Выживаемость</th>";

                html += '</tr></thead><tbody>';
                var ind = 1;
                for (var i in arrs) {
                    if (!isNaN(i)) {
                        if (arrs[i][4]) {
                            html += '<tr>';
                            html += "<td>" + ind + "</td>";
                            html += "<td>" + arrs[i][0] + "</td>";
                            html += "<td>" + arrs[i][5] + "</td>";
                            html += "<td>" + arrs[i][2] + "</td>";
                            html += "<td>" + arrs[i][1] + "</td>";
                            html += "<td>" + arrs[i][3] + "</td>";
                            html += '</tr>';
                            ind++;
                        }
                    }
                }
                html += '</tbody></table></div>';
                $(".result").append(html);
                var chartDiv = document.createElement('div'); // Create a new div
                $('#result').append(chartDiv);
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: chartDiv,
                        type: 'line'
                    },
                    title: {
                        text: 'Анализ выживаемости',
                        x: -20 //center
                    },
                    subtitle: {
                        text: 'За период времени:' + startDate + '-' + endDate,
                        x: -20
                    },
                    xAxis: {
                        categories: []
                    },
                    yAxis: {
                        title: {
                            text: 'Вероятность выживания'
                        },
                        plotLines: [{
                            value: 0,
                            width: 2,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: '%'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Линия выживаемости',
                        step: 'left',
                        data: []
                    }]
                });
                var points = "[";
                var stops = "[";
                for (var i in arrs) {
                    if (!isNaN(i)) {
                        stops = stops.concat("\"" + arrs[i][6] + "\", ");
                        if (arrs[i][4] == true) {
                            points = points.concat(arrs[i][3] + ",");
                        } else {
                            points = points.concat("{y: " + arrs[i][3] + ", color: '#BF0B23', marker:{ fillColor:'red'}}, ");
                        }
                    }
                }
                points = points.substring(0, points.length - 2);
                points = points.concat("]");
                stops = stops.substring(0, stops.length - 2);
                stops = stops.concat("]");
                chart.xAxis[0].setCategories(eval(stops), true, true);
                chart.series[0].setData(eval(points), false);
                chart.redraw(true);
            }
        });

    }).on('click', '#calc_desp', function (e) {
        var activeColumns = [];
        $("#list10 .active").each(function () {
            var id = $(this).attr('class').split(' ')[2];
            activeColumns.push(parseInt(id));
        });
        var values = [];
        for (var i = 0; i < activeColumns.length; i++) {
            values.push([]);
            var columnId = parseInt(activeColumns[i]);
            $('#valuesTable tbody tr').each(function () {
                if ($(this).css('display') != 'none') {
                    values[i].push($(this).find('td').eq(columnId).text());
                }
            });
        }
        var activeGrouping = [];
        $("#list11 .active").each(function () {
            var id = $(this).attr('class').split(' ')[2];
            activeGrouping.push(parseInt(id));
        });
        var groupingValues = [];
        for (var i = 0; i < activeGrouping.length; i++) {
            groupingValues.push([]);
            var columnId = parseInt(activeGrouping[i]);
            $('#valuesTable tbody tr').each(function () {
                if ($(this).css('display') != 'none') {
                    groupingValues[i].push($(this).find('td').eq(columnId).text());
                }
            });
        }
        e.preventDefault();
        $('.var').removeClass('active');
        $('.com').removeClass('active');
        var jsonParams = {};
        jsonParams[csrfParameter] = csrfToken;
        jsonParams['values'] = JSON.stringify(values);
        jsonParams['indexes'] = activeColumns;
        jsonParams['groupValues'] = JSON.stringify(groupingValues);
        $.ajax({
            type: 'post',//тип запроса: get,post либо head
            url: 'anovaStatistics',//url адрес файла обработчика
            data: jsonParams,
            success: function (data) {//возвращаемый результат от сервера
                var arrs = JSON.parse('[' + data + ']');
                var html = '<div class="header"><h5>Дисперсионный анализ</h5></div>';
                html += '<div class="table-responsive">' +
                    '<table class="table table-striped table-bordered table-hover table-condensed">' +
                    '<thead><tr>';
                html += "<th>Переменные</th>";
                for (var func in activeGrouping) {
                    if (!isNaN(func)) {
                        var $el = $("#list11 ." + activeGrouping[func])[0];
                        html += "<th>Grouped by:" + $el.innerText + "</th>";
                    }
                }
                html += '</tr></thead><tbody>';
                for (var i in arrs) {
                    if (!isNaN(i)) {
                        html += '<tr>';
                        var $el = $("#list10 ." + activeColumns[i])[0];
                        html += "<td>" + $el.innerText + "</td>";
                        for (var j in arrs[i]) {
                            if (!isNaN(j)) {
                                html += "<td>" + arrs[i][j] + "</td>";
                            }
                        }
                        html += '</tr>';
                    }
                }
                html += '</tbody></table></div>';
                $(".result").append(html);
            }
        });
    }).on('click', '#export', function (e) {
        //  $(".loader").fadeOut("slow");
        $("#loading-image").show();
        var exportUrl = 'http://export.highcharts.com/';
        var images = [];
        $("[data-highcharts-chart]").each(function () {
            var object = {
                options: JSON.stringify($(this).highcharts().options),
                type: 'image/png',
                async: true
            };
            $.ajax({
                type: 'post',
                url: exportUrl,
                data: object,
                async: false,
                success: function (data) {
                    images.push(data);
                },
                error: function () {
                    $("#loading-image").hide();
                    alert("Не удалось создать .xlsx-файл! Попробуйбе позже.");
                    return;
                }
            });
        });
        var jsonObj = [];
        var imageIndex = 0;
        var tableValues = [];
        tableValues.push([]);
        tableValues.push([]);
        $("#valuesTable").find('thead tr th').each(function () {
            if ($(this).css('display') != 'none') {
                tableValues[1].push($(this).text());
                tableValues[0].push(this.getAttribute("data-original-title"));
            }
        });
        var ind = 2;
        $("#valuesTable").find('tbody tr').each(function () {
            tableValues.push([]);
            $(this).find('td').each(function () {
                if ($(this).css('display') != 'none') {
                    tableValues[ind].push($(this).text());
                }
            });
            //     tableValues[ind].pop();
            ind++;
        });
        jsonObj.push(tableValues);
        $("#result").find('div').each(function () {
            if ($(this).hasClass("table-responsive")) {
                var text = [];
                text.push([]);
                $(this).find('thead tr th').each(function () {
                    text[0].push($(this).text());
                });
                var ind = 1;
                $(this).find('tbody tr').each(function () {
                    text.push([]);
                    $(this).find('td').each(function () {
                        text[ind].push($(this).text());
                    });
                    ind++;
                });
                jsonObj.push(text);
            } else if ($(this).attr("data-highcharts-chart")) {
                var text = [];
                text.push([]);
                text[0][0] = images[imageIndex];
                imageIndex++;
                jsonObj.push(text);
            } else if ($(this).hasClass("header")) {
                var text = [];
                text.push([]);
                text[0][0] = $(this).find("h5").text();
                jsonObj.push(text);
            }
        });
        var jsonParams = {};
        jsonParams[csrfParameter] = csrfToken;
        jsonParams['values'] = JSON.stringify(jsonObj);
        $.ajax({
            type: 'post',
            url: "exportPng",
            data: jsonParams,
            success: function (data) {
                $("#loading-image").hide();
                window.location.href = "getExcelFile/" + data;
            },
            error: function () {
                $("#loading-image").hide();
                alert("Не удалось создать .xlsx-файл! Попробуйбе позже.");
                return;
            }

        });
    });

    $('#loading-image').bind('ajaxStart', function () {
        $(this).show();
    }).bind('ajaxStop', function () {
        $(this).hide();
    });

    function parseMDY(str) {
        // this example parses dates like "month/date/year"
        var parts = str.split('/');
        if (parts.length == 3) {
            return new XDate(
                parseInt(parts[2]), // year
                parseInt(parts[1] ? parts[1] - 1 : 0), // month
                parseInt(parts[0]) // date
            );
        }
    }
}(jQuery));
