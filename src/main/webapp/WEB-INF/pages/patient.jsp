<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>

<jsp:useBean id="now" class="java.util.Date"/>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Пациент</title>
    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery-3.1.0.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>

    <style>
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            margin-bottom: 80px;
        }

    </style>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://mknc.ru/">
                MKNC Patients
            </a>
        </div>
    </div>
</div>
<div class="container-fluid">

    <div class="row">
        <div class="container-fluid">
            <ol class="breadcrumb">
                <li><a href="<c:url value="${(patient.id==null)?'/welcome':'../../welcome'}"/>"><i
                        class="fa fa-home"></i> </a></li>
                <li class="active"><a
                        href="patients/update/+${patient.id}">Пациент</a>
                </li>
            </ol>
            <!--new patient-->
            <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
                <c:if test="${not empty msg}">
                    <div class="alert alert-${css} alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>${msg}</strong>
                    </div>
                </c:if>

            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-lg-offset-1">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-user fa-fw"></i> Пациент
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        ФИО<span class="pull-right text-muted small">${gpi.getPatient().name}</span>
                    </a>
                    <a href="#" class="list-group-item">
                        Пол<span class="pull-right text-muted small">${(gpi.getPatient().sex.equals("0"))?
                            "Женский":"Мужской"}</span>
                    </a>
                    <a href="#" class="list-group-item">
                        Дата рождения<span class="pull-right text-muted small">${gpi.getPatient().birthdate}
                    </span>
                    </a>
                    <a href="#" class="list-group-item">
                        Возраст<span class="pull-right text-muted small">${gpi.getPatient().age}
                    </span>
                    </a>
                    <a href="#" class="list-group-item">
                        Номер истории болезни<span class="pull-right text-muted small">${gpi.getPatient().diseaseNumber}
                    </span>
                    </a>
                    <a href="#" class="list-group-item">
                        Диагноз<span class="pull-right text-muted small">
                        ${gpi.getPatient().getDiagnosisName()}</span>
                    </a>
                    <a href="#" class="list-group-item">
                        Дата госпитализации<span class="pull-right text-muted small">
                    <div id="startDate">${gpi.getPatient().startDate}</div>
                </span>
                    </a>
                    <a href="#" class="list-group-item" id="surgeryDiv">
                        Дата операции<span class="pull-right text-muted small">
                    <div id="surgeryDate">${gpi.getPatient().surgeryDate}</div>
                </span>
                    </a>
                    <a href="#" class="list-group-item">
                        Количество койко-дней<span class="pull-right text-muted small">
                    <div id="bedDaysNum">${gpi.getPatient().bedDaysNumber}</div>
                </span>
                    </a>
                    <a href="#" class="list-group-item" id="afterSurgery">
                        Количество койко-дней после операции<span class="pull-right text-muted small">
                    <div id="surgeryBedDaysNum"></div>
                </span>
                    </a>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">Документы</div>
            <div class="panel-body" style="max-height: 100px;overflow-y: scroll;">
                <c:if test="${files != null && files.size() > 0}">
                    <div class="list-group">
                        <c:forEach var="i" begin="0" end="${files.size()-1}">
                            <a href="#"
                               class="list-group-item getFile ${files.get(i).patientId} ${files.get(i).fileId}">
                                    ${files.get(i).fileName}
                                        <span class="pull-right text-muted">
                                            <c:choose>
                                                <c:when test="${extensions.get(i).equals('pdf')}">
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                </c:when>
                                                <c:when test="${extensions.get(i).equals('xls') ||
                                                extensions.get(i).equals('xlsx')}">
                                                    <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                                                </c:when>
                                                <c:when test="${extensions.get(i).equals('doc') ||
                                                extensions.get(i).equals('docx')}">
                                                    <i class="fa fa-file-word-o" aria-hidden="true"></i>
                                                </c:when>
                                                <c:when test="${extensions.get(i).equals('png') ||
                                                extensions.get(i).equals('jpeg') || extensions.get(i).equals('jpg') ||
                                                extensions.get(i).equals('gif') || extensions.get(i).equals('bmp')}">
                                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                                </c:when>
                                                <c:when test="${extensions.get(i).equals('zip') ||
                                                extensions.get(i).equals('7z') || extensions.get(i).equals('rar')}">
                                                    <i class="fa fa-file-archive-o" aria-hidden="true"></i>
                                                </c:when>
                                                <c:when test="${extensions.get(i).equals('mp3') ||
                                                extensions.get(i).equals('ogg')}">
                                                    <i class="fa fa-file-audio-o" aria-hidden="true"></i>
                                                </c:when>
                                                <c:when test="${extensions.get(i).equals('ppt') ||
                                                extensions.get(i).equals('pptx')}">
                                                    <i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>
                                                </c:when>
                                                <c:when test="${extensions.get(i).equals('txt')}">
                                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                                </c:when>
                                                <c:otherwise>
                                                    <i class="fa fa-file-o" aria-hidden="true"></i>
                                                </c:otherwise>
                                            </c:choose>
                                        </span>
                            </a>
                        </c:forEach>
                    </div>
                </c:if>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-6 col-lg-offset-1">
    <div class="row">
        <div class="container-fluid">
            <!--Поступление-->
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-7">
                                <div>До операции</div>
                            </div>
                            <div class="col-sm-5 text-right">
                                <c:choose>
                                    <c:when test="${editor0 != null && !editor0.expired}">
                                        <p>До <em>${editor0.getFormattedDate()}</em></p>
                                    </c:when>
                                    <c:when test="${editor0 != null && editor0.expired}">
                                        <i class="fa fa-times fa-3x"></i>
                                    </c:when>
                                    <c:when test="${gpi.patient.closedDate != null && gpi.patient.closedDate.before(now)}">
                                        <i class="fa fa-times fa-3x"></i>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <div class="list-group">
                                <a href="../../generalInfo/${gpi.getPatient().id}" class="list-group-item">
                                    Общая информация<span class="pull-right">
                                <c:if test="${gpi.patient.filledGeneralInfo}"> <i class="fa fa-check"></i></c:if></span>
                                </a>
                                <a href="../../analysis/firstday/${gpi.getPatient().id}" class="list-group-item">
                                    Лабораторные данные(поступление)<span class="pull-right">
                                <c:if test="${gpi.patient.filledAnalysis0}"><i class="fa fa-check"></i></c:if></span>
                                </a>
                                <a href="../../objexamination/${gpi.getPatient().id}" class="list-group-item">
                                    Данные объективного осмотра<span class="pull-right">
                                 <c:if test="${gpi.patient.filledObjExam}"><i class="fa fa-check"></i></c:if>
                            </span>
                                </a>
                                <c:if test="${gpi.patient.diagnosis != diagnosisList.size() -1}">
                                    <a href="../../diagnosis/start/${gpi.getPatient().id}" class="list-group-item">
                                        Данные о диагнозе(поступление)<span class="pull-right">
                                 <c:if test="${gpi.patient.filledDiagnosisAtStart}"><i class="fa fa-check"></i></c:if>
                            </span>
                                    </a>
                                </c:if>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <!--Поступление-->
            <!--Операция-->
            <c:if test="${gpi.patient.hasSurgery && gpi.patient.surgeryDate != null}">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-7">
                                    <div>Операция</div>
                                </div>
                                <div class="col-sm-5 text-right">
                                    <c:choose>
                                        <c:when test="${editor1 != null && !editor1.expired}">
                                            <p>До <em>${editor1.getFormattedDate()}</em></p>
                                        </c:when>
                                        <c:when test="${editor1 != null && editor1.expired}">
                                            <i class="fa fa-times fa-3x"></i>
                                        </c:when>
                                        <c:when test="${gpi.patient.closedDate != null && gpi.patient.closedDate.before(now)}">
                                            <i class="fa fa-times fa-3x"></i>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <div class="list-group">
                                    <c:if test="${gpi.getPatient().diagnosis == 1 || gpi.getPatient().diagnosis == 2 ||
                                gpi.getPatient().diagnosis == 3 || gpi.getPatient().diagnosis == 4 ||
                                gpi.getPatient().diagnosis == 5 || gpi.getPatient().diagnosis == 6 || gpi.getPatient().diagnosis == 8}">
                                        <a href="../../twoStepsTreatment/${gpi.getPatient().id}"
                                           class="list-group-item">
                                            Двухэтапное лечение
                                        </a>
                                    </c:if>

                                    <c:if test="${gpi.patient.diagnosis != diagnosisList.size() -1}">
                                        <a href="../../surgery/${gpi.getPatient().id}" class="list-group-item">
                                            Данные по операции<span class="pull-right">
                                     <c:if test="${gpi.patient.filledSurgery}"><i class="fa fa-check"></i></c:if>
                                </span>
                                        </a>
                                    </c:if>
                                    <a href="../../analysis/day1/${gpi.getPatient().id}" class="list-group-item">
                                        Лабораторные данные (1-ый день после операции)<span class="pull-right">
                                     <c:if test="${gpi.patient.filledAnalysis1}"><i class="fa fa-check"></i></c:if>
                                </span>
                                    </a>
                                    <a href="../../analysis/day5/${gpi.getPatient().id}" class="list-group-item">
                                        Лабораторные данные (5-ый день после операции)<span class="pull-right">
                                     <c:if test="${gpi.patient.filledAnalysis5}"><i class="fa fa-check"></i></c:if>
                                </span>
                                    </a>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
            </c:if>
            <!--Операция-->
            <!--Осложнения после операций-->
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-7">
                                <div>Осложнения после операций</div>
                            </div>
                            <div class="col-sm-5 text-right">
                                <c:choose>
                                    <c:when test="${editor1 != null && !editor1.expired}">
                                        <p>До <em>${editor1.getFormattedDate()}</em></p>
                                    </c:when>
                                    <c:when test="${editor1 != null && editor1.expired}">
                                        <i class="fa fa-times fa-3x"></i>
                                    </c:when>
                                    <c:when test="${gpi.patient.closedDate != null && gpi.patient.closedDate.before(now)}">
                                        <i class="fa fa-times fa-3x"></i>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <div class="list-group">

                                <c:if test="${gpi.patient.diagnosis != diagnosisList.size() -1 &&
                                gpi.patient.hasSurgery && gpi.patient.surgeryDate != null} ">
                                    <a href="../../mainComplications/${gpi.getPatient().id}" class="list-group-item">
                                        Осложнения после основной операции<span class="pull-right"></span>
                                    </a>
                                </c:if>
                                <c:if test="${drains!= null && drains.size() != 0}">
                                    <a href="../../surgeryComplications/${gpi.getPatient().id}" class="list-group-item">
                                        Осложнения после дренирующих операций<span
                                            class="pull-right"></span>
                                    </a>
                                </c:if>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <!--Осложнения после операций-->
            <!--Сопутствующие операции-->
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-7">
                                <div>Дренирующие операции</div>
                            </div>
                            <div class="col-sm-5 text-right">
                                <c:choose>
                                    <c:when test="${editor1 != null && !editor1.expired}">
                                        <p>До <em>${editor1.getFormattedDate()}</em></p>
                                    </c:when>
                                    <c:when test="${editor1 != null && editor1.expired}">
                                        <i class="fa fa-times fa-3x"></i>
                                    </c:when>
                                    <c:when test="${gpi.patient.closedDate != null && gpi.patient.closedDate.before(now)}">
                                        <i class="fa fa-times fa-3x"></i>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <div class="list-group">
                                <a href="../../drain/${gpi.getPatient().id}" class="list-group-item">
                                    Данные о дренировании желчных протоков<span class="pull-right"></span>
                                </a>
                                <a href="../../punkcia/${gpi.getPatient().id}" class="list-group-item">
                                    Данные о пункционно-дренажном лечении жидкостных скоплений брюшной полости<span
                                        class="pull-right"></span>
                                </a>
                                <c:if test="${drains!= null && drains.size() != 0}">
                                    <c:forEach var="i" begin="0" end="${drains.size()-1}">
                                        <a href="../../drain/${gpi.getPatient().id}/${drains.get(i).drenId}"
                                           class="list-group-item">
                                            Дренирование желчных протоков ${drains.get(i).date}<span
                                                class="pull-right"><i
                                                class="fa fa-check"></i></span>
                                        </a>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${punkcias!= null && punkcias.size() != 0}">
                                    <c:forEach var="i" begin="0" end="${punkcias.size()-1}">
                                        <a href="../../punkcia/${gpi.getPatient().id}/${punkcias.get(i).drenId}"
                                           class="list-group-item">
                                            Пункционно-дренажное лечениие жидкостных скоплений ${punkcias.get(i).date}<span
                                                class="pull-right"><i
                                                class="fa fa-check"></i></span>
                                        </a>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <!--Сопутствующие операции-->
            <!--Выписка-->
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-sm-7">
                                <div>Выписка</div>
                            </div>
                            <div class="col-sm-5 text-right">
                                <c:choose>
                                    <c:when test="${editor1 != null && !editor1.expired}">
                                        <p>До <em>${editor1.getFormattedDate()}</em></p>
                                    </c:when>
                                    <c:when test="${editor1 != null && editor1.expired}">
                                        <i class="fa fa-times fa-3x"></i>
                                    </c:when>
                                    <c:when test="${gpi.patient.closedDate != null && gpi.patient.closedDate.before(now)}">
                                        <i class="fa fa-times fa-3x"></i>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <a href="#">
                        <div class="panel-footer">
                            <div class="list-group">
                                <%--<a href="../../diagnosis/end/${gpi.getPatient().id}" class="list-group-item">
                                    Данные о диагнозе (выписка)<span class="pull-right">
                                     <c:if test="${gpi.patient.filledDiagnosisAtEnd}"><i class="fa fa-check"></i></c:if>
                                </span>
                                </a>--%>
                                <a href="../../analysis/lastday/${gpi.getPatient().id}" class="list-group-item">
                                    Лабораторные данные (выписка)<span class="pull-right">
                                     <c:if test="${gpi.patient.filledAnalysis10}"><i class="fa fa-check"></i></c:if>
                                </span>
                                </a>
                                <a href="../../generalEndInfo/${gpi.getPatient().id}" class="list-group-item">
                                    Дата выписки (смерти)<span class="pull-right">
                                     <c:if test="${gpi.patient.endDate != null && gpi.patient.endDate.length() > 0}">
                                         <i class="fa fa-check"></i></c:if>
                                </span>
                                </a>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    function calculateDays(d1, d2) {
        var dayInMs = 24 * 60 * 60 * 1000;
        return Math.round((d1.getTime() - d2.getTime()) / (dayInMs));
    }
    function parseDate(str) {
        var mdy = str.split('/');
        return new Date(mdy[2], mdy[1] - 1, mdy[0]);
    }
    $(document).ready(function (e) {
        if (!document.getElementById("bedDaysNum").innerHTML) {
            var difference = calculateDays(new Date(), parseDate(document.getElementById("startDate").innerHTML));
            document.getElementById("bedDaysNum").innerHTML = difference;
            difference = calculateDays(new Date(), parseDate(document.getElementById("surgeryDate").innerHTML));
            if (difference >= 0) {
                document.getElementById("surgeryBedDaysNum").innerHTML = difference;
            } else {
                document.getElementById("afterSurgery").setAttribute("style", "display:none;");
            }
        } else {
            document.getElementById("afterSurgery").setAttribute("style", "display:none;");
        }
        if (!document.getElementById("surgeryDate").innerHTML) {
            document.getElementById("surgeryDiv").setAttribute("style", "display:none;");
        }
    });

    $(document).on("click", ".getFile", function (e) {
        e.preventDefault();
        var aclasses = e.target.className.split(' ');
        var jsonParams = {};
        jsonParams['id'] = aclasses[aclasses.length - 2];
        jsonParams['fileId'] = aclasses[aclasses.length - 1];
        $.ajax({
            type: 'get',
            url: "../../getFile",
            data: jsonParams,
            success: function () {
                window.location = '../../getFile?id=' + jsonParams['id'] + '&fileId=' + jsonParams['fileId'];
            },
            error: function () {
                alert("Не удалось выгрузить файл! Попробуйбе позже.");
                return;
            }
        });
    });
</script>
</body>
</html>
