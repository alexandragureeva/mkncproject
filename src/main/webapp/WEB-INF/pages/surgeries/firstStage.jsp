<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>

<div id="firstStage">
    <div class="row buttonRow">
        <h4 class="section-heading buttonRow"><i class="fa fa-bar-chart"></i>Результаты первого этапа:</h4>
        <hr>
    </div>
    <div class="row buttonRow">
        <div class="col-xs-12">
            <label>Определение степени и скорости гипертрофии остатка
                печени:</label>
        </div>
    </div>
    <div class="row red">
        <div class="col-xs-3">
            <label for="objem">Объем (%):</label>
        </div>
        <div class="col-xs-9">
            <spring:bind path="objem">
                <div class="${ status.error ? 'has-error' : ''}">
                    <form:input type="number" step="any" id="objem"
                                path="objem"

                                class="form-control"/>
                    <form:errors path="objem" class="control-label"/>
                </div>
            </spring:bind>
        </div>
    </div>
    <div class="row red">
        <div class="col-xs-3">
            <label for="dostignuta">На какие сутки достигнута:</label>
        </div>
        <div class="col-xs-9">
            <spring:bind path="dostignuta">
                <div class="${ status.error ? 'has-error' : ''}">
                    <form:input type="number" step="any" id="dostignuta"
                                path="dostignuta"

                                class="form-control"/>
                    <form:errors path="dostignuta" class="control-label"/>
                </div>
            </spring:bind>
        </div>
    </div>
    <div class="row red">
        <div class="col-xs-3">
            <label for="budushOstPech">Исходный объем будущего остатка печени (%):</label>
        </div>
        <div class="col-xs-9">
            <spring:bind path="budushOstPech">
                <div class="${ status.error ? 'has-error' : ''}">
                    <form:input type="number" step="any" id="budushOstPech"
                                path="budushOstPech"

                                class="form-control"/>
                    <form:errors path="budushOstPech" class="control-label"/>
                </div>
            </spring:bind>
        </div>
    </div>

    <div id="pechen"></div>

    <div class="row red">
        <div class="col-xs-3">
            <label>Осложнения:</label>
        </div>
        <div class="col-xs-9">
            <ul class="list-unstyled">
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="nepolnayaPortoembF">
                            <form:checkbox id="nepolnayaPortoembF" path="nepolnayaPortoembF"
                                           value="1"/><b>
                            Неполная портоэмболизация</b>
                        </spring:bind></div>

                        <div class="col-xs-6" id="nepolnayaPortoembRow">
                            <spring:bind path="nepolnayaPortoembTypeF">
                                <form:select
                                        class="form-control" id="nepolnayaPortoembTypeF"
                                        path="nepolnayaPortoembTypeF">
                                    <form:option value="0">---------</form:option>
                                    <form:option
                                            value="1">Технически-обусловленная</form:option>
                                    <form:option
                                            value="2">Анатомически-обусловленная</form:option>
                                </form:select> </spring:bind>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="nepolnayaGipertrofiaF">
                            <form:checkbox id="nepolnayaGipertrofiaF"
                                           path="nepolnayaGipertrofiaF" value="1"/><b>
                            Недостаточная гипертрофия печени</b>
                        </spring:bind></div>

                        <div class="col-xs-6" id="nepolnayaGipertrofiaRow">
                            <spring:bind path="nepolnayaGipertrofiaTypeF">
                                <form:select
                                        class="form-control"
                                        id="nepolnayaGipertrofiaTypeF"
                                        path="nepolnayaGipertrofiaTypeF">
                                    <form:option value="0">---------</form:option>
                                    <form:option
                                            value="1">В результате технических погрешностей ПВЭ</form:option>
                                    <form:option
                                            value="2">В результате снижения регенераторного потенциала печени</form:option>
                                </form:select> </spring:bind>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="pechNedostatochnostF">
                            <form:checkbox id="pechNedostatochnostF"
                                           path="pechNedostatochnostF"
                                           value="1"/><b>
                            Печеночная недостаточность</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="zelcheistechenieF">
                            <form:checkbox id="zelcheistechenieF" path="zelcheistechenieF"
                                           value="1"/><b>
                            Желчеистечение</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="cholangitF">
                            <form:checkbox id="cholangitF"
                                           path="cholangitF" value="1"/><b>
                            Холангит</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="nagnoeniePecheniF">
                            <form:checkbox id="nagnoeniePecheniF" path="nagnoeniePecheniF" value="1"/><b>
                            Нагноение по срезу печени</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="postoblSyndromF">
                            <form:checkbox id="postoblSyndromF" path="postoblSyndromF" value="1"/><b>
                            Постобляционный синдром</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="postEmbSyndromF">
                            <form:checkbox id="postEmbSyndromF" path="postEmbSyndromF" value="1"/><b>
                            Постэмболизационный синдром</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="krovotechenieF">
                            <form:checkbox id="krovotechenieF"
                                           path="krovotechenieF" value="1"/><b>
                            Кровотечение в брюшную полость</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="gemobiliaF">
                            <form:checkbox id="gemobiliaF" path="gemobiliaF" value="1"/><b>
                            Гемобилия</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-12"><spring:bind path="febLichoradkaF">
                            <form:checkbox id="febLichoradkaF" path="febLichoradkaF" value="1"/><b>
                            Фебрильная лихорадка (резорбтивная)</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-12"><spring:bind path="abscessF">
                            <form:checkbox id="abscessF" path="abscessF" value="1"/><b>
                            Абсцедирование печени</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-12"><spring:bind path="zhidkSkopleniaF">
                            <form:checkbox id="zhidkSkopleniaF" path="zhidkSkopleniaF" value="1"/><b>
                            Жидкостные скопления в брюшной полости</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-12"><spring:bind path="trombozF">
                            <form:checkbox id="trombozF" path="trombozF" value="1"/><b>
                            Тромбоз воротной вены</b>
                        </spring:bind></div>
                    </div>
                </li>
                <li>
                    <div class="row buttonRow">
                        <div class="col-xs-6"><spring:bind path="otherF">
                            <form:checkbox id="otherF" path="otherF" value="1"/><b>
                            Другое</b>
                        </spring:bind></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <hr>
</div>


<script>

    function nepolnayaPortoembFunction() {
        if (document.getElementById("nepolnayaPortoembF").checked) {
            document.getElementById("nepolnayaPortoembRow").removeAttribute("style");
        } else {
            document.getElementById("nepolnayaPortoembRow").setAttribute("style", "display:none;");
            $("#nepolnayaPortoembTypeF").val(0);
        }
    }

    nepolnayaPortoembFunction();
    $("#nepolnayaPortoembF").on("change", nepolnayaPortoembFunction);

    function nepolnayaGipertrofiaPortembolFunction() {
        if (document.getElementById("nepolnayaGipertrofiaF").checked) {
            document.getElementById("nepolnayaGipertrofiaRow").removeAttribute("style");
        } else {
            document.getElementById("nepolnayaGipertrofiaRow").setAttribute("style", "display:none;");
            $("#nepolnayaGipertrofiaTypeF").val(0);
        }
    }

    nepolnayaGipertrofiaPortembolFunction();
    $("#nepolnayaGipertrofiaF").on("change", nepolnayaGipertrofiaPortembolFunction);


</script>