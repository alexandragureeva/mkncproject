<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true"%>

<spring:bind path="rezekciaType">
    <div class="row">
        <div class="col-xs-3">
            <label for="rezekciaType">Резекция печени:</label>
        </div>
        <div class="col-xs-9">
            <form:select
                    class="form-control" id="rezekciaType" path="rezekciaType">
                <form:option value="0">---------</form:option>
                <form:option value="1">Лапароскопическая</form:option>
                <form:option value="2">Роботическая</form:option>
                <form:option value="3">Открытая</form:option>
                <form:option value="4">Гибридная</form:option>
            </form:select>
        </div>
    </div>
</spring:bind>
<spring:bind path="razdelenieTkanei">
    <div class="row">
        <div class="col-xs-3">
            <label for="razdelenieTkanei">Разделение тканей печени:</label>
        </div>
        <div class="col-xs-9">
            <form:select
                    class="form-control" id="razdelenieTkanei" path="razdelenieTkanei">
                <form:option value="0">---------</form:option>
                <form:option value="1">Зажимом</form:option>
                <form:option value="2">Биполярным коагулятором</form:option>
                <form:option value="3">Ультразвуковым деструктором</form:option>
                <form:option value="4">Водоструйным деструктором</form:option>
            </form:select>
        </div>
    </div>
</spring:bind>


