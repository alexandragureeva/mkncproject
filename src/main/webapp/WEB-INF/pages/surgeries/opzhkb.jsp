<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Операция при ЖКБ</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .row:not(.buttonRow):not(.section-header):not(.unnecessaryRow):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row buttonRow">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${zhkb.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные об операции при диагнозе "Желчнокаменная болезнь"</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveOpZhkb?${_csrf.parameterName}=${_csrf.token}" var="zhkbUrl"/>
                <form:form action="${zhkbUrl}" modelAttribute="zhkb" method="post"
                           id="zhkbForm">

                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>

                    <spring:bind path="date">
                        <form:hidden path="date"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">


                    <spring:bind path="dlitop">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="dlitop">Длительность операции (мин.):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="dlitop" path="dlitop" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <jsp:include page="subSurgery.jsp"/>
                    <spring:bind path="cholecistekt">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="cholecistekt">Холецистэктомия:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="cholecistekt" path="cholecistekt">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Открытая</form:option>
                                    <form:option value="2">Лапароскопическая</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>

                    <div class="row">
                        <div class="col-xs-12">
                            <spring:bind path="drenirovanie">
                                <form:checkbox
                                        path="drenirovanie" id="drenirovanie" value="1"/><b>С дренированием желчных
                                протоков</b>
                            </spring:bind>
                        </div>
                    </div>

                    <spring:bind path="drenMethod">
                        <div class="row" style="display: none;" id="drenMethodRow">
                            <div class="col-xs-3">
                                <label for="drenMethod">Дренирование:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="drenMethod" path="drenMethod">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">По Холстеду</form:option>
                                    <form:option value="2">По Керу</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>

                    <div class="row">
                        <div class="col-xs-3">
                            <label>Особенности холецистэктомии:</label>
                        </div>
                        <div class="col-xs-9">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12"><spring:bind path="primbram">
                                            <form:checkbox id="primbram" path="primbram" value="1"/><b>
                                            По Примбраму</b>
                                        </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12"><spring:bind path="infiltrat">
                                            <form:checkbox id="infiltrat" path="infiltrat" value="1"/><b>Перипузырный
                                            инфильтрат</b>
                                        </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12"><spring:bind path="vnutrRaspol">
                                            <form:checkbox id="vnutrRaspol" path="vnutrRaspol" value="1"/><b>
                                            Внутрипеченочное расположение желчного пузыря</b>
                                        </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12"><spring:bind path="zhelchPuz">
                                            <form:checkbox id="zhelchPuz" path="zhelchPuz" value="1"/><b>
                                            Сморщенный желчный пузырь</b>
                                        </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12"><spring:bind path="kamen">
                                            <form:checkbox id="kamen" path="kamen" value="1"/><b>
                                            Камень в пузырном протоке</b>
                                        </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-6"><spring:bind path="svish">
                                            <form:checkbox id="svish" path="svish" value="1"/><b>
                                            Наличие свища желчного пузыря с полом органом</b>
                                        </spring:bind></div>

                                        <div class="col-xs-6" style="display: none;" id="svishTypeRow">
                                            <spring:bind path="svishType">
                                                <form:select
                                                        class="form-control" id="svishType" path="svishType">
                                                    <form:option value="0">---------</form:option>
                                                    <form:option value="1">С ДПК</form:option>
                                                    <form:option value="2">С толстой кишкой</form:option>
                                                </form:select> </spring:bind>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-6"><spring:bind path="mirizi">
                                            <form:checkbox id="mirizi" path="mirizi" value="1"/><b>
                                            Операция при синдроме Миризи</b>
                                        </spring:bind></div>

                                        <div class="col-xs-6" style="display: none;" id="miriziZavershenieRow">
                                            <spring:bind path="miriziZavershenie">
                                                <form:select
                                                        class="form-control" id="miriziZavershenie"
                                                        path="miriziZavershenie">
                                                    <form:option value="0">---------</form:option>
                                                    <form:option value="1">Завершение с ГЕА</form:option>
                                                    <form:option value="2">Завершение без ГЕА</form:option>
                                                </form:select> </spring:bind>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-3">
                            <label>Интраоперационные осложнения:</label>
                        </div>
                        <div class="col-xs-9">
                            <ul class="list-unstyled">
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12"><spring:bind path="perfor">
                                            <form:checkbox id="perfor" path="perfor" value="1"/><b>Перфорация желчного
                                            пузыря</b>
                                        </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-6"><spring:bind path="povrezhdeniePologo">
                                            <form:checkbox id="povrezhdeniePologo" path="povrezhdeniePologo" value="1"/><b>
                                            Повреждение полого органа</b>
                                        </spring:bind></div>

                                        <div class="col-xs-6" style="display: none;" id="povrezhdeniePologoTypeRow">
                                            <spring:bind path="povrezhdeniePologoType">
                                            <form:select
                                                    class="form-control" id="povrezhdeniePologoType"
                                                    path="povrezhdeniePologoType">
                                                <form:option value="0">---------</form:option>
                                                <form:option value="1">ДПК</form:option>
                                                <form:option value="2">Ободочной кишки</form:option>
                                            </form:select> </spring:bind>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <spring:bind path="povrezhdCholed">
                                            <form:checkbox id="povrezhdCholed" path="povrezhdCholed" value="1"/><b>
                                            Повреждение холедоха</b>
                                        </spring:bind></div>

                                        <div class="col-xs-6" style="display: none;" id="povrezhdCholedTypeRow">
                                            <spring:bind path="povrezhdCholedType">
                                            <form:select
                                                    class="form-control" id="povrezhdCholedType"
                                                    path="povrezhdCholedType">
                                                <form:option value="0">---------</form:option>
                                                <form:option value="1">Неполное пересечение</form:option>
                                                <form:option value="2">Полное пересечение</form:option>
                                                <form:option value="3">Клипирование</form:option>
                                            </form:select> </spring:bind>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12"><spring:bind path="povrezhdArterii">
                                            <form:checkbox id="povrezhdArterii" path="povrezhdArterii" value="1"/><b>
                                            Повреждение печеночной артерии
                                        </b>
                                        </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-xs-12"><spring:bind path="povrezhdVeni">
                                            <form:checkbox id="povrezhdVeni" path="povrezhdVeni" value="1"/><b>
                                            Повреждение воротной вены
                                        </b>
                                        </spring:bind></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>
                </form:form>

                <script>
                    $(document).ready(function () {
                        var expired = document.getElementById("expiredValue").value;
                        var isAdmin = document.getElementById("isAdminValue").value;
                        if (expired === "true" && isAdmin === "false") {
                            var c = document.getElementById("zhkbForm").elements;
                            for (var i = 0; i < c.length; i++) {
                                c[i].setAttribute("disabled", true);
                            }
                        }

                        function drenirovanieFunc() {
                            if (document.getElementById("drenirovanie").checked) {
                                document.getElementById("drenMethodRow").removeAttribute("style");
                            } else {
                                document.getElementById("drenMethodRow").setAttribute("style", "display:none;");
                                $("#drenMethod").val(0);
                            }
                        }

                        drenirovanieFunc();
                        $("#drenirovanie").on("change", drenirovanieFunc);

                        function svishFunc() {
                            if (document.getElementById("svish").checked) {
                                document.getElementById("svishTypeRow").removeAttribute("style");
                            } else {
                                document.getElementById("svishTypeRow").setAttribute("style", "display:none;");
                                $("#svishType").val(0);
                            }
                        }

                        svishFunc();
                        $("#svish").on("change", svishFunc);

                        function miriziFunc() {
                            if (document.getElementById("mirizi").checked) {
                                document.getElementById("miriziZavershenieRow").removeAttribute("style");
                            } else {
                                document.getElementById("miriziZavershenieRow").setAttribute("style", "display:none;");
                                $("#miriziZavershenie").val(0);
                            }
                        }

                        miriziFunc();
                        $("#mirizi").on("change", miriziFunc);


                        function povrezhdeniePologoFunc() {
                            if (document.getElementById("povrezhdeniePologo").checked) {
                                document.getElementById("povrezhdeniePologoTypeRow").removeAttribute("style");
                            } else {
                                document.getElementById("povrezhdeniePologoTypeRow").setAttribute("style", "display:none;");
                                $("#povrezhdeniePologoType").val(0);
                            }
                        }

                        povrezhdeniePologoFunc();
                        $("#povrezhdeniePologo").on("change", povrezhdeniePologoFunc);


                        function povrezhdCholedFunc() {
                            if (document.getElementById("povrezhdCholed").checked) {
                                document.getElementById("povrezhdCholedTypeRow").removeAttribute("style");
                            } else {
                                document.getElementById("povrezhdCholedTypeRow").setAttribute("style", "display:none;");
                                $("#povrezhdCholedType").val(0);
                            }
                        }

                        povrezhdCholedFunc();
                        $("#povrezhdCholed").on("change", povrezhdCholedFunc);
                    });
                </script>
            </div>
        </div>
    </div>
</div>
</body>
</html>