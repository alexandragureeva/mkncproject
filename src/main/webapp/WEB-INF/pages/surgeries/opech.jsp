<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Операция при эхинококкозе</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .row:not(.buttonRow):not(.section-header):before {
            content: "*";
            color: red;
        }

        #oslozhneniyaDiv {
            border-left: 6px solid dodgerblue;
            background-color: lightblue;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row buttonRow">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${echinokokkoz.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">

            <div class="modal-body">
                <c:url value="/saveOpEchinokokkoz?${_csrf.parameterName}=${_csrf.token}" var="echinokokkozUrl"/>
                <form:form action="${echinokokkozUrl}" modelAttribute="echinokokkoz" method="post"
                           id="echinokokkozForm">
                    <div class="modal-header">
                        <h4><i class="fa fa-bar-chart"></i>Данные об операции при диагнозе "Эхинококкоз"</h4>
                    </div>
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>

                    <spring:bind path="date">
                        <form:hidden path="date"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="dlitop">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="dlitop">Длительность операции (мин.):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="dlitop" path="dlitop" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <jsp:include page="subSurgery.jsp"/>
                    <spring:bind path="rezekcia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="rezekcia">Эхинококкэктомия:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="rezekcia" path="rezekcia">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Идеальная эхтнококкэктомия</form:option>
                                    <form:option value="2">Открытая эхинококкэктомия</form:option>
                                    <form:option value="3">Сочетание обоих методов</form:option>
                                    <form:option value="4">Резекция печени</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="otkrEch">
                        <div class="row" id="otkrEchGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="otkrEch">Открытая эхинококкэктомия:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="otkrEch" path="otkrEch">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">С тотальной перицисэктомией</form:option>
                                    <form:option value="2">С субтотальной перицистэктомией</form:option>
                                    <form:option value="3">С частичной перицистэктомией</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="obrabotkaFibr">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="obrabotkaFibr">Обработка остаточной полости фиброзной капсулы:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="obrabotkaFibr" path="obrabotkaFibr">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Капитонаж</form:option>
                                    <form:option value="3">Тампонирование сальником</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="obrabotkaSvich">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="obrabotkaSvich">Обработки желочного свища:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="obrabotkaSvich" path="obrabotkaSvich">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Ушивание устья в фиброзной капсуле</form:option>
                                    <form:option value="3">Клипирование устья в фиб</form:option>
                                    <form:option
                                            value="4">Выделение и перевязка сосудисто-секреторной ножки</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="chirurgLech">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="chirurgLech">Хирургическое лечение:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="chirurgLech" path="chirurgLech">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Пункционно-дренажное лечение (PAER)</form:option>
                                    <form:option value="2">Лапроскапическая</form:option>
                                    <form:option value="3">Открытая</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="row">
                        <div class="col-xs-12">
                            <spring:bind path="etapLech">
                                <form:checkbox
                                        path="etapLech"
                                        value="0"/><b>Этапное лечение с предварительной паиротдельным этапом</b>
                            </spring:bind>
                        </div>
                    </div>
                    <spring:bind path="simOper">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="simOper">Симультанная операция:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="simOper" path="simOper">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">нет</form:option>
                                    <form:option value="2">В сочетании с резекцией легкого</form:option>
                                    <form:option
                                            value="3">В сочетании с резекцией др органов брюшной полости</form:option>
                                    <form:option value="4">В сочетании с удалением внеорганных кист</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="echgdc">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Пережатие ГДС:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="echgdc" id="echgdc_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="echgdc" id="echgdc_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="echgdcmin">
                        <div class="row" id="echgdcgroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="echgdcmin">Пережатие ГДС (мин):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="echgdcmin" path="echgdcmin" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="bypass">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Bypass:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="totIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Тотальная сосудистая изоляция (с пережатием НПВ):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="selIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Селективная сосудистая изоляция (с раздельным пережатием печеночных вен):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="selIz" id="selIz_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="selIz" id="selIz_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="intraOsl">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="intraOsl">Спецефические интраоперационные осложнение:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="intraOsl" path="intraOsl">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">нет</form:option>
                                    <form:option value="2">Анафелоксия с падением АД</form:option>
                                    <form:option value="3">Разрыв кисты с опсеменением брюшной полости</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <c:if test="${showOslozhneniya}">
                        <div id="oslozhneniyaDiv">
                            <jsp:include page="oslozhneniya.jsp"/>
                            <spring:bind path="clavien">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>Классификация осложнений по Clavien-Dindo:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="clavien" path="clavien">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">I</form:option>
                                            <form:option value="2">II</form:option>
                                            <form:option value="3">IIIa</form:option>
                                            <form:option value="4">IIIb</form:option>
                                            <form:option value="5">IVa</form:option>
                                            <form:option value="6">IVb</form:option>
                                            <form:option value="7">V</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </c:if>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>
                </form:form>
                <script>
                    $(document).on("change", "#rezekcia", function (e) {
                        var selectedVal = $(this).find("option:selected").val();
                        switch (selectedVal) {
                            case "2":
                                document.getElementById("otkrEchGroup").setAttribute("style", "display:block;");
                                break;
                            default:
                                document.getElementById("otkrEchGroup").setAttribute("style", "display:none;");
                                document.getElementById("otkrEch").value = 0;
                                break;
                        }
                    });
                    $(document).on("click", "#echgdc_yes", function (e) {
                        document.getElementById("echgdcgroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#echgdc_no", function (e) {
                        document.getElementById("echgdcgroup").setAttribute("style", "display:none;");
                        document.getElementById("echgdcmin").value = "";
                    });
                    if (document.getElementById('echgdc_yes').checked) {
                        document.getElementById("echgdcgroup").setAttribute("style", "display:block;");
                    }
                    $(document).ready(function () {
                        var expired = document.getElementById("expiredValue").value;
                        var isAdmin = document.getElementById("isAdminValue").value;
                        if (expired === "true" && isAdmin === "false") {
                            var c = document.getElementById("echinokokkozForm").elements;
                            for (var i = 0; i < c.length; i++) {
                                c[i].setAttribute("disabled", true);
                            }
                        }
                    });
                </script>
            </div>
        </div>
    </div>
</div>
</body>
</html>