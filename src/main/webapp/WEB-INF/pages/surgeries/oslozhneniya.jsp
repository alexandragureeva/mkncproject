<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>

<spring:bind path="showComplications">
    <form:hidden path="showComplications"/>
</spring:bind>

<spring:bind path="osl">
    <div class="row">
        <div class="col-xs-3">
            <label>Осложнения:</label>
        </div>
        <div class="col-xs-9">
            <label class="radio-inline"><form:radiobutton path="osl" id="osl_no" name="optradio"
                                                          value="0"/>Нет</label>
            <label class="radio-inline"><form:radiobutton path="osl" id="osl_yes"
                                                          name="optradio"
                                                          value="1"/>Да</label>
        </div>
    </div>
</spring:bind>
<div id="oslozhneniya" style="display: none;">
    <ul class="list-unstyled col-sm-offset-3">
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="vnutrKrovot">
                        <form:checkbox id="vnutrKrovot" path="vnutrKrovot" value="1"/><b>Внутрибрюшное
                        кровотечение</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="zhkk">
                        <form:checkbox id="zhkk" path="zhkk" value="1"/><b>ЖКК</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="pechNedostatA">
                        <form:checkbox id="pechNedostatA" path="pechNedostatA" value="1"/><b>Печеночная недостаточность
                        A</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="pechNedostatB">
                        <form:checkbox id="pechNedostatB" path="pechNedostatB" value="1"/><b>Печеночная недостаточность
                        B</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="pechNedostatC">
                        <form:checkbox id="pechNedostatC" path="pechNedostatC" value="1"/><b>Печеночная недостаточность
                        C</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="nagnoenie">
                        <form:checkbox id="nagnoenie" path="nagnoenie" value="1"/><b>Нагноение раны</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="cholangit">
                        <form:checkbox id="cholangit" path="cholangit" value="1"/><b>Холангит</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="cholAbscedir">
                        <form:checkbox id="cholAbscedir" path="cholAbscedir" value="1"/><b>Холангиогенное
                        абсцедирование</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="ascit">
                        <form:checkbox id="ascit" path="ascit" value="1"/><b>Асцит</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="eventracia">
                        <form:checkbox id="eventracia" path="eventracia" value="1"/><b>Эвентрация</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="kishNeprohod">
                        <form:checkbox id="kishNeprohod" path="kishNeprohod" value="1"/><b>Кишечная непроходимость</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="pankreatSvich">
                        <form:checkbox id="pankreatSvich" path="pankreatSvich" value="1"/><b>Панкреатический свищ</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="ostriiSvich">
                        <form:checkbox id="ostriiSvich" path="ostriiSvich" value="1"/><b>Острый панкреатит</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="sepsis">
                        <form:checkbox id="sepsis" path="sepsis" value="1"/><b>Сепсис</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="tyazhSepsis">
                        <form:checkbox id="tyazhSepsis" path="tyazhSepsis" value="1"/><b>Тяжелый сепсис</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="septShok">
                        <form:checkbox id="septShok" path="septShok" value="1"/><b>Септический шок</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="zhelchSvish">
                        <form:checkbox id="zhelchSvish" path="zhelchSvish" value="1"/><b>Желчный свищ</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="kishSvish">
                        <form:checkbox id="kishSvish" path="kishSvish" value="1"/><b>Кишечный свищ</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="trombVorotVeni">
                        <form:checkbox id="trombVorotVeni" path="trombVorotVeni" value="1"/><b>Тромбоз воротной вены</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="trombozPechArter">
                        <form:checkbox id="trombozPechArter" path="trombozPechArter" value="1"/><b>Тромбоз печеночной
                        артерии</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="odnostorPnevm">
                        <form:checkbox id="odnostorPnevm" path="odnostorPnevm" value="1"/><b>Односторонняя пневмония</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="dvustorPnevm">
                        <form:checkbox id="dvustorPnevm" path="dvustorPnevm" value="1"/><b>Двусторонняя пневмония</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="gidrotorax">
                        <form:checkbox id="gidrotorax" path="gidrotorax" value="1"/><b>Гидроторакс</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="pochNedostat">
                        <form:checkbox id="pochNedostat" path="pochNedostat" value="1"/><b>Почечная недостаточность</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="serdSosNedostat">
                        <form:checkbox id="serdSosNedostat" path="serdSosNedostat" value="1"/><b>Сердечно-сосудистая
                        недостаточность</b>
                    </spring:bind></div>
            </div>
        </li>
        <li>
            <div class="row buttonRow">
                <div class="col-xs-12">
                    <spring:bind path="poliorganNedostat">
                        <form:checkbox id="poliorganNedostat" path="poliorganNedostat" value="1"/><b>Полиорганная
                        недостаточность</b>
                    </spring:bind></div>
            </div>
        </li>
        <c:if test="${gcr != null}">
            <li>
                <div class="row buttonRow">
                    <div class="col-xs-12">
                        <spring:bind path="vrv">
                            <form:checkbox id="vrv" path="vrv" value="1"/><b>Из ВРВ пищевода и желудка</b>
                        </spring:bind></div>
                </div>
            </li>
            <li>
                <div class="row buttonRow">
                    <div class="col-xs-12">
                        <spring:bind path="postRezNedostA">
                            <form:checkbox id="postRezNedostA" path="postRezNedostA" value="1"/><b>Пострезекционная
                            недостаточность A</b>
                        </spring:bind></div>
                </div>
            </li>
            <li>
                <div class="row buttonRow">
                    <div class="col-xs-12">
                        <spring:bind path="postRezNedostB">
                            <form:checkbox id="postRezNedostB" path="postRezNedostB" value="1"/><b>Пострезекционная
                            недостаточность B</b>
                        </spring:bind></div>
                </div>
            </li>
            <li>
                <div class="row buttonRow">
                    <div class="col-xs-12">
                        <spring:bind path="postRezNedostC">
                            <form:checkbox id="postRezNedostC" path="postRezNedostC" value="1"/><b>Пострезекционная
                            недостаточность C</b>
                        </spring:bind></div>
                </div>
            </li>
        </c:if>
        <c:if test="${klatskin != null}">
            <li>
                <div class="row buttonRow">
                    <div class="col-xs-12">
                        <spring:bind path="nesostAnastomoza">
                            <form:checkbox id="nesostAnastomoza" path="nesostAnastomoza" value="1"/><b>
                            Несостоятельность анастомоза
                        </b>
                        </spring:bind></div>
                </div>
            </li>
        </c:if>
    </ul>
</div>
<script>
    $(document).on("click", "#osl_yes", function (e) {
        document.getElementById("oslozhneniya").setAttribute("style", "display:block;");
    });
    $(document).on("click", "#osl_no", function (e) {
        document.getElementById("oslozhneniya").setAttribute("style", "display:none;");
        $('#oslozhneniya').find('input[type=checkbox]:checked').removeAttr('checked');
    });
    $(document).ready(function () {
        if (document.getElementById('osl_yes').checked) {
            document.getElementById("oslozhneniya").setAttribute("style", "display:block;");
        }
        if ($("#showComplications").val() === "true") {
            $('html, body').animate({
                scrollTop: $('#osl_no').offset().top
            });
        }
    });

</script>