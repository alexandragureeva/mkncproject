<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Операция при опухоли Клацкина</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .row:not(.buttonRow):not(.unnecessaryRow):not(.section-header):before {
            content: "*";
            color: red;
        }

        #oslozhneniyaDiv {
            border-left: 6px solid dodgerblue;
            background-color: lightblue;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row buttonRow">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${klatskin.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">

            <div class="modal-body">
                <c:url value="/saveOpKlatskin?${_csrf.parameterName}=${_csrf.token}" var="klatskinUrl"/>
                <form:form action="${klatskinUrl}" modelAttribute="klatskin" method="post"
                           id="klatskinForm">
                    <c:if test="${twoStepsTreatment}">
                        <jsp:include page="firstStage.jsp"/>
                    </c:if>

                    <div class="modal-header">
                        <h4><i class="fa fa-bar-chart"></i>Данные об операции при диагнозе "Опухоль Клацкина"</h4>
                    </div>
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>

                    <spring:bind path="date">
                        <form:hidden path="date"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">
                    <spring:bind path="dlitop">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="dlitop">Длительность операции (мин.):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="dlitop" path="dlitop" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <jsp:include page="subSurgery.jsp"/>
                    <spring:bind path="rezekcia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="rezekcia">Объем резекции печени:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="rezekcia" path="rezekcia">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Левосторонняя гемигепатэктомия (ЛГГ)</form:option>
                                    <form:option value="2">Правосторонняя гемигепатэктомия (ПГГ)</form:option>
                                    <form:option value="3">ПГГ + 1-й сегмент</form:option>
                                    <form:option value="4">ЛГГ + 1-й сегмент</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="klatsosudrezekcia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="klatsosudrezekcia">Резекция сосудов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="klatsosudrezekcia" path="klatsosudrezekcia">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Резекция воротной вены</form:option>
                                    <form:option value="2">Резекция печеночной артерии</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="klatvorotrezekcia">
                        <div class="row" id="klatvorotrezekciaGroup" style="display: none">
                            <div class="col-xs-3">
                                <label for="klatvorotrezekcia">Резекция воротной вены:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="klatvorotrezekcia" path="klatvorotrezekcia">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Краевая</form:option>
                                    <form:option value="2">Циркулярная без протезирования</form:option>
                                    <form:option value="3">Циркулярная с протезированием</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="krovopoteria">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="krovopoteria">Объем кровопотери (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="krovopoteria" path="krovopoteria">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1"><500</form:option>
                                    <form:option value="2">500-1000</form:option>
                                    <form:option value="3">>1000</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="gemotransfusia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Гемотрансфузия:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="gemotransfusia" id="klatgem_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="gemotransfusia" id="klatgem_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="klatgemotransfusia_ml">
                        <div class="row" id="klatgemGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="klatgemotransfusia_ml">Объем гемотрансфузии (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="klatgemotransfusia_ml" path="klatgemotransfusia_ml" class="form-control"
                                        type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="plazma">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="plazma">Объем плазмы трансфузии (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="plazma" path="plazma" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="gdc">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Пережатие ГДС:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="gdc" id="klatgdc_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="gdc" id="klatgdc_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="klatgdcmin">
                        <div class="row" id="klatgdcminGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="klatgdcmin">Пережатие ГДС (мин):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="klatgdcmin" path="klatgdcmin" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Формирование гепатикоюноанастомозы(ГЕА)</label>
                            </div>
                        </div>
                        <spring:bind path="geatype1">
                            <div class="row">
                                <div class="col-xs-9 col-xs-offset-3">
                                    <form:select
                                            class="form-control" path="geatype1" id="geatype1">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Единый</form:option>
                                        <form:option value="2">Раздельный</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="geatype2">
                            <div class="row">
                                <div class="col-xs-9 col-xs-offset-3">
                                    <form:select
                                            class="form-control" path="geatype2" id="geatype2">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Моно-ГЕА</form:option>
                                        <form:option value="2">Би-ГЕА</form:option>
                                        <form:option value="3">Три-ГЕА</form:option>
                                        <form:option value="4">Тетра-ГЕА</form:option>
                                        <form:option value="5">Пента-ГЕА</form:option>
                                        <form:option value="6">Мульти-ГЕА</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="geatype3">
                            <div class="row">
                                <div class="col-xs-9 col-xs-offset-3">
                                    <form:select
                                            class="form-control" path="geatype3" id="geatype3">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">С ТПД</form:option>
                                        <form:option value="2">Без ТПД</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                    <spring:bind path="radikal">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Радикальность операции:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="radikal" path="radikal">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">R0</form:option>
                                    <form:option value="2">R1</form:option>
                                    <form:option value="3">R2</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="limfodissekcia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Лимфодиссекция:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="limfodissekcia" path="limfodissekcia">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">ГДС</form:option>
                                    <form:option value="2">ГДС+ОПА</form:option>
                                    <form:option value="3">ГДС+ОПА+ЧС</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="tnm">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="tnm">Стадия TNM:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="tnm" id="tnm">
                                    <form:option value="0">---------</form:option>
                                    <optgroup label="Стадия I">
                                        <form:option value="1">T1N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия II">
                                        <form:option value="2">T2aN0M0</form:option>
                                        <form:option value="3">T2bN0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIA">
                                        <form:option value="4">T3N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIB">
                                        <form:option value="5">T1N1M0</form:option>
                                        <form:option value="6">T2N1M0</form:option>
                                        <form:option value="7">T3N1M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVa">
                                        <form:option value="8">T4N0M0</form:option>
                                        <form:option value="9">T4N1M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVb">
                                        <form:option value="10">Любая T, N2M0</form:option>
                                        <form:option value="11">Любая T, любая N, M1</form:option>
                                    </optgroup>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="bypass">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Bypass:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="totIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Тотальная сосудистая изоляция (с пережатием НПВ):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="selIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Селективная сосудистая изоляция (с раздельным пережатием печеночных вен):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="selIz" id="selIz_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="selIz" id="selIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <c:if test="${showOslozhneniya}">
                        <div id="oslozhneniyaDiv">
                            <jsp:include page="oslozhneniya.jsp"/>

                            <spring:bind path="clavien">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>Классификация осложнений по Clavien-Dindo:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="clavien" path="clavien">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">I</form:option>
                                            <form:option value="2">II</form:option>
                                            <form:option value="3">IIIa</form:option>
                                            <form:option value="4">IIIb</form:option>
                                            <form:option value="5">IVa</form:option>
                                            <form:option value="6">IVb</form:option>
                                            <form:option value="7">V</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </c:if>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>
                </form:form>

                <script>
                    $(document).on("click", "#klatgdc_yes", function (e) {
                        document.getElementById("klatgdcminGroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#klatgdc_no", function (e) {
                        document.getElementById("klatgdcminGroup").setAttribute("style", "display:none;");
                        document.getElementById("klatgdcmin").value = "";
                    });
                    $(document).on("click", "#klatgem_yes", function (e) {
                        document.getElementById("klatgemGroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#klatgem_no", function (e) {
                        document.getElementById("klatgemGroup").setAttribute("style", "display:none;");
                        document.getElementById("klatgemotransfusia_ml").value = "";
                    });
                    $(document).on("change", "#klatsosudrezekcia", function (e) {
                        var selectedVal = $(this).find("option:selected").val();
                        switch (selectedVal) {
                            case "1":
                                document.getElementById("klatvorotrezekciaGroup").setAttribute("style", "display:block;");
                                break;
                            default:
                                document.getElementById("klatvorotrezekciaGroup").setAttribute("style", "display:none;");
                                document.getElementById("klatvorotrezekcia").value = 0;
                                break;
                        }
                    });
                    if (document.getElementById('klatgem_yes').checked) {
                        document.getElementById("klatgemGroup").setAttribute("style", "display:block;");
                    }
                    if (document.getElementById('klatgdc_yes').checked) {
                        document.getElementById("klatgdcminGroup").setAttribute("style", "display:block;");
                    }
                    var selectedVal = $("#klatsosudrezekcia").find("option:selected").val();
                    if (selectedVal == "1") {
                        document.getElementById("klatvorotrezekciaGroup").setAttribute("style", "display:block;");
                    }
                    $(document).ready(function () {
                        var expired = document.getElementById("expiredValue").value;
                        var isAdmin = document.getElementById("isAdminValue").value;
                        if (expired === "true" && isAdmin === "false") {
                            var c = document.getElementById("klatskinForm").elements;
                            for (var i = 0; i < c.length; i++) {
                                c[i].setAttribute("disabled", true);
                            }
                        }
                    });
                </script>
            </div>
        </div>
    </div>
</div>
</body>
</html>
