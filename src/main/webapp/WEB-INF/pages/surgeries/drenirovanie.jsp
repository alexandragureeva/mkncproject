<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Дренирование желчных протоков</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/datepicker.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.buttonRow):not(.section-header):before {
            content: "*";
            color: red;
        }

        #oslozhneniyaDiv {
            border-left: 6px solid dodgerblue;
            background-color: lightblue;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row buttonRow">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <c:choose>
                <c:when test="${!drenirovanie.showComplications}">
                    <a href="${(drenirovanie.drenId != null)? "../" : ""}../patients/update/${drenirovanie.id}">
                        <ins>На страницу пациента</ins>
                    </a>
                </c:when>
                <c:otherwise>
                    <a href="../../surgeryComplications/${drenirovanie.id}">
                        <ins>На страницу операций</ins>
                    </a>
                </c:otherwise>
            </c:choose>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные о дренировании печени</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveDrenirovanie?${_csrf.parameterName}=${_csrf.token}" var="drenirovanieUrl"/>
                <form:form action="${drenirovanieUrl}" modelAttribute="drenirovanie" method="post"
                           id="drenirovanieForm">

                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>

                    <spring:bind path="drenId">
                        <form:hidden path="drenId"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="date">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="date">Дата операции:</label>
                            </div>
                            <div class="col-xs-9 ${status.error ? 'has-error' : ''}">
                                <div class="input-group date">
                                    <form:input type='text' class="form-control" aria-describedby="addon"
                                                id='date' path="date"/>
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <form:errors path="date" class="control-label"/>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="out_drenages">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="out_drenages">Количество наружних дренажей:</label>
                            </div>
                            <div class="col-xs-9 ${status.error ? 'has-error' : ''}">
                                <form:input
                                        id="out_drenages" path="out_drenages" class="form-control" type="number"/>
                            </div>
                            <form:errors path="out_drenages" class="control-label"/>
                        </div>
                    </spring:bind>

                    <spring:bind path="inout_drenages">
                        <div class="row">
                            <div class="col-xs-3 ${status.error ? 'has-error' : ''}">
                                <label for="inout_drenages">Количество наружно-внутренних дренажей:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="inout_drenages" path="inout_drenages" class="form-control" type="number"/>
                            </div>
                            <form:errors path="inout_drenages" class="control-label"/>
                        </div>
                    </spring:bind>

                    <spring:bind path="block">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="block">Уровень блока:</label>
                            </div>
                            <div class="col-xs-9 ${status.error ? 'has-error' : ''}">
                                <form:select
                                        class="form-control" id="block" path="block">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Общий печеночный проток</form:option>
                                    <form:option value="2">Конфлюэнс</form:option>
                                    <form:option value="3">Холедох</form:option>
                                    <form:option value="4">Разобщение долевых протоков</form:option>
                                    <form:option value="5">Разобщение секторальных протоков</form:option>
                                </form:select>
                                <form:errors path="block" class="control-label"/>
                            </div>
                        </div>
                    </spring:bind>


                    <spring:bind path="longevity">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="longevity">Длительность желтухи:</label>
                            </div>
                            <div class="col-xs-9 ${status.error ? 'has-error' : ''}">
                                <form:input
                                        id="longevity" path="longevity" class="form-control" type="number"/>
                            </div>
                            <form:errors path="longevity" class="control-label"/>
                        </div>
                    </spring:bind>
                    <spring:bind path="previous_cholangit">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Рециссивный холангит:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="previous_cholangit"
                                                                              id="previous_cholangit_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="previous_cholangit"
                                                                              id="previous_cholangit_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="absces_cholangit">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Абсцедирующий холангит:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="absces_cholangit"
                                                                              id="absces_cholangit_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="absces_cholangit"
                                                                              id="absces_cholangit_yes" name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="dolya">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="dolya">Дренированная доля:</label>
                            </div>
                            <div class="col-xs-9 ${status.error ? 'has-error' : ''}">
                                <form:select
                                        class="form-control" id="dolya" path="dolya">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Левая</form:option>
                                    <form:option value="2">Правая</form:option>
                                    <form:option value="3">Обе</form:option>
                                </form:select>
                                <form:errors path="dolya" class="control-label"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="svoracivanie">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Сворачивание дренажа в кольцо:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="svoracivanie" id="svoracivanie_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="svoracivanie" id="svoracivanie_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="segment67">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Дренирование 6-7 сегментов:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="segment67" id="segment67_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="segment67" id="segment67_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="segment58">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Дренирование 5-8 сегментов:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="segment58" id="segment58_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="segment58" id="segment58_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="suprapapil">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Супрапапилярный дренаж:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="suprapapil" id="suprapapil_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="suprapapil" id="suprapapil_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="zh_posev">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Инфицированный посев желчи:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="zh_posev" id="zh_posev_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="zh_posev" id="zh_posev_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="form-group" id="zhelzhGroup" style="display: none;">
                        <spring:bind path="klebsiela">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label>Клебсиелла пневмонии:</label>
                                </div>
                                <div class="col-xs-9">
                                    <label class="radio-inline"><form:radiobutton path="klebsiela" id="klebsiela_no"
                                                                                  name="optradio"
                                                                                  value="0"/>Нет</label>
                                    <label class="radio-inline"><form:radiobutton path="klebsiela" id="klebsiela_yes"
                                                                                  name="optradio"
                                                                                  value="1"/>Да</label>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="sinegnoin_palochka">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label>Синегнойная палочка:</label>
                                </div>
                                <div class="col-xs-9">
                                    <label class="radio-inline"><form:radiobutton path="sinegnoin_palochka"
                                                                                  id="sinegnoin_palochka_no"
                                                                                  name="optradio"
                                                                                  value="0"/>Нет</label>
                                    <label class="radio-inline"><form:radiobutton path="sinegnoin_palochka"
                                                                                  id="sinegnoin_palochka_yes"
                                                                                  name="optradio"
                                                                                  value="1"/>Да</label>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                    <spring:bind path="kr_posev">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Инфицированный посев крови:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="kr_posev" id="kr_posev_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="kr_posev" id="kr_posev_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="showComplications">
                        <form:hidden path="showComplications"/>
                    </spring:bind>
                    <c:if test="${drenirovanie.showComplications}">
                        <div id="oslozhneniyaDiv">
                        <spring:bind path="osl">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label>Осложнения:</label>
                                </div>
                                <div class="col-xs-9">
                                    <label class="radio-inline"><form:radiobutton path="osl" id="osl_no" name="optradio"
                                                                                  value="0"/>Нет</label>
                                    <label class="radio-inline"><form:radiobutton path="osl" id="osl_yes"
                                                                                  name="optradio"
                                                                                  value="1"/>Да</label>
                                </div>
                            </div>
                        </spring:bind>
                        <div id="oslozhneniya" style="display: none;">
                            <ul class="list-unstyled col-sm-offset-3">
                                <li>
                                    <div class="row buttonRow">
                                        <div class="col-xs-12">
                                            <spring:bind path="drenageMigration">
                                                <form:checkbox id="drenageMigration" path="drenageMigration" value="1"/><b>Миграция
                                                дренажа</b>
                                            </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row buttonRow">
                                        <div class="col-xs-12">
                                            <spring:bind path="biloma">
                                                <form:checkbox id="biloma" path="biloma" value="1"/><b>Билома</b>
                                            </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row buttonRow">
                                        <div class="col-xs-12">
                                            <spring:bind path="cholangit">
                                                <form:checkbox id="cholangit" path="cholangit"
                                                               value="1"/><b>Холангит</b>
                                            </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row buttonRow">
                                        <div class="col-xs-12">
                                            <spring:bind path="krovotechenie">
                                                <form:checkbox id="krovotechenie" path="krovotechenie" value="1"/><b>Кровотечение</b>
                                            </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row buttonRow">
                                        <div class="col-xs-12">
                                            <spring:bind path="sochranivZheltucha">
                                                <form:checkbox id="sochranivZheltucha" path="sochranivZheltucha"
                                                               value="1"/><b>Сохранившаяся желтуха</b>
                                            </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row buttonRow">
                                        <div class="col-xs-12">
                                            <spring:bind path="pankreotit">
                                                <form:checkbox id="pankreotit" path="pankreotit" value="1"/><b>Панкреатит</b>
                                            </spring:bind></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row buttonRow">
                                        <div class="col-xs-12">
                                            <spring:bind path="gemobilia">
                                                <form:checkbox id="gemobilia" path="gemobilia"
                                                               value="1"/><b>Гемобилия</b>
                                            </spring:bind></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                            </div>
                    </c:if>

                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Сохранить</button>
                    </div>
                </form:form>

                <script>
                    $(document).on("click", "#zh_posev_yes", function (e) {
                        document.getElementById("zhelzhGroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#zh_posev_no", function (e) {
                        document.getElementById("zhelzhGroup").setAttribute("style", "display:none;");
                        document.getElementById("klebsiela_no").checked = true;
                        document.getElementById("sinegnoin_palochka_no").checked = true;
                    });
                    $(document).on("click", "#osl_yes", function (e) {
                        document.getElementById("oslozhneniya").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#osl_no", function (e) {
                        document.getElementById("oslozhneniya").setAttribute("style", "display:none;");
                        $('#oslozhneniya').find('input[type=checkbox]:checked').removeAttr('checked');
                    });
                </script>
                <script>
                    $('#date').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true,
                        todayBtn: 'linked',
                        pickerPosition: "bottom-left"
                    });
                    $(document).ready(function () {
                        var expired = document.getElementById("expiredValue").value;
                        var isAdmin = document.getElementById("isAdminValue").value;
                        if (expired === "true" && isAdmin === "false") {
                            var c = document.getElementById("drenirovanieForm").elements;
                            for (var i = 0; i < c.length; i++) {
                                c[i].setAttribute("disabled", true);
                            }
                        }
                        if (document.getElementById('zh_posev_yes').checked) {
                            document.getElementById("zhelzhGroup").setAttribute("style", "display:block;");
                        }
                        if (document.getElementById('osl_yes').checked) {
                            document.getElementById("oslozhneniya").setAttribute("style", "display:block;");
                        }
                        if ($("#showComplications").val() === "true") {
                            $('html, body').animate({
                                scrollTop: $('#osl_no').offset().top
                            });
                        }
                    });
                </script>
            </div>
        </div>
    </div>
</div>
</body>
</html>