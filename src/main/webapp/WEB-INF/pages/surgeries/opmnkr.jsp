<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Операция при МНКР</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .row:not(.buttonRow):not(.unnecessaryRow):not(.section-header):before {
            content: "*";
            color: red;
        }

        #oslozhneniyaDiv {
            border-left: 6px solid dodgerblue;
            background-color: lightblue;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row buttonRow">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${mnkr.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-body">
                <c:url value="/saveOpMnkr?${_csrf.parameterName}=${_csrf.token}" var="mnkrUrl"/>
                <form:form action="${mnkrUrl}" modelAttribute="mnkr" method="post"
                           id="mnkrForm">
                    <c:if test="${twoStepsTreatment}">
                        <jsp:include page="firstStage.jsp"/>
                    </c:if>
                    <div class="modal-header">
                        <h4><i class="fa fa-bar-chart"></i>Данные об операции при диагнозе "МНКР"</h4>
                    </div>
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>

                    <spring:bind path="date">
                        <form:hidden path="date"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">


                    <spring:bind path="dlitop">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="dlitop">Длительность операции (мин.):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="dlitop" path="dlitop" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <jsp:include page="subSurgery.jsp"/>

                    <spring:bind path="kratRez">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="kratRez">Кратность резекции печени:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="kratRez" path="kratRez">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Первичная</form:option>
                                    <form:option value="2">Вторая</form:option>
                                    <form:option value="3">Третья</form:option>
                                    <form:option value="4">Четвертая</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="mnkrobrezekcii">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="mnkrobrezekcii">Объем резекции:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="mnkrobrezekcii" path="mnkrobrezekcii">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Обширная резекция ЛГГЭ</form:option>
                                    <form:option value="2">Обширная резекция ПГГЭ</form:option>
                                    <form:option value="3">Cегментарная резекция</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="form-group" id="mnkrsegmRezGroup" style="display: none">
                        <spring:bind path="mnkrsegrez">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="mnkrsegrez">Тип сегментарной резекции:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="mnkrsegrez" path="mnkrsegrez">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Анатомическая</form:option>
                                        <form:option value="2">Неанатомическая</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="mnkrchisloUdSeg">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="mnkrchisloUdSeg">Число удаленных сегментов:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="mnkrchisloUdSeg" path="mnkrchisloUdSeg">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">1</form:option>
                                        <form:option value="2">2</form:option>
                                        <form:option value="2">3</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                    <spring:bind path="varRezekcii">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="varRezekcii">Вариант резекции:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="varRezekcii" path="varRezekcii">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Открытая</form:option>
                                    <form:option value="2">Лапароскопическая</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="oplech">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="oplech">Наличие внепеченочных метастазов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="oplech" path="oplech">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">В легких</form:option>
                                    <form:option value="3">В лимфатических узлах брюшной полости</form:option>
                                    <form:option value="4">По брюшине</form:option>
                                    <form:option value="5">В других органах брюшной полости</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="stadTnm">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="stadTnm">Стадии первичной опухоли по ТNМ :</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="stadTnm" path="stadTnm" class="form-control" type="text"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="gdc">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Пережатие ГДС:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="gdc" id="mnkrgdc_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="gdc" id="mnkrgdc_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="mnkrgdcmin">
                        <div class="row" id="mnkrmnkrgdcminGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="mnkrgdcmin">Пережатие ГДС (мин):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="mnkrgdcmin" path="mnkrgdcmin" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="bypass">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Bypass:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="totIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Тотальная сосудистая изоляция (с пережатием НПВ):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="selSosIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Селективная сосудистая изоляция (с раздельным пережатием печеночных вен):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="selSosIz" id="selSosIz_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="selSosIz" id="selSosIz_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="chimiot">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="chimiot">Проведение химиотерапии:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="chimiot" path="chimiot">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">нет</form:option>
                                    <form:option value="2">Цитостатики</form:option>
                                    <form:option value="3">Цитостатики + таргетная терапия</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="limfodissekcia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Выполнение лимфодиссекции :</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="limfodissekcia"
                                                                              id="limfodissekcia_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="limfodissekcia"
                                                                              id="limfodissekcia_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="sochIntraRHA">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Сочетание с интраоперационным РЧА:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="sochIntraRHA" id="sochIntraRHA_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="sochIntraRHA" id="sochIntraRHA_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="nalRHA">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="nalRHA">Наличие РЧА до резекции печени :</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="nalRHA" path="nalRHA">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">нет</form:option>
                                    <form:option value="2">1</form:option>
                                    <form:option value="3">2</form:option>
                                    <form:option value="4">3</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="simultOp">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="simultOp">Симультанная операция:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="simultOp" path="simultOp">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Резекция толстой кишки</form:option>
                                    <form:option value="2">Резекция прямой кишки</form:option>
                                    <form:option value="3">Резекция диафрагмы</form:option>
                                    <form:option value="4">Резекция других органов(тонкой кишки и др)</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="rekOpTKish">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Реконструктивная операция на толстой кишке:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="rekOpTKish" id="rekOpTKish_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="rekOpTKish" id="rekOpTKish_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <c:if test="${showOslozhneniya}">
                        <div id="oslozhneniyaDiv">
                            <jsp:include page="oslozhneniya.jsp"/>

                            <spring:bind path="clavien">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>Классификация осложнений по Clavien-Dindo:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="clavien" path="clavien">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">I</form:option>
                                            <form:option value="2">II</form:option>
                                            <form:option value="3">IIIa</form:option>
                                            <form:option value="4">IIIb</form:option>
                                            <form:option value="5">IVa</form:option>
                                            <form:option value="6">IVb</form:option>
                                            <form:option value="7">V</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </c:if>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>
                </form:form>
                <script>
                    $(document).on("change", "#mnkrobrezekcii", function (e) {
                        var selectedVal = $(this).find("option:selected").val();
                        switch (selectedVal) {
                            case "3":
                                document.getElementById("mnkrsegmRezGroup").setAttribute("style", "display:block;");
                                break;
                            default:
                                document.getElementById("mnkrsegmRezGroup").setAttribute("style", "display:none;");
                                document.getElementById("mnkrsegrez").value = 0;
                                document.getElementById("mnkrchisloUdSeg").value = 0;
                                break;
                        }
                    });
                    $(document).on("click", "#mnkrgdc_yes", function (e) {
                        document.getElementById("mnkrmnkrgdcminGroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#mnkrgdc_no", function (e) {
                        document.getElementById("mnkrmnkrgdcminGroup").setAttribute("style", "display:none;");
                        document.getElementById("mnkrgdcmin").value = "";
                    });
                    if (document.getElementById('mnkrgdc_yes').checked) {
                        document.getElementById("mnkrmnkrgdcminGroup").setAttribute("style", "display:block;");
                    }
                    var selectedVal = $("#mnkrobrezekcii").find("option:selected").val();
                    if (selectedVal == "3") {
                        document.getElementById("mnkrsegmRezGroup").setAttribute("style", "display:block;");
                    }
                    $(document).ready(function () {
                        var expired = document.getElementById("expiredValue").value;
                        var isAdmin = document.getElementById("isAdminValue").value;
                        if (expired === "true" && isAdmin === "false") {
                            var c = document.getElementById("mnkrForm").elements;
                            for (var i = 0; i < c.length; i++) {
                                c[i].setAttribute("disabled", true);
                            }
                        }
                    });
                </script>
            </div>
        </div>
    </div>
</div>
</body>
</html>