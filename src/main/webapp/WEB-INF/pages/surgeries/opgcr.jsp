<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Операция при ГЦР</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .row:not(.buttonRow):not(.section-header):not(.unnecessaryRow):before {
            content: "*";
            color: red;
        }

        hr.style4 {
            border-top: 1px dotted #8c8b8b;
        }

        #oslozhneniyaDiv {
            border-left: 6px solid dodgerblue;
            background-color: lightblue;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${gcr.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-body">
                <c:url value="/saveOpGcr?${_csrf.parameterName}=${_csrf.token}" var="gcrUrl"/>
                <form:form action="${gcrUrl}" modelAttribute="gcr" method="post"
                           id="gcrForm">
                    <c:if test="${twoStepsTreatment}">
                        <jsp:include page="firstStage.jsp"/>
                    </c:if>
                    <div class="modal-header">
                        <h4><i class="fa fa-bar-chart"></i>Данные об операции при диагнозе "ГЦР"</h4>
                    </div>
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>

                    <spring:bind path="date">
                        <form:hidden path="date"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="dlitop">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="dlitop">Длительность операции (мин.):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="dlitop" path="dlitop" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <jsp:include page="subSurgery.jsp"/>
                    <spring:bind path="gcroplech">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="gcroplech">Вид оперативного лечения:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="gcroplech" path="gcroplech">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Резекция печени</form:option>
                                    <form:option value="2">Чрескожая РЧА (чРЧА)</form:option>
                                    <form:option value="3">Алкоголизация</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="form-group" id="gcrrezekciaGroup" style="display: none;">
                        <spring:bind path="gcrobrezekcii">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="gcrobrezekcii">Объем резекции:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="gcrobrezekcii" path="gcrobrezekcii">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Обширная резекция ЛГГЭ</form:option>
                                        <form:option value="2">Обширная резекция ПГГЭ</form:option>
                                        <form:option value="3">Cегментарная резекция</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <div class="form-group" id="gcrsegmRezGroup" style="display: none">
                            <spring:bind path="gcrsegrez">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="gcrsegrez">Тип сегментарной резекции:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="gcrsegrez" path="gcrsegrez">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">Анатомическая</form:option>
                                            <form:option value="2">Неанатомическая</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                            <spring:bind path="gcrchisloUdSeg">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="gcrchisloUdSeg">Число удаленных сегментов:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="gcrchisloUdSeg" path="gcrchisloUdSeg">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">1</form:option>
                                            <form:option value="2">2</form:option>
                                            <form:option value="3">3</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="form-group" id="gcrrhaGroup" style="display:none;">
                        <spring:bind path="gcrchisloOch">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="gcrchisloOch">Число очагов:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="gcrchisloOch" path="gcrchisloOch">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">1</form:option>
                                        <form:option value="2">2</form:option>
                                        <form:option value="3">3</form:option>
                                        <form:option value="4">>3</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="gcrrazOch">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="gcrrazOch">Размер очагов:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:input
                                            id="gcrrazOch" path="gcrrazOch" class="form-control" type="number"/>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="gcrblizSosud">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label>Близость к сосудам:</label>
                                </div>
                                <div class="col-xs-9">
                                    <label class="radio-inline"><form:radiobutton path="gcrblizSosud"
                                                                                  id="gcrblizSosud_no"
                                                                                  name="optradio" value="0"/>Нет</label>
                                    <label class="radio-inline"><form:radiobutton path="gcrblizSosud"
                                                                                  id="gcrblizSosud_yes"
                                                                                  name="optradio" value="1"/>Да</label>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="gcrpolnRHA">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="gcrpolnRHA">Полнота РЧА:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="gcrpolnRHA" path="gcrpolnRHA">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Полная</form:option>
                                        <form:option value="2">Неполная</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                    <div class="form-group" id="gcralkogGroup" style="display: none;">
                        <spring:bind path="gcralkChisloOch">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="gcralkChisloOch">Число очагов:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="gcralkChisloOch" path="gcralkChisloOch">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">1</form:option>
                                        <form:option value="2">2</form:option>
                                        <form:option value="3">3</form:option>
                                        <form:option value="4">>3</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="gcralkRazOch">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="gcralkRazOch">Размер очагов:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:input
                                            id="gcralkRazOch" path="gcralkRazOch" class="form-control" type="number"/>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                    <spring:bind path="krovopoteria">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="krovopoteria">Объем кровопотери (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="krovopoteria" path="krovopoteria">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1"><500</form:option>
                                    <form:option value="2">500-1000</form:option>
                                    <form:option value="3">>1000</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="gemotransfusia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Гемотрансфузия:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="gemotransfusia" id="gcrgem_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="gemotransfusia" id="gcrgem_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="gcrgemotransfusia_ml">
                        <div class="row" id="gcrgemGroup" style="display: none">
                            <div class="col-xs-3">
                                <label for="gcrgemotransfusia_ml">Объем гемотрансфузии (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="gcrgemotransfusia_ml" path="gcrgemotransfusia_ml" class="form-control"
                                        type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="plazma">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="plazma">Объем плазмы трансфузии (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="plazma" path="plazma" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="gdc">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Пережатие ГДС:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="gdc" id="gcrgdc_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="gdc" id="gcrgdc_yes" name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="gcrgdcmin">
                        <div class="row" id="gcrgdcMinGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="gcrgdcmin">Пережатие ГДС (мин):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="gcrgdcmin" path="gcrgdcmin" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="perPolVeni">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Пережатие нижней полой вены:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="perPolVeni" id="gcrperPolVeni_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="perPolVeni" id="gcrperPolVeni_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="gcrperPolVeniMin">
                        <div class="row" id="gcrperPolVeniMinGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="gcrgdcmin">Пережатие нижней полой вены (мин):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="gcrperPolVeniMin" path="gcrperPolVeniMin" class="form-control"
                                        type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="selIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Селективная сосудистая изоляция:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="selIz" id="gcrselIz_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="selIz" id="gcrselIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="gcrselIzMin">
                        <div class="row" id="gcrselIzMinGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="gcrselIzMin">Селективная сосудистая изоляция (мин.):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="gcrselIzMin" path="gcrselIzMin" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="intrRHA">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Интраоперационная РЧА:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="intrRHA" id="intrRHA_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="intrRHA" id="intrRHA_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="tnm">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="tnm">Стадия TNM:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="tnm" id="tnm">
                                    <form:option value="0">---------</form:option>
                                    <optgroup label="Стадия I">
                                        <form:option value="1">T1N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия II">
                                        <form:option value="2">T2aN0M0</form:option>
                                        <form:option value="3">T2bN0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIA">
                                        <form:option value="4">T3N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIB">
                                        <form:option value="5">T1N1M0</form:option>
                                        <form:option value="6">T2N1M0</form:option>
                                        <form:option value="7">T3N1M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVa">
                                        <form:option value="8">T4N0M0</form:option>
                                        <form:option value="9">T4N1M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVb">
                                        <form:option value="10">Любая T, N2M0</form:option>
                                        <form:option value="11">Любая T, любая N, M1</form:option>
                                    </optgroup>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="bypass">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Bypass:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="totIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Тотальная сосудистая изоляция (с пережатием НПВ):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="selSosIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Селективная сосудистая изоляция (с раздельным пережатием печеночных вен):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="selSosIz" id="selSosIz_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="selSosIz" id="selSosIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="tache">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>ТАХЭ после операции:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="tache" id="tache_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="tache" id="tache_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="showComplications">
                        <form:hidden path="showComplications"/>
                    </spring:bind>

                    <c:if test="${showOslozhneniya}">
                        <div id="oslozhneniyaDiv">
                            <spring:bind path="osl">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>Осложнения:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <label class="radio-inline"><form:radiobutton path="osl" id="osl_no"
                                                                                      name="optradio"
                                                                                      value="0"/>Нет</label>
                                        <label class="radio-inline"><form:radiobutton path="osl" id="osl_yes"
                                                                                      name="optradio"
                                                                                      value="1"/>Да</label>
                                    </div>
                                </div>
                            </spring:bind>
                            <div id="oslozhneniya">
                                <ul class="list-unstyled col-sm-offset-3">
                                    <div class="row buttonRow" id="gcroslRezGroup" style="display: none;">
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="vnutrKrovot">
                                                        <form:checkbox id="vnutrKrovot" path="vnutrKrovot"
                                                                       value="1"/><b>Внутрибрюшное
                                                        кровотечение</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="zhkk">
                                                        <form:checkbox id="zhkk" path="zhkk" value="1"/><b>ЖКК</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="nagnoenie">
                                                        <form:checkbox id="nagnoenie" path="nagnoenie" value="1"/><b>Нагноение
                                                        раны</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="cholangit">
                                                        <form:checkbox id="cholangit" path="cholangit"
                                                                       value="1"/><b>Холангит</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="cholAbscedir">
                                                        <form:checkbox id="cholAbscedir" path="cholAbscedir" value="1"/><b>Холангиогенное
                                                        абсцедирование</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="ascit">
                                                        <form:checkbox id="ascit" path="ascit" value="1"/><b>Асцит</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="eventracia">
                                                        <form:checkbox id="eventracia" path="eventracia" value="1"/><b>Эвентрация</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="kishNeprohod">
                                                        <form:checkbox id="kishNeprohod" path="kishNeprohod" value="1"/><b>Кишечная
                                                        непроходимость</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="pankreatSvich">
                                                        <form:checkbox id="pankreatSvich" path="pankreatSvich"
                                                                       value="1"/><b>Панкреатический
                                                        свищ</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="ostriiSvich">
                                                        <form:checkbox id="ostriiSvich" path="ostriiSvich"
                                                                       value="1"/><b>Острый
                                                        панкреатит</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="sepsis">
                                                        <form:checkbox id="sepsis" path="sepsis"
                                                                       value="1"/><b>Сепсис</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="tyazhSepsis">
                                                        <form:checkbox id="tyazhSepsis" path="tyazhSepsis"
                                                                       value="1"/><b>Тяжелый
                                                        сепсис</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="septShok">
                                                        <form:checkbox id="septShok" path="septShok" value="1"/><b>Септический
                                                        шок</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="zhelchSvish">
                                                        <form:checkbox id="zhelchSvish" path="zhelchSvish"
                                                                       value="1"/><b>Желчный
                                                        свищ</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="kishSvish">
                                                        <form:checkbox id="kishSvish" path="kishSvish" value="1"/><b>Кишечный
                                                        свищ</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="trombVorotVeni">
                                                        <form:checkbox id="trombVorotVeni" path="trombVorotVeni"
                                                                       value="1"/><b>Тромбоз
                                                        воротной вены</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="trombozPechArter">
                                                        <form:checkbox id="trombozPechArter" path="trombozPechArter"
                                                                       value="1"/><b>Тромбоз
                                                        печеночной
                                                        артерии</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="odnostorPnevm">
                                                        <form:checkbox id="odnostorPnevm" path="odnostorPnevm"
                                                                       value="1"/><b>Односторонняя
                                                        пневмония</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="dvustorPnevm">
                                                        <form:checkbox id="dvustorPnevm" path="dvustorPnevm" value="1"/><b>Двусторонняя
                                                        пневмония</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="gidrotorax">
                                                        <form:checkbox id="gidrotorax" path="gidrotorax" value="1"/><b>Гидроторакс</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="pochNedostat">
                                                        <form:checkbox id="pochNedostat" path="pochNedostat" value="1"/><b>Почечная
                                                        недостаточность</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="serdSosNedostat">
                                                        <form:checkbox id="serdSosNedostat" path="serdSosNedostat"
                                                                       value="1"/><b>Сердечно-сосудистая
                                                        недостаточность</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="poliorganNedostat">
                                                        <form:checkbox id="poliorganNedostat" path="poliorganNedostat"
                                                                       value="1"/><b>Полиорганная
                                                        недостаточность</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="vrv">
                                                        <form:checkbox id="vrv" path="vrv" value="1"/><b>Из ВРВ пищевода
                                                        и
                                                        желудка</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="postRezNedostA">
                                                        <form:checkbox id="postRezNedostA" path="postRezNedostA"
                                                                       value="1"/><b>Пострезекционная
                                                        недостаточность A</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="postRezNedostB">
                                                        <form:checkbox id="postRezNedostB" path="postRezNedostB"
                                                                       value="1"/><b>Пострезекционная
                                                        недостаточность B</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="postRezNedostC">
                                                        <form:checkbox id="postRezNedostC" path="postRezNedostC"
                                                                       value="1"/><b>Пострезекционная
                                                        недостаточность C</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="row buttonRow" id="gcroslRHAGroup" style="display: none;">
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="postEmbSyndrom">
                                                        <form:checkbox id="postEmbSyndrom" path="postEmbSyndrom"
                                                                       value="1"/><b>Постабляционный
                                                        синдром</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="biloma">
                                                        <form:checkbox id="biloma" path="biloma"
                                                                       value="1"/><b>Билома</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="povrPolOrgan">
                                                        <form:checkbox id="povrPolOrgan" path="povrPolOrgan" value="1"/><b>Повреждения
                                                        полых органов</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                    </div>
                                    <div class="row buttonRow" id="pechNedostatochnost" style="display: none;">
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="pechNedostatA">
                                                        <form:checkbox id="pechNedostatA" path="pechNedostatA"
                                                                       value="1"/><b>Печеночная
                                                        недостаточность
                                                        A</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="pechNedostatB">
                                                        <form:checkbox id="pechNedostatB" path="pechNedostatB"
                                                                       value="1"/><b>Печеночная
                                                        недостаточность
                                                        B</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="pechNedostatC">
                                                        <form:checkbox id="pechNedostatC" path="pechNedostatC"
                                                                       value="1"/><b>Печеночная
                                                        недостаточность
                                                        C</b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                    </div>

                                    <div class="row buttonRow" id="krovotechenie" style="display: none;">
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="krovVBrushPolost1">
                                                        <form:checkbox id="krovVBrushPolost1" path="krovVBrushPolost1"
                                                                       value="1"/><b>
                                                        Кровотечение в бр.полость(остановлено консервативно)
                                                    </b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row buttonRow">
                                                <div class="col-xs-12">
                                                    <spring:bind path="krovVBrushPolost2">
                                                        <form:checkbox id="krovVBrushPolost2" path="krovVBrushPolost2"
                                                                       value="1"/><b>
                                                        Кровотечение в бр.полость(потребовавшее
                                                        лапроскопии(лапоротомии))
                                                    </b>
                                                    </spring:bind></div>
                                            </div>
                                        </li>
                                    </div>
                                </ul>
                            </div>
                            <spring:bind path="clavien">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>Классификация осложнений по Clavien-Dindo:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="clavien" path="clavien">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">I</form:option>
                                            <form:option value="2">II</form:option>
                                            <form:option value="3">IIIa</form:option>
                                            <form:option value="4">IIIb</form:option>
                                            <form:option value="5">IVa</form:option>
                                            <form:option value="6">IVb</form:option>
                                            <form:option value="7">V</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </c:if>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>
                    <script>
                        function uncheckOsl(el) {
                            el.find('input[type=checkbox]:checked').removeAttr('checked');
                        }

                        $(document).on("click", "#osl_yes", function (e) {
                            document.getElementById("oslozhneniya").setAttribute("style", "display:block;");
                            var selectedVal = $("#gcroplech").find("option:selected").val();
                            if (selectedVal == "1") {
                                document.getElementById("gcrrezekciaGroup").setAttribute("style", "display:block;");
                                document.getElementById("gcroslRezGroup").setAttribute("style", "display:block;");
                                document.getElementById("pechNedostatochnost").setAttribute("style", "display:block;");
                            }
                            if (selectedVal == "2") {
                                document.getElementById("gcrrhaGroup").setAttribute("style", "display:block;");
                                document.getElementById("gcroslRHAGroup").setAttribute("style", "display:block;");
                                document.getElementById("krovotechenie").setAttribute("style", "display:block;");
                            }
                            if (selectedVal == "3") {
                                document.getElementById("gcralkogGroup").setAttribute("style", "display:block;");
                                document.getElementById("pechNedostatochnost").setAttribute("style", "display:block;");
                                document.getElementById("krovotechenie").setAttribute("style", "display:block;");
                            }
                        });
                        $(document).on("click", "#osl_no", function (e) {
                            document.getElementById("oslozhneniya").setAttribute("style", "display:none;");
                            $('#oslozhneniya').find('input[type=checkbox]:checked').removeAttr('checked');
                            uncheckOsl($("#oslozhneniya"));
                        });
                        $(document).on("change", "#gcroplech", function (e) {
                            var selectedVal = $(this).find("option:selected").val();
                            switch (selectedVal) {
                                case "0":
                                    document.getElementById("oslozhneniya").setAttribute("style", "display:none;");
                                    uncheckOsl($("#oslozhneniya"));
                                    document.getElementById("gcralkChisloOch").value = 0;
                                    document.getElementById("gcralkRazOch").value = 0;
                                    document.getElementById("gcrchisloOch").value = 0;
                                    document.getElementById("gcrrazOch").value = 0;
                                    $("#gcrblizSosud_no").prop("checked", true);
                                    document.getElementById("gcrpolnRHA").value = 0;
                                    document.getElementById("gcrobrezekcii").value = 0;
                                    document.getElementById("gcrsegrez").value = 0;
                                    document.getElementById("gcrchisloUdSeg").value = 0;

                                    break;
                                case "1":
                                    if (document.getElementById('osl_yes').checked) {
                                        document.getElementById("oslozhneniya").setAttribute("style", "display:block;");
                                        document.getElementById("gcrrezekciaGroup").setAttribute("style", "display:block;");
                                        document.getElementById("gcroslRezGroup").setAttribute("style", "display:block;");
                                        document.getElementById("gcrrhaGroup").setAttribute("style", "display:none;");
                                        document.getElementById("gcralkogGroup").setAttribute("style", "display:none;");
                                        document.getElementById("gcroslRHAGroup").setAttribute("style", "display:none;");
                                        document.getElementById("pechNedostatochnost").setAttribute("style", "display:block;");
                                        document.getElementById("krovotechenie").setAttribute("style", "display:none;");
                                    }
                                    uncheckOsl($("#oslozhneniya"));
                                    document.getElementById("gcralkChisloOch").value = 0;
                                    document.getElementById("gcralkRazOch").value = 0;
                                    document.getElementById("gcrchisloOch").value = 0;
                                    document.getElementById("gcrrazOch").value = 0;
                                    $("#gcrblizSosud_no").prop("checked", true);
                                    document.getElementById("gcrpolnRHA").value = 0;

                                    break;
                                case "2":

                                    if (document.getElementById('osl_yes').checked) {
                                        document.getElementById("oslozhneniya").setAttribute("style", "display:block;");
                                        document.getElementById("gcrrhaGroup").setAttribute("style", "display:block;");
                                        document.getElementById("gcroslRHAGroup").setAttribute("style", "display:block;");
                                        document.getElementById("krovotechenie").setAttribute("style", "display:block;");
                                        document.getElementById("gcrrezekciaGroup").setAttribute("style", "display:none;");
                                        document.getElementById("gcralkogGroup").setAttribute("style", "display:none;");
                                        document.getElementById("gcroslRezGroup").setAttribute("style", "display:none;");
                                        document.getElementById("pechNedostatochnost").setAttribute("style", "display:none;");
                                    }
                                    uncheckOsl($("#oslozhneniya"));
                                    document.getElementById("gcralkChisloOch").value = 0;
                                    document.getElementById("gcralkRazOch").value = 0;
                                    document.getElementById("gcrobrezekcii").value = 0;
                                    document.getElementById("gcrsegrez").value = 0;
                                    document.getElementById("gcrchisloUdSeg").value = 0;
                                    break;
                                case "3":

                                    if (document.getElementById('osl_yes').checked) {
                                        document.getElementById("oslozhneniya").setAttribute("style", "display:block;");
                                        document.getElementById("gcralkogGroup").setAttribute("style", "display:block;");
                                        document.getElementById("pechNedostatochnost").setAttribute("style", "display:block;");
                                        document.getElementById("krovotechenie").setAttribute("style", "display:block;");
                                        document.getElementById("gcroslRHAGroup").setAttribute("style", "display:none;");
                                        document.getElementById("gcrrezekciaGroup").setAttribute("style", "display:none;");
                                        document.getElementById("gcrrhaGroup").setAttribute("style", "display:none;");
                                        document.getElementById("gcroslRezGroup").setAttribute("style", "display:none;");
                                    }
                                    document.getElementById("gcrchisloOch").value = 0;
                                    document.getElementById("gcrrazOch").value = 0;
                                    uncheckOsl($("#oslozhneniya"));
                                    $("#gcrblizSosud_no").prop("checked", true);
                                    document.getElementById("gcrpolnRHA").value = 0;
                                    document.getElementById("gcrobrezekcii").value = 0;
                                    document.getElementById("gcrsegrez").value = 0;
                                    document.getElementById("gcrchisloUdSeg").value = 0;
                                    break;
                            }
                        });
                        $(document).on("click", "#gcrgem_yes", function (e) {
                            document.getElementById("gcrgemGroup").setAttribute("style", "display:block;");
                        });
                        $(document).on("click", "#gcrgem_no", function (e) {
                            document.getElementById("gcrgemGroup").setAttribute("style", "display:none;");
                            document.getElementById("gcrgemotransfusia_ml").value = "";
                        });
                        $(document).on("click", "#gcrgdc_yes", function (e) {
                            document.getElementById("gcrgdcMinGroup").setAttribute("style", "display:block;");
                        });
                        $(document).on("click", "#gcrgdc_no", function (e) {
                            document.getElementById("gcrgdcMinGroup").setAttribute("style", "display:none;");
                            document.getElementById("gcrgdcmin").value = "";
                        });
                        $(document).on("click", "#gcrperPolVeni_yes", function (e) {
                            document.getElementById("gcrperPolVeniMinGroup").setAttribute("style", "display:block;");
                        });
                        $(document).on("click", "#gcrperPolVeni_no", function (e) {
                            document.getElementById("gcrperPolVeniMinGroup").setAttribute("style", "display:none;");
                            document.getElementById("gcrperPolVeniMin").value = "";
                        });
                        $(document).on("click", "#gcrselIz_yes", function (e) {
                            document.getElementById("gcrselIzMinGroup").setAttribute("style", "display:block;");
                        });
                        $(document).on("click", "#gcrselIz_no", function (e) {
                            document.getElementById("gcrselIzMinGroup").setAttribute("style", "display:none;");
                            document.getElementById("gcrselIzMin").value = "";
                        });
                        $(document).on("change", "#gcrobrezekcii", function (e) {
                            var selectedVal = $(this).find("option:selected").val();
                            switch (selectedVal) {
                                case "3":
                                    document.getElementById("gcrsegmRezGroup").setAttribute("style", "display:block;");
                                    document.getElementById("pechNedostatochnost").setAttribute("style", "display:block;");
                                    break;
                                default:
                                    document.getElementById("gcrsegmRezGroup").setAttribute("style", "display:none;");
                                    document.getElementById("pechNedostatochnost").setAttribute("style", "display:none;");

                                    document.getElementById("gcrsegrez").value = 0;
                                    document.getElementById("gcrchisloUdSeg").value = 0;
                                    break;
                            }
                        });
                        if (document.getElementById('gcrgem_yes').checked) {
                            document.getElementById("gcrgemGroup").setAttribute("style", "display:block;");
                        }
                        if (document.getElementById('gcrgdc_yes').checked) {
                            document.getElementById("gcrgdcMinGroup").setAttribute("style", "display:block;");
                        }
                        if (document.getElementById('gcrperPolVeni_yes').checked) {
                            document.getElementById("gcrperPolVeniMinGroup").setAttribute("style", "display:block;");
                        }
                        if (document.getElementById('gcrselIz_yes').checked) {
                            document.getElementById("gcrselIzMinGroup").setAttribute("style", "display:block;");
                        }
                        var selectedVal = $("#gcrobrezekcii").find("option:selected").val();
                        if (selectedVal == "3") {
                            document.getElementById("gcrsegmRezGroup").setAttribute("style", "display:block;");
                        }
                        $(document).ready(function () {
                            var expired = document.getElementById("expiredValue").value;
                            var isAdmin = document.getElementById("isAdminValue").value;
                            if (expired === "true" && isAdmin === "false") {
                                var c = document.getElementById("gcrForm").elements;
                                for (var i = 0; i < c.length; i++) {
                                    c[i].setAttribute("disabled", true);
                                }
                            }
                            if ($("#showComplications").val() === "true") {
                                $('html, body').animate({
                                    scrollTop: $('#osl_no').offset().top
                                });
                            }
                            var selectedVal = $("#gcroplech").find("option:selected").val();

                            if (selectedVal == "1" && document.getElementById('osl_yes').checked) {
                                document.getElementById("gcrrezekciaGroup").setAttribute("style", "display:block;");
                                document.getElementById("gcroslRezGroup").setAttribute("style", "display:block;");
                                document.getElementById("pechNedostatochnost").setAttribute("style", "display:block;");
                            }
                            if (selectedVal == "2" && document.getElementById('osl_yes').checked) {
                                document.getElementById("gcrrhaGroup").setAttribute("style", "display:block;");
                                document.getElementById("gcroslRHAGroup").setAttribute("style", "display:block;");
                                document.getElementById("krovotechenie").setAttribute("style", "display:block;");
                            }
                            if (selectedVal == "3" && document.getElementById('osl_yes').checked) {
                                document.getElementById("gcralkogGroup").setAttribute("style", "display:block;");
                                document.getElementById("pechNedostatochnost").setAttribute("style", "display:block;");
                                document.getElementById("krovotechenie").setAttribute("style", "display:block;");
                            }
                        });
                    </script>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>