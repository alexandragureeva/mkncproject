<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Дренирование жидкостных скоплений</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/datepicker.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.buttonRow):not(.section-header):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row buttonRow">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="${(punkcia.drenId != null)? "../" : ""}../patients/update/${punkcia.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные о дренировании жидкостных скоплений брюшной полости</h4>
            </div>
            <div class="modal-body">
                <c:url value="/savePunkcia?${_csrf.parameterName}=${_csrf.token}" var="punkciaUrl"/>
                <form:form action="${punkciaUrl}" modelAttribute="punkcia" method="post"
                           id="punkciaForm">

                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>

                    <spring:bind path="drenId">
                        <form:hidden path="drenId"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="date">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="date">Дата операции:</label>
                            </div>
                            <div class="col-xs-9 ${status.error ? 'has-error' : ''}">
                                <div class="input-group date">
                                    <form:input type='text' class="form-control" aria-describedby="addon"
                                                id='date' path="date"/>
                                    <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                <form:errors path="date" class="control-label"/>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="type">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="type">Операция:</label>
                            </div>
                            <div class="col-xs-9 ${status.error ? 'has-error' : ''}">
                                <form:select
                                        class="form-control" id="type" path="type">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Дренирование</form:option>
                                    <form:option value="2">Пункция</form:option>
                                </form:select>
                                <form:errors path="type" class="control-label"/>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="organ">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="organ">Дренирование:</label>
                            </div>
                            <div class="col-xs-9 ${status.error ? 'has-error' : ''}">
                                <form:select
                                        class="form-control" id="organ" path="organ">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Серомы</form:option>
                                    <form:option value="2">Билиомы</form:option>
                                    <form:option value="3">Гематомы</form:option>
                                    <form:option value="4">Абсцесса</form:option>
                                </form:select>
                                <form:errors path="organ" class="control-label"/>
                            </div>
                        </div>
                    </spring:bind>

                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Сохранить</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<script>
    $('#date').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayBtn: 'linked',
        pickerPosition: "bottom-left"
    });
    $(document).ready(function() {
        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            var c = document.getElementById("drenirovanieForm").elements;
            for (var i = 0; i < c.length; i++) {
                c[i].setAttribute("disabled", true);
            }
        }
    });
</script>
</body>
</html>