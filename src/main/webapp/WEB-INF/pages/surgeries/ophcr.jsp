<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Операция при ХЦР</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .row:not(.buttonRow):not(.unnecessaryRow):not(.section-header):before {
            content: "*";
            color: red;
        }

        #oslozhneniyaDiv {
            border-left: 6px solid dodgerblue;
            background-color: lightblue;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row buttonRow">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${hcr.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">

            <div class="modal-body">
                <c:url value="/saveOpHcr?${_csrf.parameterName}=${_csrf.token}" var="hcrUrl"/>
                <form:form action="${hcrUrl}" modelAttribute="hcr" method="post"
                           id="hcrForm">
                    <c:if test="${twoStepsTreatment}">
                        <jsp:include page="firstStage.jsp"/>
                    </c:if>
                    <div class="modal-header">
                        <h4><i class="fa fa-bar-chart"></i>Данные об операции при диагнозе "ХЦР"</h4>
                    </div>
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>

                    <spring:bind path="date">
                        <form:hidden path="date"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="dlitop">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="dlitop">Длительность операции (мин.):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="dlitop" path="dlitop" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <jsp:include page="subSurgery.jsp"/>
                    <spring:bind path="hcroplech">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="hcroplech">Вид оперативного лечения:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="hcroplech" path="hcroplech">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Резекция печени</form:option>
                                    <form:option value="2">Чрескожая РЧА (чРЧА)</form:option>
                                    <form:option value="3">Алкоголизация</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="form-group" id="hcrrezekciaGroup" style="display: none">
                        <spring:bind path="hcrobrezekcii">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="hcrobrezekcii">Объем резекции:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="hcrobrezekcii" path="hcrobrezekcii">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Обширная резекция ЛГГЭ</form:option>
                                        <form:option value="2">Обширная резекция ПГГЭ</form:option>
                                        <form:option value="3">Cегментарная резекция</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <div class="form-group" id="hcrsegmRezGroup" style="display: none">
                            <spring:bind path="hcrsegrez">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="hcrsegrez">Тип сегментарной резекции:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="hcrsegrez" path="hcrsegrez">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">Анатомическая</form:option>
                                            <form:option value="2">Неанатомическая</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                            <spring:bind path="hcrchisloUdSeg">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="hcrchisloUdSeg">Число удаленных сегментов:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="hcrchisloUdSeg" path="hcrchisloUdSeg">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">1</form:option>
                                            <form:option value="2">2</form:option>
                                            <form:option value="3">3</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="form-group" id="hcrrhaGroup" style="display:none;">
                        <spring:bind path="hcrchisloOch">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="hcrchisloOch">Число очагов:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="hcrchisloOch" path="hcrchisloOch">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">1</form:option>
                                        <form:option value="2">2</form:option>
                                        <form:option value="3">3</form:option>
                                        <form:option value="4">>3</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="hcrrazOch">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="hcrrazOch">Размер очагов:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:input
                                            id="hcrrazOch" path="hcrrazOch" class="form-control" type="number"/>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="hcrblizSosud">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label>Близость к сосудам:</label>
                                </div>
                                <div class="col-xs-9">
                                    <label class="radio-inline"><form:radiobutton path="hcrblizSosud"
                                                                                  id="hcrblizSosud_no"
                                                                                  name="optradio" value="0"/>Нет</label>
                                    <label class="radio-inline"><form:radiobutton path="hcrblizSosud"
                                                                                  id="hcrblizSosud_yes"
                                                                                  name="optradio" value="1"/>Да</label>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="hcrpolnRHA">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="hcrpolnRHA">Полнота РЧА:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="hcrpolnRHA" path="hcrpolnRHA">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Полная</form:option>
                                        <form:option value="2">Неполная</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                    <%-- <div class="form-group" id="alkogGroup" style="display: none;">
                         <spring:bind path="alkChisloOch">
                             <div class="form-group">
                                 <label for="alkChisloOch">Число очагов:</label>
                                 <form:select class="form-control" id="alkChisloOch" path="alkChisloOch" >
                                     <form:option value="0">---------</form:option>
                                     <form:option value="1">1</form:option>
                                     <form:option value="2">2</form:option>
                                     <form:option value="3">3</form:option>
                                     <form:option value="4">>3</form:option>
                                 </form:select>
                             </div>
                         </spring:bind>
                         <div class="form-group">
                             <spring:bind path="alkRazOch">
                                 <div class="form-inline">
                                     <label for="alkRazOch">Размер очагов:</label>
                                     <form:input id="alkRazOch" path="alkRazOch"  class="form-control" type="text"/>
                                 </div>
                             </spring:bind>
                         </div>
                     </div>--%>
                    <spring:bind path="krovopoteria">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="krovopoteria">Объем кровопотери (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="krovopoteria" path="krovopoteria">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1"><500</form:option>
                                    <form:option value="2">500-1000</form:option>
                                    <form:option value="3">>1000</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="gemotransfusia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Гемотрансфузия:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="gemotransfusia" id="hcrgem_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="gemotransfusia" id="hcrgem_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="hcrgemotransfusia_ml">
                        <div class="row" id="hcrgemGroup" style="display: none">
                            <div class="col-xs-3">
                                <label for="hcrgemotransfusia_ml">Объем гемотрансфузии (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="hcrgemotransfusia_ml" path="hcrgemotransfusia_ml" class="form-control"
                                        type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="plazma">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="plazma">Объем плазмы трансфузии (мл):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="plazma" path="plazma" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="gdc">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Пережатие ГДС:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="gdc" id="hcrgdc_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="gdc" id="hcrgdc_yes" name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="hcrgdcmin">
                        <div class="row" id="hcrgdcMinGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="hcrgdcmin">Пережатие ГДС (мин):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="hcrgdcmin" path="hcrgdcmin" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="perPolVeni">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Пережатие нижней полой вены:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="perPolVeni" id="hcrperPolVeni_no"
                                                                              name="optradio" value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="perPolVeni" id="hcrperPolVeni_yes"
                                                                              name="optradio" value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="hcrperPolVeniMin">
                        <div class="row" id="hcrperPolVeniMinGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="hcrgdcmin">Пережатие нижней полой вены (мин):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="hcrperPolVeniMin" path="hcrperPolVeniMin" class="form-control"
                                        type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="selIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Селективная сосудистая изоляция:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="selIz" id="hcrselIz_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="selIz" id="hcrselIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="hcrselIzMin">
                        <div class="row" id="hcrselIzMinGroup" style="display: none;">
                            <div class="col-xs-3">
                                <label for="hcrselIzMin">Селективная сосудистая изоляция (мин.):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="hcrselIzMin" path="hcrselIzMin" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="intrRHA">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Интраоперационная РЧА:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="intrRHA" id="intrRHA_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="intrRHA" id="intrRHA_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="tnm">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="tnm">Стадия TNM:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="tnm" id="tnm">
                                    <form:option value="0">---------</form:option>
                                    <optgroup label="Стадия I">
                                        <form:option value="1">T1N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия II">
                                        <form:option value="2">T2aN0M0</form:option>
                                        <form:option value="3">T2bN0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIA">
                                        <form:option value="4">T3N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIB">
                                        <form:option value="5">T1N1M0</form:option>
                                        <form:option value="6">T2N1M0</form:option>
                                        <form:option value="7">T3N1M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVa">
                                        <form:option value="8">T4N0M0</form:option>
                                        <form:option value="9">T4N1M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVb">
                                        <form:option value="10">Любая T, N2M0</form:option>
                                        <form:option value="11">Любая T, любая N, M1</form:option>
                                    </optgroup>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="bypass">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Bypass:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="bypass" id="bypass_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="totIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Тотальная сосудистая изоляция (с пережатием НПВ):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_no" name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="totIz" id="totIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="selSosIz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Селективная сосудистая изоляция (с раздельным пережатием печеночных вен):</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="selSosIz" id="selSosIz_no"
                                                                              name="optradio"
                                                                              value="0"/>Нет</label>
                                <label class="radio-inline"><form:radiobutton path="selSosIz" id="selSosIz_yes"
                                                                              name="optradio"
                                                                              value="1"/>Да</label>
                            </div>
                        </div>
                    </spring:bind>
                    <c:if test="${showOslozhneniya}">
                        <div id="oslozhneniyaDiv">
                            <jsp:include page="oslozhneniya.jsp"/>

                            <spring:bind path="clavien">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>Классификация осложнений по Clavien-Dindo:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="clavien" path="clavien">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">I</form:option>
                                            <form:option value="2">II</form:option>
                                            <form:option value="3">IIIa</form:option>
                                            <form:option value="4">IIIb</form:option>
                                            <form:option value="5">IVa</form:option>
                                            <form:option value="6">IVb</form:option>
                                            <form:option value="7">V</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </c:if>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>
                </form:form>
                <script>

                    $(document).on("change", "#hcroplech", function (e) {
                        var selectedVal = $(this).find("option:selected").val();
                        switch (selectedVal) {
                            case "1":
                                document.getElementById("hcrrezekciaGroup").setAttribute("style", "display:block;");
                                document.getElementById("hcrrhaGroup").setAttribute("style", "display:none;");
                                document.getElementById("hcrchisloOch").value = 0;
                                document.getElementById("hcrrazOch").value = 0;
                                document.getElementById("hcrblizSosud").value = 0;
                                document.getElementById("hcrpolnRHA").value = 0;

                                break;
                            case "2":
                                document.getElementById("hcrrhaGroup").setAttribute("style", "display:block;");
                                document.getElementById("hcrrezekciaGroup").setAttribute("style", "display:none;");
                                document.getElementById("hcrobrezekcii").value = 0;
                                document.getElementById("hcrsegrez").value = 0;
                                document.getElementById("hcrchisloUdSeg").value = 0;
                                break;
                            default:
                                document.getElementById("hcrrezekciaGroup").setAttribute("style", "display:none;");
                                document.getElementById("hcrrhaGroup").setAttribute("style", "display:none;");
                                document.getElementById("hcrchisloOch").value = 0;
                                document.getElementById("hcrrazOch").value = 0;
                                document.getElementById("hcrblizSosud").value = 0;
                                document.getElementById("hcrpolnRHA").value = 0;
                                document.getElementById("hcrobrezekcii").value = 0;
                                document.getElementById("hcrsegrez").value = 0;
                                document.getElementById("hcrchisloUdSeg").value = 0;
                                break;
                        }
                    });
                    $(document).on("click", "#hcrgem_yes", function (e) {
                        document.getElementById("hcrgemGroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#hcrgem_no", function (e) {
                        document.getElementById("hcrgemGroup").setAttribute("style", "display:none;");
                        document.getElementById("hcrgemotransfusia_ml").value = "";
                    });
                    $(document).on("click", "#hcrgdc_yes", function (e) {
                        document.getElementById("hcrgdcMinGroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#hcrgdc_no", function (e) {
                        document.getElementById("hcrgdcMinGroup").setAttribute("style", "display:none;");
                        document.getElementById("hcrgdcmin").value = "";
                    });
                    $(document).on("click", "#hcrperPolVeni_yes", function (e) {
                        document.getElementById("hcrperPolVeniMinGroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#hcrperPolVeni_no", function (e) {
                        document.getElementById("hcrperPolVeniMinGroup").setAttribute("style", "display:none;");
                        document.getElementById("hcrperPolVeniMin").value = "";
                    });
                    $(document).on("click", "#hcrselIz_yes", function (e) {
                        document.getElementById("hcrselIzMinGroup").setAttribute("style", "display:block;");
                    });
                    $(document).on("click", "#hcrselIz_no", function (e) {
                        document.getElementById("hcrselIzMinGroup").setAttribute("style", "display:none;");
                        document.getElementById("hcrselIzMin").value = "";
                    });
                    $(document).on("change", "#hcrobrezekcii", function (e) {
                        var selectedVal = $(this).find("option:selected").val();
                        switch (selectedVal) {
                            case "3":
                                document.getElementById("hcrsegmRezGroup").setAttribute("style", "display:block;");
                                break;
                            default:
                                document.getElementById("hcrsegmRezGroup").setAttribute("style", "display:none;");
                                document.getElementById("hcrsegrez").value = 0;
                                document.getElementById("hcrchisloUdSeg").value = 0;
                                break;
                        }
                    });


                    if (document.getElementById('hcrgem_yes').checked) {
                        document.getElementById("hcrgemGroup").setAttribute("style", "display:block;");
                    }
                    if (document.getElementById('hcrgdc_yes').checked) {
                        document.getElementById("hcrgdcMinGroup").setAttribute("style", "display:block;");
                    }
                    if (document.getElementById('hcrperPolVeni_yes').checked) {
                        document.getElementById("hcrperPolVeniMinGroup").setAttribute("style", "display:block;");
                    }
                    if (document.getElementById('hcrselIz_yes').checked) {
                        document.getElementById("hcrselIzMinGroup").setAttribute("style", "display:block;");
                    }
                    var selectedVal = $("#hcroplech").find("option:selected").val();
                    if (selectedVal == "1") {
                        document.getElementById("hcrrezekciaGroup").setAttribute("style", "display:block;");
                    }
                    if (selectedVal == "2") {
                        document.getElementById("hcrrhaGroup").setAttribute("style", "display:block;");
                    }
                    selectedVal = $("#hcrobrezekcii").find("option:selected").val();
                    if (selectedVal == "3") {
                        document.getElementById("hcrsegmRezGroup").setAttribute("style", "display:block;");
                    }
                    $(document).ready(function () {
                        var expired = document.getElementById("expiredValue").value;
                        var isAdmin = document.getElementById("isAdminValue").value;
                        if (expired === "true" && isAdmin === "false") {
                            var c = document.getElementById("hcrForm").elements;
                            for (var i = 0; i < c.length; i++) {
                                c[i].setAttribute("disabled", true);
                            }
                        }
                    });
                </script>
            </div>
        </div>
    </div>
</div>
</body>
</html>