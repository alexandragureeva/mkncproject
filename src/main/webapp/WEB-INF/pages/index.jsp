<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<jsp:useBean id="now" class="java.util.Date"/>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">
    <title>MKNC Patients</title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/dashboard.css"/>" rel="stylesheet">
    <link rel="stylesheet" href="<c:url value="/resources/js/SyntaxHighlighter/styles/SyntaxHighlighter.css"/>"/>
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
    <script src="<c:url value="/resources/js/jquery-3.1.0.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery-1.9.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tooltip.js"/>"></script>
    <script src="<c:url value="/resources/js/TableFilter/tablefilter_all.js"/>"></script>

    <script src="<c:url value="/resources/js/SyntaxHighlighter/scripts/shCore.js"/>"></script>
    <script src="<c:url value="/resources/js/SyntaxHighlighter/scripts/shBrushJScript.js"/>"></script>
    <script src="<c:url value="/resources/js/SyntaxHighlighter/scripts/shBrushXml.js"/>"></script>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<c:url value="/resources/js/TableFilter/" var="tableFilter"/>

<style>
    .badge-notify {
        background: red;
        position: relative;
        top: -40%;
        left: -20%;
    }
    .sidebar ul.nav li.divider {
        border-bottom: 1px solid #eee;
        margin: 20px 0;
    }
</style>
<script language="javascript" type="text/javascript">
    //<![CDATA[
    tf_AddEvent(window, 'load', initPage);
    function initPage() {
        initHighlighter();
        initFilters();
    }

    function initHighlighter() {
        dp.SyntaxHighlighter.ClipboardSwf = "includes/SyntaxHighlighter/Scripts/clipboard.swf";
        dp.SyntaxHighlighter.HighlightAll("code");
    }

    //TF objects are instanciated
    function initFilters() {
        tf_Id('inpSearch').value = '';
        for (var i = 1; i < 19; i++) {
            window['tf0' + i] = setFilterGrid('tbl0' + i, tblConfig);
        }
    }
    //Global tf objects
    var tf01, tf02, tf03, tf04, tf05, tf06, tf07, tf08, tf09, tf010, tf011, tf012, tf013, tf014, tf015, tf016, tf017, tf018;

    //TF config object
    var tblConfig = {
        filters_row_index: 1,
        rows_counter: true,
        single_search_filter: true,
        highlight_keywords: true,
        rows_counter_text: "Пациентов: ",
        highlight_css_class: 'highlight',
        loader: true,
        loader_text: "Фильтрую...",
        enable_default_theme: true,
        enter_key: false,
        //Single search filter for each table is hidden
        on_filters_loaded: function (o) {
            o.tbl.rows[o.GetFiltersRowIndex()].style.display = 'none';
        }
    };

    //Filter tables
    var filterTables = function () {
        for (var i = 1; i < 19; i++) {
            window['tf0' + i].SetFilterValue(0, tf_Id('inpSearch').value);
            window['tf0' + i].Filter();
        }
    };

    //Clear tables
    var clearTables = function () {
        tf_Id('inpSearch').value = '';
        for (var i = 1; i < 19; i++) {
            window['tf0' + i].ClearFilters();
            window['tf0' + i].Filter();
        }
    };
    var tFilter = '${tableFilter}';
    //]]>
</script>
<nav class="navbar navbar-inverse navbar-fixed-top" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-brand" href="http://mknc.ru/">
            MKNC Patients
        </a>
    </div>
    <ul class="nav navbar-nav navbar-right" style="margin-right: 3%">
        <c:url value="/resources/sounds/solemn.mp3" var="mp3alert"/>
        <c:url value="/resources/sounds/solemn.ogg" var="oggalert"/>
        <c:if test="${patientsInfo != null && patientsInfo.size() > 0}">
            <div id="sound">
                <audio autoplay="autoplay">
                    <source src="${mp3alert}" type="audio/mpeg"/>
                    <source src="${oggalert}" type="audio/ogg"/>
                    <embed hidden="true" autostart="true" loop="false" src="${mp3alert}"/>
                </audio>
            </div>
        </c:if>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-bell fa-inverse fa-fw"></i>
                <c:if test="${patientsInfo != null && patientsInfo.size() > 0}">
                    <span class="badge badge-notify">${patientsInfo.size()}</span>
                </c:if>
                <i class="fa fa-caret-down fa-inverse"></i>
            </a>
            <ul class="dropdown-menu dropdown-alerts" style="height: 500%; overflow: auto">
                <c:if test="${patientsInfo != null && patientsInfo.size() > 0}">
                    <c:forEach var="i" begin="0" end="${patientsInfo.size()-1}">
                        <li>
                            <spring:url
                                    value="/patients/update/${patientsInfo.get(i).id}"
                                    var="updateUrl"/>
                            <a href="${updateUrl}">
                                <div>
                                    <p>Осталось <b>${patientsInfo.get(i).dateDiff} ${patientsInfo.get(i).day}</b>
                                        на заполнение блока "${patientsInfo.get(i).patientBlock}"
                                        у пациента <b>"${patientsInfo.get(i).patientName}"</b>.</p>
                                    <p>Ответственный: <b>${patientsInfo.get(i).userName}</b>.</p>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                    </c:forEach>
                </c:if>
            </ul>
        </li>
        <li>
            <a href="edit_account/${u.userId}">
                <i class="fa fa-cog fa-inverse  fa-fw" aria-hidden="true"></i>Настройки
            </a>
        </li>
    </ul>
</nav>
<div class="container-fluid">
    <div class="row ">
        <!--Doctor Profile-->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 sidebar">
            <ul class="nav nav-sidebar">
                <li>
                    <div class="photo-container">
                        <c:if test="${u.getPhoto()!=null}">
                            <p>
                                <img src="<c:url value="/resources/images/${u.getPhoto()}"/>">
                            </p>
                        </c:if>
                        <c:if test="${u.getPhoto()==null}">
                            <p>
                                <img src="<c:url value="/resources/images/default_icon.png"/>">
                            </p>
                        </c:if>
                    </div>
                </li>
                <li>
                    <i class="fa fa-user-md"></i><u>${u.getUserName()}</u>
                </li>
                <li>${u.getEmail()}</li>
                <li class="active"><a href="welcome">Пациенты</a></li>
                <li><a href="generalInfo">Новый пациент</a></li>
                <li>
                    <a href="statistics">Статистика</a>
                </li>
                <li role="presentation" class="divider"></li>
                <li сlass="active"><a href="help">Помощь</a></li>
                <c:url value="/j_spring_security_logout" var="logoutUrl"/>
                <li><a href="javascript:formSubmit()"><i class="fa fa-sign-out fa-fw"></i> Выход</a></li>
                <li></li>
                <%--<li style="margin-left: 5%"><b>Вход выполнен в: ${now.toLocaleString()}</b></li>--%>
                <form action="${logoutUrl}" method="post" id="logoutForm">
                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                </form>
            </ul>
        </div>
        <script>
            function formSubmit() {
                document.getElementById("logoutForm").submit();
            }
        </script>
        <!--Information-->
        <div class="col-lg-offset-2 col-xs-offset-3 col-lg-10 col-md-10 col-sm-10 col-xs-9">
            <div class="container-fluid">
                <div class="filter">
                    <form class="form-inline">
                        <div class="form-group-edit">
                            <label>Фильтр: </label>
                            <div class='input-group'>
                                <input id="inpSearch" type="input" onkeyup="filterTables();" class="form-control"
                                       value="">
                                        <span class="input-group-addon">
                                            <span class="fa fa-filter"></span>
                                        </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <%--   <c:if test="${not empty msg}">
                   <div class="alert alert-${css} alert-dismissible" role="alert">
                       <button type="button" class="close" data-dismiss="alert"
                               aria-label="Close">
                           <span aria-hidden="true">×</span>
                       </button>
                       <strong>${msg}</strong>
                   </div>
               </c:if>--%>
            <div class="tabs">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a href="#patient_pane" data-toggle="tab">Данные о пациентах</a></li>
                    <li><a href="#diagnosis_pane" data-toggle="tab">Данные по диагнозам</a></li>
                    <li><a href="#surgery_pane" data-toggle="tab">Данные по операциям</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="patient_pane">
                        <c:if test="${not empty msg}">
                            <div class="alert alert-${css} alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong>${msg}</strong>
                            </div>
                        </c:if>
                        <!--Таблица по пациентам-->
                            <div class="panel panel-default">
                                <div id="panel-body">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl01">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Пол</th>
                                                    <th>Дата рождения</th>
                                                    <th>Возраст</th>
                                                    <th>Диагноз</th>
                                                    <th>История болезни</th>
                                                    <th>Поступление</th>
                                                    <th>Выписка</th>
                                                    <th>Количество койко-дней</th>
                                                    <th>Детали</th>
                                                    <th>Дата смерти</th>
                                                    <th>Действия</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${patients!= null && patients.size() != 0}">
                                                    <c:forEach var="i" begin="0" end="${patients.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${patients.get(i).name}</td>
                                                            <td>${(patients.get(i).sex==0)?"ж":"м"}</td>
                                                            <td>${patients.get(i).birthdate}</td>
                                                            <td>${patients.get(i).age}</td>
                                                            <td>${patients.get(i).getDiagnosisName()}</td>
                                                            <td>${patients.get(i).diseaseNumber}</td>
                                                            <td>${patients.get(i).startDate}</td>
                                                            <td>${patients.get(i).endDate}</td>
                                                            <td>${patients.get(i).bedDaysNumber}</td>
                                                            <td data-container="body" data-toggle="tooltip"
                                                                data-placement="top" title="${patients.get(i).details}">
                                                                    ${(patients.get(i).details.length() < 20) ? patients.get(i).details : patients.get(i).details.substring(0, 20).concat("...")}
                                                            </td>
                                                            <td>${patients.get(i).deathDate}</td>
                                                            <td>
                                                                <spring:url
                                                                        value="/patients/update/${patients.get(i).id}"
                                                                        var="updateUrl"/>
                                                                <button class="btn btn-primary btn-sm"
                                                                        onclick="location.href='${updateUrl}'"><i
                                                                        class="fa fa-pencil-square-o"
                                                                        aria-hidden="true"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!--Конец таблицы по пациентам-->
                    <div class="tab-pane fade" id="diagnosis_pane">
                        <!--Таблица по операции 1-->
                        <div class="panel-group" id="accordion2">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-1-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по диагнозу "Опухоль Клатскина"
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-1-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl02">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>GRWR</th>
                                                    <th>ICG</th>
                                                    <th>ICGK-F</th>
                                                    <th>Сцинт.(индекс захвата)</th>
                                                    <th>Сцинт.(% функ. печени)</th>
                                                    <th>Bismuth</th>
                                                    <th>Инвазия в ворот.вену</th>
                                                    <th>Инвазия в печ.артерию</th>
                                                    <th>Степеь диф.опухоли</th>
                                                    <th>Периневральная инвазия</th>
                                                    <th>Полож.культура желчи</th>
                                                    <th>Оценка троф.статуса</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${klatskin != null && klatskin.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${klatskin.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${klatskin.get(i).name}</td>
                                                            <td>${klatskin.get(i).grwr}</td>
                                                            <td>${klatskin.get(i).icg}</td>
                                                            <td>${klatskin.get(i).icgk_f}</td>
                                                            <td>${klatskin.get(i).indexZachvata}</td>
                                                            <td>${klatskin.get(i).objemOstatka}</td>
                                                            <td>${klatskin.get(i).bismuth}</td>
                                                            <td>${klatskin.get(i).vorotInvasia}</td>
                                                            <td>${klatskin.get(i).pechInvasia}</td>
                                                            <td>${klatskin.get(i).differOpuchol}</td>
                                                            <td>${klatskin.get(i).perinevInvasia}</td>
                                                            <td>${klatskin.get(i).polozhKultura}</td>
                                                            <td>${klatskin.get(i).trofStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 1-->

                        <!--Таблица по операции 2-->
                        <div class="panel-group" id="accordion3">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-2-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по диагнозу "ГЦР"
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-2-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl03">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Цирроз(СТР)</th>
                                                    <th>ВРВ пищевода и желудка</th>
                                                    <th>Тромбоцитопения</th>
                                                    <th>Этиология цирроза</th>
                                                    <th>Вирусный гепатит</th>
                                                    <th>Фиброз печени</th>
                                                    <th>Макс. размер опухоли</th>
                                                    <th>Тип опух. узлов</th>
                                                    <th>Число опух. узлов</th>
                                                    <th>Сосудистая инвазия</th>
                                                    <th>Внепеченочные очаги</th>
                                                    <th>Тромбоз печеночных вен</th>
                                                    <th>Уровень онкомаркеров</th>
                                                    <th>ICG</th>
                                                    <th>ICGK-F</th>
                                                    <th>Сцинт.(индекс захвата)</th>
                                                    <th>Сцинт.(% функ. печени)</th>
                                                    <th>Противовирусная терапия</th>
                                                    <th>Тромбоз воротной вены</th>
                                                    <th>Лечение сорафенибом</th>
                                                    <th>ТАХЭ</th>
                                                    <th>Тип опухоли(BCLC)</th>
                                                    <th>Оценка троф.статуса</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${gcr != null && gcr.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${gcr.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${gcr.get(i).name}</td>
                                                            <td>${gcr.get(i).cirrozClass}</td>
                                                            <td>${gcr.get(i).vrvStepen}</td>
                                                            <td>${gcr.get(i).trombozStepen}</td>
                                                            <td>${gcr.get(i).etiologiaCirroza}</td>
                                                            <td>${gcr.get(i).gepatitStepen}</td>
                                                            <td>${gcr.get(i).fibrozClass}</td>
                                                            <td>${gcr.get(i).razmerOpucholi}</td>
                                                            <td>${gcr.get(i).opuchUzliType}</td>
                                                            <td>${gcr.get(i).chisloOpuchUzlov}</td>
                                                            <td>${gcr.get(i).sosudInvasia}</td>
                                                            <td>${gcr.get(i).vnepOchagiType}</td>
                                                            <td>${gcr.get(i).trombPechType}</td>
                                                            <td>${gcr.get(i).onkomarker}</td>
                                                            <td>${gcr.get(i).icg}</td>
                                                            <td>${gcr.get(i).icgk_f}</td>
                                                            <td>${gcr.get(i).indexZachvata}</td>
                                                            <td>${gcr.get(i).objemOstatka}</td>
                                                            <td>${gcr.get(i).protivTerapia}</td>
                                                            <td>${gcr.get(i).trombozVorotVeni}</td>
                                                            <td>${gcr.get(i).lechenieSorafinib}</td>
                                                            <td>${gcr.get(i).tache}</td>
                                                            <td>${gcr.get(i).bclcType}</td>
                                                            <td>${gcr.get(i).trofStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 2-->
                        <!--Таблица по операции 3-->
                        <div class="panel-group" id="accordion6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-4-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по диагнозу "МКР"
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-4-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl04">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Внепеченочные метастазы</th>
                                                    <th>Первичная опухоль</th>
                                                    <th>Стадия перв. опухоли</th>
                                                    <th>Операция на перв. опухоли</th>
                                                    <th>Макс. размер опухоли</th>
                                                    <th>Тип опух. узлов</th>
                                                    <th>Число опух. узлов</th>
                                                    <th>Сосудистая инвазия</th>
                                                    <th>Внепеченочные очаги</th>
                                                    <th>Тромбоз печеночных вен</th>
                                                    <th>Уровень онкомаркеров</th>
                                                    <th>ICG</th>
                                                    <th>ICGK-F</th>
                                                    <th>Сцинт.(индекс захвата)</th>
                                                    <th>Сцинт.(% функ. печени)</th>
                                                    <th>Неоадъювантной химиотерапия</th>
                                                    <th>Курсы химиотерапии</th>
                                                    <th>Препараты</th>
                                                    <th>Эффект</th>
                                                    <th>Метастазы в печени</th>
                                                    <th>Время появ-я метаст. в печени</th>
                                                    <th>Тромбоз воротной вены</th>
                                                    <th>Оценка троф.статуса</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${mkr != null && mkr.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${mkr.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${mkr.get(i).name}</td>
                                                            <td>${mkr.get(i).nalVnepMet}</td>
                                                            <td>${mkr.get(i).pervOpuch}</td>
                                                            <td>${mkr.get(i).stadPervOpuch}</td>
                                                            <td>${mkr.get(i).obOPerPerOpuch}</td>
                                                            <td>${mkr.get(i).razmerOpucholi}</td>
                                                            <td>${mkr.get(i).opuchUzliType}</td>
                                                            <td>${mkr.get(i).chisloOpuchUzlov}</td>
                                                            <td>${mkr.get(i).sosudInvasia}</td>
                                                            <td>${mkr.get(i).vnepOchagiType}</td>
                                                            <td>${mkr.get(i).trombPechType}</td>
                                                            <td>${mkr.get(i).onkomarker}</td>
                                                            <td>${mkr.get(i).icg}</td>
                                                            <td>${mkr.get(i).icgk_f}</td>
                                                            <td>${mkr.get(i).indexZachvata}</td>
                                                            <td>${mkr.get(i).objemOstatka}</td>
                                                            <td>${mkr.get(i).nalNeoadChimioTer}</td>
                                                            <td>${mkr.get(i).chimiotKurs}</td>
                                                            <td>${mkr.get(i).chimiotPrep}</td>
                                                            <td>${mkr.get(i).chimiotEffect}</td>
                                                            <td>${mkr.get(i).pojavMetType}</td>
                                                            <td>${mkr.get(i).pojavMetTime}</td>
                                                            <td>${mkr.get(i).trombozVorotVeni}</td>
                                                            <td>${mkr.get(i).trofStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 3-->
                        <!--Таблица по операции 3-->
                        <div class="panel-group" id="accordion4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-3-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по диагнозу "ХЦР"
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-3-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl05">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>GRWR</th>
                                                    <th>Макс. размер опухоли</th>
                                                    <th>Тип опух. узлов</th>
                                                    <th>Число опух. узлов</th>
                                                    <th>Сосудистая инвазия</th>
                                                    <th>Внепеченочные очаги</th>
                                                    <th>Уровень онкомаркеров</th>
                                                    <th>ICG</th>
                                                    <th>ICGK-F</th>
                                                    <th>Сцинт.(индекс захвата)</th>
                                                    <th>Сцинт.(% функ. печени)</th>
                                                    <th>Противовирусная терапия</th>
                                                    <th>Тромбоз воротной вены</th>
                                                    <th>Тромбоз печеночных вен</th>
                                                    <th>Оценка троф.статуса</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${hcr != null && hcr.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${hcr.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${hcr.get(i).name}</td>
                                                            <td>${hcr.get(i).grwr}</td>
                                                            <td>${hcr.get(i).razmerOpucholi}</td>
                                                            <td>${hcr.get(i).opuchUzliType}</td>
                                                            <td>${hcr.get(i).chisloOpuchUzlov}</td>
                                                            <td>${hcr.get(i).sosudInvasia}</td>
                                                            <td>${hcr.get(i).vnepOchagiType}</td>
                                                            <td>${hcr.get(i).onkomarker}</td>
                                                            <td>${hcr.get(i).icg}</td>
                                                            <td>${hcr.get(i).icgk_f}</td>
                                                            <td>${hcr.get(i).indexZachvata}</td>
                                                            <td>${hcr.get(i).objemOstatka}</td>
                                                            <td>${hcr.get(i).protivTerapia}</td>
                                                            <td>${hcr.get(i).trombozVorotVeni}</td>
                                                            <td>${hcr.get(i).trombPechType}</td>
                                                            <td>${hcr.get(i).trofStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 3-->


                        <!--Таблица по операции 3-->
                        <div class="panel-group" id="accordion8">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-6-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по диагнозу "Эхинококкоз"
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-6-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl06">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>ВОЗ</th>
                                                    <th>Анализ на антитела</th>
                                                    <th>Результат анализа</th>
                                                    <th>Уровень антител</th>
                                                    <th>Эпидем.анамнез</th>
                                                    <th>Кисты</th>
                                                    <th>Число кист</th>
                                                    <th>Тип кист</th>
                                                    <th>Размер кист</th>
                                                    <th>Осложнения</th>
                                                    <th>Кратность лечения</th>
                                                    <th>Рецидивный эхинок. после</th>
                                                    <th>Внепеч.поражение</th>
                                                    <th>Оценка троф.статуса</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${echinokokkoz != null && echinokokkoz.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${echinokokkoz.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${echinokokkoz.get(i).name}</td>
                                                            <td>${echinokokkoz.get(i).voz}</td>
                                                            <td>${echinokokkoz.get(i).antitela}</td>
                                                            <td>${echinokokkoz.get(i).antitelaRes}</td>
                                                            <td>${echinokokkoz.get(i).antitelaUroven}</td>
                                                            <td>${echinokokkoz.get(i).epidemAnamnez}</td>
                                                            <td>${echinokokkoz.get(i).mnozhKisty}</td>
                                                            <td>${echinokokkoz.get(i).kistyNumber}</td>
                                                            <td>${echinokokkoz.get(i).kistyType}</td>
                                                            <td>${echinokokkoz.get(i).kistyRazmer}</td>
                                                            <td>${echinokokkoz.get(i).kistyOsl}</td>
                                                            <td>${echinokokkoz.get(i).kratLechenia}</td>
                                                            <td>${echinokokkoz.get(i).lechenieType}</td>
                                                            <td>${echinokokkoz.get(i).vnepPorazhenie}</td>
                                                            <td>${echinokokkoz.get(i).trofStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Таблица по операции 1-->
                        <div class="panel-group" id="accordion9">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-7-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по диагнозу "Альвиококкоз"
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-7-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl07">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>GRWR</th>
                                                    <th>ICG</th>
                                                    <th>ICGK-F</th>
                                                    <th>Сцинт.(индекс захвата)</th>
                                                    <th>Сцинт.(% функ. печени)</th>
                                                    <th>Инвазия в ворот.вену</th>
                                                    <th>Инвазия в печ.артерию</th>
                                                    <th>Предыд. операции</th>
                                                    <th>Полож.культура желчи</th>
                                                    <th>Лечение альбендазолом</th>
                                                    <th>Оценка троф.статуса</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${alveokokkoz != null && alveokokkoz.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${alveokokkoz.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${alveokokkoz.get(i).name}</td>
                                                            <td>${alveokokkoz.get(i).grwr}</td>
                                                            <td>${alveokokkoz.get(i).icg}</td>
                                                            <td>${alveokokkoz.get(i).icgk_f}</td>
                                                            <td>${alveokokkoz.get(i).indexZachvata}</td>
                                                            <td>${alveokokkoz.get(i).objemOstatka}</td>
                                                            <td>${alveokokkoz.get(i).vorotInvasia}</td>
                                                            <td>${alveokokkoz.get(i).pechInvasia}</td>
                                                            <td>${alveokokkoz.get(i).predOperacii}</td>
                                                            <td>${alveokokkoz.get(i).polozhKultura}</td>
                                                            <td>${alveokokkoz.get(i).albendazol}</td>
                                                            <td>${alveokokkoz.get(i).trofStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 1-->
                        <!--Таблица по операции 3-->
                        <div class="panel-group" id="accordion7">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-5-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по диагнозу "МНКР"
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-5-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl08">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Первичная опухоль</th>
                                                    <th>Мет. нейроэнд.опухолей</th>
                                                    <th>Мет. ГИСО</th>
                                                    <th>Внепеченочные метастазы</th>
                                                    <th>Макс. размер опухоли</th>
                                                    <th>Тип опух. узлов</th>
                                                    <th>Число опух. узлов</th>
                                                    <th>Сосудистая инвазия</th>
                                                    <th>Внепеченочные очаги</th>
                                                    <th>Тромбоз печеночных вен</th>
                                                    <th>Уровень онкомаркеров</th>
                                                    <th>ICG</th>
                                                    <th>ICGK-F</th>
                                                    <th>Сцинт.(индекс захвата)</th>
                                                    <th>Сцинт.(% функ. печени)</th>
                                                    <th>Неоадъювантной химиотерапия</th>
                                                    <th>Курсы химиотерапии</th>
                                                    <th>Эффект</th>
                                                    <th>Метастазы в печени</th>
                                                    <th>Время появ-я метаст. в печени</th>
                                                    <th>Тромбоз воротной вены</th>
                                                    <th>Оценка троф.статуса</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${mnkr != null && mnkr.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${mnkr.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${mnkr.get(i).name}</td>
                                                            <td>${mnkr.get(i).pervOpuchType}</td>
                                                            <td>${mnkr.get(i).metNeiroOpuch}</td>
                                                            <td>${mnkr.get(i).metGiso}</td>
                                                            <td>${mnkr.get(i).nalVnepMet}</td>
                                                            <td>${mnkr.get(i).razmerOpucholi}</td>
                                                            <td>${mnkr.get(i).opuchUzliType}</td>
                                                            <td>${mnkr.get(i).chisloOpuchUzlov}</td>
                                                            <td>${mnkr.get(i).sosudInvasia}</td>
                                                            <td>${mnkr.get(i).vnepOchagiType}</td>
                                                            <td>${mnkr.get(i).trombPechType}</td>
                                                            <td>${mnkr.get(i).onkomarker}</td>
                                                            <td>${mnkr.get(i).icg}</td>
                                                            <td>${mnkr.get(i).icgk_f}</td>
                                                            <td>${mnkr.get(i).indexZachvata}</td>
                                                            <td>${mnkr.get(i).objemOstatka}</td>
                                                            <td>${mnkr.get(i).nalNeoadChimioTer}</td>
                                                            <td>${mnkr.get(i).chimiotKurs}</td>
                                                            <td>${mnkr.get(i).chimiotEffect}</td>
                                                            <td>${mnkr.get(i).pojavMetType}</td>
                                                            <td>${mnkr.get(i).pojavMetTime}</td>
                                                            <td>${mnkr.get(i).trombozVorotVeni}</td>
                                                            <td>${mnkr.get(i).trofStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Таблица по анализам-->
                        <div class="panel-group" id="accordion5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#analysis-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по диагнозу "Рак желудка"
                                        </a>
                                    </h4>
                                </div>
                                <div id="analysis-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl09">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>ICG</th>
                                                    <th>ICGK-F</th>
                                                    <th>Сцинт.(индекс захвата)</th>
                                                    <th>Сцинт.(% функ. печени)</th>
                                                    <th>Первичная опухоль</th>
                                                    <th>Инвазия в ворот.вену</th>
                                                    <th>Инвазия в печ.артерию</th>
                                                    <th>Степень диф.опухоли</th>
                                                    <th>Периневральная инвазия</th>
                                                    <th>Полож.культура желчи</th>
                                                    <th>Характер диагностики рака жел пузыря</th>
                                                    <th>Оценка троф.статуса</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${rzhps != null && rzhps.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${rzhps.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${rzhps.get(i).name}</td>
                                                            <td>${rzhps.get(i).icg}</td>
                                                            <td>${rzhps.get(i).icgk_f}</td>
                                                            <td>${rzhps.get(i).indexZachvata}</td>
                                                            <td>${rzhps.get(i).objemOstatka}</td>
                                                            <td>${rzhps.get(i).stadPervOpuch}</td>
                                                            <td>${rzhps.get(i).vorotInvasia}</td>
                                                            <td>${rzhps.get(i).pechInvasia}</td>
                                                            <td>${rzhps.get(i).differOpuchol}</td>
                                                            <td>${rzhps.get(i).perinevInvasia}</td>
                                                            <td>${rzhps.get(i).polozhKultura}</td>
                                                            <td>${rzhps.get(i).diagnRaka}</td>
                                                            <td>${rzhps.get(i).trofStatus}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по анализам-->

                    </div>
                    <div class="tab-pane fade" id="surgery_pane">
                        <!--Таблица по операции 1-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-10-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по операции при опухоли Клацкина
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-10-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl010">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Объем резекции печени</th>
                                                    <th>Резекция сосудов</th>
                                                    <th>Резекция воротной вены</th>
                                                    <th>Объем кровопотери (мл)</th>
                                                    <th>Длительность операции (мин.</th>
                                                    <th>Гемотрансфузия</th>
                                                    <th>Объем гемотрансфузии (мл)</th>
                                                    <th>Объем плазмы трансфузии (мл)</th>
                                                    <th>Пережатие ГДС</th>
                                                    <th>Пережатие ГДС (мин</th>
                                                    <th>Формирование ГЕА(1)</th>
                                                    <th>Формирование ГЕА(2)</th>
                                                    <th>Формирование ГЕА(3)</th>
                                                    <th>Радикальность операции</th>
                                                    <th>Лимфодиссекция</th>
                                                    <th>Стадия TNM</th>
                                                    <th>Bypass</th>
                                                    <th>Селективная сосудистая изоляция</th>
                                                    <th>Тотальная сосудистая изоляция</th>
                                                    <th>Осложнения</th>
                                                    <th>Clavien-Dindo</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${opKlatskin != null && opKlatskin.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${opKlatskin.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${opKlatskin.get(i).name}</td>
                                                            <td>${opKlatskin.get(i).rezekcia}</td>
                                                            <td>${opKlatskin.get(i).sosudrezekcia}</td>
                                                            <td>${opKlatskin.get(i).vorotrezekcia}</td>
                                                            <td>${opKlatskin.get(i).krovopoteria}</td>
                                                            <td>${opKlatskin.get(i).dlitop}</td>
                                                            <td>${opKlatskin.get(i).gemotransfusia}</td>
                                                            <td>${opKlatskin.get(i).gemotransfusia_ml}</td>
                                                            <td>${opKlatskin.get(i).plazma}</td>
                                                            <td>${opKlatskin.get(i).gdc}</td>
                                                            <td>${opKlatskin.get(i).gdcmin}</td>
                                                            <td>${opKlatskin.get(i).geatype1}</td>
                                                            <td>${opKlatskin.get(i).geatype2}</td>
                                                            <td>${opKlatskin.get(i).geatype3}</td>
                                                            <td>${opKlatskin.get(i).radikal}</td>
                                                            <td>${opKlatskin.get(i).limfodissekcia}</td>
                                                            <td>${opKlatskin.get(i).tnm}</td>
                                                            <td>${opKlatskin.get(i).bypass}</td>
                                                            <td>${opKlatskin.get(i).selIz}</td>
                                                            <td>${opKlatskin.get(i).totIz}</td>
                                                            <td>${opKlatskin.get(i).osl}</td>
                                                            <td>${opKlatskin.get(i).clavien}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 1-->

                        <!--Таблица по операции 2-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-11-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по операции при диагнозе ГЦР
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-11-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl011">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Длит-ть операции</th>
                                                    <th>Вид оператив. лечения</th>
                                                    <th>Объем резекции(Рез.)</th>
                                                    <th>Тип сегмент. резекции(Рез.)</th>
                                                    <th>Число удал. сегментов(Рез.)</th>
                                                    <th>Число очагов(РЧА)</th>
                                                    <th>Размер очагов(РЧА)</th>
                                                    <th>Близость к сосудам(РЧА)</th>
                                                    <th>Полнота РЧА(РЧА)</th>
                                                    <th>Число очагов(Алк.)</th>
                                                    <th>Размер очагов(Алк.)</th>
                                                    <th>Объем кровопотери(мл)</th>
                                                    <th>Гемотрансфузия</th>
                                                    <th>Объем гемотрансфузии(мл)</th>
                                                    <th>Объем плазмы трансфузии(мл)</th>
                                                    <th>Пережатие ГДС</th>
                                                    <th>Пережатие ГДС (мин)</th>
                                                    <th>Пережатие нижней полой вены</th>
                                                    <th>Пережатие нижней полой вены(мин)</th>
                                                    <th>Селективная сосуд. изоляция</th>
                                                    <th>Селективная сосуд. изоляция (мин.)</th>
                                                    <th>Интраоперационная РЧА</th>
                                                    <th>Стадия TNM</th>
                                                    <th>Bypass</th>
                                                    <th>Тотальная сосуд. изоляция</th>
                                                    <th>Селективная сосуд. изоляци</th>
                                                    <th>ТАХЭ после операции</th>
                                                    <th>Осложнения(РЧА)</th>
                                                    <th>Осложнения(Резекция)</th>
                                                    <th>Осложнения(Алк.)</th>
                                                    <th>Clavien-Dindo</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${opgcr != null && opgcr.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${opgcr.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${opgcr.get(i).name}</td>
                                                            <td>${opgcr.get(i).dlitop}</td>
                                                            <td>${opgcr.get(i).oplech}</td>
                                                            <td>${opgcr.get(i).obrezekcii}</td>
                                                            <td>${opgcr.get(i).segrez}</td>
                                                            <td>${opgcr.get(i).chisloUdSeg}</td>
                                                            <td>${opgcr.get(i).chisloOch}</td>
                                                            <td>${opgcr.get(i).razOch}</td>
                                                            <td>${opgcr.get(i).blizSosud}</td>
                                                            <td>${opgcr.get(i).polnRHA}</td>
                                                            <td>${opgcr.get(i).alkChisloOch}</td>
                                                            <td>${opgcr.get(i).alkRazOch}</td>
                                                            <td>${opgcr.get(i).krovopoteria}</td>
                                                            <td>${opgcr.get(i).gemotransfusia}</td>
                                                            <td>${opgcr.get(i).gemotransfusia_ml}</td>
                                                            <td>${opgcr.get(i).plazma}</td>
                                                            <td>${opgcr.get(i).gdc}</td>
                                                            <td>${opgcr.get(i).gdcmin}</td>
                                                            <td>${opgcr.get(i).perPolVeni}</td>
                                                            <td>${opgcr.get(i).perPolVeniMin}</td>
                                                            <td>${opgcr.get(i).selIz}</td>
                                                            <td>${opgcr.get(i).selIzMin}</td>
                                                            <td>${opgcr.get(i).intrRHA}</td>
                                                            <td>${opgcr.get(i).tnm}</td>
                                                            <td>${opgcr.get(i).bypass}</td>
                                                            <td>${opgcr.get(i).totIz}</td>
                                                            <td>${opgcr.get(i).selSosIz}</td>
                                                            <td>${opgcr.get(i).tache}</td>
                                                            <td>${opgcr.get(i).oslRHA}</td>
                                                            <td>${opgcr.get(i).oslRez}</td>
                                                            <td>${opgcr.get(i).oslAlk}</td>
                                                            <td>${opgcr.get(i).clavien}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 2-->
                        <!--Таблица по операции 3-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-12-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по операции при диагнозе МКР
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-12-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl012">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Длительность операции (мин.)</th>
                                                    <th>Кратность резекции печени</th>
                                                    <th>Объем резекции</th>
                                                    <th>Тип сегментарной резекции</th>
                                                    <th>Число удаленных сегментов</th>
                                                    <th>Вариант резекции</th>
                                                    <th>Выполнение лимфодиссекции</th>
                                                    <th>Сочетание с интраоперационным РЧА</th>
                                                    <th>Наличие РЧА до резекции печени</th>
                                                    <th>Симультанная операция</th>
                                                    <th>Реконструктивная операция на толстой кишке</th>
                                                    <th>Пережатие ГДС</th>
                                                    <th>Пережатие ГДС (мин)</th>
                                                    <th>Bypass</th>
                                                    <th>Тотальная сосудистая изоляция</th>
                                                    <th>Селективная сосудистая изоляция</th>
                                                    <th>Осложнения</th>
                                                    <th>Clavien-Dindo</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${opmkr != null && opmkr.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${opmkr.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${opmkr.get(i).name}</td>
                                                            <td>${opmkr.get(i).dlitop}</td>
                                                            <td>${opmkr.get(i).oplech}</td>
                                                            <td>${opmkr.get(i).obrezekcii}</td>
                                                            <td>${opmkr.get(i).segrez}</td>
                                                            <td>${opmkr.get(i).chisloUdSeg}</td>
                                                            <td>${opmkr.get(i).varRezekcii}</td>
                                                            <td>${opmkr.get(i).limfodissekcia}</td>
                                                            <td>${opmkr.get(i).sochIntrRHA}</td>
                                                            <td>${opmkr.get(i).nalRHA}</td>
                                                            <td>${opmkr.get(i).simultOp}</td>
                                                            <td>${opmkr.get(i).rekTKish}</td>
                                                            <td>${opmkr.get(i).gdc}</td>
                                                            <td>${opmkr.get(i).gdcmin}</td>
                                                            <td>${opmkr.get(i).bypass}</td>
                                                            <td>${opmkr.get(i).totIz}</td>
                                                            <td>${opmkr.get(i).selSosIz}</td>
                                                            <td>${opmkr.get(i).osl}</td>
                                                            <td>${opmkr.get(i).clavien}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 3-->
                        <!--Таблица по операции 3-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-13-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по операции при диагнозе ХЦР
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-13-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl013">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Длит-ть операции</th>
                                                    <th>Вид оператив. лечения</th>
                                                    <th>Объем резекции</th>
                                                    <th>Тип сегмент. резекции(Рез.)</th>
                                                    <th>Число удал. сегментов(Рез.)</th>
                                                    <th>Число очагов(РЧА)</th>
                                                    <th>Размер очагов(РЧА)</th>
                                                    <th>Близость к сосудам(РЧА)</th>
                                                    <th>Полнота РЧА(РЧА)</th>
                                                    <th>Объем кровопотери(мл)</th>
                                                    <th>Гемотрансфузия</th>
                                                    <th>Объем гемотрансфузии(мл)</th>
                                                    <th>Объем плазмы трансфузии(мл)</th>
                                                    <th>Пережатие ГДС</th>
                                                    <th>Пережатие ГДС (мин)</th>
                                                    <th>Пережатие нижней полой вены</th>
                                                    <th>Пережатие нижней полой вены(мин)</th>
                                                    <th>Селективная сосуд. изоляция</th>
                                                    <th>Селективная сосуд. изоляция (мин.)</th>
                                                    <th>Интраоперационная РЧА</th>
                                                    <th>Стадия TNM</th>
                                                    <th>Bypass</th>
                                                    <th>Тотальная сосуд. изоляция</th>
                                                    <th>Селективная сосуд. изоляци</th>
                                                    <th>Осложнения</th>
                                                    <th>Clavien-Dindo</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${ophcr != null && ophcr.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${ophcr.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${ophcr.get(i).name}</td>
                                                            <td>${ophcr.get(i).dlitop}</td>
                                                            <td>${ophcr.get(i).oplech}</td>
                                                            <td>${ophcr.get(i).obrezekcii}</td>
                                                            <td>${ophcr.get(i).segrez}</td>
                                                            <td>${ophcr.get(i).chisloUdSeg}</td>
                                                            <td>${ophcr.get(i).chisloOch}</td>
                                                            <td>${ophcr.get(i).razOch}</td>
                                                            <td>${ophcr.get(i).blizSosud}</td>
                                                            <td>${ophcr.get(i).polnRHA}</td>
                                                            <td>${ophcr.get(i).krovopoteria}</td>
                                                            <td>${ophcr.get(i).gemotransfusia}</td>
                                                            <td>${ophcr.get(i).gemotransfusia_ml}</td>
                                                            <td>${ophcr.get(i).plazma}</td>
                                                            <td>${ophcr.get(i).gdc}</td>
                                                            <td>${ophcr.get(i).gdcmin}</td>
                                                            <td>${ophcr.get(i).perPolVeni}</td>
                                                            <td>${ophcr.get(i).perPolVeniMin}</td>
                                                            <td>${ophcr.get(i).selIz}</td>
                                                            <td>${ophcr.get(i).selIzMin}</td>
                                                            <td>${ophcr.get(i).intrRHA}</td>
                                                            <td>${ophcr.get(i).tnm}</td>
                                                            <td>${ophcr.get(i).bypass}</td>
                                                            <td>${ophcr.get(i).totIz}</td>
                                                            <td>${ophcr.get(i).selSosIz}</td>
                                                            <td>${ophcr.get(i).osl}</td>
                                                            <td>${ophcr.get(i).clavien}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 3-->


                        <!--Таблица по операции 3-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-14-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по операции при диагнозе Эхинококкоз
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-14-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl014">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Эхинококкэктомия</th>
                                                    <th>Открытая эхинококкэктомия</th>
                                                    <th>Обработка остаточной полости фиброзной капсул</th>
                                                    <th>Обработки желочного свища</th>
                                                    <th>Хирургическое лечение</th>
                                                    <th>Этапное лечение с предварительной паиротдельным этапом</th>
                                                    <th>Симультанная операция</th>
                                                    <th>Пережатие ГДС</th>
                                                    <th>Пережатие ГДС (мин)</th>
                                                    <th>Bypass</th>
                                                    <th>Тотальная сосудистая изоляция</th>
                                                    <th>Селективная сосудистая изоляция</th>
                                                    <th>Спецефические интраоперационные осложнение</th>
                                                    <th>Осложнение</th>
                                                    <th>Clavien-Dindo</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${opech != null && opech.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${opech.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${opech.get(i).name}</td>
                                                            <td>${opech.get(i).rezekcia}</td>
                                                            <td>${opech.get(i).otkrEch}</td>
                                                            <td>${opech.get(i).obrabotkaFibr}</td>
                                                            <td>${opech.get(i).obrabotkaSvich}</td>
                                                            <td>${opech.get(i).chirurgLech}</td>
                                                            <td>${opech.get(i).etapLech}</td>
                                                            <td>${opech.get(i).simOper}</td>
                                                            <td>${opech.get(i).gdc}</td>
                                                            <td>${opech.get(i).gdcmin}</td>
                                                            <td>${opech.get(i).bypass}</td>
                                                            <td>${opech.get(i).totIz}</td>
                                                            <td>${opech.get(i).selIz}</td>
                                                            <td>${opech.get(i).intraOsl}</td>
                                                            <td>${opech.get(i).osl}</td>
                                                            <td>${opech.get(i).clavien}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Таблица по операции 1-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-15-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по операции при диагнозе Альвиококкоз
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-15-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl015">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Объем резекции печени</th>
                                                    <th>Резекция сосудов</th>
                                                    <th>Резекция воротной вены</th>
                                                    <th>Объем кровопотери (мл)</th>
                                                    <th>Длительность операции (мин.</th>
                                                    <th>Гемотрансфузия</th>
                                                    <th>Объем гемотрансфузии (мл)</th>
                                                    <th>Объем плазмы трансфузии (мл)</th>
                                                    <th>Пережатие ГДС</th>
                                                    <th>Пережатие ГДС (мин</th>
                                                    <th>Формирование ГЕА(1)</th>
                                                    <th>Формирование ГЕА(2)</th>
                                                    <th>Формирование ГЕА(3)</th>
                                                    <th>Радикальность операции</th>
                                                    <th>Лимфодиссекция</th>
                                                    <th>Стадия TNM</th>
                                                    <th>Стадия PNM</th>
                                                    <th>Bypass</th>
                                                    <th>Селективная сосудистая изоляция</th>
                                                    <th>Тотальная сосудистая изоляция</th>
                                                    <th>Резекция диафрагмы</th>
                                                    <th>Осложнения</th>
                                                    <th>Clavien-Dindo</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${opalv != null && opalv.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${opalv.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${opalv.get(i).name}</td>
                                                            <td>${opalv.get(i).rezekcia}</td>
                                                            <td>${opalv.get(i).sosudrezekcia}</td>
                                                            <td>${opalv.get(i).vorotrezekcia}</td>
                                                            <td>${opalv.get(i).krovopoteria}</td>
                                                            <td>${opalv.get(i).dlitop}</td>
                                                            <td>${opalv.get(i).gemotransfusia}</td>
                                                            <td>${opalv.get(i).gemotransfusia_ml}</td>
                                                            <td>${opalv.get(i).plazma}</td>
                                                            <td>${opalv.get(i).gdc}</td>
                                                            <td>${opalv.get(i).gdcmin}</td>
                                                            <td>${opalv.get(i).geatype1}</td>
                                                            <td>${opalv.get(i).geatype2}</td>
                                                            <td>${opalv.get(i).geatype3}</td>
                                                            <td>${opalv.get(i).radikal}</td>
                                                            <td>${opalv.get(i).limfodissekcia}</td>
                                                            <td>${opalv.get(i).tnm}</td>
                                                            <td>${opalv.get(i).pnm}</td>
                                                            <td>${opalv.get(i).bypass}</td>
                                                            <td>${opalv.get(i).selIz}</td>
                                                            <td>${opalv.get(i).totIz}</td>
                                                            <td>${opalv.get(i).rezDiafragmi}</td>
                                                            <td>${opalv.get(i).osl}</td>
                                                            <td>${opalv.get(i).clavien}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по операции 1-->
                        <!--Таблица по операции 3-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-16-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по операции при диагнозе МНКР
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-16-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl016">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Длительность операции (мин.)</th>
                                                    <th>Наличие внепеченочных метастазов</th>
                                                    <th>Стадии первичной опухоли по ТNМ</th>
                                                    <th>Пережатие ГДС</th>
                                                    <th>Пережатие ГДС (мин)</th>
                                                    <th>Bypass</th>
                                                    <th>Тотальная сосудистая изоляция</th>
                                                    <th>Селективная сосудистая изоляция</th>
                                                    <th>Проведение химиотерапии</th>
                                                    <th>Кратность резекции печени</th>
                                                    <th>Объем резекции</th>
                                                    <th>Тип сегментарной резекции</th>
                                                    <th>Число удаленных сегментов</th>
                                                    <th>Вариант резекции</th>
                                                    <th>Выполнение лимфодиссекции</th>
                                                    <th>Сочетание с интраоперационным РЧА</th>
                                                    <th>Наличие РЧА до резекции печени</th>
                                                    <th>Симультанная операция</th>
                                                    <th>Реконструктивная операция на толстой кишке</th>
                                                    <th>Осложнения</th>
                                                    <th>Clavien-Dindo</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${opmnkr != null && opmnkr.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${opmnkr.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${opmnkr.get(i).name}</td>
                                                            <td>${opmnkr.get(i).dlitop}</td>
                                                            <td>${opmnkr.get(i).oplech}</td>
                                                            <td>${opmnkr.get(i).stadTnm}</td>
                                                            <td>${opmnkr.get(i).gdc}</td>
                                                            <td>${opmnkr.get(i).gdcmin}</td>
                                                            <td>${opmnkr.get(i).bypass}</td>
                                                            <td>${opmnkr.get(i).totIz}</td>
                                                            <td>${opmnkr.get(i).selSosIz}</td>
                                                            <td>${opmnkr.get(i).chimiot}</td>
                                                            <td>${opmnkr.get(i).kratRez}</td>
                                                            <td>${opmnkr.get(i).obrezekcii}</td>
                                                            <td>${opmnkr.get(i).segrez}</td>
                                                            <td>${opmnkr.get(i).chisloUdSeg}</td>
                                                            <td>${opmnkr.get(i).varRezekcii}</td>
                                                            <td>${opmnkr.get(i).limfodissekcia}</td>
                                                            <td>${opmnkr.get(i).sochIntraRHA}</td>
                                                            <td>${opmnkr.get(i).nalRHA}</td>
                                                            <td>${opmnkr.get(i).simultOp}</td>
                                                            <td>${opmnkr.get(i).rekOpTKish}</td>
                                                            <td>${opmnkr.get(i).osl}</td>
                                                            <td>${opmnkr.get(i).clavien}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Таблица по анализам-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-17-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по операции при диагнозе рак желудка
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-17-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl017">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Объем резекции печени</th>
                                                    <th>Резекция сосудов</th>
                                                    <th>Резекция воротной вены</th>
                                                    <th>Объем кровопотери (мл)</th>
                                                    <th>Длительность операции (мин.</th>
                                                    <th>Гемотрансфузия</th>
                                                    <th>Объем гемотрансфузии (мл)</th>
                                                    <th>Объем плазмы трансфузии (мл)</th>
                                                    <th>Пережатие ГДС</th>
                                                    <th>Пережатие ГДС (мин</th>
                                                    <th>Формирование ГЕА(1)</th>
                                                    <th>Формирование ГЕА(2)</th>
                                                    <th>Формирование ГЕА(3)</th>
                                                    <th>Радикальность операции</th>
                                                    <th>Лимфодиссекция</th>
                                                    <th>Наличие камней в желчном пузыре</th>
                                                    <th>Наличие ЖКБ</th>
                                                    <th>Bypass</th>
                                                    <th>Тотальная сосудистая изоляция</th>
                                                    <th>Селективная сосудистая изоляция</th>
                                                    <th>Макроскопическая форма рака</th>
                                                    <th>Локализация опухоли в желчном пузыре</th>
                                                    <th>Стадия TNM</th>
                                                    <th>Осложнения</th>
                                                    <th>Clavien-Dindo</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${oprzhp != null && oprzhp.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${oprzhp.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${oprzhp.get(i).name}</td>
                                                            <td>${oprzhp.get(i).rezekcia}</td>
                                                            <td>${oprzhp.get(i).sosudrezekcia}</td>
                                                            <td>${oprzhp.get(i).vorotrezekcia}</td>
                                                            <td>${oprzhp.get(i).krovopoteria}</td>
                                                            <td>${oprzhp.get(i).dlitop}</td>
                                                            <td>${oprzhp.get(i).gemotransfusia}</td>
                                                            <td>${oprzhp.get(i).gemotransfusia_ml}</td>
                                                            <td>${oprzhp.get(i).plazma}</td>
                                                            <td>${oprzhp.get(i).gdc}</td>
                                                            <td>${oprzhp.get(i).gdcmin}</td>
                                                            <td>${oprzhp.get(i).geatype1}</td>
                                                            <td>${oprzhp.get(i).geatype2}</td>
                                                            <td>${oprzhp.get(i).geatype3}</td>
                                                            <td>${oprzhp.get(i).radikal}</td>
                                                            <td>${oprzhp.get(i).limfodissekcia}</td>
                                                            <td>${oprzhp.get(i).kamni}</td>
                                                            <td>${oprzhp.get(i).zhkb}</td>
                                                            <td>${oprzhp.get(i).bypass}</td>
                                                            <td>${oprzhp.get(i).totIz}</td>
                                                            <td>${oprzhp.get(i).selIz}</td>
                                                            <td>${oprzhp.get(i).formRak}</td>
                                                            <td>${oprzhp.get(i).lokalizaciaRaka}</td>
                                                            <td>${oprzhp.get(i).tnm}</td>
                                                            <td>${oprzhp.get(i).osl}</td>
                                                            <td>${oprzhp.get(i).clavien}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Конец таблицы по анализам-->
                        <div class="panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="#sur-18-table" data-toggle="collapse">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            Информация по дренированию
                                        </a>
                                    </h4>
                                </div>
                                <div id="sur-18-table" class="panel-collapse collapse">
                                    <div class="panel-content">
                                        <div class="table-responsive">
                                            <table class="table" id="tbl018">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ФИО</th>
                                                    <th>Билирубин</th>
                                                    <th>Количество наружних дренажей</th>
                                                    <th>Количество наружно-внутренних дренажей</th>
                                                    <th>Уровень блока</th>
                                                    <th>МНО</th>
                                                    <th>Длительность желтухи</th>
                                                    <th>Рециссивный холангит</th>
                                                    <th>Абсцедирующий холангит</th>
                                                    <th>Дренированная доля</th>
                                                    <th>Сворачивание дренажа в кольцо</th>
                                                    <th>Дренирование 6-7 сегментов</th>
                                                    <th>Дренирование 5-8 сегментов</th>
                                                    <th>Супрапапилярный дренаж</th>
                                                    <th>Инфицированный посев желчи</th>
                                                    <th>Клебсиелла пневмонии</th>
                                                    <th>Синегнойная палочка</th>
                                                    <th>Инфицированный посев крови</th>
                                                    <th>Осложнения</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:if test="${dren != null && dren.size() > 0}">
                                                    <c:forEach var="i" begin="0" end="${dren.size()-1}">
                                                        <tr>
                                                            <td>${i+1}</td>
                                                            <td>${dren.get(i).name}</td>
                                                            <td>${dren.get(i).bilirubin}</td>
                                                            <td>${dren.get(i).out_drenages}</td>
                                                            <td>${dren.get(i).inout_drenages}</td>
                                                            <td>${dren.get(i).block}</td>
                                                            <td>${dren.get(i).mno}</td>
                                                            <td>${dren.get(i).longevity}</td>
                                                            <td>${dren.get(i).previous_cholangit}</td>
                                                            <td>${dren.get(i).absces_cholangit}</td>
                                                            <td>${dren.get(i).dolya}</td>
                                                            <td>${dren.get(i).svoracivanie}</td>
                                                            <td>${dren.get(i).segment67}</td>
                                                            <td>${dren.get(i).segment58}</td>
                                                            <td>${dren.get(i).suprapapil}</td>
                                                            <td>${dren.get(i).zh_posev}</td>
                                                            <td>${dren.get(i).klebsiela}</td>
                                                            <td>${dren.get(i).sinegnoin_palochka}</td>
                                                            <td>${dren.get(i).kr_posev}</td>
                                                            <c:if test='${dren.get(i).complications.equals("1")}'>
                                                                <td bgcolor="#db8989"></td>
                                                            </c:if>
                                                            <c:if test='${dren.get(i).complications.equals("0")}'>
                                                                <td bgcolor="#abecbd"></td>
                                                            </c:if>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("[data-toggle='tooltip']").tooltip();
    });
</script>
</body>
</html>