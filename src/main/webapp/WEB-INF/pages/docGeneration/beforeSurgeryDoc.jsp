<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Предоперационный эпикриз</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/datepicker.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.buttonRow):not(.unnecessaryRow):before {
            content: "*";
            color: red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <c:choose>
                <c:when test="${patient.id != null && !patient.id.isEmpty()}">
                    <a href="../patients/update/${patient.id}">
                        <ins>На страницу пациента</ins>
                    </a>
                </c:when>
                <c:otherwise>
                    <a href="welcome">
                        <ins>На основную страницу</ins>
                    </a>
                </c:otherwise>
            </c:choose>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Предоперационный эпикриз. ${diagnosis}</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveGenInfo?${_csrf.parameterName}=${_csrf.token}" var="patientInfoUrl"/>
                <form:form action="${patientInfoUrl}" modelAttribute="patient" method="post" id="patientInfoForm">

                    <!--details-->

                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="date">Дата:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="date">
                                <div class="${status.error ? 'has-error' : ''}">
                                    <div class='input-group date'>
                                        <form:input type='text'
                                                    class="form-control" path="date" id='date'
                                                    aria-describedby="addon"/>
                            <span class="input-group-addon" id="addon"><i
                                    class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                    <form:errors path="date" class="control-label"/>
                                </div>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="complaints">Жалобы:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="complaints">
                                <form:textarea class="form-control" path="complaints"
                                               rows="5"></form:textarea>
                                <form:errors path="complaints" class="control-label"/>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="diseaseHistory">История заболевания:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="diseaseHistory">
                                <form:textarea class="form-control" path="diseaseHistory"
                                               rows="10"></form:textarea>
                                <form:errors path="diseaseHistory" class="control-label"/>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="obsledovanie">Обследован${(patient.sex == 0) ?"а" : ""} в отделении:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="obsledovanie">
                                <form:textarea class="form-control" path="obsledovanie" rows="10"></form:textarea>
                                <form:errors path="obsledovanie" class="control-label"/>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="konsilium">Онкологический консилиум:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="konsilium">
                                <form:textarea class="form-control" path="konsilium" rows="5"></form:textarea>
                                <form:errors path="konsilium" class="control-label"/>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="additionalDisease">Сопутствующие заболевания:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="additionalDisease">
                                <form:textarea class="form-control" path="additionalDisease" rows="5"></form:textarea>
                                <form:errors path="additionalDisease" class="control-label"/>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="inspection">Данные объективного просмотра:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="inspection">
                                <form:textarea class="form-control" path="inspection" rows="5"></form:textarea>
                                <form:errors path="inspection" class="control-label"/>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="clinical">Клинический диагноз:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="clinical">
                                <form:textarea class="form-control" path="clinical" rows="5"></form:textarea>
                                <form:errors path="clinical" class="control-label"/>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="conception">Лечебная концепция:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="conception">
                                <form:textarea class="form-control" path="conception" rows="5"></form:textarea>
                                <form:errors path="conception" class="control-label"/>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Сохранить</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>