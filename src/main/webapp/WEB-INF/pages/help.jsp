<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<jsp:useBean id="now" class="java.util.Date"/>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">
    <title>Помощь</title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/dashboard.css"/>" rel="stylesheet">
    <link rel="stylesheet" href="<c:url value="/resources/js/SyntaxHighlighter/styles/SyntaxHighlighter.css"/>"/>
    <link href="https://fonts.googleapis.com/css?family=Arimo" rel="stylesheet">
    <!--script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script-->
    <script src="<c:url value="/resources/js/jquery-3.1.0.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery-1.9.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tooltip.js"/>"></script>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<style>
    .badge-notify {
        background: red;
        position: relative;
        top: -40%;
        left: -20%;
    }

    .panel-heading .accordion-toggle:after {
        /* symbol for "opening" panels */
        font-family: 'Glyphicons Halflings'; /* essential for enabling glyphicon */
        content: "\e114"; /* adjust as needed, taken from bootstrap.css */
        float: right; /* adjust as needed */
        color: grey; /* adjust as needed */
    }

    .panel-heading .accordion-toggle.collapsed:after {
        /* symbol for "collapsed" panels */
        content: "\e080"; /* adjust as needed, taken from bootstrap.css */
    }

    .sidebar ul.nav li.divider {
        border-bottom: 1px solid #eee;
        margin: 20px 0;
    }

    h3, h4, .panel-body ol li, .panel-body ul li, p {
        font-family: 'Arimo', sans-serif;
    }

    h4 {
        margin-bottom: 20px;
        margin-top: 20px;
        color: darkblue;
    }

    .panel-body ol li, .panel-body ul li, p {
        margin-bottom: 2px;
        margin-top: 2px;
        font-size: 16px;
    }

</style>

<nav class="navbar navbar-inverse navbar-fixed-top" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-brand">
            Помощь
        </a>
    </div>
    <ul class="nav navbar-nav navbar-right" style="margin-right: 3%">
        <li>
            <a href="edit_account/${u.userId}">
                <i class="fa fa-cog fa-inverse fa-fw" aria-hidden="true"></i>Настройки
            </a>
        </li>
    </ul>
</nav>
<div class="container-fluid">
    <div class="row ">
        <!--Doctor Profile-->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 sidebar">
            <ul class="nav nav-sidebar">
                <li>
                    <div class="photo-container">
                        <c:if test="${u.getPhoto()!=null}">
                            <p>
                                <img src="<c:url value="/resources/images/${u.getPhoto()}"/>">
                            </p>
                        </c:if>
                        <c:if test="${u.getPhoto()==null}">
                            <p>
                                <img src="<c:url value="/resources/images/default_icon.png"/>">
                            </p>
                        </c:if>
                    </div>
                </li>
                <li>
                    <i class="fa fa-user-md"></i><u>${u.getUserName()}</u>
                </li>
                <li>${u.getEmail()}</li>
                <li><a href="welcome">Пациенты</a></li>
                <li><a href="generalInfo">Новый пациент</a></li>
                <li>
                    <a href="statistics">Статистика</a>
                </li>
                <li role="presentation" class="divider"></li>
                <li сlass="active"><a href="help">Помощь<span class="sr-only">(current)</span></a></li>
                <c:url value="/j_spring_security_logout" var="logoutUrl"/>
                <li><a href="javascript:formSubmit()"><i class="fa fa-sign-out fa-fw"></i> Выход</a></li>
                <form action="${logoutUrl}" method="post" id="logoutForm">
                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                </form>
            </ul>
        </div>
        <script>
            function formSubmit() {
                document.getElementById("logoutForm").submit();
            }
        </script>
    </div>
</div>

<div class="container-fluid">
    <div class="col-lg-offset-2 col-xs-offset-3 col-lg-10 col-md-10 col-sm-10 col-xs-9">
        <div class="panel-group" id="accordion">
            <!--Patient creation and edition-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Создание и редактирование пациента
                        </a>
                    </h3>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <h4>Создание нового пациента</h4>
                        <ul>
                            <li>Для создания нового пациента на основной странице нажмите на вкладку <b>"Новый
                                пациент"</b>.
                            </li>
                            <li>Программа создаст нового пациента, если будут заполнены следующие поля:
                                <ol>
                                    <li>ФИО,</li>
                                    <li>Пол,</li>
                                    <li>Дата рождения,</li>
                                    <li>Диагноз при поступлении,</li>
                                    <li>Номер истории болезни,</li>
                                    <li>Дата госпитализации,</li>
                                    <li>Если в пункте "Проведение операции" указано <b>Да</b>, то необходимо указать
                                        дату
                                        операции.
                                    </li>
                                </ol>
                            </li>
                        </ul>
                        <h4>Редактирование/заполнение информации о пациенте</h4>
                        <ul>
                            <li>Для заполнения/редактирования информации о пациенте необходимо выбрать соответствующего
                                пациента в таблице <b>"Пациенты"</b> на основной странице.
                            </li>
                            <li>
                                Информация о пациенте разделена на несколько блоков: <b>"До операции"</b>,
                                <b>"Операция"</b>,
                                <b>"Дренирующие операции"</b>, <b>"Осложнения"</b>, <b>"Выписка"</b>. Каждый блок
                                содержит в себе разделы (к примеру, блок "До операции" включает в себя следующие
                                разделы: "Общая информация", "Лабораторные данные(поступление), "Данные объективного
                                осмотра",
                                "Данные о диагнозе"), которые должны быть заполнены пользователем.
                            </li>
                            <li>
                                Блок <b>"До операции"</b> доступен пользователю для редактирования в течение <b>3-х
                                дней</b>
                                после создания пациента. По истечении данного строка возможность редактирования
                                информации в данном
                                блоке
                                будет недоступна.
                            </li>
                            <li>
                                Все остальные блоки открыты для редактирования, пока пользователь не вводит <b>дату
                                выписки</b>
                                в блоке "Выписка". После этого, в случае, если дата выписки уже прошла, система позволит
                                пользователю редактировать/вносить информацию в течение <b>1-го дня</b>. Если дата
                                выписки
                                сегодня
                                или еще не наступила, функция изменения информации о пользователе перестанет быть
                                доступна по прошествии <b>3-х дней с момента выписки</b>.
                            </li>
                            <li>
                                <b>Дата смерти</b> всегда доступна для ввода/редактирования.
                            </li>
                            <li>
                                Система показывает пользователю, заполнен ли раздел в блоке с информацией о пациенте. В
                                случае,
                                если все обязательные поля были заполнены, около названия раздела появляется пометка
                                <i class="fa fa-check"></i>. Разделы, не обязательные для заполнения, помечены следующим
                                знаком: <i class="fa fa-check-square-o" aria-hidden="true"></i>.
                            </li>
                            <li>
                                В случае, если все разделы помечены знаком <i class="fa fa-check"></i>, блок считается
                                заполненным.
                            </li>
                            <li>
                                Каждый раз при открытии основной страницы пользователем, система напоминает о
                                недозаполненной информации о пациенте. При этом указывается ФИО пациента, ответственный
                                за вводимую информацию, оставшееся для дозаполнения количество дней,
                                а также какая часть информации считается недовведенной. <b>"До операции"</b> включает в
                                себя
                                только блок "До операции", <b>"Ведение больного"</b> - все оставшиеся блоки.
                            </li>
                        </ul>
                        <h4>Другой диагноз</h4>
                        <ul>
                            <li>
                                У пользователя есть возможность выбрать "Другой диагноз" при создании пациента.
                            </li>
                            <li>
                                В данном случае пользователь будут недоступны следующие разделы: "Данные о диагнозе",
                                "Данные об операции", "Осложнения после основной операции", "Двухэтапное лечение".
                            </li>
                        </ul>
                        <h4>Двухэтапное лечение</h4>
                        <ul>
                            <li>
                                При лечении пациента может быть выбрано проведение двухэтапного лечения. Тогда в разделе
                                "Двухэтапное лечение" необходимо выбрать <b>"Проведение двухэтапного лечения"</b> и
                                заполнить
                                соответствующие поля в данном разделе.
                            </li>
                            <li>
                                При выборе ведения двухэтапного лечения, при заполнении раздела "Данные об операции"
                                появятся дополнительные поля для ввода информцации
                                о результатах первого этапа лечения (осложнения после первого этапа и информация о
                                гипертрофии остатка печени).
                            </li>
                        </ul>
                        <h4>Осложнения</h4>
                        <ul>
                            <li>
                                При возникновении осложнений у пациента необходимо внести данные в блок "Осложнения".
                            </li>
                            <li>
                                Если осложнения произошли после проведения основной операции по диагнозу, необходимо
                                выбрать раздел <b>"Осложнения после основной операции"</b> для ввода данной информации.
                            </li>
                            <li>
                                Также осложнения могут возникнуть после дренирующих операций. Раздел <b>"Осложнения
                                после дренирующих операций"</b> появится в том случае, если была введена хотя бы одна из
                                дренирующих операций. При нажатии на этот раздел откроется страница со списком всех
                                дренирующих операций, проведенных этому пациенту. Пользователь должен выбрать
                                соответствующую операцию для ввода осложнений по ней.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--Patient creation and edition-->
            <!--Patient documenting-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Документы
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <h4>Блок "Документы" на странице пациента</h4>
                        <ul>
                            <li>Страница пациента содержит блок "Документы". Данный блок содержит все файлы,
                                прикрепленные
                                к данному пациенту.
                            </li>
                            <li>Файл пациента добавляется в систему при его отправке на почту системы. Система проверяет
                                свою почту и добавляет файлы из новых (непрочитанных) писем к пациенту. Данная
                                функциональность позволяет хранить все файлы (файлы могут быть получены из любого
                                отделения больницы), связанные с пациентом, в одной системе.
                            </li>
                            <li>Любой файл может быть скачан пользователем на компьютер при нажатии на этот файл.</li>
                            <li>
                                Для отправки файла системе необходимо создать письмо, тема которого будет выглядеть
                                следующим образом: "название исследования" "номер истории болезни пациента, к которому
                                добавляется файл". Необходимо прикрепить файл и в графе "получатель" указать адрес
                                <b>mkncdoccollector@gmail.com</b>.
                            </li>
                        </ul>
                        <h4>Генерация документов пациента</h4>
                        <p>На данный момент ведется разработка дополнительного функционала программы, позволяющего
                            создавать документы, входящие в историю болезни пациента (к примеру, "Предоперационный
                            эпикриз", "Осмотр при поступлении").</p>
                    </div>
                </div>
            </div>
            <!--Patient documenting-->
            <!--Settings-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                           href="#collapseThree">
                            Найстройки пользователя
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">

                    </div>
                </div>
            </div>
            <!--Settings-->
        </div>
    </div>
</div>
</body>
</html>