<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Опухоль Клацкина</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.scintigrafia):not(.buttonRow):not(.section-header):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../../patients/update/${klatskin.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные о диагнозе "Опухоль Клацкина"
                    ${(klatskin.diagnosisAtStart)?" (поступление)":" (выписка)"}</h4>
            </div>
            <div class="modal-body">
            <c:url value="/saveKlatskin?${_csrf.parameterName}=${_csrf.token}" var="klatskinUrl"/>
            <form:form action="${klatskinUrl}" modelAttribute="klatskin" method="post"
                       id="klatskinForm">

                <spring:bind path="id">
                    <form:hidden path="id"/>
                </spring:bind>

                <spring:bind path="isNew">
                    <form:hidden path="isNew"/>
                </spring:bind>


                <spring:bind path="diagnosisAtStart">
                    <form:hidden path="diagnosisAtStart"/>
                </spring:bind>
                <input style="display: none" value="${expired}" id="expiredValue">
                <input style="display: none" value="${isAdmin}" id="isAdminValue">
                <c:if test="${klatskin.diagnosisAtStart == true}">
                    <div class="form-group">
                        <div class="row section-header">
                            <h4 class="section-heading">Сцинтиграфия</h4>
                            <hr>
                        </div>
                        <spring:bind path="indexZachvata">
                            <div class="row scintigrafia">
                                <div class="col-xs-3">
                                    <label for="index_zachvata">Стандартный объем печени, см^3:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:input step="any"
                                                id="index_zachvata" path="indexZachvata" class="form-control"
                                                type="number"/>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="objemOstatka">
                            <div class="row scintigrafia">
                                <div class="col-xs-3">
                                    <label for="ob_ostatka">Объем будущего остатка печени по данным КТ %(число),
                                        см^3:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:input id="ob_ostatka"
                                                path="objemOstatka" class="form-control" type="number" step="any"/>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="rezekciaType">
                            <div class="row scintigrafia">
                                <div class="col-xs-3">
                                    <label for="rezekciaType">Планируемый объем операции:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="rezekciaType" path="rezekciaType">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Сегментарная</form:option>
                                        <form:option value="2">Обширная</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                </c:if>
                <spring:bind path="bismuth">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="bismuth">Тип опухоли по Бисмуту:</label>
                        </div>
                        <div class="col-xs-9">
                            <form:select
                                    class="form-control" id="bismuth" path="bismuth">
                                <form:option value="0">---------</form:option>
                                <form:option value="1">I</form:option>
                                <form:option value="2">II</form:option>
                                <form:option value="3">IIIa</form:option>
                                <form:option value="4">IIIb</form:option>
                                <form:option value="5">IV</form:option>
                            </form:select>
                        </div>
                    </div>
                </spring:bind>
                <spring:bind path="vorotInvasia">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="vorot_invasia">Наличие инвазии в воротную вену:</label>
                        </div>
                        <div class="col-xs-9">
                            <form:select
                                    class="form-control" id="vorot_invasia" path="vorotInvasia">
                                <form:option value="0">---------</form:option>
                                <form:option value="1">В ствол</form:option>
                                <form:option value="2">В долевые ветви</form:option>
                                <form:option value="3">В сегментарные ветв</form:option>
                            </form:select>
                        </div>
                    </div>
                </spring:bind>
                <spring:bind path="grwr">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="grwr"> Коэффициент GRWR:</label>
                        </div>
                        <div class="col-xs-9">
                            <form:select
                                    class="form-control" id="grwr" path="grwr">
                                <form:option value="0">---------</form:option>
                                <form:option value="1">Меньше 0.8</form:option>
                                <form:option value="2">0.8 – 1</form:option>
                                <form:option value="3">Больше 1</form:option>
                            </form:select>
                        </div>
                    </div>
                </spring:bind>
                <spring:bind path="icg">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="icg">Коэффициент элиминации ICG:</label>
                        </div>
                        <div class="col-xs-9">
                            <form:select
                                    class="form-control" id="icg" path="icg">
                                <form:option value="0">---------</form:option>
                                <form:option value="1">Меньше 15%</form:option>
                                <form:option value="2">15-25 %</form:option>
                                <form:option value="3">Больше 25%</form:option>
                            </form:select>
                        </div>
                    </div>
                </spring:bind>
                <spring:bind path="icgk_f">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="icgkf">Коэффициент ICGK-F:</label>
                        </div>
                        <div class="col-xs-9">
                            <form:select
                                    class="form-control" id="icgkf" path="icgk_f">
                                <form:option value="0">---------</form:option>
                                <form:option value="1">Меньше 0.4</form:option>
                                <form:option value="2">0.4 - 0.6</form:option>
                                <form:option value="3">Больше 0.6</form:option>
                            </form:select>
                        </div>
                    </div>
                </spring:bind>
                <spring:bind path="pechInvasia">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="pech_invasia">Наличие инвазии в печеночную артерию:</label>
                        </div>
                        <div class="col-xs-9">
                            <form:select
                                    class="form-control" id="pech_invasia" path="pechInvasia">
                                <form:option value="0">---------</form:option>
                                <form:option value="1">Ипсилатеральную</form:option>
                                <form:option value="2">Контрлатеральную</form:option>
                                <form:option value="3">В собственную печеночную</form:option>
                            </form:select>
                        </div>
                    </div>
                </spring:bind>
                <spring:bind path="differOpuchol">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="differen">Степень дифференцировки опухоли:</label>
                        </div>
                        <div class="col-xs-9">
                            <form:select
                                    class="form-control" id="differen" path="differOpuchol">
                                <form:option value="0">---------</form:option>
                                <form:option value="2">Низкая</form:option>
                                <form:option value="3">Умеренная</form:option>
                                <form:option value="4">Высокая</form:option>
                            </form:select>
                        </div>
                    </div>
                </spring:bind>
                <c:if test="${klatskin.diagnosisAtStart == true}">
                    <spring:bind path="razmer_opucholi">
                        <div class="row" id="razmer_opucholiGroup">
                            <div class="col-xs-3">
                                <label for="razmer_opucholi">Размер опухоли:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="razmer_opucholi" path="razmer_opucholi" class="form-control"
                                        type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                </c:if>
                <div class="row">
                    <div class="col-xs-12">
                        <spring:bind path="perinevInvasia">
                            <form:checkbox
                                    path="perinevInvasia"/><b>Наличие периневральной инвазии</b>
                        </spring:bind>
                    </div>
                </div>

                <c:if test="${klatskin.diagnosisAtStart == true}">
                    <div class="row">
                        <div class="col-xs-12">
                            <spring:bind path="polozhKultura">
                                <form:checkbox
                                        path="polozhKultura"/><b>Положительная культура из желчи до операции</b>
                            </spring:bind>
                        </div>
                    </div>
                    <spring:bind path="trofStatus">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="trof_status">Оценка трофического статуса до
                                    операциии:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="trof_status"
                                        path="trofStatus">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Хороший</form:option>
                                    <form:option value="2">Удовлетворительный</form:option>
                                    <form:option
                                            value="3">Неудовлетворительный</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                </c:if>
                <div class="row buttonRow">
                    <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                </div>
            </form:form>
        </div>
    </div>
</div>
</div>
<script>
    $(document).ready(function () {
        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            var c = document.getElementById("klatskinForm").elements;
            for (var i = 0; i < c.length; i++) {
                c[i].setAttribute("disabled", true);
            }
        }
    });
</script>
</body>
</html>