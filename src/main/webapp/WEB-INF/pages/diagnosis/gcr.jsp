<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>ГЦР</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.scintigrafia):not(.buttonRow):not(.section-header):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../../patients/update/${gcr.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные о диагнозе "ГЦР"
                    ${(gcr.diagnosisAtStart)?" (поступление)":" (выписка)"}</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveGcr?${_csrf.parameterName}=${_csrf.token}" var="gcrUrl"/>
                <form:form action="${gcrUrl}" modelAttribute="gcr" method="post"
                           id="gcrForm">

                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>


                    <spring:bind path="diagnosisAtStart">
                        <form:hidden path="diagnosisAtStart"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="cirrozClass">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="cirroz_class">Цирроз по СТР:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="cirrozClass" id="cirroz_class">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">нет</form:option>
                                    <form:option value="2">A</form:option>
                                    <form:option value="3">B</form:option>
                                    <form:option value="4">C</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="vrvStepen">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="vrv">Наличие ВРВ пищевода и желудка :</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="vrvStepen" id="vrv">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">0</form:option>
                                    <form:option value="2">1</form:option>
                                    <form:option value="3">2</form:option>
                                    <form:option value="4">3</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="trombozStepen">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="trombocit">Наличие тромбоцитопении:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="trombozStepen" id="trombocit">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Легкая (>100)</form:option>
                                    <form:option value="3">Средняя (50-100)</form:option>
                                    <form:option value="4">Тяжелая (< 50)</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="etiologiaCirroza">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="etiologia">Этиология цирроза:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="etiologiaCirroza" id="etiologia">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Гепатит B</form:option>
                                    <form:option value="2">Гипатит С</form:option>
                                    <form:option value="3">Алкогольный</form:option>
                                    <form:option value="4">Другой этиологии</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="gepatitStepen">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="vir_gepatit">Наличие вирусного гепатита:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="gepatitStepen" id="vir_gepatit">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Гепатит В</form:option>
                                    <form:option value="3">Гепатит С</form:option>
                                    <form:option value="4">Гепатит В+С</form:option>
                                    <form:option value="5">Гепатит В+D</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="fibrozClass">
                        <div class="row" id="metavir_group">
                            <div class="col-xs-3">
                                <label for="metavir"> Фиброз печени (по metavir):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="fibrozClass" id="metavir">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">нет</form:option>
                                    <form:option value="2">f0</form:option>
                                    <form:option value="3">f1</form:option>
                                    <form:option value="4">f2</form:option>
                                    <form:option value="5">f3</form:option>
                                    <form:option value="6">f4</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="razmerOpucholi">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="razmer_opucholi">Максимальный размер опухоли(см):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="razmerOpucholi" id="razmer_opucholi">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Меньше 10 см</form:option>
                                    <form:option value="2">Больше 10 см</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="opuchUzliType">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="type_opuch_uzliGCR">Тип опухолевых узлов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="opuchUzliType" id="type_opuch_uzliGCR">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Солитарный</form:option>
                                    <form:option value="2">Множестевенные</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="chisloOpuchUzlov">
                        <div class="row" id="opuch_uzli_groupGCR" style="display: none;">
                            <div class="col-xs-3">
                                <label for="opuch_uzliGCR">Число опухолевых узлов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="chisloOpuchUzlov" id="opuch_uzliGCR">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">0</form:option>
                                    <form:option value="2">1</form:option>
                                    <form:option value="3">2</form:option>
                                    <form:option value="4">3</form:option>
                                    <form:option value="5">4</form:option>
                                    <form:option value="6">5</form:option>
                                    <form:option value="7">Больше 5</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="sosudInvasia">
                        <div class="row" id="sosud_invasia_group">
                            <div class="col-xs-3">
                                <label for="type_sosud_inv">Сосудистая инвазия:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="sosudInvasia" id="type_sosud_inv">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Микрососудистая инвазия</form:option>
                                    <form:option value="3">Макрососудистая инвазия</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="vnepOchagiType">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="vnep_ocagi"> Наличие внепеченочных очагов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="vnepOchagiType" id="vnep_ocagi">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Отдаленные метастазы</form:option>
                                    <form:option value="3">Прорастание соседних органов</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="trombPechType">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="tromb_pech_ven"> Наличие тромбоза печеночных вен:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="trombPechType" id="tromb_pech_ven">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Да</form:option>
                                    <form:option value="3">С выходом в нижнюю полую вену</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="icg">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="icg">Коэффициент элиминации ICG:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="icg" id="icg">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Меньше 15%</form:option>
                                    <form:option value="2">15-25 %</form:option>
                                    <form:option value="3">Больше 25%</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="icgk_f">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="icgkf">Коэффициент ICGK-F:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="icgk_f" id="icgkf">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Меньше 0.4</form:option>
                                    <form:option value="2">0.4 - 0.6</form:option>
                                    <form:option value="3">Больше 0.6</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <c:if test="${gcr.diagnosisAtStart == true}">
                    <div class="form-group">
                        <div class="row section-header">
                            <h4 class="section-heading">Сцинтиграфия</h4>
                            <hr>
                        </div>
                        <spring:bind path="indexZachvata">
                            <div class="row scintigrafia">
                                <div class="col-xs-3">
                                    <label for="index_zachvata">Стандартный объем печени, см^3:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:input
                                            id="index_zachvata" path="indexZachvata" class="form-control"
                                            type="number" step="any"/>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="objemOstatka">
                            <div class="row scintigrafia">
                                <div class="col-xs-3">
                                    <label for="ob_ostatka">Объем будущего остатка печени по данным КТ %(число), см^3:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:input id="ob_ostatka"
                                                path="objemOstatka" class="form-control" type="number" step="any"/>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="rezekciaType">
                            <div class="row scintigrafia">
                                <div class="col-xs-3">
                                    <label for="rezekciaType">Планируемый объем операции:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="rezekciaType" path="rezekciaType">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Сегментарная</form:option>
                                        <form:option value="2">Обширная</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                    <spring:bind path="protivTerapia">
                        <div class="row">
                            <div class="col-xs-12">
                                <form:checkbox
                                        path="protivTerapia"/><b>Наличие противовирусной терапии до операции</b>
                            </div>
                        </div>
                    </spring:bind>
                    </c:if>
                    <spring:bind path="trombozVorotVeni">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="tromb_vor_ven">Тромбоз воротной вены:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="trombozVorotVeni" id="tromb_vor_ven">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">0</form:option>
                                    <form:option value="2">1</form:option>
                                    <form:option value="3">2</form:option>
                                    <form:option value="4">3</form:option>
                                    <form:option value="5">4</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>

                    <c:if test="${gcr.diagnosisAtStart == true}">
                    <div class="row">
                        <div class="col-xs-12">
                            <spring:bind path="lechenieSorafinib">
                                <form:checkbox
                                        path="lechenieSorafinib"/><b>Наличие лечения сорафенибом до операции</b>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <spring:bind path="tache">
                                <form:checkbox path="tache"/><b>ТАХЭ до операции</b>
                            </spring:bind>
                        </div>
                    </div>
                    </c:if>
                    <spring:bind path="bclcType">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="bclc">Тип опухоли по BCLC:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="bclcType" id="bclc">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">A</form:option>
                                    <form:option value="2">B</form:option>
                                    <form:option value="3">C</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <c:if test="${gcr.diagnosisAtStart == true}">
                    <spring:bind path="trofStatus">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="trof_status">Оценка трофического статуса до операциии:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="trofStatus" id="trof_status">
                                    <form:option value="0">----</form:option>
                                    <form:option value="1">Хороший</form:option>
                                    <form:option value="2">Удовлетворительный</form:option>
                                    <form:option value="3">Неудовлетворительный</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    </c:if>

                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>


                    <script>
                        $(document).on("change", "#type_opuch_uzliGCR", function (e) {
                            var selectedVal = $(this).find("option:selected").val();
                            switch (selectedVal) {
                                case "2":
                                    document.getElementById("opuch_uzli_groupGCR").removeAttribute("style");
                                    break;
                                default:
                                    document.getElementById("opuch_uzli_groupGCR").setAttribute("style", "display:none;");
                                    document.getElementById("opuch_uzliGCR").value = 0;
                                    break;
                            }
                        });

                        var selectedVal = $("#type_opuch_uzliGCR").find("option:selected").val();
                        if (selectedVal == "2") {
                            document.getElementById("opuch_uzli_groupGCR").removeAttribute("style");
                        }

                        $(document).ready(function() {
                            var expired = document.getElementById("expiredValue").value;
                            var isAdmin = document.getElementById("isAdminValue").value;
                            if (expired === "true" && isAdmin === "false") {
                                var c = document.getElementById("gcrForm").elements;
                                for (var i = 0; i < c.length; i++) {
                                    c[i].setAttribute("disabled", true);
                                }
                            }
                        });
                    </script>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

