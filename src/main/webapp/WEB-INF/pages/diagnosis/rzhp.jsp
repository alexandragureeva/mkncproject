<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Рак желудка</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.scintigrafia):not(.buttonRow):not(.section-header):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../../patients/update/${rzhp.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные о диагнозе "Рак желудка"
                    ${(rzhp.diagnosisAtStart)?" (поступление)":" (выписка)"}</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveRzhp?${_csrf.parameterName}=${_csrf.token}" var="rzhpUrl"/>
                <form:form action="${rzhpUrl}" modelAttribute="rzhp" method="post"
                           id="rzhpForm">

                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>


                    <spring:bind path="diagnosisAtStart">
                        <form:hidden path="diagnosisAtStart"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="stadPervOpuch">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="stadPervOpuch">Первичная опухоль:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="stadPervOpuch" id="stadPervOpuch">
                                    <form:option value="0">---------</form:option>
                                    <optgroup label="Стадия I">
                                        <form:option value="1">T1N0M0</form:option>
                                        <form:option value="2">T2N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIА">
                                        <form:option value="3">T3N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIB">
                                        <form:option value="4">T4аN0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIC">
                                        <form:option value="5">T4bN0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIA">
                                        <form:option value="6">T1N1M0</form:option>
                                        <form:option value="7">T2N1M0</form:option>
                                        <form:option value="8">T1N2aM0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIB">
                                        <form:option value="9">T3N1M0</form:option>
                                        <form:option value="10">T4aN1M0</form:option>
                                        <form:option value="11">T2N2aM0</form:option>
                                        <form:option value="12">T3N2aM0</form:option>
                                        <form:option value="13">T1N2bM0</form:option>
                                        <form:option value="14">T2N2bM0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIC">
                                        <form:option value="15">T4aN2aM0</form:option>
                                        <form:option value="16">T3N2bM0</form:option>
                                        <form:option value="17">T4aN2bM0</form:option>
                                        <form:option value="18">T4bN1M0</form:option>
                                        <form:option value="19">T4bN2M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVa">
                                        <form:option value="20">Любая T, любая N, M1a</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVb">
                                        <form:option value="21">Любая T, любая N, M1b</form:option>
                                    </optgroup>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="differOpuchol">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="differen">Степень дифференцировки опухоли:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="differen" path="differOpuchol">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Низкая</form:option>
                                    <form:option value="2">Умеренная</form:option>
                                    <form:option value="3">Высокая</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="row">
                        <div class="col-xs-12">
                            <spring:bind path="perinevInvasia">
                                <form:checkbox path="perinevInvasia"/><b>Наличие периневральной инвазии</b>
                            </spring:bind>
                        </div>
                    </div>
                    <c:if test="${rzhp.diagnosisAtStart == true}">
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="polozhKultura">
                                    <form:checkbox
                                            path="polozhKultura"/><b>Положительная культура из желчи до операции</b>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row section-header">
                                <h4 class="section-heading">Сцинтиграфия</h4>
                                <hr>
                            </div>
                            <spring:bind path="indexZachvata">
                                <div class="row scintigrafia">
                                    <div class="col-xs-3">
                                        <label for="index_zachvata">Стандартный объем печени, см^3:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:input step="any"
                                                    id="index_zachvata" path="indexZachvata" class="form-control"
                                                    type="number"/>
                                    </div>
                                </div>
                            </spring:bind>
                            <spring:bind path="objemOstatka">
                                <div class="row scintigrafia">
                                    <div class="col-xs-3">
                                        <label for="ob_ostatka">Объем будущего остатка печени по данным КТ %(число),
                                            см^3:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:input id="ob_ostatka" step="any"
                                                    path="objemOstatka" class="form-control" type="number"/>
                                    </div>
                                </div>
                            </spring:bind>
                            <spring:bind path="rezekciaType">
                                <div class="row scintigrafia">
                                    <div class="col-xs-3">
                                        <label for="rezekciaType">Планируемый объем операции:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="rezekciaType" path="rezekciaType">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">Сегментарная</form:option>
                                            <form:option value="2">Обширная</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </c:if>
                    <spring:bind path="vorotInvasia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="vorot_invasia">Наличие инвазии в воротную вену:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="vorot_invasia" path="vorotInvasia">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">В ствол</form:option>
                                    <form:option value="2">В долевые ветви</form:option>
                                    <form:option value="3">В сегментарные ветв</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="pechInvasia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="pech_invasia">Наличие инвазии в печеночную артерию:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="pech_invasia" path="pechInvasia">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Ипсилатеральную</form:option>
                                    <form:option value="2">Контрлатеральную</form:option>
                                    <form:option value="3">В собственную печеночную</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="icg">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="icg">Коэффициент элиминации ICG:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="icg" path="icg">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Меньше 15%</form:option>
                                    <form:option value="2">15-25 %</form:option>
                                    <form:option value="3">Больше 25%</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="icgk_f">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="icgkf">Коэффициент ICGK-F:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="icgkf" path="icgk_f">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Меньше 0.4</form:option>
                                    <form:option value="2">0.4 - 0.6</form:option>
                                    <form:option value="3">Больше 0.6</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <c:if test="${rzhp.diagnosisAtStart == true}">
                        <spring:bind path="diagnRaka">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="diagnRaka">Характер диагностики рака желчного пузыря:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="diagnRaka" path="diagnRaka">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Манифестация желтухой</form:option>
                                        <form:option
                                                value="2">Случайная находка по данным лучевых методов обследования</form:option>
                                        <form:option
                                                value="3">Выявление при гистологическом исследовании удаленного желчного пузыря</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="trofStatus">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="trof_status">Оценка трофического статуса до
                                        операциии:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" id="trof_status"
                                            path="trofStatus">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Хороший</form:option>
                                        <form:option value="2">Удовлетворительный</form:option>
                                        <form:option
                                                value="3">Неудовлетворительный</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </c:if>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            var c = document.getElementById("rzhpForm").elements;
            for (var i = 0; i < c.length; i++) {
                c[i].setAttribute("disabled", true);
            }
        }
    });
</script>
</body>
</html>
