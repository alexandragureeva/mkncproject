<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>МКР</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <style>
        .row:not(.scintigrafia):not(.buttonRow):not(.section-header):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../../patients/update/${mkr.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные о диагнозе "МКР"
                    ${(mkr.diagnosisAtStart)?" (поступление)":" (выписка)"}</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveMkr?${_csrf.parameterName}=${_csrf.token}" var="mkrUrl"/>
                <form:form action="${mkrUrl}" modelAttribute="mkr" method="post"
                           id="mkrForm">

                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>


                    <spring:bind path="diagnosisAtStart">
                        <form:hidden path="diagnosisAtStart"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="nalVnepMet">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="nalVnepMet">Наличие внепеченочных метастазов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="nalVnepMet" id="nalVnepMet">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">нет</form:option>
                                    <form:option value="2">В легких</form:option>
                                    <form:option value="3">В лимфатических узлах брюшной полости</form:option>
                                    <form:option value="4">По брюшине</form:option>
                                    <form:option value="5">В других органах брюшной полости</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="pervOpuch">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="pervOpuch">Первичная опухоль:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="pervOpuch" id="pervOpuch">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Ободочная кишка</form:option>
                                    <form:option value="2">Прямая кишка</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="stadPervOpuch">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="stadPervOpuch">Стадия первичной опухоли:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="stadPervOpuch" id="stadPervOpuch">
                                    <form:option value="0">---------</form:option>
                                    <optgroup label="Стадия I">
                                        <form:option value="1">T1N0M0</form:option>
                                        <form:option value="2">T2N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIА">
                                        <form:option value="3">T3N0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIB">
                                        <form:option value="4">T4аN0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIC">
                                        <form:option value="5">T4bN0M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIA">
                                        <form:option value="6">T1N1M0</form:option>
                                        <form:option value="7">T2N1M0</form:option>
                                        <form:option value="8">T1N2aM0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIB">
                                        <form:option value="9">T3N1M0</form:option>
                                        <form:option value="10">T4aN1M0</form:option>
                                        <form:option value="11">T2N2aM0</form:option>
                                        <form:option value="12">T3N2aM0</form:option>
                                        <form:option value="13">T1N2bM0</form:option>
                                        <form:option value="14">T2N2bM0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IIIC">
                                        <form:option value="15">T4aN2aM0</form:option>
                                        <form:option value="16">T3N2bM0</form:option>
                                        <form:option value="17">T4aN2bM0</form:option>
                                        <form:option value="18">T4bN1M0</form:option>
                                        <form:option value="19">T4bN2M0</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVa">
                                        <form:option value="20">Любая T, любая N, M1a</form:option>
                                    </optgroup>
                                    <optgroup label="Стадия IVb">
                                        <form:option value="21">Любая T, любая N, M1b</form:option>
                                    </optgroup>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="obOPerPerOpuch">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="obOPerPerOpuch">Объем операции на первичной опухоли:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="obOPerPerOpuch" id="obOPerPerOpuch">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Правосторонняя гемиколэктомия</form:option>
                                    <form:option value="2">Резекция поперечной ободочной кишки</form:option>
                                    <form:option value="3">Левосторонняя гемиколэктомия</form:option>
                                    <form:option value="4">Резекция сигмовидной кишки</form:option>
                                    <form:option value="5">Передняя резекция прямой кишки</form:option>
                                    <form:option value="6">Низкая передняя резекция прямой кишки</form:option>
                                    <form:option value="7">Брюшноанальная резекция прямой кишки</form:option>
                                    <form:option value="8">Экстирпация прямой кишки</form:option>
                                    <form:option value="9">Другие</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="razmerOpucholi">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="razmer_opucholi">Максимальный размер опухоли(см):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="razmerOpucholi" id="razmer_opucholi">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Меньше 10 см</form:option>
                                    <form:option value="2">Больше 10 см</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="opuchUzliType">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Тип опухолевых узлов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="opuchUzliType" id="type_opuch_uzliMKR">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Солитарный</form:option>
                                    <form:option value="2">Множестевенные</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="chisloOpuchUzlov">
                        <div class="row" id="opuch_uzli_groupMKR" style="display: none;">
                            <div class="col-xs-3">
                                <label for="opuch_uzliMKR">Число опухолевых узлов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="chisloOpuchUzlov" id="opuch_uzliMKR">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">0</form:option>
                                    <form:option value="2">1</form:option>
                                    <form:option value="3">2</form:option>
                                    <form:option value="4">3</form:option>
                                    <form:option value="5">4</form:option>
                                    <form:option value="6">5</form:option>
                                    <form:option value="7">Больше 5</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="sosudInvasia">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Сосудистая инвазия:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="sosudInvasia" id="type_sosud_inv">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Микрососудистая инвазия</form:option>
                                    <form:option value="3">Макрососудистая инвазия</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <c:if test="${mkr.diagnosisAtStart == true}">
                        <div class="form-group">
                            <div class="row section-header">
                                <h4 class="section-heading">Сцинтиграфия</h4>
                                <hr>
                            </div>
                            <spring:bind path="indexZachvata">
                                <div class="row scintigrafia">
                                    <div class="col-xs-3">
                                        <label for="index_zachvata">Стандартный объем печени, см^3:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:input step="any"
                                                    id="index_zachvata" path="indexZachvata" class="form-control"
                                                    type="number"/>
                                    </div>
                                </div>
                            </spring:bind>
                            <spring:bind path="objemOstatka">
                                <div class="row scintigrafia">
                                    <div class="col-xs-3">
                                        <label for="ob_ostatka">Объем будущего остатка печени по данным КТ %(число),
                                            см^3:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:input id="ob_ostatka" step="any"
                                                    path="objemOstatka" class="form-control" type="number"/>
                                    </div>
                                </div>
                            </spring:bind>
                            <spring:bind path="rezekciaType">
                                <div class="row scintigrafia">
                                    <div class="col-xs-3">
                                        <label for="rezekciaType">Планируемый объем операции:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" id="rezekciaType" path="rezekciaType">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">Сегментарная</form:option>
                                            <form:option value="2">Обширная</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                    </c:if>
                    <spring:bind path="vnepOchagiType">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="vnep_ocagi"> Наличие внепеченочных очагов:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="vnepOchagiType" id="vnep_ocagi">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Отдаленные метастазы</form:option>
                                    <form:option value="3">Прорастание соседних органов</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="trombPechType">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="tromb_pech_ven"> Наличие тромбоза печеночных вен:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="trombPechType" id="tromb_pech_ven">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Да</form:option>
                                    <form:option value="3">С выходом в нижнюю полую вену</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="icg">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="icg">Коэффициент элиминации ICG:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="icg" id="icg">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Меньше 15%</form:option>
                                    <form:option value="2">15-25 %</form:option>
                                    <form:option value="3">Больше 25%</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="icgk_f">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="icgkf">Коэффициент ICGK-F:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="icgk_f" id="icgkf">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Меньше 0.4</form:option>
                                    <form:option value="2">0.4 - 0.6</form:option>
                                    <form:option value="3">Больше 0.6</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="trombozVorotVeni">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="tromb_vor_ven">Тромбоз воротной вены:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="trombozVorotVeni" id="tromb_vor_ven">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">0</form:option>
                                    <form:option value="2">1</form:option>
                                    <form:option value="3">2</form:option>
                                    <form:option value="4">3</form:option>
                                    <form:option value="5">4</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <c:if test="${mkr.diagnosisAtStart == true}">
                        <spring:bind path="nalNeoadChimioTer">
                            <div class="row">
                                <div class="col-xs-3"><label>Наличие неоадъювантной химиотерапии до резекции печени или
                                    РЧА:</label>

                                </div>
                                <div class="col-xs-9">
                                    <label class="radio-inline"><form:radiobutton path="nalNeoadChimioTer"
                                                                                  id="chim_noMKR"
                                                                                  name="optradio"
                                                                                  value="false"/>Нет</label>
                                    <label class="radio-inline"><form:radiobutton path="nalNeoadChimioTer"
                                                                                  id="chim_yesMKR"
                                                                                  name="optradio"
                                                                                  value="true"/>Да</label>
                                </div>
                            </div>
                        </spring:bind>
                        <div class="form-group" id="chimiotGroupMKR" style="display: none;">
                            <spring:bind path="chimiotKurs">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="chimiotKursMKR">Количество курсов химиотерапии:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" path="chimiotKurs" id="chimiotKursMKR">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">4 курса</form:option>
                                            <form:option value="2">6-8 курсов</form:option>
                                            <form:option value="3">Больше 8 курсов</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                            <spring:bind path="chimiotPrep">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="chimiotPrepMKR">Препараты:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" path="chimiotPrep" id="chimiotPrepMKR">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">Оксалиплатин</form:option>
                                            <form:option value="2">Иринотекан</form:option>
                                            <form:option value="3">Цитостатики и авастин</form:option>
                                            <form:option value="4">Цитостатики и эрбитукс (виктибикс)</form:option>
                                            <form:option value="5">Цитостатики и другой таргетный препарат</form:option>
                                            <form:option value="6">FOLFOXIRI</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                            <spring:bind path="chimiotEffect">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="chimiotEffectMKR">Эффект от химиотерапии:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <form:select
                                                class="form-control" path="chimiotEffect" id="chimiotEffectMKR">
                                            <form:option value="0">---------</form:option>
                                            <form:option value="1">Полный ответ</form:option>
                                            <form:option value="2">Частичный ответ</form:option>
                                            <form:option value="3">Стабилизация</form:option>
                                            <form:option value="4">Прогрессия</form:option>
                                        </form:select>
                                    </div>
                                </div>
                            </spring:bind>
                        </div>
                        <spring:bind path="pojavMetType">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="pojavMetType">Тип метастазов в печени:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" path="pojavMetType" id="pojavMetType">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Синхронные</form:option>
                                        <form:option value="2">Метахронные</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="pojavMetTime">
                            <div class="row" style="display: none;" id="pojavMetTimeRow">
                                <div class="col-xs-3">
                                    <label for="pojavMetTime">Время появления метастазов в печени:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" path="pojavMetTime" id="pojavMetTime">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">до 1 года после удаления первичного очага</form:option>
                                        <form:option
                                                value="2">после одного года после удаления первичного очага</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>

                        <spring:bind path="trofStatus">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="trof_status">Оценка трофического статуса до операциии:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" path="trofStatus" id="trof_status">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Хороший</form:option>
                                        <form:option value="2">Удовлетворительный</form:option>
                                        <form:option value="3">Неудовлетворительный</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </c:if>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>

                    <script>
                        $(document).on("change", "#type_opuch_uzliMKR", function (e) {
                            var selectedVal = $(this).find("option:selected").val();
                            switch (selectedVal) {
                                case "2":
                                    document.getElementById("opuch_uzli_groupMKR").removeAttribute("style");
                                    break;
                                default:
                                    document.getElementById("opuch_uzli_groupMKR").setAttribute("style", "display:none;");
                                    document.getElementById("opuch_uzliMKR").value = 0;
                                    break;
                            }
                        });
                        $(document).on("click", "#chim_yesMKR", function (e) {
                            document.getElementById("chimiotGroupMKR").removeAttribute("style");
                        });
                        $(document).on("click", "#chim_noMKR", function (e) {
                            document.getElementById("chimiotGroupMKR").setAttribute("style", "display:none;");
                            document.getElementById("chimiotKursMKR").value = 0;
                            document.getElementById("chimiotEffectMKR").value = 0;
                            document.getElementById("chimiotPrepMKR").value = 0;
                        });
                        var selectedVal = $("#type_opuch_uzliMKR").find("option:selected").val();
                        if (selectedVal == "2") {
                            document.getElementById("opuch_uzli_groupMKR").removeAttribute("style");
                        }
                        if (document.getElementById('chim_yesMKR').checked) {
                            document.getElementById("chimiotGroupMKR").removeAttribute("style");
                        }

                        $("#pojavMetType").on("change", function (e) {
                            e.preventDefault();
                            switch ($("#pojavMetType").val()) {
                                case "2":
                                    document.getElementById("pojavMetTimeRow").removeAttribute("style");
                                    break;
                                default:
                                    document.getElementById("pojavMetTimeRow").setAttribute("style", "display:none;");
                                    document.getElementById("pojavMetTime").value = 0;
                                    break;
                            }
                        });
                        if (+$("#pojavMetType").val() == 2) {
                            document.getElementById("pojavMetTimeRow").removeAttribute("style");
                        }

                        $(document).ready(function () {
                            var expired = document.getElementById("expiredValue").value;
                            var isAdmin = document.getElementById("isAdminValue").value;
                            if (expired === "true" && isAdmin === "false") {
                                var c = document.getElementById("mkrForm").elements;
                                for (var i = 0; i < c.length; i++) {
                                    c[i].setAttribute("disabled", true);
                                }
                            }
                        });
                    </script>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>