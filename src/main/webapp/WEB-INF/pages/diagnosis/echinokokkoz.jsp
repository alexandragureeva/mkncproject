<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Эхинококкоз</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.scintigrafia):not(.buttonRow):not(.section-header):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../../patients/update/${id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные о диагнозе "Эхинококкоз"
                    ${(echinokokkoz.diagnosisAtStart)?" (поступление)":" (выписка)"}</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveEchinokokkoz?${_csrf.parameterName}=${_csrf.token}" var="echinokokkozUrl"/>
                <form:form action="${echinokokkozUrl}" modelAttribute="echinokokkoz" method="post"
                           id="echinokokkozForm">

                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>


                    <spring:bind path="diagnosisAtStart">
                        <form:hidden path="diagnosisAtStart"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="voz">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="voz">Классификация печени по ВОЗ:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="voz" id="voz">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">CE0</form:option>
                                    <form:option value="2">CE1</form:option>
                                    <form:option value="3">CE2</form:option>
                                    <form:option value="4">CE3a</form:option>
                                    <form:option value="5">CE3b</form:option>
                                    <form:option value="6">CE4</form:option>
                                    <form:option value="7">CE5</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="antitela">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Анализ крови на антитела к эхинококку:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="antitela" id="antitela_no"
                                                                              name="optradio"
                                                                              value="false"/>Не сделан</label>
                                <label class="radio-inline"><form:radiobutton path="antitela" id="antitela_yes"
                                                                              name="optradio"
                                                                              value="true"/>Сделан</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="antitelaRes">
                        <div class="row" style="display: none;" id="antitelaResGroup">
                            <div class="col-xs-3">
                                <label for="antitelaRes">Результат анализа крови на антитела:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="antitelaRes" id="antitelaRes">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Положительный результат</form:option>
                                    <form:option value="2">Отрицательный результат</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="antitelaUroven">
                        <div class="row" style="display: none;" id="antitelaUrovenGroup">
                            <div class="col-xs-3">
                                <label for="antitelaUroven">Уровень антител (число):</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="antitelaUroven" path="antitelaUroven" class="form-control" type="number"/>
                            </div>
                        </div>
                    </spring:bind>
                    <c:if test="${echinokokkoz.diagnosisAtStart == true}">
                        <spring:bind path="epidemAnamnez">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="epidemAnamnez">Эпидемиологический анамнез:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" path="epidemAnamnez" id="epidemAnamnez">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Не относится к группе риска</form:option>
                                        <form:option value="2">Животновод</form:option>
                                        <form:option value="3">Постоянно проживающий в сельской местности</form:option>
                                        <form:option value="4">Охотник</form:option>
                                        <form:option value="5">Продавец мясных продуктов</form:option>
                                        <form:option
                                                value="6">Проживание в гиперэндемичной зоне (страны средней Азии)</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </c:if>
                    <spring:bind path="vnepPorazhenie">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="vnepPorazhenie">Внепеченочное поражение:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" path="vnepPorazhenie" id="vnepPorazhenie">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Нет</form:option>
                                    <form:option value="2">Кисты легких</form:option>
                                    <form:option value="3">Кисты брюшной полости</form:option>
                                    <form:option value="4">Кисты других локализаций</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <h4>Данные лучевых методов обследования узи, кт</h4>
                    <spring:bind path="mnozhKisty">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Кисты:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="mnozhKisty" id="mnozhKisty_no"
                                                                              name="optradio"
                                                                              value="false"/>Единичные</label>
                                <label class="radio-inline"><form:radiobutton path="mnozhKisty" id="mnozhKisty_yes"
                                                                              name="optradio"
                                                                              value="true"/>Множественные</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="kistyNumber">
                        <div class="row" id="kistyNumberGroup">
                            <div class="col-xs-3">
                                <label for="antitelaUroven">Число кист:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="antitelaUroven" path="kistyNumber" class="form-control" type="text"/>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="kistyType">
                        <div class="row">
                            <div class="col-xs-3">
                                <label>Тип кист:</label>
                            </div>
                            <div class="col-xs-9">
                                <label class="radio-inline"><form:radiobutton path="kistyType" name="optradio"
                                                                              value="false"/>Унилабарные</label>
                                <label class="radio-inline"><form:radiobutton path="kistyType" name="optradio"
                                                                              value="true"/>Билабарные</label>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="kistyRazmer">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="kistyRazmer">Размер кист:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:input
                                        id="kistyRazmer" path="kistyRazmer" class="form-control"
                                        type="number"/>
                            </div>
                        </div>
                    </spring:bind>

                    <div class="form-group" id="opuch_uzli_group">
                        <spring:bind path="kistyOsl">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="kistyOsl">Наличие осложнений:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" path="kistyOsl" id="kistyOsl">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">нет</form:option>
                                        <form:option value="2">Нагноение</form:option>
                                        <form:option value="3">Наличие желчного свища в кисте</form:option>
                                        <form:option value="4">Прорыв в желчные потоки</form:option>
                                        <form:option value="5">Разрыв внутрь живота</form:option>
                                        <form:option value="6">Угроза разрыва</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </div>
                    <c:if test="${echinokokkoz.diagnosisAtStart == true}">
                        <spring:bind path="kratLechenia">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="kratLechenia">Кратность лечения:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" path="kratLechenia" id="kratLechenia">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Первичный эхинококкоз</form:option>
                                        <form:option value="2">Рецидивный эхинококкоз</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>

                        <spring:bind path="lechenieType">
                            <div class="row" style="display: none;" id="lechenieTypeGroup">
                                <div class="col-xs-3">
                                    <label for="lechenieType">Рецидивный эхинококкоз после:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" path="lechenieType" id="lechenieType">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Пункционного лечения</form:option>
                                        <form:option value="2">Лапароскопии</form:option>
                                        <form:option value="3">Открытой операции</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                        <spring:bind path="trofStatus">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="trof_status">Оценка трофического статуса до операциии:</label>
                                </div>
                                <div class="col-xs-9">
                                    <form:select
                                            class="form-control" path="trofStatus" id="trof_status">
                                        <form:option value="0">---------</form:option>
                                        <form:option value="1">Хороший</form:option>
                                        <form:option value="2">Удовлетворительный</form:option>
                                        <form:option value="3">Неудовлетворительный</form:option>
                                    </form:select>
                                </div>
                            </div>
                        </spring:bind>
                    </c:if>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>

                    <script>
                        $(document).on("click", "#antitela_yes", function (e) {
                            document.getElementById("antitelaResGroup").removeAttribute("style");
                            document.getElementById("antitelaUrovenGroup").removeAttribute("style");
                        });
                        $(document).on("click", "#antitela_no", function (e) {
                            document.getElementById("antitelaResGroup").setAttribute("style", "display:none;");
                            document.getElementById("antitelaUrovenGroup").setAttribute("style", "display:none;");
                        });
                        $(document).on("change", "#kratLechenia", function (e) {
                            var selectedVal = $(this).find("option:selected").val();
                            switch (selectedVal) {
                                case "2":
                                    document.getElementById("lechenieTypeGroup").removeAttribute("style");
                                    break;
                                default:
                                    document.getElementById("lechenieTypeGroup").setAttribute("style", "display:none;");
                                    document.getElementById("lechenieType").value = 0;
                                    break;
                            }
                        });
                        var selectedVal = $("#kratLechenia").find("option:selected").val();
                        if (selectedVal == "2") {
                            document.getElementById("lechenieTypeGroup").removeAttribute("style");
                        }
                        if (document.getElementById('antitela_yes').checked) {
                            document.getElementById("antitelaResGroup").removeAttribute("style");
                            document.getElementById("antitelaUrovenGroup").removeAttribute("style");
                        }

                        $(document).ready(function () {
                            var expired = document.getElementById("expiredValue").value;
                            var isAdmin = document.getElementById("isAdminValue").value;
                            if (expired === "true" && isAdmin === "false") {
                                var c = document.getElementById("echinokokkozForm").elements;
                                for (var i = 0; i < c.length; i++) {
                                    c[i].setAttribute("disabled", true);
                                }
                            }
                        });
                    </script>

                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>