<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>ЖКБ</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.scintigrafia):not(.buttonRow):not(.section-header):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../../patients/update/${zhkb.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Данные о диагнозе "Желчнокаменная болезнь"
                    ${(zhkb.diagnosisAtStart)?" (поступление)":" (выписка)"}</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveZhkb?${_csrf.parameterName}=${_csrf.token}" var="zhkbUrl"/>
                <form:form action="${zhkbUrl}" modelAttribute="zhkb" method="post"
                           id="zhkbForm">

                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>


                    <spring:bind path="diagnosisAtStart">
                        <form:hidden path="diagnosisAtStart"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">

                    <spring:bind path="cholecistit">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="cholecistit">Калькулезный холецистит:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select class="form-control" path="cholecistit" id="cholecistit">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Хронический</form:option>
                                    <form:option value="2">Острый</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <spring:bind path="ostriiCholecistit">
                        <div class="row" id="ostrii" style="display: none;">
                            <div class="col-xs-3">
                                <label for="ostriiCholecistit">Острый калькулезный холецистит:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="ostriiCholecistit" path="ostriiCholecistit">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Катаральный</form:option>
                                    <form:option value="2">Деструктивный</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="row">
                        <div class="col-xs-12">
                            <spring:bind path="oslozhnenii">
                                <form:checkbox path="oslozhnenii" id="oslozhnenii" value="1"/><b>Осложненный калькулезный холецистит</b>
                            </spring:bind>
                        </div>
                    </div>
                    <ul class="list-unstyled" id="oslozh_chron" style="display: none;">
                        <li>
                            <spring:bind path="mirizi">
                            <div class="row">
                                <div class="col-xs-5">
                                    <form:checkbox path="mirizi" id="mirizi" value="1"/><b>Синдром Миризи</b>
                                </div>
                                </spring:bind>
                                <div class="col-xs-4"><spring:bind path="zhelt_mirizi">
                                    <form:select path="zhelt_mirizi" id="zhelt_mirizi" class="form-control"
                                                 cssStyle="display: none;">
                                        <form:option value="0">----</form:option>
                                        <form:option value="1">С желтухой</form:option>
                                        <form:option value="2">Без желтухи</form:option>
                                    </form:select>
                                </spring:bind></div>
                                <div class="col-xs-3">
                                    <spring:bind path="type">
                                        <form:select path="type" id="type" class="form-control"
                                                     cssStyle="display: none;">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">1 тип</form:option>
                                            <form:option value="2">2 тип</form:option>
                                            <form:option value="3">3 тип</form:option>
                                        </form:select>
                                    </spring:bind>
                                </div>
                            </div>
                        </li>

                        <li>
                            <div class="row">
                                <div class="col-xs-5"><spring:bind path="cholangiol">
                                    <form:checkbox id="cholangiol" path="cholangiol" value="1"/><b>Холангиолитиаз</b>
                                </spring:bind></div>
                                <div class="col-xs-4"><spring:bind path="zhelt_cholang">
                                    <form:select path="zhelt_cholang" id="zhelt_cholang" class="form-control"
                                                 cssStyle="display: none;">
                                        <form:option value="0">----</form:option>
                                        <form:option value="1">С желтухой</form:option>
                                        <form:option value="2">Без желтухи</form:option>
                                    </form:select>
                                </spring:bind></div>
                                <div class="col-xs-3">
                                    <spring:bind path="type_cholang">
                                        <form:select path="type_cholang" id="type_cholang" class="form-control"
                                                     cssStyle="display: none;">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Холедохолитиаз</form:option>
                                            <form:option value="2">Внутрипеченочный холангиолитиаз</form:option>
                                            <form:option
                                                    value="3">Сочетание внутрипеченочного и внепеченочного холангиолитиаза</form:option>
                                        </form:select>
                                    </spring:bind>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12"><spring:bind path="vodyanka">
                                    <form:checkbox id="vodyanka" path="vodyanka" value="1"/><b>Водянка желчного
                                    пузыря</b>
                                </spring:bind></div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12"><spring:bind path="svich">
                                    <form:checkbox id="svich" path="svich" value="1"/><b>Пузырно-дуоденальный свич</b>
                                </spring:bind></div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12"><spring:bind path="smorch">
                                    <form:checkbox id="smorch" path="smorch" value="1"/><b>Сморщенный желчный пузырь</b>
                                </spring:bind></div>
                            </div>
                        </li>
                    </ul>

                    <ul class="list-unstyled" id="oslozh_ostrii" style="display: none;">
                        <li>
                            <div class="row">
                                <div class="col-xs-12"><spring:bind path="peritonit">
                                    <form:checkbox id="peritonit" path="peritonit" value="1"/><b>Перфорация + местный
                                    перитонит</b>
                                </spring:bind></div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12"><spring:bind path="empiema">
                                    <form:checkbox id="empiema" path="empiema" value="1"/><b>Эмпиема желчного пузыря</b>
                                </spring:bind></div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12"><spring:bind path="cholangit">
                                    <form:checkbox id="cholangit" path="cholangit" value="1"/><b>Острый холангит</b>
                                </spring:bind></div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-xs-12"><spring:bind path="cholpankreotit">
                                    <form:checkbox id="cholpankreotit" path="cholpankreotit" value="1"/><b>Острый
                                    холецистопанкреотит</b>
                                </spring:bind></div>
                            </div>
                        </li>
                    </ul>

                    <spring:bind path="trof_status">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="trof_status">Оценка трофического статуса до
                                    операциии:</label>
                            </div>
                            <div class="col-xs-9">
                                <form:select
                                        class="form-control" id="trof_status"
                                        path="trof_status">
                                    <form:option value="0">---------</form:option>
                                    <form:option value="1">Хороший</form:option>
                                    <form:option value="2">Удовлетворительный</form:option>
                                    <form:option
                                            value="3">Неудовлетворительный</form:option>
                                </form:select>
                            </div>
                        </div>
                    </spring:bind>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            var c = document.getElementById("zhkbForm").elements;
            for (var i = 0; i < c.length; i++) {
                c[i].setAttribute("disabled", true);
            }
        }
        if (+$("#cholecistit").val() == 2) {
            document.getElementById("ostrii").removeAttribute("style");
        }

        $("#cholecistit").on("change", function() {
            $("#oslozhnenii").prop( "checked", false );
            document.getElementById("oslozh_chron").setAttribute("style", "display:none;");
            document.getElementById("oslozh_ostrii").setAttribute("style", "display:none;");
            disableChronOslozh();
            disableOstriiOslozh();
          if (+$("#cholecistit").val() == 2) {
              document.getElementById("ostrii").removeAttribute("style");
          } else {
              document.getElementById("ostrii").setAttribute("style", "display: none;");
              $("#ostriiCholecistit").val(0);
          }
        });
        function oslozhFunction() {
            if (document.getElementById("oslozhnenii").checked) {
                if ( +$("#cholecistit").val() == 1 ) {
                    document.getElementById("oslozh_chron").removeAttribute("style");
                    disableOstriiOslozh();
                }
                else if ( +$("#cholecistit").val() == 2 ) {
                    document.getElementById("oslozh_ostrii").removeAttribute("style");
                    disableChronOslozh();
                }  else {
                    document.getElementById("oslozh_chron").setAttribute("style", "display:none;");
                    document.getElementById("oslozh_ostrii").setAttribute("style", "display:none;");
                    disableChronOslozh();
                    disableOstriiOslozh();
                }
            } else {
                document.getElementById("oslozh_chron").setAttribute("style", "display:none;");
                document.getElementById("oslozh_ostrii").setAttribute("style", "display:none;");

            }
        }
        $("#oslozhnenii").on("change", oslozhFunction);
        oslozhFunction();
         function miriziFunction() {
            if (document.getElementById("mirizi").checked) {
                document.getElementById("zhelt_mirizi").removeAttribute("style");
                document.getElementById("type").removeAttribute("style");
            } else {
                document.getElementById("zhelt_mirizi").setAttribute("style", "display:none;");
                document.getElementById("type").setAttribute("style", "display:none;");
                $("#zhelt_mirizi").val(0);
                $("#type").val(0);
            }
        }
        $("#mirizi").on("change", miriziFunction );
        miriziFunction();
        function cholangiolFunction() {
            if (document.getElementById("cholangiol").checked) {
                document.getElementById("zhelt_cholang").removeAttribute("style");
                document.getElementById("type_cholang").removeAttribute("style");
            } else {
                document.getElementById("zhelt_cholang").setAttribute("style", "display:none;");
                document.getElementById("type_cholang").setAttribute("style", "display:none;");
                $("#zhelt_cholang").val(0);
                $("#type_cholang").val(0);
            }
        }
        $("#cholangiol").on("change", cholangiolFunction);
        cholangiolFunction();

        function disableChronOslozh() {
            $("#mirizi").prop( "checked", false );
            $("#cholangiol").prop( "checked", false );
            $("#vodyanka").prop( "checked", false );
            $("#svich").prop( "checked", false );
            $("#smorch").prop( "checked", false );
            $("#zhelt_mirizi").val(0);
            $("#type").val(0);
            $("#zhelt_cholang").val(0);
            $("#type_cholang").val(0);
        }
        function disableOstriiOslozh() {
            $("#peritonit").prop( "checked", false );
            $("#empiema").prop( "checked", false );
            $("#cholangit").prop( "checked", false );
            $("#cholpankreotit").prop( "checked", false );
        }
    });
</script>
</body>
</html>
