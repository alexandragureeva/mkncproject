<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">
    <title>Статистика</title>

    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/js/TableFilter/filtergrid.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/datepicker.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery-1.9.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tooltip.js"/>"></script>
    <script src="<c:url value="/resources/js/statistics.js"/>"></script>
    <script src="<c:url value="/resources/js/xdate.js"/>"></script>
    <script src="<c:url value="/resources/js/TableFilter/tablefilter_all.js"/>"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script>
</head>
<body>
<c:url value="/resources/js/TableFilter/" var="tableFilter"/>
<div class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://mknc.ru/">
                MKNC Patients
            </a>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row ">
        <div class="container-fluid">
            <ol class="breadcrumb">
                <li><a href="welcome"><i class="fa fa-home"></i></a></li>
                <li class="active"><a href="statistics">Статистика</a></li>
            </ol>
        </div>
    </div>
</div>

<!--Fields-->
<div class="panel-group" id="accordion1">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="#patients-table" data-toggle="collapse">
                    <i class="glyphicon glyphicon-plus"></i>
                    Поля страницы
                </a>
            </h4>
        </div>
        <div id="patients-table" class="panel-collapse collapse">
            <div class="panel-content">
                <div class="form-inline">
                    <input type="button" class="btn btn-default" class="form-control" id="show_all"
                           value="Показать/убрать все поля">
                    <input type="button" class="btn btn-default" class="form-control" id="gen_info"
                           value="Общая информация">
                    <input type="button" class="btn btn-default" class="form-control" id="ob_os"
                           value="Объектиный осмотр">
                    <input type="button" class="btn btn-default" class="form-control" id="an0"
                           value="Анализы поступление">
                    <input type="button" class="btn btn-default" class="form-control" id="an1"
                           value="Анализы 1 день">
                    <input type="button" class="btn btn-default" class="form-control" id="an5"
                           value="Анализы 5 день">
                    <input type="button" class="btn btn-default" class="form-control" id="an10"
                           value="Анализы выписка">
                    <input type="button" class="btn btn-default" class="form-control" id="show_klat"
                        value='Диагноз "Опухоль Клатскина" поступление'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_alv"
                           value='Диагноз "Альвиококкоз" поступление'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_ech"
                           value='Диагноз "Эхинококкоз" поступление'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_gcr"
                           value='Диагноз "ГЦР" поступление'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_hcr"
                           value='Диагноз "ХЦР" поступление'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_mkr"
                           value='Диагноз "МКР" поступление'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_mnkr"
                           value='Диагноз "МНКР" поступление'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_rzh"
                           value='Диагноз "Рак желудка" поступление'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_klat_p"
                           value='Диагноз "Опухоль Клатскина" выписка'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_alv_p"
                           value='Диагноз "Альвиококкоз" выписка'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_ech_p"
                           value='Диагноз "Эхинококкоз" выписка'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_gcr_p"
                           value='Диагноз "ГЦР" выписка'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_hcr_p"
                           value='Диагноз "ХЦР" выписка'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_mkr_p"
                           value='Диагноз "МКР" выписка'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_mnkr_p"
                           value='Диагноз "МНКР" выписка'>
                    <input type="button" class="btn btn-default" class="form-control" id="show_rzh_p"
                           value='Диагноз "Рак желудка" выписка'>
                    <input type="button" class="btn btn-default" class="form-control" id="op_klat"
                           value='Операция при "Опухоль Клатскина"'>
                    <input type="button" class="btn btn-default" class="form-control" id="op_alv"
                           value='Операция при "Альвиококкоз"'>
                    <input type="button" class="btn btn-default" class="form-control" id="op_ech"
                           value='Операция при "Эхинококкоз"'>
                    <input type="button" class="btn btn-default" class="form-control" id="op_gcr"
                           value='Операция при "ГЦР"'>
                    <input type="button" class="btn btn-default" class="form-control" id="op_hcr"
                           value='Операция при "ХЦР"'>
                    <input type="button" class="btn btn-default" class="form-control" id="op_mkr"
                           value='Операция при "МКР"'>
                    <input type="button" class="btn btn-default" class="form-control" id="op_mnkr"
                           value='Операция при "МНКР"'>
                    <input type="button" class="btn btn-default" class="form-control" id="op_rzh"
                           value='Операция при "Рак желудка"'>
                    <input type="button" class="btn btn-default" class="form-control" id="dren"
                           value='Дренирование'>

                </div>
                <div class="tag-list" id="well">
                </div>
            </div>
        </div>
    </div>
</div>

<!--Dropdown statistics-->
<div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
    <div class="container-fluid statistics">
        <div class="input-group">
            <div class="form-group">
                <button type="button" class="btn btn-default btn-sm" id="descriptive" data-toggle="modal"
                        data-target="#descriptive_modal">Описательная статистика
                </button>
                <div class="dropdown">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Непараметрическая статистика<span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li id="2x2" data-toggle="modal" data-target="#2x2_modal"><a href="#">Таблицы частот 2х2</a>
                        </li>
                        <li id="spirman" data-toggle="modal" data-target="#spirman_modal"><a href="#">Коэффициент
                            корреляции Спирмена</a></li>
                        <li id="manna_uitni" data-toggle="modal" data-target="#manna_uitni_modal"><a href="#">U-критерий
                            Манна-Уитни</a></li>
                    </ul>
                </div>
                <button type="button" class="btn btn-default btn-sm" id="survival" data-toggle="modal"
                        data-target="#survival_modal">Анализ выживаемости
                </button>
                <button type="button" class="btn btn-default btn-sm" id="dispertion" data-toggle="modal"
                        data-target="#dispertion_modal">Дисперсионный анализ
                </button>
            </div>
        </div>
    </div>

</div>
<!--Table-->
<div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xs-offset-1 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
    <!--div class="container-fluid table"-->
    <a href="#" class="scrollToTop">Вверх<i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
    <a href="#" class="scrollToBottom">Вниз<i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
    <div class="panel panel-default">
        <div class="panel-heading">Данные</div>
        <div class="panel-body">
            <div class="table-responsive">
                <div class="table-to-analyze">
                    <!--Table-->
                    <table id="valuesTable" class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                        <tr class="headers">
                            <c:forEach var="i" begin="0" end="${headers.size()-1}">
                                <th data-toggle="tooltip" title="<c:out value='${headerTooltips.get(i)}'/>"
                                    class="${i}">${headers.get(i)}</th>
                            </c:forEach>
                        </tr>
                        </thead>
                        <tbody id="table-body">
                        <c:if test="${gpis!= null && gpis.size() != 0}">
                            <c:forEach var="i" begin="0" end="${gpis.size()-1}">
                                <tr>
                                    <td>${gpis.get(i).patient.sex}</td>
                                    <td>${gpis.get(i).patient.age}</td>
                                    <td>${Integer.valueOf(gpis.get(i).patient.startDate.substring(0,2))}</td>
                                    <td>${Integer.valueOf(gpis.get(i).patient.startDate.substring(3,5))}</td>
                                    <td>${gpis.get(i).patient.startDate.substring(6,10)}</td>
                                    <td>${(gpis.get(i).patient.endDate==null) ? "" :Integer.valueOf(gpis.get(i).patient.endDate.substring(0,2))}</td>
                                    <td>${(gpis.get(i).patient.endDate==null) ? "" :Integer.valueOf(gpis.get(i).patient.endDate.substring(3,5))}</td>
                                    <td>${(gpis.get(i).patient.endDate==null) ? "" :gpis.get(i).patient.endDate.substring(6,10)}</td>
                                    <td>${(gpis.get(i).patient.bedDaysNumber==null) ? "" :gpis.get(i).patient.bedDaysNumber}</td>
                                    <td>${(gpis.get(i).patient.weight==null) ? "" :gpis.get(i).patient.weight}</td>
                                    <td>${(gpis.get(i).patient.bmi==null) ? "" :gpis.get(i).patient.bmi}</td>
                                    <td>${(gpis.get(i).patient.asaValue==null) ? "" :gpis.get(i).patient.asaValue}</td>
                                    <td>${gpis.get(i).patient.diagnosis}</td>
                                    <td>${(gpis.get(i).patient.gospitalization==null) ? "" :gpis.get(i).patient.gospitalization}</td>
                                    <td>${(gpis.get(i).patient.gospitalizationTime==null) ? "" :gpis.get(i).patient.gospitalizationTime}</td>
                                    <td>${(gpis.get(i).patient.ibsType==null) ? "" :gpis.get(i).patient.ibsType}</td>
                                    <td>${(gpis.get(i).patient.ibsSubType==null) ? "" :gpis.get(i).patient.ibsSubType}</td>
                                    <td>${(gpis.get(i).patient.hyperDiseaseType==null) ? "" :gpis.get(i).patient.hyperDiseaseType}</td>
                                    <td>${(gpis.get(i).patient.diabetType==null) ? "" :gpis.get(i).patient.diabetType}</td>
                                    <td>${(gpis.get(i).patient.chronVenDiseaseType==null) ? "" :gpis.get(i).patient.chronVenDiseaseType}</td>
                                    <td>${(gpis.get(i).patient.bronchAstmaType==null) ? "" :gpis.get(i).patient.bronchAstmaType}</td>
                                    <td>${(gpis.get(i).patient.chronBronchitType==null) ? "" :gpis.get(i).patient.chronBronchitType}</td>
                                    <td>${(gpis.get(i).patient.emfizemaType==null) ? "" :gpis.get(i).patient.emfizemaType}</td>
                                    <td>${(gpis.get(i).patient.serdechPorokiType==null) ? "" :gpis.get(i).patient.serdechPorokiType}</td>
                                    <td>${(gpis.get(i).patient.onmkType==null) ? "" :gpis.get(i).patient.onmkType}</td>
                                    <td>${(gpis.get(i).patient.parkinsonDiseaseType==null) ? "" :gpis.get(i).patient.parkinsonDiseaseType}</td>
                                    <td>${(gpis.get(i).patient.parkinsonSyndromType==null) ? "" :gpis.get(i).patient.parkinsonSyndromType}</td>
                                    <td>${(gpis.get(i).patient.ozhirenieType==null) ? "" :gpis.get(i).patient.ozhirenieType}</td>
                                    <td>${(gpis.get(i).patient.yazvenDiseaseType==null) ? "" :gpis.get(i).patient.yazvenDiseaseType}</td>
                                    <td>${(gpis.get(i).patient.yazvenDiseaseComplications==null) ? "" :gpis.get(i).patient.yazvenDiseaseComplications}</td>
                                    <td>${(gpis.get(i).patient.hyperplaziaType==null) ? "" :gpis.get(i).patient.hyperplaziaType}</td>
                                    <td>${(gpis.get(i).patient.otherDiseases==null) ? "" :gpis.get(i).patient.otherDiseases}</td>
                                    <td>${(gpis.get(i).analysis0.gemoglabin==null) ? "" :gpis.get(i).analysis0.gemoglabin}</td>
                                    <td>${(gpis.get(i).analysis0.eritrociti==null) ? "" :gpis.get(i).analysis0.eritrociti}</td>
                                    <td>${gpis.get(i).analysis0.leikociti}</td>
                                    <td>${gpis.get(i).analysis0.ph}</td>
                                    <td>${gpis.get(i).analysis0.belok}</td>
                                    <td>${gpis.get(i).analysis0.leikocitiMochi}</td>
                                    <td>${gpis.get(i).analysis0.eritrocitiMochi}</td>
                                    <td>${gpis.get(i).analysis0.glukoza}</td>
                                    <td>${gpis.get(i).analysis0.ketoni}</td>
                                    <td>${gpis.get(i).analysis0.ast}</td>
                                    <td>${gpis.get(i).analysis0.alt}</td>
                                    <td>${gpis.get(i).analysis0.obshiiBelok}</td>
                                    <td>${gpis.get(i).analysis0.albunin}</td>
                                    <td>${gpis.get(i).analysis0.obshiibilirubin}</td>
                                    <td>${gpis.get(i).analysis0.pryamoibilirubin}</td>
                                    <td>${gpis.get(i).analysis0.kreatinin}</td>
                                    <td>${gpis.get(i).analysis0.sacharKrovi}</td>
                                    <td>${gpis.get(i).analysis0.mochevayaKislota}</td>
                                    <td>${gpis.get(i).analysis0.mochevina}</td>
                                    <td>${gpis.get(i).analysis0.mno}</td>
                                    <td>${gpis.get(i).analysis0.achtv}</td>
                                    <td>${gpis.get(i).analysis0.gruppaKrovi}</td>
                                    <td>${gpis.get(i).analysis0.rezFactor}</td>
                                    <td>${(gpis.get(i).analysis0.cea == 1) ? gpis.get(i).analysis0.ceaValue :""}</td>
                                    <td>${(gpis.get(i).analysis0.onk199 == 1) ? gpis.get(i).analysis0.onk199Value :""}</td>
                                    <td>${(gpis.get(i).analysis0.alfoFetoprotein == 1) ? gpis.get(i).analysis0.alfoFetValue :""}</td>
                                    <td>${(gpis.get(i).analysis0.ca125 == 1) ? gpis.get(i).analysis0.ca125Value :""}</td>
                                    <td>${(gpis.get(i).analysis1.gemoglabin==null) ? "" :gpis.get(i).analysis1.gemoglabin}</td>
                                    <td>${(gpis.get(i).analysis1.eritrociti==null) ? "" :gpis.get(i).analysis1.eritrociti}</td>
                                    <td>${gpis.get(i).analysis1.leikociti}</td>
                                    <td>${gpis.get(i).analysis1.palochkoyadernaya}</td>
                                    <td>${gpis.get(i).analysis1.segmentarnoyadernaya}</td>
                                    <td>${gpis.get(i).analysis1.yunie}</td>
                                    <td>${gpis.get(i).analysis1.monociti}</td>
                                    <td>${gpis.get(i).analysis1.limfociti}</td>
                                    <td>${gpis.get(i).analysis1.trombociti}</td>
                                    <td>${gpis.get(i).analysis1.soe}</td>
                                    <td>${gpis.get(i).analysis1.ph}</td>
                                    <td>${gpis.get(i).analysis1.belok}</td>
                                    <td>${gpis.get(i).analysis1.leikocitiMochi}</td>
                                    <td>${gpis.get(i).analysis1.eritrocitiMochi}</td>
                                    <td>${gpis.get(i).analysis1.glukoza}</td>
                                    <td>${gpis.get(i).analysis1.ketoni}</td>
                                    <td>${gpis.get(i).analysis1.ast}</td>
                                    <td>${gpis.get(i).analysis1.alt}</td>
                                    <td>${gpis.get(i).analysis1.obshiiBelok}</td>
                                    <td>${gpis.get(i).analysis1.albunin}</td>
                                    <td>${gpis.get(i).analysis1.obshiibilirubin}</td>
                                    <td>${gpis.get(i).analysis1.pryamoibilirubin}</td>
                                    <td>${gpis.get(i).analysis1.kreatinin}</td>
                                    <td>${gpis.get(i).analysis1.sacharKrovi}</td>
                                    <td>${gpis.get(i).analysis1.mochevayaKislota}</td>
                                    <td>${gpis.get(i).analysis1.mochevina}</td>
                                    <td>${gpis.get(i).analysis1.mno}</td>
                                    <td>${gpis.get(i).analysis1.achtv}</td>
                                    <td>${(gpis.get(i).analysis5.gemoglabin==null) ? "" :gpis.get(i).analysis5.gemoglabin}</td>
                                    <td>${(gpis.get(i).analysis5.eritrociti==null) ? "" :gpis.get(i).analysis5.eritrociti}</td>
                                    <td>${gpis.get(i).analysis5.leikociti}</td>
                                    <td>${gpis.get(i).analysis5.palochkoyadernaya}</td>
                                    <td>${gpis.get(i).analysis5.segmentarnoyadernaya}</td>
                                    <td>${gpis.get(i).analysis5.yunie}</td>
                                    <td>${gpis.get(i).analysis5.monociti}</td>
                                    <td>${gpis.get(i).analysis5.limfociti}</td>
                                    <td>${gpis.get(i).analysis5.trombociti}</td>
                                    <td>${gpis.get(i).analysis5.soe}</td>
                                    <td>${gpis.get(i).analysis5.ph}</td>
                                    <td>${gpis.get(i).analysis5.belok}</td>
                                    <td>${gpis.get(i).analysis5.leikocitiMochi}</td>
                                    <td>${gpis.get(i).analysis5.eritrocitiMochi}</td>
                                    <td>${gpis.get(i).analysis5.glukoza}</td>
                                    <td>${gpis.get(i).analysis5.ketoni}</td>
                                    <td>${gpis.get(i).analysis5.ast}</td>
                                    <td>${gpis.get(i).analysis5.alt}</td>
                                    <td>${gpis.get(i).analysis5.obshiiBelok}</td>
                                    <td>${gpis.get(i).analysis5.albunin}</td>
                                    <td>${gpis.get(i).analysis5.obshiibilirubin}</td>
                                    <td>${gpis.get(i).analysis5.pryamoibilirubin}</td>
                                    <td>${gpis.get(i).analysis5.kreatinin}</td>
                                    <td>${gpis.get(i).analysis5.sacharKrovi}</td>
                                    <td>${gpis.get(i).analysis5.mochevayaKislota}</td>
                                    <td>${gpis.get(i).analysis5.mochevina}</td>
                                    <td>${gpis.get(i).analysis5.mno}</td>
                                    <td>${gpis.get(i).analysis5.achtv}</td>
                                    <td>${(gpis.get(i).analysis10.gemoglabin==null) ? "" :gpis.get(i).analysis10.gemoglabin}</td>
                                    <td>${(gpis.get(i).analysis10.eritrociti==null) ? "" :gpis.get(i).analysis10.eritrociti}</td>
                                    <td>${gpis.get(i).analysis10.leikociti}</td>
                                    <td>${gpis.get(i).analysis10.palochkoyadernaya}</td>
                                    <td>${gpis.get(i).analysis10.segmentarnoyadernaya}</td>
                                    <td>${gpis.get(i).analysis10.yunie}</td>
                                    <td>${gpis.get(i).analysis10.monociti}</td>
                                    <td>${gpis.get(i).analysis10.limfociti}</td>
                                    <td>${gpis.get(i).analysis10.trombociti}</td>
                                    <td>${gpis.get(i).analysis10.soe}</td>
                                    <td>${gpis.get(i).analysis10.ph}</td>
                                    <td>${gpis.get(i).analysis10.belok}</td>
                                    <td>${gpis.get(i).analysis10.leikocitiMochi}</td>
                                    <td>${gpis.get(i).analysis10.eritrocitiMochi}</td>
                                    <td>${gpis.get(i).analysis10.glukoza}</td>
                                    <td>${gpis.get(i).analysis10.ketoni}</td>
                                    <td>${gpis.get(i).analysis10.ast}</td>
                                    <td>${gpis.get(i).analysis10.alt}</td>
                                    <td>${gpis.get(i).analysis10.obshiiBelok}</td>
                                    <td>${gpis.get(i).analysis10.albunin}</td>
                                    <td>${gpis.get(i).analysis10.obshiibilirubin}</td>
                                    <td>${gpis.get(i).analysis10.pryamoibilirubin}</td>
                                    <td>${gpis.get(i).analysis10.kreatinin}</td>
                                    <td>${gpis.get(i).analysis10.sacharKrovi}</td>
                                    <td>${gpis.get(i).analysis10.mochevayaKislota}</td>
                                    <td>${gpis.get(i).analysis10.mochevina}</td>
                                    <td>${gpis.get(i).analysis10.mno}</td>
                                    <td>${gpis.get(i).analysis10.achtv}</td>
                                    <td>${(gpis.get(i).analysis10.cea == 1) ? gpis.get(i).analysis10.ceaValue :""}</td>
                                    <td>${(gpis.get(i).analysis10.onk199 == 1) ? gpis.get(i).analysis10.onk199Value :""}</td>
                                    <td>${(gpis.get(i).analysis10.alfoFetoprotein == 1) ? gpis.get(i).analysis10.alfoFetValue :""}</td>
                                    <td>${(gpis.get(i).analysis10.ca125 == 1) ? gpis.get(i).analysis10.ca125Value :""}</td>
                                    <td>${gpis.get(i).objectiveExamination.patientState}</td>
                                    <td>${gpis.get(i).objectiveExamination.temperature}</td>
                                    <td>${gpis.get(i).objectiveExamination.nalichieZheltuchi}</td>
                                    <td>${gpis.get(i).objectiveExamination.odishka}</td>
                                    <td>${gpis.get(i).objectiveExamination.puls}</td>
                                    <td>${gpis.get(i).objectiveExamination.tachikardia}</td>
                                    <td>${(gpis.get(i).objectiveExamination.davlenie == null) ? "" : gpis.get(i).objectiveExamination.davlenie}</td>
                                    <td>${gpis.get(i).objectiveExamination.nalichieToshnoti}</td>
                                    <td>${gpis.get(i).objectiveExamination.rvota}</td>
                                    <td>${gpis.get(i).objectiveExamination.zhivot}</td>
                                    <td>${gpis.get(i).objectiveExamination.boleznenostZhivota}</td>
                                    <td>${gpis.get(i).objectiveExamination.napryazhenieZhivota}</td>
                                    <td>${gpis.get(i).objectiveExamination.peritorialSymptomi}</td>
                                    <td>${gpis.get(i).objectiveExamination.peristaltika}</td>
                                    <td>${gpis.get(i).objectiveExamination.otchozhdenieGazov}</td>
                                    <td>${gpis.get(i).objectiveExamination.stul}</td>
                                    <td>${gpis.get(i).objectiveExamination.dizuria}</td>
                                    <td>${gpis.get(i).objectiveExamination.objemMochi}</td>
                                    <td>${gpis.get(i).alveokokkoz.grwr}</td>
                                    <td>${gpis.get(i).alveokokkoz.icg}</td>
                                    <td>${gpis.get(i).alveokokkoz.icgk_f}</td>
                                    <td>${gpis.get(i).alveokokkoz.vorotInvasia}</td>
                                    <td>${gpis.get(i).alveokokkoz.pechInvasia}</td>
                                    <td>${gpis.get(i).alveokokkoz.polozhKultura}</td>
                                    <td>${gpis.get(i).alveokokkoz.trofStatus}</td>
                                    <td>${gpis.get(i).alveokokkoz.rezekciaType}</td>
                                    <td>${gpis.get(i).alveokokkoz.indexZachvata}</td>
                                    <td>${gpis.get(i).alveokokkoz.objemOstatka}</td>
                                    <td>${gpis.get(i).alveokokkoz.albendazol}</td>
                                    <td>${gpis.get(i).alveokokkoz.predOperacii}</td>
                                    <td>${gpis.get(i).echinokokkoz.trofStatus}</td>
                                    <td>${gpis.get(i).echinokokkoz.voz}</td>
                                    <td>${(gpis.get(i).echinokokkoz!=null)?((gpis.get(i).echinokokkoz.antitela) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).echinokokkoz.antitelaUroven}</td>
                                    <td>${gpis.get(i).echinokokkoz.antitelaRes}</td>
                                    <td>${gpis.get(i).echinokokkoz.epidemAnamnez}</td>
                                    <td>${(gpis.get(i).echinokokkoz!=null)?((gpis.get(i).echinokokkoz.mnozhKisty) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).echinokokkoz.kistyNumber}</td>
                                    <td>${(gpis.get(i).echinokokkoz!=null)?((gpis.get(i).echinokokkoz.kistyType) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).echinokokkoz.kistyRazmer}</td>
                                    <td>${gpis.get(i).echinokokkoz.kistyOsl}</td>
                                    <td>${gpis.get(i).echinokokkoz.kratLechenia}</td>
                                    <td>${gpis.get(i).echinokokkoz.lechenieType}</td>
                                    <td>${gpis.get(i).echinokokkoz.vnepPorazhenie}</td>
                                    <td>${gpis.get(i).gcr.icg}</td>
                                    <td>${gpis.get(i).gcr.icgk_f}</td>
                                    <td>${gpis.get(i).gcr.trofStatus}</td>
                                    <td>${gpis.get(i).gcr.etiologiaCirroza}</td>
                                    <td>${gpis.get(i).gcr.gepatitStepen}</td>
                                    <td>${gpis.get(i).gcr.fibrozClass}</td>
                                    <td>${gpis.get(i).gcr.opuchUzliType}</td>
                                    <td>${gpis.get(i).gcr.razmerOpucholi}</td>
                                    <td>${gpis.get(i).gcr.chisloOpuchUzlov}</td>
                                    <td>${gpis.get(i).gcr.sosudInvasia}</td>
                                    <td>${gpis.get(i).gcr.vnepOchagiType}</td>
                                    <td>${gpis.get(i).gcr.trombPechType}</td>
                                    <td>${gpis.get(i).gcr.rezekciaType}</td>
                                    <td>${gpis.get(i).gcr.indexZachvata}</td>
                                    <td>${gpis.get(i).gcr.objemOstatka}</td>
                                    <td>${(gpis.get(i).gcr!=null)?((gpis.get(i).gcr.protivTerapia) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).gcr.trombozVorotVeni}</td>
                                    <td>${(gpis.get(i).gcr!=null)?((gpis.get(i).gcr.lechenieSorafinib) ? 1 : 0):""}</td>
                                    <td>${(gpis.get(i).gcr!=null)?((gpis.get(i).gcr.tache) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).gcr.bclcType}</td>
                                    <td>${gpis.get(i).gcr.cirrozClass}</td>
                                    <td>${gpis.get(i).gcr.vrvStepen}</td>
                                    <td>${gpis.get(i).gcr.trombozStepen}</td>
                                    <td>${gpis.get(i).hcr.grwr}</td>
                                    <td>${gpis.get(i).hcr.icg}</td>
                                    <td>${gpis.get(i).hcr.icgk_f}</td>
                                    <td>${gpis.get(i).hcr.trofStatus}</td>
                                    <td>${gpis.get(i).hcr.razmerOpucholi}</td>
                                    <td>${gpis.get(i).hcr.opuchUzliType}</td>
                                    <td>${gpis.get(i).hcr.chisloOpuchUzlov}</td>
                                    <td>${gpis.get(i).hcr.sosudInvasia}</td>
                                    <td>${gpis.get(i).hcr.vnepOchagiType}</td>
                                    <td>${gpis.get(i).hcr.trombPechType}</td>
                                    <td>${gpis.get(i).hcr.rezekciaType}</td>
                                    <td>${gpis.get(i).hcr.indexZachvata}</td>
                                    <td>${gpis.get(i).hcr.objemOstatka}</td>
                                    <td>${(gpis.get(i).hcr!=null)?((gpis.get(i).hcr.protivTerapia) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).hcr.trombozVorotVeni}</td>
                                    <td>${gpis.get(i).mkr.icg}</td>
                                    <td>${gpis.get(i).mkr.icgk_f}</td>
                                    <td>${gpis.get(i).mkr.trofStatus}</td>
                                    <td>${gpis.get(i).mkr.razmerOpucholi}</td>
                                    <td>${gpis.get(i).mkr.opuchUzliType}</td>
                                    <td>${gpis.get(i).mkr.chisloOpuchUzlov}</td>
                                    <td>${gpis.get(i).mkr.sosudInvasia}</td>
                                    <td>${gpis.get(i).mkr.vnepOchagiType}</td>
                                    <td>${gpis.get(i).mkr.trombPechType}</td>
                                    <td>${gpis.get(i).mkr.rezekciaType}</td>
                                    <td>${gpis.get(i).mkr.indexZachvata}</td>
                                    <td>${gpis.get(i).mkr.objemOstatka}</td>
                                    <td>${gpis.get(i).mkr.trombozVorotVeni}</td>
                                    <td>${gpis.get(i).mkr.nalVnepMet}</td>
                                    <td>${gpis.get(i).mkr.pervOpuch}</td>
                                    <td>${gpis.get(i).mkr.stadPervOpuch}</td>
                                    <td>${gpis.get(i).mkr.obOPerPerOpuch}</td>
                                    <td>${(gpis.get(i).mkr!=null)?((gpis.get(i).mkr.nalNeoadChimioTer) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).mkr.chimiotKurs}</td>
                                    <td>${gpis.get(i).mkr.chimiotPrep}</td>
                                    <td>${gpis.get(i).mkr.chimiotEffect}</td>
                                    <td>${gpis.get(i).mkr.pojavMetType}</td>
                                    <td>${gpis.get(i).mkr.pojavMetTime}</td>
                                    <td>${gpis.get(i).mnkr.icg}</td>
                                    <td>${gpis.get(i).mnkr.icgk_f}</td>
                                    <td>${gpis.get(i).mnkr.trofStatus}</td>
                                    <td>${gpis.get(i).mnkr.metGiso}</td>
                                    <td>${gpis.get(i).mnkr.metNeiroOpuch}</td>
                                    <td>${gpis.get(i).mnkr.razmerOpucholi}</td>
                                    <td>${gpis.get(i).mnkr.opuchUzliType}</td>
                                    <td>${gpis.get(i).mnkr.chisloOpuchUzlov}</td>
                                    <td>${gpis.get(i).mnkr.sosudInvasia}</td>
                                    <td>${gpis.get(i).mnkr.vnepOchagiType}</td>
                                    <td>${gpis.get(i).mnkr.trombPechType}</td>
                                    <td>${gpis.get(i).mnkr.rezekciaType}</td>
                                    <td>${gpis.get(i).mnkr.indexZachvata}</td>
                                    <td>${gpis.get(i).mnkr.objemOstatka}</td>
                                    <td>${gpis.get(i).mnkr.trombozVorotVeni}</td>
                                    <td>${gpis.get(i).mnkr.nalVnepMet}</td>
                                    <td>${gpis.get(i).mnkr.pervOpuchType}</td>
                                    <td>${(gpis.get(i).mnkr!=null)?((gpis.get(i).mnkr.nalNeoadChimioTer) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).mnkr.chimiotKurs}</td>
                                    <td>${gpis.get(i).mnkr.chimiotEffect}</td>
                                    <td>${gpis.get(i).mnkr.pojavMetType}</td>
                                    <td>${gpis.get(i).mnkr.pojavMetTime}</td>
                                    <td>${gpis.get(i).klatskin.grwr}</td>
                                    <td>${gpis.get(i).klatskin.icg}</td>
                                    <td>${gpis.get(i).klatskin.icgk_f}</td>
                                    <td>${gpis.get(i).klatskin.bismuth}</td>
                                    <td>${gpis.get(i).klatskin.vorotInvasia}</td>
                                    <td>${gpis.get(i).klatskin.pechInvasia}</td>
                                    <td>${gpis.get(i).klatskin.differOpuchol}</td>
                                    <td>${gpis.get(i).klatskin.razmer_opucholi}</td>
                                    <td>${(gpis.get(i).klatskin!=null)?((gpis.get(i).klatskin.perinevInvasia) ? 1 : 0):""}</td>
                                    <td>${(gpis.get(i).klatskin!=null)?((gpis.get(i).klatskin.polozhKultura) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).klatskin.trofStatus}</td>
                                    <td>${gpis.get(i).klatskin.rezekciaType}</td>
                                    <td>${gpis.get(i).klatskin.indexZachvata}</td>
                                    <td>${gpis.get(i).klatskin.objemOstatka}</td>
                                    <td>${gpis.get(i).rzhp.icg}</td>
                                    <td>${gpis.get(i).rzhp.icgk_f}</td>
                                    <td>${gpis.get(i).rzhp.vorotInvasia}</td>
                                    <td>${gpis.get(i).rzhp.pechInvasia}</td>
                                    <td>${gpis.get(i).rzhp.differOpuchol}</td>
                                    <td>${(gpis.get(i).rzhp!=null)?((gpis.get(i).rzhp.perinevInvasia) ? 1 : 0):""}</td>
                                    <td>${(gpis.get(i).rzhp!=null)?((gpis.get(i).rzhp.polozhKultura) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).rzhp.trofStatus}</td>
                                    <td>${gpis.get(i).rzhp.rezekciaType}</td>
                                    <td>${gpis.get(i).rzhp.indexZachvata}</td>
                                    <td>${gpis.get(i).rzhp.objemOstatka}</td>
                                    <td>${gpis.get(i).rzhp.stadPervOpuch}</td>
                                    <td>${gpis.get(i).rzhp.diagnRaka}</td>

                                    <td>${gpis.get(i).zhkb.trof_status}</td>
                                    <td>${gpis.get(i).zhkb.cholecistit}</td>
                                    <td>${gpis.get(i).zhkb.ostriiCholecistit}</td>
                                    <td>${(gpis.get(i).zhkb.oslozhnenii!=null)?gpis.get(i).zhkb.oslozhnenii:""}</td>
                                    <td>${(gpis.get(i).zhkb.mirizi!=null)?gpis.get(i).zhkb.mirizi:""}</td>
                                    <td>${(gpis.get(i).zhkb.zhelt_mirizi!=null)?gpis.get(i).zhkb.zhelt_mirizi:""}</td>
                                    <td>${(gpis.get(i).zhkb.type!=null)?gpis.get(i).zhkb.type:""}</td>
                                    <td>${(gpis.get(i).zhkb.cholangiol!=null)?gpis.get(i).zhkb.cholangiol:""}</td>
                                    <td>${(gpis.get(i).zhkb.zhelt_cholang!=null)?gpis.get(i).zhkb.zhelt_cholang:""}</td>
                                    <td>${(gpis.get(i).zhkb.type_cholang!=null)?gpis.get(i).zhkb.type_cholang:""}</td>
                                    <td>${(gpis.get(i).zhkb.vodyanka!=null)?gpis.get(i).zhkb.vodyanka:""}</td>
                                    <td>${(gpis.get(i).zhkb.svich!=null)?gpis.get(i).zhkb.svich:""}</td>
                                    <td>${(gpis.get(i).zhkb.smorch!=null)?gpis.get(i).zhkb.smorch:""}</td>
                                    <td>${(gpis.get(i).zhkb.peritonit!=null)?gpis.get(i).zhkb.peritonit:""}</td>
                                    <td>${(gpis.get(i).zhkb.empiema!=null)?gpis.get(i).zhkb.empiema:""}</td>
                                    <td>${(gpis.get(i).zhkb.cholangit!=null)?gpis.get(i).zhkb.cholangit:""}</td>
                                    <td>${(gpis.get(i).zhkb.cholpankreotit!=null)?gpis.get(i).zhkb.cholpankreotit:""}</td>
                                    
                                    
                                    
                                    <td>${gpis.get(i).alveokokkozEnd.grwr}</td>
                                    <td>${gpis.get(i).alveokokkozEnd.icg}</td>
                                    <td>${gpis.get(i).alveokokkozEnd.icgk_f}</td>
                                    <td>${gpis.get(i).alveokokkozEnd.vorotInvasia}</td>
                                    <td>${gpis.get(i).alveokokkozEnd.pechInvasia}</td>
                                    <td>${gpis.get(i).echinokokkozEnd.voz}</td>
                                    <td>${(gpis.get(i).echinokokkozEnd.antitela!=null)?((gpis.get(i).echinokokkozEnd.antitela) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).echinokokkozEnd.antitelaUroven}</td>
                                    <td>${gpis.get(i).echinokokkozEnd.antitelaRes}</td>
                                    <td>${(gpis.get(i).echinokokkozEnd.mnozhKisty!=null)?((gpis.get(i).echinokokkozEnd.mnozhKisty) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).echinokokkozEnd.kistyNumber}</td>
                                    <td>${(gpis.get(i).echinokokkozEnd.kistyType!=null)?((gpis.get(i).echinokokkozEnd.kistyType) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).echinokokkozEnd.kistyRazmer}</td>
                                    <td>${gpis.get(i).echinokokkozEnd.kistyOsl}</td>
                                    <td>${gpis.get(i).echinokokkozEnd.vnepPorazhenie}</td>
                                    <td>${gpis.get(i).gcrEnd.icg}</td>
                                    <td>${gpis.get(i).gcrEnd.icgk_f}</td>
                                    <td>${gpis.get(i).gcrEnd.etiologiaCirroza}</td>
                                    <td>${gpis.get(i).gcrEnd.gepatitStepen}</td>
                                    <td>${gpis.get(i).gcrEnd.fibrozClass}</td>
                                    <td>${gpis.get(i).gcrEnd.opuchUzliType}</td>
                                    <td>${gpis.get(i).gcrEnd.razmerOpucholi}</td>
                                    <td>${gpis.get(i).gcrEnd.chisloOpuchUzlov}</td>
                                    <td>${gpis.get(i).gcrEnd.sosudInvasia}</td>
                                    <td>${gpis.get(i).gcrEnd.vnepOchagiType}</td>
                                    <td>${gpis.get(i).gcrEnd.trombPechType}</td>
                                    <td>${gpis.get(i).gcrEnd.trombozVorotVeni}</td>
                                    <td>${gpis.get(i).gcrEnd.bclcType}</td>
                                    <td>${gpis.get(i).gcrEnd.cirrozClass}</td>
                                    <td>${gpis.get(i).gcrEnd.vrvStepen}</td>
                                    <td>${gpis.get(i).gcrEnd.trombozStepen}</td>
                                    <td>${gpis.get(i).hcrEnd.grwr}</td>
                                    <td>${gpis.get(i).hcrEnd.icg}</td>
                                    <td>${gpis.get(i).hcrEnd.icgk_f}</td>
                                    <td>${gpis.get(i).hcrEnd.razmerOpucholi}</td>
                                    <td>${gpis.get(i).hcrEnd.opuchUzliType}</td>
                                    <td>${gpis.get(i).hcrEnd.chisloOpuchUzlov}</td>
                                    <td>${gpis.get(i).hcrEnd.sosudInvasia}</td>
                                    <td>${gpis.get(i).hcrEnd.vnepOchagiType}</td>
                                    <td>${gpis.get(i).hcrEnd.trombPechType}</td>
                                    <td>${gpis.get(i).hcrEnd.trombozVorotVeni}</td>
                                    <td>${gpis.get(i).mkrEnd.icg}</td>
                                    <td>${gpis.get(i).mkrEnd.icgk_f}</td>
                                    <td>${gpis.get(i).mkrEnd.razmerOpucholi}</td>
                                    <td>${gpis.get(i).mkrEnd.opuchUzliType}</td>
                                    <td>${gpis.get(i).mkrEnd.chisloOpuchUzlov}</td>
                                    <td>${gpis.get(i).mkrEnd.sosudInvasia}</td>
                                    <td>${gpis.get(i).mkrEnd.vnepOchagiType}</td>
                                    <td>${gpis.get(i).mkrEnd.trombPechType}</td>
                                    <td>${gpis.get(i).mkrEnd.trombozVorotVeni}</td>
                                    <td>${gpis.get(i).mkrEnd.nalVnepMet}</td>
                                    <td>${gpis.get(i).mkrEnd.pervOpuch}</td>
                                    <td>${gpis.get(i).mkrEnd.stadPervOpuch}</td>
                                    <td>${gpis.get(i).mkrEnd.obOPerPerOpuch}</td>
                                    <td>${gpis.get(i).mnkrEnd.icg}</td>
                                    <td>${gpis.get(i).mnkrEnd.icgk_f}</td>
                                    <td>${gpis.get(i).mnkrEnd.metGiso}</td>
                                    <td>${gpis.get(i).mnkrEnd.metNeiroOpuch}</td>
                                    <td>${gpis.get(i).mnkrEnd.razmerOpucholi}</td>
                                    <td>${gpis.get(i).mnkrEnd.opuchUzliType}</td>
                                    <td>${gpis.get(i).mnkrEnd.chisloOpuchUzlov}</td>
                                    <td>${gpis.get(i).mnkrEnd.sosudInvasia}</td>
                                    <td>${gpis.get(i).mnkrEnd.vnepOchagiType}</td>
                                    <td>${gpis.get(i).mnkrEnd.trombPechType}</td>
                                    <td>${gpis.get(i).mnkrEnd.trombozVorotVeni}</td>
                                    <td>${gpis.get(i).mnkrEnd.nalVnepMet}</td>
                                    <td>${gpis.get(i).mnkr.pervOpuchType}</td>
                                    <td>${gpis.get(i).klatskinEnd.grwr}</td>
                                    <td>${gpis.get(i).klatskinEnd.icg}</td>
                                    <td>${gpis.get(i).klatskinEnd.icgk_f}</td>
                                    <td>${gpis.get(i).klatskinEnd.bismuth}</td>
                                    <td>${gpis.get(i).klatskinEnd.vorotInvasia}</td>
                                    <td>${gpis.get(i).klatskinEnd.pechInvasia}</td>
                                    <td>${gpis.get(i).klatskinEnd.differOpuchol}</td>
                                    <td>${(gpis.get(i).klatskin!=null)?((gpis.get(i).klatskinEnd.perinevInvasia) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).rzhpEnd.icg}</td>
                                    <td>${gpis.get(i).rzhpEnd.icgk_f}</td>
                                    <td>${gpis.get(i).rzhpEnd.vorotInvasia}</td>
                                    <td>${gpis.get(i).rzhpEnd.pechInvasia}</td>
                                    <td>${gpis.get(i).rzhpEnd.differOpuchol}</td>
                                    <td>${(gpis.get(i).rzhp!=null)?((gpis.get(i).rzhpEnd.perinevInvasia) ? 1 : 0):""}</td>
                                    <td>${gpis.get(i).rzhpEnd.stadPervOpuch}</td>
                                    <td>${gpis.get(i).rzhpEnd.diagnRaka}</td>
                                    <td>${gpis.get(i).opalv.rezekciaType}</td>
                                    <td>${gpis.get(i).opalv.razdelenieTkanei}</td>
                                    <td>${gpis.get(i).opalv.rezekcia}</td>
                                    <td>${gpis.get(i).opalv.alvsosudrezekcia}</td>
                                    <td>${gpis.get(i).opalv.alvvorotrezekcia}</td>
                                    <td>${gpis.get(i).opalv.alvkrovopoteria}</td>
                                    <td>${gpis.get(i).opalv.alvdlitop}</td>
                                    <td>${gpis.get(i).opalv.alvgemotransfusia}</td>
                                    <td>${gpis.get(i).opalv.alvgemotransfusia_ml}</td>
                                    <td>${gpis.get(i).opalv.plazma}</td>
                                    <td>${gpis.get(i).opalv.gdc}</td>
                                    <td>${gpis.get(i).opalv.alvgdcmin}</td>
                                    <td>${gpis.get(i).opalv.geatype1}</td>
                                    <td>${gpis.get(i).opalv.geatype2}</td>
                                    <td>${gpis.get(i).opalv.geatype3}</td>
                                    <td>${gpis.get(i).opalv.radikal}</td>
                                    <td>${gpis.get(i).opalv.limfodissekcia}</td>
                                    <td>${gpis.get(i).opalv.tnm}</td>
                                    <td>${gpis.get(i).opalv.pnm}</td>
                                    <td>${gpis.get(i).opalv.bypass}</td>
                                    <td>${gpis.get(i).opalv.totIz}</td>
                                    <td>${gpis.get(i).opalv.selIz}</td>
                                    <td>${gpis.get(i).opalv.rezDiafragmi}</td>
                                    <td>${gpis.get(i).opalv.osl}</td>
                                    <td>${gpis.get(i).opalv.clavien}</td>
                                    <td>${gpis.get(i).opech.rezekciaType}</td>
                                    <td>${gpis.get(i).opech.razdelenieTkanei}</td>
                                    <td>${gpis.get(i).opech.rezekcia}</td>
                                    <td>${gpis.get(i).opech.dlitop}</td>
                                    <td>${gpis.get(i).opech.echgdc}</td>
                                    <td>${gpis.get(i).opech.echgdcmin}</td>
                                    <td>${gpis.get(i).opech.bypass}</td>
                                    <td>${gpis.get(i).opech.totIz}</td>
                                    <td>${gpis.get(i).opech.selIz}</td>
                                    <td>${gpis.get(i).opech.osl}</td>
                                    <td>${gpis.get(i).opech.clavien}</td>
                                    <td>${gpis.get(i).opech.otkrEch}</td>
                                    <td>${gpis.get(i).opech.obrabotkaFibr}</td>
                                    <td>${gpis.get(i).opech.obrabotkaSvich}</td>
                                    <td>${gpis.get(i).opech.chirurgLech}</td>
                                    <td>${gpis.get(i).opech.etapLech}</td>
                                    <td>${gpis.get(i).opech.simOper}</td>
                                    <td>${gpis.get(i).opech.intraOsl}</td>
                                    <td>${gpis.get(i).opgcr.rezekciaType}</td>
                                    <td>${gpis.get(i).opgcr.razdelenieTkanei}</td>
                                    <td>${gpis.get(i).opgcr.krovopoteria}</td>
                                    <td>${gpis.get(i).opgcr.dlitop}</td>
                                    <td>${gpis.get(i).opgcr.gemotransfusia}</td>
                                    <td>${gpis.get(i).opgcr.gcrgemotransfusia_ml}</td>
                                    <td>${gpis.get(i).opgcr.plazma}</td>
                                    <td>${gpis.get(i).opgcr.gdc}</td>
                                    <td>${gpis.get(i).opgcr.gcrgdcmin}</td>
                                    <td>${gpis.get(i).opgcr.tnm}</td>
                                    <td>${gpis.get(i).opgcr.bypass}</td>
                                    <td>${gpis.get(i).opgcr.totIz}</td>
                                    <td>${gpis.get(i).opgcr.selSosIz}</td>
                                    <td>${gpis.get(i).opgcr.osl}</td>
                                    <td>${gpis.get(i).opgcr.clavien}</td>
                                    <td>${gpis.get(i).opgcr.gcroplech}</td>
                                    <td>${gpis.get(i).opgcr.gcrobrezekcii}</td>
                                    <td>${gpis.get(i).opgcr.gcrsegrez}</td>
                                    <td>${gpis.get(i).opgcr.gcrchisloUdSeg}</td>
                                    <td>${gpis.get(i).opgcr.gcrchisloOch}</td>
                                    <td>${gpis.get(i).opgcr.gcrrazOch}</td>
                                    <td>${gpis.get(i).opgcr.gcrblizSosud}</td>
                                    <td>${gpis.get(i).opgcr.gcrpolnRHA}</td>
                                    <td>${gpis.get(i).opgcr.gcralkChisloOch}</td>
                                    <td>${gpis.get(i).opgcr.gcralkRazOch}</td>
                                    <td>${gpis.get(i).opgcr.perPolVeni}</td>
                                    <td>${gpis.get(i).opgcr.gcrperPolVeniMin}</td>
                                    <td>${gpis.get(i).opgcr.selIz}</td>
                                    <td>${gpis.get(i).opgcr.gcrselIzMin}</td>
                                    <td>${gpis.get(i).opgcr.intrRHA}</td>
                                    <td>${gpis.get(i).opgcr.tache}</td>
                                    <td>${gpis.get(i).ophcr.rezekciaType}</td>
                                    <td>${gpis.get(i).ophcr.razdelenieTkanei}</td>
                                    <td>${gpis.get(i).ophcr.krovopoteria}</td>
                                    <td>${gpis.get(i).ophcr.dlitop}</td>
                                    <td>${gpis.get(i).ophcr.gemotransfusia}</td>
                                    <td>${gpis.get(i).ophcr.hcrgemotransfusia_ml}</td>
                                    <td>${gpis.get(i).ophcr.plazma}</td>
                                    <td>${gpis.get(i).ophcr.gdc}</td>
                                    <td>${gpis.get(i).ophcr.hcrgdcmin}</td>
                                    <td>${gpis.get(i).ophcr.tnm}</td>
                                    <td>${gpis.get(i).ophcr.bypass}</td>
                                    <td>${gpis.get(i).ophcr.totIz}</td>
                                    <td>${gpis.get(i).ophcr.selSosIz}</td>
                                    <td>${gpis.get(i).ophcr.osl}</td>
                                    <td>${gpis.get(i).ophcr.clavien}</td>
                                    <td>${gpis.get(i).ophcr.hcroplech}</td>
                                    <td>${gpis.get(i).ophcr.hcrobrezekcii}</td>
                                    <td>${gpis.get(i).ophcr.hcrsegrez}</td>
                                    <td>${gpis.get(i).ophcr.hcrchisloUdSeg}</td>
                                    <td>${gpis.get(i).ophcr.hcrchisloOch}</td>
                                    <td>${gpis.get(i).ophcr.hcrrazOch}</td>
                                    <td>${gpis.get(i).ophcr.hcrblizSosud}</td>
                                    <td>${gpis.get(i).ophcr.hcrpolnRHA}</td>
                                    <td>${gpis.get(i).ophcr.perPolVeni}</td>
                                    <td>${gpis.get(i).ophcr.hcrperPolVeniMin}</td>
                                    <td>${gpis.get(i).ophcr.selIz}</td>
                                    <td>${gpis.get(i).ophcr.hcrselIzMin}</td>
                                    <td>${gpis.get(i).ophcr.intrRHA}</td>
                                    <td>${gpis.get(i).opmkr.rezekciaType}</td>
                                    <td>${gpis.get(i).opmkr.razdelenieTkanei}</td>
                                    <td>${gpis.get(i).opmkr.dlitop}</td>
                                    <td>${gpis.get(i).opmkr.sochIntrRHA}</td>
                                    <td>${gpis.get(i).opmkr.gdc}</td>
                                    <td>${gpis.get(i).opmkr.mkrgdcmin}</td>
                                    <td>${gpis.get(i).opmkr.limfodissekcia}</td>
                                    <td>${gpis.get(i).opmkr.bypass}</td>
                                    <td>${gpis.get(i).opmkr.totIz}</td>
                                    <td>${gpis.get(i).opmkr.selSosIz}</td>
                                    <td>${gpis.get(i).opmkr.osl}</td>
                                    <td>${gpis.get(i).opmkr.clavien}</td>
                                    <td>${gpis.get(i).opmkr.simultOp}</td>
                                    <td>${gpis.get(i).opmkr.mkrobrezekcii}</td>
                                    <td>${gpis.get(i).opmkr.mkrsegrez}</td>
                                    <td>${gpis.get(i).opmkr.mkrchisloUdSeg}</td>
                                    <td>${gpis.get(i).opmkr.oplech}</td>
                                    <td>${gpis.get(i).opmkr.varRezekcii}</td>
                                    <td>${gpis.get(i).opmkr.nalRHA}</td>
                                    <td>${gpis.get(i).opmkr.rekTKish}</td>
                                    <td>${gpis.get(i).opmnkr.rezekciaType}</td>
                                    <td>${gpis.get(i).opmnkr.razdelenieTkanei}</td>
                                    <td>${gpis.get(i).opmnkr.dlitop}</td>
                                    <td>${gpis.get(i).opmnkr.sochIntraRHA}</td>
                                    <td>${gpis.get(i).opmnkr.gdc}</td>
                                    <td>${gpis.get(i).opmnkr.mnkrgdcmin}</td>
                                    <td>${gpis.get(i).opmnkr.limfodissekcia}</td>
                                    <td>${gpis.get(i).opmnkr.bypass}</td>
                                    <td>${gpis.get(i).opmnkr.totIz}</td>
                                    <td>${gpis.get(i).opmnkr.selSosIz}</td>
                                    <td>${gpis.get(i).opmnkr.osl}</td>
                                    <td>${gpis.get(i).opmnkr.clavien}</td>
                                    <td>${gpis.get(i).opmnkr.simultOp}</td>
                                    <td>${gpis.get(i).opmnkr.mnkrobrezekcii}</td>
                                    <td>${gpis.get(i).opmnkr.mnkrsegrez}</td>
                                    <td>${gpis.get(i).opmnkr.mnkrchisloUdSeg}</td>
                                    <td>${gpis.get(i).opmnkr.stadTnm}</td>
                                    <td>${gpis.get(i).opmnkr.chimiot}</td>
                                    <td>${gpis.get(i).opmnkr.kratRez}</td>
                                    <td>${gpis.get(i).opmnkr.varRezekcii}</td>
                                    <td>${gpis.get(i).opmnkr.nalRHA}</td>
                                    <td>${gpis.get(i).opmnkr.rekOpTKish}</td>
                                    <td>${gpis.get(i).opmnkr.oplech}</td>
                                    <td>${gpis.get(i).opkl.rezekciaType}</td>
                                    <td>${gpis.get(i).opkl.razdelenieTkanei}</td>
                                    <td>${gpis.get(i).opkl.rezekcia}</td>
                                    <td>${gpis.get(i).opkl.klatsosudrezekcia}</td>
                                    <td>${gpis.get(i).opkl.klatvorotrezekcia}</td>
                                    <td>${gpis.get(i).opkl.krovopoteria}</td>
                                    <td>${gpis.get(i).opkl.dlitop}</td>
                                    <td>${gpis.get(i).opkl.gemotransfusia}</td>
                                    <td>${gpis.get(i).opkl.klatgemotransfusia_ml}</td>
                                    <td>${gpis.get(i).opkl.plazma}</td>
                                    <td>${gpis.get(i).opkl.gdc}</td>
                                    <td>${gpis.get(i).opkl.klatgdcmin}</td>
                                    <td>${gpis.get(i).opkl.geatype1}</td>
                                    <td>${gpis.get(i).opkl.geatype2}</td>
                                    <td>${gpis.get(i).opkl.geatype3}</td>
                                    <td>${gpis.get(i).opkl.radikal}</td>
                                    <td>${gpis.get(i).opkl.limfodissekcia}</td>
                                    <td>${gpis.get(i).opkl.tnm}</td>
                                    <td>${gpis.get(i).opkl.bypass}</td>
                                    <td>${gpis.get(i).opkl.totIz}</td>
                                    <td>${gpis.get(i).opkl.selIz}</td>
                                    <td>${gpis.get(i).opkl.osl}</td>
                                    <td>${gpis.get(i).opkl.clavien}</td>
                                    <td>${gpis.get(i).oprzhp.rezekciaType}</td>
                                    <td>${gpis.get(i).oprzhp.razdelenieTkanei}</td>
                                    <td>${gpis.get(i).oprzhp.rezekcia}</td>
                                    <td>${gpis.get(i).oprzhp.rzhpsosudrezekcia}</td>
                                    <td>${gpis.get(i).oprzhp.rzhpvorotrezekcia}</td>
                                    <td>${gpis.get(i).oprzhp.krovopoteria}</td>
                                    <td>${gpis.get(i).oprzhp.dlitop}</td>
                                    <td>${gpis.get(i).oprzhp.gemotransfusia}</td>
                                    <td>${gpis.get(i).oprzhp.rzhpgemotransfusia_ml}</td>
                                    <td>${gpis.get(i).oprzhp.plazma}</td>
                                    <td>${gpis.get(i).oprzhp.gdc}</td>
                                    <td>${gpis.get(i).oprzhp.rzhpgdcmin}</td>
                                    <td>${gpis.get(i).oprzhp.geatype1}</td>
                                    <td>${gpis.get(i).oprzhp.geatype2}</td>
                                    <td>${gpis.get(i).oprzhp.geatype3}</td>
                                    <td>${gpis.get(i).oprzhp.radikal}</td>
                                    <td>${gpis.get(i).oprzhp.limfodissekcia}</td>
                                    <td>${gpis.get(i).oprzhp.tnm}</td>
                                    <td>${gpis.get(i).oprzhp.bypass}</td>
                                    <td>${gpis.get(i).oprzhp.totIz}</td>
                                    <td>${gpis.get(i).oprzhp.selIz}</td>
                                    <td>${gpis.get(i).oprzhp.osl}</td>
                                    <td>${gpis.get(i).oprzhp.clavien}</td>
                                    <td>${gpis.get(i).oprzhp.kamni}</td>
                                    <td>${gpis.get(i).oprzhp.zhkb}</td>
                                    <td>${gpis.get(i).oprzhp.formRak}</td>
                                    <td>${gpis.get(i).oprzhp.lokalizaciaRaka}</td>

                                    <td>${gpis.get(i).opZhkb.dlitop}</td>
                                    <td>${gpis.get(i).opZhkb.cholecistekt}</td>
                                    <td>${(gpis.get(i).opZhkb.drenirovanie!=null)?gpis.get(i).opZhkb.drenirovanie:""}</td>
                                    <td>${gpis.get(i).opZhkb.drenMethod}</td>
                                    <td>${(gpis.get(i).opZhkb.primbram!=null)?gpis.get(i).opZhkb.primbram:""}</td>
                                    <td>${(gpis.get(i).opZhkb.infiltrat!=null)?gpis.get(i).opZhkb.infiltrat:""}</td>
                                    <td>${(gpis.get(i).opZhkb.vnutrRaspol!=null)?gpis.get(i).opZhkb.vnutrRaspol:""}</td>
                                    <td>${(gpis.get(i).opZhkb.zhelchPuz!=null)?gpis.get(i).opZhkb.zhelchPuz:""}</td>
                                    <td>${(gpis.get(i).opZhkb.kamen!=null)?gpis.get(i).opZhkb.kamen:""}</td>
                                    <td>${(gpis.get(i).opZhkb.svish!=null)?gpis.get(i).opZhkb.svish:""}</td>
                                    <td>${(gpis.get(i).opZhkb.svish!=null)?gpis.get(i).opZhkb.svishType:""}</td>
                                    <td>${(gpis.get(i).opZhkb.mirizi!=null)?gpis.get(i).opZhkb.mirizi:""}</td>
                                    <td>${(gpis.get(i).opZhkb.miriziZavershenie!=null)?gpis.get(i).opZhkb.miriziZavershenie:""}</td>
                                    <td>${(gpis.get(i).opZhkb.perfor!=null)?gpis.get(i).opZhkb.perfor:""}</td>
                                    <td>${(gpis.get(i).opZhkb.povrezhdeniePologo!=null)?gpis.get(i).opZhkb.povrezhdeniePologo:""}</td>
                                    <td>${(gpis.get(i).opZhkb.povrezhdeniePologo!=null)?gpis.get(i).opZhkb.povrezhdeniePologoType:""}</td>
                                    <td>${(gpis.get(i).opZhkb.povrezhdCholed!=null)?gpis.get(i).opZhkb.povrezhdCholed:""}</td>
                                    <td>${(gpis.get(i).opZhkb.povrezhdCholed!=null)?gpis.get(i).opZhkb.povrezhdCholedType:""}</td>
                                    <td>${(gpis.get(i).opZhkb.povrezhdArterii!=null)?gpis.get(i).opZhkb.povrezhdArterii:""}</td>
                                    <td>${(gpis.get(i).opZhkb.povrezhdVeni!=null)?gpis.get(i).opZhkb.povrezhdVeni:""}</td>
                                    
                                    <td>${gpis.get(i).drainNumber}</td>
                                    <td>${(gpis.get(i).hasOsl) ? 1 : 0}</td>
                                    <td>${gpis.get(i).dren.date}</td>
                                    <td>${gpis.get(i).dren.out_drenages}</td>
                                    <td>${gpis.get(i).dren.inout_drenages}</td>
                                    <td>${gpis.get(i).dren.block}</td>
                                    <td>${gpis.get(i).dren.longevity}</td>
                                    <td>${gpis.get(i).dren.previous_cholangit}</td>
                                    <td>${gpis.get(i).dren.absces_cholangit}</td>
                                    <td>${gpis.get(i).dren.dolya}</td>
                                    <td>${gpis.get(i).dren.svoracivanie}</td>
                                    <td>${gpis.get(i).dren.segment67}</td>
                                    <td>${gpis.get(i).dren.segment58}</td>
                                    <td>${gpis.get(i).dren.suprapapil}</td>
                                    <td>${gpis.get(i).dren.zh_posev}</td>
                                    <td>${gpis.get(i).dren.klebsiela}</td>
                                    <td>${gpis.get(i).dren.sinegnoin_palochka}</td>
                                    <td>${gpis.get(i).dren.kr_posev}</td>
                                    <td>${gpis.get(i).dren.osl}</td>
                                    <td>${(gpis.get(i).patient.deathDate==null) ? "" :Integer.valueOf(gpis.get(i).patient.deathDate.substring(0,2))}</td>
                                    <td>${(gpis.get(i).patient.deathDate==null) ? "" :Integer.valueOf(gpis.get(i).patient.deathDate.substring(3,5))}</td>
                                    <td>${(gpis.get(i).patient.deathDate==null) ? "" :gpis.get(i).patient.deathDate.substring(6,10)}</td>
                                    <td class="-1 hidden">${gpis.get(i).patient.id}</td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                    <!--End Table-->
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="result" id="result"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="pull-right">
                <button class="btn btn-success" id="export"><i class="fa fa-file-excel-o"></i> Экспорт в .xlsx-файл</button>
            </div>

            <div id="loading-image" class="pull-right" style="display: none;">
                <img src="<c:url value="/resources/images/ajax-loader.gif"/>" >
            </div>
        </div>
    </div>
</div>

<!--Modals-->
<!--Descriptive modal-->
<div class="modal fade" id="descriptive_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><i class="fa fa-bar-chart"></i>Описательная статистика</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <form role="form">
                                <label for="list">Переменные:</label>
                                <ul class="list-group pre-scrollable" id="list">
                                </ul>
                                <button type="submit" class="btn btn-default pull-left select_all_var">Выбрать все
                                </button>
                                <button type="submit" class="btn btn-default pull-right unselect_all_var">Убрать все
                                </button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form role="form">
                                <label for="list2">Вычислить:</label>
                                <ul class="list-group pre-scrollable" id="list2">
                                    <li class="list-group-item com 0">
                                        <small>Количество</small>
                                    </li>
                                    <li class="list-group-item com 1">
                                        <small>Среднее значение</small>
                                    </li>
                                    <li class="list-group-item com 2">
                                        <small>Стандартная ошибка</small>
                                    </li>
                                    <li class="list-group-item com 3">
                                        <small>Стандартное отклонение</small>
                                    </li>
                                    <li class="list-group-item com 4">
                                        <small>Математическое ожидание</small>
                                    </li>
                                    <li class="list-group-item com 5">
                                        <small>Минимум</small>
                                    </li>
                                    <li class="list-group-item com 6">
                                        <small>Максимум</small>
                                    </li>
                                    <li class="list-group-item com 7">
                                        <small>Медиана</small>
                                    </li>
                                    <li class="list-group-item com 8">
                                        <small>Мода</small>
                                    </li>
                                </ul>
                                <button type="submit" class="btn btn-default pull-left select_all_com">Выбрать все
                                </button>
                                <button type="submit" class="btn btn-default pull-right unselect_all_com">Убрать все
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span
                        class="glyphicon glyphicon-remove"></span>Отмена
                </button>
                <button type="submit" class="btn btn-success btn-default pull-right" data-dismiss="modal"
                        id="calc_desc">Вычислить
                </button>
            </div>
        </div>
    </div>
</div>
<!--2x2 modal-->
<div class="modal fade" id="2x2_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><i class="fa fa-bar-chart"></i>Таблицы частот 2х2</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                Введите частоты в таблицу частот 2х2.
                            </p>
                        </div>
                        <div class="col-md-4">
                            <form role="form">
                                <div class="input-group spinner">
                                    <input type="text" min="0" class="form-control" name="frequency" id="f1" value="0">
                                    <div class="input-group-btn-vertical">
                                        <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i>
                                        </button>
                                        <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="input-group spinner">
                                    <input type="text" min="0" class="form-control" name="frequency" id="f2" value="0">
                                    <div class="input-group-btn-vertical">
                                        <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i>
                                        </button>
                                        <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <form role="form">
                                <div class="input-group spinner">
                                    <input type="text" min="0" class="form-control" name="frequency" id="f3" value="0">
                                    <div class="input-group-btn-vertical">
                                        <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i>
                                        </button>
                                        <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="input-group spinner">
                                    <input type="text" min="0" class="form-control" name="frequency" id="f4" value="0">
                                    <div class="input-group-btn-vertical">
                                        <button class="btn btn-default" type="button"><i class="fa fa-caret-up"></i>
                                        </button>
                                        <button class="btn btn-default" type="button"><i class="fa fa-caret-down"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span
                        class="glyphicon glyphicon-remove"></span>Отмена
                </button>
                <button type="submit" class="btn btn-success btn-default pull-right" data-dismiss="modal" id="calc_22">
                    Вычислить
                </button>
            </div>
        </div>
    </div>
</div>


<!--Spirman-->
<div class="modal fade" id="spirman_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><i class="fa fa-bar-chart"></i>Коэффициент корреляции Спирмена</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <form role="form">
                                <label for="list3">Переменные:</label>
                                <ul class="list-group pre-scrollable" id="list3">
                                </ul>
                                <button type="submit" class="btn btn-default pull-left select_all_var">Выбрать все
                                </button>
                                <button type="submit" class="btn btn-default pull-right unselect_all_var">Убрать все
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span
                        class="glyphicon glyphicon-remove"></span>Отмена
                </button>
                <button type="submit" class="btn btn-success btn-default pull-right" data-dismiss="modal"
                        id="calc_spear">Вычислить
                </button>
            </div>
        </div>
    </div>
</div>

<!--U Manna-Uitni-->
<div class="modal fade" id="manna_uitni_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><i class="fa fa-bar-chart"></i>U-критерий Манна-Уитни</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <form role="form">
                                <label for="list4">Переменные:</label>
                                <ul class="list-group pre-scrollable" id="list4">
                                </ul>
                                <button type="submit" class="btn btn-default pull-left select_all_var">Выбрать все
                                </button>
                                <button type="submit" class="btn btn-default pull-right unselect_all_var">Убрать все
                                </button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form role="form">
                                <label for="list5">Группировать по:</label>
                                <ul class="list-group pre-scrollable" id="list5">
                                </ul>
                                <button type="submit" class="btn btn-default pull-left select_all_com">Выбрать все
                                </button>
                                <button type="submit" class="btn btn-default pull-right unselect_all_com">Убрать все
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span
                        class="glyphicon glyphicon-remove"></span>Отмена
                </button>
                <button type="submit" class="btn btn-success btn-default pull-right" data-dismiss="modal" id="calc_mw">
                    Вычислить
                </button>
            </div>
        </div>
    </div>
</div>
<%--

<!--U Manna-Uitni-->
<div class="modal fade" id="correlation_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><i class="fa fa-bar-chart"></i>Корреляционный анализ</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <form role="form">
                                <label for="list6">Первая переменная:</label>
                                <ul class="list-group pre-scrollable" id="list6">
                                </ul>
                                <button type="submit" class="btn btn-default pull-left select_all_var">Выбрать все
                                </button>
                                <button type="submit" class="btn btn-default pull-right unselect_all_var">Убрать все
                                </button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form role="form">
                                <label for="list7">Вторая переменная:</label>
                                <ul class="list-group pre-scrollable" id="list7">
                                </ul>
                                <button type="submit" class="btn btn-default pull-left select_all_com">Выбрать все
                                </button>
                                <button type="submit" class="btn btn-default pull-right unselect_all_com">Убрать все
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span
                        class="glyphicon glyphicon-remove"></span>Отмена
                </button>
                <button type="submit" class="btn btn-success btn-default pull-right" data-dismiss="modal">Вычислить
                </button>
            </div>
        </div>
    </div>
</div>
--%>

<!--Survival analysis-->
<div class="modal fade" id="survival_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><i class="fa fa-bar-chart"></i>Анализ выживаемости</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <form role="form">
                                <div class="form-group">
                                    <label>Начало периода:</label>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id='startPeriod'
                                               data-date-format="mm/dd/yy" class="datepicker" aria-describedby="addon"/>
                                        <span class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Конец периода:</label>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control" id='endPeriod'
                                               data-date-format="mm/dd/yy" class="datepicker" aria-describedby="addon"/>
                                        <span class="input-group-addon"><i
                                                class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Разделение периода по:</label>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" id="rb1" checked>Неделям</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="optradio" id="rb2" checked>Месяцам</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" id="rb3" name="optradio">Кварталам</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" id="rb4" name="optradio">Годам</label>
                                    </div>
                                    <div class="input-group">
                                        <div class="radio">
                                            <label><input type="radio" id="rb5" name="optradio">Другое:</label>
                                        </div>
                                        <input id="numDays" class="form-control" placeholder="Каждые ... дней (число)">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span
                        class="glyphicon glyphicon-remove"></span>Отмена
                </button>
                <button type="submit" class="btn btn-success btn-default pull-right" data-dismiss="modal"
                        id="calc_surv">Вычислить
                </button>
            </div>
        </div>
    </div>
</div>

<!--Dispersion analysis-->
<div class="modal fade" id="dispertion_modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4><i class="fa fa-bar-chart"></i>Дисперсионный анализ</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <form role="form">
                                <label for="list10">Переменные:</label>
                                <ul class="list-group pre-scrollable" id="list10">
                                </ul>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form role="form">
                                <label for="list11">Групировать по:</label>
                                <ul class="list-group pre-scrollable" id="list11">
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span
                        class="glyphicon glyphicon-remove"></span>Отмена
                </button>
                <button type="submit" class="btn btn-success btn-default pull-right" data-dismiss="modal"
                        id="calc_desp">Вычислить
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var csrfParameter = '${_csrf.parameterName}';
    var csrfToken = '${_csrf.token}';
    var tFilter = '${tableFilter}';
    $('.datepicker').datepicker();
    $('th[data-toggle="tooltip"]').tooltip();
    $(document).ready(function () {
        $('.datepicker').datepicker();
    })
</script>
</body>
</html>
