<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<jsp:useBean id="now" class="java.util.Date"/>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">
    <title>Администрирование</title>

    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/dashboard.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery-1.9.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tooltip.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="http://mknc.ru/">
                MKNC Patients
            </a>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row ">
        <!--Doctor Profile-->
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 sidebar">
            <ul class="nav nav-sidebar">
                <li style="margin-left: 5%"><u>Администратор:</u></li>
                <li style="margin-left: 5%">${user.userName}</li>
            </ul>
            <ul class="nav nav-sidebar" id="admin_sidebar">
                <li class="active"><a href="#new_patients" onclick="setVisibleBlock(0)">Поступившие пациенты</a></li>
                <li><a href="#user_addition" onclick="setVisibleBlock(1)">Добавление пользователей</a>
                </li>
                <li><a href="#user_deletion" onclick="setVisibleBlock(2)">Удаление пользователей</a></li>
                <li><a href="#patient_deletion" onclick="setVisibleBlock(3)">Удаление/изменение пациентов</a></li>
                <li><a href="#unfilled_blocks" onclick="setVisibleBlock(4)">Незаполненные блоки информации</a></li>
            </ul>
            <br>
            <br>
            <ul class="nav nav-sidebar footer">
                <c:url value="/j_spring_security_logout" var="logoutUrl"/>
                <li><a href="javascript:formSubmit()">Выход <i class="fa fa-sign-out"></i></a></li>
                <li></li>
                <li style="margin-left: 5%"><b>Вход выполнен в: ${now.toLocaleString()}</b></li>
                <form action="${logoutUrl}" method="post" id="logoutForm">
                    <input type="hidden" name="${_csrf.parameterName}"
                           value="${_csrf.token}"/>
                </form>
            </ul>
        </div>
        <div class="col-sm-offset-4 col-sm-6" style="padding: 1%">
            <c:if test="${not empty msg}">
                <div class="alert alert-${css} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong>${msg}</strong>
                </div>
            </c:if>
        </div>
        <div class="col-sm-offset-3 col-sm-8 col-xs-offset-3 col-xs-9">
            <div id="new_patients">
                <br>
                <br>
                <p>Поступившие пациенты в период с <b>${from}</b> до <b>${today}</b>.</p>
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ФИО</th>
                        <th>Пол</th>
                        <th>Дата рождения</th>
                        <th>Возраст</th>
                        <th>Диагноз</th>
                        <th>История болезни</th>
                        <th>Поступление</th>
                        <th>Выписка</th>
                        <th>Количество койко-дней</th>
                        <th>Детали</th>
                        <th>Дата смерти</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${newPatients!= null && newPatients.size() != 0}">
                        <c:forEach var="i" begin="0" end="${newPatients.size()-1}">
                            <tr>
                                <td>${i+1}</td>
                                <td>${newPatients.get(i).name}</td>
                                <td>${(newPatients.get(i).sex==0)?"ж":"м"}</td>
                                <td>${newPatients.get(i).birthdate}</td>
                                <td>${newPatients.get(i).age}</td>
                                <td>${newPatients.get(i).getDiagnosisName()}</td>
                                <td>${newPatients.get(i).diseaseNumber}</td>
                                <td>${newPatients.get(i).startDate}</td>
                                <td>${newPatients.get(i).endDate}</td>
                                <td>${newPatients.get(i).bedDaysNumber}</td>
                                <td data-container="body" data-toggle="tooltip" data-placement="top"
                                    title="${newPatients.get(i).details}">
                                        ${(newPatients.get(i).details.length() < 20) ? newPatients.get(i).details : newPatients.get(i).details.substring(0, 20).concat("...")}
                                </td>
                                <td>${newPatients.get(i).deathDate}</td>
                                <td>
                                    <spring:url value="/patients/update/${newPatients.get(i).id}/" var="showUrl"/>
                                    <button class="btn btn-primary btn-sm"
                                            onclick="location.href='${showUrl}'"><i class="fa fa-pencil-square-o"
                                                                                    aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </div>
            <div id="user_creation" style="display: none">
                <c:url value="/new_profile?${_csrf.parameterName}=${_csrf.token}" var="newProfileUrl"/>
                <form action="${newProfileUrl}" method="POST">
                    <br>
                    <br>
                    <br>
                    <div class="form-group-edit">
                        <label for="fio">ФИО:</label>
                        <input type="text" id="fio" name="fio" class="form-control">
                    </div>
                    <div class="form-group-edit">
                        <label for="email">E-mail:</label>
                        <input type="text" id="email" name="email" class="form-control">
                    </div>
                    <div class="form-group-edit">
                        <label for="password">Пароль:</label>
                        <input type="password" id="password" name="password" class="form-control">
                    </div>
                    <div class="form-group-edit">
                        <div class="checkbox">
                            <!--input type="hidden" name="isAdmin" value="false"-->
                            <label><input type="checkbox" id="isAdmin"
                                          onclick="CBSelectedValueToTrue(this);" value="true" checked name="isAdmin"><b>Права
                                администратора</b></label>
                        </div>
                    </div>
                    <div class="create_user">
                        <button class="btn btn-primary btn-block">Создать</button>
                    </div>
                </form>
            </div>
            <div id="user_deletion" style="display: none">
                <br>
                <br>
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Фото</th>
                        <th>ФИО</th>
                        <th>email</th>
                        <th>Администратор</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${users!= null && users.size() != 0}">
                        <c:forEach var="i" begin="0" end="${users.size()-1}">
                            <tr>
                                <td>${i+1}</td>
                                <td>
                                    <div class="photo-container">
                                        <c:if test="${users.get(i).getPhoto()!=null}">
                                            <p>
                                                <img src="<c:url value="/resources/images/min_${users.get(i).getPhoto()}"/>">
                                            </p>
                                        </c:if>
                                        <c:if test="${users.get(i).getPhoto()==null}">
                                            <p>
                                                <img src="<c:url value="/resources/images/min_default_icon.png"/>">
                                            </p>
                                        </c:if>
                                    </div>
                                </td>
                                <td>${users.get(i).userName}</td>
                                <td>${users.get(i).email}</td>
                                <td>${(users.get(i).admin)?"<div>Да</div>" : "<div>Нет</div>" }</td>
                                <td>
                                    <c:url value="/delete_profile/${users.get(i).userId}" var="deleteProfileUrl"/>
                                    <button class="btn btn-danger btn-sm"
                                            onclick="this.disabled=true;location.href='${deleteProfileUrl}'"><i
                                            class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </div>
            <div id="patient_deletion" style="display: none">
                <br>
                <br>
                <br>
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ФИО</th>
                        <th>Пол</th>
                        <th>Дата рождения</th>
                        <th>Возраст</th>
                        <th>Диагноз</th>
                        <th>История болезни</th>
                        <th>Поступление</th>
                        <th>Выписка</th>
                        <th>Количество койко-дней</th>
                        <th>Детали</th>
                        <th>Дата смерти</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${patients!= null && patients.size() != 0}">
                        <c:forEach var="i" begin="0" end="${patients.size()-1}">
                            <tr>
                                <td>${i+1}</td>
                                <td>${patients.get(i).name}</td>
                                <td>${(patients.get(i).sex==0)?"ж":"м"}</td>
                                <td>${patients.get(i).birthdate}</td>
                                <td>${patients.get(i).age}</td>
                                <td>${patients.get(i).getDiagnosisName()}</td>
                                <td>${patients.get(i).diseaseNumber}</td>
                                <td>${patients.get(i).startDate}</td>
                                <td>${patients.get(i).endDate}</td>
                                <td>${patients.get(i).bedDaysNumber}</td>
                                <td data-container="body" data-toggle="tooltip" data-placement="top"
                                    title="${patients.get(i).details}">
                                        ${(patients.get(i).details.length() < 20) ? patients.get(i).details : patients.get(i).details.substring(0, 20).concat("...")}
                                </td>
                                <td>${patients.get(i).deathDate}</td>
                                <td>
                                    <spring:url value="/patients/update/${patients.get(i).id}/" var="showUrl"/>
                                    <button class="btn btn-primary btn-sm"
                                            onclick="location.href='${showUrl}'"><i class="fa fa-pencil-square-o"
                                                                                    aria-hidden="true"></i>
                                    </button>
                                    <spring:url value="/patients/${patients.get(i).id}/delete" var="deleteUrl"/>
                                    <button class="btn btn-danger btn-sm"
                                            onclick="this.disabled=true;location.href='${deleteUrl}'"><i
                                            class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </div>
            <div id="unfilled_blocks" style="display: none">
                <br>
                <br>
                <br>
                <table class="table" id="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Пациент</th>
                        <th>Блок</th>
                        <th>Ответственный</th>
                        <th>Дедлайн</th>
                        <th>Просрочен</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:if test="${unfilledBlocks!= null && unfilledBlocks.size() != 0}">
                        <c:forEach var="i" begin="0" end="${unfilledBlocks.size()-1}">
                            <tr class="${unfilledBlocks.get(i).id} ${i+1}">
                                <td class="${unfilledBlocks.get(i).blockId}">${i+1}</td>
                                <td>${unfilledBlocks.get(i).patientName}</td>
                                <td>${unfilledBlocks.get(i).patientBlock}</td>
                                <td>${unfilledBlocks.get(i).userName}</td>
                                <td>${unfilledBlocks.get(i).toDate}</td>
                                <td><p>${(unfilledBlocks.get(i).expired) ? 'Да' : 'Нет'}</p></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${unfilledBlocks.get(i).expired == true}">
                                            <button class="btn btn-primary btn-sm accessButton">
                                                <i
                                                        aria-hidden="true"></i>Открыть доступ
                                            </button>
                                        </c:when>
                                        <c:otherwise>
                                            <p>${unfilledBlocks.get(i).dateDiff} ${unfilledBlocks.get(i).day}</p>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script>
    function CBSelectedValueToTrue(cb) {
        if (cb.checked) {
            cb.value = "true";
        } else {
            cb.value = "false";
        }
    }

    function formSubmit() {
        document.getElementById("logoutForm").submit();
    }
    function setVisibleBlock(number) {
        switch (number) {
            case 0:
                document.getElementById("new_patients").setAttribute("style", "display:block;");
                document.getElementById("user_creation").setAttribute("style", "display:none;");
                document.getElementById("user_deletion").setAttribute("style", "display:none;");
                document.getElementById("patient_deletion").setAttribute("style", "display:none;");
                document.getElementById("unfilled_blocks").setAttribute("style", "display:none;");
                $(document.getElementById("admin_sidebar").children[0]).addClass("active");
                $(document.getElementById("admin_sidebar").children[1]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[2]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[3]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[4]).removeClass("active");
                break;
            case 1:
                document.getElementById("user_creation").setAttribute("style", "display:block;");
                document.getElementById("new_patients").setAttribute("style", "display:none;");
                document.getElementById("user_deletion").setAttribute("style", "display:none;");
                document.getElementById("patient_deletion").setAttribute("style", "display:none;");
                document.getElementById("unfilled_blocks").setAttribute("style", "display:none;");
                $(document.getElementById("admin_sidebar").children[1]).addClass("active");
                $(document.getElementById("admin_sidebar").children[0]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[2]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[3]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[4]).removeClass("active");
                break;
            case 2:
                document.getElementById("user_creation").setAttribute("style", "display:none;");
                document.getElementById("new_patients").setAttribute("style", "display:none;");
                document.getElementById("user_deletion").setAttribute("style", "display:block;");
                document.getElementById("patient_deletion").setAttribute("style", "display:none;");
                document.getElementById("unfilled_blocks").setAttribute("style", "display:none;");
                $(document.getElementById("admin_sidebar").children[2]).addClass("active");
                $(document.getElementById("admin_sidebar").children[0]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[1]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[3]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[4]).removeClass("active");
                break;
            case 3:
                document.getElementById("user_creation").setAttribute("style", "display:none;");
                document.getElementById("new_patients").setAttribute("style", "display:none;");
                document.getElementById("user_deletion").setAttribute("style", "display:none;");
                document.getElementById("patient_deletion").setAttribute("style", "display:block;");
                document.getElementById("unfilled_blocks").setAttribute("style", "display:none;");
                $(document.getElementById("admin_sidebar").children[3]).addClass("active");
                $(document.getElementById("admin_sidebar").children[0]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[1]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[2]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[4]).removeClass("active");
                break;
            case 4:
                document.getElementById("user_creation").setAttribute("style", "display:none;");
                document.getElementById("new_patients").setAttribute("style", "display:none;");
                document.getElementById("user_deletion").setAttribute("style", "display:none;");
                document.getElementById("patient_deletion").setAttribute("style", "display:none;");
                document.getElementById("unfilled_blocks").setAttribute("style", "display:block;");
                $(document.getElementById("admin_sidebar").children[4]).addClass("active");
                $(document.getElementById("admin_sidebar").children[0]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[1]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[2]).removeClass("active");
                $(document.getElementById("admin_sidebar").children[3]).removeClass("active");
                break;
        }
    }
    $(document).on("change", "#userss", function (e) {
        var now = document.getElementById("chosen_user").value;
        var selectedVal = $(this).find("option:selected").val();
        document.getElementById("chosen_user").value = selectedVal;
    });
    $("#table tr .accessButton").click(function () {
        $(this).addClass('selected').siblings().removeClass('selected');
        var userId = $(this).closest("tr")[0].className.split(' ')[0];
        var id = $(this).closest("tr")[0].className.split(' ')[1];
        var blockId = $(this).closest("tr").find('td:first')[0].className.split(' ')[0];
        $.ajax({
            type: 'get',//тип запроса: get,post либо head
            url: 'accessBlock/' + userId + '/' + blockId,
            success: function (data) {//возвращаемый результат от сервера
                var cells = document.getElementById("table").rows[id].cells;
                cells[4].innerHTML = "<p>" + data + "</p>";
                cells[5].innerHTML = "<p>Нет</p>";
                cells[6].innerHTML = "<p>3 дня</p>";
                /* var dat = $(this).closest("tr").find('td:first');
                 dat.innerHTML = "<p>" + data + "</p>";
                 $(this).closest("tr").find('td:eq(6)').innerHTML = "<p>3 дня</p>";*/
            }
        });
    });

</script>
</body>
</html>