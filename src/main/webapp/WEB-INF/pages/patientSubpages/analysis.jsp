<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Ввод анализов</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.buttonRow):not(.section-header):not(.unnecessaryRow):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../../patients/update/${analysis.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Анализы</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveAnalysis?${_csrf.parameterName}=${_csrf.token}" var="patientAnakysisUrl"/>
                <form:form action="${patientAnakysisUrl}" modelAttribute="analysis" method="post"
                           id="patientAnalysisForm">
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>
                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>
                    <spring:bind path="day">
                        <form:hidden path="day"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">
                    <div class="container-fluid">
                        <div class="row section-header">
                            <h4 class="section-heading">Общий анализ крови</h4>
                            <hr>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="gemoglabin">Гемоглабин:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="gemoglabin">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="gemoglabin" path="gemoglabin"

                                                    class="form-control"/>
                                        <form:errors path="gemoglabin" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="eritrociti">Эритроциты:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="eritrociti">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="eritrociti" path="eritrociti"

                                                    class="form-control"/>
                                        <form:errors path="eritrociti" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="leikociti">Лейкоциты:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="leikociti">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="leikociti" path="leikociti"

                                                    class="form-control"/>
                                        <form:errors path="leikociti" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <c:if test="${analysis.day != 0}">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="palochkoyadernaya">Палочко-ядерная:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="palochkoyadernaya">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" id="palochkoyadernaya"
                                                        path="palochkoyadernaya"

                                                        class="form-control"/>
                                            <form:errors path="palochkoyadernaya" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="segmentarnoyadernaya">Сегментоядерная:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="segmentarnoyadernaya">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" id="segmentarnoyadernaya"
                                                        path="segmentarnoyadernaya"

                                                        class="form-control"/>
                                            <form:errors path="segmentarnoyadernaya" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="yunie">Юные:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="yunie">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" id="yunie" path="yunie"

                                                        class="form-control"/>
                                            <form:errors path="yunie" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="monociti">Моноциты:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="monociti">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" id="monociti" path="monociti"

                                                        class="form-control"/>
                                            <form:errors path="monociti" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="limfociti">Лифмоциты:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="limfociti">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" id="limfociti" path="limfociti"

                                                        class="form-control"/>
                                            <form:errors path="limfociti" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="trombociti">Тромбоциты:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="trombociti">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" id="trombociti" path="trombociti"

                                                        class="form-control"/>
                                            <form:errors path="trombociti" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="soe">СОЭ:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="soe">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" id="soe" path="soe"

                                                        class="form-control"/>
                                            <form:errors path="soe" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                        </c:if>
                        <div class="row section-header">
                            <h4 class="section-heading">Общий анализ мочи</h4>
                            <hr>
                        </div>

                        <div class="row">
                            <div class="col-xs-3">
                                <label for="ph">pH:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="ph">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="ph" path="ph"

                                                    class="form-control"/>
                                        <form:errors path="ph" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="belok">Белок:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="belok">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="belok" path="belok"

                                                    class="form-control"/>
                                        <form:errors path="belok" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="leikocitiMochi">Лейкоциты:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="leikocitiMochi">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="leikocitiMochi" path="leikocitiMochi"

                                                    class="form-control"/>
                                        <form:errors path="leikocitiMochi" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="eritrocitiMochi">Эритроциты:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="eritrocitiMochi">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="eritrocitiMochi" path="eritrocitiMochi"

                                                    class="form-control"/>
                                        <form:errors path="eritrocitiMochi" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="glukoza">Глюкоза:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="glukoza">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="glukoza" path="glukoza"

                                                    class="form-control"/>
                                        <form:errors path="glukoza" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="ketoni">Кетоны:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="ketoni">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="ketoni"
                                                    path="ketoni"

                                                    class="form-control"/>
                                        <form:errors path="ketoni" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row section-header">
                            <h4 class="section-heading">Биохимический аннализ крови</h4>
                            <hr>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="ast">АСТ:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="ast">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="ast" path="ast"

                                                    class="form-control"/>
                                        <form:errors path="ast" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="alt">АЛТ:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="alt">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="alt" path="alt"

                                                    class="form-control"/>
                                        <form:errors path="alt" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="obshiiBelok">Общий белок:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="obshiiBelok">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="obshiiBelok" path="obshiiBelok"

                                                    class="form-control"/>
                                        <form:errors path="obshiiBelok" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="albunin">Альбумин:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="albunin">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="albunin" path="albunin"

                                                    class="form-control"/>
                                        <form:errors path="albunin" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="obshiibilirubin">Общий билирубин:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="obshiibilirubin">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="obshiibilirubin" path="obshiibilirubin"

                                                    class="form-control"/>
                                        <form:errors path="obshiibilirubin" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="pryamoibilirubin">Прямой билирубин:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="pryamoibilirubin">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="pryamoibilirubin"
                                                    path="pryamoibilirubin"

                                                    class="form-control"/>
                                        <form:errors path="pryamoibilirubin" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="kreatinin">Креатинин:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="kreatinin">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="kreatinin" path="kreatinin"

                                                    class="form-control"/>
                                        <form:errors path="kreatinin" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="sacharKrovi">Сахар крови:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="sacharKrovi">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="sacharKrovi" path="sacharKrovi"

                                                    class="form-control"/>
                                        <form:errors path="sacharKrovi" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="mochevayaKislota">Мочевая кислота:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="mochevayaKislota">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="mochevayaKislota"
                                                    path="mochevayaKislota"

                                                    class="form-control"/>
                                        <form:errors path="mochevayaKislota" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="mochevina">Мочевина:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="mochevina">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="mochevina" path="mochevina"

                                                    class="form-control"/>
                                        <form:errors path="mochevina" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row section-header">
                            <h4 class="section-heading">Коагулограмма</h4>
                            <hr>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="mno">МНО:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="mno">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="mno" path="mno"

                                                    class="form-control"/>
                                        <form:errors path="mno" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="achtv">АЧТВ:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="achtv">
                                    <div class="${ status.error ? 'has-error' : ''}">
                                        <form:input type="number" step="any" id="achtv" path="achtv"

                                                    class="form-control"/>
                                        <form:errors path="achtv" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <c:if test="${analysis.day == 0}">
                            <div class="row section-header">
                                <h4 class="section-heading">Анализ на группу крови</h4>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="gruppaKrovi">Группа крови:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="gruppaKrovi">
                                        <div class="${status.error ? 'has-error' : ''}">
                                            <form:select path="gruppaKrovi" id="gruppaKrovi"

                                                         class="form-control">
                                                <form:option value="0">----</form:option>
                                                <form:option value="1">0</form:option>
                                                <form:option value="2">О(I)</form:option>
                                                <form:option value="3">A(II)</form:option>
                                                <form:option value="4">B(III)</form:option>
                                                <form:option value="5">AB(IV)</form:option>
                                            </form:select>
                                            <form:errors path="gruppaKrovi" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="rezFactor">Резус-фактор:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="rezFactor">
                                        <div class="${status.error ? 'has-error' : ''}">
                                            <form:select path="rezFactor" id="rezFactor"

                                                         class="form-control">
                                                <form:option value="0">----</form:option>
                                                <form:option value="1">Положительный</form:option>
                                                <form:option value="2">Отрицательный</form:option>
                                            </form:select>
                                            <form:errors path="rezFactor" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${analysis.day == 0 || analysis.day == 10}">
                            <div class="row section-header">
                                <h4 class="section-heading">Анализ крови на онкомаркеры</h4>
                                <hr>
                            </div>
                            <div class="row section-header">
                                <label>Найдены онкомаркеры:</label>
                            </div>
                            <div class="row unnecessaryRow">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <spring:bind path="onk199">
                                        <form:checkbox

                                                path="onk199" id="onk199" value="1"/><b>CA 19-9</b>
                                    </spring:bind>
                                </div>
                                <div class="col-xs-3" id="onk199Value" style="display: none;">
                                    <spring:bind path="onk199Value">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" path="onk199Value"
                                                        class="form-control"/>
                                            <form:errors path="onk199Value" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row unnecessaryRow">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <spring:bind path="cea">
                                        <form:checkbox

                                                path="cea" id="cea" value="1"/><b>CEA</b>
                                    </spring:bind>
                                </div>
                                <div class="col-xs-3" id="ceaValue" style="display: none;">
                                    <spring:bind path="ceaValue">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" path="ceaValue"
                                                        class="form-control"/>
                                            <form:errors path="ceaValue" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row unnecessaryRow">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <spring:bind path="alfoFetoprotein">
                                        <form:checkbox

                                                path="alfoFetoprotein" id="alfoFetoprotein" value="1"/><b>Альфо-фетопротеин</b>
                                    </spring:bind>
                                </div>
                                <div class="col-xs-3" id="alfoFetValue" style="display: none;">
                                    <spring:bind path="alfoFetValue">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" path="alfoFetValue"
                                                        class="form-control"/>
                                            <form:errors path="alfoFetValue" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="row unnecessaryRow">
                                <div class="col-xs-5 col-xs-offset-3">
                                    <spring:bind path="ca125">
                                        <form:checkbox
                                                path="ca125" id="ca125" value="1"/><b>CA 125</b>
                                    </spring:bind>
                                </div>
                                <div class="col-xs-3" id="ca125Value" style="display: none;">
                                    <spring:bind path="ca125Value">
                                        <div class="${ status.error ? 'has-error' : ''}">
                                            <form:input type="number" step="any" path="ca125Value"
                                                        class="form-control"/>
                                            <form:errors path="ca125Value" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                        </c:if>
                        <div class="row buttonRow">
                            <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            var c = document.getElementById("patientAnalysisForm").elements;
            for (var i = 0; i < c.length; i++) {
                c[i].setAttribute("disabled", true);
            }
        }
        Ca125Function();
        onk199Function();
        ceaFunction();
        alfoFetFunction();
    });
    function onk199Function() {
        if (document.getElementById("onk199").checked) {
            document.getElementById("onk199Value").removeAttribute("style");
            $("#onk199Value").closest(".row").removeClass("unnecessaryRow");
        } else {
            document.getElementById("onk199Value").setAttribute("style", "display:none;");
            document.getElementById("onk199Value").value = 0;
            $("#onk199Value").closest(".row").addClass("unnecessaryRow");
        }
    }
    $("#onk199").on("change", onk199Function);
    function ceaFunction() {
        if (document.getElementById("cea").checked) {
            document.getElementById("ceaValue").removeAttribute("style");
            $("#ceaValue").closest(".row").removeClass("unnecessaryRow");
        } else {
            document.getElementById("ceaValue").setAttribute("style", "display:none;");
            document.getElementById("ceaValue").value = 0;
            $("#ceaValue").closest(".row").addClass("unnecessaryRow");
        }
    }
    $("#cea").on("change", ceaFunction);
    function alfoFetFunction() {
        if (document.getElementById("alfoFetoprotein").checked) {
            document.getElementById("alfoFetValue").removeAttribute("style");
            $("#alfoFetValue").closest(".row").removeClass("unnecessaryRow");
        } else {
            document.getElementById("alfoFetValue").setAttribute("style", "display:none;");
            document.getElementById("alfoFetValue").value = 0;
            $("#alfoFetValue").closest(".row").addClass("unnecessaryRow");
        }
    }
    $("#alfoFetoprotein").on("change", alfoFetFunction);
    function Ca125Function() {
        if (document.getElementById("ca125").checked) {
            document.getElementById("ca125Value").removeAttribute("style");
            $("#ca125").closest(".row").removeClass("unnecessaryRow");
        } else {
            document.getElementById("ca125Value").setAttribute("style", "display:none;");
            document.getElementById("ca125Value").value = 0;
            $("#ca125").closest(".row").addClass("unnecessaryRow");
        }
    }
    $("#ca125").on("change", Ca125Function);
</script>
</body>
</html>
