<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Объективный осмотр</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.buttonRow):not(.unnecessaryRow):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${objExamination.id}"><ins>На страницу пациента</ins></a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Объективный осмотр врача</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveObjectiveExamination?${_csrf.parameterName}=${_csrf.token}"
                       var="patientObjExaminationUrl"/>
                <form:form action="${patientObjExaminationUrl}" modelAttribute="objExamination" method="post"
                           id="patientObjExaminationForm">
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>
                    <spring:bind path="isNew">
                        <form:hidden path="isNew"/>
                    </spring:bind>
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="patientState">Состояние больного:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="patientState">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="patientState" id="patientState"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Удовлетворительное</form:option>
                                            <form:option value="2">Средняя степень тяжести</form:option>
                                            <form:option value="3">Тяжелое</form:option>
                                        </form:select>
                                        <form:errors path="patientState" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="temperature">Температура:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="temperature">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="temperature" id="temperature"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Нормальная</form:option>
                                            <form:option value="2">37-38</form:option>
                                            <form:option value="3">>38</form:option>
                                        </form:select>
                                        <form:errors path="temperature" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="nalichieZheltuchi">
                                    <form:checkbox
                                            path="nalichieZheltuchi" id="nalichieZheltuchi" value="1"/><b>Наличие желтухи</b>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="odishka">Одышка:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="odishka">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="odishka" id="odishka"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Нет</form:option>
                                            <form:option value="2">Есть, ЧДД >20</form:option>
                                            <form:option value="3">Есть, ЧДД>30</form:option>
                                        </form:select>
                                        <form:errors path="odishka" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="puls">Пульс:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="puls">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="puls" id="puls"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Ритмичный</form:option>
                                            <form:option value="2">Аритмичный</form:option>
                                        </form:select>
                                        <form:errors path="puls" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="tachikardia">Тахикардия:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="tachikardia">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="tachikardia" id="tachikardia"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Нет</form:option>
                                            <form:option value="2">ЧСС < 100</form:option>
                                            <form:option value="3">ЧСС > 100</form:option>
                                        </form:select>
                                        <form:errors path="tachikardia" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="davlenie">Артериальное давление:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="davlenie">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:input type="text" id="davlenie" path="davlenie"
                                                    class="form-control"/>
                                        <form:errors path="davlenie" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="nalichieToshnoti">
                                    <form:checkbox
                                            path="nalichieToshnoti" id="nalichieToshnoti" value="1"/><b>Наличие тошноты</b>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="rvota">Рвота:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="rvota">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="rvota" id="rvota"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Нет</form:option>
                                            <form:option value="2">< 1 литра</form:option>
                                            <form:option value="3">> 1 литра</form:option>
                                        </form:select>
                                        <form:errors path="rvota" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="zhivot">Состояние живота:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="zhivot">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="zhivot" id="zhivot"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Увеличен</form:option>
                                            <form:option value="2">Не увеличен</form:option>
                                        </form:select>
                                        <form:errors path="zhivot" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="boleznenostZhivota">
                                    <form:checkbox
                                            path="boleznenostZhivota" id="boleznenostZhivota"
                                            value="1"/><b>Наличие болезненности при пальпации живота</b>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="napryazhenieZhivota">
                                    <form:checkbox
                                            path="napryazhenieZhivota" id="napryazhenieZhivota"
                                            value="1"/><b>Наличие напряжения мышц живота</b>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="peritorialSymptomi">
                                    <form:checkbox
                                            path="peritorialSymptomi" id="peritorialSymptomi"
                                            value="1"/><b>Наличие перитонеальных симптомов</b>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="peristaltika">Перистальтика:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="peristaltika">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="peristaltika" id="peristaltika"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Нормальная</form:option>
                                            <form:option value="2">Усиленная</form:option>
                                            <form:option value="3">Ослабленная</form:option>
                                        </form:select>
                                        <form:errors path="peristaltika" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="otchozhdenieGazov">
                                    <form:checkbox
                                            path="otchozhdenieGazov" id="otchozhdenieGazov"
                                            value="1"/><b>Наличие отхождения газов</b>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="stul">Стул:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="stul">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="stul" id="stul"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1">Регулярный</form:option>
                                            <form:option value="2">Задержка</form:option>
                                            <form:option value="3">Диарея</form:option>
                                        </form:select>
                                        <form:errors path="stul" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="dizuria">
                                    <form:checkbox
                                            path="dizuria" id="dizuria"
                                            value="1"/><b>Наличие дизурии</b>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="objemMochi">Объем суточной мочи:</label>
                            </div>
                            <div class="col-xs-9">
                                <spring:bind path="objemMochi">
                                    <div class="${status.error ? 'has-error' : ''}">
                                        <form:select path="objemMochi" id="objemMochi"
                                                     class="form-control">
                                            <form:option value="0">----</form:option>
                                            <form:option value="1"><500 мл</form:option>
                                            <form:option value="2">500-1000 мл</form:option>
                                            <form:option value="3">>1000 мл</form:option>
                                        </form:select>
                                        <form:errors path="objemMochi" class="control-label"/>
                                    </div>
                                </spring:bind>
                            </div>
                        </div>
                        <div class="row buttonRow">
                            <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            var c = document.getElementById("patientObjExaminationForm").elements;
            for (var i = 0; i < c.length; i++) {
                c[i].setAttribute("disabled", true);
            }
        }
    });
</script>
</body>
</html>
