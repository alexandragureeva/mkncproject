<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Информация при выписке</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/datepicker.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.buttonRow):not(.unnecessaryRow):before {
            content:"*";
            color:red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${generalInfoEnd.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Информация при выписке</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveEndGenInfo?${_csrf.parameterName}=${_csrf.token}" var="generalInfoEndUrl"/>
                <form:form action="${generalInfoEndUrl}" modelAttribute="generalInfoEnd" method="post"
                           id="generalInfoEndForm">
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="endDate">Дата выписки:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="endDate">
                                <div class="${status.error ? 'has-error' : ''}">
                                    <div class='input-group date'>
                                        <form:input type='text'
                                                    class="form-control" path="endDate" id='endDate'
                                                    aria-describedby="addon"/>
                            <span class="input-group-addon" id="addon"><i
                                    class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                    <form:errors path="endDate" class="control-label"/>
                                </div>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="col-xs-3">
                            <label for="deathDate">Дата смерти:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="deathDate">
                                <div class="${status.error ? 'has-error' : ''}">
                                    <div class='input-group date'>
                                        <form:input type='text'
                                                    class="form-control" path="deathDate" id='deathDate'
                                                    aria-describedby="addon"/>
                            <span class="input-group-addon" id="addon"><i
                                    class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                    <form:errors path="deathDate" class="control-label"/>
                                </div>
                            </spring:bind>
                        </div>
                    </div>

                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Сохранить</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<%--</:formform>--%>
<script type="text/javascript">
    $('#endDate').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayBtn: 'linked',
        pickerPosition: "bottom-left"
    });
    $('#deathDate').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayBtn: 'linked',
        pickerPosition: "bottom-left"
    });
    $(document).ready(function () {
        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            document.getElementById("endDate").setAttribute("disabled", true);
        }
    });
</script>
</body>
</html>