<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/resources/images/database.png"/>">

    <title>Информация при выписке</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/datepicker.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.buttonRow):not(.unnecessaryRow):before {
            content: "*";
            color: red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <c:choose>
                <c:when test="${patient.id != null && !patient.id.isEmpty()}">
                    <a href="../patients/update/${patient.id}">
                        <ins>На страницу пациента</ins>
                    </a>
                </c:when>
                <c:otherwise>
                    <a href="welcome">
                        <ins>На основную страницу</ins>
                    </a>
                </c:otherwise>
            </c:choose>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Информация о пациенте при выписке</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveGenInfo?${_csrf.parameterName}=${_csrf.token}" var="patientInfoUrl"/>
                <form:form action="${patientInfoUrl}" modelAttribute="patient" method="post" id="patientInfoForm">
                    <spring:bind path="id">
                        <form:hidden path="id"/>
                    </spring:bind>

                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">
                    <div class="row">
                        <div class="col-xs-3">
                            <label for="diagnosis">Диагноз при поступлении:</label>
                        </div>
                        <div class="col-xs-9">
                            <spring:bind path="diagnosis">
                                <div class="${status.error ? 'has-error' : ''}">
                                    <form:select path="diagnosis" id="diagnosis" class="form-control">
                                        <c:if test="${diagnosisList != null && diagnosisList.size() > 2 }">
                                            <c:forEach var="i" begin="0" end="${diagnosisList.size()-2}">
                                                <form:option value="${i}">${diagnosisList.get(i)}</form:option>
                                            </c:forEach>
                                        </c:if>
                                    </form:select>
                                    <form:errors path="diagnosis" class="control-label"/>
                                </div>
                            </spring:bind>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row  unnecessaryRow">
                            <h3 class="section-heading">Сопутствующие заболевания</h3>
                            <hr>
                            <!--сопутствующие заболевания-->
                            <ul class="list-unstyled">
                                <li>
                                    <spring:bind path="ibs">
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5">
                                            <form:checkbox

                                                    path="ibs" id="ibs" value="1"/><b>ИБС</b>
                                            </spring:bind></div>
                                        <div class="col-xs-4"><spring:bind path="ibsType">
                                            <form:select path="ibsType" id="ibsType"

                                                         class="form-control" cssStyle="display: none;">
                                                <form:option value="0">----</form:option>
                                                <form:option value="1">Стенокардия напряжения</form:option>
                                                <form:option value="2">Постинфарктный кардиосклероз</form:option>
                                            </form:select>
                                        </spring:bind></div>
                                        <div class="col-xs-3">
                                            <spring:bind path="ibsSubType">
                                                <form:select path="ibsSubType" id="ibsSubType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">1 класс</form:option>
                                                    <form:option value="2">2 класс</form:option>
                                                    <form:option value="3">3 класс</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                    </div>
                                </li>

                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="hyperDisease">
                                            <form:checkbox id="hyperDisease"

                                                           path="hyperDisease" value="1"/><b>Гипертоническая болезнь</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="hyperDiseaseType">
                                                <form:select path="hyperDiseaseType" id="hyperDiseaseType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">1</form:option>
                                                    <form:option value="2">2</form:option>
                                                    <form:option value="3">3</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>

                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="diabet">
                                            <form:checkbox id="diabet"

                                                           path="diabet" value="1"/><b>Сахарный диабет</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="diabetType">
                                                <form:select path="diabetType" id="diabetType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">1 тип</form:option>
                                                    <form:option value="2">2 тип</form:option>
                                                    <form:option value="3">1 тип, компенсация</form:option>
                                                    <form:option value="4">1 тип, декомпенсация</form:option>
                                                    <form:option value="5">2 тип, компенсация</form:option>
                                                    <form:option value="6">2 тип, декомпенсация</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="chronVenDisease">
                                            <form:checkbox id="chronVenDisease"

                                                           path="chronVenDisease"
                                                           value="1"/><b>Хроническая венозная недостаточность нижних
                                            конечностей</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="chronVenDiseaseType">
                                                <form:select path="chronVenDiseaseType" id="chronVenDiseaseType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Варикозная болезнь с ХВН</form:option>
                                                    <form:option
                                                            value="2">Тромбоз глубоких вен нижних конечностей</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="bronchAstma">
                                            <form:checkbox id="bronchAstma"

                                                           path="bronchAstma"
                                                           value="1"/><b>Бронхиальная астма с дыхательной
                                            недостаточностью</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="bronchAstmaType">
                                                <form:select path="bronchAstmaType" id="bronchAstmaType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">0</form:option>
                                                    <form:option value="2">1</form:option>
                                                    <form:option value="3">2</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="chronBronchit">
                                            <form:checkbox id="chronBronchit"

                                                           path="chronBronchit"
                                                           value="1"/><b>Хронический бронхит с дыхательной
                                            недостаточностью</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="chronBronchitType">
                                                <form:select path="chronBronchitType" id="chronBronchitType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">0</form:option>
                                                    <form:option value="2">1</form:option>
                                                    <form:option value="3">2</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="emfizema">
                                            <form:checkbox id="emfizema"

                                                           path="emfizema" value="1"/><b>Эмфизема легких</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="emfizemaType">
                                                <form:select path="emfizemaType" id="emfizemaType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">0</form:option>
                                                    <form:option value="2">1</form:option>
                                                    <form:option value="3">2</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="serdechPoroki">
                                            <form:checkbox id="serdechPoroki"

                                                           path="serdechPoroki"
                                                           value="1"/><b>Сердечные пороки с недостаточностью
                                            крообращения</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="serdechPorokiType">
                                                <form:select path="serdechPorokiType" id="serdechPorokiType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">0</form:option>
                                                    <form:option value="2">1</form:option>
                                                    <form:option value="3">2</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="onmk">
                                            <form:checkbox id="onmk"

                                                           path="onmk" value="1"/><b>ОНМК в анамнезе</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="onmkType">
                                                <form:select path="onmkType" id="onmkType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Транзиторное ОНМК</form:option>
                                                    <form:option value="2">Ишемический инсульт</form:option>
                                                    <form:option value="3">Геморрагический инсульт</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="parkinsonDisease">
                                            <form:checkbox id="parkinsonDisease"

                                                           path="parkinsonDisease" value="1"/><b>Болезнь Паркинсона</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="parkinsonDiseaseType">
                                                <form:select path="parkinsonDiseaseType" id="parkinsonDiseaseType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Компенсированная</form:option>
                                                    <form:option value="2">Декомпенсированная</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="parkinsonSyndrom">
                                            <form:checkbox id="parkinsonSyndrom"

                                                           path="parkinsonSyndrom" value="1"/><b>Синдром Паркинсона</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="parkinsonSyndromType">
                                                <form:select path="parkinsonSyndromType" id="parkinsonSyndromType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Компенсированный</form:option>
                                                    <form:option value="2">Декомпенсированный</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="ozhirenie">
                                            <form:checkbox id="ozhirenie"

                                                           path="ozhirenie" value="1"/><b>Ожирение</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="ozhirenieType">
                                                <form:select path="ozhirenieType" id="ozhirenieType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">1</form:option>
                                                    <form:option value="2">2</form:option>
                                                    <form:option value="3">3</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="yazvenDisease">
                                            <form:checkbox id="yazvenDisease"

                                                           path="yazvenDisease" value="1"/><b>Язвенная болезнь</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="yazvenDiseaseType">
                                                <form:select path="yazvenDiseaseType" id="yazvenDiseaseType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Желудка (обострение)</form:option>
                                                    <form:option value="2">Желудка (ремиссия)</form:option>
                                                    <form:option
                                                            value="3">Двенадцатиперстной кишки (обострение)</form:option>
                                                    <form:option
                                                            value="4">Двенадцатиперстной кишки (ремиссия)</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3" style="display: none;" id="compGroup">
                                            <spring:bind path="yazvenDiseaseComplications">
                                                <label for="yazvenDiseaseComplications">Осложнения язвенной болезни
                                                    желудка
                                                    и
                                                    ДПК:</label>
                                                <form:select path="yazvenDiseaseComplications"
                                                             id="yazvenDiseaseComplications"

                                                             class="form-control">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Нет</form:option>
                                                    <form:option
                                                            value="2">Стеноз выходного отдела желудка и/или ДПК</form:option>
                                                    <form:option value="3">Перфорация желудка или ДПК</form:option>
                                                    <form:option value="4">Кровотечение из язвы</form:option>
                                                    <form:option
                                                            value="5">Операции по поводу осложнений в анамнезе (перфорации, кровотечения)</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="hyperplazia">
                                            <form:checkbox id="hyperplazia"

                                                           path="hyperplazia"
                                                           value="1"/><b>Доброкачественная гиперплазия предстательной
                                            железы</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4">
                                            <spring:bind path="hyperplaziaType">
                                                <form:select path="hyperplaziaType" id="hyperplaziaType"

                                                             class="form-control" cssStyle="display: none;">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">1 стадия</form:option>
                                                    <form:option value="2">2 стадия</form:option>
                                                </form:select>
                                            </spring:bind>
                                        </div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row unnecessaryRow">
                                        <div class="col-xs-5"><spring:bind path="otherDiseases">
                                            <form:checkbox

                                                    path="otherDiseases" value="1"/><b>Другие болезни</b>
                                        </spring:bind></div>
                                        <div class="col-xs-4"></div>
                                        <div class="col-xs-3"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row unnecessaryRow">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Операция</th>
                                    <th>Осложнения</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:if test="${surgeries != null && surgeries.size() > 0}">
                                    <c:forEach var="i" begin="0" end="${surgeries.size()-1}">
                                        <tr>
                                            <td>${i+1}</td>
                                            <td>${surgeries.get(i).name}</td>
                                            <td>${surgeries.get(i).complications}</td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row buttonRow">
                        <button type="submit" class="btn btn-default btn-block">Сохранить</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<%--</:formform>--%>
<script type="text/javascript">
    var csrfParameter = '${_csrf.parameterName}';
    var csrfToken = '${_csrf.token}';

    function ibsFunc() {
        if (document.getElementById("ibs").checked) {
            document.getElementById("ibsType").removeAttribute("style");
            //     document.getElementById("ibsSubType").removeAttribute("style");
        } else {
            document.getElementById("ibsType").setAttribute("style", "display:none;");
            document.getElementById("ibsSubType").setAttribute("style", "display:none;");
            document.getElementById("ibsType").value = 0;
            document.getElementById("ibsSubType").value = 0;

        }
    }
    $("#ibs").on("change", ibsFunc);


    function hyperDiseaseFunc() {
        if (document.getElementById("hyperDisease").checked) {
            document.getElementById("hyperDiseaseType").removeAttribute("style");
        } else {
            document.getElementById("hyperDiseaseType").setAttribute("style", "display:none;");
            document.getElementById("hyperDiseaseType").value = 0;
        }
    }
    $("#hyperDisease").on("change", hyperDiseaseFunc);
    function diabetFunc() {
        if (document.getElementById("diabet").checked) {
            document.getElementById("diabetType").removeAttribute("style");
        } else {
            document.getElementById("diabetType").setAttribute("style", "display:none;");
            document.getElementById("diabetType").value = 0;
        }
    }
    $("#diabet").on("change", diabetFunc);
    function chronVenDiseaseFunc() {
        if (document.getElementById("chronVenDisease").checked) {
            document.getElementById("chronVenDiseaseType").removeAttribute("style");
        } else {
            document.getElementById("chronVenDiseaseType").setAttribute("style", "display:none;");
            document.getElementById("chronVenDiseaseType").value = 0;
        }
    }
    $("#chronVenDisease").on("change", chronVenDiseaseFunc);
    function bronchAstmaFunc() {
        if (document.getElementById("bronchAstma").checked) {
            document.getElementById("bronchAstmaType").removeAttribute("style");
        } else {
            document.getElementById("bronchAstmaType").setAttribute("style", "display:none;");
            document.getElementById("bronchAstmaType").value = 0;
        }
    }
    $("#bronchAstma").on("change", bronchAstmaFunc);
    function chronBronchitFunc() {
        if (document.getElementById("chronBronchit").checked) {
            document.getElementById("chronBronchitType").removeAttribute("style");
        } else {
            document.getElementById("chronBronchitType").setAttribute("style", "display:none;");
            document.getElementById("chronBronchitType").value = 0;
        }
    }
    $("#chronBronchit").on("change", chronBronchitFunc);
    function emfizemaFunc() {
        if (document.getElementById("emfizema").checked) {
            document.getElementById("emfizemaType").removeAttribute("style");
        } else {
            document.getElementById("emfizemaType").setAttribute("style", "display:none;");
            document.getElementById("emfizemaType").value = 0;
        }
    }
    $("#emfizema").on("change", emfizemaFunc);
    function serdechPorokiFunc() {
        if (document.getElementById("serdechPoroki").checked) {
            document.getElementById("serdechPorokiType").removeAttribute("style");
        } else {
            document.getElementById("serdechPorokiType").setAttribute("style", "display:none;");
            document.getElementById("serdechPorokiType").value = 0;
        }
    }
    $("#serdechPoroki").on("change", serdechPorokiFunc);
    function onmkFunc() {
        if (document.getElementById("onmk").checked) {
            document.getElementById("onmkType").removeAttribute("style");
        } else {
            document.getElementById("onmkType").setAttribute("style", "display:none;");
            document.getElementById("onmkType").value = 0;
        }
    }
    $("#onmk").on("change", onmkFunc);
    function parkinsonDiseaseFunc() {
        if (document.getElementById("parkinsonDisease").checked) {
            document.getElementById("parkinsonDiseaseType").removeAttribute("style");
        } else {
            document.getElementById("parkinsonDiseaseType").setAttribute("style", "display:none;");
            document.getElementById("parkinsonDiseaseType").value = 0;
        }
    }
    $("#parkinsonDisease").on("change", parkinsonDiseaseFunc);
    function parkinsonSyndromFunc() {
        if (document.getElementById("parkinsonSyndrom").checked) {
            document.getElementById("parkinsonSyndromType").removeAttribute("style");
        } else {
            document.getElementById("parkinsonSyndromType").setAttribute("style", "display:none;");
            document.getElementById("parkinsonSyndromType").value = 0;
        }
    }
    $("#parkinsonSyndrom").on("change", parkinsonSyndromFunc);
    function ozhirenieFunc() {
        if (document.getElementById("ozhirenie").checked) {
            document.getElementById("ozhirenieType").removeAttribute("style");
        } else {
            document.getElementById("ozhirenieType").setAttribute("style", "display:none;");
            document.getElementById("ozhirenieType").value = 0;
        }
    }
    $("#ozhirenie").on("change", ozhirenieFunc);
    function yazvenDiseaseFunc() {
        if (document.getElementById("yazvenDisease").checked) {
            document.getElementById("yazvenDiseaseType").removeAttribute("style");
            document.getElementById("compGroup").removeAttribute("style");
        } else {
            document.getElementById("yazvenDiseaseType").setAttribute("style", "display:none;");
            document.getElementById("compGroup").setAttribute("style", "display:none;");
            document.getElementById("yazvenDiseaseComplications").value = 0;
            document.getElementById("yazvenDiseaseType").value = 0;
        }
    }
    $("#yazvenDisease").on("change", yazvenDiseaseFunc);
    function hyperplaziaFunc() {
        if (document.getElementById("hyperplazia").checked) {
            document.getElementById("hyperplaziaType").removeAttribute("style");
        } else {
            document.getElementById("hyperplaziaType").setAttribute("style", "display:none;");
            document.getElementById("hyperplaziaType").value = 0;
        }
    }
    $("#hyperplazia").on("change", hyperplaziaFunc);
    function ibsTypefunc() {
        var selectedVal = $("#ibsType").find("option:selected").val();
        switch (selectedVal) {
            case "1":
                document.getElementById("ibsSubType").removeAttribute("style");
                break;
            default:
                document.getElementById("ibsSubType").setAttribute("style", "display:none;");
                document.getElementById("ibsSubType").value = 0;
                break;
        }
    }
    $("#ibsType").on("change", ibsTypefunc);
    function gospitalizationFunc() {
        var selectedVal = $("#gospitalization").find("option:selected").val();
        switch (selectedVal) {
            case "2":
                document.getElementById("gospitalizationTimeGroup").removeAttribute("style");
                break;
            default:
                document.getElementById("gospitalizationTimeGroup").setAttribute("style", "display:none;");
                document.getElementById("gospitalizationTime").value = 0;
                break;
        }
    }
    $("#gospitalization").on("change", gospitalizationFunc);
    $(document).ready(function () {
        ibsFunc();
        ibsTypefunc();
        gospitalizationFunc();
        hyperDiseaseFunc();
        diabetFunc();
        chronVenDiseaseFunc();
        bronchAstmaFunc();
        chronBronchitFunc();
        emfizemaFunc();
        serdechPorokiFunc();
        onmkFunc();
        parkinsonDiseaseFunc();
        parkinsonSyndromFunc();
        ozhirenieFunc();
        yazvenDiseaseFunc();
        hyperplaziaFunc();

        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            var c = document.getElementById("patientInfoForm").elements;
            for (var i = 0; i < c.length; i++) {
                c[i].setAttribute("disabled", true);
            }
        }
    });
</script>
</body>
</html>