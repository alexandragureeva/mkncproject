<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session="true" %>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Двухэтапное лечение</title>
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/font-awesome.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-tab.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .row:not(.buttonRow):not(.unnecessaryRow):before {
            content: "*";
            color: red;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-sm-offset-1 col-sm-10 col-xs-11">
        <c:if test="${not empty msg}">
            <div class="alert alert-${css} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"
                        aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>${msg}</strong>
            </div>
        </c:if>

    </div>
    <div class="row">
        <h4 class="pull-left">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
            <a href="../patients/update/${twostepstreat.id}">
                <ins>На страницу пациента</ins>
            </a>
        </h4>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <div id="genInfoModal">
            <div class="modal-header">
                <h4><i class="fa fa-bar-chart"></i>Двухэтапное лечение</h4>
            </div>
            <div class="modal-body">
                <c:url value="/saveTwoStepsTreatment?${_csrf.parameterName}=${_csrf.token}"
                       var="twostepstreatUrl"/>
                <form:form action="${twostepstreatUrl}" modelAttribute="twostepstreat" method="post"
                           id="twostepstreatForm">
                    <input style="display: none" value="${expired}" id="expiredValue">
                    <input style="display: none" value="${isAdmin}" id="isAdminValue">
                    <div class="container-fluid">
                        <spring:bind path="id">
                            <form:hidden path="id"/>
                        </spring:bind>
                        <spring:bind path="isNew">
                            <form:hidden path="isNew"/>
                        </spring:bind>
                        <div class="row">
                            <div class="col-xs-12">
                                <spring:bind path="provedenie2Etapnogo">
                                    <form:checkbox
                                            path="provedenie2Etapnogo" id="provedenie2Etapnogo" value="1"/><b>
                                    Проведение двухэтапного лечения
                                </b>
                                </spring:bind>
                            </div>
                        </div>
                        <div id="twoetapnoeRow" style="display: none;">
                            <div class="row">
                                <div class="col-xs-3">
                                    <label for="variant2Etapnogo">Вариант двухэтапного лечения:</label>
                                </div>
                                <div class="col-xs-9">
                                    <spring:bind path="variant2Etapnogo">
                                        <div class="${status.error ? 'has-error' : ''}">
                                            <form:select path="variant2Etapnogo" id="variant2Etapnogo"
                                                         class="form-control">
                                                <form:option value="0">----</form:option>
                                                <form:option value="1">ALPPS (различные виды окклюзии воротной вены в сочетании с вариантами обработки будущего среза печени. Возможно РЧА/резекция будущего остатка печени)</form:option>
                                                <form:option value="2">Портоэмболизация (варианты без обработки будущего среза печени и резекции/РЧА будущего остатка печени)</form:option>
                                                <form:option value="3">Перевязка воротной вены (открытая и лапороскопическая перевязка воротной вены без обработки будущего среза печени и резекции будущего остатка печени, без резекции/РЧА будущего остатка печени)</form:option>
                                                <form:option value="4">Двухэтапная резекция (все варианты окклюзии воротной вены в сочетании с резекцией/РЧА будущего остатка печени без обработки будущего среза печени)</form:option>
                                            </form:select>
                                            <form:errors path="variant2Etapnogo" class="control-label"/>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>

                            <div id="alpps" style="display: none;">
                                <div class="row unnecessaryRow">
                                    <h4 class="section-heading">ALPPS</h4>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="razobsheniePecheni">Вариант разобщения долей печени:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <spring:bind path="razobsheniePecheni">
                                            <div class="${status.error ? 'has-error' : ''}">
                                                <form:select path="razobsheniePecheni" id="razobsheniePecheni"
                                                             class="form-control">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Классический</form:option>
                                                    <form:option
                                                            value="2">Открытый с неполным разделением паринхимы печени</form:option>
                                                    <form:option value="3">RALPPS</form:option>
                                                </form:select>
                                                <form:errors path="razobsheniePecheni" class="control-label"/>
                                            </div>
                                        </spring:bind>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="dostup">Доступ:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <spring:bind path="dostup">
                                            <div class="${status.error ? 'has-error' : ''}">
                                                <form:select path="dostup" id="dostup"
                                                             class="form-control">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Открытый</form:option>
                                                    <form:option value="2">Лапороскопический</form:option>
                                                </form:select>
                                                <form:errors path="dostup" class="control-label"/>
                                            </div>
                                        </spring:bind>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="rezekcia">Вариант резекции:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <spring:bind path="rezekcia">
                                            <div class="${status.error ? 'has-error' : ''}">
                                                <form:select path="rezekcia" id="rezekcia"
                                                             class="form-control">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Правосторонняя гемигепатэктомия</form:option>
                                                    <form:option value="2">Левосторонняя гемигепатэктомия</form:option>
                                                    <form:option
                                                            value="3">Расширенная правосторонняя гемигепатэктомия</form:option>
                                                    <form:option
                                                            value="4">Расширенная левосторонняя гемигепатэктомия</form:option>
                                                </form:select>
                                                <form:errors path="rezekcia" class="control-label"/>
                                            </div>
                                        </spring:bind>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="vmeshatelstvo">Вмешательство на будущем остатке печени:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <spring:bind path="vmeshatelstvo">
                                            <div class="${status.error ? 'has-error' : ''}">
                                                <form:select path="vmeshatelstvo" id="vmeshatelstvo"
                                                             class="form-control">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Без вмешательства</form:option>
                                                    <form:option value="2">РЧА</form:option>
                                                    <form:option value="3">Резекция</form:option>
                                                </form:select>
                                                <form:errors path="vmeshatelstvo" class="control-label"/>
                                            </div>
                                        </spring:bind>
                                    </div>
                                </div>
                            </div>

                            <div id="okklusii" style="display: none;">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="okklusia">Варианты окклюзий воротной вены:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <spring:bind path="okklusia">
                                            <div class="${status.error ? 'has-error' : ''}">
                                                <form:select path="okklusia" id="okklusia"
                                                             class="form-control">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Портоэмболизация</form:option>
                                                    <form:option value="2">Открытая/Лапороскопическая перевязка воротной вены</form:option>
                                                </form:select>
                                                <form:errors path="okklusia" class="control-label"/>
                                            </div>
                                        </spring:bind>
                                    </div>
                                </div>
                            </div>

                            <div id="portoembolisacia" style="display: none;">
                                <div class="row unnecessaryRow">
                                    <h4 class="section-heading">Портоэмболизация</h4>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="portoembType">Вид:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <spring:bind path="portoembType">
                                            <div class="${status.error ? 'has-error' : ''}">
                                                <form:select path="portoembType" id="portoembType"
                                                             class="form-control">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Правосторонняя</form:option>
                                                    <form:option
                                                            value="2">Левостороння + правый передний сектор</form:option>
                                                    <form:option value="3">Правосторонняя + 4 сегмент</form:option>
                                                </form:select>
                                                <form:errors path="portoembType" class="control-label"/>
                                            </div>
                                        </spring:bind>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label>Материал:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <ul class="list-unstyled">
                                            <li>
                                                <div class="row unnecessaryRow">
                                                    <div class="col-xs-12"><spring:bind path="microspheres">
                                                        <form:checkbox id="microspheres" path="microspheres" value="1"/><b>
                                                        С микросферами</b>
                                                    </spring:bind></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row unnecessaryRow">
                                                    <div class="col-xs-12"><spring:bind path="spirt">
                                                        <form:checkbox id="spirt" path="spirt" value="1"/><b>
                                                        Со спиртом</b>
                                                    </spring:bind></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row unnecessaryRow">
                                                    <div class="col-xs-12"><spring:bind path="lipiodol">
                                                        <form:checkbox id="lipiodol" path="lipiodol" value="1"/><b>
                                                        С липиодолом</b>
                                                    </spring:bind></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="row unnecessaryRow">
                                                    <div class="col-xs-12"><spring:bind path="otherMaterial">
                                                        <form:checkbox id="otherMaterial" path="otherMaterial" value="1"/><b>
                                                        Другой</b>
                                                    </spring:bind></div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div id="perevazkaVorVeni" style="display: none;">
                                <div class="row unnecessaryRow">
                                    <h4 class="section-heading">Перевязка воротной вены</h4>
                                    <hr>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3">
                                        <label for="perevyazkaType">Вид:</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <spring:bind path="perevyazkaType">
                                            <div class="${status.error ? 'has-error' : ''}">
                                                <form:select path="perevyazkaType" id="perevyazkaType"
                                                             class="form-control">
                                                    <form:option value="0">----</form:option>
                                                    <form:option value="1">Открытая</form:option>
                                                    <form:option value="2">Лапороскопическая</form:option>
                                                </form:select>
                                                <form:errors path="perevyazkaType" class="control-label"/>
                                            </div>
                                        </spring:bind>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row buttonRow">
                            <button type="submit" class="btn btn-default btn-block">Cохранить</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var expired = document.getElementById("expiredValue").value;
        var isAdmin = document.getElementById("isAdminValue").value;
        if (expired === "true" && isAdmin === "false") {
            var c = document.getElementById("patienttwostepstreatForm").elements;
            for (var i = 0; i < c.length; i++) {
                c[i].setAttribute("disabled", true);
            }
        }

        function twostepFunction() {
            if (document.getElementById("provedenie2Etapnogo").checked) {
                document.getElementById("twoetapnoeRow").removeAttribute("style");
            } else {
                document.getElementById("twoetapnoeRow").setAttribute("style", "display:none;");
                $("#variant2Etapnogo").val(0);
                $("#okklusia").val(0);
                disablePortoembolizaciu();
                disableVorVenu();
                disableALPPS();
                document.getElementById("alpps").setAttribute("style", "display:none;");
                document.getElementById("okklusii").setAttribute("style", "display:none;");
                document.getElementById("portoembolisacia").setAttribute("style", "display:none;");
                document.getElementById("perevazkaVorVeni").setAttribute("style", "display:none;");

            }
        }

        twostepFunction();
        $("#provedenie2Etapnogo").on("change", twostepFunction);

        function checkVariant2Etapnogo(first) {
            switch(+$("#variant2Etapnogo").val()) {
                case 0:
                    if (!first.data) {
                        document.getElementById("alpps").setAttribute("style", "display:none;");
                        document.getElementById("okklusii").setAttribute("style", "display:none;");
                        document.getElementById("portoembolisacia").setAttribute("style", "display:none;");
                        document.getElementById("perevazkaVorVeni").setAttribute("style", "display:none;");
                        disablePortoembolizaciu();
                        disableVorVenu();
                        disableALPPS();
                        $("#okklusia").val(0);
                    }
                    break;
                case 1:
                    document.getElementById("okklusii").removeAttribute("style");
                    document.getElementById("alpps").removeAttribute("style");
                    if (!first.data) {
                        document.getElementById("portoembolisacia").setAttribute("style", "display:none;");
                        document.getElementById("perevazkaVorVeni").setAttribute("style", "display:none;");
                        disablePortoembolizaciu();
                        disableVorVenu();
                        $("#okklusia").val(0);
                    }
                    break;
                case 2:
                    document.getElementById("portoembolisacia").removeAttribute("style");
                    if (!first.data) {
                        document.getElementById("alpps").setAttribute("style", "display:none;");
                        document.getElementById("okklusii").setAttribute("style", "display:none;");
                        document.getElementById("perevazkaVorVeni").setAttribute("style", "display:none;");
                        disableVorVenu();
                        disableALPPS();
                        $("#okklusia").val(0);
                    }
                    break;
                case 3:
                    document.getElementById("perevazkaVorVeni").removeAttribute("style");
                    if (!first.data) {
                        document.getElementById("alpps").setAttribute("style", "display:none;");
                        document.getElementById("okklusii").setAttribute("style", "display:none;");
                        document.getElementById("portoembolisacia").setAttribute("style", "display:none;");
                        disablePortoembolizaciu();
                        disableALPPS();
                        $("#okklusia").val(0);
                    }
                    break;
                case 4:
                    document.getElementById("okklusii").removeAttribute("style");
                    if (!first.data) {
                        document.getElementById("alpps").setAttribute("style", "display:none;");
                        document.getElementById("portoembolisacia").setAttribute("style", "display:none;");
                        document.getElementById("perevazkaVorVeni").setAttribute("style", "display:none;");
                        disablePortoembolizaciu();
                        disableVorVenu();
                        disableALPPS();
                        $("#okklusia").val(0);
                    }
                    break;
            }
        }
        checkVariant2Etapnogo({data: true});
        $("#variant2Etapnogo").on("change", false, checkVariant2Etapnogo);

        function checkOkklusii(first) {
            switch(+$("#okklusia").val()) {
                case 0:
                    if (!first.data) {
                        disablePortoembolizaciu();
                        disableVorVenu();
                        document.getElementById("perevazkaVorVeni").setAttribute("style", "display:none;");
                        document.getElementById("portoembolisacia").setAttribute("style", "display:none;");
                    }
                    break;
                case 1:
                    if (!first.data) {
                        disableVorVenu();
                        document.getElementById("perevazkaVorVeni").setAttribute("style", "display:none;");
                    }
                    document.getElementById("portoembolisacia").removeAttribute("style");
                    break;
                case 2:
                    if (!first.data) {
                        disablePortoembolizaciu();
                        document.getElementById("portoembolisacia").setAttribute("style", "display:none;");
                    }
                    document.getElementById("perevazkaVorVeni").removeAttribute("style");
                    break;
            }
        }
        checkOkklusii({data: true});
        $("#okklusia").on("change", false, checkOkklusii);

        function disablePortoembolizaciu() {
            $("#portoembType").val(0);
            $("#microspheres").prop("checked", false);
            $("#lipiodol").prop("checked", false);
            $("#spirt").prop("checked", false);
            $("#otherMaterial").prop("checked", false);
        }
        function disableVorVenu() {
            $("#perevyazkaType").val(0);
        }
        function disableALPPS() {
            $("#razobsheniePecheni").val(0);
            $("#dostup").val(0);
            $("#rezekcia").val(0);
            $("#vmeshatelstvo").val(0);
        }
    });
</script>
</body>
</html>
